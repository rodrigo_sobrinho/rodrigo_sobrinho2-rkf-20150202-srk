<?php
class TurmasUsuarios extends AppModel {
    
    var $name = 'TurmasUsuarios';
    
    var $hasAndBelongsToMany = array(
        'Usuario' =>
        array(
            'className' => 'Usuario',
            'joinTable' => 'usuarios',
            'foreignKey' => 'id',
            'associationForeignKey' => 'id',
            'unique' => true
        ),
        'Turma' =>
        array(
            'className' => 'Turma',
            'joinTable' => 'turmas',
            'foreignKey' => 'id',
            'associationForeignKey' => 'id',
            'unique' => true,
        )
    );

}
?>