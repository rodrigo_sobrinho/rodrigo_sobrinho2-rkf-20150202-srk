<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: reload"></a>
        Mapa de Mesa - Plantas
    </h2>
</div>

<button type="button" class="adicionar-planta default bg-color-greenDark">
    Adicionar
</button>

<?php if (count($mapas) > 0) : ?>
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Nome</th>
                <th scope="col">URL Imagem</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($mapas as $mapa) : ?>
            <tr>
                <td><?=$mapa['Mapa']['id']; ?></td>
                <td width="30%"><?=$mapa['Mapa']['titulo']; ?></td>
                <td><?=$mapa['Mapa']['url_imagem']; ?></td>
                <td>
                    <button type="button" class="ver-planta default mini bg-color-blueDark" urlimagem="<?= $mapa['Mapa']['url_imagem'] ?>">
                        Ver
                    </button>
                    <button type="button" class="editar-planta default mini bg-color-orange" idimagem="<?= $mapa['Mapa']['id'] ?>">
                        Editar
                    </button>
                    <button type="button" class="apagar-planta default mini bg-color-red" idimagem="<?= $mapa['Mapa']['id'] ?>">
                        Apagar
                    </button>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else : ?>
    <h2 class="fg-color-red">Nenhum Mapa Encontrado</h2>
<?php endif; ?>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $(".ver-planta").click(function(event){
            event.preventDefault();
            bootbox.alert('<img src="/'+$(this).attr('urlimagem')+'" />');
        });

        $(".editar-planta").click(function(event){
            event.preventDefault();
            bootbox.dialog('Carregando',[{
                label: 'Fechar'
            }],{
                remote: '/<?=$this->params['prefix']?>/mapa_mesas/editar_planta/'+$(this).attr('idimagem')
            });
        });

        $(".apagar-planta").click(function(event){
            event.preventDefault();
            var idimagem = $(this).attr('idimagem');
            bootbox.confirm("Tem certeza que deseja apagar esse mapa?", function(result) {
                if(result){
                    var context = ko.contextFor($(".metro-button.reload")[0]);
                    var url = '/<?=$this->params['prefix']?>/mapa_mesas/apagar_planta/'+idimagem;
                    $.ajax({
                        url : url,
                        type : "POST",
                        dataType : "json",
                        complete : function(data) {
                            context.$data.reload();
                            bootbox.hideAll();
                        }
                    });
                }
            });
        });

        $(".adicionar-planta").click(function(event){
            event.preventDefault();
            bootbox.dialog('Carregando',[{
                label: 'Fechar'
            }],{
                remote: '/<?=$this->params['prefix']?>/mapa_mesas/importar_planta/'
            });
        });
    });
</script>