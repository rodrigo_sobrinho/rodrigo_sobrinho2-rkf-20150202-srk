<?php

class ExtrasController extends AppController {

    var $name = 'Extras';
    var $uses = array('Extra', 'Evento','ViewRelatorioExtras','ViewFormandos');
    var $helpers = array('Excel');
    
    private function _remover($id = false) {

        $extra = $this->Extra->find('first', array('conditions' => array('Extra.id' => $id)));

        if ($this->Extra->delete($id)) {
            $this->Session->setFlash('Extra excluído com sucesso.', 'metro/flash/success');
        } else {
            $this->Session->setFlash('Erro ao excluir extra.', 'metro/flash/error');
        }
        json_encode(array());
    }
    
    function planejamento_remover($id = false){
        $this->_remover($id);
    }
    
    private function _alterar($id = false) {
        $this->layout = false;
        $extra = $this->Extra->find('first', array('conditions' => array('Extra.id' => $id)));
        if (!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            if ($this->Extra->save($this->data)) {
                $this->Session->setFlash('Extra foi salvo corretamente.', 'metro/flash/success');
            } else {
                $this->Session->setFlash('Um erro ocorreu na atualização do evento.', 'metro/flash/error');
            }
        } else {
            if (empty($extra)) {
                $this->Session->setFlash('Extra não existente.', 'metro/flash/error');
            } else {
                $this->data = $extra;
            }
            json_encode(array());
        }

        $this->set('extra', $extra);
    }
    
    function planejamento_alterar($id = false){
        $this->_alterar($id);
    }
    
    private function _inserir($evento_id = false) {
        $this->layout = false;
        if ($evento_id == null) {
            $this->Session->setFlash('Evento inválido ou não existente.', 'metro/flash/error');
        }
        if (!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            if(!empty($this->data['Extra']['brinde'])){
            $this->data['Extra']['nome'] = (!empty($this->data['Extra']['lote'])) ? "{$this->data['Extra']['nome']} - {$this->data['Extra']['brinde']} - {$this->data['Extra']['lote']}" : "{$this->data['Extra']['nome']} - {$this->data['Extra']['brinde']}";
                unset($this->data['Extra']['brinde']);
            }elseif(!empty($this->data['Extra']['outros'])){
                $this->data['Extra']['nome'] = (!empty($this->data['Extra']['lote'])) ? "{$this->data['Extra']['nome']} - {$this->data['Extra']['outros']} - {$this->data['Extra']['lote']}" : "{$this->data['Extra']['nome']} - {$this->data['Extra']['outros']}";
                unset($this->data['Extra']['outros']);
            }else{
                $this->data['Extra']['nome'] = (!empty($this->data['Extra']['lote'])) ? "{$this->data['Extra']['nome']} - {$this->data['Extra']['lote']}" : $this->data['Extra']['nome'];
            }
            unset($this->data['Extra']['lote']);
            if ($this->Extra->saveAll($this->data)) {
                $this->Session->setFlash('Extra criado com sucesso.', 'metro/flash/success');
            } else {
                $this->Session->setFlash('Ocorreu um erro ao criar o Extra.', 'metro/flash/error');
            }
            json_encode(array());
        }
        $this->data['Extra']['evento_id'] = $evento_id;
        $this->set('extras', array('' => 'Selecione', 'Convite Extra' => 'Convite Extra', 'Convite Infantil' => 'Convite Infantil', 'Convite Luxo' => 'Convite Luxo', 'Mesa Extra' => 'Mesa Extra', 
                                    'Combo' => 'Combo', 'Brinde' => 'Brinde', 'Outros' => 'Outros'));
        $this->set('lotes', array('' => 'Selecione', 'Primeiro Lote' => 'Primeiro Lote', 'Segundo Lote' => 'Segundo Lote', 'Terceiro Lote' => 'Terceiro Lote', 'Quarto Lote' => 'Quarto Lote',
                                  'Quinto Lote' => 'Quinto Lote', 'Benefício' => 'Benefício', 'Comissão' => 'Comissão'));
        $this->set('evento', $evento_id);
    }
    
    function planejamento_inserir($evento_id = false){
        $this->_inserir($evento_id);
    }
    
    private function _exibir($id = false) {
        $this->layout = false;
        $this->set('extra', $this->Extra->find('first', array('conditions' => array('Extra.id' => $id))));
    }
    
    function planejamento_exibir($id = false) {
        $this->_exibir($id);
    }
    
    private function _listar($evento_id = false) {
        $this->layout = false;
        $this->set('extras', $this->Extra->find('all', array('conditions' => array('Extra.evento_id' => $evento_id))));
        $this->set('evento', $this->Evento->find('first', array('conditions' => array('Evento.id' => $evento_id))));
    }
    
    function planejamento_listar($evento_id = false) {
        $this->_listar($evento_id);
    }

    function planejamento_consolidado() {
        $turma = $this->obterTurmaLogada();
        $this->loadModel("ViewRelatorioExtras");
        $extras = $this->ViewRelatorioExtras->find('all', array(
            'conditions' => array(
                'turma_id' => $turma['Turma']['id'], 'cancelada' => 0
            ),
            'group' => array('campanha_id,campanha_extra_id'),
            'fields' => array(
                'sum(quantidade_total) as quantidade_retirada',
                'campanha',
                'extra',
                'evento'
            )
        ));
        $this->set('extras', $extras);
        $options['joins'] = array(
            array(
                "table" => "protocolos",
                "type" => "inner",
                "alias" => "Protocolo",
                "conditions" => array("Protocolo.usuario_id = CheckoutUsuarioItem.usuario_id")
            ), array(
                "table" => "vw_formandos",
                "type" => "inner",
                "alias" => "ViewFormandos",
                "conditions" => array("ViewFormandos.id = Protocolo.usuario_id")
            )
        );
        $options['conditions']['ViewFormandos.turma_id'] = $turma['Turma']['id'];
        $options['conditions']['CheckoutUsuarioItem.retirou'] = 1;
        $options['group'] = 'CheckoutItem.id';
        $options['fields'] = array(
            'sum(CheckoutUsuarioItem.quantidade) as quantidade_retirada',
            'CheckoutItem.titulo'
        );
        $this->loadModel('CheckoutUsuarioItem');
        $checkout = $this->CheckoutUsuarioItem->find('all', $options);
        $this->set('checkout', $checkout);
    }
    
    function planejamento_relatorio_vendas_novo(){
        $this->layout = false;
        $this->planejamento_relatorio_vendas();
    }
    
    function planejamento_relatorio_vendas() {
        ini_set('memory_limit', "256M");
        set_time_limit(0);
        $turma = $this->obterTurmaLogada();
        $this->loadModel("CheckoutUsuarioItem");
        $options['joins'] = array(
            array(
                "table" => "protocolos",
                "type" => "inner",
                "alias" => "Protocolo",
                "conditions" => array(
                    "Protocolo.usuario_id = ViewFormandos.id",
                    "Protocolo.tipo = 'checkout'"
                )
            )
        );
        $options['conditions'] = array(
            'turma_id' => $turma['Turma']['id'],
            "situacao <> 'cancelado'"
        );
        $options['fields'] = array('ViewFormandos.*','Protocolo.*');
        $formandos = $this->ViewFormandos->find('all',$options);
        $lista = array();
        foreach($formandos as $formando) {
            $query = "select * from vw_relatorio_extras ViewRelatorioExtras where " .
                    "usuario_id = {$formando['ViewFormandos']['id']} and cancelada = 0";
            $extras = $this->ViewFormandos->query($query);
            if(!empty($formando['Protocolo']['id']))
                $checkout = $this->CheckoutUsuarioItem->find('all',array(
                    'conditions' => array(
                        'protocolo_id' => $formando['Protocolo']['id'],
                        //'retirou' => 1
                    )
                ));
            else
                $checkout = array();
            $lista[] = array(
                'ViewFormandos' => $formando['ViewFormandos'],
                'extras' => $extras,
                'checkout' => $checkout
            );
        }
        $this->set('formandos',$lista);
    }

    function planejamento_index($evento_id) {
        $cond = array(
            'Extra.evento_id' => $evento_id
        );

        $this->set('extras', $this->paginate('Extra', $cond));
        $this->set('evento', $this->Evento->find('first', array('conditions' => array('Evento.id' => $evento_id))));
    }

    function planejamento_adicionar($evento_id = null) {
        if ($evento_id == null) {
            $this->Session->setFlash('Evento inválido ou não existente.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/eventos/");
        }


        if (!empty($this->data)) {
            if ($this->Extra->saveAll($this->data)) {
                $this->Session->setFlash('Extra criado com sucesso.', 'flash_sucesso');
                $this->redirect("/{$this->params['prefix']}/extras/visualizar/{$this->Extra->id}");
            } else {
                $this->Session->setFlash('Ocorreu um erro ao criar o Extra.', 'flash_erro');
            }
        }

        $this->data['Extra']['evento_id'] = $evento_id;
        $this->set('evento', $evento_id);
    }

    function planejamento_editar($id) {

        $extra = $this->Extra->find('first', array('conditions' => array('Extra.id' => $id)));

        if (!empty($this->data)) {
            if ($this->Extra->save($this->data)) {
                $this->Session->setFlash('Extra foi salvo corretamente.', 'flash_sucesso');
                $this->redirect("/{$this->params['prefix']}/extras/visualizar/$id");
            } else {
                $this->Session->setFlash('Um erro ocorreu na atualização do evento.', 'flash_erro');
            }
        } else {
            if (empty($extra)) {
                $this->redirect("/{$this->params['prefix']}/eventos/index");
                $this->Session->setFlash('Extra não existente.', 'flash_erro');
            } else {
                $this->data = $extra;
            }
        }

        $this->set('extra', $extra);
    }

    function planejamento_visualizar($id) {
        $this->set('extra', $this->Extra->find('first', array('conditions' => array('Extra.id' => $id))));
    }

    function planejamento_excluir($id) {

        $extra = $this->Extra->find('first', array('conditions' => array('Extra.id' => $id)));

        if ($this->Extra->delete($id)) {
            $this->Session->setFlash('Extra excluído com sucesso.', 'flash_sucesso');
            $this->redirect("/{$this->params['prefix']}/extras/index/" . $extra['Evento']['id']);
        } else {
            $this->Session->setFlash('Erro ao excluir extra.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/extras/visualizar/$id");
        }
    }
    
    function planejamento_relatorio_novo(){
        $this->planejamento_relatorio();
    }

    function planejamento_relatorio() {
        $this->loadModel("ViewRelatorioExtras");
        $turma = $this->Session->read('turma');
        $query_extras = $this->ViewRelatorioExtras->query("select f.codigo_formando,f.nome,f.mesas_contrato," .
                "f.convites_contrato,i.titulo as item_checkout,c.quantidade,e.extra,e.quantidade_total as " .
                "quantidade from vw_formandos f left join vw_relatorio_extras e on e.usuario_id = f.id left" .
                " join protocolos p on (p.usuario_id = f.id and p.tipo = 'checkout') left join " .
                "checkout_usuario_itens c on c.protocolo_id = p.id left join checkout_itens i on " .
                "i.id = c.checkout_item_id where f.turma_id = " . $turma["Turma"]["id"]);
        $this->paginate["ViewRelatorioExtras"] = array(
            "limit" => "99999",
            'order' => array(
                'ViewRelatorioExtras.codigo_formando' => 'ASC',
                'ViewRelatorioExtras.campanhas_usuario_id' => 'ASC',
                'ViewRelatorioExtras.extra_id' => 'ASC'));
        $formandos = $this->paginate('ViewRelatorioExtras', array("ViewRelatorioExtras.turma_id = " . $turma["Turma"]["id"]));
        $campanhas = array();
        $status = array('inadimplente', 'pago parcialmente', 'a retirar', 'inativo', 'cancelado');
        foreach ($formandos as $formando) {
            $relatorio = $formando['ViewRelatorioExtras'];
            $extras[$formando['ViewRelatorioExtras']['extra_id']] = $formando['ViewRelatorioExtras']['extra'];
            if (!isset($campanhas[$relatorio['campanha_id']]['titulo']))
                $campanhas[$relatorio['campanha_id']]['titulo'] = $relatorio['campanha'];
            if (!isset($campanhas[$relatorio['campanha_id']]['itens'][$relatorio['extra_id']])) {
                $campanhas[$relatorio['campanha_id']]['itens'][$relatorio['extra_id']]['total'] = 0;
                $campanhas[$relatorio['campanha_id']]['itens'][$relatorio['extra_id']]['retirada'] = 0;
                $campanhas[$relatorio['campanha_id']]['itens'][$relatorio['extra_id']]['cancelada'] = 0;
                $campanhas[$relatorio['campanha_id']]['itens'][$relatorio['extra_id']]['retirar'] = 0;
                foreach ($status as $s)
                    $campanhas[$relatorio['campanha_id']]['itens'][$relatorio['extra_id']][$s] = 0;
            }
            $campanhas[$relatorio['campanha_id']]['itens'][$relatorio['extra_id']]['total']+= $relatorio['quantidade_total'];
            $campanhas[$relatorio['campanha_id']]['itens'][$relatorio['extra_id']]['retirada']+= $relatorio['quantidade_retirada'];
            $campanhas[$relatorio['campanha_id']]['itens'][$relatorio['extra_id']]['cancelada']+= $relatorio['quantidade_cancelada'];
            $campanhas[$relatorio['campanha_id']]['itens'][$relatorio['extra_id']][$relatorio['status']]+= $relatorio['quantidade_total'];
            if ($relatorio['status'] == 'a retirar')
                $campanhas[$relatorio['campanha_id']]['itens'][$relatorio['extra_id']]['retirar']+= $relatorio['quantidade_total'] - $relatorio['quantidade_retirada'] - $relatorio['quantidade_cancelada'];
        }
        $this->set('formandos', $formandos);
        $this->set('extras', $extras);
        $this->set('resumo', $campanhas);
        
    }

    function planejamento_gerar_excel() {
        $this->layout = false;
        ini_set('memory_limit', '256M');
        $this->planejamento_relatorio();
    }

    function planejamento_relatorio_checkout() {
        $this->loadModel("ViewRelatorioExtras");
        $turma = $this->Session->read('turma');
        $relatorio_checkout = $this->ViewRelatorioExtras->query("select formando.codigo_formando,formando.nome,formando.mesas_contrato," .
                "formando.convites_contrato,item.titulo as item_checkout,checkout.quantidade,extra.extra,extra.quantidade_total as " .
                "quantidade from vw_formandos formando left join vw_relatorio_extras extra on extra.usuario_id = formando.id left" .
                " join protocolos protocolo on (protocolo.usuario_id = formando.id and protocolo.tipo = 'checkout') left join " .
                "checkout_usuario_itens checkout on checkout.protocolo_id = protocolo.id left join checkout_itens item on " .
                "item.id = checkout.checkout_item_id where formando.turma_id = " . $turma["Turma"]["id"] . " order by formando.codigo_formando asc");

        $this->set('relatorio_checkout', $relatorio_checkout);
    }

}

?>