<span id="conteudo-titulo" class="box-com-titulo-header">Faculdades</span>
<div id="conteudo-container">
	<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
	<?php $session->flash(); ?>
	<div class="tabela-adicionar-item">
		<?php echo $form->create('Faculdades', array('url' => "/{$this->params['prefix']}/faculdades/procurar", 'class' => 'procurar-form-inline')) ?>
		<?php echo $form->input('chave', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
		<?php echo $form->end(array('label' => ' ', 'div' => false, 'class' => 'submit-busca')) ?>
		<?php echo $html->link('Adicionar',array($this->params['prefix'] => true, 'action' => 'adicionar')); ?>
		<div style="clear:both;"></div>
	</div>
	<div class="container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col"><?php echo $paginator->sort('Id', 'id'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Nome', 'nome'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Sigla', 'sigla'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Universidade', 'Universidade.id'); ?></th>
					<th scope="col"> &nbsp;</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="4"><?php echo $paginator->counter(array('format' => 'Itens %start% ao %end% - página %page% de %pages%')); ?>
						<span class="paginacao">
							<?php echo $paginator->numbers(array('separator' => ' ')); ?>				
						</span>
					</td>
					<td colspan="2"><?php echo $paginator->counter(array('format' => 'Total : %count% ' .  $this->name)); ?></td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach ($faculdades as $faculdade): ?>
				<?php if($isOdd):?><tr class="odd"><?php else:?><tr><?php endif;?>
					<td colspan="1" width="5%"><?php echo $faculdade['Faculdade']['id']; ?></td>
					<td colspan="1" width="30%"><?php echo $html->link($faculdade['Faculdade']['nome'], array($this->params['prefix'], 'controller' => 'Faculdades', 'action' =>'visualizar', $faculdade['Faculdade']['id'])); ?></td>
					<td colspan="1" width="5%"><?php echo $faculdade['Faculdade']['sigla']; ?></td>
					<td colspan="1" width="30%"><?php echo $faculdade['Universidade']['nome']; ?></td>
					<td  colspan="1" width="30%">
						<?php echo $html->link('Visualizar', array($this->params['prefix'] => true, 'controller' => 'Faculdades', 'action' =>'visualizar', $faculdade['Faculdade']['id']), array('class' => 'submit button')); ?>
						<?php echo $html->link('Editar', array($this->params['prefix'] => true, 'controller' => 'Faculdades', 'action' => 'editar', $faculdade['Faculdade']['id']), array('class' => 'submit button')) ?>
						<?php echo $html->link('Deletar', array($this->params['prefix'] => true, 'controller' => 'Faculdades', 'action' => 'deletar', $faculdade['Faculdade']['id']), 
							array('class' => 'submit button', 'onclick' => "javacript: if(confirm('Deseja excluir o item {$faculdade['Faculdade']['nome']}?')) return true; else return false;")); ?>
					</td>
				</tr>
				<?php if($isOdd){ $isOdd = false; } else {$isOdd = true; } ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>