<?php

class RelatorioFinanceiroController extends AppController {

    var $name = 'RelatorioFinanceiro';
    var $uses = array('FormandoProfile', 'Usuario', 'Turma');
    var $helpers = array('Html', 'Excel');
    
    private function _exibir() {
        $this->layout = false;
        $turma = $this->obterTurmaLogada();
        $this->loadModel("ViewRelatorioFinanceiro");
        $this->paginate["ViewRelatorioFinanceiro"] = array("limit" => "99999");
        $conditions = array(
            "AND" => array(
                'ViewRelatorioFinanceiro.turma_id' => $turma["Turma"]["id"],
                "OR" => array(
                    array('ViewRelatorioFinanceiro.grupo' => 'formando'),
                    array(
                        'ViewRelatorioFinanceiro.grupo' => 'comissao',
                        'ViewRelatorioFinanceiro.valor_total > 0'
                    )
                )
            )
        );
        $options = array("conditions" => $conditions);
        $options["fields"] = array(
            "count(ViewRelatorioFinanceiro.id) as total",
            "sum(if(ViewRelatorioFinanceiro.grupo = 'comissao',1,0)) as comissao",
            "sum(if(ViewRelatorioFinanceiro.status = 'Cancelado',1,0)) as cancelados",
            "sum(if(ViewRelatorioFinanceiro.status = 'Inadimplente',1,0)) as inadimplentes",
            "sum(if(ViewRelatorioFinanceiro.status = 'Inativo',1,0)) as inativos",
            "sum(if(ViewRelatorioFinanceiro.status = 'Ativo',1,0)) as ativos"
        );
        $resumo = $this->ViewRelatorioFinanceiro->find("all", $options);
        $resumo = $resumo[0][0] !== false ? $resumo[0][0] : false;
        $formandos = $this->paginate('ViewRelatorioFinanceiro',$conditions);
        $this->set('turmaLogada', $turma);
        $this->set('formandos', $formandos);
        $this->set('resumo', $resumo);
        $this->render('_exibir');
    }
    
    function atendimento_exibir() {
        $this->_exibir();
    }
    
    function comissao_exibir() {
        $this->_exibir();
    }
    
    function comercial_exibir() {
        $this->_exibir();
    }
    
    function planejamento_exibir() {
        $this->_exibir();
    }
    
    function producao_exibir() {
        $this->_exibir();
    }
    
    function super_exibir() {
        $this->_exibir();
    }

    function planejamento_index() {
        $turma = $this->obterTurmaLogada();
        $this->loadModel("ViewRelatorioFinanceiro");
        $this->paginate["ViewRelatorioFinanceiro"] = array("limit" => "99999");
        $options = array("conditions" => array("ViewRelatorioFinanceiro.turma_id" => $turma["Turma"]["id"]));
        $options["fields"] = array(
            "sum(1) as total",
            "sum(if(ViewRelatorioFinanceiro.status = 'Cancelado',1,0)) as cancelados",
            "sum(if(ViewRelatorioFinanceiro.status = 'Inadimplente',1,0)) as inadimplentes",
            "sum(if(ViewRelatorioFinanceiro.status = 'Inativo',1,0)) as inativos",
            "sum(if(ViewRelatorioFinanceiro.status = 'Ativo',1,0)) as ativos"
        );
        $resumo = $this->ViewRelatorioFinanceiro->find("all", $options);
        $resumo = $resumo[0][0] !== false ? $resumo[0][0] : false;
        $formandos = $this->paginate('ViewRelatorioFinanceiro', array("ViewRelatorioFinanceiro.turma_id = " . $turma["Turma"]["id"]));
        $this->set('turmaLogada', $turma);
        $this->set('formandos', $formandos);
        $this->set('resumo', $resumo);
    }

    function comissao_index() {
        $this->planejamento_index();
    }

    function atendimento_index() {
        $this->planejamento_index();
    }

    function comercial_index() {
        $this->planejamento_index();
    }

    function planejamento_gerar_excel() {
        ini_set('memory_limit', '256M');
        $this->planejamento_index();
    }

    function atendimento_gerar_excel() {
        $this->planejamento_index();
    }

    function comissao_gerar_excel() {
        $this->planejamento_index();
    }
    
    function comercial_gerar_excel() {
        $this->planejamento_index();
    }
    
}
