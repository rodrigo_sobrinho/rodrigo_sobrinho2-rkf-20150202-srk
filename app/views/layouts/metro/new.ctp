<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
	<?=$html->charset(); ?>
	<title><?=$title_for_layout; ?></title>
	<link rel="icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon"> 
	<link rel="shortcut icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon"> 
	<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/bootmetro.css">
	<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/bootmetro-icons.css">
	<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/bootmetro-responsive.css">
	<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/metroui/core.css">
	<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/metroui/fonts.css">
	<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/metroui/typography.css">
	<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/metroui/color.css">
	<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/metroui/tile.css">
	<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/metroui/form.css">
	<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/metroui/button.css">
	<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/metroui/dropdown.css">
	<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/metroui/icon.css">
	<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/default.css">
	<link rel="stylesheet" type="text/css" href="http://www.lab.westilian.com/falgun/css/fullcalendar.css">
	<!--[if IE 7]>
	<link rel="stylesheet" type="text/css" href="content/css/bootmetro-icons-ie7.css">
	<![endif]-->
	<script type="text/javascript" src="<?=$this->webroot?>metro/js/modernizr-2.6.2.min.js"></script>
	<script type="text/javascript" src="<?=$this->webroot?>metro/js/jquery-1.8.2.min.js"></script>
	<script type="text/javascript" src="http://knockoutjs.com/downloads/knockout-2.2.1.js"></script>
	<script>window.jQuery || document.write("<script src='<?=$this->webroot?>metro/js/jquery-1.8.2.min.js'>\x3C/script>")</script>
	<script type="text/javascript" src="<?=$this->webroot?>metro/js/metroui/input-control.js"></script>
	<script type="text/javascript" src="<?=$this->webroot?>metro/js/metroui/dropdown.js"></script>
	<script type="text/javascript" src="<?=$this->webroot?>metro/js/bootstrap/transition.js"></script>
	<script type="text/javascript" src="<?=$this->webroot?>metro/js/bootstrap/alert.js"></script>
	<script type="text/javascript" src="<?=$this->webroot?>metro/js/bootstrap/tooltip.js"></script>
	<script type="text/javascript" src="<?=$this->webroot?>metro/js/bootstrap/modal.js"></script>
	<script type="text/javascript" src="<?=$this->webroot?>metro/js/bootstrap/popover.js"></script>
	<script type="text/javascript" src="<?=$this->webroot?>metro/js/bootstrap/tab.js"></script>
	<script type="text/javascript" src="<?=$this->webroot?>metro/js/bootbox.js"></script>
	<script type="text/javascript" src="<?=$this->webroot?>metro/js/utils.js"></script>
	<script type="text/javascript" src="<?=$this->webroot?>metro/js/knockout/content.js"></script>
	<script type="text/javascript" src="<?=$this->webroot?>metro/js/knockout/mensagens.js"></script>
	<script type="text/javascript" src="http://www.lab.westilian.com/falgun/js/fullcalendar.min.js"></script>
	<script type="text/javascript" src="<?=$this->webroot?>metro/js/jquery.easing.1.3.min.js"></script>
</head>
<body>
<script type="text/html" id="tmpMensagensPreview">
		<li>
			<div class="content">
				<div class="foto">
					<!-- ko if: Mensagem.Usuario.diretorio_foto_perfil == null -->
					<i class="icon-user"></i>
					<!-- /ko -->
					<!-- ko if: Mensagem.Usuario.diretorio_foto_perfil != null -->
					<img data-bind="attr: { width: 30,
						src: 'http://sistema.asformaturas.com.br/'+Mensagem.Usuario.diretorio_foto_perfil }" />
					<!-- /ko -->
				</div>
				<div data-bind="css: { 'fg-color-red': MensagemUsuario.lida != 1, 'p': true }">
					<b data-bind="text: Mensagem.Usuario.nome"></b> 
					<span data-bind="if: Mensagem.Assunto.Item.nome != undefined,
						text: Mensagem.Assunto.Item.nome + ' '"></span>
					<span data-bind="text: Mensagem.Assunto.nome + ' '"></span>
					<!-- ko if: Mensagem.texto != null -->
					<span data-bind="text: Mensagem.texto.substring(0,18)"></span>
					<!-- /ko -->
				</div>
			</div>
		</li>
</script>
<div id="menu-topo" class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container todo">
			<a class="brand">
				<img src="<?=$this->webroot?>metro/img/logo_footer.png" />
			</a>
			<div class="nav-collapse collapse">
				<ul class="nav">
					<li><a href="#" data-bind="click: function() { hideHomeButton() }">Home</a></li>
					<li>
						<a href="#"
							data-bind="click:
								function() { page('<?=$this->webroot.$this->params['prefix']?>/usuarios/editar_dados') }">
							Meus Dados
						</a>
					</li>
					<li>
						<a href="#"
							data-bind="click:
								function() { page('<?=$this->webroot.$this->params['prefix']?>/area_financeira/dados') }">
							&Aacute;rea Financeira
						</a>
					</li>
					<li><a href="#" data-bind="click: function() { hideHomeButton() }">Mensagens</a></li>
					<li>
						<a href="#"
							data-bind="click:
								function() { page('<?=$this->webroot.$this->params['prefix']?>/eventos/listar') }">
							Eventos
						</a>
					</li>
					<li>
						<a href="#"
							data-bind="click:
								function() { page('<?=$this->webroot.$this->params['prefix']?>/campanhas/listar') }">
							Loja
						</a>
					</li>
					<li>
						<a href="#"
							data-bind="click:
								function() { page('<?=$this->webroot.$this->params['prefix']?>/parcerias/listar') }">
							Clube &Aacute;S
						</a>
					</li>
					<li>
						<a href="#"
							data-bind="click:
								function() { page('<?=$this->webroot.$this->params['prefix']?>/rifas/cupons') }">
							Rifa Online
						</a>
					</li>
					<!--
					<li class="dropdown">
						<a href="#" data-role="dropdown">
							Mais
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
							<li><a href="#" class="load-home">Clube &Aacute;S</a></li>
							<li><a href="#" class="load-home">Rifa Online</a></li>
						</ul>
					</li>
					-->
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="container todo">
	<header id="nav-bar" class="container" style="width:100%">
		<!--<div class="logo-menu"></div>-->
		<div class="row-fluid">
			<div class="span12">
				<div id="header-container">
					<a id="homeButton" class="win-backbutton home" href="#"
						data-bind="click: function() { hideHomeButton() }"></a>
					<h5>AS Formaturas</h5>
					<div class="dropdown">
						<a class="header-dropdown dropdown-toggle accent-color"
							data-role="dropdown" href="#">Iniciar<b class="caret"></b>
						</a> 
						<ul class="dropdown-menu">
							<li><a href="#" data-bind="click: function() { hideHomeButton() }">Home</a></li>
							<li>
								<a href="#"
									data-bind="click:
										function() { page('<?=$this->webroot.$this->params['prefix']?>/usuarios/editar_dados') }">
									Meus Dados
								</a>
							</li>
							<li>
								<a href="#"
									data-bind="click:
										function() { page('<?=$this->webroot.$this->params['prefix']?>/area_financeira/dados') }">
									&Aacute;rea Financeira
								</a>
							</li>
							<li><a href="#" data-bind="click: function() { hideHomeButton() }">Mensagens</a></li>
							<li>
								<a href="#"
									data-bind="click:
										function() { page('<?=$this->webroot.$this->params['prefix']?>/eventos/listar') }">
									Eventos
								</a>
							</li>
							<li>
								<a href="#"
									data-bind="click:
										function() { page('<?=$this->webroot.$this->params['prefix']?>/campanhas/listar') }">
									Loja
								</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="#" data-bind="click: function() { hideHomeButton() }">
									Clube &Aacute;S
								</a>
							</li>
							<li>
								<a href="#"
									data-bind="click:
										function() { page('<?=$this->webroot.$this->params['prefix']?>/rifas/cupons') }">
									Rifa Online
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div id="top-info" class="pull-right">
					<a id="logged-user" href="<?=$this->webroot.$this->params['prefix']?>/usuarios/logout"
						class="pull-right" rel="tooltip" title="Sair">
						<b class="iconui-switch" style="font-size:28px"></b>
					</a>
					<a id="settings" class="pull-right">
						<b class="icon-settings"></b>
					</a>
					<!--<hr class="separator pull-right"/>-->
					<?php
                    	$fotoPerfil = "img/uknown_user.gif";
                    	if(isset($usuario['Usuario']['diretorio_foto_perfil']))
                    		if(!empty($usuario['Usuario']['diretorio_foto_perfil']))
                    			if(file_exists(APP . "webroot/{$usuario['Usuario']['diretorio_foto_perfil']}"))
                    				$fotoPerfil = $usuario['Usuario']['diretorio_foto_perfil'];
                    ?>
					<a id="logged-user" data-bind="click:
										function() { page('<?=$this->webroot.$this->params['prefix']?>/usuarios/editar_dados') }"
						class="pull-right perfil" rel="tooltip" title="Editar Dados">
						<img src="<?="{$this->webroot}{$fotoPerfil}"?>" />
					</a>
					<div class="pull-left">
						<a data-bind="click:
										function() { page('<?=$this->webroot.$this->params['prefix']?>/usuarios/editar_dados') }"
							class="pull-right" rel="tooltip" title="Editar Dados">
							<h2><?=$usuario['Usuario']['nome']?></h2>
						</a>
						<h4><?=$usuario['Usuario']['grupo']?></h4>
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="row-fluid" id="conteudo">
		<?=$content_for_layout; ?>
	</div>
</div>
<footer>
	<div>
		<img src="<?=$this->webroot?>metro/img/logo_footer.png" />&nbsp; formaturas <?=date("Y")?> - Todos os direitos reservados &reg;
	</div>
</footer>
</body>
</html>