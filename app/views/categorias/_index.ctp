<?=$html->css('messages')?>
<?=$html->css('sortable')?>
<style type="text/css">
#aguarde { font-size:1.3em }
</style>
<script type="text/javascript">
	$(document).ready( function() {
		$("#inserir").click(function(event) {
			event.preventDefault();
			if($("#form-inserir").is(":visible")) {
				$("#form-inserir").fadeOut(500,function() { $("#inserir").html("Nova Categoria");});
			} else {
				$("#form-inserir").fadeIn(500,function() { $("#inserir").html("Cancelar"); });
			}
		});
		$( "ul.sort" ).sortable({ connectWith: "ul" });
		$("#enviar").click(function(event) {
			event.preventDefault();
			nome = $(this).prev().val();
			if(nome != "") {
				jQuery.ajax({
					url : "<?=$html->url(array($this->params['prefix'] => true,"controller" => "categorias", "action" => "adicionar"));?>",
					type : 'POST',
					data : { categoria : nome },
					dataType : 'json',
					error: function(response) {
						$("#aguarde").html('');
						exibirMensage("Erro","Ocorreu um erro com nossos servidores. Por favor tente novamente mais tarde.","error");
					},
					success : function(response) {
						$("#aguarde").html('');
						if(response.error) {
							$("#form-inserir").fadeOut(500, function() {
								$("#inserir").html("Nova Categoria");
								exibirMensage("Erro",response.message,"error");
							});
						} else {
							if(response.categoria == undefined) {
								window.location.href = window.location.href;
							} else {
								id = 'categoria-' + response.categoria;
								li = '<li class="ui-state-default" style="display:none" id="' + id + '">';
								li+= '<input type="text" value="' + nome + '" />';
								li+= '<div class="close"></div></li>';
								$('ul.ativo').prepend(li);
								$("#" + id).fadeIn(500, function() {
									$("#form-inserir").fadeOut(500, function() {
										$('#nome').val('');
										$("#inserir").html("Nova Categoria");
										$("#atualizar").trigger('click');
									});
								});
							}
						}
					}
				});
			} else {
				exibirMensage("Erro","Digite o nome da categoria","error");
			}
		});
		$("#atualizar").click(function(event) {
			event.preventDefault();
			categorias = {Categorias : []};
			$('ul.ativo li').each(function() {
				categorias.Categorias.push({id:$(this).attr('id').substring(10), nome:$(this).children('input').val(), ordem:$(this).index()+1,status:1});
			});
			$('ul.inativo li').each(function() {
				categorias.Categorias.push({id:$(this).attr('id').substring(10), nome:$(this).children('input').val(),ordem:0,status:0});
			});
			$("#aguarde").html('Aguarde...');
			jQuery.ajax({
				url : "<?=$html->url(array($this->params['prefix'] => true,"controller" => "categorias", "action" => "atualizar"));?>",
				type : 'POST',
				data : { categorias : categorias },
				dataType : 'json',
				error: function(response) {
					$("#aguarde").html('');
					exibirMensage("Erro","Ocorreu um erro com nossos servidores. Por favor tente novamente mais tarde.","error");
				},
				success : function(response) {
					$("#aguarde").html('');
					if(response.error)
						exibirMensage("Erro",response.message,"error");
					else
						exibirMensage("Sucesso","Categorias foram atualizadas com sucesso","success");
				}
			});
		});
		$('.close').live('click',function() {
			if(confirm('Quer apagar a categoria?')) {
				obj = $(this).parent();
				categoria = { id:obj.attr('id').substring(10) }
				jQuery.ajax({
					url : "<?=$html->url(array($this->params['prefix'] => true,"controller" => "categorias", "action" => "apagar"));?>",
					type : 'POST',
					data : { categoria : categoria },
					dataType : 'json',
					error: function(response) {
						$("#aguarde").html('');
						exibirMensage("Erro","Ocorreu um erro com nossos servidores. Por favor tente novamente mais tarde.","error");
					},
					success : function(response) {
						$("#aguarde").html('');
						if(response.error) {
							exibirMensage("Erro",response.message,"error");
						} else {
							obj.fadeOut(500, function() { obj.remove(); $("#atualizar").trigger('click'); });
						}
					}
				});
			}
		});
		function exibirMensage(titulo,mensagem,tipo) {
			obj = $("#message");
			html = "<h3>" + titulo + "</h3><p>" + mensagem + "</p>";
			obj.addClass(tipo);
			obj.html(html);
			obj.fadeIn(500, function() {
				setTimeout(function() {
					obj.fadeOut(500, function() {
						obj.removeClass(tipo);
						obj.html('');
					});
				},4000);
			});
		}
	});
</script>
<span id="conteudo-titulo" class="box-com-titulo-header">Parcerias</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<span id='aguarde'></span>
	<div id='message' class='message'>
	</div>
	<div class="tabela-adicionar-item">
		<div style='float:left; display:none' id='form-inserir'>
			<p>
				Nome da Categoria:&nbsp;
				<input type='text' name='nome' size="20" id='nome' style="padding:4px; margin-right:20px" />
				<input type="submit" class='button' id='enviar' value="Inserir" />
			</p>
		</div>
		<?php echo $html->link('Nova Categoria',array($this->params['prefix'] => true, 'action' => 'adicionar'),array('id' => 'inserir')); ?>
		<div style="clear:both;"></div>
	</div>
	<div class='clearfix'>
		<div class="div-sortable">
			<div class='titulo'>Categorias Ativas</div>
			<ul id="sortable1" class='ativo sort'>
				<?php if(sizeof($categorias['ativas']) > 0) { foreach($categorias['ativas'] as $categoria) { ?>
				<li class="ui-state-default" id="categoria-<?=$categoria['Categoria']['id']?>">
					<input type="text" value="<?=$categoria['Categoria']['nome']?>" />
					<div class='close'></div>
				</li>
				<?php } } ?>
			</ul>
		</div>
		
		<div class="div-sortable">
			<div class='titulo'>Categorias Inativas</div>
			<ul id="sortable2" class='inativo sort'>
				<?php if(sizeof($categorias['inativas']) > 0) { foreach($categorias['inativas'] as $categoria) { ?>
				<li class="ui-state-default" id="categoria-<?=$categoria['Categoria']['id']?>">
					<input type="text" value="<?=$categoria['Categoria']['nome']?>" />
					<div class='close'></div>
				</li>
				<?php } } ?>
			</ul>
		</div>
		
	</div>
	<input type="submit" class='submit button' value="Atualizar" id="atualizar" style="float:left; margin-top:20px" />
</div>