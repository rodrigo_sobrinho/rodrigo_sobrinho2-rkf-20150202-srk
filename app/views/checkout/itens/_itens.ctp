<?= $html->script('checkout_itens') ?>
<?= $html->script('jquery.tools.min'); ?>
<?= $html->css('tooltip'); ?>
<?= $html->css('messages'); ?>
<script type="text/javascript">
    $(function() {
        jQuery(".help").tooltip();
        $('.adicionar').live('click', function() {
            checkout_itens.criar({item: checkout_itens.itens.length});
        });
        $('.remover').live('click', function() {
            checkout_itens.remover($(this).parent().parent().parent().attr('id'));
        });
        $('#confirmar').click(function() {
            erros = [];
            $("#container-itens table tbody tr").each(function(i, tr) {
                if ($(this).find('.titulo').val() == "")
                    erros.push('Digite o titulo do item ' + (i + 1));
                if ($(this).find('.valor').val() == "")
                    erros.push('Digite o valor do item ' + (i + 1));
                if ($(this).find('.quantidade').val() == "")
                    erros.push('Digite a quantidade maxima para vendas do item ' + (i + 1));
                if ($(this).find('.itens_por_pessoa').val() == "")
                    erros.push('Digite a quantidade maxima por pessoa do item ' + (i + 1));
                if ($(this).find('.quantidade_convites').val() == "")
                    erros.push('Digite a quantidade de convites do item ' + (i + 1));
                if ($(this).find('.quantidade_mesas').val() == "")
                    erros.push('Digite a quantidade de mesas do item ' + (i + 1));
            });
            if (erros.length > 0) {
                mensagens = "";
                $.each(erros, function(i, erro) {
                    mensagens += erro + "<br />";
                });
                mensagens = mensagens.substring(0, mensagens.length - 6);
                message.exibir(mensagens, 'error');
                return false;
            } else {
                return true;
            }
        });
    });
</script>
<style type='text/css'>
    #container-itens { float:left; width:100%; margin-top:10px }
    #container-itens table td,th { text-align:left }
    #container-itens table th { padding:10px!important }
</style>
<span id="conteudo-titulo" class="box-com-titulo-header">Itens para venda no Checkout</span>
<?php $session->flash(); ?>
<?= $form->create('CheckoutItem', array('url' => "/{$this->params['prefix']}/checkout/itens")); ?>
<?= $form->hidden('CheckoutItem.turma_id', array('value' => $turma['Turma']['id'])); ?>
<div id="conteudo-container" style="margin:0">
    <div id='container-itens'>
        <div class='message'></div>
        <table style='width:100%'>
            <thead>
                <tr>
                    <th width="20%">Título</th>
                    <th width="20%">Valor</th>
                    <th width="20%">
                        <span class='item'>Qtd p/ Venda
                            <div class='help' title='Quando ultrapassada a quantidade, a venda será suspensa'></div>
                        </span>
                    </th>
                    <th width="20%">
                        <span class='item'>Qtd por pessoa</span>
                    </th>
                    <th width="20%"></th>
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($itens)) : foreach ($itens as $indice => $item) : ?>
                    <script type="text/javascript">
                        checkout_itens.adicionar(<?= json_encode($item['CheckoutItem']) ?>,<?= $indice ?>);
                    </script>
                    <tr id='<?= $indice ?>'>
                    <input name='data[CheckoutItem][itens][<?= $indice ?>][id]' type='hidden' value="<?= $item['CheckoutItem']['id'] ?>" />
                    <td>
                        <input name='data[CheckoutItem][itens][<?= $indice ?>][titulo]' type='text' value="<?= $item['CheckoutItem']['titulo'] ?>" size='20' class='titulo' />
                    </td>
                    <td>
                        <input name='data[CheckoutItem][itens][<?= $indice ?>][valor]' type='text' value="<?= number_format($item['CheckoutItem']['valor'], 2, ',', '.') ?>" size='5' class='valor' />
                    </td>
                    <td>
                        <input name='data[CheckoutItem][itens][<?= $indice ?>][quantidade]' type='text' value="<?= $item['CheckoutItem']['quantidade'] ?>" size='5' class='quantidade' />
                    </td>
                    <td>
                        <input name='data[CheckoutItem][itens][<?= $indice ?>][itens_por_pessoa]' type='text' value="<?= $item['CheckoutItem']['itens_por_pessoa'] ?>" size='5' class='itens_por_pessoa' />
                    </td>
                    <td>
                        <div class='item'><span class='remover'>Remover</span></div>
                    </td>
                    </tr>
                <?php endforeach;
            endif; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td class='item'><div class="adicionar">Adicionar</div></td>
                </tr>
                <tr>
                    <td colspan='4'>
<?= $form->end(array('label' => 'Confirmar', 'id' => 'confirmar', 'div' => false, 'class' => 'submit')); ?>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>