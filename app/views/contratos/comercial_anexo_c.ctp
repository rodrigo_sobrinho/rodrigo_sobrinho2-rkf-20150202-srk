<?php
/* 
 * Dados recebidos do controller são:
 * $parametros['faculdade']
 * $parametros['curso']
 * $parametros['numero_aderentes']
 * $turma['data_conclusao']
 * $turma['valor_formando']
 * $turma['data_assinatura_contrato']
 * $turma['beneficios_comissao']
 * $turma['negociacoes_contratuais']
 * $turma['valor_total_contrato']
 */

App::import('Vendor','xtcpdf');  
$tcpdf = new XTCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans' 

$tcpdf->SetAuthor("RK Formaturas http://rkformaturas.com.br/"); 

$textfont = 'helvetica';
 
$tcpdf->SetAuthor("RK Formaturas");
 
$tcpdf->setPrintHeader(false);
$tcpdf->setPrintFooter(false);
$tcpdf->SetMargins(30, 40, 30);
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'',9);
 
$tcpdf->AddPage();
 
// Cria string de formandos com nome e RG
$lista_comissao = $this->data['Contrato']['nomes_rgs'];

// create some HTML content
$htmlcontent = 

"
<p><p><b>ANEXO C – COMISSÃO DE FORMATURA</b></p>

<p>Faculdade: <b>".$this->data['Contrato']['faculdade']."</b> </p>

<p>Curso: <b>".$this->data['Contrato']['cursos']."</b></p>

<p>Conclusão: <b>".$this->data['Contrato']['previsao_conclusao']."</b></p>

<p>Fica estipulada a criação de comissão de formatura do curso, faculdade e conclusão acima listados, representando os alunos nos assuntos referentes à formatura. </p>

<p>Os membros são:</p>

".$lista_comissao;

// output the HTML content
$tcpdf->writeHTML($htmlcontent, true, 0, true, 0, 'J');

echo $tcpdf->Output(date('Y-m-d H:i:s').' - Anexo C.pdf', 'D'); 
?>