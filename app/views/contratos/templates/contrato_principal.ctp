<?php if($out == "html") : ?>
<?php include('salvar_contrato.ctp')?>
<?php endif; ?>
<?php 
setlocale( LC_ALL, 'pt_BR', 'pt_BR.iso-8859-1', 'pt_BR.utf-8', 'portuguese'); 
$mes = strftime("%B");
?>
<h4 style="text-align: center"><b>CONTRATO DE PRESTAÇÃO DE SERVIÇOS</b></h4>
<br />
<p style="text-align: justify">
Pelo presente instrumento particular, de um lado:
<br />
<br />
Na qualidade de <b>Contratada: ÁS EVENTOS LTDA</b>, pessoa jurídica de direito privado devidamente inscrita no CNPJ/MF sob o 
nº 06.163.144/0001-45, com sede a Rua dos Bogaris, no4 - Mirandópolis, São Paulo/SP - CEP 04047-020 e Coligadas, neste 
ato representada por seu sócio, o Sr. Rachid Sader, brasileiro, casado, empresário, portador da cédula de identidade 
RG n.º 26.572.035 e inscrito no CPF sob o n.º 289.174.098-06, residente e domiciliado à Alameda dos Uapes, 265 – Casa 4, 
Planalto Paulista, São Paulo - SP e/ou por seu Procurador.
<br />
<br />
E de outro lado:
<br />
<br />
Na qualidade de <b>Contratantes</b>: Os formandos do(s) curso(s) de <b><?=$turma['Turma']['cursos']?></b>,
da <b><?=$turma['Turma']['nome']?></b>, com previsão de formatura para o <b><?=($turma['Turma']['semestre_formatura'] == 1) ? 'primeiro' : 'segundo';?> 
    semestre de <?=$turma['Turma']['ano_formatura']?></b>, neste ato devidamente representados pela <b>Comissão de Formatura</b>;
<br />
<br />
<b>CONSIDERANDO</b> que:
<br />
<p style="margin-left: 30px; text-align: justify">
• Os formandos do(s) curso(s) de <b><?=$turma['Turma']['cursos']?></b>,
da <b><?=$turma['Turma']['nome']?></b> (doravante denominados coletivamente como "Formandos" ou individualmente
como "Formando"), desejam comemorar a conclusão do curso através da realização de um evento de formatura;
<br />
<br />
• Para que todas as atividades relacionadas ao evento de formatura sejam realizadas de forma organizada e ágil, foram eleitos 
membros integrantes dos Formandos e criada uma comissão de formatura (doravante denominada simplesmente “Comissão”) que 
representará a vontade dos Formandos, contratando e executando todas as atividades em nome destes;
<br />
<br />
• A <b>Contratada</b> é uma empresa renomada e que está no mercado de eventos e formaturas desde <b>2004</b>, detendo o conhecimento 
técnico e mercadológico, parceria com fornecedores conceituados e qualificados, possuindo uma estrutura adequada para a realização 
do evento de formatura;
<br />
</p>
<b>RESOLVEM</b> firmar o presente Contrato de Prestação de Serviços (o “Contrato”), que será regido pelos termos e condições a seguir 
estipulados, em conformidade com os artigos 125 e seguintes do Código Civil Brasileiro:
<br />
<br />
<b style="text-decoration: underline">1. DO OBJETO</b>
<br />
<br />
<b>1.1.</b> É objeto do presente instrumento a contratação da <b>Contratada</b> pelos <b>Contratantes</b>, devidamente representados pela Comissão, 
conforme Anexo ‘C’, para a prestação de serviços especializados para a realização de um evento de formatura (doravante denominado 
simplesmente “Evento”) na forma e termos descritos no Anexo ‘A’.
<br />
<br />
<p style="margin-left: 30px; text-align: justify">
<b>1.1.1.</b> Fica desde já estabelecido que a Comissão que assina esse Contrato não tem nenhum tipo de obrigação ou responsabilidade 
civil ou financeira referente a este Contrato. Assim sendo, toda e qualquer obrigação ou responsabilidade civil ou financeira é 
de inteira responsabilidade da <b>Contratada</b>.
</p>
<b>1.2.</b> A <b>Contratada</b> providenciará a contratação de todos os serviços e produtos mencionados no Anexo ‘A’ para a 
realização do Evento.<br/><br/>
<b>1.3.</b> Também é objeto do presente instrumento a prestação de serviços da <b>Contratada</b> aos <b>Contratantes</b> de cobertura fotográfica 
e de filmagem do Evento, além da venda das respectivas fotografias e filmes, devidamente editados, na forma e termos descritos 
no Anexo ‘E’.
<br />
<br />
<b style="text-decoration: underline">2. DOS DOCUMENTOS INTEGRANTES AO CONTRATO (“ANEXOS”)</b>
<br />
<br />
<b>2.1.</b> São partes integrantes do Contrato os seguintes documentos:
<br />
<br />
<p style="margin-left: 30px; text-align: justify">
<b>a)</b> Anexo ‘A’ – Planilha de Preços e Serviços (doravante denominada simplesmente “Planilha”);<br/><br/>
<b>b)</b> Anexo ‘B’ – Termo de Finalização da negociação;<br/><br/>
<b>c)</b> Anexo ‘C’ – Ata de Constituição da Contratante;<br/><br/>
<b>d)</b> Anexo ‘D’ – Modelo de Contrato de Adesão e Compromisso;<br/><br/>
<b>e)</b> Anexo ‘E’ – Contrato de Cobertura Fotográfica e de Filmagem;<br/><br/>
<b>f)</b> Anexo ‘F’ – Boletim Informativo.<br/><br/>
<b>g)</b> Anexo ‘G’ – Informações sobre valores arrecadados e a arrecadar.<br/><br/>
<b>Parágrafo Primeiro</b> – A Planilha do Anexo ‘A’ foi devidamente aprovada pela Comissão em <?=$turma['Turma']['data_assinatura_contrato']?>. A Planilha contém e destacam 
os serviços e produtos que comporão o Evento, definidos pelas partes em conjunto, assim <font style="text-decoration: underline">como a explicitação da taxa administrativa
praticada pela <b>Contratada</b></font>.<br/><br/>
<b>Parágrafo Segundo</b> – O Anexo ‘B’ refere-se a um padrão de ata que deverá ser assinado pelas Partes toda vez que houver 
acertos verbais estabelecidos e extracontratuais.<br/><br/>
<b>Parágrafo Terceiro</b> – O Anexo ‘C’ refere-se à ata de nomeação e constituição da Comissão;<br/><br/>
<b>Parágrafo Quarto</b> – O Anexo ‘D’ refere-se ao contrato de adesão individual a ser firmado entre cada formando e a <b>Contratada</b>,
cujos termos ficarão vinculados ao presente Contrato tão logo cada Formando venha a aderir ao Evento, objeto deste Contrato e 
descrito no Anexo A;<br/><br/>
<b>Parágrafo Quinto</b> – O serviço do Anexo ‘E’, conforme convencionado em conjunto pelas Partes, será realizado pela <b>Contratada</b>
ou por empresa que esta subcontratar ou terceirizar os serviços.<br/><br/>
<b>Parágrafo Sexto</b> – O anexo ‘F’ refere-se aos valores e formas de pagamento para adesão dos Formandos ao Evento.<br/><br/>
<b>Parágrafo Sétimo</b> – O anexo ‘G’ refere-se aos acordos de pagamento celebrados entre a <b>Contratada</b> e <b>Contratante</b>.<br/><br/>
</p>
<br />
<br />
<b style="text-decoration: underline">3. DO PREÇO, REAJUSTE E FORMA DE PAGAMENTO</b>
<br />
<br />
<b>3.1.</b> Ficam desde já estipuladas entre as Partes as seguintes condições gerais do contrato:
<br />
<br />
<p style="margin-left: 30px; text-align: justify">
<b>3.1.1.</b> Número mínimo de Formandos aderentes: ________________________________________ (número por extenso)<br/><br/>
<b>3.1.2.</b> Valor total do Contrato: R$_______,00 (____________________________ reais).<br/><br/>
<b>3.1.3.</b> Valor por Formando: R$_______,00 (____________________________ reais).<br/><br/>
</p>
<b>3.2.</b> A <b>Contratada</b> obriga-se a realizar os Serviços do Anexo A, representando os <b>Contratantes</b> na contratação de terceiros 
pelo preço estabelecido na cláusula 3.1.2, rateado entre o número de Formandos definidos na cláusula 3.1.1, cabendo 
individualmente ao Formando cumprir com todas as responsabilidades e obrigações previstas no Contrato de Adesão e Compromisso 
– Anexo ‘D’, especialmente pelo pagamento da cota estipulada na cláusula 3.1.3, que poderá ser pago conforme as opções de 
pagamento descritas no Anexo ‘F’.<br/><br/>
<b>3.3.</b> Dos valores e pagamentos oriundos deste Contrato, a <b>Contratada</b> fará jus aos valores de taxa de administração destacados
no Anexo ‘A’.<br /><br />
<b>3.4.</b> O valor dos serviços de cobertura fotográfica e de filmagem do Evento – Anexo ‘E’ serão cobrados nos termos do referido 
Anexo (valor e forma de pagamento), sendo que as demais disposições previstas nesta cláusula se aplicam aos valores lá 
estabelecidos.<br /><br />
<b>3.5.</b> Os valores das parcelas remanescentes serão reajustados após o 12º (décimo segundo) mês, contados da data da assinatura 
do presente Contrato e não da data de adesão de cada Formando, conforme a variação do índice IGP-M, divulgado pela Fundação 
Getúlio Vargas. Caso esse índice seja extinto, o índice de referência para o cálculo de reajuste será substituído por outro 
índice que venha a ser publicado pelo Governo para substituí-lo qualitativamente, ou seja, para refletir a inflação do período 
acima descrito. Na ausência de um índice oficial publicado pelo Governo, as partes acordarão, de boa fé, acerca do índice de 
reajuste alternativo.<br /><br />
<b>3.6.</b> A <b>Contratada</b> garante, desde já, que se obriga à prestação de todos os serviços nas mesmas condições e preços 
estabelecidos neste Contrato e seus Anexos, desde que seja alcançado o número mínimo de Formandos aderentes estabelecido na 
cláusula 3.1.1. Caso esse número não seja atingido, as Partes se comprometem a repactuar as condições e preços estabelecidos 
neste Contrato e seus Anexos ao número de Formandos participantes, ficando o compromisso das Partes, desde já, em não onerar 
ou aumentar as cotas individuais de cada Formando.<br /><br />
<p style="margin-left: 30px; text-align: justify">
<b>3.6.1.</b> Entende-se por Formando aderente aquele que aderir aos termos e assinar o Contrato de Adesão e Compromisso – 
Anexo ‘D’, cumprir com as obrigações financeiras estabelecidas e não desistir do Evento.
</p>
<b>3.7.</b> Em caso de falecimento, reprovação, transferência de universidade, transferência de Estado, desistência do curso, ou 
enfermidade, o Formando (ou seu representante legal) receberá a quantia já paga corrigida pelo mesmo índice previsto no Contrato.<br/><br/>
<b>3.8.</b> A desistência por motivo diverso dos descritos acima, poderá ser feita até 90 (noventa) dias antes do termino do ano 
letivo e implicará na perda de <b style="text-decoration: underline">20% (vinte por cento)</b> do valor contratual, em favor da 
<b>Contratada</b>. Caso a prestação dos serviços e/ou o fornecimento dos produtos já tenha sido parcialmente cumprido o montante devido 
por qualquer desistência nos termos desta cláusula será de <b style="text-decoration: underline">50% (cinqüenta por cento)</b> do valor 
contratual. Após este período de 90 (noventa) dias e por qualquer desistência nos termos desta cláusula será exigido do Formando 
o pagamento integral do Contrato.<br/><br/>
<b>3.9.</b> <b>A Contratada</b>, ou quem esta indicar, se incumbirá de emitir boleto bancário de arrecadação a cada Formando através de sua 
plataforma online, que o Formando poderá acessar pelo website <a href="http://www.asformaturas.com.br" target="_blank">http://www.asformaturas.com.br</a>, 
devendo se dirigir para a área específica para formandos. Havendo qualquer dificuldade técnica para o acesso, o Formando deverá entrar 
em contato diretamente com a <b>Contratada</b> através de e-mail ou telefones disponíveis.<br/><br/>
<b>3.10.</b> Eventual atraso do Formando em efetuar os pagamentos acarretará multa de <b style="text-decoration: underline">10% (dez por cento)</b>
, mais juros de mora de <b style="text-decoration: underline">1% (um por cento)</b> ao mês sobre o valor em atraso, calculados 
desde a data do vencimento até a data do efetivo pagamento.<br/><br/>
<b>3.11.</b> É de inteira responsabilidade de a Contratada assumir os reajustes de preços dos serviços e locações constantes no 
Anexo ‘A’, não cabendo quaisquer responsabilidades aos <b>Contratantes</b>.
<b>3.12.</b> Serviços e produtos não constantes do Anexo ‘A’ e que eventualmente os <b>Contratantes</b> solicitem à <b>Contratada</b>
, e caso ambas as Partes cheguem a um acordo referente ao preço e forma de prestação de serviço e/ou fornecimento de produto, 
deverão ser tratados em documento específico e firmados mediante a assinatura de um Aditivo Contratual.<br/><br/>
<br />
<br />
<b style="text-decoration: underline">4. DA ARRECADAÇÃO E DA GESTÃO DOS VALORES PAGOS PELOS FORMANDOS</b>
<br />
<br />
<b>4.1.</b> A emissão de boletos, a gestão dos valores efetivamente pagos pelos Formandos para a realização do Evento em razão deste 
contrato, assim como os pagamentos aos fornecedores e prestadores de serviços elencados no Anexo ‘A’ serão realizados pela 
empresa <b>ZAPE GESTÃO DE VALORES LTDA</b>., pessoa jurídica de direito privado devidamente inscrita no CNPJ/MF sob o 
nº 18.405.543/0001-54, com sede na Avenida José Maria Whitaker, 882, Planalto Paulista, São Paulo/SP, CEP 04057-000, neste 
ato representado na forma de seu contrato social (doravante denominada simplesmente <b>“Zape”</b>).<br /><br />
<b>4.2.</b> A <b>Zape</b> é parte integrante do presente Contrato e obriga-se a atender, no que for de sua responsabilidade, todas as 
disposições do mesmo, especialmente no que se refere a arrecadação, gestão de valores e pagamentos aos fornecedores e 
prestadores de serviços a serem contratados para a realização do Evento, bem como outras definições no contrato assinado 
entre a <b>Zape</b> e a Comissão de Formatura.<br /><br />
<b>4.3.</b> Ainda que a gestão financeira dos recursos oriundos deste Contrato seja de responsabilidade da <b>Zape</b>, a <b>Contratada</b> 
é responsável pela contratação de todos os prestadores de serviço e fornecedores destacados no Anexo ‘A’;<br /><br />
<b>4.4.</b> Os valores pagos individualmente pelos Formandos permanecerão em posse da <b>Zape</b> de forma transitória, até o momento
do pagamento e/ou repasse dos valores devidos aos fornecedores e prestadores de serviços do Evento, assim como pagamento dos
valores devidos à <b>Contratada</b> pela prestação de serviços objeto do presente Contrato, conforme termos e valores descritos no 
Anexo ‘A’.<br /><br />
<b>4.5.</b> A <b>Zape</b> é uma empresa especializada em gestão de valores e foi devidamente contratada pela <b>Contratante</b> para esta 
finalidade e para o Evento. O contrato assinado entre as partes define as responsabilidades de cada parte.
<br />
<br />
<b style="text-decoration: underline">5. DAS OBRIGAÇÕES E RESPONSABILIDADES DA CONTRATADA</b>
<br />
<br />
<b>5.1.</b>	Obriga-se a <b>Contratada</b> a:
<br />
<br />
<p style="margin-left: 30px; text-align: justify">
<b>5.1.1.</b> A <b>Contratada</b> declara ter estrutura adequada para atender ao objeto do presente Contrato e assume, neste ato, 
o compromisso de organizar e realizar todas as atividades descritas no Anexo ‘A’;.<br/><br/>
<b>5.1.2.</b> Responsabilizar-se integralmente pela prestação dos serviços, execução de subcontratações, fornecimento de produtos
e prestação de informações que se façam necessárias ou que sejam solicitadas pela <b>Contratante</b>;<br/><br/>
<b>5.1.3.</b> Cumprir as posturas dos municípios, bem como as disposições legais, seja elas estaduais ou federais, que interfiram
na prestação dos serviços ou fornecimento dos produtos;<br/><br/>
<b>5.1.4.</b> Reparar, corrigir, remover, substituir, no todo ou em parte, quaisquer itens constantes do Anexo ‘A’ e do Anexo ‘E’
do presente Contrato, em que se verifiquem vícios, defeitos ou incorreções resultantes da execução ou de materiais empregados, por
qualquer uma das Partes, sem com isso repassar qualquer custo aos <b>Contratantes</b>, desde que por culpa exclusiva da <b>Contratada</b>, e ou
terceiros contratados por esta, dentro de um prazo de 30 (trinta) dias úteis após a notificação por parte dos <b>Contratantes</b> à 
<b>Contratada</b>, salvo nos casos de produtos de uso inferior a 30 (trinta) dias, que deverão ser trocados em tempo hábil para a 
utilização dos mesmos;<br/><br/>
<b>5.1.5.</b> Fornecer para controle dos <b>Contratantes</b> listagens dos pagamentos efetuados pelos Formandos, sempre que solicitados 
por escrito, sendo expressamente proibido aos <b>Contratantes</b> a sua divulgação pública;<br/><br/>
<b>5.1.6.</b> Executar os serviços dentro dos melhores padrões de qualidade, utilizando-se do que for convencionado com os 
<b>Contratantes</b>, empregando material de primeira qualidade;<br/><br/>
<b>5.1.7.</b> Operar com empregados, mesmo os terceirizados, devidamente treinados e preparados para a execução das atividades ora 
contratadas, em número suficiente à perfeita execução dos serviços;<br/><br/>
<b>5.1.8.</b> A <b>Contratada</b> se responsabilizará por todo dano causado em qualquer convidado no Evento desde que oriundos da 
montagem do Evento, de seus funcionários ou prepostos (garçom, segurança, equipe de coordenação, recepcionistas, etc);<br/><br/>
<b>5.1.9.</b> A <b>Contratada</b>, de posse dos Contratos de Adesão e Compromisso do número mínimo de Formandos aderentes, mesmo que haja
inadimplência por parte de> alguns Formandos, se compromete a cumprir o que ora fica convencionado neste Contrato;<br/><br/>
<b>5.1.10.</b> Caso o número de formandos que realizarem adesão for superior ao estabelecido no item 3.1.1, é obrigação da 
<b>Contratada</b> reverter 50% (cinquenta por cento) de cada adesão adicional para um fundo de caixa a ser usado pela <b>Contratante</b>. 
Esse fundo de caixa pode ser usado para novos investimentos no Evento, tais como inclusão de novos itens, contratação de novos 
serviços ou até mesmo reembolso aos Formandos.<br/><br/>
<b>5.1.11.</b> Apresentar aos <b>Contratantes</b>, quando solicitado, a situação financeira do presente Contrato, bem como da situação 
individual de cada Formando, sobre as quais deverá manter sigilo.<br/><br/>
</p>
<br />
<br />
<b style="text-decoration: underline">6. DAS OBRIGAÇÔES E RESPONSABILIDADES DOS CONTRATANTES</b>
<br />
<br />
<b>6.1. Obrigam-se os Contrantes:</b>
<br />
<br />
<p style="margin-left: 30px; text-align: justify">
<b>6.1.1.</b> Fornecer o número mínimo de formandos pagantes estabelecido no item 3.1.1, possibilitando-se, assim, a quitação 
total do presente Contrato;.
<p style="margin-left: 60px; text-align: justify">
<b>6.1.1.1.</b> Caso o número de adesões de Formandos aderentes e pagantes, participantes do Evento previstos neste Contrato 
não seja alcançado até o fim do ano letivo, a <b>Contratada</b> poderá alterar os produtos e/ou serviços ora contratados, de forma 
a compensar o débito eventualmente existente, uma vez que o custo individual a ser pago pelo Formando, a pedido exclusivo 
dos <b>Contratantes</b>, foi calculado dividindo-se o custo total deste instrumento pelo número estabelecido no item 3.1.1. Esse 
valor pago individualmente não sofrerá em hipótese alguma nenhum tipo de reajuste;<br/><br/>
<b>6.1.1.2.</b> A alteração de produtos e/ou serviços será feita de comum acordo entre as Partes;<br/><br/>
<b>6.1.1.3.</b> Definida a alteração de produtos e/ou serviços a <b>Contratada</b> apresentará aos <b>Contratantes</b> a 
adequação do Contrato.
</p>
</p>
<p style="margin-left: 30px; text-align: justify">
<b>6.1.2.</b> Divulgar o presente Contrato junto aos Formandos a fim de se atingir o número mínimo de adesões;<br/><br/>
<b>6.1.3.</b> Fornecer as informações necessárias à fiel execução do presente Contrato, em tempo hábil, determinado quando 
da solicitação das mesmas;<br/><br/>
<b>6.1.4.</b> No que se refere aos convites, descritos no Anexo ‘A’, fornecer especificações relativas aos textos, fotolitos,
desenhos, distintivos, brasões, medalhas, gravuras ou quaisquer outras especificações que tenham sido previamente tratadas com
a antecedência de 60 (sessenta) dias da data de entrega do serviço.
</p>
<b>6.2.</b>	Uma vez confirmados as datas do Evento junto a <b>Contratada</b> e esta se comprometendo com terceiros, os <b>Contratantes</b>
não mais poderão alterá-las, salvo motivo de força maior.<br /><br />
<br />
<br />
<b style="text-decoration: underline">7. DAS OBRIGAÇÕES RECÍPROCAS DAS PARTES</b>
<br />
<br />
<b>7.1.</b> Será de responsabilidade tanto da <b>Contratante</b> como da <b>Contratada</b> a formulação de listas como os nomes e dados 
relevantes dos Formandos, para que os mesmos façam as devidas alterações e correções em tempo hábil. A fiscalização dos 
convites já impressos também será de responsabilidade de ambas as partes.<br /><br />
<b>7.2.</b> Convocar e realizar reuniões com o departamento de Atendimento da <b>Contratada</b>, desde que marcada com 
antecedência mínima de 5 (cinco) dias úteis, encaminhando obrigatoriamente o pedido por e-mail, contendo a pauta da reunião, 
com clara especificação dos assuntos a serem abordados, a fim de que a <b>Contratada</b> possa programar-se, visando a 
antecipação das possíveis soluções.<br/><br/>
<b>7.3.</b> Verificar e fiscalizar todos e quaisquer produtos e serviços que estão orçados no Anexo ‘A’, através de degustações,
apresentações, visita a fornecedores terceirizados e através de solicitação prévia para o atendimento da <b>Contratada.</b><br /><br />
<br />
<br />
<b style="text-decoration: underline">8. DAS DECLARAÇÕES DAS PARTES</b>
<br />
<br />
<b>8.1.</b> As Partes declaram que:
<br />
<br />
<p style="margin-left: 30px; text-align: justify">
<b>8.1.1.</b> Este Contrato é firmado com estrita observância ao princípio da boa-fé contratual;<br/><br/>
<b>8.1.2.</b> O presente Contrato é celebrado de forma irretratável e irrevogável, obrigando as Partes a qualquer título;<br/><br/>
<b>8.1.3.</b> Mediante a sua assinatura, prevalecerá o Contrato e seus respectivos Anexos, substituindo todos os outros documentos, cartas, memorandos, e-mail ou propostas entre as partes, bem como os entendimentos verbais mantidos entre as mesmas, anteriores a presente data;<br/><br/>
<b>8.1.4.</b> Qualquer alteração deverá ocorrer mediante a assinatura de termo aditivo;<br/><br/>
<b>8.1.5.</b> A nulidade de qualquer cláusula ou condição deste Contrato não afetará a validade ou exequibilidade das demais cláusulas e condições deste Contrato como um todo. Caso qualquer uma das cláusulas ou condições do presente Contrato seja considerada nula, inválida ou inexequível, as partes comprometem-se a negociar em boa-fé a substituição de referida cláusula ou condição por uma cláusula ou condição equivalente que seja válida, eficaz e exequível.<br/><br/>
<b>8.1.6.</b> Toda e qualquer tolerância quanto ao cumprimento, por qualquer das Partes, das condições estabelecidas no presente Contrato, não importará em alteração das disposições ora pactuadas, assim o não exercício de qualquer dos direitos que lhe assegurem este Contrato não constituirá causa de novação, sendo facultado à Parte afetada, a qualquer momento, exigir o cumprimento integral das condições pactuadas.
</p>
<br />
<br />
<b style="text-decoration: underline">9. DISPOSIÇÕES GERAIS</b>
<br />
<br />
<b>9.1.</b>Este Contrato tem validade temporária, com início da data de sua assinatura e término previsto imediatamente após a 
prestação de serviços e contas do Evento objeto deste Contrato.<br /><br />
<b>9.2.</b> Este Contrato e quaisquer direitos ou obrigações dele decorrentes não poderão ser cedidos ou, de qualquer forma, 
transferidos pela <b>Contratada</b>, sem o prévio consentimento por escrito dos <b>Contratantes</b>.<br/><br/>
<b>9.3.</b> Nenhuma das Partes será responsabilizada, nos termos deste Contrato, na ocorrência de caso fortuito ou força maior,
nos termos da legislação vigente. Neste caso, a parte não cumpridora das obrigações será dispensada do seu cumprimento ou 
observância por todo o período durante o qual os efeitos de tal evento permanecer.<br/><br/>
<b>9.4.</b> Fica eleito o Foro Central da Comarca da capital do Estado de São Paulo, para dirimir eventuais questões oriundas
da celebração, interpretação e execução do presente Contrato, com renúncia a qualquer outro, por mais privilegiado que seja, 
arcando a parte vencida com todo o ônus de sucumbência, inclusive honorários advocatícios de 20% (vinte por cento) do total da 
condenação.<br /><br />
</p>
<h5 style="text-align: center; color: black">São Paulo, <?=date('d')?> de <?=ucfirst($mes)?> de <?=date('Y')?></h5><br /><br />
<?php foreach($turma['Turma']['comissao'] as $comissao) : ?>
<p style="line-height: 1.3em">
<b>Nome: </b><?=key($comissao)?>
<br />
<b>RG: </b><?=$comissao[key($comissao)]?>
<br />
<br />
<?=str_pad("",60,"_")?>
<br />
ASSINATURA
<br />
</p>
<?php endforeach; ?>
