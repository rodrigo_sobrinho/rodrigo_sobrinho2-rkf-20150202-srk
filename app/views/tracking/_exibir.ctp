<?php
    $paginator->options(array(
        'url' => array($this->params['prefix'] => true)));
    $sortOptions = array('data-bind' => 'click: loadThis');
?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<style type="text/css">
#grafico-membros-ativos { position:relative; margin-top:-60px }
</style>
<script type="text/javascript" src="<?=$this->webroot?>metro/js/max/jscharts.js"></script>
<script type='text/javascript'>
    $(document).ready(function() {
        $('#tab-dados li a[data-toggle="tab"]').on('show', function (e) {
            atualizaAltura($($(e.target).attr('href')).height()+100);
        });
    })
    var myData = new Array();
    var colors = new Array();		
    var coresDeCadaBarra = ['#E8B4B1','#E8B4B1','#E8B4B1','#DAA9A6','#D59490','#E78883','#DF6862','#C36864','#BB433D','#A71009'];

    <?php

    function removerAcentosDaPalavra($palavra) {
        $acentos = array(
        'A' => '/&Agrave;|&Aacute;|&Acirc;|&Atilde;|&Auml;|&Aring;/',
        'a' => '/&agrave;|&aacute;|&acirc;|&atilde;|&auml;|&aring;/',
        'C' => '/&Ccedil;/',
        'c' => '/&ccedil;/',
        'E' => '/&Egrave;|&Eacute;|&Ecirc;|&Euml;/',
        'e' => '/&egrave;|&eacute;|&ecirc;|&euml;/',
        'I' => '/&Igrave;|&Iacute;|&Icirc;|&Iuml;/',
        'i' => '/&igrave;|&iacute;|&icirc;|&iuml;/',
        'N' => '/&Ntilde;/',
        'n' => '/&ntilde;/',
        'O' => '/&Ograve;|&Oacute;|&Ocirc;|&Otilde;|&Ouml;/',
        'o' => '/&ograve;|&oacute;|&ocirc;|&otilde;|&ouml;/',
        'U' => '/&Ugrave;|&Uacute;|&Ucirc;|&Uuml;/',
        'u' => '/&ugrave;|&uacute;|&ucirc;|&uuml;/',
        'Y' => '/&Yacute;/',
        'y' => '/&yacute;|&yuml;/',
        'a.' => '/&ordf;/',
        'o.' => '/&ordm;/');
        return preg_replace($acentos,array_keys($acentos),htmlentities($palavra));
    }

    foreach($estatisticasDosMembros as $membro) {
        $membro['Usuario']['nome'] = removerAcentosDaPalavra($membro['Usuario']['nome']);
        if (strlen($membro['Usuario']['nome']) >= 35) {
                $membro['Usuario']['nome'] = substr($membro['Usuario']['nome'], 0, 35)."...";
        }
        echo "\n adicionarUsuarioNoArrayDeDados(\"{$membro['Usuario']['nome']}\", {$membro['Usuario']['numero_acesso']});";
    }

    ?>

    var myChart = new JSChart('grafico-membros-ativos', 'bar');
    myChart.setDataArray(myData);
    myChart.colorizeBars(colors);
    myChart.setTitle('10 membros mais ativos');
    myChart.setTitleColor('#D31217');
    myChart.setAxisNameX('');
    myChart.setAxisNameY('');
    myChart.setAxisNameFontSize(14);
    myChart.setAxisNameColor('#000');
    myChart.setAxisValuesAngle(90);
    myChart.setAxisValuesColor('#000');
    myChart.setAxisColor('#aaa');
    myChart.setAxisWidth(1);
    myChart.setBarValuesColor('#555');
    myChart.setBarOpacity(0.5);
    myChart.setAxisPaddingTop(90);
    myChart.setAxisPaddingBottom(150);
    myChart.setAxisPaddingLeft(30);
    myChart.setTitleFontSize(11);
    myChart.setBarBorderWidth(0);
    myChart.setBarSpacingRatio(50);
    myChart.setBarOpacity(0.9);
    myChart.setFlagRadius(6);
    myChart.setSize(616, 450);
    myChart.draw();

    function adicionarUsuarioNoArrayDeDados(nomeUsuario, numeroAcessos) {
        var novoUsuario = [nomeUsuario, numeroAcessos];
        myData.push(novoUsuario);
        colors.push(coresDeCadaBarra.pop());
    }
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Traking da Comiss&atilde;o
        </h2>
    </div>
</div>
<br />
<div class='row-fluid'>
    <ul class="nav nav-tabs" id='tab-dados'>
        <li class="active">
            <a href="#formandos" data-toggle="tab">
                Membros da Comiss&atilde;o
            </a>
        </li>
        <li>
            <a href="#estatistica" data-toggle="tab">
                Estat&iacute;stica de Acesso
            </a>
        </li>
    </ul>
    <div class="tab-content" id="tab-content-dados">
        <div class="tab-pane active fade in" id="formandos">
            <?php if (sizeof($trackingFormandos) == 0) { ?>
                <h2 class="fg-color-red">Nenhum Membro da Comiss&atilde;o Cadastrado Para Esta Turma</h2>
            <?php } else { ?>
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th scope="col"><?=$paginator->sort('Nome', 'Usuario.nome',$sortOptions); ?></th>
                            <th scope="col"><?=$paginator->sort('E-mail', 'Usuario.email',$sortOptions); ?></th>
                            <th scope="col"><?=$paginator->sort('Ultimo Acesso','Usuario.ultimo_acesso',$sortOptions); ?></th>
                            <th scope="col"><?=$paginator->sort('Total de Acessos','Usuario.numero_acesso',$sortOptions); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($trackingFormandos as $trackingFormando): ?>
                        <tr>
                            <td><?=$trackingFormando['Usuario']['nome']?></td>
                            <td><?=$trackingFormando['Usuario']['email']; ?></td>
                            <td>
                                <?php if(!empty($trackingFormando['Usuario']['ultimo_acesso'])) : ?>
                                <?=date('d/m/Y',strtotime($trackingFormando['Usuario']['ultimo_acesso']))?>
                                <?php else : ?>
                                N&atilde;o Acessou
                                <?php endif; ?>
                            </td>
                            <td><?=$trackingFormando['Usuario']['numero_acesso']; ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="6">
                                <?=$paginator->numbers(array('separator' => ' ','data-bind' => 'click: loadThis')); ?>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            <?php } ?>
        </div>
        <div class="tab-pane fade" id="estatistica">
            <h4 class="fg-color-red">10 Membros Ativos</h4>
            <div id="grafico-membros-ativos">
                Carregando gr&aacute;fico...
            </div>
        </div>
    </div>
</div>