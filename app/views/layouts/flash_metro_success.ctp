<div class="alert alert-block alert-success fade in">
	<a class="close" data-dismiss="alert" href="#"></a>
	<h4 class="alert-heading">OK</h4>
	<p>
	<?php if(is_array($content_for_layout)) : foreach($content_for_layout as $message) : echo "$message<br />"; endforeach; ?>
	<?php  else : echo $content_for_layout; endif; ?>
	</p>
</div>