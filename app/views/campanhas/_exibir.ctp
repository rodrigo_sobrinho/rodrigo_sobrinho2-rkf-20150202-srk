<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/metroui/table.css">
<div class="row-fluid">
    <h2>
        <a class="metro-button back" data-bind="click: function() { 
            page('<?="/{$this->params['prefix']}/campanhas/listar"?>') }">
        </a>
        Visualizar Campanha
    </h2>
</div>
<div id="row-fluid">
<?php $session->flash(); ?>
    <div class="row-fluid">    
        <div class="span8">
                <strong>Nome</strong><br/>
                <?php echo $data['Campanha']['nome']; ?>
        </div>
    </div>
    <br/>
    <div class="row-fluid">
        <span class="span8">
                <strong>Descrição</strong><br/>
                <?php echo $data['Campanha']['descricao']; ?>
        </span>
    </div>
    <br/>
<!-- Não mostrar 'tipo de campanha' para o formando -->
<?php if($this->params['prefix'] != 'formando') { ?>
    <div class="row-fluid">
        <div class="span8">
                <strong>Tipo</strong><br/>
                <?php echo $data['Campanha']['tipo']; ?>
        </div>
    </div>
    <br/>
<?php } ?>
    <div class="row-fluid">
            <div class="span6">
                    <strong>Data Início</strong><br/>
                    <?php echo date('d/m/Y',strtotime($data['Campanha']['data_inicio'])); ?>
            </div>
            <div class="span6">
                    <strong>Data Fim</strong><br/>
                    <?php echo date('d/m/Y',strtotime($data['Campanha']['data_fim'])); ?>
            </div>
    </div>
    <br/>
    <div class="row-fluid">
            <div class="span6">
                    <strong>Máx. Parcelas</strong><br/>
                    <?php echo $data['Campanha']['max_parcelas']; ?>
            </div>
            <div class="span6">
                    <strong>Status</strong><br/>
                    <?php echo $data['Campanha']['status']; ?>
            </div>
    </div>
</div>
<br/>
<?php if(!($data['CampanhasExtra'])) { ?>
    <h2 class="fg-color-red">Nenhuma Extra cadastrado para essa campanha.</h2>
<?php } else { ?>
<div class="row-fluid">
    <div class="span8">
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Evento</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Qtde. Min.</th>
                    <th scope="col">Qtde. Máx.</th>
                    <th scope="col">Qtde. Dispon.</th>
                    <th scope="col">Valor</th>
                </tr>
            </thead>
            <tbody>
            <?php $isOdd = false; ?>
            <?php foreach($data['CampanhasExtra'] as $extra): ?>
                <?php if($isOdd):?>
                    <tr class="odd">
                <?php else:?>
                    <tr>
                <?php endif;?>
                    <td colspan="1"><?php echo $extra['Extra']['id'];?></td>
                    <td colspan="1"><?php echo $extra['Extra']['Evento']['nome'];?></td>
                    <td colspan="1"><?php echo $extra['Extra']['nome'];?></td>
                    <td colspan="1"><?php echo $extra['quantidade_minima'];?></td>
                    <td colspan="1"><?php echo $extra['quantidade_maxima']?></td>
                    <td colspan="1"><?php echo $extra['quantidade_disponivel']?></td>
                    <td colspan="1"><?php echo $extra['valor'];?></td>
                </tr>
                <?php $isOdd = !($isOdd); ?>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<?php } ?>
<br/>
<div class="row-fluid">
    <div class="span8">
        <?php if($this->params['prefix'] == 'planejamento'){ ?>
        <a class="button default bg-color-blueDark pull-right"
           data-bind="click: function() {
           page('<?= "/{$this->params['prefix']}/campanhas/editar/{$data['Campanha']['id']}" ?>') }">
            Alterar
        </a>                            
        <?php } ?>
    </div>
</div>

