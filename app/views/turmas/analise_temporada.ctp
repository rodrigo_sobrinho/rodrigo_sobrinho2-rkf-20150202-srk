<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/tooltip.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery.meio.mask.js"></script>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<style type="text/css">
    .bootstrap-select:not([class*="span"]):not([class*="col-"]):not([class*="form-control"]) { width: 120px }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        $.Input();
        $('.selectpicker').selectpicker(); 
        $('input[alt]').setMask();
        $("#form-filtro").submit(function(e){
            e.preventDefault();
            var context = ko.contextFor($(".metro-button.reload")[0]);
            context.$data.showLoading(function() {
                $("#form-filtro").find('[value="false"]').attr('disabled','disabled');
                var dados = $("#form-filtro").serialize();
                var url = $("#form-filtro").attr('action');
                $.ajax({
                    url : url,
                    data : dados,
                    type : "post",
                    dataType : "json",
                    complete : function() {
                        context.$data.reload();
                    }
                });
            });
        });
        $('.ficha').click(function(){
            var context = ko.contextFor($(".metro-button.reload")[0]);
            tid = $(this).attr('tid');
            context.$data.showLoading(function() {
                var url = "/turmas/ficha_evento/" + tid;
                $.ajax({
                    url : url,
                    complete : function() {
                        context.$data.page("/turmas/ficha_evento/");
                    }
                });
            });
            
        });
    });
</script>
<?php $session->flash(); ?>
<div class="row-fluid">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Análise Temporada
        </h2>
</div>
<?php 
$sortOptions = array('data-bind' => 'click: loadThis');
?>
<div class="row-fluid">
    <?=$form->create('Turma',array(
        'url' => "/turmas/analise_temporada/",
        'id' => 'form-filtro')) ?>
    <div class="row-fluid">
        <div class="span2">
            <?=$form->input('id',
                array('label' => 'ID', 'div' => 'input-control mini text',
                    'error' => false,'type' => 'text')); ?>
        </div>
        <div class="span2">
            <?=$form->input('ano_formatura',
                array('label' => 'Ano', 'div' => 'input-control mini text',
                    'alt' => "9999", 'error' => false,'type' => 'text')); ?>
        </div>
        <div class="span2">
            <?=$form->input('semestre_formatura',
                array('label' => 'Semestre', 'div' => 'input-control mini text', 'options' => $semestres,
                'error' => false,'type' => 'select', 'class' => 'selectpicker')); ?>
        </div>
        <div class="span6">
            <label>&nbsp;</label>
            <button type='submit' class='mini max bg-color-red'>
                Filtrar
                <i class='icon-search-2'></i>
            </button>
            <a class='button mini bg-color-blueDark pull-right' href='<?="/turmas/analise_temporada_excel"?>'
                target='_blank'>
                Gerar Excel
                <i class='icon-file-excel'></i>
            </a>
        </div>
    </div>
<?php if (sizeof($turmas > 0)) : ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="10%"><?=$paginator->sort('ID', 'Turma.id',$sortOptions); ?></th>
                <th scope="col" width="20%"><?=$paginator->sort('Nome', 'Turma.nome',$sortOptions); ?></th>
                <th scope="col" width="5%"><?=$paginator->sort('Expectativa Formandos', 'expectativa_formandos.nome',$sortOptions); ?></th>
                <th scope="col" width="5%"><?=$paginator->sort('Formandos', 'Formandos',$sortOptions); ?></th>
                <th scope="col" width="5%"><?=$paginator->sort('Aderidos', 'aderidos',$sortOptions); ?></th>
                <th scope="col" width="5%"><?=$paginator->sort('Checkout', 'checkout',$sortOptions); ?></th>
                <th scope="col" width="10%"><?=$paginator->sort('Data Assinatura', 'Turma.data_assinatura_contrato',$sortOptions); ?></th>
                <th scope="col" width="10%"><?=$paginator->sort('Ano/Sem Conclusao', 'ano',$sortOptions); ?></th>
                <th scope="col" width="30%">Eventos</th>
                <th scope="col" width="10%"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($turmas as $turma) : ?>
            <tr>
                <td width="5%" style="text-align: center"><?=$turma['Turma']['id']; ?></td>
                <td width="20%"><?=$turma['Turma']['nome']; ?></td>
                <td width="5%" style="text-align: center"><?=$turma['Turma']['expectativa_formandos']; ?></td>
                <td width="5%" style="text-align: center"><?=$turma[0]['formandos']; ?></td>
                <td width="5%" style="text-align: center"><?=$turma[0]['aderidos']; ?></td>
                <td width="5%" style="text-align: center"><?=$turma[0]['checkout']; ?></td>
                <td width="5%" style="text-align: center">
                    <?php if(!empty($turma['Turma']['data_assinatura_contrato'])) : ?>
                        <?=date('d/m/Y',strtotime($turma['Turma']['data_assinatura_contrato']))?>
                    <?php else : ?>
                        Indefinida
                    <?php endif; ?>
                </td>
                <td width="5%" style="text-align:center"><?="{$turma['Turma']['ano_formatura']}.{$turma['Turma']['semestre_formatura']}"; ?></td>
                <td width="30%"><?=$turma[0]['eventos'];?></td>
                <td width="15%">
                    <?php if(!empty($turma[0]['eventos'])) : ?>
                    <button type="button" class="default mini bg-color-greenDark ficha" tid="<?=$turma['Turma']['id']?>">
                        Ficha
                    </button>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="8" class="paginacao">
                    <?=$paginator->numbers(array('separator' => ' ','data-bind' => 'click: loadThis')); ?>
                </td>
                <td colspan="2">
                    <?=$paginator->counter(array('format' => 'Total : %count% ' .  'Turmas')); ?>
                </td>
            </tr>
        </tfoot>
    </table>
    <?= $form->end(array('label' => false,
        'div' => false, 'class' => 'hide')); ?>
<?php else : ?>
    <h2 class="fg-color-red">Nenhuma Turma Encontrada.</h2>
<?php endif; ?>
</div>