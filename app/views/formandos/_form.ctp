<style type="text/css">
#container-parcelas { line-height:33px; }
#container-parcelas input { padding:4px }
</style>
<script type="text/javascript">

    $('.data-inicio-pagamento').ready(function() {
        carregaParcelas();
    });

    var carregouParcelamentos = false;
    
    var itens = { parcelas: [], parcelamentos: 1, planoSelecionado: {} };

    function carregaParcelas() {
        if ($('.data-inicio-pagamento').val() != null &&
                $('.data-inicio-pagamento').val() != "") {
            itens.parcelas = [];
            $('.pagamento-parcelas').html('');
            $('.pagamento-parcelas').attr('disabled', true);
            var url = "<?= $html->url(array('prefix' => false,
                'controller' => 'formandos', 'action' => 'parcelas')); ?>";
            if ($('.pagamento-parcelamentos').val() != "" &&
                    $('.pagamento-parcelamentos').val() != undefined)
                url += "/" + $('#TurmaId').val() + "/" + $('.data-inicio-pagamento').val() + "/" + $('.pagamento-parcelamentos').val();
            else
                url += "/" + $('#TurmaId').val() + "/" + $('.data-inicio-pagamento').val();
            $.get(url, function(data) {
                dados = jQuery.parseJSON(data);
                if(dados.parcelamentos > 1 && !carregouParcelamentos) {
                    itens.parcelamentos = dados.parcelamentos;
                    $('.pagamento-parcelas').parent().hide();
                    titulos = dados.titulos;
                    planos = $('.pagamento-parcelamentos');
                    planos.parent().find(".carregando-planos").stop().html('Carregando Planos...');
                    planos.html('<option selected="selected">Selecione</option>');
                    jQuery.each(titulos, function() {
                        planos.append("<option value='" + this + "'>" + this + "</option>")
                    });
                    planos.parent().show();
                    planos.parent().find(".carregando-planos").stop().html('');
                    carregouParcelamentos = true;
                    $(".detalhar-parcelamento").hide();
                    carregaParcelas();
                } else {
                    itens.planos = dados.parcelas;
                    $('.input-parcelas').remove();
                    jQuery.each(itens.planos, function(i) {
                        selected = "";
                        if(this.Parcelamento.id == <?=(isset($this->data['FormandoProfile']['parcelas'])) ? $this->data['FormandoProfile']['parcelas'] : 0; ?>)
                            selected = " selected = 'selected' ";
                        $('.pagamento-parcelas').append("<option contrato='" + this.Parcelamento.valor +
                            "' dir=" + i + " value=" + this.Parcelamento.id +
                            selected + ">" + this.Parcelamento.texto + "</option>");
                    });
                }
            });
            $('.pagamento-parcelas').attr('disabled', false);
        } else {
            $('.pagamento-parcelas').html('');
            $('.pagamento-parcelas').attr('disabled', true);
        }
    }

    $('.data-inicio-pagamento').live('change', function() {
        carregouParcelamentos = false;
        carregaParcelas();
    });

    $('.pagamento-parcelamentos').live('change', function() {
        $('.pagamento-parcelas').parent().show();
        if($("#detalhar-parcelamento").attr('id') == 'detalhar-parcelamento')
            $(".detalhar-parcelamento").fadeIn(500);
        carregaParcelas();
    });
    
    $('.pagamento-parcelas').live('change', function() {
        if($(this).attr('dir') != "especial") {
            dir = $('.pagamento-parcelas > option:selected').attr('dir');
            itens.planoSelecionado = itens.planos[dir].Parcelamento;
            itens.parcelas = [];
            $('.input-parcelas').remove();
        }
    });
    
    $("#detalhar-parcelamento").live('click', function() {
        $.colorbox({
            width : 700,
            height: 600,
            html : exibirDetalheParcelamentos(),
            onComplete : function() { montaSelectParcelas(); },
            onClosed : function() { enviarParcelarPraView(); }
        });
    });
    
    $("#qtde-parcelas").live('change',function() {
        montaParcelas();
    });
    
    $('#container-parcelas > input').live('blur',function() {
        $(this).val(number_format(stringMoneyToNumber($(this).val()),2,',','.'));
        validarValorParcelas(this);
    })
    
    function exibirDetalheParcelamentos() {
        var div = $("<div>",{ class:'grid_6 first'});
        div.append("In&iacute;cio: ");
        div.append($("<select>",{
            class:'data-inicio-pagamento',
            html:$('.data-inicio-pagamento').html()
        }));
        if(itens.parcelamentos > 1) {
            div.append("<br /><br />Plano: ");
            div.append($("<select>",{
                class:'pagamento-parcelamentos',
                html:$('.pagamento-parcelamentos').html()
            }));
        }
        div.append("<br /><br />Parcelas: ");
        div.append($('<select>',{id:'qtde-parcelas',class:'pagamento-parcelas'}));
        var container = $("<div>",{
            class:'container_12',
            style:'min-width:100%!important; margin:0!important'
        }).append(div);
        $("<div>",{ class:'grid_6'})
            .append('O valor M&iacute;nimo das parcelas &eacute; de R$50,00<br /><br />')
            .append('O saldo precisa ser quitado nas 03 &uacute;ltimas parcelas<br /><br />')
            .append($("<div>",{ id:'container-parcelas'}))
            .appendTo(container);
        return container;
    }
    
    function montaSelectParcelas() {
        var option = $('.pagamento-parcelas > option:selected');
        var select = $('#qtde-parcelas');
        select.html('');
        $.each(itens.planos,function(i) {
            p = this.Parcelamento.parcelas;
            selected = option.attr('dir') == i ? ' selected = "selected"' : '';
            select.append("<option dir="+i+" value="+p+selected+'>'+p+
                '</option>');
        });
        montaParcelas();
    }
    
    function montaParcelas() {
        var option = $('#qtde-parcelas > option:selected');
        var div = $("#container-parcelas");
        var dataInicio = $('.data-inicio-pagamento').val().split('-');
        var data = new Date();
        data.setDate($("#FormandoProfileDiaVencimentoParcelas").val());
        data.setMonth(dataInicio[1]-1);
        data.setFullYear(dataInicio[0]);
        div.html('');
        var total = 0;
        if(itens.planoSelecionado.parcelas > 3)
            var parcelasNaoEditaveis = 3;
        else
            var parcelasNaoEditaveis = itens.planoSelecionado.parcelas;
        if(itens.parcelas.length < 1) {
            if(itens.planoSelecionado.valor == undefined)
                itens.planoSelecionado = itens.planos[option.attr('dir')].Parcelamento;
            for(var a = 1; a <= itens.planoSelecionado.parcelas; a++) {
                div.append((a < 10?"0":'')+a+"&ordf;   ");
                valorParcela = itens.planoSelecionado.valor/itens.planoSelecionado.parcelas;
                displayParcela = number_format(valorParcela,2,',','.');
                valorParcela = parseFloat(stringMoneyToNumber(displayParcela));
                if(a == itens.planoSelecionado.parcelas)
                    valorParcela = itens.planoSelecionado.valor - total;
                var parcela = { valorReal: valorParcela, valor: valorParcela };
                total+= valorParcela;
                if(itens.planoSelecionado.parcelas-a >= parcelasNaoEditaveis)
                    $("<input>",{
                        name:'data[detalheParcelas][]',
                        dir:itens.parcelas.length,
                        value:number_format(valorParcela,2,',','.')})
                        .appendTo(div);
                else
                    $("<input>",{
                        name:'data[detalheParcelas][]',
                        dir:itens.parcelas.length,
                        readonly:'readonly',
                        value:number_format(valorParcela,2,',','.')})
                        .appendTo(div);
                data.setMonth(data.getMonth()+1);
                div.append("&nbsp;"+data.getDate()+"/"+data.getMonth()+"/"+data.getFullYear());
                itens.parcelas.push(parcela);
                div.append('<br />');
            }
        } else {
            $.each(itens.parcelas,function(i) {
                div.append((i < 10?"0":'')+(i+1)+"&ordf;   ");
                if(itens.planoSelecionado.parcelas-i > parcelasNaoEditaveis)
                    $("<input>",{
                        name:'data[detalheParcelas][]',
                        dir:i,
                        value:number_format(this.valor,2,',','.')})
                        .appendTo(div);
                else
                    $("<input>",{
                        name:'data[detalheParcelas][]',
                        dir:i,
                        readonly:'readonly',
                        value:number_format(this.valor,2,',','.')})
                        .appendTo(div);
                data.setMonth(data.getMonth()+1);
                div.append("&nbsp;"+data.getDate()+"/"+data.getMonth()+"/"+data.getFullYear());
                div.append('<br />');
            });
        }
        div.append('Soma Parcelas: R$<span id="total-parcelas">'+
            number_format(itens.planoSelecionado.valor,2,',','.')+"</span>");
    }
    
    function number_format(number, decimals, dec_point, thousands_sep) {
        var n = number, prec = decimals;
        n = !isFinite(+n) ? 0 : +n;
        prec = !isFinite(+prec) ? 0 : Math.abs(prec);
        var sep = (typeof thousands_sep == "undefined") ? ',' : thousands_sep;
        var dec = (typeof dec_point == "undefined") ? '.' : dec_point;

        var s = (prec > 0) ? n.toFixed(prec) : Math.round(n).toFixed(prec);

        var abs = Math.abs(n).toFixed(prec);
        var _, i;

        if (abs >= 1000) {
            _ = abs.split(/\D/);
            i = _[0].length % 3 || 3;

            _[0] = s.slice(0,i + (n < 0)) +
                  _[0].slice(i).replace(/(\d{3})/g, sep+'$1');

            s = _.join(dec);
        } else {
            s = s.replace('.', dec);
        }

        return s;
    }
    
    function stringMoneyToNumber(stringMoney) {
        number = stringMoney.replace('R$','');
	number = number.replace('.','');
	number = number.replace(',','.');
	return number;
    }
    
    function validarValorParcelas(obj) {
        if(itens.planoSelecionado.parcelas > 1) {
            if(itens.planoSelecionado.parcelas > 3)
                var parcelasNaoEditaveis = 3;
            else
                var parcelasNaoEditaveis = itens.planoSelecionado.parcelas;
            var valorMinimo = 50;
            var total = 0;
            $('#container-parcelas > input').each(function(i,input) {
                valorInput = parseFloat(stringMoneyToNumber($(this).val()));
                if(valorInput < valorMinimo)
                    valorInput = valorMinimo;
                if(itens.planoSelecionado.parcelas-i <= parcelasNaoEditaveis) {
                    valorInput = parseFloat((itens.planoSelecionado.valor-total)/parcelasNaoEditaveis);
                    if(itens.planoSelecionado.parcelas-i == 0)
                        valorInput+= itens.planoSelecionado.valor-total;
                    parcelasNaoEditaveis--;
                }
                valorInput = parseFloat(valorInput.toFixed(2));
                total+= valorInput;
                itens.parcelas[$(this).attr('dir')].valor = valorInput;
                $(this).val(number_format(valorInput,2,',','.'));
            });
        } else if(obj != false) {
            $(obj).val(number_format(itens.planoSelecionado.valor,2,',','.'));
        }
    }
    
    function enviarParcelarPraView() {
        $.each(itens.parcelas,function(i) {
            $("<input>",{
                name:'data[parcelas][]',
                class: 'input-parcelas',
                type: 'hidden',
                value:number_format(this.valor,2,'.','')})
                .appendTo('#tabs-dados-turma');
        });
        $("<input>",{
            type:'hidden',
            value:itens.planoSelecionado.id,
            name:'data[parcelamento]'
        }).appendTo('#tabs-dados-turma');
        if($("#FormandoProfileParcelas option:first-child").attr('dir') != "especial")
            $("#FormandoProfileParcelas").prepend('<option selected="selected" dir="especial" value="'+itens.planoSelecionado.id+'">Condição Especial</option>');
        else
            $("#FormandoProfileParcelas option[dir='especial']").val(itens.planoSelecionado.id).attr('selected','selected');
    }

</script>

<script type="text/javascript">
    jQuery(function($) {
        $("#FormandoProfileCpf").setMask("999.999.999-99");
        $("#FormandoProfileTelResidencial").setMask("phone");
        $("#FormandoProfileTelComercial").setMask("phone");
        $("#FormandoProfileEndCep").setMask("99999-999");
        $("#FormandoProfileEndCep").setMask("99999-999");
        $("#FormandoProfileTelPai").setMask("phone");
        $("#FormandoProfileTelCelPai").setMask("(99) 99999-9999");
        $("#FormandoProfileTelMae").setMask("phone");
        $("#FormandoProfileTelCelMae").setMask("(99) 99999-9999");
        $("#FormandoProfileTelCelular").setMask("(99) 99999-9999");
        $("#FormandoProfileDataNascimento").setMask("99-99-9999");
        $("#upload-image").colorbox({iframe: true, width: "800", height: "500"});
        $("#formulario-adesao").submit(function(e) {
            if (!$("#termos-adesao").is(":checked")) {
                alert("Você deve aceitar os termos do contrato para prosseguir com a adesão");
                return false;
            } else {
                return true;
            }
        });
    });
</script>

<meta charset="utf-8">

<script>
    $(function() {
        $("#tabs").tabs();

        $(".botao-proximo").click(function() {
            indiceTabSelecionada = ($(".ui-state-active").get(0).id).replace("tab-", "");
            indiceDaProximaTab = parseInt(indiceTabSelecionada) + 1;
            nomeDaProximaTab = $("#tab-" + indiceDaProximaTab).children().get(0).rel;
            $("#tabs").tabs("select", nomeDaProximaTab);
            window.top.location.href = "#conteudo";
            return false;
        });

        $(".botao-anterior").click(function() {
            indiceTabSelecionada = ($(".ui-state-active").get(0).id).replace("tab-", "");
            indiceDaProximaTab = parseInt(indiceTabSelecionada) - 1;
            nomeDaProximaTab = $("#tab-" + indiceDaProximaTab).children().get(0).rel;
            $("#tabs").tabs("select", nomeDaProximaTab);
            window.top.location.href = "#conteudo";
            return false;
        });

        $(".form-error").each(function() {
            idTabPai = $(this).parents("div").get(0).id;
            $("a[rel=" + idTabPai + "]").parents("li").addClass("erro");
        });

        $("#FormandoProfileEndCep").blur(function() {
            var cep = $(this).val().replace("-", "");
            var url = "http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep=" + cep;
            $("#load-cep").html("<em>Aguarde...</em>");
            $.getScript(url, function() {
                if (resultadoCEP["resultado"] && resultadoCEP["tipo_logradouro"] != "") {
                    $("#load-cep").html("");
                    $("#FormandoProfileEndRua").val(unescape(resultadoCEP["tipo_logradouro"]) + " " + unescape(resultadoCEP["logradouro"]));
                    $("#FormandoProfileEndBairro").val(unescape(resultadoCEP["bairro"]));
                    $("#FormandoProfileEndCidade").val(unescape(resultadoCEP["cidade"]));
                    $('#FormandoProfileEndUf option[value="' + resultadoCEP["uf"] + '"]').attr("selected", "selected");
                    //$("#estado").val(unescape(resultadoCEP["uf"]));
                    $("#FormandoProfileEndNumero").focus();
                } else {
                    $("#load-cep").html("<em>Endereço não encontrado.</em>");
                }
            });
        });
    });
</script>
<?php $session->flash(); ?>
<?= $form->hidden('Turma.id', array('value' => $turma['Turma']['id'])); ?>

<div id="tabs" class="grid_full alpha omega">
    <ul>
        <li id="tab-0"><a href="#tabs-dados-pessoais" rel="tabs-dados-pessoais">Dados da turma</a></li>
        <li class="seta"></li>
        <li id="tab-1"><a href="#tabs-dados-turma" rel="tabs-dados-turma">Dados Pessoais</a></li>
        <li class="seta"></li>
        <li id="tab-2"><a href="#tabs-dados-residenciais" rel="tabs-dados-residenciais">Dados residenciais</a></li.>
        <li class="seta"></li>
        <li id="tab-3"><a href="#tabs-dados-formatura" rel="tabs-dados-formatura">Dados da formatura</a></li>
    </ul>
    <div id="tabs-dados-pessoais" class="grid_full alpha omega">
        <p class="grid_full alpha omega titulo">
            1. Dados da turma
        </p>
        <p class="grid_full alpha omega">
            Para continuar seu cadastro prossiga para o preenchimento dos seus dados pessoais.
        </p>
        <!-- Informações da turma e de quem está compondo a turma-->
        <p class="grid_full alpha omega">
            <label class="grid_3 alpha omega">ID da Turma</label>
            <span class="grid_10 alpha "> <?= $turma['Turma']['id'] ?> </span>
        </p>
        <p class="grid_full alpha omega">
            <label class="grid_3 alpha omega">Nome da Turma</label>
            <span class="grid_10 alpha "> <?= $turma['Turma']['nome'] ?> </span>
        </p>

        <p class="grid_full alpha omega">
            <label class="grid_3 alpha omega">Data de conclusão</label>
            <span class="grid_10 alpha "> <?= $turma['Turma']['ano_formatura'] ?> <?= $turma['Turma']['semestre_formatura'] ?>&ordm; Semestre</span>
        </p>

        <p class="grid_full alpha omega">
            <label class="grid_5 alpha omega">Cursos participantes</label>
            <?php
            $indiceDeCursos = 1;
            foreach ($info['universidades'] as $universidade) {
                foreach ($universidade['faculdades'] as $faculdade) {
                    foreach ($faculdade['cursos'] as $curso) {
                        ?>
                    <p class="grid_full"><?= $indiceDeCursos . " - " . $universidade['nome'] . " - " . $faculdade['nome'] . " - " . $curso; ?></p>
                    <?php
                }
            }
            $indiceDeCursos++;
        }
        ?>
        </p>
        <p class="grid_16">
            <input type="submit" class="submit botao-proximo" value="Próximo">
<?= $html->link('Voltar', array('controller' => 'cadastro'), array('class' => 'cancel')); ?>
        </p>
    </div>
    <div id="tabs-dados-turma" class="grid_full alpha omega">
        <p class="grid_full alpha omega titulo">
            2. Dados pessoais
        </p>
        <p class="grid_full alpha omega">
            Preencha seus dados pessoais e prossiga para preencher seus dados residenciais.
        </p>
        <p class="grid_11 alpha omega">
            <label class="grid_11 alpha omega">Foto</label>
        <p id="foto-formando" class="grid_11 alpha omega first"><img src="<?= $this->webroot . $this->data['FormandoProfile']['diretorio_foto_perfil']; ?>"></img></p>
<?= $form->hidden('FormandoProfile.diretorio_foto_perfil', array('id' => "diretorio-upload")); ?>
        <p class="grid_11 alpha omega first"><?= $html->link('Enviar foto', array('controller' => 'formandos', 'action' => 'uploadimg1'), array('id' => 'upload-image', 'class' => 'cancel grid_4 first alpha omega', 'style' => 'float:left; margin-left:0;')); ?>
        </p>
        </p>
        <p class="grid_11 alpha omega">
            <label class="grid_11 alpha omega">Nome</label>
<?= $form->input('nome', array('class' => 'grid_10 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
        <p class="grid_11 alpha omega first">
            <label class="grid_11 alpha omega">E-mail</label>
<?= $form->input('email', array('class' => 'grid_10 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
        <p class="grid_6 alpha first">
            <label class="grid_11 alpha omega">Senha</label>
<?= $form->password('senha', array('class' => 'grid_10 alpha omega first', 'value' => "", 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
        <p class="grid_6 omega">
            <label class="grid_11 alpha omega">Confirmar Senha</label>
<?= $form->password('confirmar', array('class' => 'grid_10 alpha omega first', 'value' => "", 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
        <p class="grid_6 alpha first">
            <label class="grid_11 alpha">Data de nascimento</label>
<?= $form->input('FormandoProfile.data_nascimento', array('class' => 'grid_10 alpha omega first',
    'label' => false, 'div' => false, 'type' => 'text', 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
        <p class="grid_6 omega">
            <label class="grid_11 alpha omega">Sexo</label>
<?= $form->select('FormandoProfile.sexo', array('M' => 'Masculino', 'F' => 'Feminino'), null, array('empty' => false, 'class' => 'grid_10 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
        <p class="grid_6 alpha first">
            <label class="grid_11 alpha omega">RG</label>
<?= $form->input('FormandoProfile.rg', array('class' => 'grid_10 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
        <p class="grid_6 omega">
            <label class="grid_16 alpha omega">CPF</label>
<?= $form->input('FormandoProfile.cpf', array('class' => 'grid_10 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
        <p class="grid_4 alpha first">
            <label class="grid_16 alpha omega">Telefone Residencial</label>
<?= $form->input('FormandoProfile.tel_residencial', array('class' => 'grid_12 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
        <p class="grid_4">
            <label class="grid_16 alpha omega">Telefone Comercial</label>
<?= $form->input('FormandoProfile.tel_comercial', array('class' => 'grid_12 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
        <p class="grid_4">
            <label class="grid_4 alpha omega">Celular</label>
<?= $form->input('FormandoProfile.tel_celular', array('class' => 'grid_12 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
        <p class="grid_4 omega">
            <label class="grid_11 alpha omega">Nextel</label>
<?= $form->input('FormandoProfile.nextel', array('class' => 'grid_12 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
        <p class="grid_8 alpha first">
            <label class="grid_full alpha omega">Nome do Pai</label>
<?= $form->input('FormandoProfile.nome_pai', array('class' => 'grid_full alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
        <p class="grid_4 omega">
            <label class="grid_8 alpha omega">Telefone Pai</label>
<?= $form->input('FormandoProfile.tel_pai', array('class' => 'grid_12 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
        <p class="grid_4 omega">
            <label class="grid_8 alpha omega">Celular Pai</label>
<?= $form->input('FormandoProfile.tel_cel_pai', array('class' => 'grid_12 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
        <p class="grid_8 alpha first">
            <label class="grid_ful alpha omega">Nome da Mãe</label>
<?= $form->input('FormandoProfile.nome_mae', array('class' => 'grid_full alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
        <p class="grid_4 omega">
            <label class="grid_8 alpha omega">Telefone Mãe</label>
<?= $form->input('FormandoProfile.tel_mae', array('class' => 'grid_12 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
        <p class="grid_4 omega">
            <label class="grid_8 alpha omega">Celular Mãe</label>
<?= $form->input('FormandoProfile.tel_cel_mae', array('class' => 'grid_12 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
        <p class="grid_16">
            <input type="submit" class="submit botao-proximo" value="Próximo">
            <a href="#header" class="cancel botao-anterior">Anterior</a>
        </p>
    </div>
    <div id="tabs-dados-residenciais" class="grid_full alpha omega">
        <p class="grid_full alpha omega titulo">
            3. Dados residenciais
        </p>
        <p class="grid_full alpha omega">
            Preencha seus dados residenciais e prossiga para preencher seus dados da formatura.
        </p>
        <p class="grid_5 omega" style="margin-right:300px; margin-left:0">
            <label class="grid_11 alpha omega" style="width:20%!important">CEP</label><span id='load-cep'></span>
<?= $form->input('FormandoProfile.end_cep', array('class' => 'grid_full alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
        <p class="grid_10 alpha ">
            <label class="grid_11 alpha omega">Endereço</label>
<?= $form->input('FormandoProfile.end_rua', array('class' => 'grid_full alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
        <p class="grid_2 alpha first">
            <label class="grid_11 alpha omega">Número</label>
<?= $form->input('FormandoProfile.end_numero', array('class' => 'grid_full alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
        <p class="grid_5">
            <label class="grid_11 alpha omega">Complemento</label>
<?= $form->input('FormandoProfile.end_complemento', array('class' => 'grid_full alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
        <p class="grid_6 omega">
            <label class="grid_11 alpha omega">Bairro</label>
<?= $form->input('FormandoProfile.end_bairro', array('class' => 'grid_full alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
        <p class="grid_5 alpha first">
            <label class="grid_11 alpha omega">Cidade</label>
<?= $form->input('FormandoProfile.end_cidade', array('class' => 'grid_full alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
        <p class="grid_5 omega">
            <label class="grid_11 alpha omega">Estado</label>
            <?php
            $uf_brasil = array(
                'AC' => 'Acre',
                'AL' => 'Alagoas',
                'AP' => 'Amapá',
                'AM' => 'Amazonas',
                'BA' => 'Bahia',
                'CE' => 'Ceará',
                'DF' => 'Distrito Federal',
                'ES' => 'Espírito Santo',
                'GO' => 'Goiás',
                'MA' => 'Maranhão',
                'MT' => 'Mato Grosso',
                'MS' => 'Mato Grosso do Sul',
                'MG' => 'Minas Gerais',
                'PA' => 'Pará',
                'PB' => 'Paraíba',
                'PR' => 'Paraná',
                'PE' => 'Pernambuco',
                'PI' => 'Piauí',
                'RJ' => 'Rio de Janeiro',
                'RN' => 'Rio Grande do Norte',
                'RS' => 'Rio Grande do Sul',
                'RO' => 'Rondonia',
                'RR' => 'Roraima',
                'SC' => 'Santa Catarina',
                'SP' => 'São Paulo',
                'SE' => 'Sergipe',
                'TO' => 'Tocantins');
            echo $form->select('FormandoProfile.end_uf', $uf_brasil, 'SP', array('empty' => false, 'class' => 'grid_full first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10')));
            ?>
        </p>
        <p class="grid_16">
            <input type="submit" class="submit botao-proximo" value="Próximo">
            <a href="#header" class="cancel botao-anterior">Anterior</a>
        </p>
    </div>
    <div id="tabs-dados-formatura" class="grid_full alpha omega">
        <p class="grid_full alpha omega titulo">
            4. Dados da formatura
        </p>
        <p class="grid_full alpha omega">
            Preencha seus dados da formatura e finalize seu cadastro.
        </p>
        <p class="grid_full alpha omega">
            <label class="grid_full alpha omega">Curso</label>
            <?= $form->select('FormandoProfile.curso_turma_id', $cursoTurma, null, array('empty' => false, 'class' => 'grid_14 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
        <p class="grid_4 alpha first">
            <label class="grid_11 alpha omega">Sala</label>
<?= $form->input('FormandoProfile.sala', array('class' => 'grid_10 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>

        <p class="grid_6 alpha first">
            <label class="grid_11 alpha omega">Número Calçado</label>
            <?php
            $numero_calcado;
            for ($i = 33; $i <= 43; $i+=2) {
                $j = $i + 1;
                $numero_calcado[$i] = "$i / $j";
            }
            echo $form->select('FormandoProfile.numero_havaiana', $numero_calcado, null, array('empty' => false, 'class' => 'grid_4 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10')));
            ?>
        </p>

        <p class="grid_6 omega">
            <label class="grid_11 alpha omega">Tamanho Camiseta</label>
        <?php $tamanho_camiseta = array('PP' => 'PP', 'P' => 'P', 'M' => 'M', 'G' => 'G', 'GG' => 'GG');
        echo $form->select('FormandoProfile.tam_camiseta', $tamanho_camiseta, null, array('empty' => false, 'class' => 'grid_3 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10')));
        ?>
        </p>

        <?php if (!$comissao) include('_form_pagamento.ctp'); ?>
        <?php //echo $form->hidden('pertence_comissao'); ?>
        <input name="data[FormandoProfile][pertence_comissao]" value="<?= $comissao; ?>" id="FormandoProfilePertenceComissao" type="hidden">
        <?php
        /*
          <p class="grid_11 alpha omega">
          <label class="grid_11 alpha omega">Convites Extras</label>
          <?=$form->select('FormandoProfile.convites_extras', $select_convites_extras, array('empty'=>false), array('class' => 'grid_10 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
          </p>
          <p class="grid_11 alpha omega">
          <label class="grid_11 alpha omega">Mesas Extras</label>
          <?=$form->select('FormandoProfile.mesas_extras', $select_mesas_extras, array('empty'=>false), array('class' => 'grid_10 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
         */
        ?>
        </p>



        <p class="grid_16">
            <input type="submit" class="submit" value="Finalizar">
            <a href="#header" class="cancel botao-anterior">Voltar</a>
        </p>
    </div>
</div>
