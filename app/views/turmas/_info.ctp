<?php if($permissao) : ?>
<script type="text/javascript">
    $(document).ready(function() {
        var button = $("<button>",{
            type: 'button',
            class: 'button bg-color-blue',
            id:'editar-turma',
            text:'Editar'
        });
        $('.modal-footer').prepend(button);
        $("#editar-turma").click(function(e) {
            e.preventDefault();
            var context = ko.contextFor($("#content-body")[0]);
            context.$data.page('/<?=$this->params['prefix']?>/turmas/editar_dados');
            bootbox.hideAll();
        })
    })
</script>
<?php endif; ?>
<h2 class='fg-color-red'>
    <?=$turma['Turma']['nome']?> <?=$turma['Turma']['id']?>
    <small>
        - <?=date('d/m/Y',strtotime($turma['Turma']['data_abertura_turma']));?>
    </small>
    <span class='label label-important pull-right'>
        <?=ucfirst($turma['Turma']['status'])?>
    </span>
</h2>
<br />
<div class="row-fluid">
    <div class="span12">
        <h4>Cursos</h4>
        <?php
        foreach($turma['CursoTurma'] as $cursoturma) :
            $universidade = $cursoturma['Curso']['Faculdade']['Universidade']['sigla'];
            if($universidade == '')
                $universidade = $cursoturma['Curso']['Faculdade']['Universidade']['nome'];
            $faculdade = $cursoturma['Curso']['Faculdade']['sigla'];
            if($faculdade == '')
                $faculdade = $cursoturma['Curso']['Faculdade']['nome'];
            $cursoTurno = $cursoturma['Curso']['nome'];
            if($cursoturma['turno'] != '')
                $cursoTurno.= " - {$cursoturma['turno']}";
            echo "<h5>{$universidade} - {$faculdade} - {$cursoTurno}</h5>";
        endforeach;
        ?>
    </div>
</div>
<br />
<div class='row-fluid'>
    <div class='span5'>
        <h4>Formatura</h4>
        <h5>
            <?=$turma['Turma']['ano_formatura']?> - 
            <?=$turma['Turma']['semestre_formatura']?> &ordm; Semestre
        </h5>
    </div>
    <div class='span5'>
        <h4>Expectativa Formandos</h4>
        <h5>
            <?=$turma['Turma']['expectativa_formandos']?>
        </h5>
    </div>
</div>
<br />
<div class='row-fluid'>
    <div class='span5'>
        <h4>Comissão</h4>
        <h5>
            <?php if($turma['Turma']['pessoas_comissao'] == 0) : ?>
            Nenhuma Pessoa na Comissão
            <?php else : ?>
            <?=$turma['Turma']['pessoas_comissao']?> Pessoas na Comissão
            <?php endif; ?>
        </h5>
    </div>
    <div class='span5'>
        <h4>Como Chegou</h4>
        <h5><?=$turma['Turma']['como_chegou'];?></h5>
    </div>
</div>
<br />
<div class='row-fluid'>
    <div class='span5'>
        <h4>Detalhes de Como Chegou</h4>
        <h5>
            <?php if($turma['Turma']['como_chegou_detalhes'] == '') : ?>
            Nenhum Detalhe Adicionado
            <?php else : ?>
            <?=$turma['Turma']['como_chegou_detalhes']?>
            <?php endif; ?>
        </h5>
    </div>
        <div class='span5'>
        <h4>Data Correção do IGPM</h4>
        <h5>			
        <?php 
            if ($turma['Turma']['data_igpm'] != "0000-00-00")
                    echo date('d/m/Y',strtotime($turma['Turma']['data_igpm'])); 
            else
                    echo "-";
        ?>
        </h5>
        </div>
</div>
<br />
<div class='row-fluid'>
    <div class='span5'>
        <h4>Pretensão</h4>
        <h5><?=ucfirst($turma['Turma']['pretensao'])?></h5>
    </div>
        <div class='span5'>
            <h4>Data Assinatura do Contrato</h4>
            <h5><?=ucfirst($turma['Turma']['data_assinatura_contrato'])?></h5>
        </div>
</div>
<br />
<div class='row-fluid'>
    <div class='span5'>
        <h4>Atendidos por:</h4>
        <h5><?php foreach($turma['Usuario'] as $u):
                echo $u['nome'] . ' - ' . $u['email'];
                echo '<br/>';
            endforeach;?></h5>
    </div>
</div>
<br/>
<div class='row-fluid'>
    <div class='span5'>
        <h4>Convites Inclusos na Adesão</h4>
        <h5><?php echo $turma['Turma']['convites_contrato']; ?></h5>
    </div>
    <div class='span5'>
        <h4>Mesas Inclusas na Adesão</h4>
        <h5><?php echo $turma['Turma']['mesas_contrato']; ?></h5>
    </div>
</div>
