<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<h2><?php echo $turmaMapa['Evento']['nome'] ?> - Setor <span class="fg-color-red"><?php echo $setor ?></span></h2>

<?php if (count($formandos_sem_setor) > 0) : ?>
    <br/>
    <h3>Adicionar Formandos</h3>
    <?php echo $form->create('FormandoMapaHorario',array(
        'url' => "/{$this->params['prefix']}/mapa_mesas/setorizacao_adicionar_formandos/{$evento_id}/".urlencode($setor),
        'id' => 'form-filtro'
    )) ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th style="width:20px;text-align:center;" scope="col"></th>
                <th class="input-small" scope="col">Código Formando</th>
                <th scope="col">Nome</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($formandos_sem_setor as $formando) : ?>
            <tr>
                <td><?php echo $form->input($formando['FormandoMapaHorario']['codigo_formando'],array('type' => 'checkbox','label' => false)) ?></td>
                <td><?=$formando['FormandoMapaHorario']['codigo_formando']; ?></td>
                <td><?=$formando['VwCodigoFormando']['nome']; ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>