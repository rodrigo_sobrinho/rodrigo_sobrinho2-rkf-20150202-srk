<style type="text/css">
#configuracao { display:none; font-size: 16px }
#configuracao .opcoes button i { font-size:14px!important;
    line-height:1px; vertical-align:middle; }
#mapa { position:relative; border:solid 2px #525252;
    background-image: url("/<?=$mapa['EventoMapa']['arquivo']?>");
    background-repeat:no-repeat;
    background-size: 100% auto
}
#mapa .divisoria { position:absolute; display:block; cursor:pointer }
#mapa .linha { width:100%; height:2px }
#mapa .coluna { width:2px; height:100% }
</style>
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#imagem-mapa").load(function() {
            var largura = $(this).width();
            var altura = $(this).height();
            var cor = "#525252";
            $("#mapa").width(largura).height(altura);
            $("#configuracao").fadeIn(500);
            
            $(".adicionar-linha").click(function() {
                if($(".divisoria.linha.parada").length == 0)
                    adicionarDivisoria('linha');
            });
            
            $(".adicionar-coluna").click(function() {
                if($(".divisoria.coluna.parada").length == 0)
                    adicionarDivisoria('coluna');
            });
            
            $(".alterar-cor").click(function() {
                cor = $(this).css('background-color');
                $("#mapa").css('border-color',cor);
                $(".divisoria").css('background-color',cor);
            });
            
            $("#confirmar-configuracao").click(function() {
                var linhas = $(".divisoria.linha:not(.parada)");
                var colunas = $(".divisoria.coluna:not(.parada)");
                if(linhas.length > 0 && colunas.length > 0) {
                    var configuracao = { linhas: [altura], colunas: [largura] };
                    linhas.each(function() {
                        configuracao.linhas.push(parseInt($(this).css('top')));
                    });
                    colunas.each(function() {
                        configuracao.colunas.push(parseInt($(this).css('left')));
                    });
                    configuracao.linhas.sort(function(a,b){return a-b});
                    configuracao.colunas.sort(function(a,b){return a-b});
                    var medidas = { linhas: [], colunas: [] };
                    for(var i in configuracao.linhas) {
                        var medida = configuracao.linhas[i];
                        if(i != 0)
                            medida-= configuracao.linhas[(i-1)];
                        medidas.linhas.push(medida);
                    }
                    for(var i in configuracao.colunas) {
                        var medida = configuracao.colunas[i];
                        if(i != 0)
                            medida-= (configuracao.colunas[(i-1)]+3);
                        medidas.colunas.push(medida);
                    }
                    medidas.colunas[medidas.colunas.length-1]+= 2;
                    var context = ko.contextFor($(".metro-button.reload")[0]);
                    context.$data.showLoading(function() {
                        var dados = $("#formulario").serialize();
                        var url = $("#formulario").attr('action');
                        $.ajax({
                            url : '/<?=$this->params['prefix']?>/eventos/mapa_configurar',
                            data : {
                                data : {
                                    medidas : medidas,
                                    id : '<?=$mapa['EventoMapa']['id']?>'
                                }
                            },
                            type : "POST",
                            dataType : "json",
                            success : function(response) {
                                if(response.error == 1)
                                    context.$data.reload();
                                else
                                    context.$data.page('/<?=$this->params['prefix']?>/eventos/mapa_alocar/<?=$mapa['EventoMapa']['id']?>');
                            },
                            error: function() {
                                context.$data.reload();
                            }
                        });
                    });
                } else {
                    alert('Voce deve inserir ao menos uma linha e uma coluna');
                }
            });
            
            function adicionarDivisoria(tipo) {
                var pos = 'left';
                var axis = "x";
                if(tipo == 'linha') {
                    pos = 'top';
                    axis = "y";
                }
                var divisoria = $("<div>",{
                    class: 'divisoria parada ' + tipo,
                    style: pos+':20px;background-color:'+cor
                }).appendTo($("#mapa"));
                divisoria.draggable({
                    containment: "parent",
                    axis: axis,
                    stop: function(e,u) {
                        if(u.position[pos] == 0)
                            divisoria.remove();
                        else if(u.helper.hasClass('parada'))
                            u.helper.removeClass('parada');
                    }
                });
            }
            
        });
    });
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: reload"></a>
            Configura&ccedil;&atilde;o do Mapa
        </h2>
    </div>
</div>
<?php $session->flash(); ?>
<?php if($mapa) : ?>
<img src="/<?=$mapa['EventoMapa']['arquivo']?>" id="imagem-mapa" class="hide" />
<h3>
    Para remover uma linha carregue para a borda do topo
    <br />
    Para remover uma coluna carregue para a borda da esquerda
</h3>
<br />
<div class="row-fluid" id="configuracao">
    <button type="button" class="button mini bg-color-blueDark"
        id="confirmar-configuracao">
        Confirmar configura&ccedil;&atilde;o
    </button>
    <div class="row-fluid opcoes">
        <button type="button"
            class="button mini default adicionar-linha">
            Adicionar Linha
            <i class="icon-list"></i>
        </button>
        <button type="button"
            class="button mini default adicionar-coluna">
            Adicionar Coluna
            <i class="icon-grid"></i>
        </button>
        <span style="margin-left:20px">Cor das Linhas</span>
        <button type="button"
            class="button mini bg-color-grayDark alterar-cor">
            &nbsp;
        </button>
        <button type="button"
            class="button mini bg-color-blue alterar-cor">
            &nbsp;
        </button>
        <button type="button"
            class="button mini bg-color-red alterar-cor">
            &nbsp;
        </button>
        <button type="button"
            class="button mini bg-color-green alterar-cor">
            &nbsp;
        </button>
        <button type="button"
            class="button mini bg-color-yellow alterar-cor">
            &nbsp;
        </button>
    </div>
    <div id="mapa">
        
    </div>
</div>
<?php else : ?>
<h2 class="fg-color-red">Mapa n&atilde;o encontrado</h2>
<?php endif; ?>
