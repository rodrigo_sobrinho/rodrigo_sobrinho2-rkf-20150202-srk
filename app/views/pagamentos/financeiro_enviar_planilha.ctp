<span id="conteudo-titulo" class="box-com-titulo-header">Recebimento de Pagamentos</span>
<div id="conteudo-container">
    <?php $session->flash(); ?>
    <?=$form->create('UploadPagamento',
            array('url' => "/{$this->params['prefix']}/pagamentos/iniciar_upload",
                    'type' => 'file')); ?>

    <p class="grid_11 alpha omega">
        <label class="grid_11 alpha">Arquivo xls com lista de boletos:</label>
        <?=$form->file('arquivo',
            array('class' => 'grid_11 first alpha omega', 'label' => false,
            'div' => false, 'error' => array('wrap' => 'span')))?>
    </p>


    <p class="grid_11 alpha omega">
        <span class="grid_11 alpha omega">
            <?=$form->end(array('label' => 'Enviar', 'div' => false, 'class' => 'submit'));?>
        </span>
    </p>
    		<span class="grid_11 alpha omega">
			<?php echo $html->link('Gerar excel',array($this->params['prefix'] => true, 'action' => 'gerar_excel', 'controller' => 'formandos'), array('id' => 'link-gerar-excel', 'style' => 'font-size: 16px;', 'target' => '_blank')); ?>
		</span>
</div>
