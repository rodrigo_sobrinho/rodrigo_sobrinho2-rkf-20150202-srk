<script type="text/javascript">
    $(document).ready(function() {
        if($('[data-segundos]').length > 0) {
            var s = $('[data-segundos]').data('segundos');
            $('[data-segundos]').text("em " + s);
            $(".rodape-opcoes").remove();
            var t = setInterval(function() {
                s--;
                if(s == 0) {
                    clearInterval(t);
                    $('[data-segundos]').html("agora");
                } else {
                    $('[data-segundos]').html("em " + s);
                }
            },1000);
        }
        $("#exibir-login").click(function(e) {
            e.preventDefault();
            $(".resposta-facebook.finalizado").fadeOut(500,function() {
                $(".resposta-facebook.aguardando").fadeIn(500,function() {
                    FB.logout(function(response) {
                        bootbox.hideAll();
                    });
                });
            });
        });
        
        $(".container-usuario").click(function() {
            var usuario = $(this).data('usuario');
            var facebook = <?=json_encode($facebook)?>;
            $(".resposta-facebook.aguardando").text('Aguarde enquanto configuramos sua conta');
            $(".resposta-facebook.finalizado").fadeOut(500,function() {
                $(".resposta-facebook.aguardando").fadeIn(500,function() {
                    $.ajax({
                        type : 'POST',
                        url : '/usuarios/conectar_facebook',
                        data : {
                            data : {
                                usuario : usuario,
                                facebook : facebook
                            }
                        },
                        dataType : 'json',
                        success : function(response) {
                            if(response.erro) {
                                var texto = "<br />";
                                if(response.mensagem.length > 0)
                                    texto+= response.mensagem.join("<br />");
                                else
                                    texto+= "Dados n&atilde;o enviados. Tente novamente mais tarde";
                                $(".resposta-facebook.aguardando").append(texto);
                            } else {
                                window.location.href = "/";
                            }
                        },
                        error : function() {
                            var texto = "<br />Erro ao conectar servidor. Tente novamente mais tarde";
                            $(".resposta-facebook.aguardando").append(texto);
                        }
                    });
                });
            });
        });
    });
</script>
<style type="text/css">
    .rodape-opcoes {
        height:40px;
        padding-top: 20px;
    }
    .rodape-opcoes a { margin-bottom: 0; }
    .respostad-facebook {
        float:left;
        margin-bottom:50px;
        max-height: 350px;
        overflow: auto;
    }
    .container-usuario {
        text-align: center;
        cursor:pointer;
    }
    .container-usuario:hover .foto-facebook img {
        border:solid 2px red;
    }
    .container-usuario:hover .nome-formando {
        color:red;
    }
    .foto-facebook-old {
        max-height:150px;
        overflow: hidden;
    }
    .img-center {
        width:80%;
        height:auto;
        margin: 0 auto;
        border:solid 2px transparent;
        transition: all .2s ease;
    }
    .nome-formando {
        display:block;
        margin: 5px 0;
        transition: all .2s ease;
    }
</style>
<h3 class="resposta-facebook aguardando fg-color-red hide">
    Aguarde
</h3>
<div class="row-fluid resposta-facebook finalizado">
    <?php if($conta) : ?>
    <?php if($logado) : ?>
    <meta http-equiv="refresh" content="3; URL=/">
    <h3 class="fg-color-red">
        Voc&ecirc; ser&aacute; logado <span data-segundos="3"></span>
    </h3>
    <?php else : ?>
    <h3 class="fg-color-red">
        Erro ao completar login
        <br />
        Tente novamente
    </h3>
    <?php endif; ?>
    <?php elseif($usuario) : ?>
    <h2 class="fg-color-red">
        Clique em seu nome para logar com o Facebook
    </h2>
    <br />
    <div class="row-fluid">
        <div class="container-usuario span4 offset4"
            data-usuario="<?=$usuario['Usuario']['id']?>">
            <div class="foto-facebook">
                <?php if(empty($usuario['Usuario']['diretorio_foto_perfil'])) : ?>
                <img src="https://graph.facebook.com/<?=$facebook['id']?>/picture?width=400&height=400"
                    onerror="this.src='/img/no-image.gif'"
                    class="img-circle img-center" />
                <?php else : ?>
                <img src="<?=URL_IMG.$usuario['Usuario']['diretorio_foto_perfil']?>"
                    onerror="this.src='/img/no-image.gif'"
                    class="img-circle img-center" />
                <?php endif; ?>
            </div>
            <h3 class='nome-formando'>
                <?=$usuario['Usuario']['nome']?>
            </h3>
        </div>
    </div>
    <?php else : ?>
    <h3 class="fg-color-red">
        Nenhum formando foi encontrado
        <br />
        Fa&ccedil;a login com seus dados ou cadastre-se para usar o Facebook
    </h3>
    <?php endif; ?>
</div>
<div class="rodape-opcoes">
    <a href="#" class="button bg-color-blue" id="exibir-login">
        Login
    </a>
    <a href="/cadastro" class="button bg-color-green">
        Cadastre-se
    </a>
</div>