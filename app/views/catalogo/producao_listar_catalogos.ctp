<h2>
    <a class="metro-button back" data-bind="click: loadThis"
        href="/<?=$this->params['prefix']?>/catalogo/listar/<?=$evento["TiposEvento"]['id']?>"></a>
    <a class="metro-button reload" data-bind="click: reload"></a>
    <?=$evento["TiposEvento"]['nome']?> - <?=$item['CatalogoItem']['nome']; ?>
    <a href="/<?=$this->params['prefix']?>/catalogo/editar/<?=$evento["TiposEvento"]['id']?>/<?=$item['CatalogoItem']['id']; ?>" data-bind="click: loadThis"
        class="button mini default pull-right">
        Novo Catálogo
    </a>
</h2>
<div class="row-fluid">
    <?php if (sizeof($catalogos) > 0) : ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th>Evento</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($catalogos as $catalogo) : ?>
            <tr>
                <td><?=$catalogo['Catalogo']['nome']?></td>
                <td>
                    <button class="button mini default" type="button"
                        data-bind="click: loadThis"
                        href="/<?=$this->params['prefix']?>/catalogo/editar/<?=$evento["TiposEvento"]['id']?>/<?=$item['CatalogoItem']['id']; ?>/<?=$catalogo['Catalogo']['id']; ?>">
                        Editar
                    </button>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php else : ?>
    <h2 class="fg-color-red">Nenhum cat&aacute;logo cadastrado</h2>
    <?php endif; ?>
</div>