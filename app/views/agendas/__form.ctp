<script type="text/javascript">
    $(function() {
        $( "#datepicker" ).datetimepicker( $.datepicker.regional[ "pt-BR" ] );
    });
</script>

<?php echo $form->input('id', array('hiddenField' => true)); ?>

<p class="grid_11 alpha omega">
    <label class="grid_5 alpha">Título</label>
    <?php echo $form->input('titulo', array('class' => 'grid_6 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>

<p class="grid_11 alpha omega">
    <label class="grid_5 alpha">Descricao</label>
    <?php echo $form->input('descricao', array('class' => 'grid_6 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_11 alpha omega">
    <label class="grid_5 alpha">Local</label>
    <?php echo $form->input('local', array('class' => 'grid_6 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_11 alpha omega">
    <label class="grid_5 alpha">Data/Horário</label>
    <?php echo $form->input('data-hora', array('class' => 'grid_6 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'), 'id' => 'datepicker')); ?>
</p>
<div class="grid_11 alpha omega compartilhar">
    <label class="grid_5 alpha">Compartilhamento Turma</label>
    <?php 
    foreach($turmas as $k => $v) {
        $turma[$k] = $k . ' - ' . $v;   
    }
    echo $form->input('Turma.id', array(
        'options' => $turma,
        'class' => 'grid_6 first alpha omega checkbox',
        'label' => false,
        'div' => false,
        'error' => array('wrap' => 'span', 'class' => 'grid_10'),
        'before' => '<div class="clear"></div>',
        'after' => '<div class="clear"></div>'));
    ?>

</div>

<div class="grid_11 alpha omega compartilhar">
    <label class="grid_11 alpha">Compartilhamento Interno</label>

    <?php
    $grupo_usuario = ucfirst($usuario['Usuario']['grupo']);
    $grupos = array('Comercial', 'Planejamento', 'Atendimento', 'Super');

    foreach ($grupos as $grupo) :
        $texto = "visivel" . $grupo;
        if ($grupo != $grupo_usuario) {
            ?>
            <div class="grid_11 alpha omega checkbox">
                <?php echo $form->input($texto, array('label' => $grupo, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
            </div>
            <?php
        } else {
            ?>
            <div class="grid_11 alpha omega checkbox">
                <?php echo $form->input($texto, array('label' => $grupo, 'checked' => true, 'disabled' => true, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
            </div>
            <?php
        }

    endforeach

    /*
      echo $form->input('Compartilhado', array(
      'multiple' => 'checkbox',
      'options' => $usuarios,
      'class' => 'grid_6 first alpha omega checkbox',
      'label' => false,
      'div' => false,
      'error' => array('wrap' => 'span', 'class' => 'grid_10'),
      'before' => '<div class="clear"></div>',
      'after' => '<div class="clear"></div>'));
     */
    ?>

</div>
<?php echo $form->hidden('Criador.id', array('value' => $usuario_id)); ?>
<?php echo $form->hidden('id'); ?>
