<?php

class Categoria extends AppModel {

    var $name = "Categoria";
    var $validate = array(
        'nome' => array(
            'vazio' => array(
                'rule' => 'notEmpty',
                'message' => 'Preencha o nome da categoria'
            )
        )
    );
    var $order = "ordem";

}

?>
