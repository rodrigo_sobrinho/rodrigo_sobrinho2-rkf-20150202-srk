<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/alert.js?v=0.1"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/tooltip.js?v=0.1"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/modal.js?v=0.1"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/popover.js?v=0.1"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/tab.js?v=0.1"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/bootbox.js?v=0.2"></script>
<script type="text/javascript">

$(document).ready(function(){
    //plugin de ativacao do plugin
    $('.selectpicker').selectpicker({width:'100%', size:'10'});
    $(".enviar").click(function(){
        //pega o value (id)
        valor = $('.selectpicker option:selected').val();
        $.ajax ({
            //prefixo, controller, action e parametro
            url : '/<?=$this->params['prefix']?>/eventos/mapa/' + valor,
            data : {
                Evento : $("select[name='data[Evento][id]']").val(),
                Mapa : $("select[name='data[Evento][mapa]']").val()
            },
            type : "POST",
            complete: function(response) {
                $('#conteudo').html(response.responseText);
            }
        });
    });

    $(".consultar").click(function(){
        //pega o value (id)
        valor = $('.selectpicker option:selected').val();
        $.ajax ({
            //prefixo, controller, action e parametro
            url : '/<?=$this->params['prefix']?>/eventos/mapa/' + valor,
            data : {
                Evento : $("select[name='data[Evento][id]']").val(),
                Consultar : true
            },
            type : "POST",
            complete: function(response) {
                $('#conteudo').html(response.responseText);
            }
        });
    });

    $(".replicar").click(function(){
        bootbox.dialog('Carregando',[{
            label: 'Fechar'
        }],{
            remote: '/<?=$this->params['prefix']?>/mapa_mesas/replicar_mapa/'+$("select[name='data[Evento][id]']").val()
        });
    });
});
</script>
<?php $session->flash(); ?>
<div class="row-fluid">
    <h2>
        Selecione o Evento
    </h2>
</div>
<br />
<div class="row-fluid">
    <div class="span6">
        <?=$form->input('Evento.id', array(
            'options' => $eventos,
            'type' => 'select',
            'class' => 'selectpicker',
            'label' => 'Eventos',
            'div' => 'input-control text')); ?>
    </div>
        <div class="span6">
    <!-- tipo do mapa -->
    <?php if ( $mapas ) : ?>
        <?=$form->input('Evento.mapa', array(
            'options' => $mapas,
            'type' => 'select',
            'class' => 'selectpicker',
            'label' => 'Locais',
            'div' => 'input-control text')); ?>
            <?php endif; ?>
    </div>

</div>
<div class="row-fluid">
    <div class="span12">
        <button type='submit' class='button max bg-color-greenDark enviar'>
            Enviar
            <i class='icon-ok'></i>
        </button>
        <button type='submit' class='button max bg-color-purple consultar'>
            Consultar Mapa de Mesa
            <i class='icon-ok'></i>
        </button>
        <button type='submit' class='button max bg-color-blue replicar'>
            Replicar Mapa de Mesa
            <i class='icon-ok'></i>
        </button>
    </div>
</div>
