    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/max/knockout/relatorio_itens_checkout.js"></script>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<style type="text/css">
td,th { text-align: center!important; position:relative }
td span,th span { vertical-align: text-top }
.pode-ordenar > thead > tr > th:hover { cursor: pointer; opacity: .9 }
td > i,th > i { position:absolute; top:3px }
</style>
<div class="row-fluid">
    <h2>
        <a class="win-backbutton home" href="/<?=$this->params['prefix']?>"></a>
        Relatorio de Mesas e Convites <?=$turma['Turma']['id']?> <?=$turma['Turma']['nome']?>
    </h2>
</div>
<br />
<div class="row-fluid" id="relatorio-itens">
    <!-- ko if: $root.carregando -->
    <div class="row-fluid">
        <h2>Carregando</h2>
    </div>
    <!-- /ko -->
    <!-- ko ifnot: $root.carregando -->
    <div class="row-fluid">
        <h3 style="vertical-align: text-top">
            Convites Recebidos&nbsp;&nbsp;
            <!-- ko if: convitesRecebidos() == 0 -->
            <input type="number" data-bind="value:convitesAlterados,
                valueUpdate: 'afterkeydown'" />&nbsp;&nbsp;
            <button type="button" data-bind="disable:disableConvites, click: enviarConvites">
                Enviar
            </button>
            <!-- /ko -->
            <!-- ko if: convitesRecebidos() != 0 -->
            <span class="label" data-bind="text: convitesRecebidos"></span>
            <!-- /ko -->
        </h3>
        <h3>Itens cadastrados</h3>
        <br />
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th width="30%">Campanha</th>
                    <th width="30%">Item</th>
                    <th width="10%">Convites</th>
                    <th width="10%">Mesas</th>
                    <th width="10%">Convites Vendidos</th>
                    <th width="10%">Mesas Vendidas</th>
                </tr>
            </thead>
            <tbody data-bind="foreach: campanhas">
                <tr>
                    <td data-bind="text:campanha"></td>
                    <td data-bind="text:item"></td>
                    <td data-bind="text:convites"></td>
                    <td data-bind="text:mesas"></td>
                    <td data-bind="text:total_convites"></td>
                    <td data-bind="text:total_mesas"></td>
                </tr>
            </tbody>
        </table>
        <br />
        <h3>Resumo</h3>
        <br />
        <table class="table table-condensed table-striped">
            <tbody>
                <tr>
                    <td width="25%"></td>
                    <td width="15%" class="header">Ja fizeram Checkout</td>
                    <td width="15%" class="header">Pagamentos em dia</td>
                    <td width="15%" class="header">Ativos</td>
                    <td width="15%" class="header">Inadimplentes</td>
                    <td width="15%" class="header">Total</td>
                </tr>
                <tr>
                    <td class="header">Formandos</td>
                    <td data-bind="text:$root.quantidade().formandos.checkout.length"></td>
                    <td data-bind="text:$root.quantidade().formandos.valido.length"></td>
                    <td data-bind="text:$root.quantidade().formandos.ativo.length"></td>
                    <td data-bind="text:$root.quantidade().formandos.inadimplente.length - $root.quantidade().formandos.ativo.length"></td>
                    <td data-bind="text:$root.quantidade().formandos.inadimplente.length"></td>
                </tr>
                <tr>
                    <td class="header">Convites</td>
                    <td data-bind="text:$root.quantidade().convites.checkout"></td>
                    <td data-bind="text:$root.quantidade().convites.valido"></td>
                    <td data-bind="text:$root.quantidade().convites.ativo"></td>
                    <td data-bind="text:$root.quantidade().convites.inadimplente - $root.quantidade().convites.ativo"></td>
                    <td data-bind="text:$root.quantidade().convites.inadimplente"></td>
                </tr>
                <!-- ko if: convitesRecebidos() != 0 -->
                <tr class="fg-color-green">
                    <td class="header">Saldo Convites</td>
                    <td data-bind="css:{'fg-color-red':convitesRecebidos() - $root.quantidade().convites.checkout < 0}">
                        <span data-bind="text:Math.abs(convitesRecebidos() - $root.quantidade().convites.checkout)">
                        </span>
                        <i data-bind="css:{
                            'icon-minus-3':convitesRecebidos() - $root.quantidade().convites.checkout < 0,
                            'icon-plus-3':convitesRecebidos() - $root.quantidade().convites.checkout > 0}"></i>
                    </td>
                    <td data-bind="css:{'fg-color-red':convitesRecebidos() - $root.quantidade().convites.valido < 0}">
                        <span data-bind="text:Math.abs(convitesRecebidos() - $root.quantidade().convites.valido)">
                        </span>
                        <i data-bind="css:{
                            'icon-minus-3':convitesRecebidos() - $root.quantidade().convites.valido < 0,
                            'icon-plus-3':convitesRecebidos() - $root.quantidade().convites.valido > 0}"></i>
                    </td>
                    <td data-bind="css:{'fg-color-red':convitesRecebidos() - $root.quantidade().convites.ativo < 0}">
                        <span data-bind="text:Math.abs(convitesRecebidos() - $root.quantidade().convites.ativo)">
                        </span>
                        <i data-bind="css:{
                            'icon-minus-3':convitesRecebidos() - $root.quantidade().convites.ativo < 0,
                            'icon-plus-3':convitesRecebidos() - $root.quantidade().convites.ativo > 0}"></i>
                    </td>
                    <td data-bind="css:{'fg-color-red':convitesRecebidos() - $root.quantidade().convites.inadimplente < 0}">
                        <span data-bind="text:Math.abs(convitesRecebidos() - $root.quantidade().convites.inadimplente)">
                        </span>
                        <i data-bind="css:{
                            'icon-minus-3':convitesRecebidos() - $root.quantidade().convites.inadimplente < 0,
                            'icon-plus-3':convitesRecebidos() - $root.quantidade().convites.inadimplente > 0}"></i>
                    </td>
                    <td>-</td>
                </tr>
                <!-- /ko -->
                <tr>
                    <td class="header">Mesas</td>
                    <td data-bind="text:$root.quantidade().mesas.checkout"></td>
                    <td data-bind="text:$root.quantidade().mesas.valido"></td>
                    <td data-bind="text:$root.quantidade().mesas.ativo"></td>
                    <td data-bind="text:$root.quantidade().mesas.inadimplente - $root.quantidade().mesas.ativo"></td>
                    <td data-bind="text:$root.quantidade().mesas.inadimplente"></td>
                </tr>
            </tbody>
        </table>
        <br />
        <h3>Vendas Checkout</h3>
        <br />
        <table class="table table-condensed table-striped" style="width:80%">
            <thead>
                <tr>
                    <th width="50%">Dia</th>
                    <th width="25%">Convites</th>
                    <th width="25%">Mesas</th>
                </tr>
            </thead>
            <tbody data-bind="foreach: dias">
                <tr>
                    <td data-bind="text:dia"></td>
                    <td data-bind="text:convites"></td>
                    <td data-bind="text:mesas"></td>
                </tr>
            </tbody>
        </table>
        <br />
        <h3>Lista</h3>
        <br />
        <a class="button mini bg-color-green pull-right" target="blank" href="/<?=$this->params['prefix']?>/turmas/gerar_excel_relatorio_entrega/<?=$turma['Turma']['id']?>">
            Excel
            <i class='icon-file-excel'></i>
        </a>
        <table class="table table-condensed table-striped pode-ordenar">
            <thead>
                <tr>
                    <th width="5%" data-bind="click: function() { $root.ordenar('codigo') }">
                        Cod
                        <i data-bind="attr:{ class: $root.classeOrdem('codigo') }"></i>
                    </th>
                    <th width="25%" data-bind="click: function() { $root.ordenar('nome') }">
                        Formando
                        <i data-bind="attr:{ class: $root.classeOrdem('nome') }"></i>
                    </th>
                    <th width="20%" data-bind="click: function() { $root.ordenar('nomeCampanha') }">
                        Campanha
                        <i data-bind="attr:{ class: $root.classeOrdem('nomeCampanha') }"></i>
                    </th>
                    <th width="30%" data-bind="click: function() { $root.ordenar('tipo') }">
                        Tipo
                        <i data-bind="attr:{ class: $root.classeOrdem('tipo') }"></i>
                    </th>
                    <th width="10%" data-bind="click: function() { $root.ordenar('status') }">
                        Status
                        <i data-bind="attr:{ class: $root.classeOrdem('status') }"></i>
                    </th>
                    <th width="5%" data-bind="click: function() { $root.ordenar('convites') }">
                        Convites
                        <i data-bind="attr:{ class: $root.classeOrdem('convites') }"></i>
                    </th>
                    <th width="5%" data-bind="click: function() { $root.ordenar('mesas') }">
                        Mesas
                        <i data-bind="attr:{ class: $root.classeOrdem('mesas') }"></i>
                    </th>
                </tr>
            </thead>
            <tbody data-bind="foreach: itens">
                <tr>
                    <td data-bind="text:codigo"></td>
                    <td data-bind="text:nome"></td>
                    <td data-bind="text:nomeCampanha"></td>
                    <td data-bind="text:tipo"></td>
                    <td data-bind="text:status"></td>
                    <td data-bind="text:convites"></td>
                    <td data-bind="text:mesas"></td>
                </tr>
            </tbody>
        </table>
    </div>
    <!-- /ko -->
</div>