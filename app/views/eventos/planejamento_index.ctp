<span id="conteudo-titulo" class="box-com-titulo-header">Eventos</span>
<div id="conteudo-container">
	<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
	<?php $session->flash(); ?>
	<div class="tabela-adicionar-item">
		<?php echo $html->link('Novo Evento',array($this->params['prefix'] => true, 'action' => 'escolher')); ?>
		<div style="clear:both;"></div>
	</div>
	<div class="container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col"><?php echo $paginator->sort('Id', 'id'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Nome', 'nome'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Tipo', 'tipo_evento'); ?></th>					
					<th scope="col"><?php echo $paginator->sort('Status', 'status'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Data', 'data'); ?></th>
					<th scope="col"> &nbsp;</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="4"><?php echo $paginator->counter(array('format' => 'Tipo de Evento %start% ao %end% - página %page% de %pages%')); ?>
						<span class="paginacao">
							<?php echo $paginator->numbers(array('separator' => ' ')); ?>
						</span>
					</td>
					<td colspan="1"><?php echo $paginator->counter(array('format' => 'Total : %count% ' .  $this->name)); ?></td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach($eventos as $evento): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<td colspan="1"><?php echo $evento['Evento']['id'];?></td>
					<td colspan="1"><?php echo $evento['Evento']['nome'];?></td>
					<td colspan="1"><?php echo $evento['TiposEvento']['nome']?></td>
					<td colspan="1"><?php echo $evento['Evento']['status'];?></td>
					<td colspan="1"><?php echo date('d/m/Y',strtotime($evento['Evento']['data'])); ?></td>					
					<td colspan="1">
					<?php echo $html->link('Visualizar', array($this->params['prefix'] => true, 'controller' => 'eventos', 'action' =>'visualizar', $evento['Evento']['id']), array('class' => 'submit button')); ?>
					<?php echo $html->link('Editar', array($this->params['prefix'] => true, 'controller' => 'eventos', 'action' =>'editar', $evento['Evento']['id']), array('class' => 'submit button')); ?>
                                        <?php echo $html->link('Extras', array($this->params['prefix'] => true, 'controller' => 'extras', 'action' =>'index', $evento['Evento']['id']), array('class' => 'submit button')); ?></td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>