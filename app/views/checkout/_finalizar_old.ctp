<script>
	var credito = false;
	
	$(function() {
		$('.ok').click( function() {
			var pendente = true;
			var dir = $(this).attr('dir');
			var item = checkout.get(dir);
			if($(this).is(":checked"))
				item.debitar(0);
			else
				item.saldado = false;
			checkout.pendencias.validar(item);
			checkout.total.atualizar();
		});
	});

</script>
<div class='adicionar-itens clearfix' style='width:100%;float:left; margin-bottom:40px'>
<div class='resumo-checkout'>
	<p>
		<br />
		O formando precisa quitar o saldo negativo para poder finalizar o checkout.
		<br />
		Clique em OK nos itens pendentes para confirmar o recebimento do dinheiro.
		<br />
		<span class='pendencias' dir='adesao'>
			Adesão - <span class='pendencia' dir='adesao'>Pendente</span>
			<br />
		</span>
		<span class='pendencias' dir='campanhas'>
			Campanha - <span class='pendencia' dir='campanhas'>Pendente</span>
			<br />
		</span>
		<span class='pendencias' dir='convites' alt='final'>
			Convites - <span class='pendencia' dir='convites'>Pendente</span>
			<br />
		</span>
		<span class='pendencias' dir='mesas' alt='final'>
			Mesas - <span class='pendencia' dir='mesas'>Pendente</span>
			<br />
		</span>
	</p>
</div>
<table style='width:55%; float:left; margin-bottom:20px' class='pendencias-finais'>
	<thead>
		<tr>
			<th width="20%">Item</th>
			<th width="60%">Descrição</th>
			<th width="20%">OK</th>
		</tr>
	</thead>
	<tr class='pendencias final' dir='adesao'>
		<td class='teste'>Adesão<div class='tooltip'>Veja aqui mais teste</div></td>
		<td class='descricao-parcelamento'></td>
		<td><?=$form->checkbox('okPagamentoAdesa',array('class' => 'ok', 'dir' => 'adesao')); ?></td>
	</tr>
	<tr class='pendencias final' dir='campanhas'>
		<td>Campanhas</td>
		<td class='descricao-parcelamento'></td>
		<td><?=$form->checkbox('okPagamentoCampanha',array('class' => 'ok', 'dir' => 'campanhas')); ?></td>
	</tr>
	<tr class='pendencias final' dir='convites'>
		<td>Convites Extras</td>
		<td class='descricao-parcelamento'></td>
		<td><?=$form->checkbox('okPagamentoConvites',array('class' => 'ok', 'dir' => 'convites')); ?></td>
	</tr>
	<tr class='pendencias final' dir='mesas'>
		<td>Mesas Extras</td>
		<td class='descricao-parcelamento'></td>
		<td><?=$form->checkbox('okPagamentoMesas',array('class' => 'ok', 'dir' => 'mesas')); ?></td>
	</tr>
</table>
<table style='width:50%; float:left'>
	<thead>
		<tr>
			<th colspan="2">Confira os itens da festa de formatura</th>
		</tr>
	</thead>
	<tr>
		<td>Mesas</td>
		<td class='qtde-mesas'><?=$turma['Turma']['mesas_contrato'];?></td>
	</tr>
	<tr>
		<td>Convites</td>
		<td class='qtde-convites'><?=$turma['Turma']['convites_contrato'];?></td>
	</tr>
</table>
</div>
<span id='finalizar-checkout' style='display:none' class='button submit'>FINALIZAR CHECKOUT</span>