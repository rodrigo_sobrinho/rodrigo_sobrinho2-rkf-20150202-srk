<style type="text/css">
    .container-conta {
        text-align: center;
    }
    .img-center {
        width:50%;
        height:auto;
        margin-bottom: 10px;
    }
    .container-conta .tipo {
        text-align: left;
        font-size: 15px;
        padding: 5px 0;
        margin-bottom: 10px;
        border-bottom:solid 2px transparent;
    }
    .container-conta .tipo i { vertical-align: text-top }
    .container-conta .button { transition: all .2s ease; }
    .container-conta .button:disabled { opacity: .6; }
    .container-conta.facebook .tipo {
        color:#2d89ef;
        border-color: #2d89ef;
    }
    .container-conta.facebook .button {
        border-color: #0E5DB5;
        background-color: #0E5DB5;
        color:white;
    }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        
        var context = ko.contextFor($("#content-body")[0]);
        
        $("body").on('fbinit',function() {
            $(".conta-facebook").removeClass('disabled');
        });
        
        if($("body").hasClass('fbinit')) {
            $(".conta-facebook").removeClass('disabled');
        };
        
        $("#conectar-facebook a").click(function(e) {
            e.preventDefault();
            var a = $(this);
            FB.api('/me', function(response) {
                if(response.id) {
                    FB.logout(function() {
                        a.addClass('clicado');
                        conectarFacebook();
                    });
                } else {
                    a.addClass('clicado');
                    conectarFacebook();
                }
            });
        });
        
        $("body").one('conta-logada',function(d) {
            if(d.tipo == 'facebook')
                conectarFacebook();
        });
        
        $(".alterar-status").click(function() {
            var button = this;
            $(button).attr('disabled','disabled');
            setTimeout(function() {
                $(button).removeAttr('disabled');
            },2000);
        });
        
        function conectarFacebook() {
            var li = $("#conectar-facebook");
            if(!li.hasClass('disabled') && li.children('a').hasClass('clicado')) {
                if(li.children('a').hasClass('enviando')) {
                    FB.api('/me', function(response) {
                        if(response.id) {
                            var div = $("<div/>");
                            var texto = $("<h3/>",{
                                html : "Erro ao conectar servidor. Tente novamente mais tarde"
                            });
                            texto.appendTo(div);
                            $.ajax({
                                type : 'POST',
                                url : '/usuarios/conectar_facebook',
                                data : {
                                    data : {
                                        usuario : '<?=$usuario['Usuario']['id']?>',
                                        facebook : response
                                    }
                                },
                                dataType : 'json',
                                success : function(r) {
                                    if(r.erro) {
                                        texto.addClass('fg-color-red');
                                        texto.html("Erro ao vincular conta. Tente novamente mais tarde");
                                        if(r.mensagem.length > 0)
                                            texto.html(r.mensagem.join("<br />"));
                                    } else {
                                        texto.html("Conta vinculada com sucesso").addClass('fg-color-green');
                                    }
                                    bootbox.alert(div.html(),function() {
                                        li.children('a').removeClass('enviando');
                                        if(r.erro) {
                                            li.children('a').removeClass('enviando');
                                        } else {
                                            context.$data.reload();
                                        }
                                    });
                                },
                                error : function() {
                                    texto.addClass('fg-color-red');
                                    bootbox.alert(div.html(),function() {
                                        li.children('a').removeClass('enviando');
                                    });
                                }
                            });
                        }
                    });
                } else {
                    li.children('a').addClass('enviando');
                    FB.login(function() {
                        li.children('a').removeClass('enviando');
                    },{
                        scope: 'email,user_likes'
                    });
                }
            }
        };
    });
</script>
<div class="row-fluid">
    <div class="span3">
        <div class="dropdown">
            <a class="dropdown-toggle button default btn-block" data-toggle="dropdown" href="#">
                Vincular Conta
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                <li class="disabled conta-facebook" id="conectar-facebook">
                    <a href="#">Facebook</a>
                </li>
            </ul>
        </div>
    </div>
</div>
<br />
<div class="row-fluid">
    <?php if(count($usuario['UsuarioConta']) > 0) : ?>
    <?php foreach($usuario['UsuarioConta'] as $i => $conta) : ?>
    <div class="span3 container-conta <?=$conta['tipo']?>">
        <?php if($conta['tipo'] == 'facebook') : ?>
        <div class="tipo">
            <i class="icon-facebook-2"></i>
            Facebook
        </div>
        <img src="https://graph.facebook.com/<?=$conta['uid']?>/picture?width=200&height=200"
            onerror="this.src='/img/no-image.gif'"
            class="img-circle img-center" />
        <?php endif; ?>
        <!--
        <button type="button" class="button btn-block alterar-status">
            <?=$conta['ativo'] == 1 ? "Desativar" : "Ativar"?>
        </button>
        -->
    </div>
    <?php if(($i+1) % 4 == 0) echo '</div><div class="row-fluid">'; endforeach; ?>
    <?php else : ?>
    <h3 class="fg-color-red">Nenhuma conta vinculada</h3>
    <?php endif; ?>
</div>