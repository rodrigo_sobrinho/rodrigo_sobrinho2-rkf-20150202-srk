<span id="conteudo-titulo" class="box-com-titulo-header">Relatorio Consolidado de Vendas</span>
<div id="conteudo-container">
    <h2 style="color:red">Venda de Extras Antecipados</h2>
    <br />
    <div class="container-tabela">
        <table>
            <thead>
                <tr>
                    <th scope="col">Campanha</th>
                    <th scope="col">Extra</th>
                    <th scope="col">Quantidade Retirada</th>
                </tr>
            </thead>
            <tbody>
                <?php if(count($extras) > 0) : ?>
                <?php foreach($extras as $extra) : ?>
                <tr>
                    <td><?=$extra['ViewRelatorioExtras']['campanha']?></td>
                    <td><?=$extra['ViewRelatorioExtras']['extra']?></td>
                    <td><?=$extra[0]['quantidade_retirada']?></td>
                </tr>
                <?php endforeach; ?>
                <?php else : ?>
                <tr>
                    <td colspan='3'>
                        <h2 style="color:red">Nenhum Extra Vendido</h2>
                    </td>
                </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
    <br />
    <br />
    <h2 style="color:red">Venda de Extras P&oacute;s-Checkout</h2>
    <br />
    <div class="container-tabela">
        <table>
            <thead>
                <tr>
                    <th scope="col">Extra</th>
                    <th scope="col">Quantidade Retirada</th>
                </tr>
            </thead>
            <tbody>
                <?php if(count($checkout) > 0) : ?>
                <?php foreach($checkout as $item) : ?>
                <tr>
                    <td><?=$item['CheckoutItem']['titulo']?></td>
                    <td><?=$item[0]['quantidade_retirada']?></td>
                </tr>
                <?php endforeach; ?>
                <?php else : ?>
                <tr>
                    <td colspan='3'>
                        <h2 style="color:red">Nenhum Extra P&oacute;s-Checkout Vendido</h2>
                    </td>
                </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>