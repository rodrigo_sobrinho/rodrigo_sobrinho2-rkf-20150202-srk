<h2>
    <a class="metro-button reload" data-bind="click: reload"></a>
    Eventos
    <a href="/<?=$this->params['prefix']?>/catalogo/editar_evento" data-bind="click: loadThis"
        class="button mini default pull-right">
        Novo Evento
    </a>
</h2>
<?php $session->flash(); ?>
<div class="row-fluid">
    <?php if (sizeof($eventos) > 0) : ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th>Evento</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($eventos as $evento) : ?>
            <tr>
                <td><?=$evento['TiposEvento']['nome']?></td>
                <td>
                    <button class="button mini default" type="button"
                        data-bind="click: loadThis"
                        href="/<?=$this->params['prefix']?>/catalogo/listar/<?=$evento['TiposEvento']['id']; ?>">
                        Entrar
                    </button>
                    <button class="button mini bg-color-green" type="button"
                        data-bind="click: loadThis"
                        href="/<?=$this->params['prefix']?>/catalogo/editar_evento/<?=$evento['TiposEvento']['id']; ?>">
                        <i class="icon-pen-alt-fill"></i>
                        Editar
                    </button>
                    <button class="button mini bg-color-<?=$evento["TiposEvento"]["ativo"] == 1 ? "red" : "blue"?>"
                        type="button" data-bind="click: loadThis"
                        href="/<?=$this->params['prefix']?>/catalogo/status_evento/<?=$evento['TiposEvento']['id']; ?>">
                        <?=$evento["TiposEvento"]["ativo"] == 1 ? "Desativar" : "Ativar"?>
                    </button>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php else : ?>
    <h2 class="fg-color-red">Nenhum evento para cat&aacute;logo cadastrado</h2>
    <?php endif; ?>
</div>