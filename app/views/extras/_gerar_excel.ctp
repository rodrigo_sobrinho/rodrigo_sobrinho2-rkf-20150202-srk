<?php 
	$listaDeFormandosParaExcel = array();

	foreach ($formandos as $formando)
		$listaDeFormandosParaExcel[] = array(
				"Codigo do formando" => $formando['ViewRelatorioExtras']['codigo_formando'],
				"Nome" => $formando['ViewRelatorioExtras']['formando'],
				"Extra" => $formando['ViewRelatorioExtras']['extra'],
				"Qtde." => $formando['ViewRelatorioExtras']['quantidade_total'],
				"Campanha" => $formando['ViewRelatorioExtras']['campanha'],
				"Evento" => $formando['ViewRelatorioExtras']['evento'],
				"Valor da Campanha" => number_format($formando['ViewRelatorioExtras']['valor_campanha'],2,',','.'),
				"Valor Pago" => number_format($formando['ViewRelatorioExtras']['valor_pago'],2,',','.'),
				"Status" => $formando['ViewRelatorioExtras']['status']
			);
	
	$excel->generate($listaDeFormandosParaExcel, 'lista_extras_turma_'.$formandos[0]['ViewRelatorioExtras']['turma_id']);
?>