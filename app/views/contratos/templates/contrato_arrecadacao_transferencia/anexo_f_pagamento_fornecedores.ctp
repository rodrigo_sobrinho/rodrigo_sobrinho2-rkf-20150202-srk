<?php if($out == "html") : ?>
<?php include('salvar_contrato.ctp')?>
<?php endif; ?>
<h4>ANEXO E – ATA DE PAGAMENTO A FORNECEDORES</h4>
<p style="font-size: 1.05em; line-height: 1.8em">
<?=str_pad("Faculdade:", 70, "&nbsp;")?><?=$turma['Turma']['nome']?>
<br />
<?=str_pad("Curso:", 108, "&nbsp;")?><?=$turma['Turma']['cursos']?>
<br />
<?=str_pad("Conclusão:", 71, "&nbsp;")?><?=$turma['Turma']['ano_formatura']?>.<?=$turma['Turma']['semestre_formatura']?>
</p>
<p style="text-align: justify">
O pagamento aos Fornecedores dos itens necessários para realização do Evento deve ser feito da seguinte maneira:
</p>
<p style="text-align: justify">
Nome do Fornecedor: ________________________________________________<br /><br />
CPF ou CNPJ: ________________________________________________<br /><br />
Produto(s) adquirido(s): ________________________________________________<br /><br />
Forma de Pagamento: ________________________________________________<br /><br />
</p>
<p style="text-align: justify;margin-left: 20px">
<b>a.</b> Nome ou Razão Social – Zape Gestão de Valores LTDA
<b>b.</b> CPF ou CNPJ – 184.055.43/0001-54
<b>c.</b> Banco – HSBC
<b>d.</b> Agencia – 
<b>e.</b> Conta Corrente com Dígito – 00739-66 
</p>
<br />
Os membros são:
<br />
<br />
<?php foreach($turma['Turma']['comissao'] as $comissao) : ?>
<p style="line-height: 1.3em">
<br />
<b>Nome: </b><?=key($comissao)?>
<br />
<b>RG: </b><?=$comissao[key($comissao)]?>
<br />
<br />
<?=str_pad("",60,"_")?>
<br />
ASSINATURA
<br />
</p>
<?php endforeach; ?>