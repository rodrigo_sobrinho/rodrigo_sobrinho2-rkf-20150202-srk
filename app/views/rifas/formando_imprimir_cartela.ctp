<?php

$contagemCupons = 0;
$cuponsPorLinha = 10;
$larguraQuadrado = 80; // Valor definido no tabelas.css
		
?>

<div class="container_16 container-cartela">
	<div style="width: '<?php echo $larguraQuadrado * $cuponsPorLinha;?> px'; margin-right: 80px">
		<h1><?php echo $html->image('logo_rifa.png', array('alt' => 'Logo', 'width' => '200px')) ?></h1>
		<h1> Rifa </h1>
		<br /><br />
		<h2><b>Data do Sorteio</b></h2>
		<br />
		<p style="font-size: 12pt"><?php echo $data_sorteio_formatada;?></p>
		<br /><br />
		<h2><b> Prêmios</b></h2>
		<br />
		<div style="margin:0 auto; width:420px; padding:10px; border:solid 1px #333333; text-align:center; font-style:italic; font-size:14px">
		Os prêmios serão sorteados com os números da loteria federal.
		<br /><br />
		Os prêmios deverão ser retirados PELO FORMANDO que
		<br />
		vendeu a rifa em um prazo máximo de 15 dias após o sorteio.
		</div>
		<br />
		<?php $cupom = $cupons[0]; ?>
		<span style="font-size: 10pt; text-align: center;">
			<p><b>1&deg; Pr&ecirc;mio:</b>  <?php echo $cupom['Rifa']['premio_1']; ?></p>
			<p><?php if ($cupom['Rifa']['premio_2'] != "") echo "<b>2&deg; Pr&ecirc;mio:</b> ".$cupom['Rifa']['premio_2']; ?></p>
			<p><?php if ($cupom['Rifa']['premio_3'] != "") echo "<b>3&deg; Pr&ecirc;mio:</b> ".$cupom['Rifa']['premio_3']; ?></p>
		</span>
		
		<br /><br />
	</div>
	<?php		

		echo '<table style="border: 1px solid;" class="tabela-cartela">';

		foreach($cupons as $cupom) {
			$numero_cupom_formatado = str_pad($cupom['Cupom']['numero'] % 100000, 5, '0', STR_PAD_LEFT);
				
			if ($contagemCupons % $cuponsPorLinha == 0 ) {
				echo "<tr>";
			} 
			
			echo "<td '> ";
			echo "$numero_cupom_formatado";
			if($cupom['Cupom']['nome_comprador'] != '') {
				echo '<br /><br />' . $cupom['Cupom']['nome_comprador'];
			}
			echo "</td>";
			
			if($contagemCupons % $cuponsPorLinha == ($cuponsPorLinha - 1)) {
				echo "</tr>";
			}
		
			$contagemCupons++;
		};
		
		// Terminar de preencher a tabela com células vazias
		if($contagemCupons % $cuponsPorLinha > 0) {
			for($i = 0 ; $i < $cuponsPorLinha - ($contagemCupons % $cuponsPorLinha); $i++) {
				echo "<td> - </td>";
			}	
			echo "</tr>";
		}
		 
		
		echo "</table>";
?>
</div>

