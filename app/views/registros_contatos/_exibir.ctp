<h2 class='fg-color-red'>
    <?=$registro['RegistrosContato']['assunto']?>
</h2>
<span class='label label-important'>
    <?=date('d/m/Y',strtotime($registro['RegistrosContato']['data']))?>
</span>
<br />
<br />
<div class='row-fluid'>
    <div class='span5'>
        <h4>Criador</h4>
        <h5><?=$registro['Usuario']['nome']?></h5>
    </div>
    <div class='span5'>
        <h4>Turma</h4>
        <h5><?=$registro['Turma']['nome']?></h5>
    </div>
</div>
<br />
<div class='row-fluid'>
    <div class='span5'>
        <h4>Participantes AS</h4>
        <h5><?=$registro['RegistrosContato']['funcionarios_as']?></h5>
    </div>
    <div class='span5'>
        <h4>Participantes Comissão</h4>
        <h5>
        <?php if(count($registro['RegistroContatoUsuario']) > 0) : ?>
        <?php foreach($registro['RegistroContatoUsuario'] as $registroUsuario) : ?>
        <span class='label label-info'><?=(!empty($registroUsuario['Usuario']['nome'])) ? $registroUsuario['Usuario']['nome'] : 'Nenhum';?></span>
        <?php endforeach; ?>
        <?php else : ?>
            Nenhum membro da comiss&atilde;o
        <?php endif; ?>
        </h5>
    </div>
</div>
<br />
<div class='row-fluid'>
    <div class='span12'>
        <h4>Tipo</h4>
        <h5><?=ucfirst(utf8_encode($registro['RegistrosContato']['tipo']))?></h5>
    </div>
</div>
<br />
<div class='row-fluid'>
    <div class='span11'>
        <h4>Detalhes</h4>
        <h5><?=$registro['RegistrosContato']['detalhes']?></h5>
    </div>
</div>