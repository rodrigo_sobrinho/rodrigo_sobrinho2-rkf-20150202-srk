<?php

class RelatorioCheckoutComponent extends Object {

    var $uses = array('Turma', 'Protocolo', 'Usuario', 'ViewFormandos', 'CampanhasUsuario', 'CheckoutFormandoPagamento',
                      'CheckoutUsuarioItem', 'CampanhasUsuarioCampanhasExtra', 'CampanhasExtra', 'CampanhasUsuario',
                      'CheckoutUsuarioItem', 'Usuario');

    var $components = array('Financeiro');


    function __construct() {
        if ($this->uses !== false)
            foreach ($this->uses as $modelClass)
                $this->$modelClass = ClassRegistry::init($modelClass);
    }
    
    function obterRelatorioCheckout($turmaId = false){
        $this->layout = false;
        $this->Turma->unBindModelAll();
        $turma = $this->Turma->read(null, $turmaId);
        ini_set('memory_limit', '1024M');
        set_time_limit(0);
        $formandos = $this->ViewFormandos->find('all',array(
                'conditions' => array(
                    'turma_id' => $turma['Turma']['id'],
                    'situacao' => 'ativo'
                ),
                'joins' => array(
                    array(
                        "table" => "protocolos",
                        "type" => "inner",
                        "alias" => "Protocolo",
                        "conditions" => array(
                            "ViewFormandos.id = Protocolo.usuario_id",
                            "Protocolo.tipo = 'checkout'"
                        )
                    )
                ),
                'group' => array('ViewFormandos.id'),
                'fields' => array(
                    'ViewFormandos.*',
                    'Protocolo.*'
                )
        ));
        $this->CampanhasUsuarioCampanhasExtra->unbindModel(array(
            'belongsTo' => array('CampanhasUsuario')
        ),false);
        $this->CampanhasUsuario->bindModel(array(
            'hasMany' => array(
                'CampanhasUsuarioCampanhasExtra' => array(
                    'className' => 'CampanhasUsuarioCampanhasExtra',
                    'joinTable' => 'campanhas_usuarios_campanhas_extras',
                    'foreignKey' => 'campanhas_usuario_id'
                )
            )
        ),false);
        $this->CampanhasUsuario->recursive = 3;
        $relatorio = array();
        $totais = array();
        $totais['convites'] = 0;
        $totais['mesas'] = 0;
        $totais['meias'] = 0;
        foreach($formandos as $i => $formando){
            $protocolo = $this->Protocolo->find('first', array(
               'conditions' => array(
                   'usuario_id' => $formando['ViewFormandos']['id'],
                   'tipo' => 'checkout'
               )
            ));
            $itensCheckout = $this->CheckoutUsuarioItem->find('all', array(
                'conditions' => array(
                    'protocolo_id' => $protocolo['Protocolo']['id'],
                )
            ));
            $relatorio[$i]['codigo_formando'] = $formando['ViewFormandos']['codigo_formando'];
            $relatorio[$i]['nome'] = $formando['ViewFormandos']['nome'];
            $relatorio[$i]['total_mesas'] = $formando['ViewFormandos']['mesas_contrato'];
            $relatorio[$i]['total_convites'] = $formando['ViewFormandos']['convites_contrato'];
            $relatorio[$i]['total_meias'] = 0;
            $atendente = $this->Usuario->findById($protocolo['Protocolo']['usuario_criador']);
            $relatorio[$i]['atendente'] = ucfirst($atendente['Usuario']['nome']);
            $relatorio[$i]['data'] = date("d/m/Y  H:i:s", strtotime($protocolo['Protocolo']['data_cadastro']));
            foreach ($itensCheckout as $itemCheckout) {
                $relatorio[$i]['total_convites'] += $itemCheckout['CheckoutUsuarioItem']['quantidade'] * $itemCheckout['CheckoutItem']['quantidade_convites'];
                $relatorio[$i]['total_mesas'] += $itemCheckout['CheckoutUsuarioItem']['quantidade'] * $itemCheckout['CheckoutItem']['quantidade_mesas'];
                if($itemCheckout['CheckoutItem']['quantidade_convites'] == 0 && 
                   $itemCheckout['CheckoutItem']['quantidade_mesas'] == 0){
                    if(strpos($itemCheckout['CheckoutItem']['titulo'], 'Meia') !== false ||
                       strpos($itemCheckout['CheckoutItem']['titulo'], 'MEIA') !== false ||
                       strpos($itemCheckout['CheckoutItem']['titulo'], 'Infantil') !== false ||
                       strpos($itemCheckout['CheckoutItem']['titulo'], 'INFANTIL') !== false)
                        $relatorio[$i]['total_meias'] += $itemCheckout['CheckoutUsuarioItem']['quantidade'];
                }
            }
            $campanhas = $this->CampanhasUsuario->find('all',array(
                'conditions' => array(
                    'usuario_id' => $formando['ViewFormandos']['id'],
                    'cancelada' => 0
                )
            ));
            $itensCampanhaRetirados = array();
            foreach($campanhas as $campanha) {
                foreach($campanha['CampanhasUsuarioCampanhasExtra'] as $extra) {
                    $relatorio[$i]['total_mesas'] += $extra['CampanhasExtra']['quantidade_mesas'] * $extra['quantidade'];
                    $relatorio[$i]['total_convites'] += $extra['CampanhasExtra']['quantidade_convites'] * $extra['quantidade'];
                    if($relatorio[$i]['total_mesas'] == 0 &&
                       $relatorio[$i]['total_convites'] == 0){
                        if(strpos($extra['CampanhasExtra']['Extra']['nome'], 'Meia') !== false ||
                           strpos($extra['CampanhasExtra']['Extra']['nome'], 'MEIA') !== false ||
                           strpos($extra['CampanhasExtra']['Extra']['nome'], 'Infantil') !== false ||
                           strpos($extra['CampanhasExtra']['Extra']['nome'], 'INFANTIL') !== false)
                            $relatorio[$i]['total_meias'] += $extra['quantidade'];
                    }
                }
            }
        }
        foreach($relatorio as $r){
            $totais['convites'] += $r['total_convites'];
            $totais['mesas'] += $r['total_mesas'];
            $totais['meias'] += $r['total_meias'];
        }
        return array($turma, $totais, $relatorio);
    }
}
