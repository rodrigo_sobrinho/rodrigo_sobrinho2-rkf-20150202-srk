<?php

class Despesa extends AppModel {
	var $name = 'Despesa';
	
	var $useTable = 'despesas';
	
    var $hasMany = array(
        'DespesaPagamento' =>
			array(
                'className'              => 'DespesaPagamento',
                'joinTable'              => 'despesas_pagamentos',
                'foreignKey'             => 'despesa_id',
                'unique'                 => true,
			)
	);
	
	const MULTA_PERCENTUAL = 10 ; // Aplicar multa de 10% em despesas atrasadas
	const MULTA_POR_DIA = 0.10 ;
}
?>