<?php

class MapaDeMesasFormando {

	Const GRUPO_FORMANDOS = 1;
	Const GRUPO_COMISSAO = 2;

	/**
	 * Identificação única do foramndo
	 * @var Integer
	 */
	private $id;

	/**
	 * Codigo formando da concatenação entre id da turma mais
	 * ordem em adesão do formando em sua turma
	 * @var String
	 */
	private $codigoFormando;

	/**
	 * Nome do formando
	 * @var String
	 */
	private $nome;

	/**
	 * Grupo a qual pertence o formando
	 * @var Integer
	 */
	private $grupo;

	/**
	 * Flag que informa se formando realizou ou não o checkout
	 * @var Boolean
	 */
	private $realizouCheckout;

	public function __construct() {
		$this->realizouCheckout = 0;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setNome($nome) {
		$this->nome = $nome;
	}

	public function setCodigoFormando($codigoFormando) {
		$this->codigoFormando = $codigoFormando;
	}
}