<script type="text/javascript" src="<?= $this->webroot ?>metro/js/imageToBase64.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var context = ko.contextFor($("#content-body")[0]);
        $("#logotipo").imageToBase64({
            success : function(file) {
                var button = $("#logotipo");
                button.attr("src",file.data);
                if($("[name='data[Parceiro][logo_src]']").length == 0)
                    $("#formulario").append($("<input>",{
                        type : "hidden",
                        name : "data[Parceiro][logo_src]"
                    })).append($("<input>",{
                        type : "hidden",
                        name : "data[Parceiro][logo_name]"
                    }));
                $("[name='data[Parceiro][logo_src]']").val(file.data.replace(/^data:image\/(gif|png|jpe?g);base64,/, ""));
                $("[name='data[Parceiro][logo_name]']").val(file.name);
                context.$data.loaded(true);
            },
            error : function() {
                alert("Erro ao carregar imagem");
            }
        });
        $("#formulario").submit(function(e) {
            e.preventDefault();
            return false;
        });
        $("#enviar").click(function() {
            context.$data.showLoading(function() {
                $.ajax({
                    type : 'POST',
                    url : $("#formulario").attr("action"),
                    dataType : 'json',
                    data : $("#formulario").serialize(),
                    error : function() {
                        alert("Erro ao enviar dados");
                    },
                    complete : function() {
                        context.$data.page($("#formulario").attr("action"));
                    }
                });
            });
        });
    });
</script>
<h2>
    <a class="metro-button reload" data-bind="click: reload"></a>
    <?=$parceiro ? "Editar" : "Inserir"?> Parceiro
</h2>
<?php $session->flash(); ?>
<?=$form->create('Parceiro',array(
    'url' => $this->here,
    'id' => 'formulario')) ?>
<?=$form->hidden("Parceiro.id");?>
<div class="row-fluid">
    <div class="span6">
        <?=$form->input('Parceiro.nome', array(
            'label' => 'Nome',
            'error' => false,
            'div' => 'input-control text')); ?>
    </div>
    <div class="span6">
        <?=$form->input('Parceiro.site', array(
            'label' => 'Site',
            'error' => false,
            'div' => 'input-control text')); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span8">
        <?=$form->input('Parceiro.descricao', array(
            'label' => 'Descrição',
            'error' => false,
            'type' => 'textarea',
            'div' => 'input-control text')); ?>
    </div>
    <div class="span2">
        <?=$form->input('Parceiro.ativo', array(
            'label' => 'Ativo',
            'error' => false,
            'type' => 'select',
            'options' => array('Não','Sim'),
            'div' => 'input-control text')); ?>
    </div>
    <div class="span2">
        <label>Logotipo</label>
        <img src="/fotos/carregar/<?=base64_encode("img/parceiros/logos/{$parceiro["Parceiro"]["logo"]}")?>/200"
            class="img-rounded" id="logotipo" />
    </div>
</div>
<button type="button" class="button bg-color-greenDark" id="enviar">
    Enviar
</button>
<?= $form->end(array('label' => false, 'div' => false, 'class' => 'hide')); ?>