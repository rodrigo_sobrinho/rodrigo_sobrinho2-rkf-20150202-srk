<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<style type="text/css">
    @media print {
        body * {
          visibility: hidden;
        }
        #printable, #printable * {
          visibility: visible;
        }
        #printable {
          position: fixed;
          left: 0;
          top: 0;
        }
    }
</style>
<div id="printable">
    <h2>Atendimentos dia <?=$data?> atendente <?=$atendente['Usuario']['nome']?></h2>
    <br />
    <h3>Resumo</h3>
    <br />
    <div class="row-fluid">
        <div class="span9">
            <table class="table table-condensed table-striped">
                <tbody>
                    <tr>
                        <td class="header">Atendimentos realizados</td>
                        <td><?=$resumo['atendimentos']?></td>
                    </tr>
                    <tr>
                        <td class="header">Convites Entregues</td>
                        <td><?=$resumo['convites']?></td>
                    </tr>
                    <tr>
                        <td class="header">Pagamentos em cheque</td>
                        <td><?=$resumo['cheque']?></td>
                    </tr>
                    <tr>
                        <td class="header">Soma em cheque</td>
                        <td>R$<?=number_format($resumo['valor_cheque'], 2, ',', '.') ?></td>
                    </tr>
                    <tr>
                        <td class="header">Pagamentos em dinheiro</td>
                        <td><?=$resumo['dinheiro']?></td>
                    </tr>
                    <tr>
                        <td class="header">Soma em dinheiro</td>
                        <td>R$<?=number_format($resumo['valor_dinheiro'], 2, ',', '.') ?></td>
                    </tr>
                    <tr>
                        <td class="header">Comprovantes recebidos</td>
                        <td><?=$resumo['comprovante']?></td>
                    </tr>
                    <tr>
                        <td class="header">Soma em comprovantes</td>
                        <td>R$<?=number_format($resumo['valor_comprovante'], 2, ',', '.') ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <br />
    <h3>Formandos Atendidos</h3>
    <br />
    <div class="row-fluid">
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th width="10%" scope="col">Turma</th>
                    <th width="15%" scope="col">Cod</th>
                    <th width="20%" scope="col">Tipo</th>
                    <th width="30%" scope="col">Nome</th>
                    <th width="10%" scope="col">Convites</th>
                    <th width="15%" scope="col">Hor&aacute;rio</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($atendimentos as $atendimento) : ?>
                <tr>
                    <td><?=$atendimento['Formando']['turma_id']?></td>
                    <td><?=$atendimento['Formando']['codigo_formando']?></td>
                    <td><?=$atendimento['Formando']['tipo']?></td>
                    <td><?=$atendimento['Formando']['nome']?></td>
                    <td><?=$atendimento['Formando']['convites']?></td>
                    <td><?=date('H:i',strtotime($atendimento['Protocolo']['data_cadastro']))?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <br />
    <h3>Pagamentos Recebidos</h3>
    <br />
    <div class="row-fluid">
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th width="25%" scope="col">Cod</th>
                    <th width="15%" scope="col">Forma</th>
                    <th width="20%" scope="col">Vencimento</th>
                    <th width="20%" scope="col">Valor</th>
                    <th width="20%" scope="col">Hor&aacute;rio</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($atendimentos as $atendimento) : if(isset($atendimento['CheckoutFormandoPagamento'])) : ?>
                <?php foreach($atendimento['CheckoutFormandoPagamento'] as $checkoutPagamento) : ?>
                <tr>
                    <td><?=$atendimento['Formando']['codigo_formando']?></td>
                    <td><?=$checkoutPagamento['tipo']?></td>
                    <td><?=date('d/m/Y',strtotime($checkoutPagamento['data_vencimento']))?></td>
                    <td>R$<?=number_format($checkoutPagamento['valor'], 2, ',', '.') ?></td>
                    <td><?=date('H:i',strtotime($checkoutPagamento['data_cadastro']))?></td>
                </tr>
                <?php endforeach; ?>
                <?php endif; ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>