<?php if($out == "html") : ?>
<?php include('salvar_contrato.ctp')?>
<?php endif; ?>
<h4>ANEXO E – CONTRATO DE COBERTURA FOTOGRÁFICA E DE FILMAGEM</h4>
<p style="font-size: 1.05em; line-height: 1.6em">
<?=str_pad("Faculdade:", 70, "&nbsp;")?><?=$turma['Turma']['nome']?>
<br />
<?=str_pad("Curso:", 108, "&nbsp;")?><?=$turma['Turma']['cursos']?>
<br />
<?=str_pad("Conclusão:", 71, "&nbsp;")?><?=$turma['Turma']['ano_formatura']?>.<?=$turma['Turma']['semestre_formatura']?>
</p>
<p style="text-align: justify;">
<b>1.</b> As fotografias que comporão os álbuns serão coloridas, no tamanho 24 x 30 cm, com padrão de qualidade compatível com o mercado 
fotográfico, ilustrando todos os eventos extraoficiais solicitados pela Comissão de formatura, além da Colação de Grau e 
Baile de Gala.<br/><br/>
<b>2.</b> Os materiais de vídeo serão editados em uma ESTAÇÃO DIGITAL onde todas as imagens serão captadas através de câmeras digitais. 
A captação digital garante a máxima qualidade de imagem e som.<br/><br/>
<b>3.</b> Os profissionais destacados para os eventos Extra Oficiais estarão vestidos com calça social preta, sapato social preto, 
camisa social branca e para os eventos oficiais, estarão em traje social completo. Em todos os eventos os profissionais estarão
ainda identificados de crachás ÁS EVENTOS.<br/><br/>
<b>4.</b> Serão destacados para o baile de formatura no mínimo um fotógrafo para cada doze alunos e um cinegrafista para cada vinte
e cinco alunos.<br/><br/>
<b>5.</b> Os preços das fotografias terão como base o valor de R$ 26,50, sendo esse preço referência em Janeiro de 2015, sendo que será
corrigido pelo IGPM no momento da venda do álbum ao formando (a correção do IGPM será o índice acumulado entre 01/01/2015 e a 
data da venda do álbum). Serão dadas diversas opções de parcelamento do álbum, negociadas diretamente entre formando e vendedor, 
sendo que os preços das fotos serão ajustados para absorver o financiamento.<br/><br/>
<b>6.</b> O prazo de entrega do material fotográfico dar-se-á no máximo de 180 dias úteis após a realização do Baile de Gala. A visita
para apresentação do material será em local e horário a critério do formando através de agendamento prévio. A forma de pagamento
que estará sujeito o formando respeitará todos os critérios ora citados, podendo a empresa criar meios complementares e 
facilitadores para aquisição dos produtos mencionados. Assim, o álbum poderá ser parcelado em até 12 vezes.<br/><br/>
<b>7.</b> Na festa de formatura será permitido o uso de máquinas fotográficas <b>não profissionais</b> após a valsa de formatura. Na colação
de grau não será permitido o uso de máquina fotográfica nem filmadora. O uso de máquinas fotográficas profissionais e filmadoras
ficam extremamente proibidos em qualquer evento.<br/><br/>
<b>8.</b> O formando não tem nenhuma obrigação na aquisição de fotografias e filmagem. No caso da compra das fotos, é permitido um 
descarte máximo de 30% de fotos do total do álbum de formatura.<br/><br/>
<b>9.</b> A ÁS EVENTOS é detentora da exclusividade vídeo-fotográfica dos eventos relativos às festividades de formatura, oferecendo 
descontos e eventos extras conforme consta no Anexo A e eventualmente no Anexo B, sempre proporcional ao número de formandos 
identificados e que estejam aderidos ao baile de formatura.<br/><br/>
</p>
