<span id="conteudo-titulo" class="box-com-titulo-header">Enviar Parcelamaker</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<?php echo $form->create('Parcelamaker', array('url' => "/{$this->params['prefix']}/parcelamentos/processa_excel",'type' => 'file')); ?>
	
	<p class="grid_11 alpha omega">
		<label class="grid_11 alpha omega">Nome</label>
		<?=$form->input('titulo', array('value' => 'Padrão', 'class' => 'grid_10 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
	</p>
	<p class="grid_11 alpha omega">
		<label class="grid_11 alpha">Arquivo Parcelamaker:</label>
		<?php echo $form->file('arquivo', array('class' => 'grid_11 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span')))?>
	</p>


	<p class="grid_11 alpha omega">
		<span class="grid_11 alpha omega">
			<?php echo $form->end(array('label' => 'Enviar', 'div' => false, 'class' => 'submit'));?>
			<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index') ,array('class' => 'cancel')); ?>
		</span>
	</p>
</div>