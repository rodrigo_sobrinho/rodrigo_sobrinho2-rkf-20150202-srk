<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/datetimepicker.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/locales/bootstrap-datetimepicker.pt-BR.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/utils.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/jquery.meio.mask.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/max/knockout/renegociacao.js"></script>
<style type="text/css">
.bootstrap-select * { font-size:12px!important; }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        //var context = ko.contextFor($("#content-body")[0]);
        $('.selectpicker').selectpicker();
        $("#total").setMask();
        $(".abrir-modal").click(function(e) {
            e.preventDefault();
            bootbox.dialog('Carregando',[{
                label: 'Fechar'
            }],{
                remote: $(this).attr('href')
            });
        });
    });
</script>
<script type="text/html" id="tmpPagamentos">
    <!-- ko if: $index() == 0 -->
    <h4 class="fg-color-red">
        Saldo: <strong data-bind="text:$root.formatarValor($root.saldo())"></strong>
    </h4>
    <h4 class="fg-color-blueDark">
        Soma das Parcelas: <strong data-bind="text:$root.formatarValor($root.somaPagamentos())"></strong>
    </h4>
    <br />
    <button type='button' class='bg-color-blue' data-bind="click: $root.adicionarParcela">
        Adicionar parcela
    </button>
    <br />
    <div class="row-fluid">
        <div class="span2">
            <label>Valor</label>
        </div>
        <div class="span3">
            <label>Data Venc</label>
        </div>
    </div>
    <!-- /ko -->
    <div class="row-fluid">
        <div class="span2">
            <div class="input-control text">
                <input type="text" class="valor" alt="decimal" data-bind="value:valor,valueUpdate:'afterkeydown'" />
            </div>
        </div>
        <div class="span3">
            <?=$form->input(false,array(
                'type' => 'text',
                'readonly' => 'readonly',
                'class' => 'datepicker',
                'data-bind' => "value: data_vencimento,valueUpdate:'afterkeydown'",
                'label' => false,
                'div' => 'input-control text')); ?>
        </div>
    </div>
</script>
<div class="row-fluid">
    <h2>
        <a class="metro-button back" data-bind="click: function() {
               page('<?="/{$this->params['prefix']}/formandos/listar"?>') }"></a>
        <a class="metro-button reload" data-bind="click: reload "
            href="/<?=$this->params['prefix']?>/area_financeira/renegociar/<?=$formando['id']?>"></a>
        <?=$formando['codigo_formando']?> - Renegociar Parcelas
        <button type="button" class="pull-right mini default abrir-modal"
            href="/<?=$this->params['prefix']?>/area_financeira/tabela_pagamentos/<?=$formando['id']?>">
            Tabela de pagamentos
        </button>
    </h2>
</div>
<?php $session->flash(); ?>
<h3 class="fg-color-blue">
    Total Pago: <strong>R$<?=number_format($totalPago, 2, ',', '.')?></strong>
    <br />
    Pacote: <strong><?=$parcelamento ? $parcelamento['Parcelamento']['titulo'] : "Não disponível"?></strong>
</h3>
<br />
<div data-bind="stopBinding: true">
    <div class="row-fluid" id="renegociacao">
        <?=$form->hidden(false,array(
            'value' => $formando['id'],
            'id' => "usuario-id"
            )); ?>
        <?=$form->hidden(false,array(
            'value' => $totalPago,
            'id' => "total-pago"
            )); ?>
        <div class="row-fluid" data-bind="visible: !erroPlanos()">
            <div class="span3">
                <?=$form->input(false,array(
                    'type' => 'select',
                    'id' => 'planos',
                    'class' => 'selectpicker',
                    'data-width' => '100%',
                    'data-container' => 'body',
                    'data-bind' => 'options: planos, value: plano',
                    'label' => 'Plano',
                    'div' => 'input-control text')); ?>
            </div>
            <div class="span2" data-bind="if: meses">
                <?=$form->input(false,array(
                    'type' => 'select',
                    'id' => 'meses',
                    'class' => 'selectpicker',
                    'data-width' => '100%',
                    'data-container' => 'body',
                    'data-bind' => "options: meses, optionsValue: 'value', optionsText: 'text', value: mes",
                    'label' => 'Mês',
                    'div' => 'input-control text')); ?>
            </div>
            <div class="span1" data-bind="if: meses">
                <?=$form->input(false,array(
                    'type' => 'select',
                    'options' => range(1,31),
                    'id' => 'dias',
                    'class' => 'selectpicker',
                    'data-width' => '100%',
                    'data-container' => 'body',
                    'data-bind' => "value: dia",
                    'label' => 'Dia',
                    'div' => 'input-control text')); ?>
            </div>
            <div class="span1" data-bind="if: parcelamentos">
                <?=$form->input(false,array(
                    'type' => 'select',
                    'id' => 'parcelamentos',
                    'class' => 'selectpicker',
                    'data-width' => '100%',
                    'data-container' => 'body',
                    'data-bind' => "options: parcelamentos, optionsValue: 'value', optionsText: 'text', value: parcelamento",
                    'label' => 'Parcelas',
                    'div' => 'input-control text')); ?>
            </div>
            <div class="span2" data-bind="if: parcelamentos">
                <?=$form->input(false,array(
                    'type' => 'text',
                    //'class' => 'readonly',
                    //'readonly' => 'readonly',
                    'alt' => 'decimal',
                    'id' => 'total',
                    'data-bind' => "value: total, valueUpdate:'afterkeydown'",
                    'label' => 'Total',
                    'div' => 'input-control text')); ?>
            </div>
            <div class="span2" data-bind="if: parcelamentos">
                <label>&nbsp;</label>
                <button type="button" class="default bg-color-blueDark input-block-level"
                    data-bind="click: gerarParcelas">
                    Parcelar
                </button>
            </div>
        </div>
        <div class="row-fluid" data-bind="visible: planoDiferenciado()">
            <div class="span11">
                <label>Justificativa</label>
                <textarea class="input-control input-block-level" id="justificativa"></textarea>
            </div>
        </div>
        <div class="row-fluid" data-bind="visible: erroPlanos()">
            <h2 class="fg-color-red">Nenhuma forma de pagamento encontrada para essa turma</h2>
        </div>
        <div class="row-fluid" data-bind="visible:parcelas().length > 0">
            <div class="row-fluid" data-bind="template: {
                name: 'tmpPagamentos', foreach: parcelas(), afterAdd: camposPagamento }">
            </div>
        </div>
        <div class="row-fluid" data-bind="visible:parcelas().length > 0 && saldo() - somaPagamentos() != 0">
            <h4 class="fg-color-red" data-bind="visible:saldo() - somaPagamentos() < 0">
                <em data-bind="text: formatarValor(saldo() - somaPagamentos())"></em> acima do valor total
            </h4>
            <h4 class="fg-color-red" data-bind="visible:saldo() - somaPagamentos() > 0">
                <em data-bind="text: formatarValor(Math.abs(saldo() - somaPagamentos()))"></em> abaixo do valor total
            </h4>
        </div>
        <div class="row-fluid" data-bind="visible:parcelas().length > 0 && saldo() - somaPagamentos() == 0">
            <div class="span5">
                <button type="button" class="default bg-color-greenDark input-block-level"
                    data-bind="click: enviarParcelas">
                    Enviar Renegocia&ccedil;&atilde;o
                </button>
            </div>
        </div>
    </div>
</div>