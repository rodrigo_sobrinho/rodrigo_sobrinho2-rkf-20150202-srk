<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery.meio.mask.js"></script>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/imageToBase64.js"></script>
<style type="text/css">
.div-foto { position:relative; display:inline-block; margin:0 5px 5px 0; }
.div-foto img { max-width:100px!important; max-height:100px; border:solid 2px #F2F2F2 }
.div-foto i { position: absolute; top:-5px; right:-5px; }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        var context = ko.contextFor($("#content-body")[0]);
        $("[alt]").setMask();
        $(".selectpicker").selectpicker();
        $("#fotos").imageToBase64({
            success : function(file) {
                var i = $(".div-foto").length;
                var div = $("<div>",{
                    class : 'div-foto'
                });
                div.append($("<i>",{ class : "icon-remove remover-foto fg-hover-color-red" }));
                div.append($("<input>",{
                    type : "hidden",
                    name : "data[fotos]["+i+"][name]",
                    value : file.name
                }));
                div.append($("<input>",{
                    type : "hidden",
                    name : "data[fotos]["+i+"][data]",
                    value : file.data.replace(/^data:image\/(gif|png|jpe?g);base64,/, "")
                }));
                div.append($("<img>",{
                    class : "img-rounded",
                    src : file.data
                }));
                $("#div-fotos").append(div);
                context.$data.loaded(true);
            },
            error : function() {
                alert("Erro ao carregar imagem");
            }
        });
        $("#categorias").click(function() {
            if($("[data-val="+$(".select-categorias").val()+"]").length == 0) {
                var button = $("<button>",{
                    class : 'button bg-color-blueDark botao-categorias',
                    text : $(".select-categorias").find("option[value="+$(".select-categorias").val()+"]").text(),
                    type : 'button'
                });
                var i = $(".botao-categorias").length;
                button.append($("<i>",{ class : "icon-remove remover-categoria pull-right fg-hover-color-red" }));
                button.append($("<input>",{
                    type : "hidden",
                    'data-val' : $(".select-categorias").val(),
                    name : "data[ParceriaCategorias][]",
                    value : $(".select-categorias").val()
                }));
                $("#div-categorias").append(button);
                context.$data.loaded(true);
            }
        });
        $("#turmas").click(function() {
            if($("[data-val="+$(".select-turmas").val()+"]").length == 0) {
                var button = $("<button>",{
                    class : 'button bg-color-blueDark botao-turmas',
                    text : $(".select-turmas").find("option[value="+$(".select-turmas").val()+"]").text(),
                    type : 'button'
                });
                var i = $(".botao-turmas").length;
                button.append($("<i>",{ class : "icon-remove remover-turma pull-right fg-hover-color-red" }));
                button.append($("<input>",{
                    type : "hidden",
                    'data-val' : $(".select-turmas").val(),
                    name : "data[turmas][]",
                    value : $(".select-turmas").val()
                }));
                $("#div-turmas").append(button);
                context.$data.loaded(true);
            }
        });
        $("#div-fotos").on('click','.remover-foto',function() {
            var div = $(this).parent('.div-foto');
            if(div.data('id') != undefined)
                $("#formulario").append($("<input>",{
                    type : "hidden",
                    name : "data[remover_fotos][]",
                    value : div.data('id')
                }));
            div.fadeOut(500,function() {
                div.remove();
                context.$data.loaded(true);
            });
        });
        $("#div-turmas").on('click','.remover-turma',function() {
            var button = $(this).parent('button');
            if(button.data('id') != undefined)
                $("#formulario").append($("<input>",{
                    type : "hidden",
                    name : "data[remover_turmas][]",
                    value : button.data('id')
                }));
            button.fadeOut(500,function() {
                button.tooltip("hide");
                button.remove();
                context.$data.loaded(true);
            });
        });
        $("#div-categorias").on('click','.remover-categoria',function() {
            var button = $(this).parent('button');
            if(button.data('id') != undefined)
                $("#formulario").append($("<input>",{
                    type : "hidden",
                    name : "data[remover_categorias][]",
                    value : button.data('id')
                }));
            button.fadeOut(500,function() {
                button.tooltip("hide");
                button.remove();
                context.$data.loaded(true);
            });
        });
        
        $("#enviar").click(function() {
            context.$data.showLoading(function() {
                $.ajax({
                    type : 'POST',
                    url : $("#formulario").attr("action"),
                    dataType : 'json',
                    data : $("#formulario").serialize(),
                    error : function() {
                        alert("Erro ao enviar dados");
                    },
                    complete : function() {
                        context.$data.page($("#formulario").attr("action"));
                    }
                });
            });
        });
    });
</script>
<h2>
    <a class="metro-button reload" data-bind="click: reload"></a>
    <?=$parceria ? "Editar" : "Inserir"?> Parceria
</h2>
<?php $session->flash(); ?>
<?=$form->create('Parceria',array(
    'url' => $this->here,
    'id' => 'formulario')) ?>
<?=$form->hidden("Parceria.id");?>
<div class="row-fluid">
    <?=$form->input('Parceria.parceiro_id', array(
        'label' => "Parceiro",
        'empty' => "Selecione",
        "options" => $parceiros,
        'type' => 'select',
        'error' => false,
        'div' => 'input-control text')); ?>
</div>
<div class="row-fluid">
    <div class="span6">
        <?=$form->input('Parceria.titulo', array(
            'label' => 'Titulo',
            'error' => false,
            'div' => 'input-control text')); ?>
    </div>
    <div class="span3">
        <?=$form->input('Parceria.valor', array(
            'label' => 'Valor',
            'error' => false,
            'div' => 'input-control text')); ?>
    </div>
    <div class="span3">
        <?=$form->input('Parceria.desconto', array(
            'label' => 'Desconto',
            'error' => false,
            'div' => 'input-control text')); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span6">
        <?=$form->input('Parceria.descricao', array(
            'label' => 'Descrição',
            'type' => 'textarea',
            'error' => false,
            'div' => 'input-control text')); ?>
    </div>
    <div class="span6">
        <?=$form->input('Parceria.observacoes', array(
            'label' => 'Observações',
            'type' => 'textarea',
            'error' => false,
            'div' => 'input-control text')); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span6">
        <?=$form->input('Parceria.local', array(
            'label' => 'Local',
            'error' => false,
            'div' => 'input-control text')); ?>
    </div>
    <div class="span3">
        <?=$form->input('Parceria.data_inicio', array(
            'label' => 'Início',
            'type' => 'text',
            'alt' => '99/99/9999 99:99',
            'error' => false,
            'div' => 'input-control text')); ?>
    </div>
    <div class="span3">
        <?=$form->input('Parceria.data_fim', array(
            'label' => 'Fim',
            'alt' => '99/99/9999 99:99',
            'type' => 'text',
            'error' => false,
            'div' => 'input-control text')); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span3">
        <label>Ativa</label>
    </div>
    <div class="span3">
        <?=$form->input('Parceria.ativa', array(
            'label' => false,
            "options" => array("Não","Sim"),
            'type' => 'select',
            'error' => false,
            'div' => 'input-control text')); ?>
    </div>
    <div class="span3">
        <label>P&uacute;blica</label>
    </div>
    <div class="span3">
        <?=$form->input('Parceria.publica', array(
            'label' => false,
            "options" => array("Não","Sim"),
            'type' => 'select',
            'error' => false,
            'div' => 'input-control text')); ?>
    </div>
</div>
<div class="row-fluid" id="div-fotos">
    <button type="button" class="button default" id="fotos">
        Adicionar Fotos
        <i class="icon-images"></i>
    </button>
    <br />
    <?php if(count($parceria["FotoParceria"]) > 0) : foreach($parceria["FotoParceria"] as $foto) : ?>
    <!--
    <button type="button" class="button bg-color-blueDark botao-fotos"
        data-id="<?=$foto["id"]?>" href="/img/parcerias/fotos/<?="{$parceria["Parceria"]["id"]}/{$foto["nome"]}"?>">
        <?=$foto["nome"]?>
        <i class="icon-remove remover-foto fg-hover-color-red"></i>
    </button>
    -->
    <div class="div-foto" data-id="<?=$foto["id"]?>">
        <i class="icon-remove remover-foto fg-hover-color-red"></i>
        <img src="/fotos/fit/<?=base64_encode("img/parcerias/fotos/{$parceria["Parceria"]["id"]}/{$foto["nome"]}")?>/100/100"
            class="img-rounded" />
    </div>
    <?php endforeach; endif; ?>
</div>
<br />
<div class="row-fluid">
    <div class="span4">
        <?=$form->input("Parceria.categorias", array(
            'label' => false,
            "options" => $categorias,
            'type' => 'select',
            'class' => 'selectpicker select-categorias',
            'error' => false,
            'div' => 'input-control text')); ?>
    </div>
    <div class="span3">
        <button type="button" class="button default" id="categorias">
            Adicionar Categoria
        </button>
    </div>
</div>
<div class="row-fluid" id="div-categorias">
    <?php if(count($parceria["Categoria"]) > 0) : foreach($parceria["Categoria"] as $categoria) : ?>
    <button type="button" class="button bg-color-blueDark botao-categorias"
        data-id="<?=$categoria["ParceriasCategoria"]["id"]?>">
        <?=$categoria["nome"]?>
        <i class="icon-remove remover-categoria fg-hover-color-red"></i>
    </button>
    <?php endforeach; endif; ?>
</div>
<div class="row-fluid">
    <div class="span4">
        <?=$form->input("Parcerias.turmas", array(
            'label' => false,
            "options" => $turmas,
            'type' => 'select',
            'class' => 'selectpicker select-turmas',
            'error' => false,
            'div' => 'input-control text')); ?>
    </div>
    <div class="span3">
        <button type="button" class="button default" id="turmas">
            Adicionar Turma
        </button>
    </div>
</div>
<div class="row-fluid" id="div-turmas">
    <?php if(count($parceria["Turma"]) > 0) : foreach($parceria["Turma"] as $turma) : ?>
    <button type="button" class="button bg-color-blueDark botao-turmas"
        data-id="<?=$turma["id"]?>" data-val="<?=$turma["id"]?>">
        <?=$turma["id"]?> - <?=$turma["nome"]?>
        <i class="icon-remove remover-turma fg-hover-color-red"></i>
    </button>
    <?php endforeach; endif; ?>
</div>
<button type="button" class="button bg-color-greenDark" id="enviar">
    Enviar
</button>
<?= $form->end(array('label' => false, 'div' => false, 'class' => 'hide')); ?>