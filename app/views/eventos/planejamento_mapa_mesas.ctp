<div class="span9 metro" style="position:relative" id="content-container">
    <div class="row-fluid content-loader hide" id="content-loading">
        <div class="cycle-loader"></div>
    </div>
    <div class="row-fluid content-loader hide" id="content-body"></div>
    <div class="row-fluid content-loader" id="content-home">

        <div class="tile icon bg-color-green"
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/checkout/listar') }">
            <div class="tile-content">
                <span class="img icon-calendar"></span>
            </div>
            <div class="brand"><span class="name">Agendamento Escolha</span></div>
        </div>

        <div class="tile icon bg-color-brown"
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/checkout/listar') }">
            <div class="tile-content">
                <span class="img icon-key"></span>
            </div>
            <div class="brand"><span class="name"></span></div>
        </div>

        <div class="tile icon bg-color-blue"
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/checkout/listar') }">
            <div class="tile-content">
                <span class="img icon-drawer-2"></span>
            </div>
            <div class="brand"><span class="name">Relatório</span></div>
        </div>

    </div>
</div>