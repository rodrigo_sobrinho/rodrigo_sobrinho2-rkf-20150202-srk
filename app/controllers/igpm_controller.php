<?php
class IgpmController extends AppController {

	var $name = 'igpm';
	
	var $paginate = array(
		'limit' => 100,
		'order' => array(
			'Igpm.ano' => 'desc',
			'Igpm.mes' => 'desc'
		)
	);

	function super_index() {
		$this->set('igpm', $this->paginate('Igpm'));
	}
        
        function super_listar(){
            $this->layout = false;
            $usuario = $this->Session->read('Usuario');
            if (!empty($this->data)) {
                $this->autoRender = false;
                Configure::write(array('debug' => 0));
                    $this->Session->write("filtros.{$usuario['Usuario']['grupo']}.igpm", $this->data['Igpm']);
            } else {
                $options['order'] = array('Igpm.ano' => 'desc', 'Igpm.mes' => 'desc');
                $filtro = $this->Session->read("filtros.{$usuario['Usuario']['grupo']}.igpm");
                if($filtro) {
                    $this->data['Igpm'] = $filtro;
                    foreach ($filtro as $chave => $valor)
                        $options['conditions']['lower(ano) LIKE '] = "%".strtolower($valor)."%";
                }
                $options['limit'] = 30;
                $this->paginate['Igpm'] = $options;
                $igpms = $this->paginate('Igpm');
                $this->set('igpms', $igpms);
                $this->render('super_listar');
            }
        }
        
        function super_alterar($id = false){
        $this->layout = false;
            $this->Igpm->id = $id;
            if (!empty($this->data)) {
                $this->autoRender = false;
                Configure::write(array('debug' => 0));
                if ($this->Igpm->save($this->data['Igpm'])) {
                    $this->Session->setFlash('Dados salvos com sucesso', 'metro/flash/success');
                }else{
                    $this->Session->setFlash('Ocorreu um erro ao alterar o IGPM.', 'metro/flash/error');
                }
            }else{
                $this->data = $this->Igpm->read();
            }
        }
        
        function super_inserir(){
        $this->layout = false;
            if (!empty($this->data)) {
                $this->autoRender = false;
                Configure::write(array('debug' => 0));
                if ($this->Igpm->save($this->data['Igpm'])) 
                    $this->Session->setFlash('Dados salvos com sucesso', 'metro/flash/success');
                else
                    $this->Session->setFlash('Ocorreu um erro ao inserir o IGPM.', 'metro/flash/error');
            }
        }

	function super_adicionar() {
		if ( !empty ( $this->data ) ) {
			
			$dateTime =  $this->create_date_time_from_format('d-m-Y H:i', $this->data['Igpm']['data-hora']);
            $this->data['Igpm']['data'] = date_format($dateTime, 'Y-m-d H:i:s');
            unset($this->data['Igpm']['data-hora']);
		
			$valorFormatado = r(",", ".", $this->data['Igpm']['valor']);
			$this->data['Igpm']['valor'] = $valorFormatado;

			if ( $this->Igpm->save ( $this->data ) ) {
				$this->Session->setFlash (__('O IGP-M foi salvo com sucesso.', true), 'flash_sucesso');
				$this->redirect ("/{$this->params['prefix']}/igpm");
			} else {
				$this->Session->setFlash (__('Ocorreu um erro ao salvar o IGP-M.', true), 'flash_erro');
			}
		}

		$this->data['Igpm']['data-hora'] = date('d-m-Y H:i');

	}

	function super_editar($id = null) {
		$this->Igpm->id = $id;
		
		if ( !empty($this->data) ) {

			$dateTime =  $this->create_date_time_from_format('d-m-Y H:i', $this->data['Igpm']['data-hora']);
            $this->data['Igpm']['data'] = date_format($dateTime, 'Y-m-d H:i:s');
            unset($this->data['Igpm']['data-hora']);
		
			$valorFormatado = r(",", ".", $this->data['Igpm']['valor']);
			$this->data['Igpm']['valor'] = $valorFormatado;

			if ( $this->Igpm->save ( $this->data['Igpm'] ) ) {
				$this->Session->setFlash(__('O IGP-M foi salvo com sucesso', true), 'flash_sucesso');
				$this->redirect("/{$this->params['prefix']}/igpm");
			} else {
				$this->Session->setFlash(__('Ocorreu um erro ao salvar o IGP-M.', true), 'flash_erro');
			}
		} else 
			$this->data = $this->Igpm->read();

		if(!$this->data)
			$this->Session->setFlash('Igpm não existente');
		
		$dateTime =  $this->create_date_time_from_format('Y-m-d H:i', $this->data['Igpm']['data']);
        $this->data['Igpm']['data-hora'] = date_format($dateTime, 'd-m-Y H:i:s');
	}

}

?>