<script type="text/javascript" src="<?=$this->webroot?>metro/js/max/bootstrap/collapse.js"></script>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css?v=0.1">
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/utils.js?v=0.1"></script>
<style type="text/css">
    a > .accordion-toggle { text-decoration: none!important }
    h2 { text-align: center }
    .accordion-heading { text-align: center; font-size: 20px; border: 1px solid grey }
    tfoot > tr > td { border: 1px #ddd solid!important; } 
    tfoot > tr > td > label{ cursor: default!important } 
</style>
<script type="text/javascript">
    $(document).ready(function(){
        $('#collapse').collapse({
            toggle: false
        });
        
        $('.accordion-body').collapse('show');
        
        $('#mostrar').click(function(){
            $('.accordion-body').collapse('show');
            $('#mostrar').fadeOut('fast');
            $('#esconder').fadeIn('slow');
        });
        
        $('#esconder').click(function(){
            $('.accordion-body').collapse('hide');
            $('#esconder').fadeOut('fast');
            $('#mostrar').fadeIn('slow');
        });
        
        $('.accordion-toggle').on('show', function (e){
           atualizaAltura($($(e.target)).height()+$(this).height());
        });
    });
</script>
<div class="row-fluid">
    <h2>
        <i class="icon-calendar-2"></i> Checkout Por Data
    </h2>
</div>
<br />
<br />
<div class="row-fluid">
    <label id="mostrar" class="fg-color-greenDark hide"><i class="icon-arrow-down-7"></i>&nbspEXPANDIR TODAS AS DATAS</label>
    <label id="esconder" class="fg-color-red"><i class="icon-arrow-up-7"></i>&nbspESCONDER TODAS AS DATAS</label>
    <div class="accordion" id="accordion2">
    <?php $totais = array("formandos" => 0, "mesas" => 0, "convites" => 0, "meias" => 0); ?>
    <?php foreach($relatorio as $i => $rel) : ?>
      <div class="accordion-group">
        <div class="accordion-heading">
          <a class="accordion-toggle fg-color-red" data-toggle="collapse" data-parent="#accordion2" href="#collapse<?=$i?>">
            <?php echo date("d/m/Y", strtotime($i)); ?>
          </a>
        </div>
        <div id="collapse<?=$i?>" class="accordion-body collapse">
          <div class="accordion-inner">
            <table class="table table-condensed table-striped">
                <thead>
                    <tr>
                        <th scope="col" width="10%">Cód. Formando</th>
                        <th scope="col" width="30%">Nome</th>
                        <th scope="col" width="10%">Mesas</th>
                        <th scope="col" width="10%">Convites</th>
                        <th scope="col" width="10%">Meias</th>
                        <th scope="col" width="15%">Atendente</th>
                        <th scope="col" width="15%">Data</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $formandos = 0; $mesas = 0; $convites = 0; $meias = 0; ?>
                    <?php foreach($rel as $r):?>
                    <?php 
                        $formandos++;
                        $mesas += $r['total_mesas'];
                        $convites += $r['total_convites'];
                        $meias += $r['total_meias'];
                    ?>
                    <tr>
                        <td colspan="1"><?php echo $r['codigo_formando'];?></td>
                        <td colspan="1"><?php echo $r['nome'];?></td>
                        <td colspan="1"><?php echo $r['total_mesas']?></td>
                        <td colspan="1"><?php echo $r['total_convites']?></td>
                        <td colspan="1"><?php echo $r['total_meias']?></td>
                        <td colspan="1"><?php echo $r['atendente']?></td>
                        <td colspan="1"><?php echo $r['data']?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="1" class="paginacao">
                            <label class="fg-color-red"><b>Totais</b></label>
                        </td>
                        <td colspan="1">
                            <label class="fg-color-greenDark"><b><?=$formandos?></b></label>
                        </td>
                        <td colspan="1" class="paginacao">
                            <label class="fg-color-greenDark"><b><?=$mesas?></b></label>
                        </td>
                        <td colspan="1" class="paginacao">
                            <label class="fg-color-greenDark"><b><?=$convites?></b></label>
                        </td>
                        <td colspan="1" class="paginacao">
                            <label class="fg-color-greenDark"><b><?=$meias?></b></label>
                        </td>
                        <td colspan="2">
                            &nbsp
                        </td>
                    </tr>
                </tfoot>
                <?php 
                    $totais['formandos'] += $formandos;
                    $totais['mesas'] += $mesas;
                    $totais['convites'] += $convites;
                    $totais['meias'] += $meias;
                ?>
            </table>
          </div>
        </div>
      </div>
    <?php endforeach; ?>
        <div class="accordion-group">
            <div class="accordion-heading">
              <a class="accordion-toggle fg-color-red" data-toggle="collapse" data-parent="#accordion2" href="#collapse<?=date("d-m-Y")?>">
                Totais</a>
            </div>
            <div id="collapse<?=date("d-m-Y")?>" class="accordion-body collapse">
                <div class="accordion-inner">
                    <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th scope="col" width="25%">Formandos</th>
                            <th scope="col" width="25%">Mesas</th>
                            <th scope="col" width="25%">Convites</th>
                            <th scope="col" width="25%">Meias</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="1"><?php echo $totais['formandos'];?></td>
                            <td colspan="1"><?php echo $totais['mesas'];?></td>
                            <td colspan="1"><?php echo $totais['convites']?></td>
                            <td colspan="1"><?php echo $totais['meias']?></td>
                        </tr>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>