<?php

class FotoPerfilShell extends Shell {

    var $uses = array('Usuario');
    var $FotoPerfil = false;
    
    private function carregar() {
        App::import('Component', 'FotoPerfil');
        $this->FotoPerfil = new FotoPerfilComponent();
    }
    
    function validarArquivos() {
        $this->Usuario->unbindModelAll();
        $usuarios = $this->Usuario->find('all',array(
            'conditions' => array(
                'ativo' => 1,
                "diretorio_foto_perfil is not null",
                "diretorio_foto_perfil <> ''"
            )
        ));
        $arquivos = 0;
        foreach($usuarios as $usuario) {
            $diretorioFotoPerfil = APP."webroot/{$usuario['Usuario']['diretorio_foto_perfil']}";
            if(file_exists($diretorioFotoPerfil)) {
                $this->out("usuario existe {$usuario['Usuario']['id']}");
                $arquivos++;
            } else {
                $this->out("usuario nao existe {$usuario['Usuario']['id']}");
            }
        }
        $this->out("$arquivos arquivos existem no servidor");
    }
    
    function validarUsuarios() {
        ini_set('memory_limit', '1024M');
        set_time_limit(0);
        $this->Usuario->unbindModelAll();
        $usuarios = $this->Usuario->find('all',array(
            'conditions' => array(
                'ativo' => 1,
                "diretorio_foto_perfil is not null",
                "diretorio_foto_perfil <> ''"
            )
        ));
        $this->carregar();
        $this->FotoPerfil->criarPastas();
        foreach($usuarios as $usuario) {
            $diretorioFotoPerfil = APP."webroot/{$usuario['Usuario']['diretorio_foto_perfil']}";
            if(file_exists($diretorioFotoPerfil)) {
                $extensaoReal = $this->FotoPerfil->obterExtensaoDaImagem($diretorioFotoPerfil);
                if($extensaoReal) {
                    list($arquivo,$extensaoAtual) = $this->FotoPerfil->obterInfoDoArquivo($diretorioFotoPerfil);
                    if($this->FotoPerfil->criarArquivos($diretorioFotoPerfil,$arquivo,$extensaoReal)) {
                        if($extensaoAtual != $extensaoReal) {
                            //$this->FotoPerfil->removerArquivosAdicionais($arquivo, $extensaoAtual);
                            unlink($diretorioFotoPerfil);
                            $dir = str_replace($extensaoAtual, $extensaoReal, $usuario['Usuario']['diretorio_foto_perfil']);
                            $this->Usuario->id = $usuario['Usuario']['id'];
                            $this->Usuario->saveField('diretorio_foto_perfil',$dir);
                        }
                    }
                }
            } else {
                $this->Usuario->id = $usuario['Usuario']['id'];
                $this->Usuario->saveField('diretorio_foto_perfil',null);
            }
        }
    }
    
    function import() {
        $pathSrc = "/home/thweb/sistema/app/webroot/";
        $pathDst = APP . "webroot/";
        $this->Usuario->unbindModelAll();
        $formandos = $this->Usuario->find('all',array(
            'conditions' => array(
                'ativo' => 1,
                "diretorio_foto_perfil is not null",
                "diretorio_foto_perfil <> ''",
                'grupo' => array('comissao','formando')
            )
        ));
        $r = array('copiados' => 0, 'nao_copiados' => 0, 'existentes' => 0, 'inexistentes' => 0);
        foreach($formandos as $formando) {
            if(!file_exists($pathDst.$formando['Usuario']['diretorio_foto_perfil'])) {
                if(file_exists($pathSrc.$formando['Usuario']['diretorio_foto_perfil'])) {
                    if(copy($pathSrc.$formando['Usuario']['diretorio_foto_perfil'], $pathDst.$formando['Usuario']['diretorio_foto_perfil'])) {
                        $r['copiados']++;
                        continue;
                    } else {
                        $r['nao_copiados']++;
                    }
                } else {
                    $r['inexistentes']++;
                }
            } else {
                $r['existentes']++;
                continue;
            }
            $this->Usuario->id = $formando['Usuario']['id'];
            $this->Usuario->saveField('diretorio_foto_perfil',null);
        }
        $this->out("{$r['copiados']} copiados");
        $this->out("{$r['nao_copiados']} nao copiados");
        $this->out("{$r['existentes']} ja existem");
        $this->out("{$r['inexistentes']} nao existem");
    }
}