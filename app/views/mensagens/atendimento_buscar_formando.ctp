<script type="text/javascript">
$(document).ready(function() {
    var formando = false;
    $("#codigo-formando-mensagem").focus();
    $("#buscar-formando-mensagem").click(function() {
        var resp = $(".resp-mensagem"),
            enviar = $("#confirmar-formando-mensagem");
        enviar.hide();
        if($("#codigo-formando-mensagem").val() == "") {
            resp.text("Digite o código do formando");
        } else {
            var dados = {
                data : {
                    codigo_formando : $("#codigo-formando-mensagem").val()
                }
            };
            resp.text("Aguarde");
            $.ajax({
                url : "<?=$this->here?>",
                type : "POST",
                dataType : "json",
                data : dados,
                success : function(response) {
                    formando = response.formando;
                    if(!response.formando) {
                        if(response.mensagem != "")
                            resp.text(response.mensagem);
                        else
                            resp.text("Erro ao consultar os dados. Tente novamente");
                        $("#codigo-formando-mensagem").focus();
                    } else {
                        resp.text(response.formando.ViewFormandos.nome);
                        enviar.show();
                    }
                },
                error : function() {
                    resp.text("Erro ao consultar os dados. Tente novamente");
                }
            });
        }
    });
    $("#confirmar-formando-mensagem").click(function() {
        if(formando.ViewFormandos != undefined) {
            $(".modal-body").html('<h2>Carregando</h2>');
            $(".modal-body").load("/atendimento/mensagens/enviar/"+formando.ViewFormandos.id);
        }
    });
});
</script>
<h2>Digite o c&oacute;digo do formando</h2>
<br />
<div class="row-fluid">
    <div class="span3">
        <?=$form->input('codigo_formando',array(
            'type' => 'text',
            'id' => 'codigo-formando-mensagem',
            'label' => false,
            'placeholder' => "COD",
            'div' => 'input-control text')); ?>
    </div>
    <div class="span3">
        <button type="button" id="buscar-formando-mensagem" class="default">Buscar</button>
    </div>
</div>
<span class="strong fg-color-red resp-mensagem"></span>
<br />
<br />
<button type="button" class="bg-color-blueDark hide" id="confirmar-formando-mensagem">
    Escrever Mensagem
</button>