<?=$html->css('uniform.default')?>
<?=$html->css('chosen')?>
<?=$html->script('jquery.uniform')?>
<?=$html->script('chosen.jquery')?>
<?=$html->script('ajax-chosen')?>
<style type="text/css">
#nav { margin-bottom:0; margin-top:20px; font-size:12px }
#nav ul,ol { margin-bottom:0 }
#nav li a { color:#333333 }
#nav li a i { margin-top:-1px; margin-right:2px }
#nav li a:hover { color:#333333; }
#nav li.active a { font-weight:bold; /*background-color: whiteSmoke*/ }
.tab-mensagens { border:solid 1px #DDDDDD; border-top:none }
.collapse { float:left; width:100%; }
.accordion .panel { position:relative; float:left; display:block; width:100% }
.accordion .panel .load { width:26px; margin:0 auto; padding:10px 0 }
.accordion-group { position:relative; float:left; width:100%; margin:0;
	border:none; border-top:solid 1px #DDDDDD }
.accordion-group:nth-child(even) { background-color:#F7F7F7 }
.accordion-inner { padding:9px 6px; background-color:white; /*background-image:url("<?=$this->webroot?>img/8x8.png")*/ }
.accordion-inner .load { width:26px; margin-left:60px }
.accordion-inner .alert { margin:0 }
.accordion-inner .alert h4 { font-size:16px; margin-bottom:5px }
.accordion-inner .alert-block { padding:10px; padding-right:35px }
.tm { position:relative; float:left; display:block; width:100%; margin-top:0px }
.hm { position:relative; }
.hm .c, .tm .c { float:left; position:relative; padding:6px 0; line-height:19px; }
.tm .c { padding:12px 0; }
.hm .c div, .tm .c div { margin:0 }
.tm .actions { position:relative; width:90%; display:none; margin-left:40px; }
.tm .actions .load { display:none; width:22px }
.tm .subactions { position:relative; width:90%; display:none; margin-left:70px; padding:0; padding-bottom:8px }
.tm .c input { padding:2px 4px; font-size:12px }
.tm .c .checker { margin-top:5px; height:27px; }
.check, .favorite { width:30px; text-align:center; cursor:pointer }
.favorite:before { position:relative; content:''; display:block; width:16px; height:16px; top:1px;
	left:7px; background-image:url("<?=$this->webroot?>images/stars_min.png"); background-position:0px 32px }
.favorite:hover:before { background-position:0px 16px }
.favorite.off:before { background-position:0px 0px }
.favorite.off:hover:before { background-position:0px 48px }
.hm .title { position:relative; width:90%; font-size:12px; margin-top:1px }
.hm .title.nao-lida { font-weight:bold; color:#D10A0F }
.hm .title.nao-lida .t:not(.data):hover { color:#780E00 }
.hm .title .t { float:left; padding:0; cursor:pointer }
.hm .title .nome { width:18%; margin-left:10px; }
.hm .title .assunto { width:67% }
.hm .title .data { width:13%; color:#575756; text-align:right; font-weight:normal }
.btn-action {
	background-color: whiteSmoke;
	background-image: -webkit-linear-gradient(top,whiteSmoke,#F1F1F1);
	background-image: -moz-linear-gradient(top,whiteSmoke,#F1F1F1);
	background-image: -ms-linear-gradient(top,whiteSmoke,#F1F1F1);
	background-image: -o-linear-gradient(top,whiteSmoke,#F1F1F1);
	background-image: linear-gradient(top,whiteSmoke,#F1F1F1);
	color: #444;
	border: 1px solid gainsboro;
	border: 1px solid rgba(0, 0, 0, 0.1);
	-webkit-border-radius: 2px!important;
	-moz-border-radius: 2px!important;
	border-radius: 2px!important;
	cursor: pointer;
	font-size: 11px;
	font-weight: bold;
	text-align: center;
	white-space: nowrap;
	margin-right: 16px;
	height: 27px;
	line-height:25px;
	min-width: 54px;
	outline: 0;
	padding: 0 8px;
}
.btn-action .icon { margin-top:0; width:19px }
.texto { margin:10px; background:white; border-radius:4px; -webkit-box-shadow: 0px 0px 18px rgba(184,184,184,.8);
-moz-box-shadow:    0px 0px 18px rgba(184,184,184,.8);
box-shadow:         0px 0px 18px rgba(184,184,184,.8);  }
.texto div { display:block; width:100%; padding:10px }
.texto .info { font-style:italic; line-height:20px }
.texto .corpo p,
.texto .corpo span { margin:0; padding:0 }
.texto .anexos .btn-action {
	display:inline-block;
	margin-right:10px;
	height:22px;
	line-height:20px;
	min-width:44px;
	outline: 0;
	padding: 0 6px;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		function decode(s){
 			return $("<div/>").html(s).text();
 		}

 		function mensagens(tipo) {
 			var url = "/mensagens/listar/" + tipo;
 			var container = $(".accordion[dir='" + tipo + "']").children(".panel");
 			container.html("<div class='load'><img src='<?=$this->webroot?>img/load.gif' /></div>");
 			var divError = $("<div>",{
				class:'alert alert-block alert-error fade in',
				html:'<h4 class="alert-heading">Erro</h4>'
			});
 			$.ajax({
	 	 		url : url,
	 	 		dataType : "json",
	 	 		success : function(dados) {
		 	 		if(dados.error) {
		 	 			container.html(divError.append(response.message.join("<br />")));
		 	 		} else {
			 	 		container.html("");
			 	 		if(dados.mensagens.length > 0) {
			 	 			$.each(dados.mensagens,function(i,mensagem) {
				 	 			data = mensagem.Mensagem.data.split(" ")[0].split("-").reverse().join("/");
				 	 			html = '<div class="accordion-group" dir="' + mensagem.Mensagem.id + '">' +
									'<div class="accordion-heading hm"><div class="c check">' +
									'<input type="checkbox" class="uniform checkable" id="check-message-' +
									mensagem.Mensagem.id + '" /></div><div class="c favorite ' +
									(mensagem.MensagemUsuario.favorita == "0" ? "off" : "")  +
									'"></div><div class="c title ' + (mensagem.MensagemUsuario.lida == "0" ? "nao-lida" : "") +
									'"><div class="t nome" data-toggle="collapse" data-parent="#accordion-' + tipo + '" ' +
									'href="#collapse-' + tipo + '-' + mensagem.Mensagem.id + '">' +
									mensagem.Mensagem.Usuario.nome + '</div><div class="t accordion-toggle assunto" data-toggle="collapse" ' +
									'data-parent="#accordion-' + tipo + '" href="#collapse-' + tipo + '-' + mensagem.Mensagem.id + '">' +
									mensagem.Mensagem.Assunto.Item.nome + ' - ' + mensagem.Mensagem.Assunto.nome + '</div>' +
									'<div class="t data" data-toggle="collapse" data-parent="#accordion-' + tipo + '" ' +
									'href="#collapse-' + tipo + '-' + mensagem.Mensagem.id + '">' + data +
									'</div></div></div><div id="collapse-' + tipo + '-' + mensagem.Mensagem.id +
									'" class="accordion-body collapse"><div class="accordion-inner">' +
									decode(mensagem.Mensagem.texto) + '</div></div></div>';
								container.append(html);
			 	 			});
			 	 			$(".uniform:not(.no-uniform)").uniform();
			 	 		} else {
			 	 			container.html(divError.append("<p>Nenhuma mensagem encontrada.</p>"));
			 	 		}
		 	 		}
	 	 		},
	 	 		error : function() {
	 	 			container.html(divError.append("<p>Erro ao buscar servidor. Por favor tente mais tarde.</p>"));
				}
	 	 	});
 		}
 		mensagens("mensagens");
 		var alert = $("<div>",{
			class:'alert alert-block alert-error fade in',
			html:'<h4 class="alert-heading">Erro</h4>'
		});
 		function actions() {
 	 		if($(".checkable:visible:not(.all):checked").length > 0) {
 	 	 		var checados = $(".checkable:visible:not(.all):checked").length;
 	 			var lida,naoLida,marcarFavorita,desmarcarFavorita;
 	 			$(".checkable:visible:not(.all):checked").each(function() {
 	 				title = $(this).parents(".accordion-group").children(".hm").children('.title');
 	 				f = $(this).parents(".accordion-group").children(".hm").children('.favorite');
 	 				lida = lida !== true ? (title.hasClass("nao-lida")) : true;
 	 				naoLida = naoLida !== true ? (!title.hasClass("nao-lida")) : true;
 	 				marcarFavorita = marcarFavorita !== true ? (f.hasClass("off")) : true;
 	 				desmarcarFavorita = desmarcarFavorita !== true ? (!f.hasClass("off")) : true;
 	 			});
 	 			lida ? $(".tm > .actions > .lida").show() : $(".tm > .actions > .lida").hide();
 	 			naoLida ? $(".tm > .actions > .nao-lida").show() : $(".tm > .actions > .nao-lida").hide();
 	 			marcarFavorita ? $(".tm > .actions > .favorita.marcar").show() : $(".tm > .actions > .favorita.marcar").hide();
 	 			desmarcarFavorita ? $(".tm > .actions > .favorita.desmarcar").show() : $(".tm > .actions > .favorita.desmarcar").hide();
 	 			checados == 1 ? $(".tm > .actions > .responder").hide() : $(".tm > .actions > .responder").hide();
 	 			$(".tm:visible > .actions").show();
 	 		} else {
 	 	 		$(".tm:visible > .actions,.tm:visible > .subactions").hide();
 	 		}
 	 		$(".tm:visible > .actions > .load").hide();
 		}
		$(".uniform").uniform();
		$('a[data-toggle="tab"]').on('shown', function (e) {
			mensagens(e.target.hash.replace("#",""));
			actions();
		});
		$(".checkable").live('click',function() {
			if($(this).hasClass("all")) {
				if($(".checkable:visible:not(.all):checked").length != $(".checkable:visible:not(.all)").length)
					$(".checkable:visible").attr("checked",true);
				else
					$(".checkable:visible").attr("checked",false);
			} else {
				if($(".checkable:visible:not(.all):checked").length != $(".checkable:visible:not(.all)").length)
					$(".checkable:visible.all").attr("checked",false);
				else
					$(".checkable:visible.all").attr("checked",true);
			}
			$.uniform.update();
			actions();
		});
		$(".encaminhar").live('click',function() {
			$(".tm:visible > .subactions").fadeIn(500,function() {
				var url = "/mensagens/listar_usuarios";
				if(!$(".tm:visible > .subactions > .encaminhar").is(":visible")) {
					$(".tm:visible > .subactions > div:not(.encaminhar)").fadeOut(500,function() {
						$('#input-encaminhar').typeahead({
							source : function(typehead,query) {
								console.log(query);
							}
						});
					});
				} else {
					$(".tm:visible .subactions .encaminhar .select-encaminhar:not(.criado)").ajaxChosen({
						type : 'POST',
						url : url,
						dataType : 'json',
						minTermLength : 6,
					}, function(response) {
						var results = {};
						$.each(response.usuarios,function(i,usuario) {
							results[usuario.ViewFormandos.id] = usuario.ViewFormandos.nome;
						});
						return results;
					});
				}
			});
		});
		$(".favorite").live('click',function() {
			var that = this;
			var mensagem = $(that).parents(".accordion-group").attr("dir");
			var favorita = $(that).hasClass("off");
			var url = "/mensagens/marcar_favorita";
			$.ajax({
	 	 		url : url,
	 	 		data : { favorita : favorita, mensagem : mensagem },
	 	 		type : "POST",
	 	 		dataType : "json",
	 	 		success : function(response) {
	 	 			if(!response.error)
		 	 			$(that).toggleClass("off");
	 	 		},
	 	 		complete : function() { actions(); }
	 	 	});
		});
		$(".favorita").live('click',function() {
			var bt = $(this);
			$(".tm:visible > .actions > .load").show();
			var checadas = $(".checkable:visible:not(.all):checked").length;
			setTimeout(function() {
				$(".checkable:visible:not(.all):checked").each(function() {
					var that = this;
					var group = $(that).parents(".accordion-group");
					var mensagem = group.attr("dir");
					var favorita = bt.hasClass("marcar");
					var url = "/mensagens/marcar_favorita";
					var marcacao = group.children(".hm").children('.favorite').hasClass("off");
					if(favorita == marcacao) {
						$.ajax({
				 	 		url : url,
				 	 		data : { favorita : favorita, mensagem : mensagem },
				 	 		type : "POST",
				 	 		dataType : "json",
				 	 		success : function(response) {
				 	 			if(!response.error) {
				 	 				if(bt.hasClass("marcar"))
				 	 					group.children(".hm").children(".favorite").removeClass("off");
					 	 			else
					 	 				group.children(".hm").children(".favorite").addClass("off");
				 	 			}
				 	 		},
				 	 		complete : function(response) {
				 	 			checadas == 1 ? actions() : checadas--;
							}
				 	 	});
					} else {
						checadas == 1 ? actions() : checadas--;
					}
				});
			},500);
		});
		$("button.leitura").live('click',function() {
			var bt = $(this);
			var checadas = $(".checkable:visible:not(.all):checked").length;
			$(".tm:visible > .actions > .load").show();
			setTimeout(function() {
				$(".checkable:visible:not(.all):checked").each(function() {
					var that = this;
					var group = $(that).parents(".accordion-group");
					var mensagem = group.attr("dir");
					var lida = !bt.hasClass("nao-lida");
					var marcacao = group.children(".hm").children('.title').hasClass("nao-lida");
					var url = "/mensagens/marcar_leitura";
					if(lida == marcacao) {
						$.ajax({
				 	 		url : url,
				 	 		data : { mensagem : mensagem, lida : lida },
				 	 		type : "POST",
				 	 		dataType : "json",
				 	 		success : function(response) {
				 	 			if(!response.error) {
					 	 			if(!lida)
				 	 					group.children(".hm").children(".title").addClass("nao-lida");
					 	 			else
					 	 				group.children(".hm").children(".title").removeClass("nao-lida");
				 	 			}
				 	 		},
				 	 		complete : function(response) {
				 	 			checadas == 1 ? actions() : checadas--;
							}
				 	 	});
					} else {
						checadas == 1 ? actions() : checadas--;
					}
				});
			},500);
		});
		$(".apagar").live('click',function(e) {
			e.preventDefault();
			var qtde = $(".checkable:visible:not(.all):checked").length;
			var mensagem = "Tem certeza que deseja mover " + qtde;
			mensagem+= qtde > 1 ? " mensagens " : " mensagem ";
			mensagem+= "para a lixeira?";
			bootbox.dialog(mensagem,[{
				"label" : "Cancelar",
				"class" : "btn-danger",
				"callback" : function() {
					return true;
				}
			},{
				"label" : "Confirmar",
                "class" : "btn-primary",
                "callback" : function() {
                    $(".checkable:visible:not(.all):checked").each(function() {
                    	var that = this;
    					var group = $(that).parents(".accordion-group");
    					var mensagem = group.attr("dir");
    					var url = "/mensagens/descartar";
    					$.ajax({
				 	 		url : url,
				 	 		data : { mensagem : mensagem },
				 	 		type : "POST",
				 	 		dataType : "json",
				 	 		success : function(response) {
				 	 			if(!response.error)
					 	 			group.remove();
				 	 		},
				 	 		complete : function() { actions(); }
				 	 	});
                    });
					bootbox.hideAll();
                }
			}]);
		});
		var anexos = $("<div/>",{
			class : "anexos"
		});
		$(".collapse").live("show",function() {
			var that = this;
			var title = $(that).prev().children('.title');
			var mensagem = $(that).parents(".accordion-group").attr("dir");
			var url = "/mensagens/exibir";
			var container = $(that).children(".accordion-inner");
			var divLoad = "<div class='load'><img src='<?=$this->webroot?>img/load.gif' /></div>";
			container.html(divLoad);
	 	 	$.ajax({
	 	 		url : url,
	 	 		data : { mensagem : mensagem },
	 	 		type : "POST",
	 	 		dataType : "json",
	 	 		success : function(response) {
	 	 			if(response.error) {
		 	 			container.html(alert.clone().append(response.message.join("<br />")));
	 	 			} else {
		 	 			var hora = response.mensagem.Mensagem.data.split(" ")[1].split(":");
		 	 			hora.pop();
		 	 			var texto = $("<div>",{
			 	 			html:"<div class='info'>Enviada &agrave;s "+hora.join(":")+"<br />" +
			 	 					"Por " + response.mensagem.Usuario.nome + "</div>" +
		 	 						"<div class='corpo'>" + decode(response.mensagem.Mensagem.texto) +
		 	 						"</div>",
			 	 			class:"texto"
				 	 	});
				 	 	texto.append(anexos.clone());
	 	 				if(response.mensagem.Arquivo.length > 0) {
		 	 				/*
	 	 					if(response.mensagem.Arquivo.length > 1)
	 	 						texto.children(".anexos").append(
					 	 			"<a class='btn-action' href='<?="{$this->webroot}{$this->params['prefix']}"?>/arquivos/baixar/" +
					 	 			"todos/" + response.mensagem.Mensagem.id + "' target='_blank'>" +
					 	 			"<i class='icon icon-download-alt'></i>Baixar Todos</a>");
					 	 	*/
		 	 				$.each(response.mensagem.Arquivo,function(i,arquivo) {
			 	 				texto.children(".anexos").append(
			 	 					"<a class='btn-action' href='<?="{$this->webroot}{$this->params['prefix']}"?>/arquivos/baixar/" +
					 	 			arquivo.id + "' target='_blank'><i class='icon icon-download'></i>"+ arquivo.nome+"</a>");
		 	 				});
	 	 				} else {
	 	 					texto.children(".anexos").append("<span class='text-info'>Nenhum arquivo anexado</span>");
	 	 				}
		 	 			container.html(texto);
		 	 			title.removeClass("nao-lida");
		 	 			actions();
	 	 			}
	 	 		},
	 	 		error : function() {
 	 	 			container.html(divError.append("<p>Erro ao buscar servidor. Por favor tente mais tarde.</p>"));
	 	 		}
	 	 	});
		});
	});
</script>
<div class="row-fluid">
	<div class="titulo-header span12">Mensagens</div>
</div>
<div class="br"></div>
<ul class="nav nav-tabs" id="nav">
	<?php foreach($tipos as $tipo) : ?>
	<li class="<?=$tipo['active'] ? "active" : ""?>">
		<a href="<?="#{$tipo['id']}"?>" data-toggle="tab">
			<?php if(isset($tipo['icon'])) echo "<i class='icon {$tipo['icon']}'></i>"?>
			<?=$tipo['titulo']?>
		</a>
	</li>
	<?php endforeach; ?>
</ul>
<div class="tab-content tab-mensagens" id="tab">
	<?php foreach($tipos as $tipo) : ?>
	<div class="tab-pane <?=$tipo['active'] ? "active" : ""?>" id="<?=$tipo['id']?>">
		<div class="accordion" dir="<?=$tipo['id']?>">
			<div class="tm">
				<div class="c check">
					<input type="checkbox" class="uniform checkable all no-uniform" id="all" />
				</div>
				<div class="c actions">
					<button class="btn-action responder"><i class="icon icon-arrow-right"></i>Responder</button>
					<button class="btn-action encaminhar"><i class="icon icon-share"></i>Encaminhar</button>
					<button class="btn-action leitura lida">Lida</button>
					<button class="btn-action leitura nao-lida">N&atilde;o Lida</button>
					<button class="btn-action favorita marcar"><i class="icon icon-star"></i>Estrela</button>
					<button class="btn-action favorita desmarcar"><i class="icon icon-star-empty"></i>Remover Estrela</button>
					<button class="btn-action apagar"><i class="icon icon-trash"></i>Lixeira</button>
					<img src="<?=$this->webroot?>img/load.gif" class="load" />
				</div>
				<div class="c subactions">
					<div class="encaminhar">
						<select data-placeholder="Selecione um formando" multiple class="select-encaminhar">
						</select>
					</div>
				</div>
			</div>
			<div class="panel" id="accordion-<?=$tipo['id']?>">
			</div>
		</div>
	</div>
	<?php endforeach; ?>
</div>
<div class="br"></div>