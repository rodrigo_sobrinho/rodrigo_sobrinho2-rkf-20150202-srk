<?php

class IgpmShell extends Shell {

    var $uses = array('Turma','ViewFormandos','Despesa','DespesaPagamento','Igpm',
        'IgpmAplicacao','IgpmTurmas', 'IgpmTurmasUsuarios','IgpmAtualizacao');
    var $helpers = array('Html');
    var $relatorio = "";

    function turmas() {
        if ($this->args)
            $this->main($this->args);
    }

    function main($turma = false) {
        $this->aumentarMemoria();
        App::import('Component', 'Mailer');
        $mailer = new MailerComponent();
        $this->Turma->recursive = -1;
        $beta = false;
        $this->IgpmAplicacao->create();
        $this->IgpmAplicacao->save(array(
            'data_aplicacao' => date('Y-m-d H:i:s')
        ));
        $log = "";
        $igpmAplicacao = $this->IgpmAplicacao->getLastInsertId();
        $this->Turma->begin();
        $this->Despesa->begin();
        $this->IgpmTurmas->begin();
        $this->IgpmTurmasUsuarios->begin();
        $turmas = $this->obterTurmas($turma);
        $turmasAfetadas = 0;
        foreach ($turmas as $turma) {
            $this->relatorio.= "turma {$turma['Turma']['id']}\n\n";
            if (!empty($turma['Turma']['data_assinatura_contrato']) && $this->validarData($turma['Turma']['data_assinatura_contrato'])) {
                $dataContrato = $turma['Turma']['data_assinatura_contrato'];
                if (!empty($turma['Turma']['data_igpm']) && $this->validarData($turma['Turma']['data_igpm']))
                    $dataBase = explode('-', $turma['Turma']['data_igpm']);
                else
                    $dataBase = explode('-', $dataContrato);
                $dataBase[0]++;
                for ($dataBase[0]; $dataBase[0] . $dataBase[1] . $dataBase[2] <= date('Ymd'); $dataBase[0]++) {
                    $this->relatorio.= "igpm base {$dataBase[1]}-{$dataBase[0]}\n";
                    $igpm = $this->obterIGPM($dataBase[0], date('m', strtotime($dataContrato)));
                    if ($igpm) {
                        $this->IgpmTurmas->create();
                        $this->IgpmTurmas->save(array(
                            'igpm_id' => $igpm['Igpm']['id'],
                            'igpm_aplicacao_id' => $igpmAplicacao,
                            'turma_id' => $turma['Turma']['id'],
                            'valor' => $igpm['Igpm']['valor'],
                            'data_igpm_anterior' => empty($turma['Turma']['data_igpm']) ? null : $turma['Turma']['data_igpm'],
                            'data_aplicacao' => date('Y-m-d H:i:s')));
                        $igpmTurma = $this->IgpmTurmas->getLastInsertId();
                        $options = array('order' => 'ViewFormandos.codigo_formando', 'group' =>
                            'ViewFormandos.id', 'conditions' =>
                            array('ViewFormandos.turma_id' => $turma['Turma']['id']/* ,'ViewFormandos.codigo_formando' => '3121028' */)
                        );
                        $options['joins'] = array(
                            array(
                                'table' => 'despesas',
                                'alias' => 'Despesa',
                                'type' => 'inner',
                                'conditions' => array('ViewFormandos.id = Despesa.usuario_id')
                            )
                        );
                        $formandos = $this->ViewFormandos->find('all', $options);
                        foreach ($formandos as $formando)
                            if ($this->formandoDeveTerIGPM($formando, $turma, $dataBase[0]))
                                $this->aplicarIGPMFormando($formando, $igpm, $turma, $igpmTurma, $dataBase[0]);
                            else
                                $this->relatorio.= "formando nao tem igpm no ano {$dataBase[0]}\n";
                        $dataTurma = date('Y-m-d',mktime(0,0,0,
                                date('m', strtotime($turma['Turma']['data_assinatura_contrato'])),
                                date('d', strtotime($turma['Turma']['data_assinatura_contrato'])),
                                $dataBase[0]));
                        //$dataTurma = $dataBase[0] . date('-m-d', strtotime($turma['Turma']['data_assinatura_contrato']));
                        $this->Turma->updateAll(array('Turma.data_igpm' => "'$dataTurma'"), array('Turma.id' => $turma['Turma']['id']));
                        $turma['Turma']['data_igpm'] = $this->obterDataIgpm($igpm, $turma);
                    }
                }
                $turmasAfetadas++;
            } else {
                $log.= "Turma {$turma['Turma']['id']} esta sem data de assinatura, ";
            }
        }
        $this->IgpmAplicacao->updateAll(
                array('turmas_afetadas' => $turmasAfetadas, 'log' => "'$log'"), array('id' => $igpmAplicacao));
        if ($beta) {
            $this->Turma->rollback();
            $this->Despesa->rollback();
            $this->IgpmTurmas->rollback();
            $this->IgpmTurmasUsuarios->rollback();
            echo $this->relatorio;
        } else {
            $this->Turma->commit();
            $this->Despesa->commit();
            $this->IgpmTurmas->commit();
            $this->IgpmTurmasUsuarios->commit();
        }
    }

    function desaplicar() {
        if ($this->args) {
            $error = true;
            $this->Turma->begin();
            $turmas = implode(",", $this->args);
            try {
                $q = "update turmas set data_igpm = null where id in($turmas);";
                $this->Turma->query($q);
                $q = "update despesas d, (select id from vw_formandos where turma_id in($turmas)) " .
                        "as usuarios set correcao_igpm = null, status_igpm = 'nao_virado' " .
                        "where usuario_id in(usuarios.id) and status_igpm <> 'pago';";
                $this->Turma->query($q);
                $q = "delete from despesas_pagamentos where despesa_id in(select id from despesas where " .
                        "tipo = 'igpm' and usuario_id in(select id from vw_formandos where turma_id in($turmas)));";
                $this->Turma->query($q);
                $q = "delete from despesas where tipo = 'igpm' and usuario_id in(select id from " .
                        "vw_formandos where turma_id in($turmas));";
                $this->Turma->query($q);
                $this->Turma->commit();
                echo "\nIGPM desaplicado com sucesso!\n";
            } catch (Exception $e) {
                $this->Turma->rollback();
                echo "\nErro ao desaplicar igpm";
                echo "\n";
                var_dump($e);
            }
        }
    }

    function obterIGPM($ano, $mes) {
        $igpm = array();
        while (empty($igpm)) {
            $data = @explode('-', date('m-Y', mktime(0, 0, 0, $mes, 1, $ano)));
            $igpm = $this->Igpm->find('first', array('conditions' => array('mes' => $data[0], 'ano' => $data[1])));
            $mes--;
        }
        return $igpm;
    }

    function obterTurmas($turma = false) {
        if (!$turma) {
            $dataAtual = explode('-', date('d-m-Y'));
            $conditions = array(
                'AND' => array(
                    "date_format(data_assinatura_contrato,'%m') = '{$dataAtual[1]}'",
                    "date_format(data_assinatura_contrato,'%d') = '{$dataAtual[0]}'",
                    "date_format(data_assinatura_contrato,'%Y') <= '{$dataAtual[2]}'",
                    'status' => 'fechada'
                )
            );
            $sql = "select * from turmas where (date_format(data_assinatura_contrato,'%m') = '{$dataAtual[1]}' and " .
                    "date_format(data_assinatura_contrato,'%d') = '{$dataAtual[0]}' and date_format(data_assinatura_contrato,'%Y') <= " .
                    "'{$dataAtual[2]}' and status = 'fechada') or (data_assinatura_contrato <= DATE_SUB(NOW(),INTERVAL 1 YEAR) and data_igpm " .
                    "is null)";
        } else {
            $conditions = array("id in(" . implode(",", $turma) . ")");
        }
        $turmas = $this->Turma->find('all', array('conditions' => $conditions));
        return $turmas;
    }

    function aplicarIGPMFormando($formando, $igpm, $turma, $igpmTurma, $anoBase) {
        $this->relatorio.= "formando tem igpm no ano {$igpm['Igpm']['ano']}\n";
        $this->IgpmTurmasUsuarios->create();
        $this->IgpmTurmasUsuarios->save(array(
            'igpm_turma_id' => $igpmTurma,
            'usuario_id' => $formando['ViewFormandos']['id'],
            'data_aplicacao' => date('Y-m-d H:i:s')
        ));
        $valorIGPM = (float) number_format($igpm['Igpm']['valor'], 2, '.', '');
        $this->Despesa->recursive = 2;
        $despesas = $this->Despesa->find('all', array('conditions' => array('usuario_id' => $formando['ViewFormandos']['id'], "status <> 'cancelada'", 'tipo' => 'adesao')));
        $timeLimite = mktime(0, 0, 0, date('m', strtotime($turma['Turma']['data_assinatura_contrato'])), date('d', strtotime($turma['Turma']['data_assinatura_contrato'])), $this->obterAnoBaseIGPM($turma, $anoBase));
        $despesasAfetadas = 0;
        foreach ($despesas as $despesa) {
            $dataVencimento = date('Ymd', strtotime($despesa['Despesa']['data_vencimento']));
            $dataPagamento = date('Ymd', strtotime($despesa['Despesa']['data_pagamento']));
            if ($despesa['Despesa']['status'] == "aberta") {
                $this->aplicarIGPMDespesaAberta($despesa, $valorIGPM);
                $despesasAfetadas++;
            } elseif (isset($despesa['DespesaPagamento'][0]['Pagamento']) && $despesa['Despesa']['status'] == "paga") {
                $this->relatorio.= "despesa {$despesa['Despesa']['parcela']} paga\n";
                $correcaoIGPM = empty($despesa['Despesa']['correcao_igpm']) ? 0 : $despesa['Despesa']['correcao_igpm'];
                if ($despesa['DespesaPagamento'][0]['Pagamento']['valor_pago'] < $despesa['DespesaPagamento'][0]['Pagamento']['valor_nominal'])
                    $valorPago = $despesa['DespesaPagamento'][0]['Pagamento']['valor_pago'];
                else
                    $valorPago = $despesa['DespesaPagamento'][0]['Pagamento']['valor_nominal'];
                $valorPago = $despesa['Despesa']['valor'] < $valorPago ? $despesa['Despesa']['valor'] : $valorPago;
                //$this->relatorio.= "data pagamento {$despesa['Despesa']['data_pagamento']}, data limite " . date('Y-m-d',$timeLimite) . "\n";
                if (strtotime($despesa['Despesa']['data_pagamento']) > $timeLimite) {
                    $this->relatorio.= "passou da data limite\n";
                    $this->relatorio.= "pagou em {$despesa['Despesa']['data_pagamento']} - limite " . date("Y-m-d H:i:s", $timeLimite) . "\n";
                    $this->relatorio.= "time pag " . strtotime($despesa['Despesa']['data_pagamento']) . " - time lim " . "$timeLimite\n";
                    $this->aplicarIGPMDespesaAberta($despesa, $valorIGPM);
                    $despesasAfetadas++;
                } elseif (($despesa['Despesa']['valor'] + $correcaoIGPM) > $valorPago && strtotime($despesa['Despesa']['data_vencimento']) > $timeLimite) {
                    $this->relatorio.= "nao pagou tudo\n";
                    $this->aplicarIGPMDespesaPaga($despesa, $valorIGPM, $valorPago);
                    $despesasAfetadas++;
                }
            }
        }
        $this->IgpmTurmasUsuarios->updateAll(
                array('despesas_afetadas' => $despesasAfetadas), array('id' => $this->IgpmTurmasUsuarios->getLastInsertId()));
    }

    function obterCorrecaoIGPMPorDespesa($despesa, $igpm) {
        $correcaoIGPM = empty($despesa['Despesa']['correcao_igpm']) ? 0 : $despesa['Despesa']['correcao_igpm'];
        $despesaComCorrecao = $despesa['Despesa']['valor'] + $correcaoIGPM;
        $novaCorrecao = ($despesaComCorrecao * $igpm / 100) + $correcaoIGPM;
        return number_format($novaCorrecao, 2, '.', '');
    }

    function aplicarIGPMDespesaPaga($despesa, $igpm, $valorPago) {
        $correcaoIGPM = empty($despesa['Despesa']['correcao_igpm']) ? 0 : $despesa['Despesa']['correcao_igpm'];
        $saldoDespesa = number_format(($despesa['Despesa']['valor'] + $correcaoIGPM) - $valorPago, 2, '.', '');
        $novaCorrecao = number_format(($saldoDespesa * $igpm / 100) + $correcaoIGPM, 2, '.', '');
        $update = array(
            'correcao_igpm' => $novaCorrecao,
            'status_igpm' => "'nao_pago'"
        );
        $this->Despesa->updateAll($update, array('Despesa.id' => $despesa['Despesa']['id']));
    }

    function aplicarIGPMDespesaAberta($despesa, $igpm) {
        $this->relatorio.= "despesa {$despesa['Despesa']['parcela']} aplicada\n";
        $update = array(
            'correcao_igpm' => $this->obterCorrecaoIGPMPorDespesa($despesa, $igpm),
            'status_igpm' => "'nao_pago'"
        );
        $this->Despesa->updateAll($update, array('Despesa.id' => $despesa['Despesa']['id']));
    }

    function aplicarIGPMParcelamento($parcelamento, $igpm) {
        $this->Parcelamento->updateAll(
                array('valor' => $this->obterParcelamentoCorrigido($parcelamento, $igpm)), array('Parcelamento.id' => $parcelamento['Parcelamento']['id']));
    }

    function obterDataIgpm($igpm, $turma) {
        $diasDoMesIGPM = date('d', strtotime($turma['Turma']['data_assinatura_contrato']));
        return "{$igpm['Igpm']['ano']}-{$igpm['Igpm']['mes']}-{$diasDoMesIGPM}";
    }

    function formandoDeveTerIGPM($formando, $turma, $anoBase) {
        $return = true;
        $anoUltimoParcelamento = $this->obterAnoBaseIGPM($turma, $anoBase);
        $dataUltimoParcelamento = $anoUltimoParcelamento . "-" . date('m-d', strtotime($turma['Turma']['data_assinatura_contrato']));
        $timeLimite = mktime(0, 0, 0, date('m', strtotime($dataUltimoParcelamento)), date('d', strtotime($dataUltimoParcelamento)), (int) date('Y', strtotime($dataUltimoParcelamento)) + 1);
        $primeiraParcela = $this->Despesa->find('first', array('conditions' =>
            array(
                'Despesa.usuario_id' => $formando['ViewFormandos']['id'],
                'Despesa.status not' => array('cancelada','renegociada'),
                'Despesa.parcela' => 1)));
        $timeParcela = strtotime($primeiraParcela['Despesa']['data_vencimento']);
        if (!$this->validarData($formando['ViewFormandos']['data_adesao']))
            $timeParcela = strtotime($formando['ViewFormandos']['data_adesao']);
        $this->relatorio.= "formando {$formando['ViewFormandos']['id']} - {$formando['ViewFormandos']['codigo_formando']}\n";
        $this->relatorio.= "data ultimo parcelamento {$dataUltimoParcelamento}, data primeira parcela {$primeiraParcela['Despesa']['data_vencimento']}\n";
        if ($timeParcela >= strtotime($dataUltimoParcelamento)/* && $timeParcela <= $timeLimite */)
            $return = false;
        return $return;
    }

    function obterAnoBaseIGPM($turma, $anoBase) {
        if ($anoBase > date('Y', strtotime($turma['Turma']['data_assinatura_contrato'])))
            return $anoBase;
        else
            return $anoBase;
    }

    function validarData($data) {
        return $data == date('Y-m-d', strtotime($data));
    }
    
    private function aumentarMemoria($m = '512M') {
        ini_set('memory_limit',$m);
        set_time_limit(0);
    }
    
    function atualizar_formandos() {
        $this->aumentarMemoria('2048M');
        $this->Despesa->recursive = 0;
        $usuarios = array();
        if ($this->args) {
            foreach($this->args as $id)
                $usuarios[$id] = array(
                    'soma' => 0,
                    'igpm' => $this->Despesa->find('first',
                        array('conditions' => array('tipo' => 'igpm','usuario_id' => $id))),
                    'despesas' => array()
                );
        } else {
            $aplicacoes = $this->IgpmTurmasUsuarios->find('all', array(
                'conditions' => array("despesas_afetadas >" => "0"),
                'group' => 'usuario_id'));
            foreach($aplicacoes as $a)
                $usuarios[$a['IgpmTurmasUsuarios']['usuario_id']] = array(
                    'soma' => 0,
                    'igpm' => $this->Despesa->find('first',
                        array('conditions' => array('tipo' => 'igpm',
                            'usuario_id' => $a['IgpmTurmasUsuarios']['usuario_id']))),
                    'despesas' => array()
                );
        }
        $despesasPagas = $this->Despesa->find('all',
            array('conditions' =>
                array(
                    'status' => 'paga',
                    'tipo' => 'adesao',
                    'correcao_igpm is not null',
                    "correcao_igpm >" => "0",
                    'Despesa.usuario_id' => array_keys($usuarios),
                )
            )
        );
        foreach($despesasPagas as $despesaPaga) {
            $despesaPaga['Pagamento'] = $this->obterPagamento($despesaPaga["Despesa"]['id']);
            $usuario = $despesaPaga["Despesa"]['usuario_id'];
            $valorCreditado = min(array(
                $despesaPaga['Pagamento']['valor_nominal'],
                $despesaPaga['Pagamento']['valor_pago'],
                //$despesaPaga['Despesa']['valor']
            ));
            $saldo = array_sum(array(
                $despesaPaga['Despesa']['valor'],
                $despesaPaga['Despesa']['correcao_igpm']
            ));
            $diferenca = $saldo - $valorCreditado;
            if(floor($diferenca) > 0) {
                $status = "recriado";
                $usuarios[$usuario]['soma']+= 
                        $despesaPaga['Despesa']['correcao_igpm'];
            } else {
                 $status = "pago";
            }
            $despesaPaga['Despesa']['status_igpm_antes'] =
                    $despesaPaga['Despesa']['status_igpm'];
            $despesaPaga['Despesa']['status_igpm'] = $status;
            $usuarios[$usuario]['despesas'][] = $despesaPaga['Despesa'];
        }
        foreach($usuarios as $id => $usuario) {
            $log = sprintf("%-4s;%-7s;%-10s;%-10s;%-10s;%-4s;%s;\n",
                    'Usu','Despe','Antes','Depois','Correcao','Erro','Obs');
            foreach($usuario['despesas'] as $despesa) {
                if($despesa['status_igpm_antes'] != $despesa['status_igpm']) {
                    $this->Despesa->begin();
                    if($this->Despesa->save(array('Despesa' => $despesa))) {
                        $this->Despesa->commit();
                        //$this->Despesa->rollback();
                        $log.= sprintf("%-4s;%-7s;%-10s;%-10s;%-10s;%-4s;%s;\n",
                                $id,
                                $despesa['parcela'],
                                $despesa['status_igpm_antes'],
                                $despesa['status_igpm'],
                                $despesa['correcao_igpm'],0,'');
                    } else {
                        $this->Despesa->rollback();
                        $log.= sprintf("%-4s;%-7s;%-10s;%-10s;%-10s;%-4s;%s;\n",
                                $id,$despesa['parcela'],0,0,0,1,'');
                    }
                }
            }
            if($usuario['igpm']) {
                if($usuario['igpm']['Despesa']['valor'] != $usuario['soma']) {
                    $antes = $usuario['igpm']['Despesa']['valor'];
                    $usuario['igpm']['Despesa']['valor'] = $usuario['soma'];
                    if($this->Despesa->save(array('Despesa' => $usuario['igpm']['Despesa']))) {
                        $this->Despesa->commit();
                        //$this->Despesa->rollback();
                        $log.= sprintf("%-4s;%-7s;%-10s;%-10s;%-10s;%-4s;%s;\n",
                            $id,'igpm',$antes,$usuario['soma'],0,0,'atualizada');
                    } else {
                        $this->Despesa->rollback();
                        $log.= sprintf("%-4s;%-7s;%-10s;%-10s;%-10s;%-4s;%s;\n",
                            $id,'igpm',0,$usuario['soma'],0,1,'');
                    }
                } else {
                    $log.= sprintf("%-4s;%-7s;%-10s;%-10s;%-10s;%-4s;%s;\n",
                            $id,
                            'igpm',
                            $usuario['igpm']['Despesa']['valor'],
                            $usuario['soma'],0,0,
                            'Valor nao modificou');
                }
            } else {
                $despesaIgpm = array(
                    'valor' => $usuario['soma'],
                    'parcela' => '0',
                    'total_parcelas' => '0',
                    'tipo' => 'igpm',
                    'usuario_id' => $id,
                    'data_cadastro' => date('Y-m-d H:i:s')
                );
                $this->Despesa->begin();
                $this->Despesa->create();
                if($this->Despesa->save(array('Despesa' => $despesaIgpm))) {
                    $this->Despesa->commit();
                    //$this->Despesa->rollback();
                    $log.= sprintf("%-4s;%-7s;%-10s;%-10s;%-10s;%-4s;%s;\n",
                        $id,'igpm',0,$usuario['soma'],0,0,'nao existia');
                } else {
                    $this->Despesa->rollback();
                    $log.= sprintf("%-4s;%-7s;%-10s;%-10s;%-10s;%-4s;%s;\n",
                        $id,'igpm',0,$usuario['soma'],0,1,'');
                }
            }
            $l = array('IgpmAtualizacao' => array(
                'usuario_id' => $id,
                'log' => $log,
                'data_cadastro' => date('Y-m-d H:i:s')
            ));
            $this->IgpmAtualizacao->create();
            $this->IgpmAtualizacao->save($l);
        }
    }
    
    private function obterPagamento($despesa) {
        $despesaPagamento = $this->DespesaPagamento->find('first',
                array('conditions' => array(
                    'DespesaPagamento.despesa_id' => $despesa,
                    'Pagamento.status' => 'pago')));
        return $despesaPagamento['Pagamento'];
    }

}

?>