<?php
$tipo = isset($this->data['tipo']) ? $this->data['tipo'] : "formando";
?>
<style type="text/css">
.filtro-selecionado { display:none }
.filtro-selecionado.aberto { display:inline-block }
.container-tabela table tbody tr:nth-child(even) { background-color:#CCCCCC!important }
</style>
<script type="text/javascript">
	jQuery(function($) {
		$(".hover td:not(.unclick)").click(function() {
			$(this).colorbox({
 	 			width:"930",
 	 			height:"900",
 	 			href: "/<?=$this->params['prefix']?>/pagmidas/transacao/" + $(this).parent().children('.transacao-id').val(),
 	 			top : 0,
 	 			onCleanup : function() { /*window.location.reload();*/ }
 	 	 	});
		});
		$("#filtro").change(function() {
			var that = this;
			$(".filtro-selecionado:not(."+$(this).val()+")").fadeOut(500,
				function() {
					setInterval(function() {
						$(".filtro-selecionado."+$(that).val()).fadeIn(500).css("display","inline-block");
					},750);
				}
			)
		});
 	});
</script>
<span id="conteudo-titulo" class="box-com-titulo-header">Transações</span>
<div id="conteudo-container">
	<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
	<?php $session->flash(); ?>
	<div class="tabela-adicionar-item">
		<p>
			<?php echo $form->create(false, array('url' => "/{$this->params['prefix']}/pagmidas/transacoes",
				'class' => 'procurar-form-inline', 'id' => 'form-filtro')) ?>
			<label style="display:inline">
				Tipo: <?php echo $form->input('tipo',
					array('options' => $filtro, 'type' => 'select', 'id' => 'filtro',
						'label' => false, 'div' => false)); ?>
			</label>
			<div class="filtro-selecionado formando <?=$tipo == "formando" ? "aberto" : ""?>">
				<?php echo $form->input('formando',
					array('label' => false, 'div' => false)); ?>
			</div>
			<div class="filtro-selecionado periodo <?=$tipo == "periodo" ? "aberto" : ""?>">
				de 
				<?php echo $form->input('periodo-inicial',
					array('label' => false, 'div' => false)); ?>
				&agrave; 
				<?php echo $form->input('periodo-final',
					array('label' => false, 'div' => false)); ?>
			</div>
			<div class="filtro-selecionado bandeira <?=$tipo == "bandeira" ? "aberto" : ""?>">
				<select name="data[bandeira]">
				<?php foreach($bandeiras as $bandeira) : ?>
					<option value="<?=$bandeira['id']?>"><?=$bandeira['nome']?></option>
				<?php endforeach; ?>
			</select>
			</div>
			<label style="display:inline">
				<?php echo $form->end(array('label' => false, 'div' => false, 'class' => 'submit-busca')) ?>
			</label>
		</p>
		<?=$html->link('Nova Transação',array($this->params['prefix'] => true, 'action' => 'index')); ?>
		<div style="clear:both;"></div>
	</div>
	<div class="container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col"><?=$paginator->sort('Data da Venda', 'data_cadastro'); ?></th>
					<!--<th scope="col">Código</th>-->
					<th scope="col"><?=$paginator->sort('Formando', 'PagmidasCartoes.nome'); ?></th>
					<th scope="col"><?=$paginator->sort('Nome no Cartão', 'PagmidasCartoes.nome_cartao'); ?></th>
					<th scope="col"><?=$paginator->sort('Bandeira', 'PagmidasCartoes.bandeira_id'); ?></th>
					<th scope="col"><?=$paginator->sort('Valor', 'valor'); ?></th>
					<th scope="col"><?=$paginator->sort('Parcelas', 'PagmidasCartoes.parcelas'); ?></th>
					<th scope="col">Status</th>
					<!--<th scope="col">&nbsp;</th>-->
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="4">
						<?=$paginator->counter(array('format' =>
								'Itens %start% ao %end% - página %page% de %pages%')); ?>
						<span class="paginacao">
							<?=$paginator->numbers(array('separator' => ' ')); ?>				
						</span>
					</td>
					<td colspan="3"><?=$paginator->counter(array('format' => 'Total : %count% ' .  $this->name)); ?></td>
				</tr>
			</tfoot>
			<tbody>
			<?php foreach ($transacoes as $transacao): ?>
				<?php $response = end($transacao['PagmidasResponses']); ?>
				<tr class="hover">
					<input type='hidden' class='transacao-id' value="<?=$transacao['PagmidasTransacoes']['id']?>" />
					<td colspan="1" width="15%">
						<?=date("d/m/Y",strtotime($transacao['PagmidasTransacoes']['data_cadastro']))?>
					</td>
					<!--<td colspan="1" width="7%">
						<?=!empty($response) ? $response['autorizacao'] : "Indefinido"?>
					</td>-->
					<td colspan="1" width="15%"><?=$transacao['PagmidasCartoes']['nome']; ?></td>
					<td colspan="1" width="15%"><?=$transacao['PagmidasCartoes']['nome_cartao']; ?></td>
					<td colspan="1" width="15%">
						<?php if(!empty($transacao['PagmidasCartoes']['bandeira_id'])) : ?>
							<img src="<?="{$this->webroot}img/" . 
							$bandeiras[$transacao['PagmidasCartoes']['bandeira_id']]['imagem']?>" width="35" />
						<?php endif; ?>
					</td>
					<td colspan="1" width="10%">
						R$<?=number_format($transacao['PagmidasTransacoes']['valor'],2,',','.');?>
					</td>
					<td colspan="1" width="3%"><?=$transacao['PagmidasCartoes']['parcelas']?></td>
					<td colspan="1" width="15%">
						<?php
							if(!empty($response) && isset($codigos[$response['status_transacao']]['mensagem']))
								echo $codigos[$response['status_transacao']]['mensagem'];
							else
								echo "Processando";
						?>
					</td>
					<!--
					<td class='unclick' colspan="1" width="20%">
						<?=$html->link('Abrir em nova janela', array(
								$this->params['prefix'] => true,
								'controller' => 'pagmidas',
								'action' =>'transacao',
								$transacao['PagmidasTransacoes']['id']),
								array('class' => 'submit button', 'target' => '_blank')); ?>
					</td>
					-->
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>