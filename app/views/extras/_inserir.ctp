<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var salvar = $("<button>",{
            type: 'button',
            class: 'button bg-color-greenDark salvar',
            text:'Salvar'
        });
        $('.modal-footer').prepend(salvar);
        $(".selectpicker").selectpicker({width:'100%', dropdown_position: "down"});
        $(".selectpicker.nome").change(function(){
            if($('.selectpicker.nome option').filter(':selected').text() == 'Brinde'){
                $('.brinde').fadeIn('slow');
            }else{
                $('#ExtraBrinde').val('');
                $('.brinde').fadeOut('slow');
            }
        });
        $(".selectpicker").change(function(){
            if($('.selectpicker.nome option').filter(':selected').text() == 'Outros'){
                $('.outros').fadeIn('slow');
            }else{
                $('#ExtraOutros').val('');
                $('.outros').fadeOut('slow');
            }
        });
        $(".salvar").click(function(e) {
            e.preventDefault();
            if($('.nome option').filter(':selected').val() == ''){
                bootbox.alert("<h2 class='fg-color-red'>Preencha o Tipo de Extra.</h2>");
            }else{
                var context = ko.contextFor($(".metro-button.reload")[0]);
                var dados = $("#form").serialize();
                var url = $("#form").attr('action');
                bootbox.hideAll(); 
                context.$data.showLoading(function() {
                    $.ajax({
                        url: url,
                        data: dados,
                        type: "POST",
                        dataType: "json",
                        complete: function() {
                            context.$data.reload();
                        }
                    });
                });
            }
        });
    });
</script>
<div class="row-fluid">
	<?php $session->flash(); ?>
	<?php echo $form->create('Extra', array('url' => "/{$this->params['prefix']}/extras/inserir/" . $evento, 'id' => 'form')); ?>
        <?php echo $form->hidden('evento_id', array()); ?>
        <div class="row-fluid">
            <div class="span4">
                <?=$form->input('nome',array(
                'label' => 'Tipo de Extra',
                'type' => 'select',
                'class' => 'selectpicker nome',
                'options' => $extras,
                'div' => 'input-control text',
                'error' => false,
                'id' => 'beca')); ?>
            </div>
            <div class="span4">
                <?=$form->input('lote',array(
                'label' => 'Lote',
                'type' => 'select',
                'class' => 'selectpicker',
                'options' => $lotes,
                'div' => 'input-control text',
                'error' => false,
                'id' => 'beca')); ?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6 hide outros">
                    <?php echo $form->input('outros', array('label' => 'Outros', 'div' => 'input-control', 'error' => false)); ?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6 hide brinde">
                    <?php echo $form->input('brinde', array('label' => 'Brinde', 'div' => 'input-control', 'error' => false)); ?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span6">
                    <label>Descrição</label>
                    <?php echo $form->input('descricao', array('label' => false, 'div' => 'input-control', 'error' => false)); ?>
            </div>
        </div>
</div>