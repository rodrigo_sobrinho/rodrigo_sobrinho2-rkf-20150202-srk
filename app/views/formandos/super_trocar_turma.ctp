<style type="text/css">
#cboxLoadedContent { background-color:white!important; border:solid 1px black; border-radius:4px; padding:20px }
</style>
<span id="conteudo-titulo" class="box-com-titulo-header">Escolher Formando</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	
	<div class="tabela-adicionar-item">
		<?=$form->create(false, array('url' => "/{$this->params['prefix']}/formandos/trocar_turma", 'class' => 'procurar-form-inline', 'id' => 'form-filtro')); ?>
		<span>Busca: <?=$form->input('filtro-id', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false , 'value' => isset($valor) ? $valor : "")); ?> </span>
		<?=$form->input('filtro-tipo', array('options' => $arrayOptions, 'type' => 'select', 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false , "selected" => $tipo)); ?>
		&nbsp;&nbsp;&nbsp;
		<span>Qtde Por Página:
		<?=$form->input('qtde-por-pagina', array('options' => array("20" => "20" , "50" => "50" , "100" => "100"), 'type' => 'select', "selected" => $limit, 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
		</span>
		&nbsp;
		<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
		<?=$form->end(array('label' => ' ', 'div' => false, 'class' => 'submit-busca')); ?>

		<div style="clear:both;"></div>
	</div>
    <div class="container-tabela">
    	<table>
            <thead>
                <tr>
                	<th scope="col"><?=$paginator->sort('Turma', 'turma_id'); ?></th>
                    <th scope="col"><?=$paginator->sort('Cód', 'codigo_formando'); ?></th>
                    <th scope="col"><?=$paginator->sort('Nome', 'nome'); ?></th>
                    <th scope="col"><?=$paginator->sort('E-mail', 'email'); ?></th>
                    <th scope="col"><?=$paginator->sort('Curso', 'curso_nome'); ?></th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="5"><?=$paginator->counter(array('format' => 'Itens %start% ao %end% - página %page% de %pages%')); ?>
                        <span class="paginacao">
                            <?=$paginator->numbers(array('separator' => ' ')); ?>
                        </span>
                    </td>
                    <td><?=$paginator->counter(array('format' => 'Total : %count% ')); ?></td>
                </tr>
            </tfoot>
            <tbody>
                <?php $isOdd = false; ?>
                <?php foreach ($formandos as $formando): ?>
                	<tr class="hover<?=$isOdd ? " odd" : ""?>">
                		<input type="hidden" class='usuario-id' value="<?=$formando['ViewFormandos']['id']?>" />
                		<td  colspan="1" width="4%"><?=$formando['ViewFormandos']['turma_id']?></td>
                        <td  colspan="1" width="6%"><?=$formando['ViewFormandos']['codigo_formando']?></td>
                        <td  colspan="1" width="22%"><?=$formando['ViewFormandos']['nome']; ?></td>
                        <td  colspan="1" width="22%"><?=$formando['ViewFormandos']['email']; ?></td>
                        <td  colspan="1" width="15%"><?=$formando['ViewFormandos']['curso_nome']; ?></td>
                        <td  colspan="1" width="33%">
							<?php echo $html->link('Selecionar', array($this->params['prefix'] => true,
							'controller' => 'formandos', 'action' =>'escolher_turma', $formando['ViewFormandos']['id']),
							 array('class' => 'submit button')); ?>
                        </td>
                    </tr>
                    <?php $isOdd = !($isOdd); ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>