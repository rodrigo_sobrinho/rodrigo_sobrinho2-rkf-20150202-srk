<div class="container_16">
    <?php
    $eCupomDaEsquerda = true;
    $numeroDaLinha = 0;
    $linhas = count($cupons)-2;
    foreach ($cupons as $cupom) {
        $numero_cupom_formatado = str_pad($cupom['Cupom']['numero'] % 100000, 5, '0', STR_PAD_LEFT);

        $margemDaPagina = "";
        $classeQueTiraMargem = ($eCupomDaEsquerda) ? "alpha" : "omega";
        if ($numeroDaLinha % 10 == 0) {
            $margemDaPagina = "style='margin-top:20px;'";
        } elseif($numeroDaLinha == $linhas)
            $margemDaPagina = "style='margin-bottom:20px;'";

        if ($numeroDaLinha % 10 == 0 && $numeroDaLinha != 0 && $eCupomDaEsquerda)
            echo "</div><div style='page-break-after:always; display: block;'>&nbsp;</div><div class='container_16'>";

        if ($eCupomDaEsquerda)
            echo "<div class='grid_full alpha omega first' {$margemDaPagina} >";
        ?>

        <div class="cupom-container grid_8 alpha omega <?php echo $classeQueTiraMargem; ?>" style="position: relative;">
            <div class="cupom-container-esq grid_8 first alpha omega">

                <div class="cupom-numero-centro">
                    <span class="cupom-numero">
    <?php echo $numero_cupom_formatado; ?>
                    </span>
                </div>

                <p class="grid_full" style="font-size: 12pt">Nome:</p>
                <p class="grid_full cupom-sublinhado"></p>
                <p class="grid_full cupom-sublinhado"></p>
                <p class="grid_full" style="font-size: 12pt; margin-top: 20px;">Telefone:</p>
                <p class="grid_full cupom-sublinhado"></p>

                <div style="position: absolute; bottom: 0px; width: 100%;">
                    <div style="float: left;">
    <?php echo $html->image('logo_rifa.png', array('alt' => 'Logo', 'width' => '50px')) ?>
                    </div>
                    <span>
                        www.formaturas.as<br />
                        Tel. 11 5585 1294
                    </span>
                </div>

            </div>

            <div class="cupom-container-dir grid_8 alpha omega">

                <div>
                    <div>
                        <div style="float: left; width: 60%; text-align: left;">
                            <p style="font-size: 10pt; font-weight: bold;">
                                Ação Entre Amigos
                            </p>
                            <h3> <?php echo $cupom['Rifa']['nome'] ?> </h3>
                        </div>

                        <div class="cupom-numero-direita">
                            <span class="cupom-numero">
    <?php echo $numero_cupom_formatado; ?>
                            </span>
                        </div>
                    </div>

                    <p style="font-size: 8pt; text-align: justify;">
                        O colaborador concorrerá ao sorteio federal no dia 
                        <b><?php echo $data_sorteio_formatada; ?></b>, 
                        recebendo o prêmio conforme o sorteio da loteria.
                    </p>

                    <span style="font-size:12px; font-weight: bold; text-align:left">
                        <p style="height:70px!important; overflow:hidden; margin-top:3px">1&deg; Pr&ecirc;mio:  <?php echo $cupom['Rifa']['premio_1']; ?>
                            <?php if ($cupom['Rifa']['premio_2'] != "") echo "<br />2&deg; Pr&ecirc;mio: " . $cupom['Rifa']['premio_2']; ?>
    <?php if ($cupom['Rifa']['premio_3'] != "") echo "<br />3&deg; Pr&ecirc;mio: " . $cupom['Rifa']['premio_3']; ?>
                        </p>
                    </span>

                </div>

                <div style="position: relative; bottom: 0px;">
                    <p style="width: 70%; float: left; font-size: 6pt;">
                        O ganhador terá o prazo de 10 dias após a extração para 
                        retirar o prêmio, caso contrário perderá o mesmo.
                    </p>

                    <div style="float: right;">
    <?php echo $html->image('logo_rifa.png', array('alt' => 'Logo', 'width' => '50px')) ?>
                        <br />
                        <span style="font-size: 6pt">
                            www.formaturas.as<br />
                            Tel. 11 5585 1294
                        </span>
                    </div>
                </div>

            </div>
        </div>

        <?php
        if (!$eCupomDaEsquerda)
            echo "</div>";

        $eCupomDaEsquerda = !($eCupomDaEsquerda);
        $numeroDaLinha++;
    };
    ?>
</div>
