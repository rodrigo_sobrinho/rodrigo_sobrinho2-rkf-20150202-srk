<?php

 $javascript->link(
  array(
   'https://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js',
   'jquery.meio.mask.js'
  ),
 false);
 
 $javascript->codeBlock('
 	jQuery(function($) {
 		$("#CPF").setMask("999.999.999-99");
 		$("#FormandoProfileTelResidencial").setMask("phone");
 		$("#FormandoProfileTelComercial").setMask("phone");
 		$("#FormandoProfileTelCelular").setMask("phone");
 	});
 	', array('inline' => false));
 
?>


<?php echo $form->hidden('Turma.id', array('value' => $turma['Turma']['id'])); ?>

	<p class="grid_full alpha omega">
		<label class="grid_3 alpha omega">Turma</label>
		<span class="grid_101 alpha "> <?php echo $turma['Turma']['nome']?> </span>
	</p>
	
	<p class="grid_full alpha omega">
		<label class="grid_5 alpha">Nome</label>
		<?php echo $form->input('nome', array('class' => 'grid_6 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
	</p>
	
	<p class="grid_full alpha omega">
		<label class="grid_5 alpha">RG</label>
		<?php echo $form->input('FormandoProfile.rg', array('class' => 'grid_6 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
	</p>
	
        <p class="grid_full alpha omega">
            <label class="grid_5 alpha omega">Curso</label>
            <?php echo $form->select('FormandoProfile.curso_turma_id', $cursoTurma, null,  array('empty'=>false, 'class' => 'grid_6 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
        </p>
	
	<p class="grid_full alpha omega">
		<label class="grid_5 alpha">E-mail</label>
		<?php echo $form->input('email', array('class' => 'grid_6 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
	</p>	



	<p class="grid_full alpha omega">
		<label class="grid_5 alpha"><?php echo $form->label('Usuario.senha'); ?></label>
		<?php echo $form->input('Usuario.senha', array('class' => 'grid_6 first alpha omega', 'type' => 'password', 'value' => '', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
	</p>

	<p class="grid_full alpha omega">
		<label class="grid_10 alpha">Confirmar Senha</label>
		<?php echo $form->input('Usuario.confirmar', array('class' => 'grid_6 first alpha omega', 'type' => 'password', 'value' => '', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
	</p>
	
	<p class="grid_full alpha omega">
		<label class="grid_5 alpha">Telefone Residencial</label>
		<?php echo $form->input('FormandoProfile.tel_residencial', array('class' => 'grid_5 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
	</p>
	<p class="grid_full alpha omega">	
		<label class="grid_5 alpha">Telefone Comercial</label>
		<?php echo $form->input('FormandoProfile.tel_comercial', array('class' => 'grid_5 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
	</p>
	<p class="grid_full alpha omega">
		<label class="grid_5 alpha">Telefone Celular</label>
		<?php echo $form->input('FormandoProfile.tel_celular', array('class' => 'grid_5 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
		</span>
	</p>
	<p class="grid_full alpha omega">
		<label class="grid_5 alpha">Cargo</label>
		<span class="grid_5 alpha omega first">
			<?php echo $form->input('FormandoProfile.cargo_comissao', array('class' => 'grid_full alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_11'))); ?>
		</span>
	</p>
	