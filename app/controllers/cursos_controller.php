<?php

class CursosController extends AppController {

    var $name = 'Cursos';
    // Padrão de paginação
    var $paginate = array(
        'limit' => 10,
        'order' => array(
            'Curso.id' => 'desc'
        )
    );
    var $uses = array('Curso', 'Universidade', 'Usuario');
    //var $uses = array('Turma','Faculdade','Universidade');

    var $nomeDoTemplateSidebar = 'turmas';
    
    private function _remover($universidadeId,$faculdadeId,$cursoId) {
        $this->layout = false;
        $this->loadModel('CursoTurma');
        if(!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $data = $this->data;
            $turmas = $this->CursoTurma->find('count',array(
                'conditions' => array(
                    'CursoTurma.curso_id' => $data['Curso']['id']
                )
            ));
            if($turmas > 0 && empty($data['Curso']['substituir'])) {
                $this->Session->setFlash('Selecione uma Curso para substituir o removido', 'metro/flash/error');
            } elseif($turmas > 0) {
                if(!$this->CursoTurma->updateAll(
                        array('CursoTurma.curso_id' => $data['Curso']['substituir']),
                        array('CursoTurma.curso_id' => $data['Curso']['id'])
                ))
                    $this->Session->setFlash("Erro ao substituir curso", 'metro/flash/error');
                elseif(!$this->Curso->delete($data['Curso']['id']))
                    $this->Session->setFlash("Erro ao remover curso", 'metro/flash/error');
                else
                    $this->Session->setFlash("Curso removido com sucesso", 'metro/flash/success');
            } else {
                if(!$this->Curso->delete($data['Curso']['id']))
                    $this->Session->setFlash("Erro ao remover curso", 'metro/flash/error');
                else
                    $this->Session->setFlash("Curso removido com sucesso", 'metro/flash/success');
            }
            echo json_encode(array());
        } elseif($cursoId) {
            $this->data['Curso']['id'] = $cursoId;
            $turmas = $this->CursoTurma->find('count',array(
                'conditions' => array(
                    'CursoTurma.curso_id' => $cursoId
                )
            ));
            if($turmas > 0) {
                $cursos = $this->Curso->find('list',array(
                    'conditions' => array(
                        "Curso.id <> $cursoId",
                        'Curso.faculdade_id' => $faculdadeId
                    ),
                    'fields' => 'Curso.nome',
                    'order' => 'Curso.nome'
                ));
                $this->set('cursos',$cursos);
                $this->set('permitido',false);
            } else {
                $this->set('cursos',array());
                $this->set('permitido',true);
            }
            $this->render('_remover');
        }
    }
    
    function comercial_remover($universidadeId,$faculdadeId,$cursoId) {
        $this->_remover($universidadeId,$faculdadeId,$cursoId);
    }
    
    function super_remover($universidadeId,$faculdadeId,$cursoId) {
        $this->_remover($universidadeId,$faculdadeId,$cursoId);
    }
    
    private function _editar($universidadeId,$faculdadeId,$cursoId = false) {
        $this->layout = false;
        if(!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            if($this->Curso->save($this->data))
                $this->Session->setFlash('Dados salvos com sucesso', 'metro/flash/success');
            else {
                $erros = array('Erro ao salvar dados');
                if(!empty($this->Curso->validationErrors))
                    $erros = array_merge($erros,array_values($this->Curso->validationErrors));
                $this->Session->setFlash($erros, 'metro/flash/error');
            }
            echo json_encode(array());
        } else {
            $this->loadModel('Faculdade');
            $faculdades = array();
            foreach($this->Faculdade->find('all') as $faculdade)
                $faculdades[$faculdade['Universidade']['nome']][$faculdade['Faculdade']['id']] = $faculdade['Faculdade']['nome'];
            $this->Faculdade->recursive = 0;
            $this->data = $this->Faculdade->read(null,$faculdadeId);
            $this->data['Curso']['faculdade_id'] = $faculdadeId;
            if($cursoId) {
                $this->Curso->recursive = 0;
                $curso = $this->Curso->read(null,$cursoId);
                $this->data['Curso'] = $curso['Curso'];
            }
            $this->set('faculdades',$faculdades);
            $this->render('_editar');
        }
    }
    
    function comercial_editar($universidadeId,$faculdadeId,$cursoId = false) {
        $this->_editar($universidadeId,$faculdadeId,$cursoId);
    }
    
    function super_editar($universidadeId,$faculdadeId,$cursoId = false) {
        $this->_editar($universidadeId,$faculdadeId,$cursoId);
    }

    function super_index() {

        $this->set('cursos', $this->paginate('Curso'));
    }

    function super_procurar() {
        $chave = empty($this->data['Cursos']['chave']) ? "" : $this->data['Cursos']['chave'];
        $chaves = preg_split("[, -.]", $this->data['Cursos']['chave']);

        function construirQuery($c) {
            return array('Curso.nome LIKE' => '%' . $c . '%');
        }

        $chaves = array_map('construirQuery', $chaves);

        $conditions = empty($chaves) ? array() : array('OR' => $chaves);
        $this->set('cursos', $this->paginate('Curso', $conditions));

        $this->set('chave', $chave);
        $this->render('super_index');
    }

    function super_adicionar() {
        $this->selectDeFaculdades();
        if (!empty($this->data)) {
            if (is_numeric($this->data['Curso']['faculdade_id'])) {
                if ($this->Curso->save($this->data['Curso'])) {
                    $this->Session->setFlash('O curso foi salvo corretamente.', 'flash_sucesso');
                    $this->redirect("/{$this->params['prefix']}/cursos/visualizar/{$this->Curso->id}");
                }
                else
                    $this->Session->setFlash('Ocorreu um erro ao salvar o curso.', 'flash_erro');
            }
            else
                $this->Session->setFlash('Ocorreu um erro ao salvar o curso. Selecione uma faculade.', 'flash_erro');
        }
    }

    function super_editar_antigo($id = null) {
        $this->Curso->id = $id;
        $this->selectDeFaculdades();
        if (!empty($this->data)) {
            if (is_numeric($this->data['Curso']['faculdade_id'])) {
                if ($this->Curso->save($this->data['Curso'])) {
                    $this->Session->setFlash('O curso foi salvo corretamente.', 'flash_sucesso');
                    $this->redirect("/{$this->params['prefix']}/cursos/visualizar/{$this->Curso->id}");
                }
                else
                    $this->Session->setFlash('Ocorreu um erro ao salvar o curso.', 'flash_erro');
            }
            else
                $this->Session->setFlash('Ocorreu um erro ao salvar o curso. Selecione uma faculade.', 'flash_erro');
        }
        else
            $this->data = $this->Curso->read();

        if (!$this->data)
            $this->Session->setFlash('Curso não existente', 'flash_erro');
    }

    function super_visualizar($id = null) {

        $curso = $this->Curso->find(array('Curso.id' => $id));

        if (!$curso) {
            $this->Session->setFlash('Curso não existente', 'flash_erro');
        }
        $this->set('curso', $curso);
    }

    function super_deletar($id = null) {

        if ($this->Curso->delete($id))
            $this->Session->setFlash('Curso excluido com sucesso', 'flash_sucesso');
        else
        // TODO melhorar estas frases de erro
        // Talvez colocá-las em um lugar unificado seja interessante
            $this->Session->setFlash('Ocorreu um erro ao excluir o curso. Verifique se ele não possui uma turma atrelada.', 'flash_erro');

        $this->redirect("/{$this->params['prefix']}/cursos");
    }

    /**
     * Cria três listas(ou arrays) que contém:
     * 		Id e Nome de Universidades
     * 		Id e Nome de Faculdades
     * 		Id de universidade associado a uma lista/array de id de faculdades
     */
    private function selectDeFaculdades() {
        $faculdades = $this->Curso->Faculdade->find('all', array('order' => array('Faculdade.nome')));

        $chavesFaculdadeUniversidade = array();

        foreach ($faculdades as $faculdade) {
            if (empty($chavesFaculdadeUniversidade[$faculdade['Universidade']['id']])) {
                $chavesFaculdadeUniversidade[$faculdade['Universidade']['id']] = array();
            }

            $chavesFaculdadeUniversidade[$faculdade['Universidade']['id']][] = $faculdade['Faculdade']['id'];

            if (empty($universidadesArray[$faculdade['Universidade']['id']]))
                $universidadesArray[$faculdade['Universidade']['id']] = $faculdade['Universidade']['nome'];

            $faculdadesArray[$faculdade['Faculdade']['id']] = $faculdade['Faculdade']['nome'];
        }


        $this->set('chaves_faculdade_universidade', $chavesFaculdadeUniversidade);
        $this->set('universidade_select', $universidadesArray);
        $this->set('faculdade_select', $faculdadesArray);
    }

}

?>