<h2>Reuni&otilde;es para o dia <?=date('d/m/Y',$dia)?></h2>
<br />
<?php if(count($atividades) > 0) : ?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<div class="row-fluid">
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="20%">Descri&ccedil;&atilde;o</th>
                <th scope="col" width="20%">Consultor</th>
                <th scope="col" width="10%">Local</th>
                <th scope="col" width="10%">Sala</th>
                <th scope="col" width="10%">N&ordm; Pessoas</th>
                <th scope="col" width="15%">In&iacute;cio</th>
                <th scope="col" width="15%">Fim</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($atividades as $i => $atividade) : ?>
            <?php if($atividade['Atividade']['local']) : ?>
            <tr>
                <td><?=$atividade['Atividade']['nome']; ?></td>
                <td><?=$atividade['Usuario']['nome']; ?></td>
                <td><?=$atividade['Atividade']['local']; ?></td>
                <td>
                    <?php if(!empty($atividade['AtividadeLocal']['nome'])) : ?>
                    <?=$atividade['AtividadeLocal']['nome']; ?>
                    <?php else : ?>
                    N&atilde;o informado
                    <?php endif; ?>
                </td>
                <td><?=$atividade['Atividade']['numero_pessoas'] > 0 ?
                        $atividade['Atividade']['numero_pessoas'] : "N&atilde;o informado"?></td>
                <td><?=date('H:i',strtotime($atividade['Atividade']['data_inicio']))?></td>
                <td>
                    <?php if(!empty($atividade['Atividade']['data_fim'])) : ?>
                    <?=date('H:i',strtotime($atividade['Atividade']['data_fim']))?>
                    <?php else : ?>
                    N&atilde;o informado
                    <?php endif; ?>
                </td>
            </tr>
            <?php endif; ?>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="7">
                    <span class="label label-info">
                    <?=count($atividades)?> eventos
                    </span>
                </td>
            </tr>
        </tfoot>
    </table>
</div>
<?php else : ?>
<h3 class="fg-color-red">
    Nenhuma atividade encontrada
</h3>
<?php endif; ?>
