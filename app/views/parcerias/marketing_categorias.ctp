<style type="text/css">
.btn-group {  }
.categoria { display:inline-block; margin:0 10px 10px 0!important }
.btn-danger,.btn-danger:hover { text-decoration: line-through }
</style>
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var context = ko.contextFor($("#content-body")[0]);
        $(".categorias").on('click','.categoria',function() {
            if($(this).hasClass("btn-danger")) {
                $(this).removeClass("btn-danger").find(".ativa").val(1);
            } else {
                $(this).addClass("btn-danger").find(".ativa").val(0);
            }
        });
        $(".categorias").on('click','.remover-categoria',function() {
            var button = $(this).parent('a.btn');
            if(button.data('id') != undefined)
                $("#formulario").append($("<input>",{
                    type : "hidden",
                    name : "data[remover][]",
                    value : button.data('id')
                }));
            button.fadeOut(500,function() {
                button.tooltip("hide");
                button.remove();
                context.$data.loaded(true);
            });
        });
        $(".remover-categoria").tooltip();
        $(".categorias").sortable({
            containment : 'parent',
            items : ' > .categoria',
            update : function() {
                $(".categoria").each(function() {
                    $(this).find(".ordem").val($(this).index());
                });
            }
        });
        $("#adicionar").click(function() {
            if($("#input-categoria").val() != "") {
                var a = $("<a>",{
                    class : "btn categoria nova",
                    text : $("#input-categoria").val()
                });
                var id = $(".nova.categoria").length;
                a.append($("<input>",{
                    type : 'hidden',
                    name : 'data[adicionar]['+id+'][nome]',
                    value : $("#input-categoria").val()
                }));
                a.append($("<input>",{
                    type : 'hidden',
                    class : 'ordem',
                    name : 'data[adicionar]['+id+'][ordem]',
                    value : $(".categoria").length+1
                }));
                a.append($("<input>",{
                    type : 'hidden',
                    class : 'ativa',
                    name : 'data[adicionar]['+id+'][ativa]',
                    value : 1
                }));
                a.append($("<i>",{
                    class : "icon-close remover-categoria"
                }));
                $("#categorias").append(a);
                $("#input-categoria").val("").focus();
            }
        });
        $("#enviar").click(function() {
            context.$data.showLoading(function() {
                $.ajax({
                    type : 'POST',
                    url : $("#formulario").attr("action"),
                    dataType : 'json',
                    data : $("#formulario").serialize(),
                    error : function() {
                        alert("Erro ao enviar dados");
                    },
                    complete : function() {
                        context.$data.page($("#formulario").attr("action"));
                    }
                });
            });
        });
    })
</script>
<h2>
    <a class="metro-button reload" data-bind="click: reload"></a>
    Categorias
</h2>
<?php $session->flash(); ?>
<div class="row-fluid">
    <div class="span3">
        <div class="input-control text">
            <input type="text" class="form-control" id="input-categoria" placeholder="Nova categoria" />
        </div>
    </div>
    <div class="span9">
        <button type="button" class="default" id="adicionar">
            Adicionar
        </button>
    </div>
</div>
<?=$form->create(false,array(
    'url' => $this->here,
    'id' => 'formulario')) ?>
<div class="row-fluid categorias" id="categorias">
    <?php if(count($categorias) > 0) : ?>
    <?php foreach($categorias as $i => $categoria) : ?>
    <a class="btn categoria<?=$categoria["Categoria"]["ativa"] == 0 ? ' btn-danger' : ''?>"
        data-id="<?=$categoria["Categoria"]["id"]?>">
        <?=$categoria["Categoria"]["nome"]?>
        <input type="hidden" name="data[Categoria][<?=$i?>][id]"
            value="<?=$categoria["Categoria"]["id"]?>" />
        <input class="ativa" type="hidden" name="data[Categoria][<?=$i?>][ativa]"
            value="<?=$categoria["Categoria"]["ativa"]?>" />
        <input class="ordem" type="hidden" name="data[Categoria][<?=$i?>][ordem]"
            value="<?=$categoria["Categoria"]["ordem"]?>" />
        <i class="icon-close remover-categoria" title="remover"></i>
    </a>
    <?php endforeach; ?>
    <?php endif; ?>
</div>
<button type="button" class="button bg-color-greenDark" id="enviar">
    Enviar
</button>
<?= $form->end(array('label' => false, 'div' => false, 'class' => 'hide')); ?>