<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/datepicker.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/metroui/table.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/fileupload.css">
<style type="text/css">
#tab-content-dados { overflow:visible; }
.tab-pane { min-height: 300px; }
.opcoes div button { margin-left:10px; color:white }
.opcoes div button:first-child { margin-left:0px; }
</style>
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/bootstrap/datepicker.js"></script>
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/bootstrap/fileupload.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.selectpicker').selectpicker({width:'100%'});
    $("#sexoFormando").change(function() {
        if(!$(this).is(":disabled")) {
            if($(this).is(":checked"))
                $(this).next().html('Masc');
            else
                $(this).next().html('Fem');
        }
    });
    
    $(".exibir-opcoes").click(function(e) {
        e.preventDefault();
        var seletor = ".opcoes[dir='"+$(this).attr("dir")+"']";
        $('.exibir-opcoes').fadeIn(500);
        $(this).fadeOut(500,function() {
            if($('.opcoes[dir]:visible').length > 0) {
                $('.opcoes[dir]:visible').fadeOut(500,function() {
                    $(seletor).fadeIn(500,function() {
                        atualizaAltura($("#pessoais").height()+100);
                    });
                });
            } else {
                $(seletor).fadeIn(500,function() {
                    atualizaAltura($("#pessoais").height()+100);
                });
            }
        });
    });

    $(".ocultar-opcoes").click(function(e) {
        e.preventDefault();
        var seletor = ".exibir-opcoes[dir='"+$(this).parents(".opcoes").attr("dir")+"']";
        $(".opcoes[dir]").fadeOut(500,function() {
            $(seletor).fadeIn(500,function() {
                atualizaAltura($("#pessoais").height()+100);
            });
        });
    });
    
    $("#enviar-nova-senha").on('click',function() {
        if($("#senha").val() != "" && $("#confirmar").val() != "") {
            if($("#senha").val() == $("#confirmar").val()) {
                var url = "/<?=$this->params["prefix"]?>/usuarios/alterar_senha";
                $.ajax({
                    url : url,
                    data : {
                        data: {
                            Usuario : {
                                id: '<?=$usuario['Usuario']['id']?>',
                                senha: $("#senha").val()
                            }
                        }},
                    type : "POST",
                    dataType : "json",
                    success: function(response) {
                        if(response.error == 1)
                            var message = "Erro ao alterar senha";
                        else
                            var message = "Senha alterada com sucesso";
                        bootbox.alert(message,
                        function() {
                            $(".opcoes[dir='senha']").find('.ocultar-opcoes').trigger('click');
                        });
                    },
                    error : function() {
                        bootbox.alert("Erro ao alterar Senha");
                    }
                });
            } else {
                bootbox.alert("As senhas n&atilde;o coincidem");
            }
        } else {
            bootbox.alert("Preencha os campos \"Senha\" e \"Confirmar Senha\"");
        }
    })

    $("#brindes").click(function() {
        if($(this).attr('clicado') != 'true')
            $(this).attr('clicado','true').after(" Em Breve");
    });

    $("#habilitar").on('click',function() {
        if($(this).attr("ativo") != "true") {
            $('#formulario-edicao').find('input,a,button,select,li').each(function() {
                $(this).removeAttr('disabled').removeClass('.disabled');
            });
            $('.selectpicker').find('.disabled').each(function() {
                $('.selectpicker').removeClass('disabled');
                $('li').removeClass('disabled');
            });
            $(this).attr('ativo','true')
                .html('Bloquear Dados <i class="icon-locked"></i>');
        } else {
            $('#formulario-edicao').find('input,a,button,select,li').not("#habilitar").each(function() {
                $(this).attr('disabled','disabled').addClass('.disabled');
            });
 
            $(this).removeAttr('ativo')
                .html('Editar Dados <i class="icon-unlocked"></i>');
        }
    });

    $("#busca-cep:not([disabled='disabled'])").click(function(e) {
        e.preventDefault();
        $("#cep-load").text('Buscando...');
        buscarCep($("#FormandoProfileEndCep").val());
    });

    $("#FormandoProfileEndCep").blur(function() {
        $("#cep-load").text('Buscando...');
        buscarCep($("#FormandoProfileEndCep").val());
    });

    $("#FormandoProfileEndCep").focus(function() {
        $("#cep-load").attr('class','').text('');
    });

    $('.datepicker').datepicker();

    $("#busca-cep").bind('resposta-busca',function(response) {
        if(response.error) {
            $("#cep-load").addClass('fg-color-red').text("Endereço não encontrado");
        } else {
            $("#cep-load").addClass('fg-color-green').text("Endereço encontrado!");
            $("#FormandoProfileEndRua").val(unescape(response.endereco["tipo_logradouro"]) + " "
                    + unescape(response.endereco["logradouro"]));
            $("#FormandoProfileEndBairro").val(unescape(response.endereco["bairro"]));
            $("#FormandoProfileEndCidade").val(unescape(response.endereco["cidade"]));
            $('#FormandoProfileEndUf option[value="' + response.endereco["uf"] + '"]').attr("selected","selected");
            $("#FormandoProfileEndUf").trigger("liszt:updated");
            $("#FormandoProfileEndNumero").focus();
        }
    });

    $("#formulario-edicao").submit(function(e) { e.preventDefault() });

    $("#atualizar-cadastro").click(function(e) {
        e.stopPropagation();
        e.preventDefault();
        $("#conteudo-load").stop().fadeTo(200,0, function() {
            $('body').animate({
                scrollTop: $("html, body").offset().top
            },1000,function() {
                data = $("#formulario-edicao").serialize();
                var url = "/<?= $this->params["prefix"] ?>/usuarios/editar_dados";
                $("#conteudo-load").css({'opacity':1,'display':'none'});
                $.ajax({
                    url : url,
                    data : data,
                    type : "POST",
                    dataType : "json",
                    success : function(response) {
                        carregarPagina(url);
                    },
                    error : function(response) {
                        carregarPagina(url);
                    }
                });
            });
        });
    });
    
    $('#tab-dados li a[data-toggle="tab"]').on('show', function (e) {
        atualizaAltura($($(e.target).attr('href')).height()+100);
    });
	
    $("#foto-perfil").change(function(e) {
        if(e.target.files[0] != undefined)
            $("#enviar-foto-nova").fadeIn(500);
        else
            $("#enviar-foto-nova").fadeOut(500);
    });
    
    var imagem = {};
    
    $('.fileupload').bind('loaded',function(e) {
        var src = e.imagem.replace(/^data:image\/(gif|png|jpe?g);base64,/, "");
        var extensao = e.imagem.match(/^data:image\/(gif|png|jpe?g);base64,/);
        if(src && extensao[1]) {
            imagem.src = src;
            imagem.extensao = extensao[1];
        }
        return;
    });
    
    $("#enviar-foto-nova").on('click',function() {
        $(this).attr('disabled','disabled').addClass('disabled');
        $(".fileupload-preview").animate({
            opacity:0.1
        },600,function() {
            if(imagem.src != "" && imagem.extensao != "") {
                if($(".fileupload .load").hasClass('load'))
                    $(".fileupload .load").text('Aguarde');
                else
                    $(".fileupload").append("<div class='load'>Aguarde</div>");
                $.ajax({
                    url : "/<?=$this->params["prefix"]?>/usuarios/alterar_imagem",
                    data : {
                        extensao: imagem.extensao,
                        usuario:<?=$usuario['Usuario']['id']?>,
                        imagem:imagem.src
                    },
                    type : "POST",
                    dataType : "json",
                    async : true,
                    success : function(response) {
                        $(".fileupload-preview").animate({
                            opacity:1
                        },600,function() {
                            $(".fileupload .load").remove();
                        });
                        if(response.error) {
                            bootbox.alert(response.mensagem);
                        } else {
                            bootbox.alert("Foto alterada com sucesso",
                                function() {
                                    if(response.path)
                                        $("#logged-user img").attr('src',response.url);
                                $('.opcoes[dir=foto]').find(".ocultar-opcoes").click();
                                $("#enviar-foto-nova").hide();
                            });
                        }
                    },
                    error : function(response) {
                        $(".fileupload .load").text('Falha ao enviar');
                    }
                });
            }
        });
    });
    
    $("#tab-dados li a[href='#<?=$exibir?>']").tab('show');
});
function atualizarCadastro(callback) {
    data = $("#formulario-edicao").serialize();
    var url = "/<?=$this->params["prefix"]?>/usuarios/editar_dados";
    $.ajax({
        url : url,
        data : data,
        type : "POST",
        dataType : "json",
        complete : function() {
            callback();
        }
    });
}

</script>
<?php
$fotoPerfil = "";
if (isset($usuario['Usuario']['diretorio_foto_perfil']))
    if (!empty($usuario['Usuario']['diretorio_foto_perfil']))
        if (file_exists(APP . "webroot/{$usuario['Usuario']['diretorio_foto_perfil']}"))
            $fotoPerfil = $usuario['Usuario']['diretorio_foto_perfil'];
$session->flash();
?>
<div class="row-fluid">
    <h2>
        <?php if($this->params['prefix'] == 'atendimento') : ?>
        <a class="metro-button back" data-bind="click: function() {
               page('<?="/{$this->params['prefix']}/usuarios/listar"?>') }"></a>
        <?php endif; ?>
        <a class="metro-button reload" data-bind="click: reload"></a>
        Meus Dados
    </h2>
</div>
<br />
<div class="row-fluid">
    <div class="span12">
        <ul class="nav nav-tabs" id='tab-dados'>
            <li>
                <a href="#pessoais" data-toggle="tab">Dados Cadastrais</a>
            </li>
            <li>
                <a href="#financeiro" data-toggle="tab">Dados Financeiros</a>
            </li>
            <!--
            <li>
                <a href="#formatura" data-toggle="tab">Dados Para Brindes</a>
            </li>
            -->
            <li>
                <a href="#contrato" data-toggle="tab">Informa&ccedil;&otilde;es Contratuais</a>
            </li>
            <li>
                <a href="#redes-sociais" data-toggle="tab">Redes Sociais</a>
            </li>
        </ul>
        <div class="tab-content" id="tab-content-dados">
            <div class="tab-pane fade in" id="pessoais">
                <br />
                <?php include('opcoes.ctp'); ?>
                <br />
                <?=$form->create('Usuario', array(
                    'url' => "/{$this->params['prefix']}/usuarios/editar_dados",
                    'id' => 'formulario-edicao')); ?>
                <?=$form->hidden('Usuario.id'); ?>
                <?php include('pessoais.ctp'); include('contatos.ctp'); ?>
                <button class="default bg-color-red big"
                    disabled="disabled" data-bind="click: function() {
                        showLoading(function() {
                            atualizarCadastro(
                                function() { reload() }) }) }">
                    <i class="icon-checkmark"></i>
                    Atualizar
                </button>
                <?= $form->end(array('label' => false,
                    'div' => false, 'class' => 'hide')); ?>
            </div>
            <div class="tab-pane fade" id="contrato">
                <?php include('contrato.ctp'); ?>
            </div>
            <div class="tab-pane fade" id="financeiro">
                <?php include('financeiro.ctp'); ?>
            </div>
            <div class="tab-pane fade" id="formatura">
                <h2>
                    Essas informações são solicitadas de forma padronizada, 
                    não garantindo nenhum brinde específico para o formando.
                </h2>
                <br />
                <button class="default" id="brindes">
                    Curso, Camiseta, Calçado, Fotos para o telão
                </button>
            </div>
            <div class="tab-pane fade" id="redes-sociais">
                <?php include('redes_sociais.ctp'); ?>
            </div>
        </div>
    </div>
</div>
