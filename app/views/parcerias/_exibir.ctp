<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>css/jquery.lightbox-0.5.css">
<script type="text/javascript" src="<?= $this->webroot ?>js/jquery.lightbox-0.5.min.js"></script>

<script type="text/javascript">
    $(function() {
        $('#gallery a').lightBox();
    });
</script>

<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button back" data-bind="click: function() { 
               page('<?= "/{$this->params['prefix']}/parcerias/listar" ?>') }"></a>
               <?= $parcerias ? "{$parcerias['Parceria']['titulo']}" : "Parceria" ?>
        </h2>
    </div>
</div
<?php if (!empty($parcerias['Parceiro']['logo'])) { ?>
    <img src="<?= 'img/' . $caminho_raiz_logos . '/' . $parcerias['Parceiro']['logo']; ?>" 
         class="img-polaroid" style="width: 100px; height: 100px"/>
     <?php } ?>
<br/>
<br/>
<div class="row-fluid">
    <div class="span6">
        <strong>Promocao:</strong> <?= $parcerias['Parceria']['descricao'] ?>
    </div>
    <div class="span6">
        <strong>Imagens:</strong>
        <br/>
        <div id="gallery">
            <?php
            if (!empty($parceiro['FotoParceiro']) || !empty($parcerias['FotoParceria'])) {
                foreach ($parceiro['FotoParceiro'] as $foto) {
                    $foto_tag = $html->image(
                            $caminho_raiz_fotos_parceiro . '/' . $foto['nome'], array('style' => 'max-height: 80px; max-width:100px; margin-left: 20px; margin-top: 20px; border: solid 1px red;'));
                    $foto_caminho_completo = $this->webroot . IMAGES_URL . $caminho_raiz_fotos_parceiro;

                    echo '<a href="' . $foto_caminho_completo . '/' . $foto['nome'] . '">' . $foto_tag . '</a>';
                }

                foreach ($parcerias['FotoParceria'] as $foto) {
                    $foto_tag = $html->image(
                            $caminho_raiz_fotos_parceria . '/' . $foto['nome'], array('style' => 'max-height: 80px; max-width:100px; margin-left: 20px;'));
                    $foto_caminho_completo = $this->webroot . IMAGES_URL . $caminho_raiz_fotos_parceria;

                    echo '<a href="' . $foto_caminho_completo . '/' . $foto['nome'] . '">' . $foto_tag . '</a>';
                }
            }
            ?>
        </div>
        <br/>
<?php
echo $html->link('Adquira j&aacute; o seu voucher <i class="icon-gift"></i>', array($this->params['prefix'] => true, 'controller' => 'parcerias', 'action' => 'voucher', $parcerias['Parceria']['id']), array('escape' => false, 'class' => 'btn bg-color-gray btn-large', 'target' => '_blank'), false);
?>
    </div>
</div>
<br/>
<div class="row-fluid">
    <div class="span6">
        <strong>Data Inicio:</strong> <?= date('d/m/Y', strtotime($parcerias['Parceria']['data_inicio'])); ?>
    </div>
</div>
<br/>
<div class="row-fluid">
    <div class="span6">
        <strong>Data de Fim:</strong> <?= ($parcerias['Parceria']['data_fim'] == "") ? "  -" :
                date('d/m/Y', strtotime($parcerias['Parceria']['data_fim']));
?>
    </div>
</div>
<br/>
<div class="row-fluid">
    <div class="span6">
        <strong>Desconto:</strong> <?= $parcerias['Parceria']['desconto']; ?>
    </div>
</div>
<br/>
<div class="row-fluid">
    <div class="span6">
        <td><strong>Local:</strong> <?= $parcerias['Parceria']['local']; ?></td>
    </div>
</div>
<br/>
<div class="row-fluid">
    <div class="span6">
        <strong>Valor:</strong> <?= $parcerias['Parceria']['valor'] ?>
    </div>
</div>
<br/>
<div class="row-fluid">
    <div class="span6">
        <strong>Observacoes:</strong> <?= $parcerias['Parceria']['observacoes'] ?>
    </div>
</div>
<br/>
