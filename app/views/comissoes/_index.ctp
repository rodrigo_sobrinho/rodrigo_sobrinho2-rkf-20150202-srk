<span id="conteudo-titulo" class="box-com-titulo-header">Membros da Comissão</span>
<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col"><?php echo $paginator->sort('Nome', 'Usuario.nome'); ?></th>
					<th scope="col"><?php echo $paginator->sort('E-mail', 'Usuario.email'); ?></th>
					<th scope="col"><?php echo $paginator->sort('RG', 'FormandoProfile.rg'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Tel. Celular', 'FormandoProfile.tel_celular'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Cargo', 'FormandoProfile.cargo_comissao'); ?></th>
					<th scope="col"><?php echo $paginator->sort('ativo', 'Usuario.ativo'); ?></th>
					<th scope="col"> &nbsp;</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="6"><?php echo $paginator->counter(array('format' => 'Itens %start% ao %end% - página %page% de %pages%')); ?>
						<span class="paginacao">
							<?php echo $paginator->numbers(array('separator' => ' ')); ?>
						</span>
					</td>
					<td colspan="2"><?php echo $paginator->counter(array('format' => 'Total : %count% ' .  'Membro(s) da Comissão')); ?></td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach ($formandos as $formando): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<td  colspan="1" width="10%"><?php echo $formando['Usuario']['nome']; ?></td>
					<td  colspan="1" width="20%"><?php echo $formando['Usuario']['email']; ?></td>
					<td  colspan="1" width="10%"><?php echo $formando['FormandoProfile']['rg']; ?></td>					
					<td  colspan="1" width="15%"><?php echo $formando['FormandoProfile']['tel_celular']; ?></td>
					<td  colspan="1" width="10%"><?php echo $formando['FormandoProfile']['cargo_comissao']; ?></td>
					<td  colspan="1" width="10%"><?php echo ($formando['Usuario']['ativo'] == 1) ? 'sim' : 'nao'; ?></td>
					<td  colspan="1" width="20%">
						<?php echo $html->link('Visualizar', array($this->params['prefix'] => true, 'controller' => 'Comissoes', 'action' =>'visualizar', $formando['FormandoProfile']['usuario_id']), array('class' => 'submit button')); ?>
						<?php 
						if ($formando['Usuario']['ativo'])
							echo $html->link('Desativar', array($this->params['prefix'] => true, 'controller' => 'Comissoes', 'action' => 'desativar', $formando['Usuario']['id']), 
							array('class' => 'submit button', 'onclick' => "javacript: if(confirm('Deseja desativar o membro {$formando['Usuario']['nome']}?')) return true; else return false;"));
						else
							echo $html->link('Ativar', array($this->params['prefix'] => true, 'controller' => 'Comissoes', 'action' => 'ativar', $formando['Usuario']['id']), 
							array('class' => 'submit button', 'onclick' => "javacript: if(confirm('Deseja ativar o membro {$formando['Usuario']['nome']}?')) return true; else return false;"));
							?>
					</td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>