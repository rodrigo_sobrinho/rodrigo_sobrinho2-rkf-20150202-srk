<?php $session->flash(); ?>
<?php if($evento) : ?>
<script type="text/javascript" src="<?= $this->webroot ?>js/mapas-de-mesas/jcanvas.min.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>js/mapas-de-mesas/jcanvas-handles.min.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>js/mapas-de-mesas/jscolor.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>js/mapas-de-mesas/main.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>js/mapas-de-mesas/agendador-mesas-1.0.0.js"></script>
<style type="text/css">
    .menu {
        position:fixed;
        background: white;
        right:15px;
        top:60px;
        padding: 5px;
        width: 250px;
    }
</style>
<br />
<br />
<br />
<br />
<div class="row-fluid">
    <h2>
        Mapa de Mesa
    </h2>
</div>
<br />
<div class="row-fluid">
    <h4 class="fg-color-red">
        <?="{$evento['TiposEvento']['nome']} - {$evento['Evento']['nome']}"?>
    </h4>
</div>
<br />
<?=$form->hidden(false,array('id' => 'src'));?>
<div class="row-fluid">
    

    <div class="span9">
    <img id="mapa_imagem" src="<?php echo $webroot.$mapa_src ?>" class="hide"  />
    <canvas tabindex="1" width="964" id="meucanvas" height="798" style="border: 1px solid">
    </canvas>
    </div>

    <div class="menu">

        <div class="container-form-canvas">
   
            <!-- formulário do componente -->
            <form id="componente" action=""/>

                <label id="legenda">Ferramentas de manipulação</label>
                
                <input type="hidden" id="mapalayout" name="mapaLayout" value="" />
                <input type="hidden" id="turmaMapaID" name="turmaMapaID" value="<?php echo $turmaMapa; ?>" />
                
                <!-- Componente de linhas e colunas -->
                <div id="containerCriarGrupo" class="container-linhas hide"> 
                    <label>Linha X Coluna</label>
                    <p>
                        <input type="text" class="linha" maxlength="2" style="width:25px;"/>
                        <input type="text" class="coluna"  maxlength="2" style="width:25px;"/>
                    </p>
                    <a href="javascript:void(0)" id="mesaConfigToggle"><label>Configurações Avançadas</label></a>
                    <div id="mesaConfig" style="display:none;">
                        <label>Mesa</label>
                        <p>
                            Raio: <input type="text" class="raioMesa" maxlength="2" style="width:25px;"/>
                            Distância: <input type="text" class="distanciaMesa" maxlength="2" style="width:25px;"/>
                        </p>
                        <label>Tipo Referência</label>
                        <p>
                            <select class="tipoReferencia">
                                <option value="1">Topo</option>
                                <option value="2">Base</option>
                            </select>
                        </p>
                        <label>Setor</label>
                        <p>
                            <input type="text" class="setor" value="A" maxlength="50" style="width:25px;"/>
                        </p>
                    </div>
                    <div>
                        <input type="submit" id="criarObjetosHandler" class="criar" value="Criar">
                        <input type="submit" id="editarObjetosHandler" class="editar hide" value="Editar">
                        <input class="cancelargrupo" id="cancelarObjetosHandler" type="submit" value="Cancelar">
                    </div>
                </div>

                <!-- Componente de criação de retângulos -->
                <div id="containerCriarRetangulo" class="container-linhas hide"> 
                    <p>
                        Texto: <input type="text" id="texto"/>
                    </p>
                    <p>
                        Cor Preenchimento: <input class="color" value="AAAAAA" type="text" id="corPreenchimento" style="width: 75px"/>
                    </p>
                    <p>
                        Cor Texto: <input class="color" value="000000" type="text" id="corTexto" style="width: 75px"/>
                    </p>
                    <div>
                        <input type="submit" id="criarObjetoRetanguloHandler" class="criar" value="Criar">
                        <input type="submit" id="editarObjetoRetanguloHandler" class="editar hide" value="Editar">
                        <input class="cancelargrupo" id="cancelarObjetoRetanguloHandler" type="submit" value="Cancelar">
                    </div>
                </div>
                
                <div id="containerPainel">
                    <div>
                        <input id="mapearReferenciaCheckbox" type="checkbox" checked /> Mapear Referência
                    </div>
                    <div>
                        <input id="criarPainelHandler" type="submit" value="Criar Grupo">
                        <input id="criarRetanguloHandler" type="submit" value="Criar Retângulo">
                    </div>
                    <div id="infoObjetoDiv" class="hide">
                        <label id="legenda">Informações do Objeto</label>
                        <div id="infoObjeto"></div>
                        <input id="apagarPainelHandler" type="submit" value="Apagar Grupo">
                        <input id="editarPainelHandler" type="submit" value="Editar Objeto">
                    </div>
                    <input id="salvarHandler" type="submit" value="Salvar composição">
                </div>
            </form> 
        </div>
    </div>
</div>
<?php else : ?>
<h2 class="fg-color-red">Nenhuma festa encontrada para esta turma</h2>
<?php endif; ?>