<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/cronograma.css?v=0.2">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/cronograma.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#novo-item').click(function() {
        url = $(this).attr('href');
        bootbox.dialog('Carregando',[{
            label: 'Fechar'
        }],{
            remote: url
        });
    });
    $('#nova-mensagem').click(function() {
        bootbox.dialog('Carregando',[{
            label: 'Enviar',
            'class': 'bg-color-red',
            callback: function() {
                $("#formulario").trigger('submit');
                return false;
            }
        },{
            label: 'Fechar',
            'class': 'bg-color-blue'
        }],{
            remote: '/mensagens/adicionar'
        });
    });
    $('#historico').click(function() {
        if($(".carregar-mensagens.carregado").length > 0) {
            url = "/<?=$this->params['prefix']?>/mensagens/historico_assunto/" + $(".carregar-mensagens.carregado").attr('dir');
            bootbox.dialog('Carregando',[{
                label: 'Fechar'
            }],{
                remote: url
            });
        }
    });
});
</script>
<?=$form->hidden('usuario',array('value' => $usuario['Usuario']['id'],'id' => 'usuario_id'))?>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" id="recarregar" data-bind="click: function() { reload() }"></a>
            Mensagens
            <button type="button" class="pull-right button default" id="nova-mensagem">
                Nova Mensagem
            </button>
            <?php if($this->params['prefix'] == 'planejamento') : ?>
            <button type="button" class="pull-right button bg-color-greenDark"
                href="/<?=$this->params['prefix']?>/itens/editar" id="novo-item">
                Novo Item
                <i class="icon-plus-2"></i>
            </button>
            <?php endif; ?>
            <button type="button" class="pull-right button bg-color-red"
                href="/mensagens/email" data-bind="click:loadThis">
                Visão Email
                <i class="icon-email"></i>
            </button>
        </h2>
    </div>
</div>
<?php $session->flash(); ?>
<div class='row-fluid'>
    <div class='span3'>
        <div class='well' id='itens'>
            <ul class="nav nav-list">
                <li class='active'>
                    <a>Itens</a>
                </li>
                <?php $header = ''; ?>
                <?php foreach($itens as $titulo => $grupo) : foreach($grupo as $item) : ?>
                <?php if(!is_int($titulo) && $titulo != $header) : ?>
                <li class="nav-header"><?=$titulo?></li>
                <?php $header = $titulo;?>
                <?php elseif(is_int($titulo)) : ?>
                <li class="divider"></li>
                <?php endif; ?>
                <li>
                    <a class='carregar-assuntos' dir='<?=$item['id']?>'
                        title='<?=$item['nome']?>' titulo="<?=is_int($titulo)?"":$titulo?>">
                        <?=$item['nome']?>
                        <?php if($item['pendencias'][0]['total'] > 0) { ?>
                        <?php if($item['pendencias'][0]['total'] == $item['pendencias'][0]['resolvidos']) { ?>
                        <i class="pendencia icon-Resolvido"></i>
                        <?php } elseif($item['pendencias'][0]['as_eventos'] > 0 && $item['pendencias'][0]['comissao'] > 0) { ?>
                        <i class="pendencia icon-As ambos"></i>
                        <?php } elseif($item['pendencias'][0]['as_eventos'] > 0) { ?>
                        <i class="pendencia icon-As"></i>
                        <?php } elseif($item['pendencias'][0]['comissao'] > 0) { ?>
                        <i class="pendencia icon-Comissao"></i>
                        <?php } ?>
                        <?php } ?>
                    </a>
                </li>
                <?php endforeach; endforeach; ?>
            </ul>
        </div>
    </div>
    <div class='span3'>
        <div class='well' id='assuntos'>
            <ul class="nav nav-list">
                <li class='active default'>
                    <a>Assuntos</a>
                </li>
                <li class="nav-header load default"
                    style='display:none'>
                    Carregando
                </li>
                <li class="nav-header item default"
                    style='display:none'>
                </li>
            </ul>
        </div>
    </div>
    <div class='span6'>
        <div class='well' id='mensagens'>
            <ul class="nav nav-list">
                <li class='active'>
                    <a>
                        Mensagens
                        <i class="icon-history pull-right" id="historico"></i>
                    </a>
                </li>
                <li class="nav-header load default"
                    style='display:none'>
                    Carregando
                </li>
            </ul>
            <?php
                    $fotoPerfil = "img/uknown_user.gif";
                    if (isset($usuario['Usuario']['diretorio_foto_perfil']))
                        if (!empty($usuario['Usuario']['diretorio_foto_perfil']))
                            if (file_exists(APP . "webroot/{$usuario['Usuario']['diretorio_foto_perfil']}"))
                                $fotoPerfil = $usuario['Usuario']['diretorio_foto_perfil'];
                    ?>
            <div class='l'>
                <div class='m default' style='display:none'>
                    <div class='img'>
                        <img src='<?="{$this->webroot}img/"?>uknown_user.gif' />
                    </div>
                    <div class='b'>
                        <div class='d'></div>
                        <div class='t'></div>
                    </div>
                </div>
                <div class='resp'>
                    <i class="icon-cancel-4 fechar"></i>
                    <div class="mcont">
                        <div class="mhed">
                            <div class='img'>
                                <img src='<?="{$this->webroot}{$fotoPerfil}" ?>'/>
                            </div>
                            <div class='mdet'>
                                <div class='maut'></div>
                                <div class='mtyp'>
                                    <i></i>
                                    <div class="mdate"></div>
                                </div>
                            </div>
                        </div>
                        <div class="mopt">

                        </div>
                        <div class="mbody">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row-fluid hide msg-model" id="msg-model">
    <form>
        <div class="row-fluid">
            <div class="span6">
                <label>Item</label>
                <select class="item nova-mensagem" name="data[Item][id]">

                </select>
            </div>
            <div class="span6 div-assunto existente">
                <label>
                    Assunto
                    <a class="pull-right button mini default exibir-assunto" dir="novo">
                        Novo Assunto
                    </a>
                </label>
                <select class="assunto nova-mensagem" name="data[Mensagem][assunto_id]">

                </select>
            </div>
            <div class="span6 novo div-assunto hide">
                <label>
                    Assunto
                    <a class="pull-right button mini default exibir-assunto" dir="existente">
                        Cancelar
                    </a>
                </label>
                <input type="text" class="assunto nova-mensagem input-medium" name="data[Assunto][nome]" />
            </div>
        </div>
        <div class="row-fluid">
            <label>Anexo</label>
            <div class="upload" data-reader>
                <span class="btn-file">
                    <a class="button default fileupload-new">Adicionar</a>
                    <span class="fileupload-exists">Alterar</span>
                    <input type="file" id="anexo-nova-mensagem" />
                </span>
                <span class="fileupload-preview"></span>
            </div>
        </div>
        <div class="row-fluid">
            <label>Mensagem</label>
            <textarea class="texto nova-mensagem" name="data[Mensagem][texto]"></textarea>
        </div>
    </form>
</div>