<?=$html->css('messages')?>
<script type="text/javascript">
	$(function () {
		var formando = { titular : false };
		$( "#formando" ).autocomplete({
			source: function( request, response ) {
				$.ajax({
					url: "/Pagmidas/listar_formandos",
					dataType: "json",
					data: {
						data : request.term
					},
					success: function( data ) {
						response( $.map( data, function( formando ) {
							return {
								formando: formando,
								value: formando.nome
							}
						}));
					}
				});
			},
			minLength: 2,
			select: function( event, ui ) {
				$('#formando').next().val(ui.item.formando.id);
				formando.dados = ui.item.formando;
				preencheFormularioPorFormando();
			}
		});
		$('.masked').each(function() {
			if($(this).attr('mask') != undefined) $(this).setMask($(this).attr('mask'));
		});
		$("#PagmidasCartoesEndCep").blur( function() {
			var cep = $(this).val().replace("-","");
			var url = "http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep=" + cep;
			var that = this;
			$(that).prev().html("CEP&nbsp;&nbsp;&nbsp;<small>Buscando...</small>");
			$.getScript(url, function(){
				if(resultadoCEP["resultado"] && resultadoCEP["tipo_logradouro"] != "") {
					$("#load-cep").html("");
					$("#PagmidasCartoesEndRua").val(unescape(resultadoCEP["tipo_logradouro"]) + " " + unescape(resultadoCEP["logradouro"]));
					$("#PagmidasCartoesEndBairro").val(unescape(resultadoCEP["bairro"]));
					$("#PagmidasCartoesEndCidade").val(unescape(resultadoCEP["cidade"]));
					$('#PagmidasCartoesEndUf option[value="' + resultadoCEP["uf"] + '"]').attr("selected","selected");
					$(that).prev().children('small').html("");
					$("#PagmidasCartoesEndNumero").focus();
				} else {
					$(that).prev().children('small').html("Erro");
				}
			});
		});
		$("#formando-titular").click(function() {
			formando.titular = $(this).is(":checked");
			if(formando.dados != undefined)
				preencheFormularioPorFormando();
		});

		function preencheFormularioPorFormando() {
			if(formando.titular) {
				$('[name^="data[PagmidasCartoes]"]').each(function() {
					if($(this).attr('dir') != undefined) {
						valor = eval('formando.dados.'+$(this).attr('dir'));
						if(valor != undefined) {
							if($(this).is('input'))
								$(this).val(valor);
							else if($(this).is('select'))
								$(this).find('option[value="' + valor + '"]').attr('selected','selected');
						}
					}
				});
			}
		}
	});
</script>
<style type='text/css'>
.radio { float:left; margin:4px 4px 0 0; }
</style>
<span id="conteudo-titulo" class="box-com-titulo-header">Pagmidas</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<?php echo $form->create('PagmidasTransacoes', array('url' => "/{$this->params['prefix']}/Pagmidas", 'id' => 'formulario')); ?>
	<div class="grid_10 first">
		<p>
			<label>Digite o nome ou código do formando para buscar</label>
		</p>
		<p class="grid_full alpha omega">
			<label class="grid_full alpha omega">
				Formando
				&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="checkbox" id="formando-titular" />
				<small>Formando é o titular?</small>
			</label>
			<input type="text" id="formando" value="<?=isset($formando) ? $formando['ViewFormandos']['nome'] : ''?>" class="grid_6 alpha omega first">
			<?=$form->input('usuario_id_formando', array(
				'type' => 'hidden',
				'value' => (isset($formando) ? $formando['ViewFormandos']['id'] : ''),
				'label' => false, 'div' => false, 
				'error' => false));
			?>
		</p>
		<h1>Dados do titular</h1>
		<br />
		<br />
		<div class="grid_full first alpha">
			<p class='grid_6 alpha omega first'>
				<label class="full_grid alpha omega">Nome</label>
				<?=$form->input('PagmidasCartoes.nome', array(
					'class' => 'grid_full alpha',
					'dir' => 'nome',
					'label' => false, 'div' => false, 
					'error' => false)); ?>
			</p>
			<p class='grid_2 alpha omega prefix_1'>
				<label class="grid_full alpha omega">Sexo</label>
				<?=$form->select('PagmidasCartoes.sexo', array('M'=>'Masc', 'F'=>'Fem'),null, array(
					'empty'=>false,
					'class' => 'grid_full last alpha omega',
					'dir' => 'sexo',
					'label' => false, 'div' => false,
					'error' => false)); ?>
			</p>
			<p class='grid_3 alpha omega prefix_1'>
				<label class="full_grid alpha omega">Tel Res</label>
				<?=$form->input('PagmidasCartoes.tel_residencial', array(
					'class' => 'grid_full masked',
					'mask' => '(99) 9999-9999',
					'dir' => 'tel_residencial',
					'label' => false, 'div' => false, 
					'error' => false)); ?>
			</p>
			<p class='grid_3 omega prefix_1 last'>
				<label class="full_grid alpha omega">Tel Cel</label>
				<?=$form->input('PagmidasCartoes.tel_celular', array(
					'class' => 'grid_full omega last masked',
					'mask' => '(99) 9999-9999',
					'dir' => 'tel_celular',
					'label' => false, 'div' => false, 
					'error' => false)); ?>
			</p>
			<p class='grid_3 alpha omega first'>
				<label class="full_grid alpha omega">CEP</label>
				<?=$form->input('PagmidasCartoes.end_cep', array(
					'class' => 'grid_full alpha masked', 
					'mask' => '99999999',
					'dir' => 'end_cep',
					'label' => false, 'div' => false, 
					'error' => false)); ?>
			</p>
			<p class='grid_9 prefix_1 alpha omega'>
				<label class="full_grid alpha omega">Endereço</label>
				<?=$form->input('PagmidasCartoes.end_rua', array(
					'class' => 'grid_full alpha',
					'dir' => 'end_rua',
					'label' => false, 'div' => false, 
					'error' => false)); ?>
			</p>
			<p class='grid_2 prefix_1 alpha omega last'>
				<label class="full_grid alpha omega">N&ordm;</label>
				<?=$form->input('PagmidasCartoes.end_numero', array(
					'class' => 'grid_full omega last',
					'dir' => 'end_numero',
					'label' => false, 'div' => false, 
					'error' => false)); ?>
			</p>
			<p class='grid_6 alpha omega first'>
				<label class="full_grid alpha omega">Bairro</label>
				<?=$form->input('PagmidasCartoes.end_bairro', array(
					'class' => 'grid_full alpha',
					'dir' => 'end_bairro',
					'label' => false, 'div' => false, 
					'error' => false)); ?>
			</p>
			<p class='grid_5 prefix_1 alpha omega'>
				<label class="full_grid alpha omega">Cidade</label>
				<?=$form->input('PagmidasCartoes.end_cidade', array(
					'class' => 'grid_full alpha',
					'dir' => 'end_cidade',
					'label' => false, 'div' => false, 
					'error' => false)); ?>
			</p>
			<p class='grid_3 prefix_1 alpha omega last'>
				<label class="full_grid alpha omega">UF</label>
				<?=$form->select('PagmidasCartoes.end_uf', $html->listaUF(), 'SP', array(
					'empty'=>false,
					'dir' => 'end_uf',
					'class' => 'grid_full last alpha omega',
					'label' => false, 'div' => false,
					'error' => false)); ?>
			</p>
		</div>
		<h1>Dados do cartão</h1>
		<br />
		<br />
		<br />
		<div class="grid_full alpha first">
			<div class="grid_full first">
				<?php foreach($bandeiras as $bandeira) : ?>
				<p class='grid_3 alpha omega'>
					<input type="radio" alt="<?="bandeira-{$bandeira['PagmidasBandeiras']['nome']}"?>"
						name='data[PagmidasCartoes][bandeira_id]' value='<?=$bandeira['PagmidasBandeiras']['id']?>'
						class='radio' id='<?="bandeira-{$bandeira['PagmidasBandeiras']['id']}"?>' />
					<label for='<?="bandeira-{$bandeira['PagmidasBandeiras']['id']}"?>'>
						<img src="<?="{$this->webroot}img/{$bandeira['PagmidasBandeiras']['imagem']}"?>" width="35" />
					</label>
				</p>
				<?php endforeach; ?>
			</div>
			<p class='grid_full alpha first'>
				<label class="grid_full alpha omega">Nome no Cart&atilde;o 
				<small>(Digite exatamente como aparece no cart&atilde;o)</small></label>
				<?=$form->input('PagmidasCartoes.nome_cartao', array(
					'class' => 'grid_10 alpha first',
					'label' => false, 'div' => false, 
					'error' => false)); ?>
			</p>
			<p class='grid_6 alpha first'>
				<label class="full_grid alpha omega">N&ordm;</label>
				<?=$form->input('PagmidasCartoes.numero_cartao', array(
					'class' => 'grid_full alpha first',
					'label' => false, 'div' => false, 
					'error' => false)); ?>
			</p>
			<p class='grid_4 alpha omega prefix_1'>
				<label class="grid_full alpha omega">Cod Segurança:</label>
				<?=$form->input('PagmidasCartoes.codigo_seguranca', array(
					'class' => 'grid_full alpha', 
					'label' => false, 'div' => false, 
					'error' => false)); ?>
			</p>
			<p class='grid_4 alpha omega prefix_1'>
				<label class="grid_full alpha omega">Data de Validade</label>
				<?=$form->input('PagmidasCartoes.data_validade', array(
					'class' => 'grid_8 alpha masked',
					'mask' => '99/9999',
					'label' => false, 'div' => false, 
					'error' => false));
				?>
				&nbsp;<em>mm/yyyy</em>
			</p>
		</div>
		<h1>Dados da compra</h1>
		<br />
		<br />
		<p class='grid_5 alpha first'>
			<label class="full_grid alpha omega">Item</label>
			<?=$form->input('PagmidasItens.nome', array(
				'class' => 'grid_full alpha first', 
				'label' => false, 'div' => false, 
				'error' => false)); ?>
		</p>
		<p class='grid_5 alpha omega prefix_1'>
			<label class="grid_full alpha omega">Valor <small>ex: 15.000,52</small></label>
			<?=$form->input('PagmidasTransacoes.valor', array(
				'class' => 'grid_full alpha', 
				'label' => false, 'div' => false, 
				'error' => false)); ?>
		</p>
		<p class='grid_4 alpha omega prefix_1'>
			<label class="grid_full alpha omega">Parcelas</label>
			<select name='data[PagmidasCartoes][parcelas]'>
				<?php for($a = 1;$a<=12;$a++) : ?>
				<option value='<?=$a?>'><?=$a < 10 ? "0$a" : $a?></option>
				<?php endfor; ?>
			</select>
		</p>
	</div>
	<div class='grid_full first'>
		<?php echo $form->end(array('label' => 'Enviar', 'div' => false, 'class' => 'submit'));?>
	</div>
</div>