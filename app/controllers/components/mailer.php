<?php

class MailerComponent extends Object {

    var $smtpOptions = array(
        'port' => '587',
        'timeout' => '30',
        'host' => 'smtp.eventos.as',
        'username' => 'notificacoes@eventos.as',
        'password' => 'notificacoes12'
    );
    var $name = 'Mailer';
    var $Email;

    function __construct() {
        App::import('Component', 'Email');
        $this->Email = new EmailComponent();
    }
    
    function enviar($to, $subject, $message,$atach = false) {
        $this->config();
        $this->Email->from = "RK Formaturas - Sistema Online <{$this->smtpOptions['username']}>";
        $this->Email->to = $to;
        $this->Email->subject = $subject;
        //if($atach)
            //$this->Email->attachments = $atach;
        $resp = $this->Email->send($message);
        $this->Email->reset();
        return $resp;
    }

    function enviarEmail($to, $subject, $message) {
        $this->config();
        $this->Email->from = "RK Formaturas - Sistema Online <{$this->smtpOptions['username']}>";
        $this->Email->to = $to;
        $this->Email->sendAs = 'html';
        $this->Email->subject = $subject;
        $this->Email->send($message);
        $this->Email->reset();
    }

    function enviarEmailCancelamento($formando, $cc = false) {
        $email = 'notificacoes.cancelamentos@eventos.as';
        $this->config(array('username' => $email, 'password' => 'cancelamentos12'));
        $this->Email->from = "RK Formaturas - Sistema Online <$email>";
        $this->Email->to = $formando['nome'] . ' - ' . '<' . $formando['email'] . '>';
        if ($cc)
            $this->Email->cc = $cc;
        $this->Email->subject = 'Cancelamento de contrato';
        $mensagem = "O formando {$formando['nome']} cancelou o contrato";
        $this->Email->send($mensagem);
        $this->Email->reset();
    }
    
    function enviarEmailCancelamentoJustificadoFormando($usuario, $cc = false) {
        $email = 'notificacoes.cancelamentos@eventos.as';
        $this->config(array('username' => $email, 'password' => 'cancelamentos12'));
        $this->Email->from = "RK Formaturas - Sistema Online <$email>";
        $this->Email->to = $usuario['Usuario']['nome'] . ' - ' . '<' . $usuario['Usuario']['email'] . '>';
        if ($cc)
            $this->Email->cc = $cc;
        $this->Email->subject = 'Cancelamento de contrato';
        $this->Email->sendAs = 'html';
        $mensagem = "<h3>O seu contrato foi cancelado. Por favor, acesse o seu espaço e clique em <strong style='color: red'>'Devolução Pagamentos'</strong> "
                . "para que possamos concluir o processo de devolução.</h3><br />"
                . "<h4>ATT. RK Formaturas.</h4>";
        $this->Email->send($mensagem);
        $this->Email->reset();
    }
    
    function enviarEmailCancelamentoJustificativa($usuario, $formando, $justificativa) {
        $email = 'notificacoes.cancelamentos@eventos.as';
        $this->config(array('username' => $email, 'password' => 'cancelamentos12'));
        $this->Email->from = "RK Formaturas - Sistema Online <$email>";
        $this->Email->to = 'Danilo Poletto - < danilo.poletto@eventos.as >';
        $this->Email->subject = 'Pedido de cancelamento';
        $this->Email->sendAs = 'html';
        $mensagem = "<h4>A atendente {$usuario['Usuario']['nome']} solicita o cancelamento do(a) formando(a) {$formando['ViewFormandos']['nome']} da Turma {$formando['ViewFormandos']['turma_id']}. </h4>";
        $mensagem.= "<h4>Motivo:</h4> <em>{$justificativa}</em>";
        $this->Email->send($mensagem);
        $this->Email->reset();   
    }

    function enviarInformativo($titulo, $mensagem, $email, $nome) {
        $username = "informativos@eventos.as";
        $this->config(array('username' => $username, 'password' => 'informativos12'));
        $this->Email->from = "RK Formaturas - Sistema Online <$username>";
        $this->Email->to = $nome . ' - ' . '<' . $email . '>';
        $this->Email->subject = $titulo;
        $this->Email->sendAs = 'html';
        $mensagem = "Caro formando, enviaram um informativo no espaço do formando.<br /><br />\"<em>$mensagem</em>\"";
        $mensagem.= "<br /><br />Acesse " . Router::url('/', true) . " para mais inform&ccedil;&otilde;es";
        $resp = $this->Email->send($mensagem);
        $this->Email->reset();
        return $resp;
    }

    function config($smtpOptions = false, $delivery = false) {
        if ($smtpOptions)
            $this->smtpOptions = array_merge($smtpOptions, $this->smtpOptions);
        $this->Email->smtpOptions = $this->smtpOptions;
        if ($delivery)
            $this->Email->delivery = $delivery;
        else
            $this->Email->delivery = 'smtp';
    }

}