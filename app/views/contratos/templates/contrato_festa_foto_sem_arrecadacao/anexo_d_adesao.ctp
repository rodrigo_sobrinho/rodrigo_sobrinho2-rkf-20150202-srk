<?php if($out == "html") : ?>
<?php include('salvar_contrato.ctp')?>
<?php endif; ?>
<h4>ANEXO D - CONTRATO DE ADESÃO E COMPROMISSO</h4><br />
<p>
Pelo presente instrumento particular de prestação de serviços e na melhor forma de direito, o(a) formando(a) abaixo qualificado
</p>
Nome: ____________________________________________________ Data Nascimento: ___/___/________<br />
R.G.: ___________ CPF: _ _ _ . _ _ _ . _ _ _ - _ _ Rua/Av: _________________________________________<br />
Núm.: ____ Complemento: _____________ CEP: _ _ _ _ _ - _ _ _ Cidade: ____________________________<br />
Tel Residencial: ( _ _ ) _ _ _ _ - _ _ _ _ Tel Celular: ( _ _ ) _ _ _ _ _ - _ _ _ _ Outro: (_ _) _ _ _ _ _ - _ _ _ _<br />
Email: _____________________________________________(será o seu login no sistema da Contratada)<br />
Senha Acesso: _________(defina senha para o seu 1º acesso http://www.asformaturas.com.br/espaco_formando)<br />
adere ao Contrato de Prestação de Serviços de organização de Eventos firmado entre a <b>COMISSÃO DE FORMATURA DE SEU CURSO</b> 
e a empresa ÁS EVENTOS LTDA., inscrita no CNPJ/MF sob N.º06.163.144/0001-45, com sede na Rua Bogaris, 04- CEP 04047-020, 
São Paulo/SP, neste ato representada na forma de seu contrato social. Os dados básicos do contrato estão especificados abaixo, 
sendo que o contrato com todas as definições é considerado como <b>CONTRATO COLETIVO</b>.<br />
CÓDIGO DO CONTRATO - <b><?=$turma['Turma']['id']?></b> - ESTE CÓDIGO DEVE SER USADO PARA ADESÃO ONLINE - WWW.ASFORMATURAS.COM.BR<br />
DATA DE ASSINATURA DO CONTRATO ENTRE A COMISSÃO DE FORMATURA E A ÁS EVENTOS  <b><?=date("d/m/Y", strtotime($turma['Turma']['data_assinatura_contrato']))?></b><br />
Faculdade: _______________________________________  Curso: _________________________________ <br />
Sala/Turma: ________ Período: __________________<br /><br />
<b>OPÇÕES DE PAGAMENTO: </b> (os dados financeiros, bem como a opção de pagamento deverão ser preenchidas para validação)<br />
Valor R$  _________,_____  Número de parcelas ______
Data da Primeira parcela ____/_____/__________.
<br />
Opção de pagamento: (____) Cheques pré-datados (____)
Boletos online, sem custo (____) Boletos entregue em casa, custo de R$20,00.
<p style="text-align: justify">
<b>1.</b> O formando acima qualificado reconhece a Comissão de Formatura de sua turma como legítima representante para 
tratar dos assuntos relacionados à sua Formatura, outorgando poderes para representá-lo em atos referentes à sua formatura, 
ratificando os termos do CONTRATO COLETIVO, declarando ter plenos conhecimentos desse contrato e outorgando poderes para a Comissão 
celebrar aditivos contratuais que se fizerem necessários à realização dos eventos.<br />
<b>2.</b> As informações importantes a respeito do seu contrato estarão disponibilizadas no link - http://www.asformaturas.com.br/espaco_formando<br />
<b>3.</b> A Comissão de Formatura não poderá tratar assuntos de cunho financeiro, tais como forma de pagamento, renegociação de 
dívida ou cancelamento de contrato em nome do formando.<br />
<b>4.</b> O CONTRATANTE se obriga a pagar a CONTRATADA pelos eventos de formatura descritos no CONTRATO COLETIVO
e resumidos no boletim informativo entregue para o formando junto com esse contrato.<br />
<b>5. No momento da retirada dos convites o formando que optar por boletos bancários deverá quitar as parcelas em atraso e 
trocar os boletos ainda em aberto por cheques pré-datados. No mesmo momento, o formando que tiver optado por cheques pré-datados
e ainda não tiver os entregue, deverá entregar os mesmos.</b><br />
<b>6.</b> Os valores consignados neste contrato serão atualizados de 12 em 12 meses, a partir da assinatura do Contrato de Prestação
de Serviços celebrado entre a Comissão de Formatura e a ÁS EVENTOS LTDA., data essa disponibilizada no cabeçalho desse contrato,
adotando-se a variação do IGPM/FGV do período, conforme autoriza a Lei 9.069/95, ou por outro índice que venha a substituí-lo.
A ÁS EVENTOS não é autorizada a realizar qualquer outro tipo de reajuste de valores contratuais.<br />
<b>7.</b> Os convites serão entregues individualmente a cada contratante, após a quitação individual do contrato, seja pela 
efetiva quitação do mesmo ou pela substituição do débito em aberto por cheques pré-datados.<br />
<b>8.</b> No caso de rescisão contratual, o aluno poderá rescindir até 90 dias antes do término do ano letivo de conclusão
do curso, desde que pago a título de multa o valor dos itens ou brindes já entregues ou contratados para os formandos, 
adicionado de 20% do valor total do contrato. Após este período será exigido o pagamento integral do Contrato, exceto devido
a motivos justificados e comprovados, tais como reprovação ou transferência por motivos profissionais, nesse caso ficando o 
formando livre de multa.<br />
<b>9.</b> Constitui obrigação do (a) CONTRATANTE manter seu endereço e demais dados cadastrais atualizados, bem como 
a responsabilidade pela grafia correta de seu nome, nos textos originais que deverão ser entregues à CONTRATADA para a execução
dos serviços gráficos, eximindo a CONTRATADA pela falta do nome e/ou grafia incorreta conforme previsão no Contrato Principal.<br />
<b>10.</b> Se houver convite de luxo incluso no orçamento, os mesmos serão montados com fotos da turma e/ou formando, a 
Comissão de Formatura avisará todos os formandos da data/local e horário marcado junto à CONTRATADA para serem 
fotografados, estando o formando ciente que sua ausência isentará a CONTRATADA de qualquer responsabilidade referente às 
fotografias no convite. <br />
<b>11.</b> A empresa não se responsabiliza por objetos pessoais em todos os eventos organizados pela empresa.<br />
<b>12.</b> Fica eleito o Foro da Capital do Estado de São Paulo por mais privilegiado que outro seja.<br /><br />
<table border="0">
<tr style="text-align: center">
<td><b>E assim por estarem justos e contratados,</b></td>
<td>São Paulo, ______ de ________________ de ________.</td>
</tr>
</table>
<br />
<br />
<br />
</p>
<br />
<table border="0">
<tr style="text-align: center">
<td><?=str_pad("",25,"_")?></td>
<td><?=str_pad("",25,"_")?></td>
</tr>
<tr style="text-align: center; border: 0">
<td>CONTRATANTE</td>
<td>AS EVENTOS</td>
</tr>
</table>