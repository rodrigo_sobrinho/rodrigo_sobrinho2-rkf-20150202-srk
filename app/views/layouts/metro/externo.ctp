<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
    <meta name="google-site-verification" content="Ip4-BMIP8v6t5ky279zPi7nhLEHl3vxifr2ccDarP30" />
    <?= $html->charset(); ?>
    <title><?= $title_for_layout; ?></title>
    <link rel="icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon"> 
    <link rel="shortcut icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon"> 
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootmetro/default.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootmetro/icons.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootmetro/responsive.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/fonts.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/typography.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/color.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/form.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/button.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/default.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/default.css?v=0.1">
    <?php $css_stylesheets = isset($css_stylesheets) ? $css_stylesheets : array();
    foreach ($css_stylesheets as $value) : ?>
        <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot.$value ?>">
    <?php endforeach; ?>
    <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootmetro/icons-ie7.css?v=0.1">
    <![endif]-->
    <script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/modernizr-2.6.2.js?v=0.1"></script>
    <script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/jquery-1.9.1.js?v=0.1"></script>
    <script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/knockout/knockout-2.2.1.js?v=0.1"></script>
    <script>window.jQuery || document.write("<script src='<?= $this->webroot ?>metro/js/min/jquery-1.9.1.js'>\x3C/script>")</script>
    <script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/metroui/input-control.js?v=0.1"></script>
    <script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/dropdown.js?v=0.1"></script>
    <script type="text/javascript" src="<?= $this->webroot ?>metro/js/max/knockout/content_login.js?v=0.1"></script>
</head>
<body>
    <div id="menu-topo" class="navbar navbar-fixed-top">
        <div class="navbar-inner bg-color-orange">
            <div class="container todo">
                <a class="brand">
                    <img src="<?=$this->webroot ?>metro/img/logo_rk_branco_min3.png" />
                    <!-- <div class='logo-min-branco'></div> -->
                </a>
            </div>
        </div>
    </div>
    <div class="container todo">
        <div class="row-fluid" id="conteudo">
            <div style="min-height:90px; display:block"></div>
            <div class="row-fluid"><?= $content_for_layout; ?></div>
        </div>
    </div>
    <footer id="footer-body" class="bg-color-orange">
        <div>
            <p>formaturas <?=date("Y") ?> - Todos os direitos reservados &reg;</p>
        </div>
    </footer>
</body>
</html>
