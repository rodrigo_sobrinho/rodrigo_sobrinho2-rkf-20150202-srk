<span id="conteudo-titulo" class="box-com-titulo-header">Parceiros</span>
<div id="conteudo-container">
	<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
	<?php $session->flash(); ?>
	<div class="tabela-adicionar-item">
		<?php echo $html->link('Novo Parceiro',array($this->params['prefix'] => true, 'action' => 'adicionar')); ?>
		<div style="clear:both;"></div>
	</div>
	<div class="container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col"><?php echo $paginator->sort('Nome', 'nome'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Descrição', 'descricao'); ?></th>				
					<th scope="col"><?php echo $paginator->sort('Ativo', 'ativo'); ?></th>
					<th scope="col"> &nbsp;</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="3"><?php echo $paginator->counter(array('format' => 'Parceiros %start% à %end% - página %page% de %pages%')); ?>
						<span class="paginacao">
							<?php echo $paginator->numbers(array('separator' => ' ')); ?>
						</span>
					</td>
					<td colspan="1"><?php echo $paginator->counter(array('format' => 'Total : %count% ' .  $this->name)); ?></td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach($parceiros as $parceiro): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<td colspan="1" width="25%"><?php echo $parceiro['Parceiro']['nome'];?></td>
					<td colspan="1" width="40%"><?php echo $parceiro['Parceiro']['descricao'];?></td>
					<td colspan="1" width="5%"><?php echo ($parceiro['Parceiro']['ativo'] == '1') ? 'Sim' : 'Não';?></td>
					<td colspan="1" width="30%">
						<?php echo $html->link('Visualizar', array($this->params['prefix'] => true, 'controller' => 'parceiros', 'action' =>'visualizar', $parceiro['Parceiro']['id']), array('class' => 'submit button')); ?>
						<?php echo $html->link('Editar', array($this->params['prefix'] => true, 'controller' => 'parceiros', 'action' =>'editar', $parceiro['Parceiro']['id']), array('class' => 'submit button')); ?>
						<?php 
						if ($parceiro['Parceiro']['ativo'] == '1')
							echo $html->link('Desativar', array($this->params['prefix'] => true, 'controller' => 'parceiros', 'action' =>'desativar', $parceiro['Parceiro']['id']), array('class' => 'submit button')); 
						else
							echo $html->link('Ativar', array($this->params['prefix'] => true, 'controller' => 'parceiros', 'action' =>'ativar', $parceiro['Parceiro']['id']), array('class' => 'submit button')); 
						
						?>
						<?php //echo $html->link('Buscar Cupom', array($this->params['prefix'] => true, 'controller' => 'parceiros', 'action' =>'buscar_parceiro', $parceiro['Parceiro']['id']), array('class' => 'submit button')); ?>
                    </td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>