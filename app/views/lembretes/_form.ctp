<?php echo $form->input('id'); ?>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Título</label>
	<?php echo $form->input('titulo', array('class' => 'grid_8 alpha first', 'label' => false, 'div' => false,'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Texto</label>
	<?php echo $form->textarea('texto', array('class' => 'grid_8 alpha first', 'label' => false, 'div' => false,'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>
