<script type="text/javascript">
    $(document).ready(function() {
        var context = ko.contextFor($("#content-body")[0]);
        $("#enviar").click(function() {
            context.$data.showLoading(function() {
                $.ajax({
                    type : 'POST',
                    url : $("#formulario").attr("action"),
                    dataType : 'json',
                    data : $("#formulario").serialize(),
                    error : function() {
                        alert("Erro ao enviar dados");
                    },
                    complete : function() {
                        context.$data.page("<?=$this->params['prefix']?>/material_venda/listar/<?=$evento["TiposEvento"]['id']?>");
                    }
                });
            });
        });
    });
</script>
<h2>
    <a class="metro-button back" data-bind="click: loadThis" href="<?=$this->params['prefix']?>/material_venda/listar/<?=$evento["TiposEvento"]['id']?>"></a>
    <a class="metro-button reload" data-bind="click: reload"></a>
    Item
</h2>
<?=$form->create('MaterialVendaItem',array(
    'url' => $this->here,
    'id' => 'formulario')) ?>
<?=$form->hidden("MaterialVendaItem.id");?>
<?=$form->hidden("MaterialVendaItem.tipos_evento_id");?>
<?=$form->hidden("MaterialVendaItem.ordem");?>
<div class="row-fluid">
    <div class="span6">
        <?=$form->input('MaterialVendaItem.nome', array(
            'label' => "Nome",'error' => false,
            'div' => 'input-control text')); ?>
    </div>
</div>
<button type="button" class="button bg-color-greenDark" id="enviar">
    Enviar
</button>
<?= $form->end(array('label' => false, 'div' => false, 'class' => 'hide')); ?>