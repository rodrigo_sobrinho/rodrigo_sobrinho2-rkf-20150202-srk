<script type="text/javascript">
    $(document).ready(function() {
        var context = ko.contextFor($("#content-body")[0]);
        $("#enviar").click(function() {
            context.$data.showLoading(function() {
                $.ajax({
                    type : 'POST',
                    url : $("#formulario").attr("action"),
                    dataType : 'json',
                    data : $("#formulario").serialize(),
                    error : function() {
                        alert("Erro ao enviar dados");
                    },
                    complete : function() {
                        context.$data.page("<?=$this->params['prefix']?>/material_venda/listar");
                    }
                });
            });
        });
    });
</script>
<h2>
    <a class="metro-button back" data-bind="click: loadThis" href="<?=$this->params['prefix']?>/material_venda/listar"></a>
    <a class="metro-button reload" data-bind="click: reload"></a>
    Eventos
</h2>
<?=$form->create('TiposEvento',array(
    'url' => $this->here,
    'id' => 'formulario')) ?>
<?=$form->hidden("TiposEvento.id");?>
<div class="row-fluid">
    <div class="span6">
        <?=$form->input('TiposEvento.nome', array(
            'label' => "Nome",'error' => false,
            'div' => 'input-control text')); ?>
    </div>
</div>
<button type="button" class="button bg-color-greenDark" id="enviar">
    Enviar
</button>
<?= $form->end(array('label' => false, 'div' => false, 'class' => 'hide')); ?>