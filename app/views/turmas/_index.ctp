<div id="conteudo-container">
    	<?php $session->flash(); ?>
    	<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
    	<div class="tabela-adicionar-item">
			<?php echo $form->create(false, array('url' => "/{$this->params['prefix']}/turmas", 'class' => 'procurar-form-inline')); ?>
			<span>ID: <?php echo $form->input('filtro-id', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?> </span>
			<?php echo $form->input('filtro-status', array('options' => $statusExistentesParaTurmas, 'type' => 'select', 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
			<?php echo $form->end(array('label' => ' ', 'div' => false, 'class' => 'submit-busca')); ?>
			<div style="clear:both;"></div>
		</div>
        <div class="container-tabela">
            <table>
                <thead>
                    <tr>
                        <th scope="col"><?php echo $paginator->sort('Id', 'id'); ?></th>
                        <th scope="col"><?php echo $paginator->sort('Nome', 'nome'); ?></th>					
                        <th scope="col"><?php echo $paginator->sort('Ano', 'ano_formatura'); ?></th>
                        <th scope="col"><?php echo $paginator->sort('Nº Formandos', 'expectativa_formandos'); ?></th>
                        <th scope="col"><?php echo $paginator->sort('Expectativa', 'expectativa_fechamento'); ?></th>
                        <th scope="col"><?php echo $paginator->sort('Status', 'status'); ?></th>
                        <th scope="col"><?php echo $paginator->sort('Como Chegou', 'como_chegou'); ?></th>
                        <!-- <th scope="col"><?php echo $paginator->sort('Pretensão', 'pretensao'); ?></th> -->
                        <th scope="col"> &nbsp;</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td colspan="6"><?php echo $paginator->counter(array('format' => 'Itens %start% ao %end% - página %page% de %pages%')); ?>
                            <span class="paginacao">
							<?php echo $paginator->numbers(array('separator' => ' ')); ?>
                            </span>
                        </td>
                        <td colspan="2"><?php echo $paginator->counter(array('format' => 'Total : %count% ' .  $this->name)); ?></td>
                    </tr>
                </tfoot>
                <tbody>
			<?php $isOdd = false; ?>
			<?php foreach ($turmas as $turma): ?>
				<?php if($isOdd):?>
                    <tr class="odd">
				<?php else:?>
                    <tr>
				<?php endif;?>
                        <td  colspan="1" width="3%"><?php echo $turma['Turma']['id']; ?></td>
                        <td  colspan="1" width="30%"><?php echo $turma['Turma']['nome']; ?></td>					
                        <td  colspan="1" width="3%"><?php echo $turma['Turma']['ano_formatura']; ?></td>
                        <td  colspan="1" width="10%"><?php echo $turma['Turma']['expectativa_formandos']; ?></td>
                        <td  colspan="1" width="5%"><?php echo $turma['Turma']['expectativa_fechamento']; ?></td>
                        <td  colspan="1" width="8%"><?php echo $turma['Turma']['status']; ?></td>
                        <td  colspan="1" width="8%"><?php echo $turma['Turma']['como_chegou']; ?></td>
                        <!-- ><td  colspan="1" width="10%"><?php echo $turma['Turma']['pretensao']; ?></td>-->
                        <td  colspan="1" width="10%">
						<?php echo $html->link('Selecionar', array($this->params['prefix'] => true, 'controller' => 'turmas','action' =>'visualizar', $turma['Turma']['id']), array('class' => 'submit button')); ?>
                        </td>
                    </tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
                </tbody>
            </table>
        </div>
</div>