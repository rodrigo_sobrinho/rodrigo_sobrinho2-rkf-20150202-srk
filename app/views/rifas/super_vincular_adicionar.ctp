<div class='box-com-titulo'>
<span id="conteudo-titulo" class="box-com-titulo-header">Rifa - Vincular</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<?php echo $form->create('RifaTurma', array('url' => "/{$this->params['prefix']}/rifas/vincular_adicionar")); ?>			
	
	<p class="grid_11 alpha omega">
		<label class="grid_5 alpha">Rifa</label>
		<?php echo $form->select('RifaTurma.rifa_id', $rifas, null, array('empty' => '-- Selecione uma Rifa --', 'class' => 'grid_10 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10 first'))); ?>
	</p>
	
	<p class="grid_11 alpha omega">
		<label class="grid_5 alpha">Turma</label>
		<?php echo $form->select('RifaTurma.turma_id', $turmas, null, array('empty' => '-- Selecione uma Turma --', 'class' => 'grid_10 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10 first'))); ?>
	</p>
	<p class="grid_11 alpha omega">
		<label class="grid_5 alpha">Cupons</label>
		<?php echo $form->select('RifaTurma.cupons', $cupons, null, array('empty' => '0', 'class' => 'grid_4 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10 first'))); ?>
	</p>
	<p class="grid_11 alpha omega">
		<label class="grid_5 alpha">Ativa</label>
		<?php echo $form->input('RifaTurma.ativo',array('class' => 'grid_4 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
	</p>
	<p class="grid_11 alpha omega">
		<?php echo $html->link('Voltar',array( $this->params['prefix'] => true, 'action' => 'vincular') ,array('class' => 'cancel')); ?>
		<?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit'));?>
	</p>
</div>
</div>
