<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/datepicker.css">
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/bootstrap/datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    var salvar = $("<button>",{
        type: 'submit',
        class: 'button bg-color-greenDark salvar',
        text: 'Salvar'
    });
    $('.modal-footer').prepend(salvar);
    $(".selectpicker").selectpicker({width:'100%', dropdown_position: "down"});
    $(".selectpicker.extra").change(function(){
        if($('.selectpicker.extra option').filter(':selected').text() == 'Brinde'){
            $('.brinde').fadeIn('slow');
        }else{
            $('#ExtraBrinde').val('');
            $('.brinde').fadeOut('slow');
        }
    });
    $(".selectpicker").change(function(){
        if($('.selectpicker.extra option').filter(':selected').text() == 'Outros'){
            $('.outros').fadeIn('slow');
        }else{
            $('#ExtraOutros').val('');
            $('.outros').fadeOut('slow');
        }
    });
    $(".selectpicker.extra").on('change', function(){
        var itemSelecionado = $('.selectpicker option').filter(':selected').text();
        var itens = ["", "Convite Infantil", "Convite Luxo", "Convite Extra", "Combo" ,"Mesa Extra", "Brinde", "Outros"];
        if($.inArray(itemSelecionado, itens)){
            if(itemSelecionado.substr(0, 13) == "Convite Extra"){
                $('#extraQuantidadeConvites').attr('disabled','disabled');
                $('#extraQuantidadeMesas').attr('disabled','disabled');
                $('#extraQuantidadeMesas').val(0);
                $('#extraQuantidadeConvites').val(1);
            }
            if(itemSelecionado.substr(0, 16) == "Convite Infantil"){
                $('#extraQuantidadeConvites').attr('disabled','disabled');
                $('#extraQuantidadeMesas').attr('disabled','disabled');
                $('#extraQuantidadeMesas').val(0);
                $('#extraQuantidadeConvites').val(0);
            }
            if(itemSelecionado.substr(0, 12) == "Convite Luxo"){
                $('#extraQuantidadeConvites').attr('disabled','disabled');
                $('#extraQuantidadeMesas').attr('disabled','disabled');
                $('#extraQuantidadeMesas').val(0);
                $('#extraQuantidadeConvites').val(0);
            }
            if(itemSelecionado.substr(0, 6) == "Brinde"){
                $('#extraQuantidadeConvites').attr('disabled','disabled');
                $('#extraQuantidadeMesas').attr('disabled','disabled');
                $('#extraQuantidadeMesas').val(0);
                $('#extraQuantidadeConvites').val(0);
            }
            if(itemSelecionado.substr(0, 6) == "Outros"){
                $('#extraQuantidadeConvites').attr('disabled','disabled');
                $('#extraQuantidadeMesas').attr('disabled','disabled');
                $('#extraQuantidadeMesas').val(0);
                $('#extraQuantidadeConvites').val(0);
            }
            if(itemSelecionado.substr(0, 10) == "Mesa Extra"){
                $('#extraQuantidadeConvites').attr('disabled','disabled');
                $('#extraQuantidadeMesas').attr('disabled','disabled');
                $('#extraQuantidadeMesas').val(1);
                $('#extraQuantidadeConvites').val(0);
            }
            if(itemSelecionado.substr(0, 5) == "Combo"){
                $('#extraQuantidadeConvites').attr('disabled','disabled');
                $('#extraQuantidadeMesas').attr('disabled','disabled');
                $('#extraQuantidadeMesas').val(1);
                $('#extraQuantidadeConvites').val(10);
            }
        }else{
            $('#extraQuantidadeConvites').removeAttr('disabled');
            $('#extraQuantidadeMesas').removeAttr('disabled');
            $('#extraQuantidadeMesas').val('');
            $('#extraQuantidadeConvites').val('');
        }
    });
    $(".salvar").click(function(e) {
        e.preventDefault();
        $('#extraQuantidadeConvites').removeAttr('disabled');
        $('#extraQuantidadeMesas').removeAttr('disabled');
        var context = ko.contextFor($(".metro-button.reload")[0]);
        var dados = $("#form").serialize();
        var url = $("#form").attr('action');
        bootbox.hideAll();
        context.$data.showLoading(function() {
            $.ajax({
                url: url,
                data: dados,
                type: "POST",
                dataType: "json",
                complete: function() {
                    context.$data.reload();
                }
            });
        });
    });
});
</script>
<div class="row-fluid">
    <?php $session->flash(); ?>
    <?=$form->create('CheckoutItem', array('url' => "/{$this->params['prefix']}/checkout/inserir_itens", 'id' => 'form')); ?>
    <?=$form->hidden('CheckoutItem.turma_id', array('value' => $turma['Turma']['id'])); ?>
    <div class="row-fluid">
        <div class="span9">
            <?=$form->input('CheckoutItem.titulo',array(
                'label' => 'Tipo de Extra',
                'type' => 'select',
                'class' => 'selectpicker extra',
                'options' => $extras,
                'div' => 'input-control text',
                'error' => false,
            )); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span9 hide outros">
                <?php echo $form->input('CheckoutItem.outros', array(
                    'label' => 'Digite o Nome',
                    'div' => 'input-control',
                    'error' => false)); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span9 hide brinde">
                <?php echo $form->input('CheckoutItem.brinde', array(
                    'label' => 'Digite o Nome',
                    'div' => 'input-control',
                    'error' => false)); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span9">
            <?=$form->input('CheckoutItem.evento_id',array(
                'label' => 'Tipo de Evento',
                'type' => 'select',
                'class' => 'selectpicker',
                'options' => $eventos,
                'div' => 'input-control text',
                'error' => false,
            )); ?>
        </div>
    </div> 
    <div class="row-fluid">
        <div class="span3">
                <?php echo $form->input('CheckoutItem.itens_por_pessoa', array(
                    'label' => 'Qtde por Pessoa',
                    'div' => 'input-control text',
                    'error' => false)); ?>
        </div>
        <div class="span3">
                <?php echo $form->input('CheckoutItem.valor', array(
                    'label' => 'Valor',
                    'div' => 'input-control text',
                    'error' => false)); ?>
        </div>
        <div class="span3">
                <?php echo $form->input('CheckoutItem.quantidade', array(
                    'label' => 'Quantidade Total',
                    'div' => 'input-control text',
                    'error' => false)); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span3">
                <?php echo $form->input('CheckoutItem.quantidade_convites', array(
                    'label' => 'Qtde Convites',
                    'id' => 'extraQuantidadeConvites',
                    'div' => 'input-control text',
                    'error' => false)); ?>
        </div>
        <div class="span3">
                <?php echo $form->input('CheckoutItem.quantidade_mesas', array(
                    'label' => 'Qtde Mesas',
                    'id' => 'extraQuantidadeMesas',
                    'div' => 'input-control text',
                    'error' => false)); ?>
        </div>
    </div>
    <div class="row-fluid">
        <?=$form->end(array(
            'label' => false,
            'div' => false,
            'class' => 'hide'
        ));?>
    </div>
</div>