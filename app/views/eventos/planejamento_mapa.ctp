<?php $session->flash(); ?>
<?php if($evento) : ?>
	<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
	<script type="text/javascript" src="<?= $this->webroot ?>js/mapas-de-mesas/jcanvas.min.js"></script>
	<script type="text/javascript" src="<?= $this->webroot ?>js/mapas-de-mesas/main.js"></script>
	<script type="text/javascript" src="<?= $this->webroot ?>js/mapas-de-mesas/agendador-mesas-1.0.0.js"></script>
</script>
<style type="text/css">
	.menu {
		position:fixed;
		background: white;
		right:15px;
		top:60px;
		padding: 5px;
	}
	.info-formando table { 
		box-shadow: 5px 3px 4px #D0D0D0;
		border: #B2B2B2 solid 1px;
	}
	.info-formando table td { 
		padding: 5px;
	}
</style>
	<img id="mapa_imagem" src="<?php echo $webroot.$mapa_src ?>" class="hide"  />
	<h2 class="fg-color-red">
		<?="{$evento['TiposEvento']['nome']} - {$evento['Evento']['nome']}"?>
	</h2>
	<br />
	<div class="row-fluid striped">
		<div class="row-fluid"id="menu">
			<div class="span9 pull-left">
				<canvas width="964" id="meucanvas" height="792">
				</canvas>
			</div>
			<div class="menu info-formando">
				<h3>Informações da Mesa</h3>
				<div class="mesa-info">
					<p></p>
				</div>
				<br/>
				<br/>
				<h4 id="exibir-legenda"><a href="javascript:void(0)">Legenda</a></h4>
				<div id='legenda' >
					<table class="table-striped sombra">
						<tr>
							<td align="center"><img src="../../img/green_button.png"></td>
							<td>Mesa Livre</td>
						</tr>
						<tr>
							<td align="center"><img src="../../img/red_button.png"></td>
							<td>Sua(s) mesa(s)</td>
						</tr>
						<tr>
							<td align="center"><img src="../../img/yellow_button.png"></td>
							<td>Mesa Reservada</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
<?php else : ?>
	<h2 class="fg-color-red">Nenhuma festa encontrada para esta turma</h2>
<?php endif; ?>
	<script type="text/javascript">

		/**
		 * Função de Callback chamada ao passar mouse em cima de uma mesa
		 * @param  {[type]} mesa Objeto Mesa
		 * @return {[type]}      [description]
		 */
		 function mostrarInfoMesa(mesa) {
		 	$('.mesa-info p').html('Mesa '+mesa.formaMesa.letra+mesa.formaMesa.numero+' - ' + mesa.texto);
		 }
		/**
		* Inicialização de Ferramenta de Mapa de Mesas
		*/

		MapaDeMesa.SETOR_FORMANDO = '';
		MapaDeMesa.PREFIXO = '<?php echo $this->params['prefix'] ?>';

	    palco = new AgendadorMesas.Palco("meucanvas");
	    palco.desenharBackground();

	    MapaDeMesa.MesaFactory = MapaDeMesa.AgendadorMesas.MesaFactory;
	    
	    params = <?php echo $mapa_parametros ?>;
	    fabrica = new MapaDeMesa.MesaFactory(palco,params.raio,params.distancia);

	    fabricaRetangulo = new AgendadorMesas.RectangleFactory(palco);

	    palco.aoPassarEmCimaMesaListener(mostrarInfoMesa);

		layout = new AgendadorMesas.GerenciadorLayout(palco);
		layout_json = '<?php echo isset($mapaLayout) ? $mapaLayout : ''; ?>';
		mesasSelecionadas_json = '<?php echo isset($mesasSelecionadas) ? $mesasSelecionadas : ''; ?>';

	    layout.desserializarLayout(layout_json, fabrica,fabricaRetangulo);
	    palco.desserializarEscolhidas(mesasSelecionadas_json);

		$("#exibir-legenda").click(function(){
			$("#legenda").toggle();
		});
	</script>