<span id="conteudo-titulo" class="box-com-titulo-header">Informações do Membro da Comissão</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="detalhes">
		<p class="grid_15 alpha omega">
			<label class="grid_15 alpha">Nome</label>
			<span class="grid_15 alpha first"><?php echo $formando['Usuario']['nome']; ?></span>
		</p>
		<p class="grid_15 alpha omega">
			<label class="grid_7 alpha">E-mail</label>
			<label class="grid_8 alpha">RG</label>
			<span class="grid_7 alpha first"><?php echo $formando['Usuario']['email']; ?></span>
			<span class="grid_8 alpha"><?php echo $formando['FormandoProfile']['rg']; ?></span>
		</p>
		<p class="grid_15 alpha omega">
			<label class="grid_7 alpha">Telefone Residencial</label>
			<label class="grid_8 alpha">Telefone Comercial</label>
			<span class="grid_7 alpha first"><?php echo $formando['FormandoProfile']['tel_residencial']; ?></span>
			<span class="grid_8 alpha"><?php echo $formando['FormandoProfile']['tel_comercial']; ?></span>
		</p>
		<p class="grid_15 alpha omega">
			<label class="grid_7 alpha">Telefone Celular</label>
			<label class="grid_8 alpha">E-mail</label>
			<span class="grid_7 alpha first"><?php echo $formando['FormandoProfile']['tel_celular']; ?></span>
			<span class="grid_8 alpha"><?php echo $formando['Usuario']['email']; ?></span>
		</p>
		<p class="grid_15 alpha omega">
			<label class="grid_15 alpha">Cargo</label>
			<span class="grid_15 alpha first"><?php echo $formando['FormandoProfile']['cargo_comissao']; ?></span>			
		</p>
		<p class="grid_11 alpha omega">
			<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index', 'controller' => 'principal') ,array('class' => 'cancel')); ?>
		</p>
	</div>
</div>