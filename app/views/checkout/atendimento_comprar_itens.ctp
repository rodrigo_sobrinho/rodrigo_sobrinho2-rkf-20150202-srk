<?= $html->script('checkout_comprar_itens'); ?>
<style type="text/css">
    table tr td, tr th {text-align:left; padding:10px!important; font-size: 12px; }
    .number {text-align:right!important; padding:10px!important; font-size: 12px; }
</style>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">

<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery.meio.mask.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery.meio.mask.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
        checkout = obterCheckout();
        var itens = <?= json_encode($itensExtras ? $itensExtras : array()) ?>;
        checkout.extras.iniciar(itens);
        $('.item-select').change(function() {
            var id = $(this).attr('id').substr(12);
            var idValorEspecial = "#valor-especial-"+id;
            checkout.extras.comprar($(this).val(), id, idValorEspecial);
        });
        $('.valor-especial').focusout(function() {
            var id = $(this).attr('id').substr(15);
            var idValorEspecial = "#valor-especial-"+id;
            checkout.extras.comprar($("#item-select-"+id).val(), id, idValorEspecial);
        });
        $('select[name="forma_pagamento"]').change(function() {
            if ($(this).val() == "cheque")
                $(".data-cheque").stop().fadeIn(500);
            else
                $(".data-cheque").stop().fadeOut(500);
        });
        $("#comprar-itens").click(function(e) {
            e.preventDefault();
            $(".modal-body").html('<h2>Carregando</h2>');
            $(".modal-body").load($(this).attr('href'));
            //$(".modal-body").load('<?=$this->here?>');
        });
        $('select[name="parcelas"]').change(function() {
            checkout.total.atualizar();
        });
        $('input[alt]').setMask();
        $(".confirmar-compra").click(function() {
            var context = ko.contextFor($(".metro-button.reload")[0]);
            if ($('select[name="forma_pagamento"]').val() == "cheque" && $('input[name="data-cheque"]').val() == "") {
                alert('Preencha a data base do cheque');
                $('input[name="data-cheque"]').focus();
            } else {
                var dados = {};
                dados.pagamento = {
                    parcelas: $('select[name="parcelas"]').val(),
                    forma_pagamento: $('select[name="forma_pagamento"]').val(),
                    data_base: $('input[name="data-cheque"]').val(),
                    obs: $('[name="obs"]').val(),
                    valor: $('[name="valor-parcela"]').val().replace('R$', '').replace('.', '').replace(',', '.')
                };
                dados.formando = <?=json_encode($formando) ?>;
                dados.itens = [];
                $.each(JSON.parse(JSON.stringify(checkout.extras.itens, null, 2)),function(i,item) {
                    if(item.comprados != undefined)
                        dados.itens.push(item);
                });

                bootbox.confirm('Tem ceteza que deseja efetuar a compra?',function(response) {
                    if(response) {
                            $.ajax({
                                url: '/<?= $this->params["prefix"] ?>/checkout/comprar_itens',
                                data: dados,
                                type: "POST",
                                dataType: "json",
                                success: function(response) {
                                    if(response.erro) {
                                        bootbox.alert("Não foi possível efetuara compra");
                                    } else {
                                        $(".modal-body").text("Compra realizada com sucesso!");
                                        var imprimir = $("<button>",{
                                            type: 'submit',
                                            class: 'button bg-color-red',
                                            id:'imprimir',
                                            text:'Imprimir' 
                                        });
                                        $('.modal-footer').prepend(imprimir);
                                        $('#imprimir').click(function(){
                                            var url = '/<?= $this->params["prefix"] ?>/solicitacoes/imprimir_compra_pos_checkout/<?=$protocolo["Protocolo"]["id"]?>';
                                            window.open(url, '_blank');
                                        });
                                    }
                                },
                                error: function() {
                                    bootbox.alert("Ocorreu um erro com os nossos servidores. Por favor tente mais tarde.");
                                }
                            });
                    }
                });
            }
        });
});
</script>
<div class="row-fluid">
    <h2>
        <span class="metro-button back" id="comprar-itens"
            href="/<?=$this->params['prefix']?>/area_financeira/dados/<?=$formando['id']?>"></span>
        Comprar Itens Checkout
    </h2>
</div>
<div class="grid_full">
    <table class="table-striped">
        <thead>
            <tr>
                <th>Item</th>
                <th class="number">Valor</th>
                <th>Valor Especial</th>
                <th class="number">Máxima de Itens</th>
                <th>Selecione a Qtde.</th>
                <th class="number">Valor Total</th>
            </tr>
        </thead>
        <tbody>
<?php foreach ($itensExtras as $itemExtra) { ?>
                <tr>
                    <td><?= $itemExtra['titulo'] ?></td>
                    <td class="number">R$<?= number_format($itemExtra['valor'], 2, ',', '.'); ?></td>
                    <td><input type="text" value='' class="valor-especial" id='valor-especial-<?= $itemExtra['id'] ?>' /></td>
                    <td class="number"><?= $itemExtra['itens_por_pessoa']; ?></td>
                    <td>
                        <select name="data[itemExtra]" class="item-select" id="item-select-<?= $itemExtra['id'] ?>">
                            <?php for ($a = 0; $a <= $itemExtra['itens_por_pessoa']; $a++) : ?>
                                <option value="<?= $a ?>"><?= $a ?></option>
                            <?php endfor; ?>
                        </select>
                    </td>   
                    <td class='number saldo-item-<?= $itemExtra['id'] ?>'>-</td>
                </tr>
<?php } ?>
    </tbody>
    </table>
    <table>
            <tr>
                <td style="display: none">
                    <select name='parcelas'>
                        <option value='1'>1</option>
                    </select>
                </td>
                <td><strong>Soma dos Itens</strong></td>
                <td class='soma-itens'>-</td>
            </tr>
            <tr>
                <td><strong>Forma de pagamentos</strong></td>
                <td>
                    <select name='forma_pagamento'>
                        <option value='cheque'>Cheque</option>
                        <option value='dinheiro'>Dinheiro</option>
                        <option value='comprovante'>Comprovante</option>
                    </select>
                </td>
                <td colspan='3'></td>
            </tr>
            <tr class='data-cheque'>
                <td><strong>Data base dos cheques</strong></td>
                <td>
                    <input type="text" name="data-cheque" alt="99/99/9999" />
                </td>
                <td colspan='3'></td>
            </tr>
            <tr>
                <td><strong>Valor da Parcela</strong></td>
                <td>
                    <input type="text" readonly="readonly" value='' name='valor-parcela' />
                </td>
                <td colspan='3'></td>
            </tr>
            <tr>
                <td><strong>Observações</strong></td>
                <td>
                    <textarea name='obs' rows='5' cols='20'></textarea>
                </td>
                <td colspan='3'></td>
            </tr>
            <tr>
                <td>
                    <span class='button default bg-color-blueDark submit confirmar-compra'>Confirmar Compra</span>
                </td>
            </tr>
    </table>
</div>