<?php

class CheckoutReciboPagamento extends AppModel {
    var $name = "CheckoutReciboPagamento";
    var $belongsTo = array(
        'CheckoutRecibo',
        'CheckoutFormandoPagamento' => array(
            'className' => 'CheckoutFormandoPagamento',
            'foreignKey' => 'checkout_pagamento_id'
        )
    );
}
?>