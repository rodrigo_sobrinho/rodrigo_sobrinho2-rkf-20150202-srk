<?php

class FormandosController extends AppController {

    var $components = array('JqImgcrop', 'Email','GerenciadorDeDownload');
    var $name = 'Formandos';
    var $uses = array('Evento','Extra', 'CampanhasUsuario', 'CampanhasUsuarioCampanhasExtra',
        'Pagamento', 'Campanha', 'FormandoProfile', 'Usuario', 'Turma', 'Curso', 'Parcelamento',
        'Despesa', 'TurmasUsuario', 'CursoTurma', 'CampanhasExtra','ViewFormandos');
    var $helpers = array('Cropimage', 'Excel');
    var $paginate = array(
        'limit' => 700,
        'order' => array(
            'Usuario.nome' => 'asc'
        ),
        'fields' => array('Usuario.id'),
        'ViewFormandos' => array('limit' => 100)
    );
    var $app = array('cursos','verificar_turma','cadastrar',
        'fotos_telao','inserir_nome_telao','inserir_fotos_telao');

    function beforeFilter() {
        $this->Auth->allow('inicio', 'selecionar_turma', 'confirmar_turma', 'cadastro',
                'cadastrar', 'adicionar', 'obrigado', 'parcelas', 'parcelamentos',
                'planos', 'formando_verificaEmail');
        parent::beforeFilter();
    }

    function atendimento_inserir_pagamento($uid = false){
        $this->layout = 'metro/externo';
        if(!empty($this->data)){
            $this->Pagamento->save(
                array(
                    'usuario_id' => $this->data['ViewFormandos']['usuario_id'],
                    'codigo' => NULL,
                    'dt_cadastro' => '2015-01-12',
                    'dt_vencimento' => '2015-01-12',
                    'dt_liquidacao' => '2015-01-12',
                    'tipo' => 'boleto',
                    'status' => 'pago',
                    'valor_nominal' => $this->data['ViewFormandos']['valor'],
                    'valor_pago' => $this->data['ViewFormandos']['valor'],
                )
            );
        }
        $formando = $this->ViewFormandos->findById($uid);
        $this->set('formando', $formando);
    }
    
    function remover_arquivo_telao() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $turma = $this->obterTurmaLogada();
        $arquivo = "fotos_telao_{$turma['Turma']['id']}.zip";
        $path = APP . "webroot/upload/turmas/$arquivo";
        if(file_exists($path))
            unlink ($path);
        echo json_encode(array());
    }
    
    function criar_arquivo_telao() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $turma = $this->obterTurmaLogada();
        $caminho = APP."../cake/console/cake.php fotos_telao criar_zip";
        $comando = "php {$caminho} {$turma['Turma']['id']} > /dev/null 2>&1 &";
        exec($comando);
        echo json_encode(array());
    }
    
    function verificar_arquivo_telao(){
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $turma = $this->obterTurmaLogada();
        $arquivo = APP ."webroot/upload/turmas/fotos_telao_{$turma['Turma']['id']}.zip";
        echo json_encode(array('file' => file_exists($arquivo)));
    }
    
    private function _baixar_fotos_telao(){
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $this->aumentar_memoria("2056M");
        $json = array('error' => true);
        $turma = $this->obterTurmaLogada();
        $arquivo = "fotos_telao_{$turma['Turma']['id']}.zip";
        $path = APP ."webroot/upload/turmas/$arquivo";
        if(file_exists($path)) {
            $data = $this->GerenciadorDeDownload->iniciarDownload($path,$arquivo,"application/zip, application/octet-stream");
            if($data) {
                $json = array(
                    'error' => false,
                    'file' => $data
                );
            } else {
                $json['message'] = "Erro ao iniciar o download";
            }
        } else {
            $json['message'] = "Erro ao compactar os arquivos";
        }
        echo json_encode($json);
    }
    
    function video_baixar_fotos_telao(){
        $this->_baixar_fotos_telao();
    }
    
    function foto_baixar_fotos_telao(){
        $this->_baixar_fotos_telao();
    }
    
    function marketing_baixar_fotos_telao(){
        $this->_baixar_fotos_telao();
    }
    
    function atendimento_upload($tipo, $uid){
        $this->formando_upload($tipo, $uid);
    }
    
    function video_excel_fotos_telao(){
        $this->_fotos_telao_listar();
        ini_set('memory_limit', '64M');
    }
    
    function foto_excel_fotos_telao(){
        $this->_fotos_telao_listar();
        ini_set('memory_limit', '64M');
    }
    function marketing_excel_fotos_telao(){
        $this->_fotos_telao_listar();
        ini_set('memory_limit', '64M');
    }

    function formando_upload($tipo, $uid) {
        $this->layout = false;
        $usuario = $this->obterUsuarioLogado(); 
        if (isset($_FILES[0]) && $_FILES[0]['error'] == 0) {
            Configure::write(array('debug' => 0));
            $this->autoRender = false;
            $dir = "upload/foto_perfil/u" . Security::hash(microtime(true) * 1000, 'sha1', true);
            $path = APP . "webroot/$dir";
            if (move_uploaded_file($_FILES[0]['tmp_name'], $path)) {
                $this->loadModel('FormandoFotoTelao');
                $data['usuario_id'] = $uid;
                $data['arquivo'] = $dir;
                $data['tipo'] = $tipo;
                $data['data_cadastro'] = DboSource::expression('NOW()');
                $foto = $this->FormandoFotoTelao->find('first', array(
                    'conditions' => array(
                        'FormandoFotoTelao.usuario_id' => $uid,
                        'FormandoFotoTelao.tipo' => $data['tipo']
                    )
                ));
                if ($foto) {
                    unlink(APP . "webroot/{$foto['FormandoFotoTelao']['arquivo']}");
                    $data['id'] = $foto['FormandoFotoTelao']['id'];
                } else {
                    $this->FormandoFotoTelao->create();
                }
                if ($this->FormandoFotoTelao->save($data)){
                    echo json_encode(array('status' => 'success','src' => $dir));
                }else{
                    echo json_encode(array('status' => 'error2'));
                }
            }
        }
    }
    
    function atendimento_fotos_telao($uid){
        $this->layout = false;
        if($uid){
            $formando = $this->FormandoProfile->find('first', array(
                'conditions' => array(
                    'FormandoProfile.usuario_id' => $uid
                )
            ));
            $this->set('formando', $formando);
            $this->loadModel('FormandoFotoTelao');
            $fotoCrianca = $this->FormandoFotoTelao->find('first', array(
                'conditions' => array(
                    'FormandoFotoTelao.usuario_id' => $uid,
                    'FormandoFotoTelao.tipo' => 'crianca'
                )
            ));
            $this->set('fotoCrianca', $fotoCrianca);
            $fotoAdulto = $this->FormandoFotoTelao->find('first', array(
                'conditions' => array(
                    'FormandoFotoTelao.usuario_id' => $uid,
                    'FormandoFotoTelao.tipo' => 'adulto'
                )
            ));
            $this->set('fotoAdulto', $fotoAdulto);
            $evento = $this->Evento->find('first', array(
                'conditions' => array(
                    'Evento.turma_id' => $formando['Usuario']['ViewFormandos']['turma_id'],
                    'Evento.tipos_evento_id' => 1
                ) 
            ));
            if($evento){
                if(date('Y-m-d', strtotime('-15 days',strtotime(substr($evento['Evento']['data'], 0, 10)))) <= date('Y-m-d')){
                    $esconder = 'esconder';
                    $mostrar  = 'mostrar';
                    if($formando['Usuario']['ViewFormandos']['turma_id'] == 5528 && date('Y-m-d') <= '2015-01-16'){
                        $esconder = '';
                        $mostrar = 'esconder';
                    }
                }else{
                    $esconder = '';
                    $mostrar = 'esconder';
                } 
            }else{
                $esconder = '';
                $mostrar = 'esconder';
            }
            $this->set('esconder', $esconder);
            $this->set('mostrar', $mostrar);
            $this->set('fotoAdulto', $fotoAdulto);
            if(!empty($formando['FormandoProfile']['nome_telao']))
                $this->render('fotos_telao');
            else
                $this->render('nome_telao');
        }
    }

    function formando_fotos_telao() {
        $this->layout = false;
        $turma = $this->obterTurmaLogada();
        $usuario = $this->obterUsuarioLogado();
        $formando = $this->FormandoProfile->find('first', array(
            'conditions' => array(
                'FormandoProfile.usuario_id' => $usuario['Usuario']['id']
            )
        ));
        $this->set('formando', $formando);
        $this->loadModel('FormandoFotoTelao');
        $fotoCrianca = $this->FormandoFotoTelao->find('first', array(
            'conditions' => array(
                'FormandoFotoTelao.usuario_id' => $usuario['Usuario']['id'],
                'FormandoFotoTelao.tipo' => 'crianca'
            )
        ));
        $this->set('fotoCrianca', $fotoCrianca);
        $fotoAdulto = $this->FormandoFotoTelao->find('first', array(
            'conditions' => array(
                'FormandoFotoTelao.usuario_id' => $usuario['Usuario']['id'],
                'FormandoFotoTelao.tipo' => 'adulto'
            )
        ));
        $evento = $this->Evento->find('first', array(
            'conditions' => array(
                'Evento.turma_id' => $turma['Turma']['id'],
                'Evento.tipos_evento_id' => 1
            ) 
        ));
        if($evento){
            if(date('Y-m-d', strtotime('-15 days',strtotime(substr($evento['Evento']['data'], 0, 10)))) <= date('Y-m-d')){
                $esconder = 'esconder';
                $mostrar  = 'mostrar';
                if($turma['Turma']['id'] == 5528 && date('Y-m-d') <= '2015-01-16'){
                    $esconder = '';
                    $mostrar = 'esconder';
                }
            }else{
                $esconder = '';
                $mostrar = 'esconder';
            } 
        }else{
            $esconder = '';
            $mostrar = 'esconder';
        }
        $this->set('esconder', $esconder);
        $this->set('mostrar', $mostrar);
        $this->set('fotoAdulto', $fotoAdulto);
        if(!empty($formando['FormandoProfile']['nome_telao']))
            $this->render('fotos_telao');
        else
            $this->render('nome_telao');
    }
    
    function excluir_foto_telao($id = false){
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        if($id){
            $this->loadModel('FormandoFotoTelao');
            $this->FormandoFotoTelao->delete($id);
            $this->Session->setFlash('Foto excluída com sucesso.', 'metro/flash/success');
        }else{
            $this->Session->setFlash('Ocorreu um erro ao excluir a foto.', 'metro/flash/error');
        }
    }
    
    function alterar_nome_telao($uid, $nome){
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        if($uid && $nome){
            $this->FormandoProfile->id = $uid;
            $this->FormandoProfile->saveField('nome_telao', $nome);
            $this->Session->setFlash('Nome alterado com sucesso.', 'metro/flash/success');
        }else{
            $this->Session->setFlash('Ocorreu um erro ao alterar o nome do telão.', 'metro/flash/error');
        }
    }
    
    function atendimento_devolucao(){
        $this->layout = false;
        if (!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->Session->write("filtros.{$this->params['prefix']}.formandos", $this->data['ViewFormandos']);
        }else{
            $usuario = $this->Session->read('Usuario');
            $options['joins'] = array(
                array(
                    'table' => 'protocolos',
                    'alias' => 'Protocolo',
                    'type' => 'right',
                    'foreignKey' => false,
                    'conditions' => array(
                        'ViewFormandos.id = Protocolo.usuario_id'
                    )
                ),
                array(
                    'table' => 'protocolos_cancelamentos',
                    'alias' => 'ProtocolosCancelamento',
                    'type' => 'left',
                    'foreignKey' => false,
                    'conditions' => array(
                        'Protocolo.id = ProtocolosCancelamento.protocolo_id'
                    )
                )
            );
            $options['conditions'] = array(
                'Protocolo.tipo' => 'cancelamento',
                'ProtocolosCancelamento.banco not' => '',
                'ProtocolosCancelamento.valor_cancelamento >' => 0,
                'ProtocolosCancelamento.ordem_pagamento' => 1
            );
            $options['fields'] = array(
                'ViewFormandos.*',
                'Protocolo.*',
                'ProtocolosCancelamento.*'
            );
            $filtro = $this->Session->read("filtros.{$this->params['prefix']}.formandos");
            if ($filtro) {
                $this->data['ViewFormandos'] = $filtro;
                foreach ($filtro as $chave => $valor)
                    if(!empty($valor)) {
                        if($chave == 'turma_id')
                            $options['conditions']['ViewFormandos.codigo_formando'] = $valor;
                        else
                            $options['conditions']["$chave LIKE "] = "%{$valor}%";
                    }
            }
            $options['limit'] = $this->paginate['ViewFormandos']['limit'];
            $this->paginate['ViewFormandos'] = $options;
            $formandos = $this->paginate('ViewFormandos');
            $this->set('formandos', $formandos);
        }
    }
    
    function atendimento_finalizar_protocolo($protocoloId){
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        if($protocoloId){
            $this->loadModel('Protocolo');
            $this->loadModel('ProtocoloCancelamento');
            $protocolo = $this->ProtocoloCancelamento->find('first', array(
                'conditions' => array(
                    'ProtocoloCancelamento.protocolo_id' => $protocoloId
                )
            ));
            $this->Protocolo->id = $protocoloId;
            $this->ProtocoloCancelamento->id = $protocolo['ProtocoloCancelamento']['id'];
            $this->Protocolo->saveField('status', 'finalizado');
            $this->ProtocoloCancelamento->saveField('ordem_pagamento', 0);
            $this->Session->setFlash('Status atualizado com sucesso.', 'metro/flash/success');
        }else{
            $this->Session->setFlash('Erro ao finalizar status.', 'metro/flash/error');
        }
    }
    
    function atendimento_alterar_nome_telao($uid) {
        $this->layout = false;
        $this->autoRender = false;
         Configure::write(array('debug' => 0));
        $data = $this->data;
        if (!empty($this->data)) {
            //$redirecionar = "/formando/formandos/fotos_telao";
            //$this->Session->write('redirecionar',$redirecionar);
            if ($this->FormandoProfile->updateAll(
                            array('FormandoProfile.nome_telao' => "'{$data['nome']}'"), array('FormandoProfile.usuario_id' => $uid)
                    )) {
                $this->Session->setFlash('Nome alterado sucesso', 'metro/flash/success');
            } else
                $this->Session->setFlash('Erro ao alterar o nome', 'metro/flash/error');
            echo json_encode(array());
        }
    }
    
    function app_fotos_telao() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('fotoTelao' => array());
        if(isset($this->data['usuario_id'])) {
            $usuario = $this->ViewFormandos->read(null,$this->data['usuario_id']);
            if($usuario) {
                $this->loadModel('FormandoFotoTelao');
                $json['fotoTelao']['nome'] = $usuario['ViewFormandos']['nome_telao'];
                $crianca = $this->FormandoFotoTelao->find('first',array(
                    'conditions' => array(
                        'FormandoFotoTelao.usuario_id' => $this->data['usuario_id'],
                        'tipo' => 'crianca'
                    )
                ));
                if($crianca) $json['fotoTelao']['crianca'] = $crianca['FormandoFotoTelao']['arquivo'];
                $adulto = $this->FormandoFotoTelao->find('first',array(
                    'conditions' => array(
                        'FormandoFotoTelao.usuario_id' => $this->data['usuario_id'],
                        'tipo' => 'adulto'
                    )
                ));
                if($adulto) $json['fotoTelao']['adulto'] = $adulto['FormandoFotoTelao']['arquivo'];
            }
        }
        echo json_encode($json);
    }
    
    private function _inserir_foto_telao($usuarioId,$tipo,$base64,$extensao = "jpg") {
        $return = array('erro' => true,'mensagem' => array());
        $imagem = base64_decode($base64);
        $nome = "u" . Security::hash(microtime(true) * 1000, 'sha1', true) . "." . $extensao;
        $dir = "upload/foto_perfil/$nome";
        $path = APP . "webroot/$dir";
        if(file_put_contents($path, $imagem)) {
            $data = array(
                'FormandoFotoTelao' => array(
                    'arquivo' => $dir,
                    'usuario_id' => $usuarioId,
                    'tipo' => $tipo
                )
            );
            if($this->FormandoFotoTelao->save($data))
                $return['erro'] = false;
            else
                $return['mensagem'][] = "Erro ao inserir dados";
        } else {
            $return['mensagem'][] = "Erro ao criar arquivo";
        }
        return $return;
    }
    
    function app_inserir_fotos_telao() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('erro' => true,'mensagem' => array());
        if(isset($this->data['usuario_id'])) {
            $this->loadModel('FormandoFotoTelao');
            if($this->data['crianca']) {
                $result = $this->_inserir_foto_telao($this->data['usuario_id'],
                        'crianca',$this->data['crianca']['data'],$this->data['crianca']['extensao']);
                $json['erro'] = $result['erro'];
                $json['mensagem'] = array_merge($json['mensagem'],$result['mensagem']);
                if(!$result['erro']) {
                    $crianca = $this->FormandoFotoTelao->find('first',array(
                        'conditions' => array(
                            'FormandoFotoTelao.usuario_id' => $this->data['usuario_id'],
                            'tipo' => 'crianca'
                        )
                    ));
                    if($crianca)
                        $json['fotoTelao']['crianca'] = $crianca['FormandoFotoTelao']['arquivo'];
                }
            }
            if($this->data['adulto']) {
                $result = $this->_inserir_foto_telao($this->data['usuario_id'],
                        'adulto',$this->data['adulto']['data'],$this->data['adulto']['extensao']);
                $json['erro'] = $result['erro'] || $json['erro'];
                $json['mensagem'] = array_merge($json['mensagem'],$result['mensagem']);
                if(!$result['erro']) {
                    $adulto = $this->FormandoFotoTelao->find('first',array(
                        'conditions' => array(
                            'FormandoFotoTelao.usuario_id' => $this->data['usuario_id'],
                            'tipo' => 'adulto'
                        )
                    ));
                    if($adulto)
                        $json['fotoTelao']['adulto'] = $adulto['FormandoFotoTelao']['arquivo'];
                }
            }
        }
        echo json_encode($json);
    }
    
    private function _alterar_nome_telao($usuarioId,$nome) {
        return $this->FormandoProfile->updateAll(
            array('FormandoProfile.nome_telao' => "'{$nome}'"),
            array('FormandoProfile.usuario_id' => $usuarioId)
        );
    }
    
    function app_inserir_nome_telao() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('erro' => true);
        if(isset($this->data['nome']) && isset($this->data['usuario_id'])) {
            if($this->_alterar_nome_telao($this->data['usuario_id'], $this->data['nome'])) {
                $json = array(
                    'erro' => false,
                    'fotoTelao' => array(
                        'nome' => $this->data['nome']
                    )
                );
            }
        }
        echo json_encode($json);
    }

    function formando_alterar_nome_telao() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $data = $this->data;
        if(!empty($this->data)) {
            if ($this->_alterar_nome_telao($this->Session->read('Usuario.Usuario.id'), $data['nome']))
                $this->Session->setFlash('Nome alterado sucesso', 'metro/flash/success');
            else
                $this->Session->setFlash('Erro ao alterar o nome', 'metro/flash/error');
        }
        echo json_encode(array());
    }

    function formando_alterar_foto_telao() {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        if (!empty($this->data)) {
            $this->loadModel('FormandoFotoTelao');
            $imagem = base64_decode($this->data['foto']);
            $dir = "upload/foto_perfil/u" . Security::hash(microtime(true) * 1000, 'sha1', true);
            $path = APP . "webroot/$dir";
            if (file_put_contents($path, $imagem)) {
                $data = $this->data;
                $data['usuario_id'] = $this->Session->read('Usuario.Usuario.id');
                $data['arquivo'] = $dir;
                $foto = $this->FormandoFotoTelao->find('first', array(
                    'conditions' => array(
                        'FormandoFotoTelao.usuario_id' => $this->Session->read('Usuario.Usuario.id'),
                        'FormandoFotoTelao.tipo' => $data['tipo']
                    )
                ));
                if ($foto) {
                    unlink(APP . "webroot/{$foto['FormandoFotoTelao']['arquivo']}");
                    $data['id'] = $foto['FormandoFotoTelao']['id'];
                } else {
                    $this->FormandoFotoTelao->create();
                }
                if ($this->FormandoFotoTelao->save($data))
                    $this->Session->setFlash('Foto enviada com sucesso', 'metro/flash/success');
                else
                    $this->Session->setFlash('Erro ao enviar foto', 'metro/flash/error');
            } else {
                $this->Session->setFlash('Erro ao salvar foto', 'metro/flash/error');
            }
        }
        echo json_encode(array());
    }
    
    private function _fotos_telao_listar(){
        $turma = $this->obterTurmaLogada();
        $this->loadModel('FormandoFotoTelao');
        $this->layout = false;
        if (!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->Session->write("filtros.{$this->params['prefix']}.formandos", $this->data['ViewFormandos']);
        } else {
            $options['order'] = array('ViewFormandos.codigo_formando' => 'asc');
            $options['fields'] = array(
                'ViewFormandos.*',
                "(select arquivo from formandos_fotos_telao where usuario_id = `ViewFormandos`.`id` and tipo = 'crianca' LIMIT 1) as foto_crianca",
                "(select arquivo from formandos_fotos_telao where usuario_id = `ViewFormandos`.`id` and tipo = 'adulto' LIMIT 1) as foto_adulto",
            );
            $options['conditions'] = array(
                'turma_id' => $turma['Turma']['id'],
                'situacao' => 'ativo',
            );
            $filtro = $this->Session->read("filtros.{$this->params['prefix']}.formandos");
            if ($filtro) {
                $this->data['ViewFormandos'] = $filtro;
                foreach ($filtro as $chave => $valor)
                    $options['conditions']['codigo_formando LIKE '] = "%{$valor}%";
            }
            $options['limit'] = 200;
            $this->paginate['ViewFormandos'] = $options;
            $formandos = $this->paginate('ViewFormandos');
            $this->set('formandos', $formandos);
            $this->set('turma', $turma);
        }
  
    }
    
    private function _rifas_listar(){
        $turma = $this->obterTurmaLogada();
        
        $options['joins'] = array(
            array(
                'table' => 'cupons',
                'alias' => 'Cupom',
                'foreignKey' => false,
                'type' => 'inner',
                'conditions' => array(
                    'Cupom.usuario_id = ViewFormandos.id'
                )
            ),
            array(
                'table' => 'rifas_turmas',
                'alias' => 'RifaTurma',
                'foreignKey' => false,
                'type' => 'inner',
                'conditions' => array(
                    'RifaTurma.turma_id = ViewFormandos.turma_id'
                )
            ),
        );
        
        $options['order'] = array('ViewFormandos.codigo_formando' => 'asc');
        $options['group'] = array('ViewFormandos.codigo_formando');
        $options['fields'] = array(
            'Cupom.*',
            'ViewFormandos.*',
            'RifaTurma.*',
            'count(0) as cupons',
            "sum(Cupom.status = 'vendido') as status");
        $options['conditions'] = array(
            'ViewFormandos.turma_id' => $turma['Turma']['id'],
        );
        $options['limit'] = 50;
        $this->paginate['ViewFormandos'] = $options;
        $formandos = $this->paginate('ViewFormandos');
        // debug($formandos);
        $this->set('formandos', $formandos);
    }
    
    function planejamento_rifas_listar(){
        $this->_rifas_listar();
    }
    
    function planejamento_fotos_telao_listar(){
        $this->_fotos_telao_listar();
    }
    
    function atendimento_fotos_telao_listar(){
        $this->_fotos_telao_listar();
    }
    
    function video_fotos_telao_listar(){
        $this->_fotos_telao_listar();
    }
    
    function foto_fotos_telao_listar(){
        $this->_fotos_telao_listar();
    }
    
    function marketing_fotos_telao_listar(){
        $this->_fotos_telao_listar();
    }
    
    function comissao_fotos_telao_listar(){
        $this->_fotos_telao_listar();
    }

    function atendimento_visualizar($id) {
        $formando = $this->FormandoProfile->find('first', array('conditions' => array('FormandoProfile.usuario_id' => $id)));
        $this->planejamento_visualizar($formando['FormandoProfile']['id']);
    }

    function financeiro_gerar_excel() {
        set_time_limit(10000);
        ini_set("memory_limit", "1024M");
        $formandos = $this->ViewFormandos->query('Select * from vw_formandos');
        $this->set('formandos', $formandos);
    }

    function formando_index() {
        $this->layout = 'metro/new';
        $this->render('formando_novo');
    }

    function formando_minha_formatura() {
        if (!$this->RequestHandler->isAjax())
            $this->redirect("/{$this->params['prefix']}/");
        $this->layout = false;
        $usuario = $this->Session->read('Usuario');
        if ($usuario) {
            App::import('Helper', 'Cores');
            $cores = new CoresHelper();
            $this->set('cores', $cores);
            $formando = $this->ViewFormandos->find('first', array('conditions' => array(
                    'ViewFormandos.id' => $usuario['Usuario']['id'])));
            if ($formando) {
                $this->set('formandoLogado', $formando);
                $this->Turma->recursive = 0;
                $turma = $this->Turma->read(null, $formando['ViewFormandos']['turma_id']);
                $this->set('turma', $turma);
                $formandos = $this->ViewFormandos->find('all', array('conditions' => array(
                        'turma_id' => $formando['ViewFormandos']['turma_id'],
                        'situacao' => 'ativo',
                        'ViewFormandos.id != ' => $usuario['Usuario']['id'])));
                $listaFormandos = array();
                foreach ($formandos as $i => $f) {
                    if (!empty($f['ViewFormandos']['diretorio_foto_perfil']) &&
                            file_exists(WWW_ROOT . $f['ViewFormandos']['diretorio_foto_perfil']))
                        $formandos[$i]['ViewFormandos']['foto_perfil'] =
                                FULL_BASE_URL . '/' . $f['ViewFormandos']['diretorio_foto_perfil'];
                    else
                        $formandos[$i]['ViewFormandos']['foto_perfil'] =
                                FULL_BASE_URL . '/img/uknown_user.gif';
                    if (empty($f['ViewFormandos']['data_adesao']))
                        $listaFormandos['nao_aderidos'][] = $formandos[$i];
                    else
                        $listaFormandos['aderidos'][] = $formandos[$i];
                }
                $this->set('formandos', $listaFormandos);
            }
        }
    }

    function planejamento_visualizar($id = null) {

        //$this->Usuario->bindModel(array('hasOne' => array('TurmasUsuario','FormandoProfile')), false);
        $this->Usuario->bindModel(array('hasOne' => array('TurmasUsuario')), false);
        //$this->Usuario->bindModel(array('hasMany' => array('Turma' => array('fields' => array('Turma.nome', 'Turma.usuario_id')))), false);
        debug($id);
        //$this->Turma->bindModel(array('hasMany' => array('CursoTurma')), false);
        $this->Usuario->unBindModel(array('hasMany' => array('Despesa'), 'hasOne' => array('FormandoProfile')), false);

        $this->Usuario->recursive = 0;
        $this->FormandoProfile->recursive = 2;
        $this->CursoTurma->recursive = 2;
        $this->Usuario->Turma->recursive = -1;


        $this->CursoTurma->bindModel(array('belongsTo' => array('Curso')), false);
        $this->Curso->bindModel(array('hasMany' => array('CursoTurma')), false);
        $this->CursoTurma->bindModel(array('hasMany' => array('FormandoProfile' => array('foreignKey' => 'curso_turma_id', 'recursive' => 3))), false);
        $this->FormandoProfile->bindModel(array('belongsTo' => array('Usuario', 'CursoTurma' => array('foreignKey' => 'curso_turma_id'))), false);

        $this->Usuario->unBindModel(array('hasAndBelongsToMany' => array('Turma')), false);
        $this->Usuario->unBindmodel(array('hasMany' => array('TurmasUsuario')), false);
        $this->TurmasUsuario->unBindModel(array('belongsTo' => array('Usuario', 'Turma')));
        //$this->CursoTurma->unBindModel(array('belongsTo' => array('Turma')));
        $this->CursoTurma->unBindModel(array('hasMany' => array('FormandoProfile')));

        $formando = $this->FormandoProfile->find(array('FormandoProfile.id' => $id));

        $dateTime = $this->create_date_time_from_format('Y-m-d', $formando['FormandoProfile']['data_nascimento']);

        if ($dateTime != null)
            $formando['FormandoProfile']['data_nascimento'] = date_format($dateTime, 'd/m/Y');


        //$formandoProfile = $this->FormandoProfile->find('first', array('cond' => array('FormandoProfile.usuario_id' => $id), 'contain' => array('Usuario','CursoTurma')));

        if ($formando) {

            $this->loadModel('Protocolo');
            $protocolo = $this->Protocolo->find('all', array('conditions' => array('Protocolo.usuario_id' => $formando['Usuario']['id'])));
            debug($protocolo);
            $this->set('protocolo', $protocolo);

            // Verificar despesas e pagamentos
            $this->LoadModel('Pagamento');

            // Listar Despesas de Adesão
            $adesoes = $this->Usuario->Despesa->find('all', array('conditions' => array('Despesa.usuario_id' => $formando['Usuario']['id'], 'Despesa.tipo' => 'adesao', 'Despesa.status not' => array('cancelada','renegociada'))));
            $this->set('despesas_adesao', $adesoes);

            $extras = $this->Usuario->Despesa->find('all', array('conditions' => array('Despesa.usuario_id' => $formando['Usuario']['id'], 'Despesa.tipo' => 'extra')));
            $this->set('despesas_extras', $extras);

            $pagamentos = $this->Pagamento->find('all', array('conditions' => array('Pagamento.usuario_id' => $formando['Usuario']['id'])));

            $totalPago = 0;
            $totalEmAberto = 0;
            $totalDespesas = 0;
            $totalDespesasExtras = 0;
            $totalDespesasAdesao = 0;
            foreach ($pagamentos as $pagamento) {
                if ($pagamento['Pagamento']['status'] == 'pago') {
                    $totalPago += $pagamento['Pagamento']['valor_pago'];
                }
            }


            foreach ($adesoes as $adesao) {
                $totalDespesasAdesao += $adesao['Despesa']['valor'] + $adesao['Despesa']['multa'] + $adesao['Despesa']['correcao_igpm'];
            }

            foreach ($extras as $extra) {
                $totalDespesasExtras += $extra['Despesa']['valor'] + $extra['Despesa']['multa'] + $extra['Despesa']['correcao_igpm'];
            }

            $totalDespesas = $totalDespesasExtras + $totalDespesasAdesao;

            $saldoAtual = $totalPago - $totalDespesas;

            $this->set('total_pago', $totalPago);
            $this->set('saldo_atual', $saldoAtual);
            $this->set('total_despesas', $totalDespesas);

            $this->set('formando', $formando);


            // Buscar informações sobre extras comprados
            $this->LoadModel('CampanhasUsuarioCampanhasExtra');
            // Buscar os ids das adesões do usuário à campanhas
            $campanhasUsuarioPertencentesAoFormando = $this->Usuario->CampanhasUsuario->find('all', array('conditions' => array('CampanhasUsuario.usuario_id' => $formando['Usuario']['id'])));
            $idsCampanhasUsuarioPertencentesAoFormando = array();
            foreach ($campanhasUsuarioPertencentesAoFormando as $campanhasUsuario) {
                $idsCampanhasUsuarioPertencentesAoFormando[] = $campanhasUsuario['CampanhasUsuario']['id'];
            }

            // Exemplo de saida: Baile de Gala - Convite Extra : 4 pagos / 2 em aberto
            // Extra -> array( 
            // -> array ( 
            //    quantidade -> campanha_id
            // 	  pago -> sim/nao
            //  )
            //)
            /*
              $extras = array(
              0 =>
              array(
              'Extra' =>
              array(
              'id' => 1,
              'evento_id' => 1,
              'nome' => 'Convite Extra',
              'descricao' => 'Convite para o baile'
              ) ,
              'Compras' =>
              array(
              'campanhas_usuario_id' => 1,
              //buscar na tabela despesas
              'pago' => true ,
              'quantidade' => 3
              )

              ),
              1 =>
              array(
              )
              );
             */

            //$this->CampanhasExtra->unBindModel(array('CampanhasUsuarioCampanhasExtra'));
            //$this->Usuario->unBindModel(array('hasMany' => array('Despesa') , 'hasOne' => array('FormandoProfile')) , false);
            // Buscar os extras comprados nessas adesões

            $lista_extras = $this->CampanhasUsuarioCampanhasExtra->find('all', array(
                'conditions' => array('campanhas_usuario_id' => $idsCampanhasUsuarioPertencentesAoFormando),
                'recursive' => 2
                    )
            );

            //debug($lista_extras);
        } else {
            $this->Session->setFlash('Formando inválido ou inexistente.', 'flash_erro');
            //$this->redirect("/{$this->params['prefix']}/formandos");
        }
    }

    function comissao_index() {
        $this->planejamento_index();
    }

    function comissao_visualizar($id) {
        $this->planejamento_visualizar($id);
    }

    function comissao_adesao() {
        $turma = $this->obterTurmaLogada();

        $mostrar_formulario = true;
        $mensagem = "";


        // Ler informações do Usuário
        $usuario = $this->Session->read('Usuario');
        if (!$usuario) {
            $this->Session->setFlash('Sessão expirada. Por favor, faça o login novamente.', 'flash_erro');
            $this->redirect("/usuarios/login");
        }

        // Ler informações do Formando Profile
        $registro_formando =
                $this->FormandoProfile->find('first', array('conditions' => array('usuario_id' => $usuario['Usuario']['id']), 'fields' => array('valor_adesao_comissao', 'max_parcelas_comissao', 'curso_turma_id', 'aderido')));


        $registro_formando = $registro_formando['FormandoProfile'];
        $valor_adesao = $registro_formando['valor_adesao_comissao'];
        $max_parcelas_comissao = $registro_formando['max_parcelas_comissao'];

        // Verificar se já está aderido
        if ($registro_formando['aderido'] == 1) {
            $this->Session->setFlash('Você já aderiu à formatura. Para acessar seu espaço de formando, clique em "Entrar como formando."', 'metro/flash/error');
            $this->redirect('/comissao');
        }

        if ($this->data) {
            debug("AQUI!");
            $usuario = $this->Usuario->find('first', array('conditions' => array('Usuario.id' => $usuario['Usuario']['id'])));

            $campos_modificados = array('forma_pagamento', 'codigo_formando', 'aderido', 'data_adesao', 'valor_adesao', 'parcelas_adesao', 'curso_turma_id');

            if ($valor_adesao == 0) {
                debug("Valor 0.. setando parametros");
                $this->data['FormandoProfile']['parcelas'] = "0";
                $this->data['FormandoProfile']['forma_pagamento'] = 'boleto_online';
                $this->data['FormandoProfile']['dia_vencimento_parcelas'] = '10';
            }

            foreach (array_keys($this->Usuario->FormandoProfile->validate) as $key) {
                if (!in_array($key, $campos_modificados)) {
                    unset($this->Usuario->FormandoProfile->validate[$key]);
                }
            }

            unset($this->Usuario->validate['senha']);
            unset($this->Usuario->validate['confirmar']);

            $numeroParcelas = $this->data['FormandoProfile']['parcelas'];

            if ($numeroParcelas > $max_parcelas_comissao) {
                $numeroParcelas = $max_parcelas_comissao;
            }


            $formaPagamento = $this->data['FormandoProfile']['forma_pagamento'];
            $diaVencimentoParcelas = $this->data['FormandoProfile']['dia_vencimento_parcelas'];

            if ($formaPagamento == 'boleto_impresso') {
                $impresso = true;
            } else {
                $impresso = false;
            }


            // ver quais curso_turma_id são da turma
            $lista_id_cursos_turma = $this->CursoTurma->find('list', array('conditions' => array('turma_id' => $turma['Turma']['id'])));

            // Achar o maior codigo_formando já utilizado na turma
            $buscaCodigoFormando = $this->FormandoProfile->find('first', array('conditions' => array('FormandoProfile.curso_turma_id' => $lista_id_cursos_turma), 'fields' => 'MAX(codigo_formando)', 'recursive' => -1));
            $proximo_codigo_formando = $buscaCodigoFormando[0]['MAX(codigo_formando)'] + 1;


            $dados = array();
            $dados['Usuario'] = $usuario['Usuario'];
            unset($dados['Usuario']['senha']);
            unset($dados['Usuario']['confirmar']);


            $dados['FormandoProfile'] = $usuario['FormandoProfile'];
            $dados['FormandoProfile']['id'] = $usuario['FormandoProfile']['id'];
            $dados['FormandoProfile']['forma_pagamento'] = $formaPagamento;
            $dados['FormandoProfile']['dia_vencimento_parcelas'] = $diaVencimentoParcelas;
            $dados['FormandoProfile']['aderido'] = 1;
            $dados['FormandoProfile']['data_adesao'] = date('Y-m-d h:i:s');
            $dados['FormandoProfile']['valor_adesao'] = $valor_adesao;
            $dados['FormandoProfile']['parcelas_adesao'] = $numeroParcelas;
            $dados['FormandoProfile']['codigo_formando'] = $proximo_codigo_formando;
            if (isset($this->data['FormandoProfile']['curso_turma_id']) && in_array($this->data['FormandoProfile']['curso_turma_id'], $lista_id_cursos_turma)) {
                $dados['FormandoProfile']['curso_turma_id'] = $this->data['FormandoProfile']['curso_turma_id'];
            } else {
                if ($registro_formando['curso_turma_id'] != null) {
                    $dados['FormandoProfile']['curso_turma_id'] = $registro_formando['curso_turma_id'];
                } else {
                    $dados['FormandoProfile']['curso_turma_id'] = null;
                }
            }

            if ($valor_adesao > 0) {
                $despesasArray = $this->geraDespesas(date('Y-m-d'), $diaVencimentoParcelas, $numeroParcelas, $valor_adesao, $impresso);
                $dados['Despesa'] = $despesasArray;
            }


            if ($this->Usuario->saveAll($dados)) {
                $strCodigoFormando = str_pad($turma['Turma']['id'], 5, '0', STR_PAD_LEFT) . str_pad($proximo_codigo_formando, 3, '0', STR_PAD_LEFT);
                $this->Session->setFlash('Adesão realizada com sucesso!', 'metro/flash/success');
                $this->Session->write('codigoFormando', $strCodigoFormando);
                $this->Session->delete('exibirSwitchComissaoFormando');
                $this->redirect('/comissao/formandos/obrigado');
            } else {
                $this->Session->setFlash('Ocorreu um erro ao fazer sua adesão.', 'metro/flash/error');
                debug($this->Usuario->validationErrors);
            }
        }

        if ($turma['Turma']['status'] == 'fechada') {
            $max_parcelas = $registro_formando['max_parcelas_comissao'];

            if ($valor_adesao == null || ( $valor_adesao != 0 && $max_parcelas == null)) {
                $mostrar_formulario = false;
                $mensagem = "Não foram cadastradas informações de valor de adesão da comissão.";
            } else {

                if ($valor_adesao > 0) {
                    for ($i = 1; $i <= $max_parcelas; $i++) {
                        $parcelamento[$i] = $i . ' x R$' .
                                number_format($valor_adesao / $i, 2, ',', '.') .
                                ' (R$' . number_format($valor_adesao, 2, ',', '.') . ')';
                    }

                    $select_forma_pagamento = array('boleto_online' => 'Boleto Online - sem custos adicionais', 'boleto_impresso' => 'Boleto Entregue Em Casa - custo adicional de 20 reais', 'cheque' => 'Cheque');
                    $this->set('select_parcelas', $parcelamento);
                    $this->set('select_forma_pagamento', $select_forma_pagamento);
                    $is_pagante = true;
                } else {
                    $is_pagante = false;
                }

                $mostrarEscolherCurso = false;
                // Se o curso_turma_id for nulo, mostrar o campo para escolher curso
                if ($registro_formando['curso_turma_id'] == null) {
                    $mostrarEscolherCurso = true;

                    // ver quais curso_turma_id são da turma
                    $select_cursos = array();
                    $this->CursoTurma->contain(array('Curso.Faculdade.Universidade'));
                    $cursos = $this->CursoTurma->find('all', array('conditions' => array('turma_id' => $turma['Turma']['id']), 'order' => 'Curso.nome'));

                    foreach ($cursos as $curso) {
                        $select_cursos[$curso['CursoTurma']['id']] = $curso['Curso']['nome'] . ' - ' . $curso['Curso']['Faculdade']['nome'] . ' - ' . $curso['Curso']['Faculdade']['Universidade']['nome'];
                    }

                    $this->set('select_cursos', $select_cursos);
                }


                $this->set('is_pagante', $is_pagante);
                $this->set('mostrar_escolher_curso', $mostrarEscolherCurso);
                $this->set('esconder_data_inicio_pagamento', true);
            }
        } else {
            $mostrar_formulario = false;
            $mensagem = "As adesões ainda não estão abertas para sua turma.";
        }

        $this->set('mostrar_formulario', $mostrar_formulario);
        $this->set('mensagem', $mensagem);
    }

    function comissao_aderir() {

        $turma = $this->obterTurmaLogada();

        $mostrar_formulario = true;
        $mensagem = "";

        // Ler informações do Usuário
        $usuario = $this->Session->read('Usuario');
        if (!$usuario) {
            $this->Session->setFlash('Sessão expirada. Por favor, faça o login novamente.', 'metro/flash/error');
        }

        // Ler informações do Formando Profile
        $registro_formando =
                $this->FormandoProfile->find('first', array('conditions' => array('usuario_id' => $usuario['Usuario']['id']), 'fields' => array('valor_adesao_comissao', 'max_parcelas_comissao', 'curso_turma_id', 'aderido')));


        $registro_formando = $registro_formando['FormandoProfile'];
        $valor_adesao = $registro_formando['valor_adesao_comissao'];
        $max_parcelas_comissao = $registro_formando['max_parcelas_comissao'];


        // Verificar se já está aderido
        if ($this->Session->check('exibir_adesao')) {
            $this->Session->delete('exibir_adesao');
            $mostrar_formulario = false;
        } elseif ($registro_formando['aderido'] == 1) {
            $this->Session->setFlash('Você já aderiu à formatura. Para acessar seu espaço de formando, clique em "Entrar como formando."', 'metro/flash/error');
            $mostrar_formulario = false;
        }

        if ($this->data) {
            $usuario = $this->Usuario->find('first', array(
                'conditions' => array('Usuario.id' => $usuario['Usuario']['id'])));

            $campos_modificados = array('forma_pagamento', 'codigo_formando', 'aderido', 'data_adesao', 'valor_adesao', 'parcelas_adesao', 'curso_turma_id');

            if ($valor_adesao == null || $valor_adesao == 0) {
                debug("Valor 0.. setando parametros");
                $this->data['FormandoProfile']['parcelas_adesao'] = 0;
                $this->data['FormandoProfile']['forma_pagamento'] = 'boleto_online';
                $this->data['FormandoProfile']['dia_vencimento_parcelas'] = 10;
                $this->data['FormandoProfile']['aderido'] = 1;
                $this->data['FormandoProfile']['valor_adesao'] = 0;
            }

            foreach (array_keys($this->Usuario->FormandoProfile->validate) as $key) {
                if (!in_array($key, $campos_modificados)) {
                    unset($this->Usuario->FormandoProfile->validate[$key]);
                }
            }

            unset($this->Usuario->validate['senha']);
            unset($this->Usuario->validate['confirmar']);

            $numeroParcelas = $this->data['FormandoProfile']['parcelas'];

            if ($numeroParcelas > $max_parcelas_comissao)
                $numeroParcelas = $max_parcelas_comissao;

            $formaPagamento = $this->data['FormandoProfile']['forma_pagamento'];
            $diaVencimentoParcelas = $this->data['FormandoProfile']['dia_vencimento_parcelas'];

            $impresso = $formaPagamento == 'boleto_impresso';

            $codigoFormando = $this->ViewFormandos->find('all', array(
                'conditions' => array(
                    'ViewFormandos.turma_id' => $turma['Turma']['id']
                ),
                'order' => array('ViewFormandos.codigo_formando' => 'desc')
            ));
            $proximo_codigo_formando = $codigoFormando[0]['ViewFormandos']['codigo_formando'] + 1;

            $dados = array();
            $dados['Usuario'] = $usuario['Usuario'];
            unset($dados['Usuario']['senha']);
            unset($dados['Usuario']['confirmar']);

            $dados['FormandoProfile'] = $usuario['FormandoProfile'];
            $dados['FormandoProfile']['id'] = $usuario['FormandoProfile']['id'];
            $dados['FormandoProfile']['forma_pagamento'] = $formaPagamento;
            $dados['FormandoProfile']['dia_vencimento_parcelas'] = $diaVencimentoParcelas;
            $dados['FormandoProfile']['aderido'] = 1;
            $dados['FormandoProfile']['data_adesao'] = date('Y-m-d h:i:s');
            $dados['FormandoProfile']['valor_adesao'] = $valor_adesao;
            $dados['FormandoProfile']['parcelas_adesao'] = $numeroParcelas;
            $dados['FormandoProfile']['codigo_formando'] = substr($proximo_codigo_formando, 4);

            if (isset($this->data['FormandoProfile']['curso_turma_id'])){
                $dados['FormandoProfile']['curso_turma_id'] = $this->data['FormandoProfile']['curso_turma_id'];
            } else {
                    $dados['FormandoProfile']['curso_turma_id'] = NULL;
            }
            
            if ($valor_adesao > 0) {
                $dataInicioPagamento = $this->data['FormandoProfile']['data_inicio_pagamento'];
                $despesasArray = $this->geraDespesas($dataInicioPagamento, $diaVencimentoParcelas, $numeroParcelas, $valor_adesao, $impresso);
                $dados['Despesa'] = $despesasArray;
            }
            if ($this->Usuario->saveAll($dados)) {
                $this->Session->setFlash('Adesão realizada com sucesso!', 'metro/flash/success');
                $this->Session->write('codigoFormando', $proximo_codigo_formando);
                $this->Session->write('exibir_adesao', true);
                $this->Session->delete('exibirSwitchComissaoFormando');
            } else {
                $this->Session->setFlash('Ocorreu um erro ao fazer sua adesão.', 'metro/flash/error');
            }
            echo json_encode(array());
            exit();
        }

        if ($turma['Turma']['status'] == 'fechada') {

            $max_parcelas = $registro_formando['max_parcelas_comissao'];

            if ($valor_adesao == null || ( $valor_adesao != 0 && $max_parcelas == null)) {
                $mostrar_formulario = false;
                $mensagem = "Não foram cadastradas informações de valor de adesão da comissão.";
            } else {

                if ($valor_adesao > 0) {
                    for ($i = 1; $i <= $max_parcelas; $i++) {
                        $parcelamento[$i] = $i . ' x R$' .
                                number_format($valor_adesao / $i, 2, ',', '.') .
                                ' (R$' . number_format($valor_adesao, 2, ',', '.') . ')';
                    }

                    $select_forma_pagamento = array('boleto_online' => 'Boleto Online - sem custos adicionais', 'boleto_impresso' => 'Boleto Entregue Em Casa - custo adicional de 20 reais', 'cheque' => 'Cheque');
                    $this->set('select_parcelas', $parcelamento);
                    $this->set('select_forma_pagamento', $select_forma_pagamento);
                    $is_pagante = true;
                } else {
                    $is_pagante = true;
                    $select_forma_pagamento = array('boleto_online' => 'Boleto Online - sem custos adicionais', 'boleto_impresso' => 'Boleto Entregue Em Casa - custo adicional de 20 reais', 'cheque' => 'Cheque');
                    $this->set('select_parcelas', null);
                    $this->set('select_forma_pagamento', null);
                }

                $mostrarEscolherCurso = false;
                // Se o curso_turma_id for nulo, mostrar o campo para escolher curso
                if ($registro_formando['curso_turma_id'] == null) {
                    $mostrarEscolherCurso = true;

                    // ver quais curso_turma_id são da turma
                    $select_cursos = array();
                    $this->CursoTurma->contain(array('Curso.Faculdade.Universidade'));
                    $cursos = $this->CursoTurma->find('all', array('conditions' => array('turma_id' => $turma['Turma']['id']), 'order' => 'Curso.nome'));

                    foreach ($cursos as $curso) {
                        $select_cursos[$curso['CursoTurma']['id']] = $curso['Curso']['nome'] . ' - ' . $curso['Curso']['Faculdade']['nome'] . ' - ' . $curso['Curso']['Faculdade']['Universidade']['nome'];
                    }

                    $this->set('select_cursos', $select_cursos);
                }



                $this->set('is_pagante', $is_pagante);
                $this->set('mostrar_escolher_curso', $mostrarEscolherCurso);



                $parcelamentos = $this->Parcelamento->find('all', array(
                    'conditions' => array(
                        'turma_id' => $turma['Turma']['id'],
                        'data >=' => date('Y-m-\0\1', strtotime('now')))
                        )
                );


                foreach ($parcelamentos as $parcelamento) {
                    setlocale(LC_TIME, 'ptb', 'pt_BR', 'portuguese-brazil', 'bra', 'brazil', 'pt_BR.utf-8', 'pt_BR.iso-8859-1', 'br');
                    $data = $parcelamento['Parcelamento']['data'];
                    $aux_data_parcela = strftime("%Y%m", strtotime($data));
                    $aux_data_atual = strftime("%Y%m", strtotime('now'));
                    if ($aux_data_parcela >= $aux_data_atual) {
                        if (!isset($datas[$data]))
                            $datas[$data] = ucfirst(strftime('%b / %Y', strtotime($data)));
                    }
                }
                $select_forma_pagamento = array(
                    'boleto_online' => 'Boleto Online - sem custos adicionais',
                    'boleto_impresso' => 'Boleto Entregue Em Casa - custo adicional de 20 reais',
                    'cheque' => 'Cheque'
                );
                $this->set('select_forma_pagamento', $select_forma_pagamento);
                $this->set('select_data_inicio_pagamento', $datas);
            }
        } else {
            $mostrar_formulario = false;
            $mensagem = "As adesões ainda não estão abertas para sua turma.";
        }
        $this->set('turma', $turma);
        $this->set('mostrar_formulario', $mostrar_formulario);
        $this->set('mensagem', $mensagem);
    }

    function comissao_obrigado() {
        $codigoFormando = $this->Session->read('codigoFormando');
        $this->set('codigo_formando_cadastro', $codigoFormando);
    }

    private function _info($usuarioId) {
        $this->layout = false;
        $formando = $this->ViewFormandos->read(null, $usuarioId);
        if ($formando) {
            $this->Turma->recursive = 0;
            $turma = $this->Turma->read(null, $formando['ViewFormandos']['turma_id']);
            $this->set('turma', $turma);
            $this->Curso->recursive = 0;
            $curso = $this->Curso->read(null, $formando['ViewFormandos']['curso_id']);
            $this->set('curso', $curso);
        }
        $this->set('formando', $formando);
        $this->render('_info');
    }

    function planejamento_info($usuarioId) {
        $this->_info($usuarioId);
    }

    function comercial_info($usuarioId) {
        $this->_info($usuarioId);
    }
    
    function foto_info($usuarioId) {
        $this->_info($usuarioId);
    }
    
    function comissao_info($usuarioId) {
        $this->_info($usuarioId);
    }
    
    function atendimento_info($usuarioId) {
        $this->_info($usuarioId);
    }
    
    function planejamento_excel_fotos_telao(){
        ini_set('memory_limit', '1024M');
        $this->_fotos_telao_listar();
    }

    
    function atendimento_excel_fotos_telao(){
        ini_set('memory_limit', '1024M');
        $this->_fotos_telao_listar();
    }
    
    function atendimento_listar() {
        $this->layout = false;
        if (!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->Session->write("filtros.{$this->params['prefix']}.formandos", $this->data['ViewFormandos']);
        } else {
            ini_set('memory_limit','60M');
            set_time_limit(0);
            $usuario = $this->Session->read('Usuario');
            $options = array(
                'group' => array('ViewFormandos.id')
            );
            $options['joins'] = array(
                array(
                    'table' => 'turmas_usuarios',
                    'alias' => 'TurmaUsuario',
                    'foreignKey' => false,
                    'conditions' => array(
                        'ViewFormandos.turma_id = TurmaUsuario.turma_id'
                    )
                )
            );
            $options['fields'] = array(
                'ViewFormandos.*',
                "(select if(checkout = 'aberto',1,0) from turmas t where ViewFormandos.turma_id = t.id) as checkout",
                "(select if((select count(0) from protocolos p where ViewFormandos.id = " .
                "p.usuario_id and p.tipo = 'checkout' limit 1) > 0,1,0)) as fez_checkout"
            );
            $options['conditions'] = array(
                'TurmaUsuario.usuario_id' => $usuario['Usuario']['id']
            );
            $filtro = $this->Session->read("filtros.{$this->params['prefix']}.formandos");
            if ($filtro) {
                $this->data['ViewFormandos'] = $filtro;
                foreach ($filtro as $chave => $valor)
                    if(!empty($valor)) {
                        if($chave == 'turma_id')
                            $options['conditions']['ViewFormandos.turma_id'] = $valor;
                        else
                            $options['conditions']["$chave LIKE "] = "%{$valor}%";
                    }
            }
            $options['limit'] = $this->paginate['ViewFormandos']['limit'];
            $this->paginate['ViewFormandos'] = $options;
            $formandos = $this->paginate('ViewFormandos');
            $this->set('formandos', $formandos);
        }
    }
    
    function super_listar(){
        $this->layout = false;
        if (!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->Session->write("filtros.{$this->params['prefix']}.formandos", $this->data['ViewFormandos']);
        } else {
            ini_set('memory_limit','120M');
            set_time_limit(0);
            $options = array(
                'group' => array('ViewFormandos.id')
            );
            $options['fields'] = array(
                'ViewFormandos.*',
                "(select if(checkout = 'aberto',1,0) from turmas t where ViewFormandos.turma_id = t.id) as checkout",
                "(select if((select count(0) from protocolos p where ViewFormandos.id = " .
                "p.usuario_id and p.tipo = 'checkout' limit 1) > 0,1,0)) as fez_checkout"
            );
            $filtro = $this->Session->read("filtros.{$this->params['prefix']}.formandos");
            if ($filtro) {
                $this->data['ViewFormandos'] = $filtro;
                foreach ($filtro as $chave => $valor)
                    if(!empty($valor)) {
                        if($chave == 'turma_id')
                            $options['conditions']['ViewFormandos.turma_id'] = $valor;
                        else
                            $options['conditions']["$chave LIKE "] = "%{$valor}%";
                    }
            }
            $options['limit'] = 20;
            $this->paginate['ViewFormandos'] = $options;
            $formandos = $this->paginate('ViewFormandos');
            $this->set('formandos', $formandos);
        }
    }

    function planejamento_listar() {
        $turma = $this->obterTurmaLogada();
        $this->layout = false;
        if (!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->Session->write("filtros.{$this->params['prefix']}.formandos", $this->data['ViewFormandos']);
        } else {
            $options['conditions'] = array(
                'turma_id' => $turma['Turma']['id']
            );
            $filtro = $this->Session->read("filtros.{$this->params['prefix']}.formandos");
            if ($filtro) {
                $this->data['ViewFormandos'] = $filtro;
                foreach ($filtro as $chave => $valor)
                    if ($chave == 'codigo_formando')
                        $options['conditions']['codigo_formando LIKE '] = "%{$valor}%";
                    else
                        $options['conditions'][$chave] = $valor;
            }
            $options['limit'] = $this->paginate['ViewFormandos']['limit'];
            $this->paginate['ViewFormandos'] = $options;
            $formandos = $this->paginate('ViewFormandos');
            $selectCurso['false'] = 'Todos';
            foreach ($turma['CursoTurma'] as $cursoTurma) {
                $this->Curso->recursive = -1;
                $curso = $this->Curso->findById($cursoTurma['curso_id']);

                $selectCurso[$curso['Curso']['id']] = $curso['Curso']['nome'];
            }
            $selectSituacao = array(
                'false' => 'Todas',
                'ativo' => 'Ativo',
                'cancelado' => 'Cancelado',
                'atrasado' => 'Atrasado',
                'aberto' => 'Aberto'
            );
            $this->set('formandos', $formandos);
            $this->set('arraySelectDeCursos', $selectCurso);
            $this->set('selectSituacao', $selectSituacao);
        }
    }

    function comissao_listar() {
        $this->planejamento_listar();
    }
    
    function comercial_listar() {
        $this->planejamento_listar();
    }
    
    function foto_listar() {
        $this->planejamento_listar();
    }
    
    function atendimento_lista_excel() {
        $this->planejamento_listar();
    }
    

    function planejamento_excel() {
        $this->paginate['ViewFormandos']['limit'] = '99999';
        set_time_limit(10000);
        ini_set("memory_limit", "1024M");
        $this->layout = false;
        $this->set('turmaLogada', $this->obterTurmaLogada());
        $this->planejamento_listar();
        $this->render('excel');
    }

    function comissao_excel() {
        $this->planejamento_excel();
    }

    function planejamento_index() {
        $turma = $this->obterTurmaLogada();
        $formandos = $this->ViewFormandos->find('all', array(
            'conditions' => array(
                'ViewFormandos.turma_id' => $turma['Turma']['id']
            ),
            'joins' => array(
                array(
                    "table" => "cursos_turmas",
                    "type" => "left",
                    "alias" => "CursoTurma",
                    "conditions" => array(
                        "ViewFormandos.curso_turma_id = CursoTurma.id",
                    )
                ),
                array(
                    "table" => "cursos",
                    "type" => "left",
                    "alias" => "Curso",
                    "conditions" => array(
                        "CursoTurma.curso_id = Curso.id",
                    )
                )
            ),
            'fields' => array(
                'ViewFormandos.*',
                'Curso.nome',
            )
        ));
        $this->set('formandos', $formandos);
    }

    function montar_resumo_extras($extras, $arrayDeCampanhasAderidas) {
        // Montar lista que traduz campanhas_extra_id para informações de extra ( extra_id , extra_nome, evento_id, evento_nome )
        $this->CampanhasExtra->recursive = 2;
        $this->CampanhasExtra->bindModel(array('belongsTo' => array('Extra')));
        $this->Extra->unbindModel(array('hasAndBelongsToMany' => array('Campanha')));
        $campanhas_extras = $this->CampanhasExtra->find('all', array('conditions' => array('CampanhasExtra.campanha_id' => $arrayDeCampanhasAderidas)));
        $arrayCampanhasExtrasParaExtra = array();
        foreach ($campanhas_extras as $campanha_extra) {
            $campanha_extra_id = $campanha_extra['CampanhasExtra']['id'];
            $extra_id = $campanha_extra['CampanhasExtra']['extra_id'];
            $extra_nome = $campanha_extra['Extra']['nome'];
            $evento_id = $campanha_extra['Extra']['Evento']['id'];
            $evento_nome = $campanha_extra['Extra']['Evento']['nome'];

            if (!isset($arrayCampanhasExtrasParaExtra[$campanha_extra_id])) {
                $arrayCampanhasExtrasParaExtra[$campanha_extra_id] = array(
                    'extra_id' => $extra_id,
                    'extra_nome' => $extra_nome,
                    'evento_id' => $evento_id,
                    'evento_nome' => $evento_nome
                );
            }
        }

        foreach ($extras as $despesaExtra) {
            $campanha_usuario_id = $despesaExtra['CampanhasUsuario']['id'];
            if (!isset($arrayDeAdesoesCampanhas[$campanha_usuario_id])) {
                $arrayDeAdesoesCampanhas[$despesaExtra['CampanhasUsuario']['id']] = array(
                    'Campanha' => array(
                        'id' => $despesaExtra['CampanhasUsuario']['campanha_id'],
                        'nome' => $despesaExtra['CampanhasUsuario']['Campanha']['nome']
                    ),
                    'Parcelas' => array(),
                    'Extras' => array(),
                    'status' => 'paga',
                    'total_pago' => 0,
                    'total_devido' => 0,
                    'saldo' => 0
                );
            }
            $numero_parcela = $despesaExtra['Despesa']['parcela'];
            $arrayDeAdesoesCampanhas[$campanha_usuario_id]['Parcelas'][$numero_parcela] = array(
                'numero' => $numero_parcela,
                'correcao_igpm' => $despesaExtra['Despesa']['correcao_igpm'],
                'valor' => $despesaExtra['Despesa']['valor'],
                'multa' => $despesaExtra['Despesa']['multa'],
                'status' => $despesaExtra['Despesa']['status']
            );

            foreach ($despesaExtra['CampanhasUsuario']['CampanhasUsuariosCampanhasExtras'] as $extra_da_adesao) {
                $campanhas_extra_id = $extra_da_adesao['campanhas_extra_id'];

                // Aqui temos o campanhas_extra_id . Buscar o extra_id na lista previamente construida de campanhas_extra_id -> extra_id

                $arrayDeAdesoesCampanhas[$campanha_usuario_id]['Extras'][$campanhas_extra_id] = array(
                    'extra_id' => $arrayCampanhasExtrasParaExtra[$extra_da_adesao['campanhas_extra_id']]['extra_id'],
                    'extra_nome' => $arrayCampanhasExtrasParaExtra[$extra_da_adesao['campanhas_extra_id']]['extra_nome'],
                    'evento_id' => $arrayCampanhasExtrasParaExtra[$extra_da_adesao['campanhas_extra_id']]['evento_id'],
                    'evento_nome' => $arrayCampanhasExtrasParaExtra[$extra_da_adesao['campanhas_extra_id']]['evento_nome'],
                    'quantidade_solicitada' => $extra_da_adesao['quantidade'],
                    'retirou' => $extra_da_adesao['retirou']
                );
            }



            // Status de uma adesão começa como paga
            // Se o status de uma despesa da adesão for cancelada, toda a campanha está cancelada
            // Se for aberta e a adesão não estiver cancelada, marcar como aberta
            // Se for paga, mantém o status anterior
            if ($despesaExtra['Despesa']['status'] == 'cancelada') {
                $arrayDeAdesoesCampanhas[$campanha_usuario_id]['status'] = 'cancelada';
            } else if ($despesaExtra['Despesa']['status'] == 'aberta' && $arrayDeAdesoesCampanhas[$campanha_usuario_id]['status'] != 'cancelada') {
                $arrayDeAdesoesCampanhas[$campanha_usuario_id]['status'] = 'aberta';
            }

            // Somar total pago e devido
            if ($despesaExtra['Despesa']['status'] == 'aberta') {
                $arrayDeAdesoesCampanhas[$campanha_usuario_id]['total_devido'] += $despesaExtra['Despesa']['valor'] + $despesaExtra['Despesa']['multa'] + $despesaExtra['Despesa']['correcao_igpm'];
            } else if ($despesaExtra['Despesa']['status'] == 'paga') {
                $arrayDeAdesoesCampanhas[$campanha_usuario_id]['total_pago'] += $despesaExtra['Despesa']['valor'] + $despesaExtra['Despesa']['multa'] + $despesaExtra['Despesa']['correcao_igpm'];
            }

            $arrayDeAdesoesCampanhas[$campanha_usuario_id]['saldo'] = $arrayDeAdesoesCampanhas[$campanha_usuario_id]['total_pago'] - $arrayDeAdesoesCampanhas[$campanha_usuario_id]['total_devido'];
        }

        /*
         * A partir da estrutura da $arrayDeAdesoesCampanhas, 
         * criar um resumo de extras com a seguinte estrutura:
         * 
         * 	⁃ resumo_extras[extra_id]
         * 		⁃ extra_id
         * 		- extra_nome
         * 		- evento_id
         * 		- evento_nome
         * 		⁃ total_em_aberto
         * 		⁃ total_pago
         * 		⁃ total_cancelado
         * 		⁃ total_retirado
         * 
         */
        $arrayResumoExtras = array();
        if (!empty($arrayDeAdesoesCampanhas)) {
            foreach ($arrayDeAdesoesCampanhas as $adesao_campanha) {
                //debug($adesao_campanha);

                $status = $adesao_campanha['status'];
                if (!empty($adesao_campanha)) {
                    foreach ($adesao_campanha['Extras'] as $extra_adesao) {

                        if (!isset($arrayResumoExtras[$extra_adesao['extra_id']])) {
                            $arrayResumoExtras[$extra_adesao['extra_id']] = array(
                                'extra_id' => $extra_adesao['extra_id'],
                                'extra_nome' => $extra_adesao['extra_nome'],
                                'evento_id' => $extra_adesao['evento_id'],
                                'evento_nome' => $extra_adesao['evento_nome'],
                                'total_em_aberto' => 0,
                                'total_pago' => 0,
                                'total_cancelado' => 0,
                                'total_retirado' => 0
                            );
                        }

                        if ($status == 'aberta') {
                            $arrayResumoExtras[$extra_adesao['extra_id']]['total_em_aberto'] += $extra_adesao['quantidade_solicitada'];
                        } else if ($status == 'paga') {
                            $arrayResumoExtras[$extra_adesao['extra_id']]['total_pago'] += $extra_adesao['quantidade_solicitada'];
                        } else if ($status == 'cancelada') {
                            $arrayResumoExtras[$extra_adesao['extra_id']]['total_cancelado'] += $extra_adesao['quantidade_solicitada'];
                        }

                        if ($extra_adesao['retirou'] == 1) {
                            $arrayResumoExtras[$extra_adesao['extra_id']]['total_retirado'] += $extra_adesao['quantidade_solicitada'];
                        }
                    }
                }
            }
        }

        return $arrayResumoExtras;
    }
    
    function planejamento_gerar_excel() {
        set_time_limit(0);
        ini_set("memory_limit", "4096M");
        $this->layout = false;
        $this->set('turmaLogada', $this->obterTurmaLogada());
        $this->planejamento_index();
    }

    function comissao_gerar_excel() {
        $this->planejamento_gerar_excel();
    }

    function atendimento_gerar_excel() {
        $this->planejamento_gerar_excel();
    }
    
    function comercial_gerar_excel() {
        $this->planejamento_gerar_excel();
    }
    
    function foto_gerar_excel() {
        $this->planejamento_gerar_excel();
    }

    function selecionar_turma() {
        if($this->siteDeTurma && $this->turmaEstaLogada())
                $this->redirect("/formandos/confirmar_turma");
        $this->layout = 'metro/externo';
    }
    
    function app_cursos($usuarioId) {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $usuario = $this->Usuario->read(null,$usuarioId);
        $json = array('erro' => true);
        if($usuario) {
            $turma = $this->Turma->read(null,$usuario['ViewFormandos']['turma_id']);
            $info = null;
            $selectCurso = null;
            foreach ($turma['CursoTurma'] as $cursoTurma) {
                $curso = $this->Curso->findById($cursoTurma['curso_id']);
                $universidade = $curso['Faculdade']['Universidade'];
                $faculdade = $curso['Faculdade'];
                $dados = array();
                
                if(!empty($faculdade['sigla']))
                    $dados[] = $faculdade['sigla'];
                elseif(!empty($faculdade['nome']))
                    $dados[] = $faculdade['nome'];
                
                if(!empty($curso['Curso']['sigla']))
                    $dados[] = $curso['Curso']['sigla'];
                
                if(!empty($curso['Curso']['nome']))
                    $dados[] = $curso['Curso']['nome'];
                
                if(!empty($cursoTurma['turno']))
                    $dados[] = $cursoTurma['turno'];
                $info[$cursoTurma['id']] = implode(" - ", $dados);
            }
            $json = array(
                'erro' => false,
                'cursos' => $info
            );
        }
        echo json_encode($json);
    }
    
    function app_verificar_turma() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('erro' => true);
        if(isset($this->data['turma'])) {
            $this->Turma->unbindModel(array(
                'hasAndBelongsToMany' => array('Curso','Usuario')
            ),false);
            $turma = $this->Turma->read(null,$this->data['turma']);
            if(!$turma) {
                $json['mensagem'] = "Turma não encontrada";
            } elseif($turma['Turma']['status'] != 'fechada' ||
                $turma['Turma']['adesoes'] != 'abertas') {
                $json['mensagem'] = 'Adesões não estão abertas para esta turma';
            } else {
                $this->Parcelamento->recursive = -1;
                $planos = $this->Parcelamento->find('all',array(
                    'conditions' => array(
                        'turma_id' => $turma['Turma']['id'],
                        'data >=' => date('Y-m-\0\1')
                    ),
                    'order' => array('data')
                ));
                if(empty($planos)) {
                    $json['mensagem'] = "As adesões para esta turma ainda não estão abertas<br />" .
                            "Contate seu representante";
                } else {
                    setlocale(LC_TIME, 'pt_BR');
                    $dados = array();
                    foreach($planos as $plano) {
                        $mesAno = strftime('%b/%y', strtotime($plano['Parcelamento']['data']));
                        $titulo = $plano['Parcelamento']['titulo'];
                        if(empty($titulo))
                            $titulo = "Comum";
                        if(!isset($dados[$titulo]))
                            $dados[$titulo] = array();
                        if(!isset($dados[$titulo][$mesAno]))
                            $dados[$titulo][$mesAno] = array();
                        $dados[$titulo][$mesAno][$plano['Parcelamento']['id']] = array(
                            'parcelas' => $plano['Parcelamento']['parcelas'],
                            'valor' => $plano['Parcelamento']['valor']
                        );
                    }
                    $turma['Turma']['parcelamentos'] = $dados;
                    $cursos = null;
                    foreach ($turma['CursoTurma'] as $cursoTurma) {
                        $curso = $this->Curso->findById($cursoTurma['curso_id']);
                        $i = array();

                        if(!empty($curso['Faculdade']['Universidade']['sigla']))
                            $i[] = $curso['Faculdade']['Universidade']['sigla'];
                        elseif(!empty($curso['Faculdade']['Universidade']['nome']))
                            $i[] = $curso['Faculdade']['Universidade']['nome'];

                        if(!empty($curso['Faculdade']['nome']))
                            $i[] = $curso['Faculdade']['nome'];
                        elseif(!empty($curso['Faculdade']['sigla']))
                            $i[] = $curso['Faculdade']['sigla'];

                        if(!empty($cursoTurma['turno']))
                            $i[] = $cursoTurma['turno'];
                        
                        $cursos[$cursoTurma['id']] = implode(" - ", $i);
                    }
                    $turma['Turma']['cursos'] = $cursos;
                    $json = array(
                        'turma' => $turma['Turma'],
                        'erro' => false
                    );
                }
            }
        }
        echo json_encode($json);
    }

    function confirmar_turma($turma_id=null) {
        $this->layout = 'metro/externo';
        if($this->siteDeTurma && $this->turmaEstaLogada()) {
            $this->data = $turma = $this->obterTurmaLogada();
        } elseif($this->data['Turma']['id'] != null) {
            $this->Turma->id = $this->data['Turma']['id'];
            $turma = $this->Turma->findById($this->data['Turma']['id']);
        } elseif($turma_id) {
            $turma = $this->Turma->findById($turma_id);
        }
        if (empty($turma['Turma'])) {
            $this->Session->setFlash('Turma não existente', 'metro/flash/error');
            $this->redirect("/cadastro");
        } elseif ($turma['Turma']['status'] != 'fechada' ||
                $turma['Turma']['adesoes'] != 'abertas') {
            $this->Session->setFlash('Adesões não estão abertas para esta turma.', 'flash_metro_error');
            $this->redirect("/cadastro");
        } else {
            $this->Parcelamento->recursive = -1;
            $parcelamentos = $this->Parcelamento->find('all', array(
                'conditions' => array('turma_id' => $turma['Turma']['id']),
                'data >=' => date('Y-m-\0\1', strtotime('now'))
                    )
            );
            if (empty($parcelamentos)) {
                $this->Session->setFlash('As adesões para esta turma ainda não estão abertas.
                    Contate seu representante.', 'metro/flash/error');
                $this->redirect("/cadastro");
            }
        }
        $this->set('turma', $turma);
        $info = null;
        $selectCurso = null;
        foreach ($turma['CursoTurma'] as $cursoTurma) {
            $curso = $this->Curso->findById($cursoTurma['curso_id']);
            $faculdade = $curso['Faculdade'];
            $universidade = $curso['Faculdade']['Universidade'];
            if (empty($info['universidades'][$universidade['id']]['nome']))
                $info['universidades'][$universidade['id']]['nome'] = $universidade['nome'];
            if (empty($info['universidades'][$universidade['id']]['faculdades'][$faculdade['id']]['nome']))
                $info['universidades'][$universidade['id']]['faculdades'][$faculdade['id']]['nome'] = $faculdade['nome'];
            $info['universidades'][$universidade['id']]['faculdades'][$faculdade['id']]['cursos'][] = $curso['Curso']['nome'] .
                    ' - ' . $cursoTurma['turno'];
            $selectCurso[$cursoTurma['id']] = $curso['Curso']['nome'] . ' - ' . $cursoTurma['turno'] . ' - ' .
                    $universidade['nome'] . ' - ' . $faculdade['nome'];
        }
        $this->set('info', $info);
        setlocale(LC_TIME, 'pt_BR');
        $dados = array();
        $this->Parcelamento->recursive = -1;
        $planos = $this->Parcelamento->find('all',array(
            'conditions' => array(
                'turma_id' => $turma['Turma']['id'],
                'data >=' => date('Y-m-\0\1')
            ),
            'order' => array('data')
        ));
        foreach($planos as $plano) {
            $anoMes = strftime('%b/%y', strtotime($plano['Parcelamento']['data']));
            if(!isset($dados[$plano['Parcelamento']['titulo']]))
                $dados[$plano['Parcelamento']['titulo']] = array();
            if(!isset($dados[$plano['Parcelamento']['titulo']][$anoMes]))
                $dados[$plano['Parcelamento']['titulo']][$anoMes] = array();
            $dados[$plano['Parcelamento']['titulo']][$anoMes][$plano['Parcelamento']['parcelas']] = array(
                'parcelas' => $plano['Parcelamento']['parcelas'],
                'valor' => $plano['Parcelamento']['valor']
            );
        }
        $this->set('planos',array_keys($dados));
        $this->set('parcelamentos',$dados);
    }

    function inicio() {
        $this->layout = 'externo';

        /* Verifica se foi definida a variavel comissao na url. */
        if (isset($this->params['url']['comissao'])) {
            /* Pega da url se eh o cadastro de um membro da comissao. */
            /* (caso nao seja, nao sera setada a variavel comissao) */
            $comissao = $this->params['url']['comissao'];
            /* Passa para o hidden do html. */
            $this->data['Turma']['comissao'] = $comissao;
        }
    }
    
    function app_cadastrar() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('erro' => true, 'mensagem' => array());
        if(isset($this->data['Usuario']) && isset($this->data['FormandoProfile']) &&
                isset($this->data['Turma'])) {
            $turma = $this->Turma->findById($this->data['Turma']['id']);
            $data = $this->data;
            $data['FormandoProfile']['data_adesao'] = date('Y-m-d H:i:s', strtotime('now'));
            $data['FormandoProfile']['aderido'] = 1;
            $data['Usuario']['grupo'] = 'formando';
            $data['Usuario']['ativo'] = 1;
            $data['FormandoProfile']['pertence_comissao'] = 0;
            $impresso = $data['FormandoProfile']['forma_pagamento'] == "boleto_impresso";
            $this->Parcelamento->id = $data['FormandoProfile']['parcelas'];
            $data['FormandoProfile']['parcelamento_id'] = $data['FormandoProfile']['parcelas'];
            $this->Parcelamento->recursive = -1;
            $parcelamento = $this->Parcelamento->read();
            if($parcelamento) {
                $data['FormandoProfile']['parcelas_adesao'] = $parcelamento['Parcelamento']['parcelas'];
                $data['FormandoProfile']['valor_adesao'] = $parcelamento['Parcelamento']['valor'] +
                        ($impresso ? (float) 20.00 : 0);
                $data['FormandoProfile']['data_inicio_pagamento'] = $parcelamento['Parcelamento']['data'];

                if ($parcelamento['Parcelamento']['valor'] > 0)
                    $despesasArray = $this->geraDespesas(
                        $parcelamento['Parcelamento']['data'],
                        $data['FormandoProfile']['dia_vencimento_parcelas'],
                        $parcelamento['Parcelamento']['parcelas'],
                        $parcelamento['Parcelamento']['valor'],
                        $impresso);
                $this->Usuario->bindModel(array(
                    'hasMany' => array(
                        'Despesa' => array(
                            'foreignKey' => 'usuario_id'
                        )
                    )
                ));
                $data['Despesa'] = $despesasArray;
                $dias_permitidos = array();
                for ($i = 1; $i < 29; $i++)
                    $dias_permitidos[$i] = $i;
                if (!in_array($data['FormandoProfile']['dia_vencimento_parcelas'], $dias_permitidos))
                    $data['FormandoProfile']['dia_vencimento_parcelas'] = 15;
            } else {
                $json['mensagem'][] = "Erro ao gerar parcelas";
                $json['mensagem'][] = "Contate seu representante";
            }
            if($this->Usuario->saveAll($data, array('validate' => 'only'))) {
               if(isset($data['Usuario']['foto_perfil'])) {
                    $imagem = base64_decode($this->data['Usuario']['foto_perfil']);
                    $nomeArquivo = "u" . Security::hash(microtime(true) * 1000, 'sha1', true);
                    $dir = "upload/foto_perfil/{$nomeArquivo}.jpeg";
                    $path = APP . "webroot/$dir";
                    if(file_put_contents($path, $imagem)) {
                        App::import('Component', 'FotoPerfil');
                        $this->FotoPerfil = new FotoPerfilComponent();
                        $this->FotoPerfil->criarArquivos($path,$nomeArquivo,".jpeg");
                        $data['Usuario']['diretorio_foto_perfil'] = $dir;
                    }
                }
                $codigo = $this->ViewFormandos->find('first',array(
                    'conditions' => array(
                        'turma_id' => $turma['Turma']['id']
                    ),
                    'order' => array('codigo_formando' => 'desc')
                ));
                $proximo_codigo_formando = (int) substr($codigo['ViewFormandos']['codigo_formando'],-3) + 1;
                $data['FormandoProfile']['codigo_formando'] = $proximo_codigo_formando;
                $this->FormandoProfile->query('LOCK TABLES formandos_profile WRITE,usuarios WRITE,
                    despesas WRITE,turmas_usuarios WRITE;');
                if($this->Usuario->saveAll($data)) {
                    $this->FormandoProfile->query('UNLOCK TABLES;');
                    $this->enviarEmailNovoFormando($data);
                    $usuario = $this->Usuario->find('first',array(
                        'conditions' => array(
                            'Usuario.email' => $data['Usuario']['email']
                        )
                    ));
                    if($usuario) {
                        $this->Turma->unbindModelAll();
                        $turma = $this->Turma->read(null,$usuario['ViewFormandos']['turma_id']);
                        $usuario['Turma'] = $turma['Turma'];
                    }
                    $json = array(
                        'erro' => false,
                        'usuario' => $usuario
                    );
                } else {
                    $this->FormandoProfile->query('UNLOCK TABLES;');
                    if(isset($this->Usuario->validationErrors['Usuario']))
                        $json['mensagem'] = array_merge($json['mensagem'],array_values($this->Usuario->validationErrors['Usuario']));
                    if(isset($this->Usuario->validationErrors['FormandoProfile']))
                        $json['mensagem'] = array_merge($json['mensagem'],array_values($this->Usuario->validationErrors['FormandoProfile']));
                }
            } else {
                if(isset($this->Usuario->validationErrors['Usuario']))
                    $json['mensagem'] = array_merge($json['mensagem'],array_values($this->Usuario->validationErrors['Usuario']));
                if(isset($this->Usuario->validationErrors['FormandoProfile']))
                    $json['mensagem'] = array_merge($json['mensagem'],array_values($this->Usuario->validationErrors['FormandoProfile']));
            }
        }
        echo json_encode($json);
    }

    function cadastrar() {
        $this->layout = 'metro/externo';
        if ($this->data['Turma']['id'] != null) {
            $this->Turma->id = $this->data['Turma']['id'];
            $turma = $this->Turma->findById($this->data['Turma']['id']);
            $this->Session->write('turma_adesao', $turma);
        } else {
            $turma = $this->Session->read('turma_adesao');
        }
        if (empty($turma['Turma'])) {
            $this->Session->setFlash('Turma não existente', 'metro/flash/error');
            $this->redirect("/cadastro");
        } elseif ($turma['Turma']['status'] != 'fechada' ||
                $turma['Turma']['adesoes'] != 'abertas') {
            $this->Session->setFlash('Adesões não estão abertas para esta turma.', 'flash_metro_error');
            $this->redirect("/cadastro");
        } else {
            $this->Parcelamento->recursive = -1;
            $parcelamentos = $this->Parcelamento->find('all', array(
                'conditions' => array('turma_id' => $turma['Turma']['id'])
                    )
            );
            if (empty($parcelamentos)) {
                $this->Session->setFlash('As adesões para esta turma ainda não estão abertas.
                    Contate seu representante.', 'metro/flash/error');
                $this->redirect("/cadastro");
            }
        }
        $this->set('turma', $turma);
        $info = null;
        $selectCurso = null;
        $usuarioLogado = $this->Session->read('Usuario');
        $planoEconomico = $turma['Turma']['plano_economico'] == 1;
        if ($usuarioLogado)
            if ($usuarioLogado['Usuario']['grupo'] == 'atendimento')
                $planoEconomico = true;
        $this->set('planoEconomico', $planoEconomico);
        foreach ($turma['CursoTurma'] as $cursoTurma) {
            $curso = $this->Curso->findById($cursoTurma['curso_id']);
            $faculdade = $curso['Faculdade'];
            $universidade = $curso['Faculdade']['Universidade'];
            if (empty($info['universidades'][$universidade['id']]['nome']))
                $info['universidades'][$universidade['id']]['nome'] = $universidade['nome'];
            if (empty($info['universidades'][$universidade['id']]['faculdades'][$faculdade['id']]['nome']))
                $info['universidades'][$universidade['id']]['faculdades'][$faculdade['id']]['nome'] = $faculdade['nome'];
            $info['universidades'][$universidade['id']]['faculdades'][$faculdade['id']]['cursos'][] = $curso['Curso']['nome'] .
                    ' - ' . $cursoTurma['turno'];
            $selectCurso[$cursoTurma['id']] = $curso['Curso']['nome'] . ' - ' . $cursoTurma['turno'] . ' - ' .
                    $universidade['nome'] . ' - ' . $faculdade['nome'];
        }
        $this->set('info', $info);
        $this->set('cursoTurma', $selectCurso);
        if (!empty($this->data['Usuario'])) {
            $data = $this->data;
            $dataNascimento = str_replace('/', '-', $data['FormandoProfile']['data_nascimento']);
            $dateTime = $this->create_date_time_from_format('d-m-Y', $dataNascimento);
            if ($dateTime)
                $data['FormandoProfile']['data_nascimento'] = date_format($dateTime, 'Y-m-d');
            else
                $data['FormandoProfile']['data_nascimento'] = null;
            $data['FormandoProfile']['pertence_comissao'] = 0;
            $data['FormandoProfile']['cpf'] = str_replace(array(",", ".", "-"), "", $data['FormandoProfile']['cpf']);
            $data['FormandoProfile']['end_cep'] = str_replace(array(",", ".", "-"), "", $data['FormandoProfile']['end_cep']);
            $data['Usuario']['nome'] = $this->formata_nome($data['Usuario']['nome']);
            $data['FormandoProfile']['nome_pai'] = $this->formata_nome($data['FormandoProfile']['nome_pai']);
            $data['FormandoProfile']['nome_mae'] = $this->formata_nome($data['FormandoProfile']['nome_mae']);
            $data['FormandoProfile']['sexo'] = isset($data['FormandoProfile']['sexo']) ? 'M' : 'F';
            $data['FormandoProfile']['data_adesao'] = date('Y-m-d H:i:s', strtotime('now'));
            $data['Usuario']['grupo'] = 'formando';
            $data['Usuario']['ativo'] = 1;
            if ($turma['Turma']['cadastro_sem_adesao'] == 0)
                $data['Usuario']['aderir'] = 1;
            if ($data['Usuario']['aderir'] == 0) {
                $data['FormandoProfile']['aderido'] = 0;
                unset($this->FormandoProfile->validate['forma_pagamento']);
                unset($this->FormandoProfile->validate['parcelas']);
            } else {
                $data['FormandoProfile']['aderido'] = 1;
                $impresso = $data['FormandoProfile']['forma_pagamento'] == "boleto_impresso";
                if ($data['FormandoProfile']['parcelas'] == 'plano_economico' &&
                        $planoEconomico && isset($data['plano_economico'])) {
                    $data['plano_economico']['valor']+= ($impresso ? (float) 20.00 : 0);
                    $data['FormandoProfile']['parcelas_adesao'] = count($data['plano_economico']['parcelas']);
                    $data['FormandoProfile']['valor_adesao'] = $data['plano_economico']['valor'];
                    $despesasArray = $this->gerarDespesasPlanoEconomico(
                            $data['plano_economico']['parcelas'], $data['FormandoProfile']['data_inicio_pagamento'], $data['FormandoProfile']['dia_vencimento_parcelas'], $impresso);
                    unset($this->FormandoProfile->validate['parcelas']);
                } else {
                    $this->Parcelamento->id = $data['FormandoProfile']['parcelas'];
                    $data['FormandoProfile']['parcelamento_id'] = $data['FormandoProfile']['parcelas'];
                    $this->Parcelamento->recursive = -1;
                    $parcelamento = $this->Parcelamento->read();
                    if ($parcelamento) {
                        $data['FormandoProfile']['parcelas_adesao'] = $parcelamento['Parcelamento']['parcelas'];
                        $data['FormandoProfile']['valor_adesao'] = $parcelamento['Parcelamento']['valor'] +
                                ($impresso ? (float) 20.00 : 0);
                        $data['FormandoProfile']['data_inicio_pagamento'] = $parcelamento['Parcelamento']['data'];

                        if ($parcelamento['Parcelamento']['valor'] > 0)
                            $despesasArray = $this->geraDespesas(
                                    $parcelamento['Parcelamento']['data'], $data['FormandoProfile']['dia_vencimento_parcelas'], $parcelamento['Parcelamento']['parcelas'], $parcelamento['Parcelamento']['valor'], $impresso);
                    }
                }
                if (isset($despesasArray)) {
                    $this->Usuario->bindModel(array('hasMany' => array('Despesa' =>
                            array('foreignKey' => 'usuario_id'))));
                    $data['Despesa'] = $despesasArray;
                    $dias_permitidos = array();
                    for ($i = 1; $i < 29; $i++)
                        $dias_permitidos[$i] = $i;
                    if (!in_array($data['FormandoProfile']['dia_vencimento_parcelas'], $dias_permitidos))
                        $data['FormandoProfile']['dia_vencimento_parcelas'] = 15;
                }
            }

            if ($this->Usuario->saveAll($data, array('validate' => 'only'))) {
                $codigoFormando = $this->ViewFormandos->find('all', array(
                    'conditions' => array(
                        'ViewFormandos.turma_id' => $turma['Turma']['id']
                    ),
                    'order' => array('ViewFormandos.codigo_formando' => 'desc')
                ));
                $proximo_codigo_formando = $codigoFormando[0]['ViewFormandos']['codigo_formando'] + 1;
                $data['FormandoProfile']['codigo_formando'] = substr($proximo_codigo_formando, 4);
                $this->FormandoProfile->query('LOCK TABLES formandos_profile WRITE,usuarios WRITE,
                    despesas WRITE,turmas_usuarios WRITE;');
                if ($data['Usuario']['foto_src'] != '') {
                    $this->loadModel('Arquivo');
                    $dir = 'upload/foto_perfil/';
                    $path = WWW_ROOT . $dir;
                    $nome_foto = 'u' . Security::hash(microtime(true) * 1000, 'sha1', true);
                    $arquivo = $path . $nome_foto;
                    if ($this->Arquivo->saveBase64File($data['Usuario']['foto_src'], $arquivo))
                        $data['Usuario']['diretorio_foto_perfil'] = $dir . $nome_foto;
                }
                if ($this->Usuario->saveAll($data)) {
                    $this->Session->setFlash(__('Cadastro efetuado com sucesso.', true), 'metro/flash/success');
                    $this->FormandoProfile->query('UNLOCK TABLES;');
                    $this->Session->write('codigo_formando_cadastro', $proximo_codigo_formando);
                    debug($proximo_codigo_formando);
                    $this->Session->write('usuarioCadastrado', $data);
                    $this->enviarEmailNovoFormando($data);
                    $this->redirect("/formandos/obrigado/");
                } else {
                    $this->FormandoProfile->query('UNLOCK TABLES;');
                    unset($this->data['Usuario']['senha']);
                    unset($this->data['Usuario']['confirmar']);
                    $erros = array_merge(array_values($this->Usuario->validationErrors), array_values($this->FormandoProfile->validationErrors));
                    $this->Session->setFlash($erros, 'metro/flash/error');
                }
            } else {
                unset($this->data['Usuario']['senha']);
                unset($this->data['Usuario']['confirmar']);
                $erros = array_merge(array_values($this->Usuario->validationErrors), array_values($this->FormandoProfile->validationErrors));
                $this->Session->setFlash($erros, 'metro/flash/error');
            }
        } else {
            $this->data['Usuario']['aderir'] = 1;
            $this->data['FormandoProfile']['sexo'] = "M";
            $this->data['FormandoProfile']['parcelas'] = '';
            $this->data['FormandoProfile']['planos'] = '';
            $this->data['FormandoProfile']['diretorio_foto_perfil'] = "img/uknown_user.gif";
        }
        if ($turma['Turma']['cadastro_sem_adesao'] == 0)
            $this->data['Usuario']['aderir'] = 1;
        if (!isset($this->data['plano_economico']))
            $this->data['plano_economico'] = array('parcelas' => array(), 'valor' => 0);
        $this->set('erros', isset($erros));
        $this->Parcelamento->recursive = -1;
        if(!in_array($turma['Turma']['id'], array('6028','6984')))
            $conditions = array(
                'conditions' => array(
                    'turma_id' => $turma['Turma']['id'],
                    'data >=' => date('Y-m-\0\1', strtotime('now'))
                )
            );
        else
            $conditions = array(
                'conditions' => array(
                    'turma_id' => $turma['Turma']['id'],
                )
            );
        $parcelamentos = $this->Parcelamento->find('all', $conditions);
        $datas = array();

        foreach ($parcelamentos as $parcelamento) {
            setlocale(LC_TIME, 'ptb', 'pt_BR', 'portuguese-brazil', 'bra', 'brazil', 'pt_BR.utf-8', 'pt_BR.iso-8859-1', 'br');
            $data = $parcelamento['Parcelamento']['data'];
            $aux_data_parcela = strftime("%Y%m", strtotime($data));
            $aux_data_atual = strftime("%Y%m", strtotime('now'));
            if(!in_array($turma['Turma']['id'], array('6028','6984'))){
                if ($aux_data_parcela >= $aux_data_atual) {
                    if (!isset($datas[$data]))
                        $datas[$data] = ucfirst(strftime('%b / %Y', strtotime($data)));
                }
            }else{
                if (!isset($datas[$data]))
                    $datas[$data] = ucfirst(strftime('%b / %Y', strtotime($data)));
            }
        }
        $select_forma_pagamento = array(
            'boleto_online' => 'Boleto Online - sem custos adicionais',
            'boleto_impresso' => 'Boleto Entregue Em Casa - custo adicional de 20 reais',
            'cheque' => 'Cheque'
        );
        $this->set('select_forma_pagamento', $select_forma_pagamento);
        $this->set('select_data_inicio_pagamento', $datas);
        $this->set('select_parcelas', null);
        $this->set('select_convites_extras', $datas);
        $this->set('select_mesas_extras', $datas);
        $this->set('fields', $this->FormandoProfile->fields);
    }
    
    function formando_verificaEmail(){
        $this->layout = false;
        $this->autoRender = false;
        Configure::write('debug', 0);
        if(!empty($this->data)){
            $email = $this->Usuario->find('count', array('conditions' => array('Usuario.email' => $this->data['email'])));
            if($email < 1)
                $response = array('mensagem' => '1');
            else
                $response = array('mensagem' => '0');
        }
        echo json_encode($response);
    }
    
    function super_verificaEmail(){
        $this->formando_verificaEmail();
    }

    function cadastro() {
        $this->layout = 'externo';
    }

    function adicionar_nao_pagante() {
        
    }

    function adicionar() {
        $this->layout = 'externo';
        $eh_comissao = isset($this->data['Turma']['comissao']) ? $this->data['Turma']['comissao'] : false;
        if ($this->data['Turma']['id'] != null) {
            $this->Turma->id = $this->data['Turma']['id'];
            $turma = $this->Turma->findById($this->data['Turma']['id']);
            $this->Session->write('turma_adesao', $turma);
        } else {
            $turma = $this->Session->read('turma_adesao');
        }
        if (empty($turma['Turma'])) {
            $this->Session->setFlash('Turma não existente', 'flash_erro');
            $this->redirect("/cadastro");
        } elseif (($turma['Turma']['status'] != 'fechada' ||
                $turma['Turma']['adesoes'] != 'abertas') && !$eh_comissao) {
            $this->Session->setFlash('Adesões não estão abertas para esta turma.', 'flash_erro');
            $this->redirect("/cadastro");
        } elseif (!$eh_comissao) {
            $this->Parcelamento->recursive = -1;
            $parcelamentos = $this->Parcelamento->find('all', array(
                'conditions' => array('turma_id' => $turma['Turma']['id']),
                'data >=' => date('Y-m-\0\1', strtotime('now'))
                    )
            );
            if (empty($parcelamentos)) {
                $this->Session->setFlash('As adesões para esta turma ainda não estão abertas. 
						Contate seu representante.', 'flash_erro');
                $this->redirect("/cadastro");
            }
        }

        $this->set('turma', $turma);
        $usuarioLogado = $this->Session->read('Usuario');
        $planoEconomico = false;
        if ($usuarioLogado)
            if ($usuarioLogado['Usuario']['grupo'] == 'atendimento')
                $planoEconomico = true;
        $this->set('planoEconomico', $planoEconomico);
        $info = null;
        $selectCurso = null;
        foreach ($turma['CursoTurma'] as $cursoTurma) {
            $curso = $this->Curso->findById($cursoTurma['curso_id']);
            $faculdade = $curso['Faculdade'];
            $universidade = $curso['Faculdade']['Universidade'];
            if (empty($info['universidades'][$universidade['id']]['nome']))
                $info['universidades'][$universidade['id']]['nome'] = $universidade['nome'];
            if (empty($info['universidades'][$universidade['id']]['faculdades'][$faculdade['id']]['nome']))
                $info['universidades'][$universidade['id']]['faculdades'][$faculdade['id']]['nome'] = $faculdade['nome'];
            $info['universidades'][$universidade['id']]['faculdades'][$faculdade['id']]['cursos'][] = $curso['Curso']['nome'] .
                    ' - ' . $cursoTurma['turno'];
            $selectCurso[$cursoTurma['id']] = $curso['Curso']['nome'] . ' - ' . $cursoTurma['turno'] . ' - ' .
                    $universidade['nome'] . ' - ' . $faculdade['nome'];
        }
        $this->set('info', $info);
        $this->set('cursoTurma', $selectCurso);
        if (!empty($this->data['Usuario'])) {
            $aderir = isset($this->data['Adesao']['aderir']) ? $this->data['Adesao']['aderir'] : 'true';
            $dateTime = $this->create_date_time_from_format('d-m-Y', $this->data['FormandoProfile']['data_nascimento']);

            if ($dateTime)
                $this->data['FormandoProfile']['data_nascimento'] = date_format($dateTime, 'Y-m-d');
            else
                $this->data['FormandoProfile']['data_nascimento'] = null;

            $this->data['FormandoProfile']['cpf'] = str_replace(array(",", ".", "-"), "", $this->data['FormandoProfile']['cpf']);
            $this->data['FormandoProfile']['end_cep'] = str_replace(array(",", ".", "-"), "", $this->data['FormandoProfile']['end_cep']);
            $this->data['Usuario']['nome'] = $this->formata_nome($this->data['Usuario']['nome']);
            $this->data['Usuario']['nome_pai'] = $this->formata_nome($this->data['Usuario']['nome']);
            $this->data['Usuario']['nome_mae'] = $this->formata_nome($this->data['Usuario']['nome_pai']);
            $this->data['Usuario']['cidade'] = $this->formata_nome($this->data['Usuario']['nome_mae']);

            //Chave utilizada para validacao, no caso de um formando que nao pertenca a comissao.
            $erro_parcelamento = false;
            $comissao = $this->data['FormandoProfile']['pertence_comissao'];
            if ($aderir === 'false') {
                $this->data['Usuario']['ativo'] = 1;
                $this->data['FormandoProfile']['aderido'] = 0;
                $this->data['Usuario']['grupo'] = 'formando';
                unset($this->FormandoProfile->validate['forma_pagamento']);
                unset($this->FormandoProfile->validate['parcelas']);
            } elseif ($comissao) {
                $this->data['Usuario']['ativo'] = 0;
                $this->data['FormandoProfile']['aderido'] = 0;
                $this->data['Usuario']['grupo'] = 'comissao';
                unset($this->FormandoProfile->validate['forma_pagamento']);
                unset($this->FormandoProfile->validate['parcelas']);
            } else {
                $this->data['Usuario']['ativo'] = 1;
                $this->data['Usuario']['grupo'] = 'formando';
                $this->data['FormandoProfile']['aderido'] = 1;
                if (($turma['Turma']['plano_economico'] == 1 || $planoEconomico) &&
                        isset($this->data['parcelamento']) &&
                        isset($this->data['parcelas'])) {
                    $planoEconomico = true;
                    $this->Parcelamento->id = $this->data['FormandoProfile']['parcelas'];
                    $this->data['FormandoProfile']['parcelamento_id'] = $this->data['FormandoProfile']['parcelas'];
                    $this->Parcelamento->recursive = -1;
                    $parcelamento = $this->Parcelamento->read();
                } else {
                    $planoEconomico = false;
                    $this->Parcelamento->id = $this->data['FormandoProfile']['parcelas'];
                    $this->data['FormandoProfile']['parcelamento_id'] = $this->data['FormandoProfile']['parcelas'];
                    $this->Parcelamento->recursive = -1;
                    $parcelamento = $this->Parcelamento->read();
                }

                if ($parcelamento['Parcelamento']['turma_id'] != $turma['Turma']['id']) {
                    $erro_parcelamento = true;
                    $parcelamento = null;
                }
                if ($parcelamento) {
                    $impresso = $this->data['FormandoProfile']['forma_pagamento'] == "boleto_impresso" ? TRUE : FALSE;
                    $this->data['FormandoProfile']['parcelas_adesao'] = $parcelamento['Parcelamento']['parcelas'];
                    $this->data['FormandoProfile']['valor_adesao'] = $parcelamento['Parcelamento']['valor'] +
                            ($impresso ? (float) 20.00 : 0);
                    $this->data['FormandoProfile']['data_inicio_pagamento'] = $parcelamento['Parcelamento']['data'];
                    if (!$planoEconomico) {
                        if ($parcelamento['Parcelamento']['valor'] > 0) {
                            $despesasArray = $this->geraDespesas(
                                    $parcelamento['Parcelamento']['data'], $this->data['FormandoProfile']['dia_vencimento_parcelas'], $parcelamento['Parcelamento']['parcelas'], $parcelamento['Parcelamento']['valor'], $impresso);
                        }
                    } else {
                        $despesasArray = $this->gerarDespesasPlanoEconomico($this->data['parcelas'], $parcelamento['Parcelamento']['data'], $this->data['FormandoProfile']['dia_vencimento_parcelas'], $impresso);
                    }
                    if (isset($despesasArray))
                        $this->data['Despesa'] = $despesasArray;
                }

                $this->Usuario->bindModel(array('hasMany' => array('Despesa' =>
                        array('foreignKey' => 'usuario_id'))));

                $this->data['FormandoProfile']['data_adesao'] = date('Y-m-d H:i:s', strtotime('now'));
                $dias_permitidos = array();

                for ($i = 1; $i < 29; $i++)
                    $dias_permitidos[$i] = $i;
                if (!in_array($this->data['FormandoProfile']['dia_vencimento_parcelas'], $dias_permitidos))
                    $this->data['FormandoProfile']['dia_vencimento_parcelas'] = 15;
            }
            if ($this->Usuario->saveAll($this->data, array('validate' => 'only')) && !$erro_parcelamento) {
                //Buscar código do formando na turm
                $this->CursoTurma->recursive = -1;
                $cursos = $this->CursoTurma->find('list', array('conditions' => array('turma_id' => $turma['Turma']['id'])));
                $cod = $this->FormandoProfile->find('first', array('conditions' => array('FormandoProfile.curso_turma_id' => $cursos),
                    'fields' => 'MAX(codigo_formando)', 'recursive' => -1));
                $proximo_codigo_formando = $cod[0]['MAX(codigo_formando)'] + 1;
                $this->data['FormandoProfile']['codigo_formando'] = $proximo_codigo_formando;
                $this->FormandoProfile->query('LOCK TABLES formandos_profile WRITE,usuarios WRITE,despesas WRITE,turmas_usuarios WRITE;');

                if (substr_count($this->data['FormandoProfile']['diretorio_foto_perfil'], "temp") > 0) {
                    $diretorioFinalDaFotoDoProfile = str_replace('temp', 'foto_perfil', $this->data['FormandoProfile']['diretorio_foto_perfil']);
                    copy($this->data['FormandoProfile']['diretorio_foto_perfil'], $diretorioFinalDaFotoDoProfile);
                    unlink($this->data['FormandoProfile']['diretorio_foto_perfil']);
                    $this->data['FormandoProfile']['diretorio_foto_perfil'] = $diretorioFinalDaFotoDoProfile;
                }

                if ($this->Usuario->saveAll($this->data)) {
                    $this->Session->setFlash(__('Cadastro efetuado com sucesso.', true), 'flash_sucesso');
                    $this->FormandoProfile->query('UNLOCK TABLES;');
                    $retorno_codigo_formando = $turma['Turma']['id'] . str_pad($proximo_codigo_formando, 3, '0', STR_PAD_LEFT);
                    $this->Session->write('codigo_formando_cadastro', $retorno_codigo_formando);
                    $this->enviarEmailNovoFormando($this->data);
                    $this->redirect("/formandos/obrigado/");
                } else {
                    $this->FormandoProfile->query('UNLOCK TABLES;');
                    unset($this->data['Usuario']['senha']); //esta com problema.
                    unset($this->data['Usuario']['confirmar']);
                    $this->Session->setFlash('Ocorreu um erro ao efetuar o cadastro. Verifique as seções em vermelho.', 'flash_erro');
                    debug($this->FormandoProfile->validationErrors);
                }
            } else {
                if ($erro_parcelamento)
                    $this->Usuario->validationErrors['FormandoProfile']['parcelas'] = 'Parcelamento inválido.';

                $this->Session->setFlash('Ocorreu um erro ao efetuar o cadastro. Verifique as seções em vermelho.', 'flash_erro');
                debug($this->FormandoProfile->validationErrors);
            }
        } else {
            /* Envia o valor da variavel comissao para a view. */
            /* (se nao for cafastro da comissao, esta nao tera valor algum) */
            $comissao = ($this->data['Turma']['comissao'] == 1 ? 1 : 0);
            $this->data['FormandoProfile']['pertence_comissao'] = $comissao;
            $this->data['FormandoProfile']['diretorio_foto_perfil'] = "img/uknown_user.gif";
        }


        $this->set('comissao', $comissao); /* Utilizado num campo hidden e em um include _form_pagamento. */

        $this->Parcelamento->recursive = -1;
        $parcelamentos = $this->Parcelamento->find('all', array(
            'conditions' => array(
                'turma_id' => $turma['Turma']['id'],
                'data >=' => date('Y-m-\0\1', strtotime('now')))
                )
        );

        $datas = array();

        foreach ($parcelamentos as $parcelamento) {
            setlocale(LC_TIME, 'ptb', 'pt_BR', 'portuguese-brazil', 'bra', 'brazil', 'pt_BR.utf-8', 'pt_BR.iso-8859-1', 'br');
            $data = $parcelamento['Parcelamento']['data'];
            $aux_data_parcela = strftime("%Y%m", strtotime($data));
            $aux_data_atual = strftime("%Y%m", strtotime('now'));
            if ($aux_data_parcela >= $aux_data_atual) {
                if (!isset($datas[$data]))
                    $datas[$data] = ucfirst(strftime('%b / %Y', strtotime($data)));
            } else {
                debug("Ignorando: " . ucfirst(strftime('%B / %Y', strtotime($data))));
            }
        }
        $select_forma_pagamento = array(
            'boleto_online' => 'Boleto Online - sem custos adicionais',
            'boleto_impresso' => 'Boleto Entregue Em Casa - custo adicional de 20 reais',
            'cheque' => 'Cheque');
        $this->set('select_forma_pagamento', $select_forma_pagamento);
        $this->set('select_data_inicio_pagamento', $datas);
        $this->set('select_parcelas', null);
        $this->set('select_convites_extras', $datas);
        $this->set('select_mesas_extras', $datas);
    }

    private function gerarDespesasPlanoEconomico($parcelas, $dataInicioPagamento, $diaDeVencimentoParcelas, $impresso) {
        $dtInicioPagamento = $this->create_date_time_from_format('Y-m-d', $dataInicioPagamento);
        $despesasArray = array();
        foreach ($parcelas as $i => $parcela) {
            $intDiaVencimento = intval($diaDeVencimentoParcelas);
            $intDiaHoje = intval(date('d', strtotime('now')));
            $intMesHoje = intval(date('m', strtotime('now')));
            $intMesInicioPagamento = intval(date_format($dtInicioPagamento, 'm'));
            $intAnoInicioPagamento = intval(date_format($dtInicioPagamento, 'Y'));
            $intAnoHoje = intval(date('Y', strtotime('now')));
            $mes = (date_format($dtInicioPagamento, 'm') + $i) % 12 == 0 ? 12 :
                    (date_format($dtInicioPagamento, 'm') + $i) % 12;
            $ano = date('Y', mktime(0, 0, 0, date_format($dtInicioPagamento, 'm') + $i, $intDiaVencimento, date_format($dtInicioPagamento, 'Y')));
            $diasDoMes = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);
            $diaDeVencimento = $intDiaVencimento;
            if (($i == 0) && ($intDiaVencimento < $intDiaHoje) && ($intMesInicioPagamento == $intMesHoje) && ($intAnoInicioPagamento == $intAnoHoje)) {
                $this->Session->write('mensagem_despesa', 'Aten&ccedil;&atilde;o: como seu dia de vencimento &eacute; menor do que o dia atual foi gerada uma despesa para ser paga at&eacute; amanh&atilde;.');
                $diaDeVencimento = date('d', strtotime('now')) + 1;
            } elseif ($diaDeVencimento > $diasDoMes) {
                $diaDeVencimento = $diasDoMes;
            }
            $despesasArray[] = array(
                'valor' => $parcela['valor'] + ($impresso ? (float) 20.00 : 0),
                'data_cadastro' => date('Y-m-d H:i:s', strtotime('now')),
                'tipo' => 'adesao',
                'parcela' => $i + 1,
                'total_parcelas' => count($parcelas),
                'data_vencimento' => date('Y-m-d', mktime(
                                0, 0, 0, date_format($dtInicioPagamento, 'm') + $i, $diaDeVencimento, date_format($dtInicioPagamento, 'Y'))
                )
            );
            $impresso = FALSE;
        }
        return $despesasArray;
    }

    private function geraDespesas($dataInicioPagamento, $diaDeVencimentoParcelas, $numeroParcelas, $valorTotal, $impresso) {
        //Cria um array com as informações da data de início de parcelamento
        $dtInicioPagamento = $this->create_date_time_from_format('Y-m-d', $dataInicioPagamento);
        $valor_parcela = $valorTotal / $numeroParcelas;
        $despesasArray = array();

        //Com os dados do parcelamento, preencher as linhas necessárias na tabela Despesas
        for ($i = 0; $i < $numeroParcelas; $i++) {
            $intDiaVencimento = intval($diaDeVencimentoParcelas);
            $intDiaHoje = intval(date('d', strtotime('now')));
            $intMesHoje = intval(date('m', strtotime('now')));
            $intMesInicioPagamento = intval(date_format($dtInicioPagamento, 'm'));
            $intAnoInicioPagamento = intval(date_format($dtInicioPagamento, 'Y'));
            $intAnoHoje = intval(date('Y', strtotime('now')));
            $mes = (date_format($dtInicioPagamento, 'm') + $i) % 12 == 0 ? 12 : (date_format($dtInicioPagamento, 'm') + $i) % 12;
            $ano = date('Y', mktime(0, 0, 0, date_format($dtInicioPagamento, 'm') + $i, $intDiaVencimento, date_format($dtInicioPagamento, 'Y')));
            $diasDoMes = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);
            $diaDeVencimento = $intDiaVencimento;
            if (($i == 0) && ($intDiaVencimento < $intDiaHoje) && ($intMesInicioPagamento == $intMesHoje) && ($intAnoInicioPagamento == $intAnoHoje)) {
                $this->Session->write('mensagem_despesa', 'Aten&ccedil;&atilde;o: como seu dia de vencimento &eacute; menor do que o dia atual foi gerada uma despesa para ser paga at&eacute; amanh&atilde;.');
                $diaDeVencimento = date('d', strtotime('now')) + 1;
            } elseif ($diaDeVencimento > $diasDoMes) {
                $diaDeVencimento = $diasDoMes;
            }
            $despesasArray[] = array(
                'valor' => $valor_parcela + ($impresso ? (float) 20.00 : 0),
                'data_cadastro' => date('Y-m-d H:i:s', strtotime('now')),
                'tipo' => 'adesao',
                'parcela' => $i + 1,
                'multa' => 0.00,
                'total_parcelas' => $numeroParcelas,
                'data_vencimento' => date('Y-m-d', mktime(
                                0, 0, 0, date_format($dtInicioPagamento, 'm') + $i, $diaDeVencimento, date_format($dtInicioPagamento, 'Y'))
                )
            );
            $impresso = FALSE;
        }
        return $despesasArray;
    }

    private function enviarEmailNovoFormando($data) {
        $this->Email->smtpOptions = array(
            'port' => '465',
            'timeout' => '30',
            'host' => 'ssl://smtp.gmail.com',
            'username' => 'emailprojetoas',
            'password' => 'enviando123emails'
        );

        $this->Email->from = 'RK Formaturas - Sistema Online <emailprojetoas@gmail.com>';
        $this->Email->to = $data['Usuario']['nome'] . ' - ' . '<' . $data['Usuario']['email'] . '>';
        $this->Email->subject = 'Cadastro de Formando';
        $usuario = str_pad("Usuário", 10, ' ', STR_PAD_RIGHT);
        $senha = str_pad("Senha", 10, ' ', STR_PAD_RIGHT);
        $mensagem = "Seja bem vindo formando {$data['Usuario']['nome']}!!!\n";
        $mensagem.= "Você acaba de aderir ao novo Sistema Online da Ás Formaturas.\n\n";
        $mensagem.= "No sistema você poderá entrar em contato com outros formandos e membros da comissão, ";
        $mensagem.= "tirar suas dúvidas, acompanhar o processo de planejamento e produção, ";
        $mensagem.= "comprar convites extras, etc.\n\n";
        $mensagem.= "Acesse o sistema no endereço " . Router::url('/', true) . " e comece a fazer parte da experiência RK Formaturas\n\n";
        $mensagem.= "Os dados de acesso ao sistema são:\n$usuario {$data['Usuario']['email']}\n$senha {$data['Usuario']['confirmar']}\n\n";
        $mensagem.= "Obs.: Caso o cadastro tenha sido entregue a um dos nossos representantes, a senha padrão utilizada é somente seu primeiro nome com o código da turmas.";
        $this->Email->delivery = 'smtp';
        $emailEnviado = $this->Email->send($mensagem);
    }

    function planos($idturma) {
        Configure::write('debug', 0);
        $this->layout = 'ajax';
        $this->autoRender = false;
        $this->Parcelamento->recursive = -1;
        $planos = $this->Parcelamento->find('all', array('conditions' => array(
                'turma_id' => $idturma), 'group' => 'titulo'));
        echo json_encode(array('quantidade' => count($planos), 'planos' => $planos));
    }

    function inicio_pagamento($idTurma, $titulo = false) {
        Configure::write('debug', 0);
        $this->layout = 'ajax';
        $this->autoRender = false;
        $this->Parcelamento->recursive = -1;
        $options = array(
            'group' => "date_format(Parcelamento.data,'%Y%m')",
            'conditions' => array('turma_id' => $idTurma));
        if ($titulo) {
            if ($titulo == "null")
                $options['conditions'][] = "Parcelamento.titulo is null";
            else
                $options['conditions']["Parcelamento.titulo"] = $titulo;
        }
        $planos = $this->Parcelamento->find('all', $options);
        foreach ($planos as $indice => $plano) {
            $data = explode("-", $plano['Parcelamento']['data']);
            $planos[$indice]['Parcelamento']['data_inicio'] = "{$data[1]}/{$data[0]}";
        }
        echo json_encode(array('planos' => $planos));
    }

    function forma_pagamento($idTurma, $data, $titulo = false) {
        Configure::write('debug', 0);
        $this->layout = 'ajax';
        $this->autoRender = false;
        $this->Parcelamento->recursive = -1;
        $options = array('conditions' => array('turma_id' => $idTurma, 'data' => $data));
        if ($titulo) {
            if ($titulo == "null")
                $options['conditions'][] = "Parcelamento.titulo is null";
            else
                $options['conditions']["Parcelamento.titulo"] = $titulo;
        }
        $parcelas = $this->Parcelamento->find('all', $options);
        foreach ($parcelas as $indice => $parcela) {
            $valor_parcela = $parcela['Parcelamento']['valor'] / $parcela['Parcelamento']['parcelas'];
            $parcelas[$indice]['Parcelamento']['forma'] = "{$parcela['Parcelamento']['parcelas']} x R$" .
                    number_format($valor_parcela, 2, ',', '.') . ' (R$' .
                    number_format($parcela['Parcelamento']['valor'], 2, ',', '.') . ')';
        }
        echo json_encode(array('parcelas' => $parcelas));
    }

    function parcelas($idturma, $data, $titulo = false) {
        $this->layout = 'ajax';
        $this->set('parcelas', $this->loadparcelas($idturma, $data, $titulo));
    }

    function parcelamentos() {
        $this->autoRender = false;
        Configure::write('debug', 0);
        $json = array();
        $this->Parcelamento->recursive = -1;
        if(isset($this->data['turma']) && isset($this->data['data'])) {
            $options['conditions'] = array(
                'turma_id' => $this->data['turma'],
                'data' => $this->data['data']
            );
            if(isset($this->data['plano']))
                $options['conditions']['titulo'] = $this->data['plano'];
            else
                $options['conditions'][] = 'titulo is null';
            $parcelas = $this->Parcelamento->find('all', $options);
            $parcelas2 = array();
            foreach ($parcelas as $parcela) {
                $valor_parcela = $parcela['Parcelamento']['valor'] / $parcela['Parcelamento']['parcelas'];
                $parcela['Parcelamento']['texto'] = $parcela['Parcelamento']['parcelas'] . ' x R$' .
                        number_format($valor_parcela, 2, ',', '.') . ' (R$' .
                        number_format($parcela['Parcelamento']['valor'], 2, ',', '.') .
                        ')';
                $parcelas2[] = $parcela;
            }
            $json['parcelas'] = $parcelas2;
        }
        echo json_encode($json);
    }

    function loadparcelas($idturma, $data, $titulo = false) {
        $this->Parcelamento->recursive = -1;
        $parcelamentos = $this->Parcelamento->find('all', array('conditions' => array('turma_id' => $idturma), 'group' => 'titulo'));
        if (count($parcelamentos) > 1 && !$titulo) {
            $titulos = array();
            foreach ($parcelamentos as $indice => $parcelamento) {
                if ($parcelamento['Parcelamento']['titulo'] == "")
                    unset($parcelamentos[$indice]);
                else
                    $titulos[] = $parcelamento['Parcelamento']['titulo'];
            }
        }
        if (count($parcelamentos) > 1 && !$titulo) {
            return(array('parcelamentos' => count($parcelamentos), 'titulos' => $titulos));
        } else {
            if ($titulo)
                $parcelas = $this->Parcelamento->find('all', array('conditions' => array('turma_id' => $idturma, 'data' => $data, 'titulo' => $titulo)));
            else
                $parcelas = $this->Parcelamento->find('all', array('conditions' => array('turma_id' => $idturma, 'data' => $data)));
            $parcelas2 = array();
            foreach ($parcelas as $parcela) {
                $valor_parcela = $parcela['Parcelamento']['valor'] / $parcela['Parcelamento']['parcelas'];
                $parcela['Parcelamento']['texto'] = $parcela['Parcelamento']['parcelas'] . ' x R$' . number_format($valor_parcela, 2, ',', '.') . ' (R$' . number_format($parcela['Parcelamento']['valor'], 2, ',', '.') . ')';
                $parcelas2[] = $parcela;
            }
            return(array('parcelamentos' => 1, 'parcelas' => $parcelas2));
        }
    }

    private function formata_nome($nome) {
        $nome = mb_strtolower($nome, "UTF-8");
        $nome = preg_replace("/[,\.\-\/()]/", "", $nome);
        $nome = trim(mb_convert_case($nome, MB_CASE_TITLE, "UTF-8"));

//		$nome = strtolower($nome);
//		$nome = preg_replace("/[,\.\-\/()]/","",$nome);
//		$nome = trim(ucwords($nome));
        return $nome;
    }

    function obrigado() {
        if ($this->Session->check('mensagem_despesa')) {
            $mensagemDeAviso = $this->Session->read('mensagem_despesa');
            $this->Session->delete('mensagem_despesa');
            $this->set('mensagem_despesa', $mensagemDeAviso);
        }
        $usuarioCadastrado = $this->Session->read('usuarioCadastrado');
        $this->Session->delete('usuarioCadastrado');
        $this->set('usuarioCadastrado', $usuarioCadastrado);
        $fotoPerfil = "img/uknown_user.gif";
        if (isset($usuarioCadastrado['Usuario']['diretorio_foto_perfil']))
            if (!empty($usuarioCadastrado['Usuario']['diretorio_foto_perfil']))
                if (file_exists(APP . "webroot/{$usuarioCadastrado['Usuario']['diretorio_foto_perfil']}"))
                    $fotoPerfil = $usuarioCadastrado['Usuario']['diretorio_foto_perfil'];
        $this->set('fotoPerfil', $fotoPerfil);
        $codigo = $this->Session->read('codigo_formando_cadastro');
        $this->set('codigo_formando_cadastro', $codigo);
        $this->Session->delete('codigo_formando_cadastro');
        $this->layout = 'metro/externo';
    }

    function calcularCredito($usuario_id) {
        $pagamentos = $this->Pagamento->find('all', array('conditions' => array('usuario_id' => $usuario_id, 'status' => 'pago'), 'fields' => array('sum(Pagamento.valor_nominal) AS total')));
        $despesas = $this->Despesa->find('all', array('conditions' => array('usuario_id' => $usuario_id, 'status' => array('aberta', 'paga')), 'fields' => array('sum(Despesa.valor + Despesa.correcao_igpm) AS total')));

        $total_pago = $pagamentos[0][0]['total'];
        $total_devido = $despesas[0][0]['total'];

        $credito = $total_pago - $total_devido;

        return $credito;
    }

    function super_listar_formandos() {
        $formandos = $this->ViewFormandos->find("all");
        $this->set("formandos", $formandos);
    }

    function atendimento_index() {
        $usuario = $this->Session->read('Usuario');
        $joins = array(
            array(
                "table" => "turmas_usuarios",
                "type" => "inner",
                "alias" => "TurmaUsuario",
                "conditions" => array("ViewFormandos.turma_id = TurmaUsuario.turma_id")
            ),
            array(
                "table" => "turmas",
                "type" => "inner",
                "alias" => "Turma",
                "conditions" => array("ViewFormandos.turma_id = Turma.id")
            ),
            array(
                "table" => "protocolos",
                "type" => "left",
                "alias" => "Protocolo",
                "conditions" => array("ViewFormandos.id = Protocolo.usuario_id", "Protocolo.tipo = 'checkout'")
            ),
        );
        $conditions = array("TurmaUsuario.usuario_id = {$usuario['Usuario']['id']}");
        $turmaLogada = $this->obterTurmaLogada();
        if (!empty($turmaLogada))
            $conditions[] = array("Turma.id = {$turmaLogada['Turma']['id']}");
        if ($this->data) {
            $this->Session->delete('AtendimentoIndex');
            $valor = $this->data['filtro-id'];
            $tipo = $this->data['filtro-tipo'];
            $limit = $this->data['qtde-por-pagina'];
            if ($valor != "")
                $conditions[] = "ViewFormandos.$tipo like('%$valor%')";
            if ($valor != "")
                $this->Session->write("AtendimentoIndex.conditions", "ViewFormandos.$tipo like('%$valor%')");
            $this->Session->write("AtendimentoIndex.valor", $this->data['filtro-id']);
            $this->Session->write("AtendimentoIndex.tipo", $this->data['filtro-tipo']);
            $this->Session->write("AtendimentoIndex.limit", $this->data['qtde-por-pagina']);
        } else {
            $valor = $this->Session->read("AtendimentoIndex.valor");
            $tipo = $this->Session->read("AtendimentoIndex.tipo");
            if ($this->Session->read("AtendimentoIndex.conditions") != "")
                $conditions[] = $this->Session->read("AtendimentoIndex.conditions");
            $limit = $this->Session->read("AtendimentoIndex.limit") == "" ? "100" : $this->Session->read("AtendimentoIndex.limit");
        }
        $options = array(
            "joins" => $joins,
            "limit" => $limit,
            'fields' => array('Turma.*', 'ViewFormandos.*,Protocolo.*')
        );
        if (isset($conditions))
            $options['conditions'] = $conditions;
        $this->paginate['ViewFormandos'] = $options;
        $formandos = $this->paginate('ViewFormandos');
        $this->set('formandos', $formandos);
        $arrayOptions = array(
            "codigo_formando" => "Código do Formando",
            "turma_id" => "Turma",
            "nome" => "Nome",
            "email" => "Email"
        );
        $this->set("turmaLogada", ($this->obterTurmaLogada() ? true : false));
        $this->set("arrayOptions", $arrayOptions);
        $this->set("valor", $valor);
        $this->set("limit", $limit);
        $this->set("tipo", $tipo);
    }
    
    function atendimento_boletos() {
        $usuario = $this->obterUsuarioLogado();
        if (!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->Session->write("filtros.{$usuario['Usuario']['grupo']}.usuarios", $this->data);
        } else {
            $options = array(
                'conditions' => array(
                    'ativo' => 1,
                    'forma_pagamento' => 'boleto_impresso',
                    'grupo' => array('formando','comissao'),
                    'situacao' => 'ativo'
                ),
                'limit' => 100
            );
            $options['order'] = array('ViewFormandos.codigo_formando' => 'desc');
            $usuarioId = array($usuario['Usuario']['id']);
            if($this->Session->check("filtros.{$usuario['Usuario']['grupo']}.usuarios")) {
                $config = $this->Session->read("filtros.{$usuario['Usuario']['grupo']}.usuarios");
                if(isset($config['Usuario']['id']))
                    $usuarioId[] = $config['Usuario']['id'];
                foreach($config as $modelo => $filtro)
                    foreach($filtro as $chave => $valor)
                        if($valor != '')
                            if($chave == 'id')
                                $options['conditions']["$modelo.$chave"] = $valor;
                            else
                                $options['conditions'][] = "lower($modelo.$chave) like('%".strtolower($valor)."%')";
                $this->data = $config;
            }
            $options['limit'] = 50;
            $this->paginate['ViewFormandos'] = $options;
            $formandos = $this->paginate('ViewFormandos');
            $this->set('formandos',$formandos);
            $this->set('boletos', array('' => 'Todos', '1' => 'Impresso', '0' => 'Não Impresso'));
        }
    }

    function financeiro_lista_formandos_boletos_impressao() {
        $options = array(
            'conditions' => array('FormandoProfile.forma_pagamento' => 'boleto_impresso'),
            'limit' => 200,
            'order' => 'FormandoProfile.impresso desc, FormandoProfile.enviado desc'
        );
        if ($this->data) {
            if ($this->data['field'] == "impresso_out") {
                $options['conditions'][] = "FormandoProfile.impresso = 0";
            } elseif ($this->data['field'] == "impresso_in") {
                $options['conditions'][] = "FormandoProfile.impresso = 1";
            } elseif ($this->data['field'] == "enviado_out") {
                $options['conditions'][] = "FormandoProfile.enviado = 0";
            } elseif ($this->data['field'] == "enviado_in") {
                $options['conditions'][] = "FormandoProfile.enviado = 1";
            } elseif ($this->data['field'] == "todos") {
                $options['conditions'][] = "FormandoProfile.enviado  = FormandoProfile.enviado";
            } else {
                $options['conditions'][] = "upper(Usuario.{$this->data['field']}) like('%" .
                        strtoupper($this->data['value']) . "%')";
            }
        }
        $this->paginate = $options;
        $this->Usuario->bindModel(array('hasOne' => array('TurmasUsuario')), false);
        $this->Usuario->unBindModel(array('hasMany' => array('Despesa')), false);
        $this->Usuario->recursive = 0;
        $this->Usuario->bindModel(array('hasAndBelongsToMany' => array('Turma')));
        $formandos = $this->paginate('Usuario');
        $this->set('formandos', $formandos);
        $optionsFiltro = array(
            'impresso_out' => 'Não Impressos',
            'impresso_in' => 'Impressos',
            'enviado_out' => 'Não Enviados',
            'enviado_in' => 'Enviados',
            'nome' => 'Nome',
            'todos' => 'Todos'
        );
        $this->set('optionsFiltro', $optionsFiltro);
    }

}

?>
