<?php
class CronogramasController extends AppController {

	var $name = 'Cronogramas';
	
	var $uses = array('Cronograma','Turma', 'Item', 'Assunto');
	
	
	function planejamento_visualizar($id) {
		$this->Cronograma->id = $id;
		
		$cronograma = $this->Cronograma->read();

		if (!$cronograma) {
			$this->Session->setFlash('Item de cronograma não existente' , 'flash_erro');
			$this->redirect("/{$this->params['prefix']}/cronogramas");
		}
		
		//calcular pendencias
		$pendencias_as = 0;
		$pendencias_comissao = 0;
		//Buscar assuntos relativos ao item e a turma
		$turma = $this->Session->read('turma');
		
		$pendencias = $this->Assunto->find('list', array(
			'conditions' => array(
				'Assunto.turma_id' => $turma['Turma']['id'],
				'Assunto.item_id' => $cronograma['Item']['id']),
			'fields' => array('pendencia')));
		if(!empty($pendencias))  {
			foreach($pendencias as $pendencia):
				if(strcmp($pendencia,'As') == 0 ){
					$pendencias_as++;
				} elseif(strcmp($pendencia, 'Comissao') == 0 ) {
					$pendencias_comissao++;
				}
			endforeach;
		}
		$pendencias['as'] = $pendencias_as;
		$pendencias['comissao'] = $pendencias_comissao;
		
		
		$dateTime =  $this->create_date_time_from_format('Y-m-d', $cronograma['Cronograma']['data_limite']);
        $cronograma['Cronograma']['data_limite'] = date_format($dateTime, 'd-m-Y');

		$this->set('pendencias',$pendencias);
		$this->set('cronograma', $cronograma);
	}
	
	function planejamento_index() {
		$turma = $this->Session->read('turma');
		
		
		$this->paginate = array(
			'limit' => 20,
			'conditions' => array(
				'Cronograma.turma_id' => $turma['Turma']['id']
			),
			'order' => array(
				'Cronograma.item_id' => 'asc'
			)
		);
		
		$cronogramas = $this->paginate('Cronograma');
		$itens = array();
		
		// Calcular somatoria de pendencias de cada item	
		foreach($cronogramas as $item_cronograma):
			$pendencias_as = 0;
			$pendencias_comissao = 0;
			//Buscar assuntos relativos ao item e a turma
			$pendencias = $this->Assunto->find('list', array(
				'conditions' => array(
					'Assunto.turma_id' => $turma['Turma']['id'],
					'Assunto.item_id' => $item_cronograma['Item']['id']),
				'fields' => array('pendencia')));
			if(!empty($pendencias))  {
				foreach($pendencias as $pendencia):
					if(strcmp($pendencia,'As') == 0 ){
						$pendencias_as++;
					} elseif(strcmp($pendencia, 'Comissao') == 0 ) {
						$pendencias_comissao++;
					}
				endforeach;
			}
	
			$itens[$item_cronograma['Item']['id']]['pendencias_as'] = $pendencias_as;
			$itens[$item_cronograma['Item']['id']]['pendencias_comissao'] = $pendencias_comissao;

		endforeach;
		$this->set('itens', $itens);
		$this->set('cronogramas', $cronogramas);
	}
	
	function planejamento_deletar($id) {
		if($this->Cronograma->delete($id)) {
			$this->Session->setFlash('Item removido do cronograma.', 'flash_sucesso');
		} else {
			$this->Session->setFlash('Erro ao remover o item do cronograma.' , 'flash_erro');
		}

		$this->redirect("/{$this->params['prefix']}/cronogramas");
		
		
	}
	
	function planejamento_adicionar() {
		$turma = $this->Session->read('turma');
		if(empty($turma)) {
			// Uma turma deve estar selecionada, redirecionar para listagem de turmas
			$this->Session->setFlash(__('Turma deve estar selecionada', true), 'flash_erro');
			$this->redirect("/{$this->params['prefix']}/turmas");
		}
		
		$itens_existentes = $this->Cronograma->find('list', array(
			'conditions' => array( 'Cronograma.turma_id' => $turma['Turma']['id']),
			'fields' => array('item_id')
			));
		
		$itens = $this->Item->find('list', array(
			'conditions' => array( 'Item.grupo' => 'planejamento', 'NOT' => array('Item.id' => $itens_existentes)),
			'fields' => array('nome')
			));

		if(!empty($this->data)) {
			
			//TODO: modificar para session
			$this->data['Cronograma']['turma_id'] = $turma['Turma']['id'];
			if ($this->data['Cronograma']['data_limite_aux'] != "") {
				$dateTime =  $this->create_date_time_from_format('d-m-Y', $this->data['Cronograma']['data_limite_aux']);
	            if ($dateTime != null)
					$this->data['Cronograma']['data_limite'] = date_format($dateTime, 'Y-m-d');
	            unset($this->data['Cronograma']['data_limite_aux']);
			
			
				$this->Cronograma->create();
			
				if ($this->Cronograma->save($this->data)) {
					$this->Session->setFlash(__('O item de cronograma foi salvo com sucesso', true), 'flash_sucesso');
					$this->redirect("/{$this->params['prefix']}/cronogramas/");
				} else {
					$this->Session->setFlash(__('Ocorreu um erro ao salvar o item de cronograma.', true), 'flash_erro');
				}
			} else {
				$this->Session->setFlash(__('Ocorreu um erro ao salvar o item de cronograma.Preencha a data limite.', true), 'flash_erro');
			}
		}
		if(empty($itens)) {
			$this->Session->setFlash(__('Não há mais itens a serem adicionados.', true), 'flash_erro');
			$this->redirect("/{$this->params['prefix']}/cronogramas/");
		}
		
		
		
		$this->set('itens', $itens);
		$this->set('statuses',$this->Cronograma->statuses);
	}
	
	function planejamento_editar($id) {
		$this->Cronograma->id = $id;
		
		$cronograma = $this->Cronograma->read();

		if (!$cronograma) {
			$this->Session->setFlash('Item de cronograma não existente' , 'flash_erro');
			$this->redirect("/{$this->params['prefix']}/cronogramas");
		}
		
		if(!empty($this->data)) {
			if ($this->data['Cronograma']['data_limite_aux'] != "") {
				$dateTime =  $this->create_date_time_from_format('d-m-Y', $this->data['Cronograma']['data_limite_aux']);
	            $this->data['Cronograma']['data_limite'] = date_format($dateTime, 'Y-m-d');
	            unset($this->data['Cronograma']['data_limite_aux']);
			
				if ($this->Cronograma->save($this->data)) {
					$this->Session->setFlash(__('O item de cronograma foi salvo com sucesso', true), 'flash_sucesso');
					$this->redirect("/{$this->params['prefix']}/cronogramas/");
				} else {
					$this->Session->setFlash(__('Ocorreu um erro ao salvar o item de cronograma.', true), 'flash_erro');
				}
			} else {
				$this->Session->setFlash(__('Ocorreu um erro ao salvar o item de cronograma.Preencha a data limite.', true), 'flash_erro');
			}
		}
		
		$this->data = $cronograma;
		$dateTime =  $this->create_date_time_from_format('Y-m-d', $this->data['Cronograma']['data_limite']);
        $this->data['Cronograma']['data_limite_aux'] = date_format($dateTime, 'd-m-Y');
		$this->set('statuses',$this->Cronograma->statuses);
	}
	
	// Método para verificar a qual turma o usuário pertence, caso seja comissão, e colocar a turma na session
	private function configurarTurmaComissao() {
		if(!$this->Session->check('turma')) {
			$usuario = $this->Session->read('Usuario');
		
			$this->Turma->bindModel(array('hasOne' => array('TurmasUsuario')), false);
	        $turma =  $this->Turma->find('first', array('conditions' => array('TurmasUsuario.usuario_id' => $usuario['Usuario']['id'])));
			
			$this->Session->write('turma',$turma);
		}
		
	}
	
	function comissao_index() {
		
		$this->configurarTurmaComissao();
		$this->planejamento_index();
	}
	
	function comissao_visualizar($id) {
		$this->planejamento_visualizar($id);
	}
}
?>