<?php

class NotasController extends AppController {

    var $name = 'Notas';
    var $uses = array('UsuarioNota');
    var $nomeDoTemplateSidebar = 'notas';
    
    function carregar() {
        $this->autoLayout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $usuario = $this->Auth->user();
        $this->UsuarioNota->unbindModelAll();
        $notas = $this->UsuarioNota->find('all',array(
            'conditions' => array('usuario_id' => $usuario['Usuario']['id']),
            'order' => array('data_atualizacao' => desc)
        ));
        echo json_encode(array('notas' => $notas));
    }
    
    function inserir() {
        $this->autoLayout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $usuario = $this->Session->read('Usuario');
        $json = array('erro' => 1);
        if($usuario && isset($this->data['UsuarioNota'])) {
            $this->data['UsuarioNota']['usuario_id'] = $usuario['Usuario']['id'];
            if($this->UsuarioNota->save($this->data)) {
                if(isset($this->data['UsuarioNota']['id']))
                    $id = $this->data['UsuarioNota']['id'];
                else
                    $id = $this->UsuarioNota->getLastInsertId();
                $this->UsuarioNota->unbindModelAll();
                $json = array(
                    'erro' => 0,
                    'nota' => $this->UsuarioNota->read(null,$id)
                );
            }
        }
        echo json_encode($json);
    }
    
    function remover() {
        $this->autoLayout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('erro' => 1);
        if(isset($this->data['id']))
            if($this->UsuarioNota->delete($this->data['id']))
                $json['erro'] = 0;
        echo json_encode($json);
    }
    
}