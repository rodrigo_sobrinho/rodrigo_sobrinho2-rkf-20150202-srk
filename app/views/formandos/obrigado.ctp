<div class="row-fluid">
    <?php $session->flash(); ?>
    <?php if (isset($mensagem_despesa)) { ?>
    <div style="padding:14px" class="bg-color-red">
        <h4 class="fg-color-white"><?=$mensagem_despesa;?></h4>
    </div>
    <br />
    <?php } ?>
</div>
<div class="row-fluid">
    <div class="span4">
        <img src="<?="{$this->webroot}{$fotoPerfil}" ?>" class="img-polaroid" width="100%" />
    </div>
    <div class="span7 offset1 bg-color-blueDark" style="padding:14px">
        <h3 class="fg-color-white">
            Obrigado <?=$usuarioCadastrado['Usuario']['nome']; ?>, pelo seu cadastro.
            <br />
            Seu Código de formando é <b><?=$codigo_formando_cadastro;?></b>
            <br />
            Para acessar sua página no sistema <?=$html->link('Clique Aqui', "/{$usuarioCadastrado['Usuario']['grupo']}/",array('class' => 'fg-color-white')); ?>
        </h3>
    </div>
</div>
