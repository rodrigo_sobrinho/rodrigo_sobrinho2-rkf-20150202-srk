<?php

class CTAMapaDeMesasAgendamento {

    /**
     * Agendamento do evento
     * @var MapaDeMesasAgendamento
     */
    private $agendamento;

    public function __construct($agendamento = null) {
        $this->FormandoHorario = ClassRegistry::init(
            'FormandoHorario');

        $this->agendamento = $agendamento;
    }

    public function load() {
    }

    public function save($formandos = null) {

        $formandos = ($formandos) : $formandos ? $this->formandos;
        if(!$formandos)
            return false;

        foreach ($formandos as $key => $formando) {
            // Registrar Agendamento
            $this->FormandoHorario->create();
            $this->FormandoHorario->set('turma_mapa_id', $mapa->getTurmaMapaId());
            $this->FormandoHorario->set('data_inicio', $agendamento->getDatainicio());
            $this->FormandoHorario->set('data_finalizacao',$agendamento->getAgendamento());
            $this->FormandoHorario->set('hora_inicio', $agendamento->getHoraInicio());
            $this->FormandoHorario->set('hora_finalizacao', $agendamento->getHoraFinalizacao());
            $this->FormandoHorario->set('data_agendamento', $agendamento->getDataAgendamento());
            $this->FormandoHorario->save();
        }
    }
    
}