<?php

class ContratosController extends AppController {

    var $name = 'Contratos';
    var $uses = array('Usuario', 'Turma', 'Faculdade', 'Curso',
        'CursoTurma', 'FormandoProfile', 'Contrato','Arquivo','ViewFormandos', 'TiposContrato', 'TiposContratoAnexos');
    var $components = array('Pdf');

    // A área de Planejamento faz upload de contratos assinados e scaneados
    // A área Comercial gera os contratos em formato PDF

    function planejamento_index() {
        $turma = $this->Session->read('turma');

        $contratos_db = $this->Contrato->find('all', array(
            'conditions' => array(
                'turma_id' => $turma['Turma']['id']
            )
        ));

        $contratos = array();
        foreach ($contratos_db as $contrato):
            if (!isset($contratos[$contrato['Contrato']['tipo']]))
                $contratos[$contrato['Contrato']['tipo']] = array();
            array_push($contratos[$contrato['Contrato']['tipo']], $contrato);
        endforeach;
        
        $this->set('contratos', $contratos);
        $this->set('tipos_contratos', $this->Contrato->tipos_contratos);
    }
    
    function comercial_listar() {
        $this->layout = false;
        $contratos = array();
        $turma = $this->obterTurmaLogada();
        $tipoContrato = $this->TiposContrato->find('first', array(
            'conditions' => array(
                'TiposContrato.id' => $turma['Turma']['tipos_contrato_id']
            )
        ));
        if(!empty($tipoContrato)){
            foreach($tipoContrato['TiposContratoAnexos'] as $tipo => $nome) {
                $contrato = $this->Contrato->find('first',array(
                    'conditions' => array(
                        'Contrato.turma_id' => $turma['Turma']['id'],
                        "Contrato.deletado is null",
                        "Contrato.tipos_contrato_anexos_id" => $nome['id'],
                    )
                ));
                if($contrato || file_exists(VIEWS."contratos/templates/{$tipoContrato['TiposContrato']['nome']}/{$nome['nome_anexo']}.ctp"))
                    $contratos[] = array(
                        'tipo' => $nome['nome_anexo'],
                        'nome' => $nome['nome'],
                        'contrato' => $contrato
                    );
            }
            $this->set('nomeContrato', $this->TiposContrato->tipos_contrato[$turma['Turma']['tipos_contrato_id']]);
            $this->set('contratos',$contratos);
        }else{
            $this->set('contratos', NULL);
            $this->Session->setFlash('Por favor, selecione o tipo de contrato na edição de turma.',
                            'metro/flash/alert');
        }
        
    }
    
    private function _listar() {
        $turma = $this->obterTurmaLogada();
        $this->layout = false;
        
        $contratos_db = $this->Contrato->find('all', array(
            'conditions' => array(
                'turma_id' => $turma['Turma']['id']
            )
        ));
        
        $contratos = array();
        foreach ($contratos_db as $contrato):
            if (!isset($contratos[$contrato['Contrato']['tipo']]))
                $contratos[$contrato['Contrato']['tipo']] = array();
            array_push($contratos[$contrato['Contrato']['tipo']], $contrato);
        endforeach;
        $this->set('tipos_contratos', $this->TiposContratoAnexos->find('all', array('conditions' => array(
            'tipos_contrato_id' => $turma['Turma']['tipos_contrato_id']))));
        $this->set('contratos', $contratos);
        $this->set('nomeContrato', $this->TiposContrato->tipos_contrato[$turma['Turma']['tipos_contrato_id']]);
    }
    
    function planejamento_listar() {
        $this->_listar();
    }
    
    function comissao_listar() {
        $this->_listar();
    }

    function comissao_index() {
        $this->planejamento_index();
    }

    function planejamento_adicionar() {
        date_default_timezone_set('America/Sao_Paulo');
        if (!empty($this->data) &&
            is_uploaded_file($this->data['Contrato']['arquivo']['tmp_name'])) {
            $contrato = $this->Contrato->create();

            $session_turma = $this->Session->read('turma');
            $usuario = $this->Session->read('Usuario');

            $contrato['Contrato']['nome'] = $this->data['Contrato']['arquivo']['name'];
            $contrato['Contrato']['tipo'] = $this->data['Contrato']['arquivo']['type'];
            $contrato['Contrato']['tamanho'] = $this->data['Contrato']['arquivo']['size'];
            $contrato['Contrato']['tmp_name'] = $this->data['Contrato']['arquivo']['tmp_name'];
            $contrato['Contrato']['turma_id'] = $session_turma['Turma']['id'];
            $contrato['Contrato']['usuario_id'] = $usuario['Usuario']['id'];
            $contrato['Contrato']['tipo'] = $this->data['Contrato']['tipo'];
            $contrato['Contrato']['criado'] = date('Y-m-d H:i:s');

            if ($this->Contrato->save($contrato['Contrato'])) {
                $this->redirect("/{$this->params['prefix']}/Contratos/visualizar/{$this->Contrato->id}");
            }
        }

        $this->set('tipos_contratos', $this->Contrato->tipos_contratos);
    }

    private function _inserir(){
        $this->layout = false;
        if(!empty($this->data)){
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->loadModel('Arquivo');
            $data = $_POST['data']['Contrato'];
            $turma = $this->obterTurmaLogada();
            $tipoContrato = $this->TiposContrato->findById($turma['Turma']['tipos_contrato_id']);
            foreach($tipoContrato['TiposContratoAnexos'] as $nome) 
                if($nome['nome_anexo'] == $data['tipo'])
                    $nomeAnexo = $nome;
            $usuario = $this->Session->read('Usuario');
            $data['nome_secreto'] = $this->Arquivo->generateUniqueId();
            $data['diretorio'] = $this->Arquivo->file_path();
            $data['path'] = $data['diretorio'].
                $data['nome_secreto'];
            if($this->Arquivo->saveBase64File($data['src'],$data['path'])) {
                $contrato = $this->Contrato->create();
                $contrato['Contrato'] = $data;
                unset($contrato['Contrato']['src']);
                $contrato['Contrato']['turma_id'] = $turma['Turma']['id'];
                $contrato['Contrato']['usuario_id'] = $usuario['Usuario']['id'];
                $contrato['Contrato']['criado'] = date('Y-m-d H:i:s');
                $contrato['Contrato']['tipos_contrato_anexos_id'] = $nomeAnexo['id'];
                if ($this->Contrato->save(array('Contrato' => $contrato['Contrato']),
                        array('callback' => false)))
                    $this->Session->setFlash('Contrato salvo com sucesso.',
                            'metro/flash/success');
                else{
                    $this->Session->setFlash('Erro ao salvar o contrato.',
                            'metro/flash/error');
                }
            }else{
                $this->Session->setFlash('Erro ao ler o arquivo.',
                            'metro/flash/error');
            }
            echo json_encode(array());
        }
        $tipos = array();
        $tiposAceitos = array(
            'ata_reuniao' => 'Ata de Reunião',
            'adendos_contratuais' => 'Adendos Contratuais',
            'anexo_f_parcelamaker' => 'Parcelamaker'
        );
        foreach($tiposAceitos as $tipo => $contrato)
            if(!file_exists(VIEWS."contratos/templates/$tipo.ctp"))
                $tipos[$tipo] = $contrato;
        $this->set('tipos_contratos', $tipos);
    }
    
    function planejamento_inserir(){
        $this->_inserir();
    }
    
    function comercial_inserir(){
        $this->_inserir();
    }
    
    function formando_visualizar() {
        $turma = $this->Session->read('turma');
        $this->set('turma', $turma);
    }
    
    function formando_exibir() {
        $this->layout = false;
        $turma = $this->Session->read('turma');
        $this->set('turma', $turma);
    }

    function comissao_visualizar($id) {
        $this->planejamento_visualizar($id);
        $this->render('planejamento_visualizar');
    }

    function planejamento_visualizar($id = null) {
        $this->Contrato->id = $id;

        $contrato = $this->Contrato->read();

        if (empty($contrato)) {
            $this->Session->setFlash('Contrato não existente', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/Contratos");
        }

        $contrato['Contrato']['tipo'] = $this->Contrato->tipos_contratos[$contrato['Contrato']['tipo']];

        $this->set('contrato', $contrato);
    }

    function comissao_baixar($id = null) {
        $this->planejamento_baixar($id);
    }

    function planejamento_baixar($id = null) {
        // Se o debugger do cake for diferente de 0 pode haver problemas
        // para alguns sistemas reconhecerem o arquivo baixado, pois pode
        // ter sido escrito texto no começo destes arquivos
        Configure::write('debug', '0');
        // Ainda assim há configurações no php.ini que podem executar
        // tarefas semelhantes, por isso tome cuidado

        $this->layout = '';

        if (!empty($id)) {
            $this->Contrato->id = $id;
            $contrato = $this->Contrato->read();

            if (!empty($contrato)) {
                header("Pragma: public"); // required 
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Cache-Control: private", false); // required for certain browsers 
                header("Content-Type: " . $contrato['Contrato']['tipo']);
                header("Content-Disposition: attachment; filename=\"" . $contrato['Contrato']['nome'] . "\";");
                header("Content-Transfer-Encoding: binary");
                header("Content-Length: " . $contrato['Contrato']['tamanho']);
                ob_clean();
                flush();
                readfile($this->Contrato->getContrato());
            } else {
                // arquivo não existe no banco
                // este erro ocorrerá se alguém modificar os arquivos do diretório sem utilizar o sistema
            }
        } else {
            $this->redirect("/{$this->params['prefix']}/Contratos/");
        }
    }
    
    function comercial_remover($tipo) {
        $this->autoRender = false;
        Configure::write('debug', '0');
        $turma = $this->obterTurmaLogada();
        $tipoContrato = $this->TiposContrato->findById($turma['Turma']['tipos_contrato_id']);
            foreach($tipoContrato['TiposContratoAnexos'] as $nome) 
                if($nome['nome_anexo'] == $tipo)
                    $nomeAnexo = $nome;
        $this->Contrato->unbindModelAll();
        $contrato = $this->Contrato->find('first',array(
            'conditions' => array(
                'Contrato.turma_id' => $turma['Turma']['id'],
                "Contrato.deletado is null",
                "Contrato.tipos_contrato_anexos_id" => $nomeAnexo['id'],
            )
        ));
        if($contrato) {
            $usuario = $this->obterUsuarioLogado();
            $contrato['Contrato']['deletado'] = 1;
            $contrato['Contrato']['deletado_por'] = $usuario['Usuario']['id'];
            if($this->Contrato->save($contrato))
                $this->Session->setFlash("Contrato removido",'metro/flash/success');
            else
                $this->Session->setFlash("Erro ao remover contrato",'metro/flash/error');
        } else {
            $this->Session->setFlash("Contrato não encontrado",'metro/flash/error');
        }
        echo json_encode(array());
    }
    
    function comercial_gerar($tipo, $out = 'pdf') {
        $this->autoRender = false;
        $this->layout = false;
        $turma = $this->obterTurmaLogada();
        $tipoContrato = $this->TiposContrato->findById($turma['Turma']['tipos_contrato_id']);
        $variaveis = array(
            'nome',
            'cursos',
            'comissao',
            'ano_formatura',
            'semestre_formatura'
        );
        $cursos = array();
        foreach($turma['CursoTurma'] as $cursoTurma) $cursos[] = $cursoTurma['Curso']['nome'] . ' - ' . ucfirst($cursoTurma['turno']);
        $turma['Turma']['cursos'] = implode(', ',$cursos);
        $turma['Turma']['comissao'] = $this->ViewFormandos->find('list',array(
            'conditions' => array(
                'turma_id' => $turma['Turma']['id'],
                'grupo' => 'comissao',
                'ativo' => 1
            ),
            'fields' => array('nome','rg','id')
        ));
        if($tipo == 'contrato_principal') {
            $variaveis[] = 'data_assinatura_contrato';
            $variaveis[] = 'expectativa_formandos';
            $variaveis[] = 'valor_total_contrato';
            $variaveis[] = 'multa_por_atraso';
        } elseif($tipo == 'anexo_b_termo_finalizacao_negociacao' || $tipo == 'anexo_b_fechamento') {
            $variaveis[] = 'beneficios_formandos';
            if(!empty($turma['Turma']['beneficios_formandos']))
                $turma['Turma']['beneficios_formandos'] = json_decode($turma['Turma']['beneficios_formandos']);
            else
                $turma['Turma']['beneficios_formandos'] = NULL;
        } elseif($tipo == 'anexo_b_beneficios_comissao') {
            $variaveis[] = 'beneficios_comissao';
            if(!empty($turma['Turma']['beneficios_comissao']))
                $turma['Turma']['beneficios_comissao'] = json_decode($turma['Turma']['beneficios_comissao']);
            else
                $turma['Turma']['beneficios_comissao'] = NULL;
        }elseif($tipo == 'anexo_c_fluxo_financeiro_arrecadacao'){
            $variaveis[] = 'expectativa_formandos';
            $variaveis[] = 'valor_total_contrato';
            if(!empty($turma['Turma']['expectativa_formandos']))
                $turma['Turma']['expectativa_formandos'] = $turma['Turma']['expectativa_formandos'];
            else
                $turma['Turma']['expectativa_formandos'] = NULL;
            if(!empty($turma['Turma']['valor_total_contrato']))
                $turma['Turma']['valor_total_contrato'] = $turma['Turma']['valor_total_contrato'];
            else
                $turma['Turma']['valor_total_contrato'] = NULL;
        }elseif($tipo == 'contrato_arrecadacao_valores'){
            $variaveis[] = 'fee';
            if(!empty($turma['Turma']['fee']))
                $turma['Turma']['fee'] = $turma['Turma']['fee'];
            else
                $turma['Turma']['fee'] = NULL;
        }elseif($tipo == 'anexo_g_valores_arrecadados_arrecadar'){
            $variaveis[] = 'valor_arrecadado_comissao';
            $variaveis[] = 'valor_arrecadar_formandos';
            if(!empty($turma['Turma']['valor_arrecadado_comissao']))
                $turma['Turma']['valor_arrecadado_comissao'] = $turma['Turma']['valor_arrecadado_comissao'];
            else
                $turma['Turma']['valor_arrecadado_comissao'] = NULL;
            if(!empty($turma['Turma']['valor_arrecadar_formandos']))
                $turma['Turma']['valor_arrecadar_formandos'] = $turma['Turma']['valor_arrecadar_formandos'];
            else
                $turma['Turma']['valor_arrecadar_formandos'] = NULL;
        }elseif($tipo == 'anexo_d_contraprestacao_cessao_exclusividade'){
            $variaveis[] = 'forma_pagamento_anexo_d';
            $variaveis[] = 'itens_adicionais_anexo_d';
            if(!empty($turma['Turma']['forma_pagamento_anexo_d']))
                $turma['Turma']['forma_pagamento_anexo_d'] = $turma['Turma']['forma_pagamento_anexo_d'];
            else
                $turma['Turma']['itens_adicionais_anexo_d'] = NULL;
            if(!empty($turma['Turma']['itens_adicionais_anexo_d']))
                $turma['Turma']['itens_adicionais_anexo_d'] = $turma['Turma']['itens_adicionais_anexo_d'];
            else
                $turma['Turma']['itens_adicionais_anexo_d'] = NULL;
        }elseif($tipo == 'anexo_e_termo_comercializacao_albuns'){
            $variaveis[] = 'informacoes_adicionais_anexo_e';
            if(!empty($turma['Turma']['informacoes_adicionais_anexo_e']))
                $turma['Turma']['informacoes_adicionais_anexo_e'] = $turma['Turma']['informacoes_adicionais_anexo_e'];
            else
                $turma['Turma']['informacoes_adicionais_anexo_e'] = NULL;
        }elseif($tipo == 'anexo_a_previsao_eventos_foto_filmagem'){
            $variaveis[] = 'eventos_anexo_a';
            if(!empty($turma['Turma']['eventos_anexo_a']))
                $turma['Turma']['eventos_anexo_a'] = $turma['Turma']['eventos_anexo_a'];
            else
                $turma['Turma']['eventos_anexo_a'] = NULL;
        }
        $completar = array();
        foreach($variaveis as $v)
            if(empty($turma['Turma'][$v]))
                $completar[] = $v;
        foreach($tipoContrato['TiposContratoAnexos'] as $nome) 
            if($nome['nome_anexo'] == $tipo)
                $nomeAnexo = $nome;
        $this->set('turma',$turma);
        $this->set('completar',$completar);
        $this->set('out',$out);
        $this->set('tipo', $tipo);
        $this->set('nome', $nomeAnexo['nome']);
        if(!empty($completar)) {
            $this->render("templates/{$tipoContrato['TiposContrato']['nome']}/completar");
        } else {
            header("Content-Type: text/html; charset=UTF-8",true);
            $contrato = $this->render("templates/{$tipoContrato['TiposContrato']['nome']}/$tipo");
            if($out == 'pdf') {
                $this->Pdf->SetAuthor("Ás Eventos http://www.eventos.as");
                $this->Pdf->setPrintHeader(false);
                $this->Pdf->setPrintFooter(false);
                if($tipo == "anexo_d_adesao") {
                    $this->Pdf->SetMargins(7, 10, 9);
                    $this->Pdf->SetFont('helvetica','',9.0);
                    $this->Pdf->setPageOrientation('P',true,10);
                } else {
                    $this->Pdf->SetMargins(13, 10, 10);
                    $this->Pdf->SetFont('helvetica','',10.7);
                    $this->Pdf->setPageOrientation('P',true,70);
                    $this->Pdf->SetAutoPageBreak(true, 30);
                }
                $this->Pdf->setFontSpacing(0);
                $this->Pdf->setCellHeightRatio(1.15);
                $this->Pdf->SetTextColor(0, 0, 0);
                $this->Pdf->AddPage();
                $this->Pdf->writeHTML($contrato, true, 0, true, 0, 'L');
                $data = array(
                    'Contrato' => array(
                        'nome_secreto' => $this->Arquivo->generateUniqueId(),
                        'diretorio' => $this->Arquivo->file_path(),
                        'tipos_contrato_anexos_id' => $nomeAnexo['id'],
                        'nome' => $nomeAnexo['nome'].".pdf"
                    )
                );
                $file = $data['Contrato']['diretorio'].$data['Contrato']['nome_secreto'];
                $this->Pdf->Output($file,'F');
                if(file_exists($file)) {
                    $usuario = $this->obterUsuarioLogado();
                    $data['Contrato']['tamanho'] = filesize($data['Contrato']['diretorio'].$data['Contrato']['nome_secreto']);
                    $data['Contrato']['turma_id'] = $turma['Turma']['id'];
                    $data['Contrato']['usuario_id'] = $usuario['Usuario']['id'];
                    $data['Contrato']['criado'] = date('Y-m-d H:i:s');
                    if($this->Contrato->save($data))
                        $this->Session->setFlash('Contrato salvo com sucesso','metro/flash/success');
                    else
                        $this->Session->setFlash("Erro ao inserir contrato",'metro/flash/error');
                } else {
                    $this->Session->setFlash("Erro ao gravar contrato",'metro/flash/error');
                }
                echo json_encode(array());
            } elseif($out == "teste") {
                $this->Pdf->SetAuthor("Ás Eventos http://www.eventos.as");
                $this->Pdf->setPrintHeader(false);
                $this->Pdf->setPrintFooter(false);
                if($tipo == "anexo_d_adesao") {
                    $this->Pdf->SetMargins(7, 10, 9);
                    $this->Pdf->setPageOrientation('P',true,10);
                } else {
                    $this->Pdf->SetMargins(13, 45, 10);
                    $this->Pdf->setPageOrientation('P',true,70);
                }
                $this->Pdf->setFontSpacing(0);
                $this->Pdf->setCellHeightRatio(1.15);
                $this->Pdf->SetTextColor(0, 0, 0);
                $this->Pdf->SetFont('helvetica','',10.7);
                $this->Pdf->AddPage();
                $c = $this->Pdf->unhtmlentities($contrato);
                $this->Pdf->writeHTML($c, true, 0, true, 0, 'L');
                header("Pragma: public");
                header("Expires: 0");
                header("Content-Type: text/html; charset=UTF-8",true);
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Cache-Control: private", false);
                header("Content-type:application/pdf");
                $this->Pdf->Output("asdfasdf");
            }
        }
    }
    
    function comercial_visualizar($tipo,$out = "web") {
        Configure::write(array('debug' => 0));
        $this->autoRender = false;
        $turma = $this->obterTurmaLogada();
        $tipoContrato = $this->TiposContrato->findById($turma['Turma']['tipos_contrato_id']);
        foreach($tipoContrato['TiposContratoAnexos'] as $nome){
            if($nome['nome_anexo'] == $tipo)
                $nomeAnexo = $nome;
        }
        $contrato = $this->Contrato->find('first',array(
            'conditions' => array(
                'Contrato.turma_id' => $turma['Turma']['id'],
                "Contrato.deletado is null",
                'Contrato.tipos_contrato_anexos_id' => $nomeAnexo['id']
            )
        ));
        if($contrato) {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private", false);
            header("Content-type:application/pdf");
            if($out == "download")
                header("Content-Disposition: attachment; filename=\"{$turma['Turma']['id']}_{$contrato['Contrato']['nome']}\";");
            header("Content-Transfer-Encoding: binary");
            header("Content-Length: " . $contrato['Contrato']['tamanho']);
            ob_clean();
            flush();
            readfile($contrato['Contrato']['diretorio'].$contrato['Contrato']['nome_secreto']);
        } else {
            $this->redirect("/{$this->params['prefix']}/");
        }
    }

    private function enviarDadosParaView() {
        $turma = $this->Session->read('turma');
        $this->set('parametros', $this->obterParametrosDaTurmaParaContrato());
        $this->set('turma', $turma);
        $this->set('formandos', $this->obterListaDeMembrosDaComissao());
    }

    private function obterParametrosDaTurmaParaContrato() {
        $parametros['numero_aderentes'] = 23;
        $turmaSelecionada = $this->obterTurmaLogada();

        $this->Turma->recursive = 2;
        $this->Turma->contain(array(
            'CursoTurma.Curso.Faculdade.Universidade',
            'TurmasUsuario',
            'Usuario' => array('conditions' => array('grupo' => array('comercial', 'planejamento')))
        ));

        $turma = $this->Turma->findById($turmaSelecionada['Turma']['id']);
        //TODO: verificar como vai ficar. se vai listar todos os cursos
        $parametros['curso'] = $turma['CursoTurma'];
        $parametros['faculdade'] = $turma['CursoTurma'][0]['Curso']['Faculdade']['nome'];
        $parametros['universidade'] = $turma['CursoTurma'][0]['Curso']['Faculdade']['Universidade']['nome'];
        if ($parametros['universidade'] == 'Sem Universidade')
            $parametros['universidade'] = '';
        return $parametros;
    }

    private function obterListaDeMembrosDaComissao() {
        $turma = $this->Session->read('turma');

        $this->Usuario->bindModel(array('hasOne' => array('TurmasUsuario', 'FormandoProfile')), false);

        $cond = array(
            'TurmasUsuario.turma_id' => $turma['Turma']['id'],
            'Usuario.grupo' => 'comissao');

        $usuarios = $this->Usuario->find('all', array('conditions' => $cond));

        $formandos = array();

        foreach ($usuarios as $formandoProfile) {
            array_push($formandos, array('nome' => $formandoProfile['Usuario']['nome'], 'rg' => $formandoProfile['FormandoProfile']['rg']));
        }

        return $formandos;
    }

}
?>