<span id="conteudo-titulo" class="box-com-titulo-header">Vincular parceria com turmas</span>
<div id="conteudo-container">
	<?php $paginator->options(array('url' => array($this->params['prefix'] => true, $parceria['Parceria']['id']))); ?>
	<?php $session->flash(); ?>
	<h2><b>Parceria: </b><?php echo $parceria['Parceria']['titulo'];?></h2>
	<div class="tabela-adicionar-item" style="text-align: right; margin-top:10px;">
		<span style='text-align:right;margin-right: 5px;'>Vincular turma: </span>
		<?php echo $html->link('Adicionar', array($this->params['prefix'] => true, 'controller' => 'parcerias', 'action' =>'vincular_adicionar', $parceria['Parceria']['id']), array('id' => 'botao-adicionar','class' => 'submit button', 'style' => 'float:right; margin-left: 5px;height: 12px;')); ?>
        <?php echo $form->select('', $turmasNaoAtreladasAParceria, null, array('empty' => '-- Selecione uma Turma --', 'id' => 'lista-turmas','class' => 'grid_10 first alpha omega','style' => 'float:right;', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10 first'))); ?>
		        	
		<div style="clear:both;"></div>
	</div>

	<div class="container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col"><?php echo $paginator->sort('Id', 'id'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Codigo', 'turma_id'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Nome', 'rifa_id'); ?></th>				

					<th scope="col"> &nbsp;</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="3"><?php echo $paginator->counter(array('format' => 'Registros %start% a %end% - página %page% de %pages%')); ?>
						<span class="paginacao">
							<?php echo $paginator->numbers(array('separator' => ' ')); ?>
						</span>
					</td>
					<td colspan="1"><?php echo $paginator->counter(array('format' => 'Total : %count% vincula&ccedil;&otilde;es')); ?></td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach($listaDeTurmasDaParceria as $parceriaTurma): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<td colspan="1" width="5%"><?php echo $parceriaTurma['ParceriaTurma']['id'];?></td>
					<td colspan="1" width="20%"><?php echo $parceriaTurma['ParceriaTurma']['turma_id']; ?></td>
					<td colspan="1" width="60%"><?php echo $parceriaTurma['Turma']['nome'];?></td>
					<td colspan="1" width="15%">
						<?php echo$html->link('Remover', array($this->params['prefix'] => true, 'controller' => 'parcerias', 'action' =>'vincular_remover', $parceriaTurma['ParceriaTurma']['id']), array('id' => 'botao-remover', 'class' => 'submit button', 'onclick' => 'return removerLinha(this);')); ?>
                    </td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
		<p class="grid_full alpha omega">
			<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'controller' => 'parcerias','action' => 'index', $parceria['Parceria']['id']) ,array('class' => 'cancel')); ?>
		</p>
	</div>
</div>

<script type="text/javascript">
	$(function($) {
		$('#botao-adicionar').click(function() {
			codigoDaTurmaSelecionada = $("#lista-turmas option:selected").val();
			nomeDaTurmaSelecionada = $("#lista-turmas option:selected").text().replace(codigoDaTurmaSelecionada + ' - ', '');
		
			if (codigoDaTurmaSelecionada == null || codigoDaTurmaSelecionada == "") {
				alert('Selecione uma turma para vincular a parceria.');
			} else {
				$.get(this.href + '/' + codigoDaTurmaSelecionada, function(data) {
					  if (/erro/.test(data)) {
						  alert('Ocorreu um erro ao vincular a turma');
					  } else {
						  $("#lista-turmas option:selected").remove();
						  $(data).prependTo('table > tbody');
					  }
				});
			}

			return false;
		});
	});

	function removerLinha(link) {
		$.get(link.href, function(data) {
			  if (/erro/.test(data)) {
				  alert('Ocorreu um erro ao desvincular a turma');
			  } else {
				  codigoDaTurma = $(link).parents("tr").children(':nth-child(2)').text();
				  nomeDaTurma = $(link).parents("tr").children(':nth-child(3)').text();
				  option = "<option vaule='" + codigoDaTurma + "'>" + codigoDaTurma + " - " + nomeDaTurma + "</option>"
				  $('select > option:first-child').after(option);	
				  $(link).parents("tr").fadeOut('normal');
			  }
		});
		return false;
	}
</script>
