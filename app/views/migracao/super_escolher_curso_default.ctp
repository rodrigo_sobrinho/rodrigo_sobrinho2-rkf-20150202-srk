<span id="conteudo-titulo" class="box-com-titulo-header">Migração de Turma</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>

<span class="grid_full alpha omega first">
	<h2> Escolha o curso padrão para alocação dos formandos. </h2>
</span>	
<span class="grid_full alpha omega first">
<?php
echo $form->create('Turma', array('url' => "/{$this->params['prefix']}/migracao/iniciar_migrar_turma"));
echo $form->hidden('turma_id', array('value' => $turma_id));
echo $form->select('curso_default', $array_cursos);
echo $form->end(array('label' => 'Enviar', 'div' => false, 'class' => 'submit'));
?>
</span>
</div>
