<div class="span12 metro" style="position:relative" id="content-container">
    <div class="row-fluid content-loader hide" id="content-loading">
        <div class="cycle-loader"></div>
    </div>
    <div class="row-fluid content-loader hide" id="content-body"></div>
    <div class="row-fluid content-loader" id="content-home">
    <?php if($turmaLogada['Turma']['id'] == 13){ ?>
        <div class="tile icon bg-color-red"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/mensagens/cronograma') }">
            <div class="tile-content">
                <div class="img icon-mail"></div>
            </div>
            <div class="brand">
                <span class="name">Mensagens</span>
            </div>
        </div>
        <div class="tile icon bg-color-greenDark"
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/arquivos/listar') }">
            <div class="tile-content">
                <span class="img icon-file-excel"></span>
            </div>
            <div class="brand"><span class="name">Arquivos</span></div>
        </div>
        <div class="tile icon bg-color-brown"
            data-bind="click: function(data, event) {
            page('<?=$this->webroot . $this->params['prefix'] ?>/usuarios/listar') }">
           <div class="tile-content">
               <div class="img icon-accessibility"></div>
           </div>
           <div class="brand">
               <span class="name">Funcionários</span>
           </div>
        </div>
        <div class="tile icon bg-color-orangeDark"
            data-bind="click: function(data, event) {
            page('<?=$this->webroot . $this->params['prefix'] ?>/usuarios/listar_aniversariantes') }">
           <div class="tile-content">
               <div class="img icon-gift"></div>
           </div>
           <div class="brand">
               <span class="name">Aniversariantes do Mês</span>
           </div>
        </div>
        <div class="tile icon bg-color-green"
            data-bind="click: function(data, event) {
            page('/arquivos/jornal_mensal') }">
           <div class="tile-content">
               <div class="img icon-newspaper"></div>
           </div>
           <div class="brand">
               <span class="name">Jornal Mensal</span>
           </div>
        </div>
        <div class="tile icon bg-color-yellow"
            data-bind="click: function(data, event) {
            page('<?=$this->webroot . $this->params['prefix'] ?>/eventos/eventos') }">
           <div class="tile-content">
               <div class="img icon-map"></div>
           </div>
           <div class="brand">
               <span class="name">Eventos do Mês</span>
           </div>
        </div>
        <div class="tile icon bg-color-purple"
            data-bind="click: function(data, event) {
            page('<?=$this->webroot . $this->params['prefix'] ?>/arquivos/manuais_procedimento') }">
           <div class="tile-content">
               <div class="img icon-book"></div>
           </div>
           <div class="brand">
               <span class="name">Manuais Procedimento</span>
           </div>
        </div>
    <?php }else{ ?>
        <div class="tile icon bg-color-red"
            data-bind="click: function(data, event) {
            page('/turmas/selecionar/13') }">
           <div class="tile-content">
               <div class="img icon-home"></div>
           </div>
           <div class="brand">
               <span class="name">Intranet</span>
           </div>
        </div>
    <?php } ?>
    </div>
</div>