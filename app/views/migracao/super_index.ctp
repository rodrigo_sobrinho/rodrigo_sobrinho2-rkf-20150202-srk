<span id="conteudo-titulo" class="box-com-titulo-header">Migração de Turma</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>

<span class="grid_full alpha omega first">
	<h2> Selecione uma turma para iniciar a migração </h2>
</span>	
<span class="grid_full alpha omega first">
<?php
echo $form->create('Turma', array('url' => "/{$this->params['prefix']}/migracao/escolher_curso_default"));
echo $form->select('turma_id', $turmas);
echo $form->end(array('label' => 'Enviar', 'div' => false, 'class' => 'submit'));

?>
</span>
</div>
