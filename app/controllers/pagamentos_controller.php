<?php

class PagamentosController extends AppController {

    var $name = 'Pagamentos';
    var $uses = array(
        'Pagamento',
        'Usuario',
        'FormandoProfile',
        'DespesasPagamento',
        'Despesa',
        'TurmasUsuario',
        'Igpm',
        'CursoTurma',
        'ViewFormandos',
        'UploadPagamento'
    );
    var $nomeDoTemplateSidebar = 'turmas';
    // Montar uma lista de turmas e aplicacoes necessarias do igpm
    // ex:
    // $turmas = array( '3544' => array(
    //			'2011-05-05' => 2.3,
    //			'2012-05-05' => 5.4
    //		))
    //
	var $igpms = array();
    var $igpms_turmas = array();

    /*
     * Resultados do processamento de cada boleto:
     * 
     * 1) Estava cadastrado, em aberto, todas as despesas também estavam em aberto, boleto pago com sucesso
     * 2) Estava cadastrado, em aberto, mas uma ou mais despesas já haviam sido pagas por outros boletos. Fica registrado como parcialmente pago
     * 3) Estava cadastrado mas já havia sido pago, boleto fica cadastrado como duplicado
     * 4) Não estava cadastrado, formando foi encontrado a partir do código do boleto, fica registrado como 'boleto não cadastrado'
     * 5) Não estava cadastrado e não foi possível encontrar um formando a partir do código do boleto. Fica registrado como 'boleto perdido'
     * 
     */

    const BOLETO_PAGO_SUCESSO = 1;
    const BOLETO_PAGO_PARCIALMENTE = 2;
    const BOLETO_DUPLICADO = 3;
    const BOLETO_NAO_CADASTRADO = 4;
    const BOLETO_PERDIDO = 5;
    const BOLETO_ANTIGO_SUCESSO = 6;
    const BOLETO_ANTIGO_PERDIDO = 7;
    const CHEQUE_PAGO_COM_DESPESA = 8;
    const CHEQUE_PAGO_SEM_DESPESA = 9;
    const CHEQUE_PAGO_PERDIDO = 10;

    var $texto_status = array(
        self :: BOLETO_PAGO_SUCESSO => 'Pago com sucesso', //pagou a despesa com sucesso
        self :: BOLETO_PAGO_PARCIALMENTE => 'Pago parcialmente', //pagou somente parte da divida
        self :: BOLETO_DUPLICADO => 'Boleto já existente. Foi duplicado', //pagamento existia e já estava pago
        self :: BOLETO_NAO_CADASTRADO => 'Boleto n&atilde;o cadastrado', // (usuario encontrato, mas boleto nao cadastrado)
        self :: BOLETO_PERDIDO => 'Boleto sem usu&aacute;rio', //(sem usuario encontrado)
        self :: BOLETO_ANTIGO_SUCESSO => 'Boleto Antigo - Com usu&aacute;rio', //(encontrou usuario)
        self :: BOLETO_ANTIGO_PERDIDO => 'Boleto Antigo - Sem usu&aacute;rio', //(sem usuario encontrado)
        self :: CHEQUE_PAGO_COM_DESPESA => 'Cheque pagou despesa',
        self :: CHEQUE_PAGO_SEM_DESPESA => 'Cheque virou crédito',
        self :: CHEQUE_PAGO_PERDIDO => 'Cheque cadastrado. FORMANDO NAO ENCONTRADO.'
    );
    var $boleto_status = array();
    var $leitorExcel = null;
    var $pagamentosASeremProcessados = array();
    var $pagamentosProcessados = array(
        'boleto' => array(
            'antigo' => array(),
            'novo' => array(),
            'igpm' => array()
         ),
        'cheque' => array()
    );
    var $ids_funcionarios;
    
    function financeiro_cancelar_agendamento($id) {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $this->UploadPagamento->id = $id;
        $upload = $this->UploadPagamento->read();
        if($upload) {
            if($this->UploadPagamento->saveField('agendado',0)) {
                $this->Session->setFlash('Agendamento Cancelado',
                    'metro/flash/success');
            } else {
                $this->Session->setFlash('Erro ao Cancelar Agendamento',
                    'metro/flash/error');
            }
        } else {
            $this->Session->setFlash('Erro ao Buscar Arquivo',
                'metro/flash/error');
        }
        exit();
    }
    
    function financeiro_inserir_agendamento($id) {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $this->UploadPagamento->id = $id;
        $upload = $this->UploadPagamento->read();
        if($upload) {
            if($this->UploadPagamento->saveField('agendado',1)) {
                $this->Session->setFlash('Pagamento Agendado',
                    'metro/flash/success');
            } else {
                $this->Session->setFlash('Erro ao Agendar Pagamento',
                    'metro/flash/error');
            }
        } else {
            $this->Session->setFlash('Erro ao Buscar Arquivo',
                'metro/flash/error');
        }
        exit();
    }
    
    function financeiro_apagar_upload($id) {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $this->UploadPagamento->id = $id;
        $upload = $this->UploadPagamento->read();
        if($upload) {
            $agendados = $this->UploadPagamento->find('count',array(
                'conditions' => array('UploadPagamento.agendado' => 1)
            ));
            if(!empty($upload['UploadPagamento']['data_inicio'])) {
                $this->Session->setFlash('Importacao ja foi iniciada. Registro nao pode ser deletado',
                        'metro/flash/error');
            } else {
                if($this->UploadPagamento->delete($id)) {
                    $this->Session->setFlash('Pagamento Deletado',
                        'metro/flash/success');
                } else {
                    $this->Session->setFlash('Erro ao Deletar Pagamento',
                        'metro/flash/error');
                }
            }
        } else {
            $this->Session->setFlash('Registro nao encontrado',
                'metro/flash/error');
        }
        exit();
    }
    
    function financeiro_verificar_upload($id = false) {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('erro' => 1);
        $usuario = $this->Session->read('Usuario');
        if($usuario) {
            if($id) {
                $this->UploadPagamento->id = $id;
                $arquivo = $this->UploadPagamento->read();
                if(!empty($arquivo['UploadPagamento']['data_inicio']))
                    $arquivo['UploadPagamento']['data_inicio'] =
                        date('d/m/Y à\s H:i',strtotime($arquivo['UploadPagamento']['data_inicio']));
                if(!empty($arquivo['UploadPagamento']['data_fim']))
                    $arquivo['UploadPagamento']['data_fim'] =
                        date('d/m/Y à\s H:i',strtotime($arquivo['UploadPagamento']['data_fim']));
                $json['arquivo'] = $arquivo;
                $json['erro'] = 0;
                if(empty($arquivo['UploadPagamento']['data_fim']) &&
                        !$this->processoIniciado($arquivo['UploadPagamento']['id'])) {
                    $caminho = APP."../cake/console/cake.php upload_pagamento processar";
                    $comando = "php {$caminho} {$arquivo['UploadPagamento']['id']} > /dev/null 2>&1 &";
                    exec($comando);
                }
            } else {
                $json['mensagem'] = 'Erro ao carregar arquivo';
            }
        } else {
            $json['mensagem'] = 'Erro ao validar usuario';
        }
        echo json_encode($json);
    }
    
    private function processoIniciado($id) {
        $retorno = false;
        $processo = shell_exec("ps -eaf | grep 'processar {$id}'");
        if(strpos($processo,'cake.php') !== FALSE)
            $retorno = true;
        return $retorno;
    }
    
    function financeiro_view($id) {
        $this->UploadPagamento->id = $id;
        $arquivo = $this->UploadPagamento->read();
        if(file_exists($arquivo['UploadPagamento']['arquivo_html'])) {
            $this->set('arquivo',$arquivo['UploadPagamento']['arquivo_html']);
        } else {
            $this->Session->setFlash('Arquivo de log não encontrado.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/pagamentos/enviar_planilha");
        }
    }
    
    function financeiro_excel($id) {
        Configure::write('debug', '0');
        $this->layout = false;
        $this->UploadPagamento->id = $id;
        $arquivo = $this->UploadPagamento->read();
        if(!empty($arquivo['UploadPagamento']['arquivo'])) {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header("Content-Type: {$arquivo['UploadPagamento']['tipo']}");
            header("Content-Disposition: attachment; filename=\"upload_" .
                    $arquivo['UploadPagamento']['id'] . ".{$arquivo['UploadPagamento']['extensao']}\";" );
            header("Content-Transfer-Encoding: binary");
            header("Content-Length: " . $arquivo['UploadPagamento']['tamanho']);
            ob_clean();
            flush();
            echo($arquivo['UploadPagamento']['arquivo']);
        } else {
            $this->Session->setFlash('Arquivo de importação não encontrado.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/pagamentos/enviar_planilha");
        }
    }
    
    function financeiro_iniciar_upload() {
        if(!empty($this->data) && is_uploaded_file($this->data['UploadPagamento']['arquivo']['tmp_name'])) {
            $usuario = $this->Session->read('Usuario');
            if($usuario) {
                $this->UploadPagamento->create();
                $dados['UploadPagamento'] = array(
                    'usuario_id' => $usuario['Usuario']['id'],
                    'tmp_name' => $this->data['UploadPagamento']['arquivo']['tmp_name'],
                    'tipo' => $this->data['UploadPagamento']['arquivo']['type'],
                    'tamanho' => $this->data['UploadPagamento']['arquivo']['size'],
                    'extensao' => end(explode('.',$this->data['UploadPagamento']['arquivo']['name'])),
                    'data_cadastro' => date('Y-m-d H:i:s')
                );
                if(!$this->UploadPagamento->save($dados))
                    $this->Session->setFlash('Ocorreu um problema ao salvar o arquivo.', 'flash_erro');
                else
                    $this->Session->setFlash('Arquivo de pagamentos salvo corretamente. Aguarde o inicio do processamento',
                            'flash_sucesso');
            } else {
                $this->Session->setFlash('Erro ao validar usu&aacute;rio.', 'flash_erro');
            }
        } else {
            $this->Session->setFlash('Ocorreu um problema com o upload do arquivo.', 'flash_erro');
        }
        $this->redirect("/{$this->params['prefix']}/pagamentos/arquivos_upload");
    }
    
    function financeiro_arquivos_upload() {
        $arquivos = $this->UploadPagamento->find('all');
        $this->set('arquivos',$arquivos);
    }

    function financeiro_enviar_planilha() {
        
    }
    
    function financeiro_upload_pagamentos() {
    	$this->autoLayout = false;
        if($this->data) {
            $usuario = $this->Session->read('Usuario');
            if($usuario) {
                $this->UploadPagamento->create();
                $dados['UploadPagamento'] = array(
                    'usuario_id' => $usuario['Usuario']['id'],
                    'tipo' => $this->data['tipo'],
                    'tamanho' => $this->data['tamanho'],
                    'extensao' => $this->data['extensao'],
                    'arquivo' => base64_decode($this->data['data']),
                    'data_cadastro' => date('Y-m-d H:i:s')
                );
                if(!$this->UploadPagamento->save($dados))
                    $this->Session->setFlash('Ocorreu um problema ao salvar o arquivo.',
                        'metro/flash/error');
                else
                    $this->Session->setFlash('Arquivo de pagamentos salvo corretamente',
                        'metro/flash/success');
            }
            exit();
        } else {
            $arquivos = $this->UploadPagamento->find('all',
                    array('order' => array('UploadPagamento.id' => 'desc')));
            $this->set('arquivos',$arquivos);
        }
    }

}

?>