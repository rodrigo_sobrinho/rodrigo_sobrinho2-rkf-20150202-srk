<span id="conteudo-titulo" class="box-com-titulo-header">Informações do Arquivo</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="detalhes">
		<p class="grid_11 alpha omega">
			<label class="grid_11 alpha omega">Nome</label>
			<span class="grid_2 alpha first"> <?php echo $arquivo['Arquivo']['nome']?> </span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_11 alpha omega">Enviado por</label>
			<span class="grid_2 alpha first"> <?php echo $arquivo['Usuario']['nome']?> </span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Tamanho</label>
			<label class="grid_6 alpha">Tipo</label>
			<span class="grid_5 alpha first"><?php echo $arquivo['Arquivo']['tamanho']?></span>
			<span class="grid_5 alpha"><?php echo $arquivo['Arquivo']['tipo']?></span>
		</p>
		<p class="grid_11 alpha omega">
			<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index') ,array('class' => 'cancel')); ?>
			<?php echo $html->link('Baixar', array($this->params['prefix'] => true, 'controller' => 'Arquivos', 'action' => 'baixar', $arquivo['Arquivo']['id']), array('class' => 'submit ')) ?>
		</p>
		
		
	</div>
</div>