<span id="conteudo-titulo" class="box-com-titulo-header">Item do Cronograma</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="detalhes">
		<?php include('_visualizar.ctp');?>
		<p class="grid_6 alpha omega">
			<?php echo $html->link('Editar',array($this->params['prefix'] => true, 'controller' => 'cronogramas' , 'action' => 'editar', $cronograma['Cronograma']['id']) ,array('class' => 'submit')); ?>
			<?php echo $html->link('Excluir',array($this->params['prefix'] => true, 'controller' => 'cronogramas' , 'action' => 'excluir', $cronograma['Cronograma']['id']) ,array('class' => 'submit')); ?>
			<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index') ,array('class' => 'cancel')); ?>
		</p>
		
	</div>
</div>
