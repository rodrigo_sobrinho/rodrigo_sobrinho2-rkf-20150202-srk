<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css?v=0.1">
<style>
    tfoot > tr > td { border: 1px #ddd solid!important; }
    tfoot > tr > td > label{ cursor: default!important }
</style>
<div class="row-fluid">
        <div class="span6">
            <h2>
                <a class="metro-button reload" data-bind="click: function() { 
                reload() }"></a>
                Checkout da Turma <?=$turma?>
            </h2>
        </div>
        <div class="span4">
            <a class="button mini bg-color-yellow pull-right" target="blank"
                href="<?=$this->params['prefix']?>/checkout/checkout_data/">
                Por Data
                <i class="icon-list"></i>
            </a>
        </div>
        <div class="span2">
            <a class="button mini bg-color-greenDark pull-right" target="blank" 
                href="<?=$this->params['prefix']?>/checkout/total_checkout_excel/<?=$turma?>">
                Gerar Excel
                <i class="icon-file-excel"></i>
            </a>
        </div>
</div>
<?php $session->flash(); ?>
<div class="row-fluid">
    <?php if(!empty($relatorio)) : ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="10%">Cód. Formando</th>
                <th scope="col" width="30%">Nome</th>
                <th scope="col" width="10%">Mesas</th>
                <th scope="col" width="10%">Convites</th>
                <th scope="col" width="10%">Meias</th>
                <th scope="col" width="15%">Atendente</th>
                <th scope="col" width="15%">Data</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($relatorio[2] as $r): ?>
            <tr>
                <td colspan="1"><?php echo $r['codigo_formando'];?></td>
                <td colspan="1"><?php echo $r['nome'];?></td>
                <td colspan="1"><?php echo $r['total_mesas']?></td>
                <td colspan="1"><?php echo $r['total_convites']?></td>
                <td colspan="1"><?php echo $r['total_meias']?></td>
                <td colspan="1"><?php echo $r['atendente']?></td>
                <td colspan="1"><?php echo $r['data']?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="1" class="paginacao">
                    <label class="fg-color-red"><b>Totais</b></label>
                </td>
                <td colspan="1">
                    &nbsp
                </td>
                <td colspan="1" class="paginacao">
                    <label class="fg-color-greenDark"><b><?=$relatorio[1]['mesas']?></b></label>
                </td>
                <td colspan="1" class="paginacao">
                    <label class="fg-color-greenDark"><b><?=$relatorio[1]['convites']?></b></label>
                </td>
                <td colspan="1" class="paginacao">
                    <label class="fg-color-greenDark"><b><?=$relatorio[1]['meias']?></b></label>
                </td>
                <td colspan="2">
                    &nbsp
                </td>
            </tr>
        </tfoot>
    </table>
    <?php else : ?>
    <h2 class="fg-color-red">Nenhum Checkout realizado.</h2>
    <?php endif; ?>
</div>