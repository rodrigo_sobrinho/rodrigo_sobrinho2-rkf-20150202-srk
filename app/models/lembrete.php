<?php
class Lembrete extends AppModel {
    var $name = 'Lembrete';

	/*
	 * Tabela no banco está assim:
	 * id INT
	 * titulo VARCHAR(45)
	 * texto VARCHAR(200)
	 * data_criacao(45)
	 * data_modificacao(45)
	 * usuario_id INT, FK
	 */
    var $validate = array(
		'titulo' => array(
		    'vazio' => array(
                'rule' => 'notEmpty',
                'message' => 'Preencha o titulo.'
            )
		)
    );
}
?>