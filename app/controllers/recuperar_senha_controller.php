<?php

class RecuperarSenhaController extends AppController {
    var $components = array();
    var $name = 'RecuperarSenha';
    var $uses = array('Usuario','ViewFormandos','RecuperarSenha');
    var $app = array('verificar_email','perguntas','respostas','trocar_senha');

    function beforeFilter() {
        $this->Auth->allow('perguntas');
        parent::beforeFilter();
    }
    
    function app_verificar_email() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('usuario_id' => false,'mensagem' => array());
        if(isset($this->data['email'])) {
            $usuario = $this->_obterUsuario($this->data['email']);
            if($usuario)
                $json['usuario_id'] = $usuario['ViewFormandos']['id'];
            else
                $json['mensagem'][] = "Nenhum usuário com o email {$this->data['email']} foi encontrado";
        } else {
            $json['mensagem'][] = "O servidor encontrou um erro, cod 1. Tente novamente";
        }
        echo json_encode($json);
    }
    
    function app_perguntas() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('perguntas' => false,'mensagem' => array());
        if(isset($this->data['usuario_id'])) {
            $usuario = $this->ViewFormandos->read(null,$this->data['usuario_id']);
            if($usuario) {
                $ids = $this->_obterIdsPerguntas();
                shuffle($ids);
                $perguntas = array();
                foreach($ids as $id) {
                    $pergunta = $this->_obterPergunta($usuario, $id);
                    if($pergunta) $perguntas[] = $pergunta;
                    if(count($perguntas) == 5) break;
                }
                if(count($perguntas) == 5)
                    $json['perguntas'] = $perguntas;
                else
                    $json['mensagem'][] = "O servidor encontrou um erro, cod 3. Tente novamente";
            } else {
                $json['mensagem'][] = "Erro ao buscar usuário";
            }
        } else {
            $json['mensagem'][] = "O servidor encontrou um erro, cod 2. Tente novamente";
        }
        echo json_encode($json);
    }
    
    function app_respostas() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('corretas' => 0,'mensagem' => array());
        if(isset($this->data['usuario_id']) && isset($this->data['perguntas'])) {
            $usuario = $this->ViewFormandos->read(null,$this->data['usuario_id']);
            if($usuario) {
                foreach($this->data['perguntas'] as $pergunta)
                    if($this->_verificarResposta($usuario,$pergunta['id'],$pergunta['resposta']))
                        $json['corretas']++;
            } else {
                $json['mensagem'][] = "Erro ao buscar usuário";
            }
        } else {
            $json['mensagem'][] = "O servidor encontrou um erro, cod 2. Tente novamente";
        }
        echo json_encode($json);
    }
    
    function app_trocar_senha() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('erro' => true,'mensagem' => array());
        if(isset($this->data['usuario_id']) && isset($this->data['senha'])) {
            $usuario = $this->ViewFormandos->read(null,$this->data['usuario_id']);
            if($usuario) {
                $this->Usuario->id = $usuario['ViewFormandos']['id'];
                if($this->Usuario->saveField('senha', Security::hash($this->data['senha'], 'sha1', true)))
                    $json['erro'] = false;
                else
                    $json['mensagem'][] = "Erro ao alterar a senha. Tente novamente";
            } else {
                $json['mensagem'][] = "Erro ao buscar usuário";
            }
        } else {
            $json['mensagem'][] = "O servidor encontrou um erro, cod 2. Tente novamente";
        }
        echo json_encode($json);
    }
    
    private function _verificarResposta($usuario,$perguntaId,$resposta) {
        $return = false;
        $pergunta = $this->RecuperarSenha->read(null,$perguntaId);
        if($pergunta) {
            $r = $this->_eval($usuario, $pergunta['RecuperarSenha']['alternativa'],$pergunta['RecuperarSenha']['function']);
            if($r == $resposta)
                $return = true;
        }
        return $return;
    }
    
    private function _obterPergunta($usuario,$perguntaId) {
        $return = false;
        $pergunta = $this->RecuperarSenha->read(null,$perguntaId);
        if($pergunta) {
            $conditions = array(
                "ViewFormandos.ativo" => 1,
                'ViewFormandos.grupo' => array('comissao','formando')
            );
            $filtros = explode(',',$pergunta['RecuperarSenha']['filtro']);
            foreach($filtros as $filtro) {
                if(empty($usuario['ViewFormandos'][$filtro]))
                    continue;
                else {
                    $conditions[] = "coalesce(ViewFormandos.{$filtro},'') <> ''";
                    $conditions[] = "{$filtro} <> '{$usuario['ViewFormandos'][$filtro]}'";
                }
            }
            $usuarios = $this->_obterUsuariosDaPergunta($conditions, $filtros);
            if($usuarios) {
                $item = array(
                    'id' => $pergunta['RecuperarSenha']['id'],
                    'titulo' => $pergunta['RecuperarSenha']['titulo'],
                    'pergunta' => $pergunta['RecuperarSenha']['pergunta'],
                    'alternativas' => array($this->_eval($usuario, $pergunta['RecuperarSenha']['alternativa'],
                            $pergunta['RecuperarSenha']['function']))
                );
                foreach($usuarios as $u)
                    $item['alternativas'][] = $this->_eval($u, $pergunta['RecuperarSenha']['alternativa'],
                            $pergunta['RecuperarSenha']['function']);
                shuffle($item['alternativas']);
                $return = $item;
            }
        }
        return $return;
    }
    
    private function _obterUsuario($email) {
        $usuario = $this->ViewFormandos->find('first',array(
            'conditions' => array(
                'ViewFormandos.ativo' => 1,
                'ViewFormandos.email' => $email
            )
        ));
        return $usuario;
    }
    
    private function _obterUsuariosDaPergunta($conditions,$group) {
        $usuarios = $this->ViewFormandos->find('all',array(
            'conditions' => $conditions,
            'limit' => 4,
            'group' => $group,
            'order' => array('rand()')
        ));
        if(count($usuarios) < 4)
            return false;
        else
            return $usuarios;
    }
    
    private function _obterIdsPerguntas() {
        return $this->RecuperarSenha->find('list',array('conditions' => array('ativa' => 1)));
    }
    
    private function _eval($usuario,$alternativa,$function) {
        $alternativa = preg_replace('/{{(.*?)}}/is','{$usuario[\'ViewFormandos\'][\'$1\']}',$alternativa);
        eval("\$alternativa = \"$alternativa\";");
        if(!empty($function))
            $alternativa = $this->{$function}($alternativa);
        return $alternativa;
    }
    
    private function formataData($data) {
        return date('d/m/Y',strtotime($data));
    }
}
?>