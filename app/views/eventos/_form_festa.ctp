<script type="text/javascript">
    $(function() {
        $( "#timepicker" ).timepicker( $.timepicker.regional[ "pt-BR" ] );
    });
</script>

<p class="grid_11 alpha omega">
	<label class="grid_0 alpha">Horário da Valsa</label>
	<?php echo $form->input('horario_valsa_aux', array( 'class' => 'grid_3 alpha first omega', 'label' => false, 'div' => false,'error' => array('wrap' => 'span', 'class' => 'grid_8'), 'id' => 'timepicker')); ?>
</p>
