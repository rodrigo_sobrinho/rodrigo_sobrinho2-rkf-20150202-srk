<script type="text/javascript">
    $(function() {
        $( "#datepicker" ).datetimepicker( $.datepicker.regional[ "pt-BR" ] );
    });
</script>
<?php echo $form->input('id', array('hiddenField' => true)); ?>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Data</label>
	<?php echo $form->input('data-hora', array('class' => 'grid_6 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'), 'id' => 'datepicker')); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Tipo</label>
	<?php echo $form->select('tipo', $tipo_select, array('selected' => utf8_encode($this->data['RegistrosContato']['tipo'])), array('empty'=>false, 'class' => 'grid_4 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Duração (minutos)</label>
	<?php echo $form->input('duracao', array('class' => 'grid_8 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Participantes da Comissão</label>
	<?php echo $form->textarea('membros_comissao', array('class' => 'grid_6 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Participantes da RK</label>
	<?php echo $form->textarea('funcionarios_as', array('class' => 'grid_6 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Assunto</label>
	<?php echo $form->textarea('assunto', array('class' => 'grid_6 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Detalhes</label>
	<?php echo $form->textarea('detalhes', array('class' => 'grid_11 alpha first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>