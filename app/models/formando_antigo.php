<?php

class FormandoAntigo extends AppModel {

    var $name = 'FormandoAntigo';
    var $useTable = 'aluno';

    
    function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);
		$this->virtualFields['diavencparc'] = "DATE_FORMAT( " . $this->alias . ".dtprimparcela , \"\%e\")";
	}
    
    function afterFind($results, $primary=false) {
        if($primary == true) {
           if(Set::check($results, '0.0')) {
              $fieldName = key($results[0][0]);
               foreach($results as $key=>$value) {
                  $results[$key][$this->alias][$fieldName] = $value[0][$fieldName];
                  unset($results[$key][0]);
               }
            }
        }
 
        return $results;
	}
    
}

?>