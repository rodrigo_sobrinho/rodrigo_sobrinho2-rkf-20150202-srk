<span id="conteudo-titulo" class="box-com-titulo-header">Campanha</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="detalhes">
		<?php include('_visualizar.ctp');?>

            <div class="conteudo-container first">
                    <table>
                            <thead>
                                    <tr>
                                            <th scope="col">Id</th>
                                            <th scope="col">Evento</th>
                                            <th scope="col">Nome</th>
                                            <th scope="col">Qtde. Min.</th>
                                            <th scope="col">Qtde. Máx.</th>
                                            <th scope="col">Valor</th>
                                    </tr>
                            </thead>
                            <tbody>
                            <?php $isOdd = false; ?>
                            <?php foreach($data['CampanhasExtra'] as $extra): ?>
                                    <?php if($isOdd):?>
                                            <tr class="odd">
                                    <?php else:?>
                                            <tr>
                                    <?php endif;?>
                                            <td colspan="1"><?php echo $extra['Extra']['id'];?></td>
                                            <td colspan="1"><?php echo $extra['Extra']['Evento']['nome'];?></td>
                                            <td colspan="1"><?php echo $extra['Extra']['nome'];?></td>
                                            <td colspan="1"><?php echo $extra['quantidade_minima'];?></td>
                                            <td colspan="1"><?php echo $extra['quantidade_maxima']?></td>
                                            <td colspan="1"><?php echo $extra['valor'];?></td>
                                    </tr>
                                    <?php $isOdd = !($isOdd); ?>
                            <?php endforeach; ?>
                            </tbody>
                    </table>
            </div>
        <p class="grid_16 alpha omega">
        <?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index') ,array('class' => 'cancel')); ?>
    </p>

</div>
</div>