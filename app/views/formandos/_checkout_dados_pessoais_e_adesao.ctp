<h1 class="grid_16 first" style="margin-top: 20px;border-bottom: 1px solid #000;">Ades&atilde;o <?php if ($this->params['action'] == 'planejamento_checkout' || $this->params['action'] == 'formando_checkout') { ?>	 				<a style="float:right;font-size:14px; margin-top:10px;">exibir<?php } ?></a><div style="clear: both;"></div></h1>
<div class="conteudo-container grid_16 checkout-secao-container" id='secao-adesao'>
	
	<div class='grid_16 clear first painel_formando'>
		<span class="grid_16 painel_titulo">
				Dados Pessoais
		</span>
		<div class='grid_6 first'>
			<span class="foto_formando">
				<img src="<?php echo $this->webroot.$formando['FormandoProfile']['diretorio_foto_perfil']; ?>"></img>
			</span>
			<p class="info_formando">
				<span class="nome_formando">
					<?php echo $formando['Usuario']['nome']; ?>
				</span>
				
				<span class="email_formando">
					<?php echo $formando['Usuario']['email']; ?>
				</span>
	
			</p>
		</div>
			
		<div class='grid_5'>
			<p class="grid_full alpha omega">
				<label class="grid_full alpha first">Curso</label>
				<span class="grid_full alpha first"><?php echo $formando['CursoTurma']['Curso']['nome'];?></span>
			</p>
			<p class="grid_full alpha omega">
				<label class="grid_full alpha first">Sala</label>
				<span class="grid_full alpha" first><?php echo $formando['FormandoProfile']['sala'];?></span>
			</p>
		</div>
		
		<div class='grid_5'>
			<p class="grid_full alpha omega">
				<label class="grid_full alpha">Realizou checkout?</label>
				<span class="grid_full alpha first"><?php echo ($formando['FormandoProfile']['realizou_checkout'] == 0) ? "N&atilde;o" : "Sim";?></label>
			</p>
						
			<p class="grid_full alpha omega">
				<label class="grid_8 alpha first">Tel. Celular</label>
				<label class="grid_8 alpha">Tel. Residencial</label>
	
				<span class="grid_8 alpha first"><?php echo $formando['FormandoProfile']['tel_celular'];?></span>
				<span class="grid_8 alpha"><?php echo $formando['FormandoProfile']['tel_residencial'];?></span>
	
			</p>
		</div>
	</div>
	
	<!-- Informações da Adesão -->
	<h2 class="grid_full first" style="margin-top: 10px;margin-bottom: 5px;"><b>Informa&ccedil;&otilde;es da ades&atilde;o</b></h2>

	<div class="grid_16 first detalhes" style="padding-left: 10px;">
		<p class="grid_8 alpha omega first" >
			<span class="grid_full alpha omega">Data contrato da Turma: <?php echo date('d/m/Y', strtotime($turmaLogada['Turma']['data_assinatura_contrato']));?></span>

		<?php 
			$proximaDataDoIGPM = $turmaLogada['Turma']['data_assinatura_contrato'];
			
			while (strtotime($proximaDataDoIGPM) <= strtotime('now')) {
				$arrayData = explode('-', $proximaDataDoIGPM);
				$arrayData[0]++;
				$proximaDataDoIGPM = implode('-', $arrayData);
			}
		?>

			<span class="grid_full alpha omega">Data IGPM: <?php echo date('d/m/Y', strtotime($proximaDataDoIGPM));?>  </span>
			<span class="grid_full alpha omega">Mesas do contrato: <?php echo $turmaLogada['Turma']['mesas_contrato'];?></span>
			<span class="grid_full alpha omega">Convites do contrato: <?php echo $turmaLogada['Turma']['convites_contrato'];?></span>
		</p>
		<p class="grid_8 alpha omega" >
			<span class="grid_full alpha omega">Valor de Contrato: R$<?php echo number_format($formando['FormandoProfile']['valor_adesao'],2,',','.');?></span>
			<span class="grid_full alpha omega">Data Adesão: <?php echo date('d/m/Y', strtotime($formando['FormandoProfile']['data_adesao']));?>  </span>
			<span class="grid_full alpha omega">Número de Parcelas do Contrato: <?php echo $formando['FormandoProfile']['parcelas_adesao'];?></span>
		</p>
	</div>

	
	<h2 class="grid_full first" style="margin-top: 10px;margin-bottom: 5px;"><b>Despesas com ades&atilde;o</b></h2>
	<?php
	if( sizeof($despesas_adesao) == 0 ) {
		echo 'Nenhuma despesa com adesão encontrada.';
	} else {
	?>
	<div class="container-tabela-financeiro grid_full first">
		<table class="tabela-financeiro tabela-boletos">
			<thead>
				<tr>
					<?php if ($this->params['prefix'] == 'planejamento') { ?><th scope="col">Id</th><?php } ?>
					<th scope="col">Data de Vencimento</th>
					<th scope="col">Valor Base</th>
					<?php if ($this->params['prefix'] == 'planejamento') { ?><th scope="col">Multa</th><?php } ?>
					<th scope="col">Status</th>
					<th scope="col">Parcela</th>
					<th scope="col">Data de Liq.</th>
					<th scope="col">Correção IGPM</th>
					<th scope="col">Status IGPM</th>
					<th scope="col"> </th>
				</tr>
			</thead>
			<tfoot>
				<tr class="linha-despesa">
					<td colspan="6"></td>
					<td colspan="3"><?php echo count($despesas_adesao);?> despesas com ades&atilde;o</td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php 
				$totalDeAdesao['valorBase'] = 0;
				$totalDeAdesao['multa'] = 0;
				$totalDeAdesao['igpm'] = 0;
				$totalDeAdesao['valorFinalDeDespesas'] = 0;
				$totalDeAdesao['totalPago'] = 0;
				$totalDeAdesao['totalPagoSemMulta'] = 0;
				$totalDeAdesao['totalDevido'] = 0;
				$totalDevidoBaseComIGPM = 0;
				
				foreach ($despesas_adesao as $despesa_adesao): 
					if ($despesa_adesao['Despesa']['status'] != "cancelada") {
						$totalDeAdesao['valorBase'] += $despesa_adesao['Despesa']['valor'];
						$totalDeAdesao['multa'] += $despesa_adesao['Despesa']['multa'];
						$totalDeAdesao['valorFinalDeDespesas'] += $despesa_adesao['Despesa']['valor'] + $despesa_adesao['Despesa']['multa'];
						
						if ($despesa_adesao['Despesa']['status_igpm'] != "nao_virado") {
							$totalDeAdesao['igpm'] += $despesa_adesao['Despesa']['correcao_igpm'];
							$totalDeAdesao['valorFinalDeDespesas'] += $despesa_adesao['Despesa']['correcao_igpm'];
						}
						
						if ($despesa_adesao['Despesa']['status'] == "paga") {
							$totalDeAdesao['totalPago'] += $despesa_adesao['Despesa']['valor'] + $despesa_adesao['Despesa']['multa'];
							$totalDeAdesao['totalPagoSemMulta'] += $despesa_adesao['Despesa']['valor'];
						} else if ($despesa_adesao['Despesa']['status'] == "aberta" || $despesa_adesao['Despesa']['status'] == "atrasada") { 
							$totalDeAdesao['totalDevido'] +=  $despesa_adesao['Despesa']['valor'] + $despesa_adesao['Despesa']['multa'];

							$totalDevidoBaseComIGPM += $despesa_adesao['Despesa']['valor'];
							if ($despesa_adesao['Despesa']['status_igpm'] != "nao_virado") {
								$totalDevidoBaseComIGPM += $despesa_adesao['Despesa']['correcao_igpm'];
							}
						}
						
						if ($despesa_adesao['Despesa']['status_igpm'] == "pago")
							$totalDeAdesao['totalPago'] += $despesa_adesao['Despesa']['correcao_igpm'];
						else if ($despesa_adesao['Despesa']['status_igpm'] == "nao_pago")
							$totalDeAdesao['totalDevido'] += $despesa_adesao['Despesa']['correcao_igpm'];
					}
			?>
				<?php if($isOdd):?>
					<tr class="odd linha-despesa"  id = "linha-despesa-<?php echo $despesa_adesao['Despesa']['id']; ?>" name = "linha-despesa-<?php echo $despesa_adesao['Despesa']['id']; ?>" name = "linha-despesa-<?php echo $despesa_adesao['Despesa']['id']; ?>">
				<?php else:?>
					<tr class="linha-despesa" id = "linha-despesa-<?php echo $despesa_adesao['Despesa']['id']; ?>" name = "linha-despesa-<?php echo $despesa_adesao['Despesa']['id']; ?>" name = "linha-despesa-<?php echo $despesa_adesao['Despesa']['id']; ?>">
				<?php endif;?>
					<?php if ($this->params['prefix'] == 'planejamento') { ?>
						<td  colspan="1" width="10%"><?php echo $despesa_adesao['Despesa']['id']; ?></td>
					<?php }?>
					<td  colspan="1" width="10%"><?php echo date('d/m/Y', strtotime($despesa_adesao['Despesa']['data_vencimento'])); ?></td>					
					<td  colspan="1" width="10%"><?php echo $despesa_adesao['Despesa']['valor']; ?></td>
					<?php if ($this->params['prefix'] == 'planejamento') { 
						?>
						<td  colspan="1" width="10%"><?php echo $despesa_adesao['Despesa']['multa']; ?></td>
					<?php }?>
					<td  colspan="1" width="10%">
						<?php 
							if ($despesa_adesao['Despesa']['status'] == 'aberta' || $despesa_adesao['Despesa']['status'] == 'vencida')
								echo "<span style='color:red;'>";
							else if ($despesa_adesao['Despesa']['status'] == 'paga' )
								echo "<span style='color:green;'>";
							else 
								echo "<span style='color:orange;'>";
							echo $despesa_adesao['Despesa']['status']; 
							echo "</span>";
						?>
					</td>
					<td  colspan="1" width="10%"><?php echo $despesa_adesao['Despesa']['parcela'] . ' de ' . $despesa_adesao['Despesa']['total_parcelas']; ?></td>
					<td  colspan="1" width="10%">
						<?php 
							if ($despesa_adesao['Despesa']['status'] == "paga")
								echo date('d/m/Y', strtotime($despesa_adesao['Despesa']['data_pagamento'])); 
							else
								echo "-";
						?>
					</td>					
					<td  colspan="1" width="10%">
						<?php 
							if ($despesa_adesao['Despesa']['status_igpm'] == 'nao_virado')
								echo "-";
							else
								echo $despesa_adesao['Despesa']['correcao_igpm']; 
						?>
					</td>
					<td  colspan="1" width="10%">
						<?php 
							if ($despesa_adesao['Despesa']['status_igpm'] == 'nao_virado')
								echo "-";
							else if ($despesa_adesao['Despesa']['status_igpm'] == 'nao_pago')
								echo "N&atilde;o pago";
							else
								echo "Pago"; 
						?>
					</td>
					<td  colspan="1" width="10%">
					<?php
					/* 
					if ($this->params['action'] == 'planejamento_checkout') {
						if ($despesa_adesao['Despesa']['status'] != 'paga') echo $html->link('Pagar', array($this->params['prefix'] => true, 'controller' => 'formandos', 'action' =>'planejamento_checkout_pagar_despesa', $idDoFormandoProfile, $despesa_adesao['Despesa']['id']), array('class' => 'submit button botao-pagar-despesa'));
						if ($despesa_adesao['Despesa']['status'] != 'cancelada') echo $html->link('Cancelar', array($this->params['prefix'] => true, 'controller' => 'formandos', 'action' =>'planejamento_checkout_cancelar_despesa', $idDoFormandoProfile, $despesa_adesao['Despesa']['id']), array('class' => 'submit button botao-cancelar-despesa'));
						if ($despesa_adesao['Despesa']['status'] == 'cancelada') echo $html->link('Reativar', array($this->params['prefix'] => true, 'controller' => 'formandos', 'action' =>'planejamento_checkout_ativar_despesa', $idDoFormandoProfile, $despesa_adesao['Despesa']['id']), array('class' => 'submit button botao-ativar-despesa'));
					}
					*/
					?>
					</td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>

	<h2 class="grid_full first" style="margin-top: 10px;margin-bottom: 5px;"><b>Resumo financeiro da ades&atilde;o</b></h2>

	<div class="detalhes first grid_full" style="padding-left: 10px;">
		<p class="grid_16 alpha omega first">
			<span class="grid_full alpha omega"> Total do contrato (Valor base + IGPM): R$ <?php echo ($totalDeAdesao['valorBase']);?> + R$ <?php echo ($totalDeAdesao['igpm']);?> = R$ <?php echo ($totalDeAdesao['valorBase'] + $totalDeAdesao['igpm']);?> </span>
		</p>
		<p class="grid_16 alpha omega first">
			<span class="grid_full alpha omega"> Total de despesas pagas (Valor base + IGPM): <span style="color:blue;">R$ <?php echo $totalDeAdesao['totalPagoSemMulta'];?> </span></span>
		</p>
		<p class="grid_16 alpha omega first">
			<span class="grid_full alpha omega"> Total de despesas abertas ou vencidas (Valor base + IGPM): <span style="color:red;">R$ <?php echo $totalDevidoBaseComIGPM;?> </span></span>
		</p>

	</div>
	
	<?php
	// Fim do if que verifica se há despesas com adesão.
	}
	?>
</div>
