<div class="span12 metro" style="position:relative" id="content-container">
    <div class="row-fluid content-loader hide" id="content-loading">
        <div class="cycle-loader"></div>
    </div>
    <div class="row-fluid content-loader hide" id="content-body"></div>
    <div class="row-fluid content-loader" id="content-home">
        <?php if(empty($turmaLogada)) : ?>
        <div class="tile icon bg-color-red"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/turmas/listar') }">
            <div class="tile-content">
                <div class="img icon-light-bulb"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Turmas Gerenciadas</span>
            </div>
        </div>
        <div class="tile icon bg-color-blueDark"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/usuarios/listar') }">
            <div class="tile-content">
                <div class="img icon-users"></div>
            </div>
            <div class="brand">
                <span class="name">Usuários</span>
            </div>
        </div>
        <div class="tile icon bg-color-purple"
             data-bind="click: function(data, event) {
             page('<?=$this->params['prefix'] ?>/faculdades/gerenciar') }">
            <div class="tile-content">
                <div class="img icon-briefcase"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Gerenciar Dados</span>
            </div>
        </div>
        <div class="tile icon bg-color-greenDark"
             data-bind="click: function(data, event) {
             page('<?=$this->params['prefix'] ?>/igpm/listar') }">
            <div class="tile-content">
                <div class="img icon-card"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">IGPM</span>
            </div>
        </div>
        <div class="tile icon bg-color-grayDark"
             data-bind="click: function(data, event) {
             page('<?=$this->params['prefix'] ?>/rifas/listar') }">
            <div class="tile-content">
                <div class="img icon-diamond"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Rifas</span>
            </div>
        </div>
        <div class="tile icon bg-color-brown"
             data-bind="click: function(data, event) {
             page('<?=$this->params['prefix'] ?>/turmas/relatorio_fechamento_novo') }">
            <div class="tile-content">
                <div class="img icon-bars-2"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Relatório Fechamento</span>
            </div>
        </div>
        <?php else : ?>
        <div class="tile icon bg-color-green"
             data-bind="click: function(data, event) {
             page('<?=$this->webroot . $this->params['prefix'] ?>/relatorio_financeiro/exibir') }">
            <div class="tile-content">
                <div class="img icon-calendar-3"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Relatório Financeiro</span>
            </div>
        </div>
        <?php endif; ?>
        <?php if($usuario['Usuario']['nivel'] == 'gerencial') : ?>
        <div class="tile icon bg-color-pinkDark"
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/checkout/relatorio_por_atendente') }">
            <div class="tile-content">
                <span class="img icon-database"></span>
            </div>
            <div class="brand"><span class="name">Checkout Por Atendente</span></div>
        </div>
        <?php endif; ?>
    </div>
</div>