<style type="text/css">
#gerar-restante-igpm,#restante-igpm { margin-bottom:0 }
.tooltip-inner{ max-width: 500px!important; max-height: 500px!important }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        $('.modal-body').tooltip({ selector: '.icon-help'});
        $('#gerar-parcelas-igpm:not(:disabled)').click(function() {
            if ($('#parcelas-igpm').val() == "") {
                $(".parcela-nao-gerada:first span").text("Selecione a quantidade de parcelas");
            } else {
                var that = this;
                $(that).attr('disabled', 'disabled').text("Aguarde...");
                var url = "/<?= $this->params["prefix"] ?>/area_financeira/";
                $.ajax({
                    url: url + "parcelar_igpm/<?= $formando['id'] ?>",
                    data: {parcelas: $('#parcelas-igpm').val()},
                    type: "POST",
                    dataType: "json",
                    async: true,
                    success: function(response) {
                        $.each(response.parcelas, function(i, parcela) {
                            $("<tr>")
                                    .append($("<td>", {text: "R$" + parcela.valor, css: {width: "10%"}}))
                                    .append($("<td>", {text: parcela.linha_digitavel}))
                                    .append($("<td>", {css: {width: "20%"}})
                                    .append($('<a>', {text: "Gerar Boleto", class: "button default mini", target: "_blank", href: url + "gerar_boleto_igpm/" + parcela.codigo}))
                                    ).appendTo($('#tabela-parcelas'));
                        });
                        if ($('#tabela-parcelas').height() > 200)
                            $("#nao-vinculados").height($('#tabela-parcelas').height() + 200);
                        $(".parcela-nao-gerada").fadeOut(500, function() {
                            $('#tabela-parcelas').fadeIn(500, function() {
                                $(".parcela-nao-gerada").remove()
                            });
                        });
                    },
                    error: function() {
                        $(".parcela-nao-gerada:first span").text("Erro ao Gerar Parcelas");
                        $(that).removeAttr('disabled').text("Gerar Parcelas");
                    }
                });
            }
        });
        $('#gerar-restante-igpm:not(:disabled)').click(function() {
            if ($('#restante-igpm').val() == "") {
                $("#resultado-restante-igpm").addClass('fg-color-red')
                        .text("Selecione a quantidade de parcelas");
            } else {
                var that = this;
                $(that).attr('disabled', 'disabled').text("Aguarde...");
                var url = "/<?= $this->params["prefix"] ?>/area_financeira/";
                $.ajax({
                    url: url + "parcelar_restante_igpm/<?= $formando['id'] ?>",
                    data: { parcelas: $('#restante-igpm').val() },
                    type: "POST",
                    dataType: "json",
                    async: true,
                    success: function(response) {
                        $.each(response.parcelas, function(i, parcela) {
                            $("<tr>")
                                .append($("<td>", {text: "R$" + parcela.valor, css: {width: "10%"}}))
                                .append($("<td>", {text: parcela.linha_digitavel}))
                                .append($("<td>", {css: {width: "20%"}})
                                .append($('<a>', {text: "Gerar Boleto", class: "button mini default",
                                    target: "_blank", href: url + "gerar_boleto_igpm/" + parcela.codigo}))
                                ).appendTo($('#tabela-parcelas'));
                        });
                        $("#div-restante-igpm").fadeOut(500, function() {
                            $(this).remove();
                        });
                    },
                    error: function() {
                        $("#resultado-restante-igpm").text("Erro ao Gerar Parcelas");
                        $(that).removeAttr('disabled').text("Gerar Parcelas");
                    }
                });
            }
        });
    });
</script>
<?php if (empty($igpm)) : ?>
    <h2 class="fg-color-red">Despesa de IGPM n&atilde;o encontrada</h2>
    <br />
<?php else : ?>
    <?php $saldoIgpm = $totalPagoIgpm - $igpm['valor'] ?>
    <div class="alert alert-block fade in">
        <a class="close" data-dismiss="alert" href="#"></a>
        <h4 class="alert-heading">importante!</h4>
        <p><?= $textoParcelaIgpm ?></p>
    </div>
    <?php if($totalParcelasIgpm < $igpm['valor'] && !empty($parcelasIgpm)) : ?>
    <div class="alert alert-block alert-info fade in" id='div-restante-igpm'>
        <a class="close" data-dismiss="alert" href="#"></a>
        <h4 class="alert-heading">importante!</h4>
        <p>
            R$<?= number_format($igpm['valor']-$totalParcelasIgpm, 2, ',', '.'); ?> 
            do valor total de IGPM ainda n&atilde;o foram parcelados. 
            Clique no bot&atilde;o abaixo para gerar parcelas do valor restante.
            <br />
            <select id='restante-igpm'>
                <option value="">Selecione</option>
                <?php for ($a = 1; $a <= ($this->params['prefix'] == "atendimento" ? 10 : 3); $a++) : ?>
                    <option value='<?= $a ?>'><?= $a ?></option>
                <?php endfor; ?>
            </select>
            <button type="button" class="button default" id="gerar-restante-igpm">
                Gerar Parcelas
            </button>
            <span id="resultado-restante-igpm"></span>
        </p>
    </div>
    <?php endif; ?>
    <div class="row-fluid">
        <div class="span4">
            <h3>Resumo</h3>
            <br />
            <table class="table table-condensed table-striped">
                <tr>
                    <td class="header">Valor</td>
                    <td><?= "R$" . number_format($igpm['valor'], 2, ',', '.') ?></td>
                </tr>
                <tr>
                    <td class="header">Total Pago</td>
                    <td>R$<?= number_format($totalPagoIgpm, 2, ',', '.') ?></td>
                </tr>
                <tr>
                    <td class="header <?= $saldoIgpm < 0 ? "bg-color-red" : '' ?>">
    <?= $saldoIgpm < 0 ? "Saldo Negativo" : ($saldoIgpm == 0 ? "Saldo Total" : "Saldo Positivo") ?>
                    </td>
                    <td class="<?= $saldoIgpm < 0 ? "strong fg-color-red" : "" ?>">
                        R$<?= number_format(abs($saldoIgpm), 2, ',', '.'); ?><i class="icon-help pull-right" rel="tooltip"
                    title="<?=$textoIGPM?>" data-placement="right"></i></td>
                    </td>
                </tr>
            </table>
        </div>
        <div class="span8">
            <h3>Parcelas</h3>
            <br />
            <?php if (empty($parcelasIgpm)) : ?>
            <p class='parcela-nao-gerada'>
                <strong>Voc&ecirc; ainda n&atilde;o gerou suas parcelas de IGPM</strong>
                <br />
                <span class="strong fg-color-red">&nbsp;</span>
            </p>
            <div class="row-fluid parcela-nao-gerada" style="min-height:300px">
                <div class="span4">
                    <select class="span1" id='parcelas-igpm'>
                        <option value="">Selecione</option>
                        <?php for ($a = 1; $a <= ($this->params['prefix'] == "atendimento" ? 10 : 3); $a++) : ?>
                            <option value='<?= $a ?>'><?= $a ?></option>
                        <?php endfor; ?>
                    </select>
                </div>
                <div class="span8">
                    <button type="button" class="button default" id="gerar-parcelas-igpm">Gerar Parcelas</button>
                </div>
            </div>
            <?php endif; ?>
            <table id="tabela-parcelas"
                   class="table table-striped table-condensed <?= empty($parcelasIgpm) ? "hide" : "" ?>">
                <?php if (!empty($parcelasIgpm)) : ?>
                    <?php foreach ($parcelasIgpm as $parcela) : ?>
                        <tr>
                            <td>
                                <?= "R\${$parcela['Pagamento']['valor_nominal']}" ?>
                            </td>
                            <td>
                                <?php
                                if ($parcela['Pagamento']['status'] == 'pago') {
                                    if (date('Y-m-d H:i:s', strtotime($parcela['Pagamento']['dt_liquidacao'])) != $parcela['Pagamento']['dt_liquidacao'])
                                        echo "Data de pagamento n&atilde;o dispon&iacute;vel";
                                    else
                                        echo "Parcela paga em " . date('d/m/Y', strtotime($parcela['Pagamento']['dt_liquidacao']));
                                } else {
                                    echo $parcela['Pagamento']['linha_digitavel'];
                                }
                                ?>
                            </td>
                            <td>
                            <?php if ($parcela['Pagamento']['status'] == 'aberto') : ?>
                                    <a href="/<?= $this->params['prefix'] ?>/area_financeira/gerar_boleto_igpm/<?= $parcela['Pagamento']['id'] ?>"
                                       class="button mini default" target="_blank">
                                        Gerar Boleto
                                    </a>
                            <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </table>
        </div>
    </div>
<?php endif; ?>
