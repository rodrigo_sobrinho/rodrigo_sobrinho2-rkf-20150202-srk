<?php if($out == "html") : ?>
<?php include('salvar_contrato.ctp')?>
<?php endif; ?>
<h4>ANEXO D - TERMO DE ADESÃO A COBRANÇA DE VALORES REFERENTE A FORMATURA</h4><br /><br />
<p>
Pelo presente instrumento particular de prestação de serviços e na melhor forma de direito, o(a) formando(a) abaixo qualificado
</p>
<br />
Nome: ____________________________________________________ Data Nascimento: ___/___/________<br /><br />
R.G.: ___________ CPF: _ _ _ . _ _ _ . _ _ _ - _ _ Rua/Av: _________________________________________<br /><br />
Núm.: ____ Complemento: _____________ CEP: _ _ _ _ _ - _ _ _ Cidade: ____________________________<br /><br />
Tel Residencial: ( _ _ ) _ _ _ _ - _ _ _ _ Tel Celular: ( _ _ ) _ _ _ _ _ - _ _ _ _ Outro: (_ _) _ _ _ _ _ - _ _ _ _<br /><br />
Email: _____________________________________________(será o seu login no sistema da Contratada)<br /><br />
Senha Acesso: _________(defina senha para o seu 1º acesso http://www.asformaturas.com.br/espaco_formando)<br /><br />
<p style="text-align: justify">
adere ao Contrato de Prestação de Serviços de cobrança de recursos firmado entre a COMISSÃO DE FORMATURA DE SUA TURMA e a 
empresa <b>ZAPE GESTÃO DE VALORES</b>, inscrita no CNPJ/MF sob N.18.405.543/0001-54 , com sede na Avenida José Maria Whitaker, 
882 – CEP 04057-000 - São Paulo/SP, neste ato representada na forma de seu contrato social. Os dados básicos do contrato estão 
especificados abaixo e o mesmo passa a ser considerado como <b>CONTRATO COLETIVO</b>.<br /><br />
</p>
CÓDIGO DO CONTRATO - <b><?=$turma['Turma']['id']?></b> - ESTE CÓDIGO DEVE SER USADO PARA ADESÃO ONLINE - WWW.ASFORMATURAS.COM.BR<br /><br />
DATA DE ASSINATURA DO CONTRATO ENTRE A COMISSÃO DE FORMATURA E A ZAPE GESTÃO DE VALORES <b><?=date("d/m/Y", strtotime($turma['Turma']['data_assinatura_contrato']))?></b><br /><br />
Faculdade: _______________________________________  Curso: _________________________________ <br /><br />
Sala/Turma: ________ Período: __________________<br />
<p style="text-align: justify">
<b>1.</b> O formando acima qualificado reconhece a Comissão de Formatura de sua turma como legítima representante para tratar 
dos assuntos relacionados à cobrança de valores, outorgando poderes para representá-lo em atos referentes a este processo, 
ratificando os termos do <b>CONTRATO PRINCIPAL</b>, declarando ter plenos conhecimentos desse contrato e outorgando poderes para a 
Comissão celebrar aditivos contratuais que se fizerem necessários à realização do serviço objeto do Contrato Principal.<br /><br />
<b>2.</b> As informações importantes a respeito do seu contrato estarão disponibilizadas no link - http://www.asformaturas.com.br/espaco_formando<br /><br />
<b>3.</b> A <b>ZAPE</b> realiza o serviço de cobrança de valores em nome da Comissão de Formatura. Fica a critério da Comissão 
de Formatura, definir se haverá ou não a transferência dos valores arrecadados, seja para fornecedor, seja para outra conta 
corrente ou qualquer outro destino, definindo a forma, periodicidade e outros detalhes no Contrato Principal.<br /><br />
<b>4.</b> Se a Comissão de Formatura optar pela transferência, a ZAPE se isenta da responsabilidade sobre qualquer evento de 
formatura, foto, filmagem, bem como qualquer outra atividade realizada pela Comissão de Formatura ou outras empresas do ramo, 
sendo exclusiva a responsabilidade da ZAPE a cobrança e transferência conforme definido no Contrato Principal. <br /><br />
<b>5.</b> Se a Comissão de Formatura optar que a ZAPE faça a GESTÃO DOS VALORES, as condições estarão definidas no Contrato 
Principal. <br /><br />
<b>E assim por estarem justos e contratados,<br /><br />
</p>
<br />
<br />
<p style="margin-left: 30px">
São Paulo, ______ de ________________ de ________.
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<table border="0" style="width: 500px" align="center">
<tr style="text-align: center">
<td><?=str_pad("",25,"_")?></td>
<td><?=str_pad("",25,"_")?></td>
</tr>
<tr style="text-align: center; border: 0">
<td>CONTRATANTE</td>
<td>ZAPE GESTÃO DE VALORES</td>
</tr>
</table>
</p>