<h2>Resumo</h2>

<table class="table table-striped table-hovered">
    <tbody >
         <tr>
            <td>Evento:</td>
            <td><?php echo $evento ?></td>
        </tr>
        <tr>
            <td>Local:</td>
            <td><?php echo 'Espaço das Americas' ?></td>
        </tr>
        <tr>
            <td>Total de Alunos:</td>
            <td><?php echo $totalFormandos ?></td>
        </tr>
        <!-- <tr> -->
            <!-- <td>Total de Mesas:</td> -->
            <!-- <td><?php echo '30' ?></td> -->
        <!-- </tr> -->
        <!-- <tr> -->
            <!-- <td>Total de Mesas Escolhidas:</td> -->
            <!-- <td><?php echo '5' ?></td> -->
        <!-- </tr> -->
    </tbody>
</table>

<h2>Agendamento da comissão</h2>

<table class="table table-striped table-hovered">
    <thead>
        <td >Codigo Formando</td>
        <td >Nome</td>
        <td >Data adesão</td>
        <td >Data liberação</td>
        <td >Status</td>
        <td >Posição da mesa</td>
    </thead>
    <tbody >
        <?php foreach ($comissao as $formando) : ?>
            <tr>
                <td><?php echo $formando['Formandos']['codigo_formando'] ?></td>
                <td><?php echo $formando['Usuario']['nome'] ?></td>
                <td><?php echo $formando['data_adesao'] ?></td>
                <td><?php echo $formando['data_agendada'] ?></td>
                <td><?php echo $formando['status'] ?></td>
                <td><?php echo $formando['referencias'] ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<h2>Lista de Formandos Agendados</h2>

<table class="table table-striped table-hovered">
    <thead>
        <td >Codigo Formando</td>
        <td >Nome</td>
        <td >Data adesão</td>
        <td >Data liberação</td>
        <td >Status</td>
        <td >Posição da mesa</td>
    </thead>
    <tbody >
        <?php foreach ($formandos as $formando) : ?>
            <tr>
                <td><?php echo $formando['Formandos']['codigo_formando'] ?></td>
                <td><?php echo $formando['Usuario']['nome'] ?></td>
                <td><?php echo $formando['data_adesao'] ?></td>
                <td><?php echo $formando['data_agendada'] ?></td>
                <td><?php echo $formando['status'] ?></td>
                <td><?php echo $formando['referencias'] ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>