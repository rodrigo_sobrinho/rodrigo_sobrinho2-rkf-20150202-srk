<?php

/*
  @author Vinicius Alves Oyama
  Component estatico que retorna o menu do sidebar
 */

class ListaSidebarMenuComponent extends Object {

    var $uses = array('Item', 'Cronograma');
    //atributos correspondentes a sidebar
    static private $configuracaoSidebarMenu = array(
        "titulo" => "Menu",
        "links" => array(),
        "sublinks" => array());

    static function obterSidebar($templateSidebar, $grupo, $turmaLogada = false) {

        switch (strtolower($grupo)) {
            case 'super':
                if($turmaLogada){
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Relatório Financeiro', 'relatorio_financeiro', 'exibir', $grupo,'','',true);
                }else{
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Turmas', 'turmas', 'listar', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Usuários', 'usuarios', 'listar', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Gerenciar Dados', 'faculdades', 'gerenciar', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('IGPM', 'igpm', 'listar', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Rifas', 'rifas', 'listar', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Relatório Fechamento', 'turmas', 'relatorio_fechamento_novo', $grupo,'','',true);
                }
            break;
            case 'formando':
                ListaSidebarMenuComponent::adicionarLinkSidebar('Editar Dados', 'usuarios', 'editar_dados', $grupo,'','',true);
                ListaSidebarMenuComponent::adicionarLinkSidebar('Eventos', 'eventos', 'listar', $grupo,'','',true);
                ListaSidebarMenuComponent::adicionarLinkSidebar('Campanhas', 'campanhas', 'listar', $grupo,'','',true);
                ListaSidebarMenuComponent::adicionarLinkSidebar('Área Financeira', 'area_financeira', 'dados/geral', $grupo,'','',true);
                //ListaSidebarMenuComponent::adicionarLinkSidebar('Clube ÁS', 'parcerias', 'index', $grupo,'','',true);
                ListaSidebarMenuComponent::adicionarLinkSidebar('Contrato', 'contratos', 'exibir', $grupo,'','',true);
                ListaSidebarMenuComponent::adicionarLinkSidebar('Rifa Online', 'rifas', 'cupons', $grupo,'','',true);
            break;
            case 'rh':
                if($turmaLogada){
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Mensagens', 'mensagens', 'cronograma', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Arquivos', 'arquivos', 'listar', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Aniversariantes do mês', 'usuarios', 'listar_aniversariantes', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Manuais Procedimento', 'arquivos', 'manuais_procedimento', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Funcionários', 'usuarios', 'listar', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Novidades', 'usuarios', 'novidades', $grupo,'','',true);
                }else{
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Intranet', 'turmas', 'selecionar/13', $grupo,'','',true);
                }
            break;
            case 'video':
                if($turmaLogada){
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Mensagens', 'mensagens', 'cronograma', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Fotos Telão', 'formandos', 'fotos_telao_listar', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Arquivos', 'arquivos', 'listar', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Aniversariantes do mês', 'usuarios', 'listar_aniversariantes', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Manuais Procedimento', 'arquivos', 'manuais_procedimento', $grupo,'','',true);
                }else{
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Turmas', 'turmas', 'listar', $grupo,'','',true);
                }
                ListaSidebarMenuComponent::adicionarLinkSidebar('Funcionários', 'usuarios', 'listar', $grupo,'','',true);
                ListaSidebarMenuComponent::adicionarLinkSidebar('Novidades', 'usuarios', 'novidades', $grupo,'','',true);
            break;
            case 'criacao':
                if($turmaLogada){
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Mensagens', 'mensagens', 'cronograma', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Arquivos', 'arquivos', 'listar', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Aniversariantes do mês', 'usuarios', 'listar_aniversariantes', $grupo,'','',true);
                ListaSidebarMenuComponent::adicionarLinkSidebar('Manuais Procedimento', 'arquivos', 'manuais_procedimento', $grupo,'','',true);
                }else{
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Turmas', 'turmas', 'listar', $grupo,'','',true);
                }
                ListaSidebarMenuComponent::adicionarLinkSidebar('Funcionários', 'usuarios', 'listar', $grupo,'','',true);
                ListaSidebarMenuComponent::adicionarLinkSidebar('Novidades', 'usuarios', 'novidades', $grupo,'','',true);
            break;
            case 'foto':
                if($turmaLogada){
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Mensagens', 'mensagens', 'cronograma', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Arquivos', 'arquivos', 'listar', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Aniversariantes do mês', 'usuarios', 'listar_aniversariantes', $grupo,'','',true);
                ListaSidebarMenuComponent::adicionarLinkSidebar('Manuais Procedimento', 'arquivos', 'manuais_procedimento', $grupo,'','',true);
                }else{
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Turmas', 'turmas', 'listar', $grupo,'','',true);
                }
                ListaSidebarMenuComponent::adicionarLinkSidebar('Funcionários', 'usuarios', 'listar', $grupo,'','',true);
                ListaSidebarMenuComponent::adicionarLinkSidebar('Novidades', 'usuarios', 'novidades', $grupo,'','',true);
            break;
            case 'producao':
                if($turmaLogada){
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Mensagens', 'mensagens', 'cronograma', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Arquivos', 'arquivos', 'listar', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Aniversariantes do mês', 'usuarios', 'listar_aniversariantes', $grupo,'','',true);
                ListaSidebarMenuComponent::adicionarLinkSidebar('Manuais Procedimento', 'arquivos', 'manuais_procedimento', $grupo,'','',true);
                }else{
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Turmas', 'turmas', 'listar', $grupo,'','',true);
                }
                ListaSidebarMenuComponent::adicionarLinkSidebar('Funcionários', 'usuarios', 'listar', $grupo,'','',true);
                ListaSidebarMenuComponent::adicionarLinkSidebar('Novidades', 'usuarios', 'novidades', $grupo,'','',true);
            break;
            case 'planejamento':
                if($turmaLogada){
                       // ListaSidebarMenuComponent::adicionarLinkSidebar('Informações', 'turmas', 'info', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Formandos', 'formandos', 'listar', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Cronograma', 'mensagens', 'cronograma', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Fotos Telão', 'formandos', 'fotos_telao_listar', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Rifas Formandos', 'formandos', 'rifas_listar', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Parcelamentos', 'parcelamentos', 'listar', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Comissão', 'comissoes', 'listar', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarSubLinkSidebar('Comissão', 'Comissões','comissoes', 'listar', $grupo,true);
                        ListaSidebarMenuComponent::adicionarSubLinkSidebar('Comissão','Tracking da comissão', 'tracking', 'exibir', $grupo,true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Relatórios', 'relatorio_financeiro', 'exibir', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarSubLinkSidebar('Relatórios','Relatório Financeiro', 'relatorio_financeiro', 'exibir', $grupo,true);
                        ListaSidebarMenuComponent::adicionarSubLinkSidebar('Relatórios','Relatório de Mesas e Convites', 'turmas', 'relatorio_entrega', $grupo);
                        ListaSidebarMenuComponent::adicionarSubLinkSidebar('Relatórios','Relatório de Checkout Por Atendente', 'checkout', 'relatorio_por_atendente', $grupo,true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Eventos', 'eventos', 'listar', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Campanhas', 'campanhas', 'listar', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Contratos', 'contratos', 'listar', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Arquivos', 'arquivos', 'listar', $grupo,'','',true);
                }else{
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Turmas', 'turmas', 'listar', $grupo,'','',true);
                }

          /*  case 'planejamento':
                switch (strtolower($templateSidebar)) {
                    case 'campanhas':
                    case 'extras':
                    case 'mensagens':
                    case 'itens':
                    case 'assuntos':
                    case 'turmas':
                    case 'eventos':
                    case 'cronogramas':
                    case 'parcelamentos':
                    case 'comissoes':
                    case 'contratos':
                    case 'formandos':
                    case 'entrega':
                    case 'relatorio_financeiro':
                    case 'checkout':
                        ListaSidebarMenuComponent::setTituloSidebar('Turma');
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Informações', 'turmas', 'visualizar', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Agenda de Turma', 'turmas', 'agenda', $grupo,true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Formandos', 'formandos', 'index', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Cronograma', 'cronogramas', 'index', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Parcelamento', 'parcelamentos', 'index', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Comissão', 'comissoes', 'index', $grupo,true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Comissão','Tracking da comissão', 'tracking', 'index', $grupo,true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Relatórios','Relatório Financeiro', 'relatorio_financeiro', 'index', $grupo,true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Relatórios','Relatório de Extras', 'extras', 'relatorio', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Relatórios','Relatório de Checkout', 'extras', 'relatorio_checkout', $grupo,true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Eventos', 'eventos', 'index', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Campanhas', 'campanhas', 'index', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Contratos', 'contratos', 'index', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Mensagens', 'itens', 'index', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Arquivos', 'arquivos', 'index', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Itens Checkout', 'checkout', 'itens', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Resumo Entrega', 'entrega', 'resumo_itens', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Relatório Vendas', 'extras', 'relatorios_vendas', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Vincular Pagamentos', 'vincular_pagamentos', 'index', $grupo,'','',true);
                        //TODO !!!
                        //ListaSidebarMenuComponent::adicionarLinkSidebar('Valores e convites', 'TODO', 'TODO', $grupo);
                        break;
                    case 'lembretes':
                        ListaSidebarMenuComponent::setTituloSidebar('Lembretes');
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Principal', 'principal', 'index', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Turmas', 'turmas', 'index', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Lembretes', 'lembretes', 'index', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Mensagens', 'itens', 'index', $grupo);
                        break;
                    case 'principal':
                        ListaSidebarMenuComponent::setTituloSidebar('Principal');
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Informações', 'turmas', 'visualizar', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Agenda de Turma', 'turmas', 'agenda', $grupo,true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Formandos', 'formandos', 'index', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Cronograma', 'cronogramas', 'index', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Parcelamento', 'parcelamentos', 'index', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Comissão', 'comissoes', 'index', $grupo,true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Comissão','Tracking da comissão', 'tracking', 'index', $grupo,true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Relatórios','Relatório Financeiro', 'relatorio_financeiro', 'index', $grupo,true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Relatórios','Relatório de Extras', 'extras', 'relatorio', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Relatórios','Relatório de Checkout', 'extras', 'relatorio_checkout', $grupo,true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Eventos', 'eventos', 'index', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Campanhas', 'campanhas', 'index', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Contratos', 'contratos', 'index', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Mensagens', 'itens', 'index', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Arquivos', 'arquivos', 'index', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Itens Checkout', 'checkout', 'itens', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Resumo Entrega', 'entrega', 'resumo_itens', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Relatório Vendas', 'extras', 'relatorios_vendas', $grupo,'','',true);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Vincular Pagamentos', 'vincular_pagamentos', 'index', $grupo,'','',true);
                        break;
                    case 'agendas':
                        ListaSidebarMenuComponent::setTituloSidebar('Agenda');
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Principal', 'principal', 'index', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Turmas', 'turmas', 'index', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Lembretes', 'lembretes', 'index', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Mensagens', 'itens', 'index', $grupo);
                        break;
                    default:
                        ListaSidebarMenuComponent::setTituloSidebar('Home');
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Formandos', 'formandos', 'index', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Turmas', 'turmas', 'index', $grupo);
                        break;
                }
                */
                break;
                
            case 'comissao':
                ListaSidebarMenuComponent::setTituloSidebar('Principal');
                ListaSidebarMenuComponent::adicionarLinkSidebar('Comissão', 'comissoes', 'listar', $grupo,'','',true);
                ListaSidebarMenuComponent::adicionarLinkSidebar('Adesao', 'formandos', 'listar', $grupo,'','',true);
                ListaSidebarMenuComponent::adicionarSubLinkSidebar('Adesao', 'Formandos', 'formandos', 'listar', $grupo,true);
                ListaSidebarMenuComponent::adicionarSubLinkSidebar('Adesao', 'Relatório Financeiro', 'relatorio_financeiro', 'exibir', $grupo,true);
                ListaSidebarMenuComponent::adicionarSubLinkSidebar('Adesao', 'Parcelamentos', 'parcelamentos', 'listar', $grupo,true);
                ListaSidebarMenuComponent::adicionarSubLinkSidebar('Adesao', 'Fazer a Adesão', 'formandos', 'aderir', $grupo,true);
                ListaSidebarMenuComponent::adicionarLinkSidebar('Cronograma', 'mensagens', 'cronograma', $grupo,'','', true);
                ListaSidebarMenuComponent::adicionarLinkSidebar('Eventos', 'eventos', 'listar', $grupo,'','',true);
                ListaSidebarMenuComponent::adicionarLinkSidebar('Loja', 'campanhas', 'listar', $grupo,'','',true);
                ListaSidebarMenuComponent::adicionarLinkSidebar('Arquivos', 'arquivos', 'listar', $grupo,'','',true);
                ListaSidebarMenuComponent::adicionarSubLinkSidebar('Arquivos', 'Contratos', 'contratos', 'listar', $grupo,true);
                ListaSidebarMenuComponent::adicionarSubLinkSidebar('Arquivos', 'Anexos', 'arquivos', 'listar', $grupo, true);
                break;


            // Links para o grupo comercial
            case 'comercial':
                if($turmaLogada) {
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Membros da Comissão', 'comissoes',
                            'listar', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Registro Interno', 'registros_internos',
                            'listar', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Tracking da comissão', 'tracking',
                            'exibir', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Cronograma', 'mensagens',
                            'cronograma', $grupo,'','',true);
                    //ListaSidebarMenuComponent::adicionarLinkSidebar('Contrato', 'contratos', 'gerar', $grupo);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Relatório Financeiro',
                            'relatorio_financeiro', 'exibir', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Arquivos', 'arquivos', 'listar',
                            $grupo,'','',true);
                } else {
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Turmas', 'turmas', 'listar', $grupo,'','',true);
                }
                /*
                switch (strtolower($templateSidebar)) {
                    case 'principal':
                    case 'lembretes':
                        ListaSidebarMenuComponent::setTituloSidebar('Home');
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Principal', 'principal', 'index', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Turmas', 'turmas', 'index', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Mensagens', 'itens', 'index', $grupo);
                        break;
                    case 'turmas':
                        ListaSidebarMenuComponent::setTituloSidebar('Turma');
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Turmas gerenciadas', 'turmas', 'index', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Informações', 'turmas', 'visualizar', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Agenda de Turma', 'turmas', 'agenda', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Comissão', 'comissoes', 'index', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Registro Interno', 'registros_internos', 'index', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Tracking da comissão', 'tracking', 'index', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Mensagens', 'itens', 'index', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Relatório Financeiro', 'relatorio_financeiro', 'index', $grupo);
//                        ListaSidebarMenuComponent::adicionarLinkSidebar('Orçamento', 'orcamento', '', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Contrato', 'contratos', 'gerar', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Plano Cascata', 'area_financeira', 'plano_cascata', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Arquivos', 'arquivos', 'index', $grupo);

                        break;
                    case 'Areafinanceira':
                    case 'comissoes':
                    case 'itens':
                    case 'mensagens':
                    case 'assuntos':
                    case 'contratos':
                    case 'mensagens':
                        ListaSidebarMenuComponent::setTituloSidebar('Turma');
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Turmas gerenciadas', 'turmas', 'index', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Informações', 'turmas', 'visualizar', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Agenda de Turma', 'turmas', 'agenda', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Comissão', 'comissoes', 'index', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Registro Interno', 'registros_internos', 'index', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Tracking da comissão', 'tracking', 'index', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Mensagens', 'itens', 'index', $grupo);
//                        ListaSidebarMenuComponent::adicionarLinkSidebar('Orçamento', 'orcamento', '', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Contrato', 'contratos', 'gerar', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Arquivos', 'arquivos', 'index', $grupo);
                        break;
                    case 'agendas':
                        ListaSidebarMenuComponent::setTituloSidebar('Agenda');
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Calendario', 'agendas', 'index', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Eventos', 'agendas', 'lista', $grupo);
                        break;

                    case 'assuntos':
                        App::import('Model', 'Item');
                        $itemModel = new Item();

                        ListaSidebarMenuComponent::setTituloSidebar('Itens');
                        foreach ($itemModel->find('all', array('conditions' => array('Item.grupo' => 'comercial'))) as $item) {
                            ListaSidebarMenuComponent::adicionarLinkSidebar($item['Item']['nome'], 'itens', 'visualizar/' . $item['Item']['id'], $grupo);
                        }
                        break;
                    default:
                        ListaSidebarMenuComponent::setTituloSidebar('Agenda');
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Calendario', 'agendas', 'index', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Eventos', 'agendas', 'lista', $grupo);
                        break;
                }
                */
                break;


            case 'financeiro':
                switch (strtolower($templateSidebar)) {
                    case 'principal':
                    case 'igpm':
                    default:
                        ListaSidebarMenuComponent::setTituloSidebar('Home');
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Principal', 'principal', 'index', $grupo);
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Pagamentos', 'pagamentos', 'enviar_planilha', $grupo);
                        //ListaSidebarMenuComponent::adicionarLinkSidebar('Boletos Antigos', 'pagamentos', 'boletos_antigos', $grupo);
                        break;
                }
                break;
            case 'marketing':
                ListaSidebarMenuComponent::setTituloSidebar('Home');
                ListaSidebarMenuComponent::adicionarLinkSidebar('Parceiros', 'parceiros', 'index', $grupo);
                ListaSidebarMenuComponent::adicionarLinkSidebar('Parcerias', 'parcerias', 'index', $grupo);
                ListaSidebarMenuComponent::adicionarLinkSidebar('Categorias', 'categorias', 'index', $grupo);
                break;
            case 'atendimento':
                if($turmaLogada){
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Relatório Financeiro', 'relatorio_financeiro', 'exibir', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Formandos', 'formandos', 'lista_excel', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Mensagens', 'mensagens', 'email', false,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Campanhas', 'campanhas', 'listar', $grupo,'','',true);
                }else{
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Formandos', 'formandos', 'listar', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Turmas', 'turmas', 'listar', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Todos Formandos', 'usuarios', 'listar', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Mensagens', 'mensagens', 'email', false,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Formandos', 'formandos', 'listar', $grupo,'','',true);
                    ListaSidebarMenuComponent::adicionarLinkSidebar('Agenda', 'calendario', 'exibir', false,'','',true);
                }
            break;
            case 'vendedor':
                ListaSidebarMenuComponent::setTituloSidebar('Vendedor');
                ListaSidebarMenuComponent::adicionarLinkSidebar('Lista de Vendas', 'pagmidas', 'transacoes', $grupo);
                ListaSidebarMenuComponent::adicionarLinkSidebar('Nova Venda', 'pagmidas', 'index', $grupo);
                break;
            default:
                switch ($templateSidebar) {
                    default:
                        ListaSidebarMenuComponent::adicionarLinkSidebar('Sem link', 'controller', 'action', $grupo);
                        break;
                }
                break;
        }

        return ListaSidebarMenuComponent::$configuracaoSidebarMenu;
    }

    static function setTituloSidebar($textoDoTitulo = 'titulo') {
        ListaSidebarMenuComponent::$configuracaoSidebarMenu['titulo'] = utf8_encode($textoDoTitulo);
    }

    static function adicionarLinkSidebar($textoDoLink = '',
            $controllerDoLink = 'controller', $actionDoLink = 'action',
            $grupo = 'grupo', $class = "", $after = "",$novoLayout = false) {
        array_push(ListaSidebarMenuComponent::$configuracaoSidebarMenu['links'], array(
            'grupo' => $grupo,
            'texto' => utf8_encode($textoDoLink),
            'controller' => utf8_encode($controllerDoLink),
            'action' => utf8_encode($actionDoLink),
            'class' => $class,
            'after' => $after,
            'novoLayout' => $novoLayout
        ));
    }

    static function adicionarSubLinkSidebar($itemPai = 'itemPai', $textoDoLink = 'texto', $controllerDoLink = 'controller',
            $actionDoLink = 'action', $grupo = 'grupo',$novoLayout = false) {
        if (!array_key_exists($itemPai, ListaSidebarMenuComponent::$configuracaoSidebarMenu['sublinks'])) {
            ListaSidebarMenuComponent::$configuracaoSidebarMenu['sublinks'][$itemPai] = array();
        }

        array_push(ListaSidebarMenuComponent::$configuracaoSidebarMenu['sublinks'][$itemPai], array(
            'grupo' => $grupo,
            'texto' => utf8_encode($textoDoLink),
            'controller' => utf8_encode($controllerDoLink),
            'action' => utf8_encode($actionDoLink),
            'novoLayout' => $novoLayout
        ));
    }

    static function retornaSideBar() {
        return ListaSidebarMenuComponent::$configuracaoSidebarMenu;
    }

}

?>
