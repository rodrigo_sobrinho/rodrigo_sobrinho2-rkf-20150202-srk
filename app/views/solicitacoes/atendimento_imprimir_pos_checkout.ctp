<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/wizard.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/max/bootstrap/wizard.js"></script>
<style type="text/css">
    @media print {
        body * { background: none!important; position: static!important; }
        #footer-body { display: none }
        #menu-topo { display: none }
        .area_print, .area_print * { visibility: visible !important; }
        .area_print { position: absolute !important; left: 0!important; top: 0px!important; }
        h5 {font-size: 14px!important }
        h4 {font-size: 12px!important }
    }
</style>
<script type="text/javascript">
    $('#wizard').wizard();
</script>
<div class="row-fluid">
    <div class="span9">
        <div id="wizard" class="wizard">
            <ul class="steps">
                <li data-target="#step1" class="active">
                    Protocolo de Atendimento / Turma - <?=$formando['turma_id']?>
                    <span class="chevron"></span>
                </li>
            </ul>
        </div>
        <div class="step-content">
            <div class="row-fluid">
                <img src="<?= empty($formando['diretorio_foto_perfil']) ? "{$this->webroot}img/uknown_user.gif" : "{$this->webroot}{$formando['diretorio_foto_perfil']}" ?>"
                     style='float:left; margin:0 5px 15px 0; height: 170px; width:150px' />
                <p>
                    <h5 class="fg-color-red"><strong><?= strtoupper($formando['nome']) ?></strong></h5>
                    <br />
                    <h5><?=strtoupper($formando['grupo']) ?></h5>
                    <br />
                    <h5>Código: <?= $formando['codigo_formando'] ?></h5>
                    <br />
                    <h5>RG: <?= $formando['rg'] ?></h5>
                    <br />
                    <h5>Protocolo: <?= $protocolo['Protocolo']['protocolo'] ?></h5>
                </p>
            </div>
            <br />
                <?php if(!empty($pagamentos)) : ?>
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <td colspan="6" style="text-align: center"><h4>Pagamentos Recebidos Durante o Pós Checkout</h4></td>
                        </tr>
                        <tr>
                            <td>Valor</td>
                            <td>Atendente</td>
                            <td>Tipo</td>
                            <td>Referência</td>
                            <td>Vencimento</td>
                            <td>Data Cadastro</td>
                        </tr>
                    </thead>
                    <?php foreach ($pagamentos as $pagamento) : ?>
                    <tr>
                        <td>R$<?=$pagamento['valor'];?></td>
                        <td><?=$pagamento['atendente'];?></td>
                        <td><?=$pagamento['tipo'];?></td>
                        <td><?=$pagamento['referente'];?></td>
                        <td><?=$pagamento['vencimento'];?></td>
                        <td><?=$pagamento['data_cadastro'];?></td>
                    </tr>
                    <?php endforeach; ?>
                </table>
                <?php endif; ?>
            <br />
            <div class="row-fluid">
                <h4>
                        A AS Formaturas confirma que, o formando (a) <em class="fg-color-gray"><strong><?= $formando['nome'] ?></strong></em>,
                        realizou o(s) pagamento(s) listado(s) acima e retirou os convite(s)/mesa(s) de direito, durante o pós checkout:
                    </h4>
            </div>
            <br />
            <div class="row-fluid">
                <?php if (count($itens) > 0) : ?>
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <td colspan="3" style="text-align: center"><h4>Itens retirados pós checkout</h4></td>
                        </tr>
                        <tr>
                            <td>Atendente</td>
                            <td>Descrição</td>
                            <td>Data Cadastro</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($itens as $item) : ?>
                        <tr>
                            <td><?= $item['atendente'] ?><br />&nbsp;</td>
                            <td><?= $item['descricao'] ?><br />&nbsp;</td>
                            <td><?= $item['data_cadastro'] ?><br />&nbsp;</td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>