<span id="conteudo-titulo" class="box-com-titulo-header">Formando - Editar dados</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div style="clear:both;"></div>
	<?php echo $form->create('Usuario', array('url' => "/{$this->params['prefix']}/usuarios/editar", 'style' => 'display:block;')); ?>
	<?php include('_form_formando.ctp'); ?>
	<?=$form->hidden('Usuario.id', array('value' => $uid)); ?>
	<p class="grid_16 alpha omega">
		<?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit'));?>
	</p>
	
	<div style="clear:both;"></div>
</div>