<?php

class MaterialVenda extends AppModel {

    var $name = 'MaterialVenda';
    var $hasMany = array(
        'MaterialVendaFoto' => array(
            'className' => 'MaterialVendaFoto',
            'order' => 'ordem'
        ),
        'MaterialVendaAnexo',"MaterialVendaEventoItem");
    
    function listarCatalogosDeItem($itemId) {
        $conditions = array(
            "MaterialVendaEventoItem.material_venda_item_id" => $itemId
        );
        return $this->listarCatalogos($conditions);
    }
    
    function listarCatalogos($conditions) {
        $this->unbindModelAll();
        $this->bindModel(array(
            "hasMany" => array(
                "MaterialVendaFoto" => array(
                    'className' => 'MaterialVendaFoto',
                    'order' => 'MaterialVendaFoto.ordem asc'
                ),
            )
        ),false);
        $options = array(
            'joins' => array(
                array(
                    "table" => "material_venda_eventos_itens",
                    "type" => "inner",
                    "alias" => "MaterialVendaEventoItem",
                    "conditions" => array(
                        "MaterialVenda.id = MaterialVendaEventoItem.material_venda_id"
                    )
                ),
                array(
                    "table" => "material_venda_itens",
                    "type" => "inner",
                    "alias" => "MaterialVendaItem",
                    "conditions" => array(
                        "MaterialVendaItem.id = MaterialVendaEventoItem.material_venda_item_id"
                    )
                ),
                array(
                    "table" => "tipos_eventos",
                    "type" => "inner",
                    "alias" => "TiposEvento",
                    "conditions" => array(
                        "TiposEvento.id = MaterialVendaItem.tipos_evento_id",
                        "TiposEvento.ativo = 1"
                    )
                ),
            ),
            'conditions' => $conditions,
            'fields' => array("MaterialVenda.*")
        );
        return $this->find('all',$options);
    }

}