<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css?v=0.1">
<script type="text/javascript">
$(document).ready(function() {
    var textarea = "<textarea id='justificativa' rows='8' style='width: 834px'></textarea><br /><br />";
    var button = "<button type='button' class='button bg-color-greenDark enviar' dir='<?=$formando['FormandoProfile']['usuario_id']?>'>Enviar</button>";
    
    $('.cancelar').on('click', function(){
        var saldo = $('#saldo-cancelamento').val();
        $(".modal-body").html('<h2>Carregando</h2>');
        var url = "area_financeira/cancelar/";
        $.ajax({
            url: url,
            data: { usuario : <?=$formando['FormandoProfile']['usuario_id']?>, despesa : saldo },
            type: "POST",
            dataType: "json",
            success: function(data) {
                if(data.error){
                    $('.modal-body').html(data.mensagem);
                    $('.modal-body').append(textarea);
                    $('.modal-body').append(button);
                }else{
                    var html = "<h4 class='fg-color-blueDark'>Caro formando, o cancelamento do contrato gerou um protocolo, <span style='color:red'>" + data.protocolo + "</span></h4>.<br />";
                    if($('#saldo-cancelamento').val() > 10) {
                            html+= "Clique no bot&atilde;o abaixo para gerar boleto do saldo devedor gerado.<br /><br />";
                            html+= '<?=$html->link('Gerar Boleto Cancelamento',array($this->params['prefix'] => true, 'action' => 'gerar_boleto_cancelamento',$formando['FormandoProfile']['usuario_id']),array('class' => 'button mini bg-color-red botao-gerar-boleto-cancelamento', 'target' => '_blank')); ?>';
                            html+= "<br />";
                    } else if($('#saldo-cancelamento').val() < 0) {
                            html+= "Voc&ecirc; tem um cr&eacute;dito de R$" + ($('#saldo-cancelamento').val()*-1).toFixed(2) + "<br />";
                            html+= "Clique no bot&atilde;o abaixo e preencha seus dados banc&aacute;rios para depositarmos seu cr&eacute;dito.<br /><br />";
                            html+= '<a href="/<?=$this->params['prefix']?>/solicitacoes/visualizar/' + data.protocolo + '" class="button mini bg-color-orangeDark" target="_blank">Dados Financeiros</a>';
                            html+= "<br />";
                    } else {
                            html+= "<h5 class='fg-color-orangeDark'>Clique <a href='/<?=$this->params['prefix']?>/solicitacoes/visualizar/" + data.protocolo + "' target='_blank' class='button mini bg-color-orangeDark'>aqui</a> para visualizar o status do cancelamento</h5>";
                    }
                    html+= "<br />Consulte o status do cancelamento usando o protocolo gerado no link \"Solicita&ccedil;&otilde;es\" na sua &Aacute;rea de formando.";
                    $('.modal-body').html(html);
                }
            }
        });
    });
    $(".modal-body").on('click', '.enviar', function(e){
        e.preventDefault();
        var text = $('modal-body').find("textarea");
        dir = $(this).attr('dir');
        var url = '<?="/{$this->params['prefix']}/area_financeira/email_justificativa/"?>'+dir;
            $.ajax({
                url: url,
                data: { data : $('#justificativa').val() },
                type: "POST",
                dataType: "json",
                complete: function() {
                    $('.modal-body').html("<h2 class='fg-color-greenDark'>Justificativa enviada com sucesso.</h2>");
                }
            });
    });
});
</script>
<?php $session->flash(); ?>
<div class="row-fluid">
    <div class="span8">
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th scope="col">Resumo Financeiro</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="1">Valor do Contrato</td>
                    <td colspan="1"><?=$formando['FormandoProfile']['valor_adesao']?></td>
                </tr>
                <tr>
                    <td colspan="1">Quantidade de Parcelas</td>
                    <td colspan="1"><?=$formando['FormandoProfile']['parcelas_adesao']?></td>
                </tr>
                <tr>
                    <td colspan="1">Total Pago</td>
                    <td colspan="1">R$ <?=$totalPago?></td>
                </tr>
                <tr>
                    <td colspan="1">Saldo em Aberto</td>
                    <td colspan="1">R$ <?=$saldoEmAberto?></td>
                </tr>
                <tr>
                    <td colspan="1">Multa por Cancelamento (20% Valor do Contrato)</td>
                    <td colspan="1">R$ <?=$formando['FormandoProfile']['valor_adesao']*0.2?></td>
                </tr>
                <tr>
                    <?php $saldoCancelamento = ($formando['FormandoProfile']['valor_adesao']*0.2) - $totalPago; ?>
                    <?php if($saldoCancelamento < 0) :?>
                    <?php $saldoPositivo = $saldoCancelamento * -1; ?>
                    <td colspan="1">Saldo Positivo</td>
                    <td colspan="1" style="color: green">R$ <?=$saldoPositivo?></td>
                    <input type='hidden' id='saldo-cancelamento' value='<?=$saldoPositivo?>' />
                    <?php else : ?>
                    <?php $saldoNegativo = $saldoCancelamento * 1; ?>
                    <td colspan="1">Saldo Negativo</td>
                    <td colspan="1" style="color: red">R$ <?=$saldoNegativo?></td>
                    <input type='hidden' id='saldo-cancelamento' value='<?=$saldoNegativo?>' />
                    <?php endif; ?>
                </tr>
            </tbody>
        </table>
        <br />
        <button class="button mini bg-color-red cancelar"
            href="/area_financeira/cancelar/<?=$formando['FormandoProfile']['usuario_id']?>">
            Cancelar
        </button>
    </div>
</div>
