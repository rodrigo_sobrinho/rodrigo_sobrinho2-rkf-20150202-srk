<div class="row-fluid">
    <?php if($usuario) { ?>
    <h2 class="fg-color-red"><?=$usuario['Usuario']['nome']?></h2>
    <br />
    <div class="row-fluid">
        <div class="span6">
            <h5>Email</h5>
            <h4><em>
                <?=$usuario['Usuario']['email']?>
            </em></h4>
        </div>
        <div class="span6">
            <h5>RG</h5>
            <h4><em>
                <?=$usuario['FormandoProfile']['rg']?>
            </em></h4>
        </div>
    </div>
    <br />
    <div class="row-fluid">
        <div class="span6">
            <h5>Tel Res</h5>
            <h4><em>
                <?=$usuario['FormandoProfile']['tel_residencial']?>
            </em></h4>
        </div>
        <div class="span6">
            <h5>Tel Com</h5>
            <h4><em>
                <?=$usuario['FormandoProfile']['tel_comercial']?>
            </em></h4>
        </div>
    </div>
    <br />
    <div class="row-fluid">
        <div class="span6">
            <h5>Cargo</h5>
            <h4><em>
                <?=$usuario['FormandoProfile']['cargo_comissao']?>
            </em></h4>
        </div>
    </div>
    <?php } else { ?>
    <h2 class="fg-color-red">Usuário Não Encontrado</h2>
    <?php } ?>
</div>