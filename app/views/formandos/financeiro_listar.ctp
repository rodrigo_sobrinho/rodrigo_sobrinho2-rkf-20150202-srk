<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.selectpicker').selectpicker({width:'100%'});
    $("#form-filtro").submit(function(e){
        e.preventDefault();
        var context = ko.contextFor($(".metro-button.reload")[0]);
        context.$data.showLoading(function() {
            $("#form-filtro").find('[value="false"]').attr('disabled','disabled');
            var dados = $("#form-filtro").serialize();
            var url = $("#form-filtro").attr('action');
            $.ajax({
                url : url,
                data : dados,
                type : "POST",
                dataType : "json",
                complete : function() {
                    context.$data.reload();
                }
            });
        });
    });
    $(".marcar-envio").click(function(e) {
        e.preventDefault();
        var context = ko.contextFor($(".metro-button.reload")[0]),
            url = $(this).attr('href');
        context.$data.showLoading(function() {
            $.getJSON(url).always(function() {
                context.$data.reload();
            });
        });
    });
})
</script>
<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: reload"></a>
        Formandos
    </h2>
</div>
<?php
$session->flash();
$paginator->options(array(
    'url' => array($this->params['prefix'] => true)));
$sortOptions = array('data-bind' => 'click: loadThis');
$buttonAfter = '<button class="helper" onclick="return false" ' .
    'tabindex="-1" type="button"></button>';
?>
<div class="row-fluid">
    <?=$form->create('ViewFormandos',array(
        'url' => "/{$this->params['prefix']}/formandos/listar/",
        'id' => 'form-filtro')) ?>
    <div class="row-fluid">
        <div class="span2">
            <?=$form->input('ViewFormandos.codigo_formando',
                array('label' => 'Codigo Formando', 'div' => 'input-control mini text',
                    'error' => false,'type' => 'text', 'after' => $buttonAfter)); ?>
        </div>
        <div class="span3">
            <?=$form->input('ViewFormandos.nome', 
                array('label' => 'Nome', 'div' => 'input-control mini text',
                    'error' => false,'type' => 'text', 'after' => $buttonAfter)); ?>
        </div>
        <div class="span3">
            <?=$form->input('ViewFormandos.boleto_impresso', 
                array('options' => $boletos, 'type' => 'select', 
                    'class' => 'selectpicker', 'label' => 'Boletos', 'div' => false)); ?>
        </div>
        <div class="span2">
            <label>&nbsp;</label>
            <button type='submit' class='mini max bg-color-red'>
                Filtrar
                <i class='icon-search-2'></i>
            </button>
        </div>
    </div>
</div>
<div class="row-fluid">
<?php if (sizeof($formandos) > 0) : ?>
    <div class="row-fluid">
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th scope="col" width="10%">
                        <?=$paginator->sort('Cod', 'codigo_formando',$sortOptions); ?>
                    </th>
                    <th scope="col" width="25%">
                        <?=$paginator->sort('Nome', 'nome',$sortOptions); ?>
                    </th>
                    <th scope="col" width="40%">Endereço</th>
                    <th scope="col" width="5%">
                        <?=$paginator->sort('Impresso', 'boleto_impresso',$sortOptions); ?>
                    </th>
                    <th scope="col" width="5%">
                        <?=$paginator->sort('Enviado', 'boleto_enviado',$sortOptions); ?>
                    </th>
                    <th scope="col" width="15%"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($formandos as $formando) : ?>
                <tr>
                    <td><?=$formando['ViewFormandos']['codigo_formando']; ?></td>
                    <td><?=$formando['ViewFormandos']['nome']; ?></td>
                    <td>
                        <?="{$formando['ViewFormandos']['end_rua']}, {$formando['ViewFormandos']['end_numero']} - "?>
                        <?=!empty($formando['ViewFormandos']['end_complemento']) ? $formando['ViewFormandos']['end_complemento'] : ""?>
                        <?=" - {$formando['ViewFormandos']['end_bairro']} - "?>
                        <?="{$formando['ViewFormandos']['end_cep']} - {$formando['ViewFormandos']['end_cidade']}/{$formando['ViewFormandos']['end_uf']}"?>
                    </td>
                    <td><?=$formando['ViewFormandos']['boleto_impresso'] == 1 ? "Sim" : "Não"; ?></td>
                    <td><?=$formando['ViewFormandos']['boleto_enviado'] == 1 ? "Sim" : "Não"; ?></td>
                    <td>
                        <?php if($formando['ViewFormandos']['boleto_impresso'] == 1) : ?>
                        <a href="/<?=$this->params['prefix']?>/boletos/gerar_todos/<?=$formando['ViewFormandos']['id']?>"
                            target="_blank" class="button mini default">
                            Reimprimir
                        </a>
                        <?php if($formando['ViewFormandos']['boleto_enviado'] == 0) : ?>
                        <br />
                        <a href="/<?=$this->params['prefix']?>/boletos/marcar_enviado/<?=$formando['ViewFormandos']['id']?>"
                            class="button mini bg-color-blueDark marcar-envio">
                            Marcar Enviado
                        </a>
                        <?php endif; ?>
                        <?php else : ?>
                        <a href="/<?=$this->params['prefix']?>/boletos/gerar_todos/<?=$formando['ViewFormandos']['id']?>"
                            target="_blank" class="button mini default">
                            Imprimir
                        </a>
                        <?php endif; ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="4" class="paginacao">
                        <?=$paginator->numbers(array('separator' => ' ',
                            'data-bind' => 'click: loadThis')); ?>
                    </td>
                    <td colspan="3">
                        <span class="label label-info">
                        <?=$paginator->counter(array(
                            'format' => 'Total : %count% Formandos')); ?>
                        </span>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
<?php else : ?>
    <h2 class="fg-color-red">Nenhum Formando Encontrado</h2>
<?php endif; ?>
</div>
