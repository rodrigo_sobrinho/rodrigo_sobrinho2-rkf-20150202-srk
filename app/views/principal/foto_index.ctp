<div class="span12 metro" style="position:relative" id="content-container">
    <div class="row-fluid content-loader hide" id="content-loading">
        <div class="cycle-loader"></div>
    </div>
    <div class="row-fluid content-loader hide" id="content-body"></div>
    <div class="row-fluid content-loader" id="content-home">
    <?php if(empty($turmaLogada['Turma']['id'])) : ?>
        <div class="tile icon bg-color-red"
            data-bind="click: function(data, event) {
            page('/foto/turmas/listar') }">
            <div class="tile-content">
                <div class="img icon-light-bulb"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Turmas Gerenciadas</span>
            </div>
        </div> 
        <?php else : ?>
        <div class="tile icon bg-color-blueDark"
            data-bind="click: function(data, event) {
            page('/foto/formandos/listar') }">
            <div class="tile-content">
                <div class="img icon-users"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Formandos</span>
            </div>
        </div>
        <div class="tile icon bg-color-black"
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/formandos/fotos_telao_listar') }">
            <div class="tile-content">
                <span class="img icon-file-excel"></span>
            </div>
            <div class="brand"><span class="name">Fotos Telão</span></div>
        </div>
        <div class="tile icon bg-color-red"
             data-bind="click: function(data, event) {
             page('foto/eventos/listar') }">
            <div class="tile-content">
                <span class="img icon-map"></span>
            </div>
            <div class="brand"><span class="name">Eventos</span></div>
        </div>
        <?php endif; ?>
        <div class="tile icon bg-color-pink"
             data-bind="click: function(data, event) {
             page('/calendario/exibir') }">
            <div class="tile-content">
                <div class="img icon-calendar"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Agenda</span>
            </div>
        </div>
        <div class="tile icon bg-color-orangeDark"
             data-bind="click: function(data, event) {
             page('/turmas/ficha_evento') }">
            <div class="tile-content">
                <div class="img icon-drawer-2"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Ficha Evento</span>
            </div>
        </div>
        <div class="tile icon bg-color-greenDark"
             data-bind="click: function(data, event) {
             page('turmas/analise_temporada') }">
            <div class="tile-content">
                <span class="img icon-amazon-2"></span>
            </div>
            <div class="brand"><span class="name">Análise Temporada</span></div>
        </div>
    </div>
</div>