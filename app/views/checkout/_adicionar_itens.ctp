<script>
	$(function() {
		$("#qtde-convites-extras").change( function() {
			var qtdeConvitesSelecionados = parseInt($(this).val());
			var maxConvites = parseInt($("#maximo-convites-extras").html());
			var precoConvite = parseFloat(stringMoneyToNumber($("#preco-convite-extra").html()));
			var qtdeConvitesComprados = parseInt($("#qtde-convites-comprados").html());
			saldoConvites = qtdeConvitesSelecionados * precoConvite;
			saldoConvites = saldoConvites.toFixed(2)*-1;
			atualizaSaldoConvites(true);
			$("#qtde-convites-comprados").html(qtdeConvitesSelecionados);
			$('.qtde-convites').html(parseInt($('#qtde-convites').html())+qtdeConvitesSelecionados);
			atualizaSaldoTotal();
		});
	});
</script>
<div class='adicionar-itens'>
<?php if(!empty($convitesExtras)) { ?>
	<table style='width:95%'>
		<thead>
			<tr>
				<th>Preço Por Convite</th>
				<th>Qtde. Máxima de Convites</th>
				<th>Selecione</th>
				<th>Valor Total Dos Convites</th>
			</tr>
		</thead>
		<tr>
			<td id='preco-convite-extra'>R$<?=number_format($convitesExtras['ConviteCheckoutTurma']['valor'],2,',','.');?></td>
			<td id='maximo-convites-extras'><?=$convitesExtras['ConviteCheckoutTurma']['convites_por_formando'];?></td>
			<td>
				<?=$form->input('qtdeConvitesExtras', array('options' => $selectConvitesExtras, 'type' => 'select', 'id' => 'qtde-convites-extras', 'label' => false, 'div' => false)); ?>
			</td>
			<td class='saldo-convites'>-</td>
		</tr>
		<tr>
			<td><input type="submit" class="submit botao-anterior" value="Anterior"></td>
			<td><input type="submit" class="submit botao-proximo" value="Próximo"></td>
		</tr>
	</table>
	<!--
	<table style='width:50%; margin-top:50px'>
		<thead>
			<tr>
				<th style='width:50%'>Item</th>
				<th style='width:50%'>Valor</th>
			</tr>
		</thead>
		<tr>
			<td><span id='qtde-convites-comprados'>0</span> Convite(s) Extras</td>
			<td class='total-convites-extras'>-</td>
		</tr>
		<tr>
			<td>Valor de Contrato</td>
			<td class='valor-contrato'>R$<?=number_format($formando['valor_adesao'],2,',','.');?></td>
		</tr>
		<tr>
			<td>Valor de IGPM</td>
			<td><?=$totalIGPM > 0 ? "R$" . number_format($totalIGPM,2,',','.') : "-";?></td>
		</tr>
		<tr>
			<td>Valor de Contrato c/ Correção</td>
			<td class='total-contrato'>R$<?=number_format($formando['valor_adesao']+$totalIGPM,2,',','.');?></td>
		</tr>
		<tr>
			<td>Valor de Campanhas</td>
			<td class='total-campanha'><?=$totalDespesasExtras > 0 ? "R$".number_format($totalDespesasExtras,2,',','.') : "-"?></td>
		</tr>
		<tr>
			<td>Total Recebido</td>
			<td class='total-pago'></td>
		</tr>
		<tr>
			<td>Valor de Campanhas</td>
			<td class='total-campanha'><?=$totalDespesasExtras > 0 ? "R$".number_format($totalDespesasExtras,2,',','.') : "-"?></td>
		</tr>
		<tr>
			<td class='saldo-total'></td>
			<td></td>
		</tr>
	</table>
	-->
<?php } else { ?>
	<h2 class='titulo'>Essa turma não tem venda de convites extras disponível</h2>
<?php } ?>
</div>