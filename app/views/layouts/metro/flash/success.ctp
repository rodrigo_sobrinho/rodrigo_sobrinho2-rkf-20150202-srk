<div class="alert alert-block alert-success fade in">
    <a class="close" data-dismiss="alert" href="#"></a>
    <p>
        <?php if(is_array($content_for_layout)) : ?>
        <h4 class="alert-heading"><?=array_shift($content_for_layout); ?></h4>
        <?=implode("<br />", $content_for_layout); ?>
        <?php else : ?>
        <h4 class='alert-heading'>OK</h4>
        <?=$content_for_layout; ?>
        <?php endif; ?>
    </p>
</div>