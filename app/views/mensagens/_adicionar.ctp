<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootstrap/fileupload.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/redactor.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/fileupload.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/redactor.js"></script>
<style type="text/css">
    .exibir-assunto { margin-bottom:0 }
    label { margin-right:0 }
    .anexos i:hover { color:red }
    input[type=text] { margin-bottom: 0 }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        var urlBase = "/<?=$usuario['Usuario']['grupo']?>/mensagens/";
        $('.selectpicker').selectpicker({
            width : '100%'
        });
        $('.exibir-assunto').click(function(e) {
            var that = this;
            $('.div-assunto:visible').fadeOut(500, function() {
                $('.div-assunto.' + $(that).attr('dir')).fadeIn(500);
            });
        });
        $('.fileupload').bind('loaded',function(e) {
            indice = $('.anexos').length;
            src = e.src.substring(e.src.indexOf('base64,') + 7);
            inputSrc = '<input type="hidden" value="' + src + '" dir=' + indice +
                    ' name="data[Arquivo][' + indice + '][src]" class="input-anexo" />';
            inputSize = '<input type="hidden" value="' + e.file.size + '" dir=' + indice +
                    ' name="data[Arquivo][' + indice + '][tamanho]" class="input-anexo" />';
            inputType = '<input type="hidden" value="' + e.file.type + '" dir=' + indice +
                    ' name="data[Arquivo][' + indice + '][tipo]" class="input-anexo" />';
            inputName = '<input type="hidden" value="' + e.file.name + '" dir=' + indice +
                    ' name="data[Arquivo][' + indice + '][nome]" class="input-anexo" />';
            buttonAnexo = "<button type='button' class='anexos input-anexo' dir=" + indice +
                    ">" + e.file.name + "<i class='icon-remove remover-anexo'></i></button>";
            $('.fileupload')
                .after(inputSrc)
                .after(inputSize)
                .after(inputType)
                .after(inputName)
                .after(buttonAnexo)
                .fileupload('reset');
            return;
        });
        $('#formulario').on('click','.remover-anexo',function(e) {
            $(".input-anexo[dir="+$(this).parent().attr('dir')+"]").remove();
        });
        var select = $('#item-nova-mensagem');
        var url = urlBase+"carregar_itens_nao_resolvidos/";
        $.getJSON(url, function(response) {
            if(response.itens.length > 0) {
                select.html("<option value=''>Selecione Um Item</option>");
                $.each(response.itens,function(i,item) {
                    select.append("<option value="+item.Item.id+
                        ">"+item.Item.nome+"</option>");
                });
            } else {
                select.html("<option value=''>Nenhum Item Pendente</option>");
            }
        }).always(function() {
            select.selectpicker('refresh');
        });
        buttons = ['formatting', '|', 'bold', 'italic', '|',
            'unorderedlist', 'orderedlist', 'outdent', 'indent', '|',
            'link', '|', 'fontcolor', 'backcolor', '|',
            'alignment', '|', 'horizontalrule'];
        $('#texto-nova-mensagem').redactor({
            buttons:buttons,
            minHeight:150
        });
        $('#item-nova-mensagem').change(function(e) {
            var id = $(this).val();
            var url = urlBase + "carregar_assuntos_nao_resolvidos/"+id;
            var select = $('#assunto-nova-mensagem');
            select.html("<option value=''>Carregando</option>");
            $.getJSON(url, function(response) {
                if(response.assuntos.length > 0) {
                    select.html("<option value=''>Selecione Um Assunto</option>");
                    $.each(response.assuntos,function(i,assunto) {
                        select.append("<option value="+assunto.Assunto.id+
                            ">"+assunto.Assunto.nome+"</option>");
                    });
                } else {
                    select.html("<option value=''>Nenhum Assunto Pendente</option>");
                }
            }).always(function() {
                select.selectpicker('refresh');
            });
        });
        $("#formulario").submit(function(e) {
            e.preventDefault();
            if($('#item-nova-mensagem').val() == "") {
                alert('Selecione Um Item');
                $('#item-nova-mensagem').focus();
            } else if($(".div-assunto:visible").find('.assunto-nova-mensagem').val() == "") {
                if($(".div-assunto:visible").hasClass('existente'))
                    alert('Selecione um Assunto');
                else
                    alert('Digite o Assunto');
            } else if($("<div/>").html($('#texto-nova-mensagem').val()).text() == "") {
                alert('Digite o texto da mensagem');
            } else {
                var button = $('.modal-footer').find(':contains("Envia")');
                if(button.attr('disabled') != 'disabled') {
                    button
                        .attr('disabled','disabled')
                        .removeClass('bg-color-red')
                        .addClass('bg-color-green')
                        .text('Enviando');
                    $('.div-assunto:not(:visible)').remove();
                    var data = $('#formulario').serialize();
                    var url = $("#formulario").attr('action');
                    $.ajax({
                        type: 'POST',
                        data: data,
                        dataType: 'json',
                        url: url,
                        success: function(response) {
                            var context = ko.contextFor($("#content-body")[0]);
                            context.$data.reload();
                            bootbox.hideAll();
                        },
                        error: function() {
                            bootbox.alert('Erro ao conectar servidor. Tente novamente mais tarde',function() {
                                bootbox.hideAll();
                            });
                        }
                    });
                }
            }
        });
    })
</script>
<?=$form->create('Turma', array(
    'url' => "/mensagens/inserir",
    'id' => 'formulario'
));
?>
<div class="row-fluid">
    <div class="span6">
        <?=$form->input('Item.id', array(
            'type' => 'select',
            'class' => 'selectpicker',
            'data-placeholder' => 'Selecione',
            'id' => 'item-nova-mensagem',
            'label' => 'Item',
            'div' => false));
        ?>
    </div>
    <div class="span6 div-assunto existente">
        <label>
            Assunto
            <a class="pull-right button mini default exibir-assunto" dir="novo">
                Novo Assunto
            </a>
        </label>
        <?=$form->input('Mensagem.assunto_id', array(
            'type' => 'select',
            'class' => 'selectpicker assunto-nova-mensagem',
            'data-placeholder' => 'Selecione',
            'label' => false,
            'id' => 'assunto-nova-mensagem',
            'error' => false,
            'div' => false));
        ?>
    </div>
    <div class="span6 novo div-assunto hide">
        <label>
            Assunto
            <a class="pull-right button mini default exibir-assunto" dir="existente">
                Cancelar
            </a>
        </label>
        <input type="text" class="assunto-nova-mensagem input-block-level" name="data[Assunto][nome]" />
    </div>
</div>
<?php if($usuario['Usuario']['grupo'] == 'planejamento') : ?>
<br />
<div class="row-fluid">
    <div class="span6">
        <?=$form->input('Assunto.pendencia', array(
            'type' => 'select',
            'class' => 'selectpicker',
            'options' => $pendencias,
            'empty' => 'Selecione',
            'label' => 'Pendencia',
            'div' => false));
        ?>
    </div>
</div>
<?php else : ?>
<?=$form->hidden('Assunto.pendencia',array('value' => 'As'))?>
<?php endif; ?>
<?=$form->hidden('Assunto.resolvido',array('value' => 0))?>
<br />
<div class="row-fluid">
    <label>Anexo</label>
    <div class="fileupload fileupload-new" data-provides="fileupload" data-reader>
        <span class="btn-file">
            <a class="button default fileupload-new">Adicionar</a>
            <span class="fileupload-exists">Change</span>
            <input type="file" id="anexo-nova-mensagem" />
        </span>
        <span class="fileupload-preview"></span>
    </div>
</div>
<br />
<div class="row-fluid">
    <?=$form->input('Mensagem.texto', array(
        'class' => 'redactor',
        'label' => 'Texto',
        'id' => 'texto-nova-mensagem',
        'error' => false,
        'div' => false));
    ?>
</div>
<?= $form->end(array('label' => false, 'div' => false, 'style' => 'display:none')); ?>