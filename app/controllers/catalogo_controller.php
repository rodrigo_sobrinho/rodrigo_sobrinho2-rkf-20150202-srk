<?php

class CatalogoController extends AppController {

    var $uses = array("Catalogo","CatalogoItem","CatalogoFoto",
        "CatalogoAnexo","TiposEvento","CatalogoEventoItem");
    
    private function _index() {
        $this->layout = "catalogos";
        $this->Session->delete("eventoId");
        $this->Session->delete("itemId");
        $this->Session->delete("catalogoId");
        $this->Session->delete("filtroCatalogo");
        $this->_itens();
    }
    
    function comercial_index() {
        $this->_index();
    }
    
    function marketing_index() {
        $this->_index();
    }

    function planejamento_index() {
        $this->_index();
    }
    
    function producao_index() {
        $this->_index();
    }
    
    function producao_baixar_anexo($id){
        $this->marketing_baixar_anexo($id);
    }
    
    function producao_busca($tipo){
        $this->marketing_busca($tipo);
    }
    
    function producao_detalhes($catalogoId){
        $this->marketing_detalhes($catalogoId);
    }
    
    function producao_editar($eventoId, $itemId, $id = false){
        $this->marketing_editar($eventoId, $itemId, $id);
    }
    
    function producao_editar_evento($tipoEventoId = false){
        $this->marketing_editar_evento($tipoEventoId);
    }
    
    function producao_editar_item($tipoEventoId, $itemId = false){
        $this->marketing_editar_item($tipoEventoId, $itemId);
    }
    
    function producao_item($itemId){
        $this->marketing_item($itemId);
    }
    
    function producao_listar($eventoId = false, $itemId = false){
        $this->marketing_listar($eventoId, $itemId);
    }
    
    function producao_listar_itens($eventoId){
        $this->marketing_listar_itens($eventoId);
    }
    
    function producao_status_evento($tipoEventoId){
        $this->marketing_status_evento($tipoEventoId);
    }
    
    function producao_ordem_itens($eventoId){
        $this->marketing_ordem_itens($eventoId);
    }

    private function _evento($eventoId) {
        $this->layout = "catalogos";
        $this->Session->write("eventoId",$eventoId);
        $this->Session->delete("itemId");
        $this->Session->delete("catalogoId");
        $this->Session->delete("filtroCatalogo");
        $this->_itens();
    }
    
    function comercial_evento($eventoId) {
        $this->_evento($eventoId);
    }

    function producao_evento($eventoId) {
        $this->_evento($eventoId);
    }
    
    function marketing_evento($eventoId) {
        $this->_evento($eventoId);
    }

    function planejamento_evento($eventoId) {
        $this->_evento($eventoId);
    }
    
    private function _item($itemId, $tipo) {
        $this->layout = "catalogos";
        if(!$this->Session->check("eventoId"))
            $this->redirect("/comercial/catalogo");
        $this->Session->delete("catalogoId");
        $this->Session->delete("filtroCatalogo");
        $this->Session->write("itemId",$itemId);
        $this->_itens();
        $view = $tipo == 'lista' ? 'lista' : 'grade';
        $this->set('view',$view);
    }
    
    function comercial_item($itemId, $tipo = 'grade') {
        $this->_item($itemId,$tipo);
    }

    function comercial_busca($tipo = 'grade') {
        $this->_busca($tipo);
    }
    
    function marketing_item($itemId, $tipo = 'grade', $catalogoId = false , $desativar = false) {
        $this->_item($itemId,$tipo);
    }
    
    function marketing_busca($tipo = 'grade') {
        $this->_busca($tipo);
    }

    function planejamento_item($itemId, $tipo = 'grade') {
        $this->_item($itemId,$tipo);
    }
    
    function planejamento_busca($tipo = 'grade') {
        $this->_busca($tipo);
    }
    
    private function _busca($tipo) {
        $this->layout = "catalogos";
        if($this->data["filtroCatalogo"]) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->Session->write("filtroCatalogo",$this->data["filtroCatalogo"]);
            echo json_encode(array());
        } elseif(!$this->Session->check("filtroCatalogo")) {
            $this->redirect("/comercial/catalogo");
        } else {
            $this->_itens();
            $view = $tipo == 'lista' ? 'lista' : 'grade';
            $filtro = $this->Session->read("filtroCatalogo");
            $f = strtolower($filtro);
            $this->set('view',$view);
            $this->set('filtro',$filtro);
            $conditions = array(
                "(lower(Catalogo.nome) like('%{$f}%') or lower(Catalogo.informacoes) like('%{$f}%'))"
            );
            if($this->Session->check("itemId"))
                $conditions['CatalogoEventoItem.catalogo_item_id'] = $this->Session->read("itemId");
            $catalogos = $this->Catalogo->listarCatalogos($conditions);
            $this->set("catalogos",$catalogos);
        }
    }
    
    function comercial_detalhes($catalogoId) {
        $this->_detalhes($catalogoId);
    }
    
    function marketing_detalhes($catalogoId, $desativar = false) {
        if($desativar){
            if(isset($catalogoId)){
                $data = array(
                    'id' => $catalogoId,
                    'desativado' => 1,
                );
                $this->Catalogo->save($data);
            }
        }
        $this->_detalhes($catalogoId);
    }

    function planejamento_detalhes($catalogoId) {
        $this->_detalhes($catalogoId);
    }
    
    function _detalhes($catalogoId) {
        $this->layout = "catalogos";
        $this->Session->write("catalogoId",$catalogoId);
        $this->Session->delete("filtroCatalogo");
        $this->_itens();
    }
    
    function marketing_baixar_anexo($id) {
        $this->_baixar_anexo($id);
    }

    function comercial_baixar_anexo($id) {
        $this->_baixar_anexo($id);
    }

    function planejamento_baixar_anexo($id) {
        $this->_baixar_anexo($id);
    }
    
    private function _baixar_anexo($id) {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        if($id) {
            $this->CatalogoAnexo->id = $id;
            $arquivo = $this->CatalogoAnexo->read();
            if (!empty($arquivo)) {
                header("Pragma: public");
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Cache-Control: private", false);
                header("Content-Type: " . $arquivo['CatalogoAnexo']['tipo']);
                header("Content-Disposition: attachment; filename=\"" . $arquivo['CatalogoAnexo']['nome'] . "\";");
                header("Content-Transfer-Encoding: binary");
                header("Content-Length: " . $arquivo['CatalogoAnexo']['tamanho']);
                echo $arquivo["CatalogoAnexo"]["data"];
            } else {
                exit();
            }
        } else {
            exit();
        }
    }
    
    private function listarItensDeEvento($tiposEventoId) {
        $options = array(
            'joins' => array(
                array(
                    "table" => "catalogos_itens",
                    "type" => "inner",
                    "alias" => "CatalogoItem",
                    "conditions" => array(
                        "CatalogoItem.tipos_evento_id = TiposEvento.id"
                    )
                ),
                array(
                    "table" => "catalogos_eventos_itens",
                    "type" => "inner",
                    "alias" => "CatalogoEventoItem",
                    "conditions" => array(
                        "CatalogoItem.id = CatalogoEventoItem.catalogo_item_id"
                    )
                )
            ),
            'order' => array('CatalogoItem.ordem'),
            'group' => array('CatalogoItem.id'),
            'conditions' => array(
                "TiposEvento.id" => $tiposEventoId
            ),
            'fields' => array('CatalogoItem.*')
        );
        return $this->TiposEvento->find("all",$options);
    }
    
    private function listarEventos() {
        $options = array(
            'joins' => array(
                array(
                    "table" => "catalogos_itens",
                    "type" => "inner",
                    "alias" => "CatalogoItem",
                    "conditions" => array(
                        "CatalogoItem.tipos_evento_id = TiposEvento.id"
                    )
                ),
                array(
                    "table" => "catalogos_eventos_itens",
                    "type" => "inner",
                    "alias" => "CatalogoEventoItem",
                    "conditions" => array(
                        "CatalogoItem.id = CatalogoEventoItem.catalogo_item_id"
                    )
                )
            ),
            'group' => array('TiposEvento.id'),
            'conditions' => array(
                "TiposEvento.ativo" => 1
            ),
            'fields' => array('TiposEvento.*')
        );
        return $this->TiposEvento->find("all",$options);
    }

    private function _itens() {
        $usuario = $this->obterUsuarioLogado();
        $this->set('usuarioLogado',$usuario);
        $eventos = $this->listarEventos();
        $this->set('eventos',$eventos);
        if($this->Session->check("eventoId")) {
            $eventoId = $this->Session->read("eventoId");
            $this->set('eventoSelecionado',$this->TiposEvento->read(null,$eventoId));
            $itens = $this->listarItensDeEvento($eventoId);
            $this->set('itens',$itens);
            if($this->Session->check("itemId")) {
                $itemId = $this->Session->read("itemId");
                $this->set('itemSelecionado',$this->CatalogoItem->read(null,$itemId));
                $catalogos = $this->Catalogo->listarCatalogosDeItem($itemId);
                $this->set('catalogos',$catalogos);
                $usuario = $this->obterUsuarioLogado();
                $this->set("grupo_usuario", $usuario['Usuario']['grupo']);
            }
        }
        if($this->Session->check("catalogoId")) {
            $this->Catalogo->bindModel(array(
                "hasMany" => array('CatalogoFoto' => array(
                    'className' => 'CatalogoFoto',
                    'order' => 'ordem'
                ),'CatalogoAnexo')
            ),false);
            $catalogo = $this->Catalogo->read(null,$this->Session->read("catalogoId"));
            $this->set("catalogo",$catalogo);
        }
    }
    
    function marketing_editar_evento($tipoEventoId = false) {
        if($this->data) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            if($this->TiposEvento->save($this->data))
                $this->Session->setFlash("Dados inseridos com sucesso", 'metro/flash/success');
            else
                $this->Session->setFlash("Erro ao alterar dados", 'metro/flash/error');
            echo json_encode(array());
        } elseif($tipoEventoId) {
            $this->data = $this->TiposEvento->read(null,$tipoEventoId);
        }
    }
    
    function marketing_editar_item($tipoEventoId, $itemId = false) {
        if($this->data) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            if($this->CatalogoItem->save($this->data))
                $this->Session->setFlash("Dados inseridos com sucesso", 'metro/flash/success');
            else
                $this->Session->setFlash("Erro ao alterar dados", 'metro/flash/error');
            echo json_encode(array());
        } else {
            if($itemId)
                $this->data = $this->CatalogoItem->read(null,$itemId);
            else {
                $maior = $this->CatalogoItem->find("first",array(
                    "fields" => array("max(ordem)+1 as maior"),
                    'conditions' => array(
                        "CatalogoItem.tipos_evento_id" => $tipoEventoId
                    )
                ));
                $ordem = !empty($maior[0]["maior"]) ? $maior[0]["maior"] : 1;
                $this->data = array(
                    "CatalogoItem" => array(
                        "ordem" => $ordem,
                        "tipos_evento_id" => $tipoEventoId
                    )
                );
            }
            $this->set("evento",$this->TiposEvento->read(null,$tipoEventoId));
        }
    }
    
    function marketing_status_evento($tipoEventoId) {
        $usuario = $this->obterUsuarioLogado();
        $tipoEvento = $this->TiposEvento->read(null,$tipoEventoId);
        if($tipoEvento) {
            $tipoEvento["TiposEvento"]["ativo"] = $tipoEvento["TiposEvento"]["ativo"] == 1 ? 0 : 1;
            if($this->TiposEvento->save($tipoEvento)) {
                $this->Session->setFlash("Evento alterado com sucesso", 'metro/flash/success');
            } else {
                $this->Session->setFlash("Erro ao alterar evento", 'metro/flash/error');
            }
        } else {
            $this->Session->setFlash("Evento não encontrado", 'metro/flash/error');
        }
        $eventos = $this->TiposEvento->find("all");
        $this->set("eventos",$eventos);
        $this->render("{$usuario['Usuario']['grupo']}_listar_eventos");
    }
    
    function marketing_listar($eventoId = false, $itemId = false, $desativar = false, $catalogoId = false) {
        $usuario = $this->obterUsuarioLogado();
        if(!$eventoId) {
            $eventos = $this->TiposEvento->find("all");
            $this->set("eventos",$eventos);
            $this->render("{$usuario['Usuario']['grupo']}_listar_eventos");
        } elseif($itemId) {
            if($desativar){
                if(isset($catalogoId)){
                    $data = array(
                        'id' => $catalogoId,
                        'desativado' => 1,
                        );
                    $this->Catalogo->save($data);
                }
            }
            $usuario = $this->obterUsuarioLogado();
            $catalogos = $this->Catalogo->listarCatalogosDeItem($itemId,$eventoId);
            $this->set("evento",$this->TiposEvento->read(null,$eventoId));
            $this->set('item',$this->CatalogoItem->read(null,$itemId));
            $this->set("catalogos",$catalogos);
            $this->set("grupo_usuario", $usuario['Usuario']['grupo']);
            $this->render("marketing_listar_catalogos");

        } else {
            $itens = $this->CatalogoItem->find("all",array(
                'conditions' => array(
                    "tipos_evento_id" => $eventoId
                    )
                ));
            $this->set("evento",$this->TiposEvento->read(null,$eventoId));
            $this->set("itens",$itens);
            $this->render("{$usuario['Usuario']['grupo']}_listar_itens");
        }
    }
    
    
    function marketing_ordem_itens() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        if(isset($this->data["itens"])) {
            if($this->CatalogoItem->saveAll($this->data["itens"])) {
                $this->Session->setFlash("Alterações salvas", 'metro/flash/success');
            } else {
                $this->Session->setFlash("Erro ao salvar alterações", 'metro/flash/error');
            }
        }
        echo json_encode(array());
    }
    
    function marketing_listar_itens($eventoId) {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $itens = $this->CatalogoItem->find("list",array(
            'conditions' => array(
                "tipos_evento_id" => $eventoId
            ),
            'fields' => array('nome')
        ));
        echo json_encode(array('itens' => $itens));
    }

    /**
     * Cria ou edita as informações de um catalogo
     *
     * @param $eventoId
     * @param $itemId
     *
     * @return
     */
    function marketing_editar($eventoId, $itemId, $id = false) {
        $catalogo = null;
        if($id)
            $catalogo = $this->Catalogo->read(null,$id);
        if($this->data) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $data = $this->data;
            $data["Catalogo"]['data_ultima_alteracao'] = date("Y-m-d H:i:s");
            $data["Catalogo"]['desativado'] = 0;
            if($this->Catalogo->saveAll($data)) {
                $catalogoId = !empty($data["Catalogo"]["id"]) ? $data["Catalogo"]["id"] : $this->Catalogo->getLastInsertId();
                if(isset($data["fotos"])) {
                    foreach($data["fotos"] as $foto) {
                        $this->CatalogoFoto->create();
                        $this->CatalogoFoto->save(array(
                            'catalogo_id' => $catalogoId,
                            'data_cadastro' => date('Y-m-d H:i:s'),
                            'ext' => $foto['ext'],
                            'src' => $foto['src'],
                            'ordem' => $foto['ordem']
                        ));
                    }
                }
                if(isset($data["itens"])) {
                    foreach($data["itens"] as $item) {
                        $this->CatalogoEventoItem->create();
                        $this->CatalogoEventoItem->save(array(
                            'catalogo_id' => $catalogoId,
                            'data_cadastro' => date('Y-m-d H:i:s'),
                            'catalogo_item_id' => $item
                        ));
                    }
                }
                if(isset($data["anexos"])) {
                    foreach($data["anexos"] as $arquivo) {
                        $this->CatalogoAnexo->create();
                        $this->CatalogoAnexo->save(array(
                            'catalogo_id' => $catalogoId,
                            'data_cadastro' => date('Y-m-d H:i:s'),
                            'nome' => $arquivo['nome'],
                            'titulo' => $arquivo['titulo'],
                            'tipo' => $arquivo['tipo'],
                            'tamanho' => $arquivo['tamanho'],
                            'data' => base64_decode($arquivo['src'])
                        ));
                    }
                }
                if(isset($data["remover_fotos"]))
                    foreach($data['remover_fotos'] as $foto)
                        $this->CatalogoFoto->delete($foto);
                if(isset($data["remover_itens"]))
                    foreach($data['remover_itens'] as $item)
                        $this->CatalogoEventoItem->delete($item);
                if(isset($data["remover_anexos"]))
                    foreach($data['remover_anexos'] as $anexo)
                        $this->CatalogoAnexo->delete($anexo);
                $this->Session->setFlash("Dados inseridos com sucesso", 'metro/flash/success');
            } else {
                $this->Session->setFlash("Erro ao enviar dados", 'metro/flash/error');
            }
            echo json_encode(array());
        } else {
            $eventosCadastrados = array($eventoId => $eventoId);
            $this->data = $catalogo;
            $itens = array();
            if($catalogo["CatalogoEventoItem"]) {
                foreach($catalogo["CatalogoEventoItem"] as $eventoItem) {
                    $catalogoItem = $this->CatalogoItem->read(null,$eventoItem["catalogo_item_id"]);
                    $itens[] = array(
                        "id" => $eventoItem["id"],
                        "item" => $eventoItem["catalogo_item_id"],
                        'nome' => "{$catalogoItem["TiposEvento"]["nome"]} - {$catalogoItem["CatalogoItem"]["nome"]}"
                    );
                }
            }
            $eventos = $this->TiposEvento->find('list',array(
                'conditions' => array(
                    'TiposEvento.ativo' => 1
                ),
                "fields" => array('nome')
            ));
            $evento = $this->TiposEvento->read(null,$eventoId);
            $this->set("evento",$evento);
            $this->set('item',$this->CatalogoItem->read(null,$itemId));
            $this->set("eventosCadastrados",$eventosCadastrados);
            $this->set("eventos",$eventos);
            $this->set("itens",$itens);
        }
        $this->set("catalogo",$catalogo);
    }
    
}