<div class='box-com-titulo'>
<span id="conteudo-titulo" class="box-com-titulo-header">Rifa Online</span>

<div id="conteudo-container">
	<?php $session->flash(); ?>
	<?php echo $form->create('Termos', array('url' => "/{$this->params['prefix']}/rifas/index")); ?>
	<h1><b> <?php echo $rifa['Rifa']['nome'];?></b></h1>
	<div class="grid_full alpha omega detalhes">
		<p class="grid_11 alpha omega first" style="margin-top: 10px;font-size: 14px;">
			<label class="grid_11 alpha omega">Dia do sorteio</label>
			<span class="grid_11 alpha first"><?php echo $rifa['Rifa']['data_sorteio'];?></span>
		</p>
		
		<p class="grid_11 alpha omega first" style="margin-top: 10px;font-size: 14px;">
			<label class="grid_11 alpha omega">Pr&ecirc;mios</label>
			<span class="grid_11 alpha first">
				<?php
					echo "1&deg; Pr&ecirc;mio: ".$rifa['Rifa']['premio_1']."<br />";
					if ($rifa['Rifa']['premio_2'] != "") {
						echo "2&deg; Pr&ecirc;mio: ".$rifa['Rifa']['premio_2']."<br />";
					}
					if ($rifa['Rifa']['premio_3'] != "") {
						echo "3&deg; Pr&ecirc;mio: ".$rifa['Rifa']['premio_3'];
					}
				?>
			</span>
		</p>
		
	</div>
	
	<p class="grid_full alpha omega first">
		<p class="grid_full alpha omega first"><b>Termos e Condições da Rifa Online</b></p>
		<p class="grid_full alpha omega first">
			<textarea name="" rows="15" cols="" width="100%" class="grid_10 alpha omega first" readonly="readonly" disabled="disabled">Os prêmios serão sorteados com os números da loteria federal. Os prêmios deverão ser retirados PELO FORMANDO que vendeu a rifa em um prazo máximo de 15 dias após o sorteio.

IMPORTANTE: Cada formando ter&aacute; direito a apenas uma rifa. Se gerar os cupons para esta rifa, n&atilde;o poder&aacute; participar de outras rifas.</textarea>	
		</p>
		<p class="grid_full alpha omega first">
			<?php echo $form->checkbox('aceito'); ?>
			Eu concordo com os termos e condições da Rifa Online da RK Formaturas
		</p>
	</p>
	
	<p class="grid_10 alpha omega first">
		<?php echo $html->link('Voltar',array( $this->params['prefix'] => true, 'action' => 'index') ,array('class' => 'submit')); ?>
		<?php echo $form->end(array('label' => 'Solicitar Cupons', 'div' => false, 'class' => 'submit'));?>
	</p>
</div>
</div>