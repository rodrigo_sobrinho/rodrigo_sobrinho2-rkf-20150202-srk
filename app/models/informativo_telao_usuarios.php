<?php
class InformativoTelaoUsuarios extends AppModel {
    
    var $name = 'InformativoTelaoUsuarios';
    var $belongsTo = array('Turma', 'Usuario');
    var $useTable = 'informativo_telao_usuarios';
    
}
?>