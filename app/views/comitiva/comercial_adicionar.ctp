<?php $buttonAfter = '<button class="helper" onclick="return false" ' .
        'tabindex="-1" type="button"></button>'; ?>
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/max/form_validate.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/jquery-validate.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery.meio.mask.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $.Input();
        $('input[alt]').setMask();
        $('#formulario').validate({
            sendForm : false,
            onBlur: true,
            eachValidField : function() {
                $(this).removeClass('error').removeClass('form-error').addClass('success');
                var label = $('label[for="'+$(this).attr('id')+'"]');
                if(label.length > 0) {
                    if(label.children('span').length > 0)
                        label.children('span').fadeOut(500,function() { $(this).remove()});
                }
            },
            eachInvalidField : function() {
                $(this).removeClass('success').addClass('error');
            },
            description: {
                notEmpty : {
                    required : function() {
                        var label = $('label[for="'+$(this).attr('id')+'"]');
                        if(label.length > 0) {
                            if(label.children('span').length > 0)
                                label.children('span').html('').attr('class','fg-color-red');
                            else
                                label.append($('<span>',{class:'fg-color-red'}));
                            mensagem = $(this).data('error') ||
                                'Complete o campo';
                            label.children('span').html(mensagem);
                        }
                        return '';
                    },
                    conditional : function() {
                        var label = $('label[for="'+$(this).attr('id')+'"]');
                        if(label.length > 0) {
                            if(label.children('span').length > 0)
                                label.children('span').html('').attr('class','fg-color-red');
                            else
                                label.append($('<span>',{class:'fg-color-red'}));
                            mensagem = $(this).data('error-conditional') ||
                                'Complete o campo';
                            label.children('span').html(mensagem);
                        }
                        return '';
                    },
                    pattern : function() {
                        var label = $('label[for="'+$(this).attr('id')+'"]');
                        if(label.length > 0) {
                            if(label.children('span').length > 0)
                                label.children('span').html('').attr('class','fg-color-red');
                            else
                                label.append($('<span>',{class:'fg-color-red'}));
                            mensagem = $(this).data('error-pattern') ||
                                'Complete o campo';
                            label.children('span').html(mensagem);
                        }
                        return '';
                    }
                }
            },
            valid: function() {
                var context = ko.contextFor($("#content-body")[0]);
                var dados = $("#formulario").serialize();
                var url = $("#formulario").attr('action');
                context.$data.showLoading(function() {
                    $.ajax({
                        url : url,
                        data : dados,
                        type : "POST",
                        dataType : "json",
                        complete : function() {
                            context.$data.reload();
                        }
                    });
                });
            },
            invalid: function() {
                return false;
            },
            conditional: {
                senha: function() {
                    return $("#senha").val() == $("#confirmar").val();
                }
            }
        });
    });
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button back" href="<?="/{$this->params['prefix']}/comissoes/listar"?>"
                data-bind="click: loadThis"></a>
            Adicionar Membro na Comiss&atilde;o
        </h2>
    </div>
</div>
<?php $session->flash(); ?>
<div class="row-fluid">
    <?=$form->create('Usuario', array(
        'url' => "/{$this->params['prefix']}/comitiva/adicionar",
        'id' => 'formulario')); ?>
    <div class="row-fluid">
        <div class="span4">
            <label class="required" for="input-nome">Nome</label>
            <?=$form->input('Usuario.nome', array(
                'label' => false,
                'div' => 'input-control text',
                'error' => false,
                'id' => 'input-nome',
                'data-required' => 'true',
                'data-description' => 'notEmpty',
                'data-describedby' => 'input-nome',
                'data-error' => 'Digite o Nome',
                'after' => $buttonAfter));
            ?>
        </div>
        <div class="span4">
            <label class="required" for="input-email">Email</label>
            <?=
            $form->input('Usuario.email', array(
                'label' => false,
                'div' => 'input-control text',
                'error' => false,
                'id' => 'input-email',
                'data-required' => 'true',
                'data-description' => 'notEmpty',
                'data-describedby' => 'input-email',
                'data-error' => 'Digite o Email',
                'data-pattern' => '^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}',
                'data-error-pattern' => 'Email inválido',
                'after' => $buttonAfter)); ?>
        </div>
        <div class="span4">
            <label class="required" for="input-rg">RG</label>
            <?=$form->input('FormandoProfile.rg', array(
                'label' => false,
                'div' => 'input-control text',
                'error' => false,
                'id' => 'input-rg',
                'max-lenght' => 10,
                'data-required' => 'true',
                'data-description' => 'notEmpty',
                'data-describedby' => 'input-rg',
                'data-error' => 'Digite o RG',
                'after' => $buttonAfter));
            ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
            <label>Celular</label>
            <?=$form->input('FormandoProfile.tel_celular',array(
                'label' => false,
                'alt' => 'celphone',
                'div' => 'input-control text',
                'error' => false,
                'after' => $buttonAfter)); ?>
        </div>
        <div class="span4">
            <label class="required" for="senha">Senha</label>
            <?=$form->input('senha',array(
                'label' => false,
                'div' => 'input-control password',
                'error' => false,
                'type' => 'password',
                'id' => 'senha',
                'data-description' => 'notEmpty',
                'data-describedby' => 'senha',
                'after' => $buttonAfter,
                'data-required' => 'true',
                'data-pattern' => '^.{5,}$',
                'data-error-pattern' => 'Deve ter no minimo 5 digitos')); ?>
        </div>
        <div class="span4">
            <label class="required" for="confirmar">Confirmar</label>
            <?=$form->input('confirmar',array(
                'label' => false,
                'div' => 'input-control password',
                'error' => false,
                'type' => 'password',
                'id' => 'confirmar',
                'data-description' => 'notEmpty',
                'data-describedby' => 'confirmar',
                'after' => $buttonAfter,
                'data-required' => 'true',
                'data-pattern' => '^.{5,}$',
                'data-error-conditional' => 'Senha não coincide',
                'data-error-pattern' => 'Deve ter no minimo 5 digitos',
                'data-conditional' => 'senha')); ?>
        </div>
    </div>
    <button type="submit" class="bg-color-blue">
        Adicionar
        <i class="icon-contact"></i>
    </button>
    <?=$form->end(array('label' => false, 'div' => false, 'class' => 'hide')); ?>
</div>
