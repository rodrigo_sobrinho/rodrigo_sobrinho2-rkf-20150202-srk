<div class="row-fluid">
    <br />
    <h2 class="fg-color-red">Dados Da Formatura</h2>
    <br />
</div>
<div class="row-fluid">
    <?=$form->input('FormandoProfile.curso_turma_id',array(
        'label' => array('text' => 'Curso','class' => 'required'),
        'div' => 'input-control text span8',
        'error' => false,
        'type' => 'select',
        'data-required' => 'true',
        'options' => $cursoTurma,
        'class' => 'chosen')); ?>
    <div class="span4">
        <label>Sala</label>
        <?=$form->input('FormandoProfile.sala',array(
            'label' => false,
            'div' => 'input-control text',
            'error' => false,
            'after' => $buttonAfter)); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span2">
        <?=$form->input('FormandoProfile.tam_camiseta',array(
            'label' => 'Camiseta',
            'type' => 'select',
            'class' => 'chosen',
            'options' => $fields['tam_camiseta'],
            'div' => 'input-control',
            'error' => false)); ?>
    </div>
    <?php $calcado = array(); for($a = 33; $a <= 49; $a+=2) $calcado[$a] = "$a / ".($a+1)?>
    <div class="span2">
        <?=$form->input('FormandoProfile.numero_havaiana',array(
            'label' => 'Calçado',
            'type' => 'select',
            'class' => 'chosen',
            'options' => $calcado,
            'div' => 'input-control',
            'error' => false)); ?>
    </div>
</div>
<div class="row-fluid">
    <br />
    <h2 class="fg-color-red">Dados Pessoais</h2>
    <br />
</div>
<div class="row-fluid">
    <div class="span6">
        <label class="required" for="input-nome">Nome</label>
        <?=
        $form->input('Usuario.nome', array(
            'label' => false,
            'div' => 'input-control text',
            'error' => false,
            'id' => 'input-nome',
            'data-required' => 'true',
            'data-description' => 'notEmpty',
            'data-describedby' => 'input-nome',
            'data-error' => 'Digite seu Nome',
            'after' => $buttonAfter));
        ?>
    </div>
    <div class="span3">
        <label>RG</label>
        <?=$form->input('FormandoProfile.rg',array(
            'label' => false,
            'div' => 'input-control text',
            'error' => false,
            'after' => $buttonAfter)); ?>
    </div>
    <div class="span3">
        <label>CPF</label>
        <?=$form->input('FormandoProfile.cpf',array(
            'label' => false,
            'div' => 'input-control text',
            'error' => false,
            'alt' => 'cpf',
            'maxlength' => '14',
            'after' => $buttonAfter)); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span3">
        <label class="required" for="input-email">Email</label>
        <?=
        $form->input('Usuario.email', array(
            'label' => false,
            'div' => 'input-control text',
            'error' => false,
            'id' => 'input-email',
            'class' => 'verificaEmail',
            'data-required' => 'true',
            'data-description' => 'notEmpty',
            'data-describedby' => 'input-email',
            'data-error' => 'Digite seu Email',
            'data-pattern' => '^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}',
            'data-error-pattern' => 'Email inválido',
            'after' => $buttonAfter)); ?>
    </div>
    <div class="span3">
        <label class="required" for="input-conf-email">Confirmar Email</label>
        <?=
        $form->input('Usuario.email_confirmar', array(
            'label' => false,
            'div' => 'input-control text',
            'error' => false,
            'id' => 'input-conf-email',
            'data-required' => 'true',
            'data-description' => 'notEmpty',
            'data-describedby' => 'input-conf-email',
            'data-error' => 'Digite seu Email',
            'data-pattern' => '^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}',
            'data-error-pattern' => 'Email inválido',
            'data-error-conditional' => 'Email não coincide',
            'data-conditional' => 'email',
            'after' => $buttonAfter)); ?>
    </div>
    <div class="span3">
        <label for="input-email-secundario">Email Alternativo</label>
        <?=
        $form->input('Usuario.email_secundario', array(
            'label' => false,
            'div' => 'input-control text',
            'error' => false,
            'id' => 'input-email-secundario',
            'data-pattern' => '^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}',
            'data-error-pattern' => 'Email Alternativo inválido',
            'data-conditional' => 'email-secundario',
            'after' => $buttonAfter)); ?>
    </div>
    <div class="span2">
        <label class="required" for="data-nascimento">Data Nasc</label>
        <?=$form->input('FormandoProfile.data_nascimento', array(
            'label' => false,
            'type' => 'text',
            'dateFormat' => 'DMY',
            'id' => 'data-nascimento',
            'data-required' => 'true',
            'alt' => '99/99/9999',
            'class' => 'datepicker',
            'div' => 'input-control',
            'error' => false)); ?>
    </div>
    <div class="span1">
        <label for="sexoFormando">Sexo</label>
        <label class="input-control switch sexo" onclick>
            <input value="M" type="checkbox" name='data[FormandoProfile][sexo]' id='sexoFormando'
                <?=isset($this->data['FormandoProfile']['sexo']) ? 'checked="checked"' : ''?>>
            <span class="helper sexo-selecionado"></span>
        </label>
    </div>
</div>
<div class="row-fluid">
    <br />
    <h2 class="fg-color-red">Dados De Contato</h2>
    <br />
</div>
<div class="row-fluid">
    <div class="span3">
        <label>Tel Comercial</label>
        <?=$form->input('FormandoProfile.tel_comercial',array(
            'label' => false,
            'div' => 'input-control text',
            'error' => false,
            'alt' => 'phone',
            'after' => $buttonAfter));
        ?>
    </div>
    <div class="span3">
        <label class="required">Tel Resid</label>
        <?=$form->input('FormandoProfile.tel_residencial',array(
            'label' => false,
            'div' => 'input-control text',
            'error' => false,
            'data-required' => 'true',
            'alt' => 'phone',
            'after' => $buttonAfter));
        ?>
    </div>
    <div class="span3">
        <label class="required">Celular</label>
        <?=$form->input('FormandoProfile.tel_celular',array(
            'label' => false,
            'div' => 'input-control text',
            'alt' => 'celphone',
            'data-required' => 'true',
            'error' => false,
            'after' => $buttonAfter));
        ?>
    </div>
    <div class="span3">
        <label>Nextel</label>
        <?=$form->input('FormandoProfile.nextel',array(
            'label' => false,
            'div' => 'input-control text',
            'error' => false,
            'after' => $buttonAfter));
        ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span6">
        <label class="required" for="nome-pai">Nome Pai</label>
        <?=$form->input('FormandoProfile.nome_pai',array(
            'label' => false,
            'div' => 'input-control text',
            'error' => false,
            'id' => 'nome-pai',
            'data-required' => 'true',
            'data-description' => 'notEmpty',
            'data-describedby' => 'nome-pai',
            'data-error' => 'Preencha o campo',
            'after' => $buttonAfter)); ?>
    </div>
    <div class="span3">
        <label>Tel Pai</label>
        <?=$form->input('FormandoProfile.tel_pai',array(
            'label' => false,
            'div' => 'input-control text',
            'error' => false,
            'alt' => 'phone',
            'after' => $buttonAfter));
        ?>
    </div>
    <div class="span3">
        <label>Celular Pai</label>
        <?=$form->input('FormandoProfile.tel_cel_pai',array(
            'label' => false,
            'div' => 'input-control text',
            'error' => false,
            'alt' => 'celphone',
            'after' => $buttonAfter));
        ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span6">
        <label class="required" for="nome-mae">Nome M&atilde;e</label>
        <?=$form->input('FormandoProfile.nome_mae',array(
            'label' => false,
            'div' => 'input-control text',
            'error' => false,
            'id' => 'nome-mae',
            'data-required' => 'true',
            'data-description' => 'notEmpty',
            'data-describedby' => 'nome-mae',
            'data-error' => 'Preencha o campo',
            'after' => $buttonAfter)); ?>
    </div>
    <div class="span3">
        <label>Tel M&atilde;e</label>
        <?=$form->input('FormandoProfile.tel_mae',array(
            'label' => false,
            'div' => 'input-control text',
            'error' => false,
            'alt' => 'phone',
            'after' => $buttonAfter));
        ?>
    </div>
    <div class="span3">
        <label>Celular M&atilde;e</label>
        <?=$form->input('FormandoProfile.tel_cel_mae',array(
            'label' => false,
            'div' => 'input-control text',
            'error' => false,
            'alt' => 'celphone',
            'after' => $buttonAfter));
        ?>
    </div>
</div>