<?php

class Agenda extends AppModel {

    var $name = 'Agenda';
    var $useTable = 'agendas';
    var $validate = array(
        'titulo' => array(
            'vazio' => array(
                'rule' => 'notEmpty',
                'message' => 'O agendamento não pode ficar sem um título'
            ),
            'tamanho' => array(
                'rule' => array('maxLength', 100),
                'message' => 'O titulo deve possuir no máximo 100 caracteres'
            )
        ),
        'descricao' => array(
            'vazio' => array(
                'rule' => 'notEmpty',
                'message' => 'Escreva uma descrição para seu agendamento'
            ),
        ),
        'local' => array(
            'vazio' => array(
                'rule' => 'notEmpty',
                'message' => 'Indique um local para o evento'
            ),
            'tamanho' => array(
                'rule' => array('maxLength', 200),
                'message' => 'O local deve possuir no máximo 200 caracteres'
            )
        ),
        'data' => array(
            'vazio' => array(
                'rule' => 'notEmpty',
                'message' => 'Escolha uma data-hora válida'
            )
        )
    );
    var $belongsTo = array(
        'Criador' => array(
            'className' => 'Usuario',
            'foreignKey' => 'usuarios_id'
        ),
        'Turma' => array(
            'className' => 'Turma',
            'foreignKey' => 'turmas_id'
        )
    );
    var $hasAndBelongsToMany = array(
        'Compartilhado' =>
        array(
            'className' => 'Usuario',
            'joinTable' => 'agendas_usuarios',
            'foreignKey' => 'agenda_id',
            'associationForeignKey' => 'usuario_id',
            'unique' => true,
            'fields' => 'id, nome, grupo'
        )
    );

}

?>
