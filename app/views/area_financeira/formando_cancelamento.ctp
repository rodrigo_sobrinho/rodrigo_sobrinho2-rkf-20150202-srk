<script type="text/javascript">
jQuery(document).ready( function() {
	jQuery('#submit-cancelamento').click( function() { jQuery(this).hide('slow'); jQuery('#alerta-cancelamento').show('slow'); });

	jQuery('.cancelar').click( function() {
		if(jQuery(this).hasClass('continuar'))
			jQuery('#info-cancelamento').show('slow');
		else
			window.location.href = "/<?=$this->params['prefix']?>";
	});

	jQuery('#confirmar-cancelamento').live('click', function() {
		jQuery.colorbox({
			html : '<p id="p-colorbox">Aguarde enquanto processamos o cancelamento</p>',
			overlayClose : false,
			escKey : false,
			width: 600,
			height: 500,
			onCleanup : function() { jQuery('#cboxClose').hide(); },
			onComplete : function() {
				jQuery.ajax({
					url : "<?=$html->url(array($this->params['prefix'] => true,"controller" => "area_financeira", "action" => "cancelar"));?>",
					type : 'POST',
					data : { usuario : <?=$formando['FormandoProfile']['usuario_id']?>, despesa : jQuery('#saldo-cancelamento').val() },
					dataType : 'json',
					error: function(data) {
						jQuery('#p-colorbox').html('Ocorreu um erro ao solicitar o cancelamento. Por favor entre em contato conosco.');
						jQuery('#cboxClose').show();
					},
					success : function(data) {
						jQuery('#cboxClose').show();
						if(data.error) {
							jQuery('#p-colorbox').html(data.message);
						} else {
							var html = "Caro formando, o cancelamento do contrato gerou um protocolo, <span style='color:red'>" + data.protocolo + "</span>.<br />";
							if(jQuery('#saldo-cancelamento').val() > 10) {
								html+= "Clique no bot&atilde;o abaixo para gerar boleto do saldo devedor gerado.<br /><br />";
								html+= '<?=$html->link('Gerar Boleto Cancelamento',array($this->params['prefix'] => true, 'action' => 'gerar_boleto_cancelamento',$formando['FormandoProfile']['usuario_id']),array('class' => 'submit botao-gerar-boleto-cancelamento button', 'target' => '_blank')); ?>';
								html+= "<br />";
							} else if(jQuery('#saldo-cancelamento').val() < 0) {
								html+= "Voc&ecirc; tem um cr&eacute;dito de R$" + (jQuery('#saldo-cancelamento').val()*-1).toFixed(2) + "<br />";
								html+= "Clique no bot&atilde;o abaixo e preencha seus dados banc&aacute;rios para depositarmos seu cr&eacute;dito.<br /><br />";
								html+= '<a href="/<?=$this->params['prefix']?>/solicitacoes/visualizar/' + data.protocolo + '" class="button submit">Dados Financeiros</a>';
								html+= "<br />";
							} else {
								html+= "Clique <a href='/<?=$this->params['prefix']?>/solicitacoes/visualizar/" + data.protocolo + "'>aqui</a> para visualizar o status do cancelamento";
							}
							html+= "<br />Consulte o status do cancelamento usando o protocolo gerado no link \"Solicita&ccedil;&otilde;es\" na sua &Aacute;rea de formando.";
							jQuery('#p-colorbox').html(html);
						}
					}
				});
			}
		});
	});
});
</script>
<style type="text/css">
.legenda span { width:auto!important; height:auto!important }
#cboxLoadedContent { background-color:white!important; border:solid 1px black; border-radius:4px; font-size:14px; padding:20px }
</style>
<label id="conteudo-titulo" class="box-com-titulo-header">Cancelamento de Contrato</label>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div style="overflow:hidden; width:100%; float:left">
	<div class="legenda" style="width:30%">
		<table>
			<tr>
				<td style="padding-bottom:10px" colspan="2"><h2>Resumo Financeiro</h2></td>
			</tr>
			<tr>
				<td>Valor do Contrato</td>
				<td>R$ <?=$formando['FormandoProfile']['valor_adesao']?></td>
			</tr>
			<tr>
				<td>Quantidade de Parcelas</td>
				<td><em><?=$formando['FormandoProfile']['parcelas_adesao']?></em></td>
			</tr>
			<tr>
				<td>Total Pago</td>
				<td><em>R$ <?=$totalPago?></em></td>
			</tr>
			<tr>
				<td>Saldo Em Aberto</td>
				<td><em>R$ <?=$saldoEmAberto?></em></td>
			</tr>
			<tr>
				<td></td>
				<td style="padding-top:10px"><span class="submit button" id='submit-cancelamento'>Cancelar</span></td>
			</tr>
		</table>
	</div>
	<div class="legenda" style="width:40%; display:none" id='alerta-cancelamento'>
		<table>
			<tr>
				<td style="padding-bottom:10px" colspan="2"></td>
			</tr>
			<tr>
				<td style="padding-bottom:10px; color:red" colspan="2">
					ATEN&Ccedil;&Atilde;O: Este processo &eacute; irrevers&iacute;vel.
					<br />
					<br />
					Com o cancelamento do contrato o formando s&oacute; poder&aacute; comparecer &agrave; festa fazendo nova ades&atilde;o e gerando novo contrato com valores atualizados.
				</td>
			</tr>
			<tr>
				<td style="padding-bottom:13px" colspan="2"></td>
			</tr>
			<tr>
				<td>
					<span style="font-size:12px" class="submit button cancelar continuar">Entendo os riscos. Quero continuar o cancelamento</span>
				</td>
				<td>
					<span style="font-size:12px" class="submit button cancelar desistir">Desistir do Cancelamento</span>
				</td>
			</tr>
			<tr>
				<td style="padding-bottom:13px" colspan="2"></td>
			</tr>
		</table>
	</div>
	<div class="legenda" style="width:30%; display:none" id='info-cancelamento'>
	<table>
			<tr>
				<td style="padding-bottom:10px" colspan="2"><h2>Cancelamento de Contrato</h2></td>
			</tr>
			<tr>
				<td>
					Multa por Cancelamento
					<br />
					(20% Valor do Contrato)
				</td>
				<td style="color:red">R$ <?=$formando['FormandoProfile']['valor_adesao']*0.2?></td>
			</tr>
			<tr>
				<?php $saldoCancelamento = ($formando['FormandoProfile']['valor_adesao']*0.2) - $totalPago; ?>
				<?php if($saldoCancelamento < 0) :?>
				<td style="color:green">Saldo Positivo</td><td style="color:green">R$ <?=$saldoCancelamento*-1?></td>
				<?php else : ?>
				<td style="color:red">Saldo Negativo</td><td style="color:red">R$ <?=$saldoCancelamento?></td>
				<?php endif; ?>
				<input type='hidden' id='saldo-cancelamento' value='<?=$saldoCancelamento?>' />
			</tr>
			<tr>
				<td></td>
				<td style="padding-top:10px">
					<span style="font-size:12px" class="submit button" id='confirmar-cancelamento'>Confirmar Cancelamento</span>
				</td>
			</tr>
		</table>
	</div>
	</div>
</div>