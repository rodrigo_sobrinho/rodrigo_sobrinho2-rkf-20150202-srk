var message = {
	exibir : function(mensagem,tipo) {
		$('.message').addClass(tipo);
		$('.message').html(mensagem);
		$('.message').fadeIn(500,
			function() {
				setTimeout(function() { $('.message').fadeOut(500,function() { $(this).removeClass(tipo)})},5000);
			}
		);
	}
}

function obterHoraAtual() {
	var data = new Date();
	var hora = strpad(data.getHours(),2,'0');
	var minuto = strpad(data.getMinutes(),2,'0');
	var segundo = strpad(data.getSeconds(),2,'0');
	var horario = hora + ":" + minuto + ":" + segundo;
	return horario;
}

function strpad(string,length,pad) {
    var output = '' + string;
    while(output.length < length)
    	output = pad + output;
    return output;
}

function initRelogio() {
	jQuery('#horario').html(obterHoraAtual());
	setInterval(function() { jQuery('#horario').html(obterHoraAtual())},1000);
}

function initCalendar() {
	var data = new Date();
	jQuery('#calendario-dia').html(strpad(data.getDate(),2,'0'));
}

function number_format( number, decimals, dec_point, thousands_sep ) {
    var n = number, prec = decimals;
    n = !isFinite(+n) ? 0 : +n;
    prec = !isFinite(+prec) ? 0 : Math.abs(prec);
    var sep = (typeof thousands_sep == "undefined") ? ',' : thousands_sep;
    var dec = (typeof dec_point == "undefined") ? '.' : dec_point;
 
    var s = (prec > 0) ? n.toFixed(prec) : Math.round(n).toFixed(prec); //fix for IE parseFloat(0.55).toFixed(0) = 0;
 
    var abs = Math.abs(n).toFixed(prec);
    var _, i;
 
    if (abs >= 1000) {
        _ = abs.split(/\D/);
        i = _[0].length % 3 || 3;
 
        _[0] = s.slice(0,i + (n < 0)) +
              _[0].slice(i).replace(/(\d{3})/g, sep+'$1');
 
        s = _.join(dec);
    } else {
        s = s.replace('.', dec);
    }
 
    return s;
}

function ucfirst(str) { 
	return str.substr(0,1).toUpperCase()+str.substr(1) 
}

function stringMoneyToNumber(stringMoney) {
	number = stringMoney.replace('R$','');
	number = number.replace('.','');
	number = number.replace(',','.');
	return number;
}

function stringToFloat(string,decimals) {
	number = string;
	number = parseFloat(number).toFixed(decimals);
	number = parseFloat(number);
	return number;
}