<?php if (sizeof($adesoes) == 0) { ?>
    <h2 class="fg-color-red">Nenhuma Despesa de ades&atilde;o cadastrada</h2>
<?php } else { ?>
    <h3>Despesas Com Ades&atilde;o</h3>
    <br />
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col">Parcela</th>
                <th scope="col">Valor<br />Base</th>
                <th scope="col">Data de<br />Vencimento</th>
                <th scope="col">Data de<br />Pagamento</th>
                <th scope="col">Valor<br />Creditado</th>
                <th scope="col">Valor<br />IGPM</th>
                <th scope="col">Status<br />(Valor Base)</th>
                <th scope="col">Saldo da<br />Parcela</th>
                <th scope="col">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($adesoes as $adesao): ?>
                <?php
                $color = "";
                if ($adesao['status'] == "aberta" && strtotime($adesao['data_vencimento']) < strtotime('now'))
                    $color = "fg-color-red";
                elseif ($adesao['status'] == "paga" && round($adesao['saldo_parcela'], 2) <= 0)
                    $color = "fg-color-green";
                elseif ($adesao['status'] == "paga" && round($adesao['saldo_parcela'], 2) > 0)
                    $color = "fg-color-yellow";
                if (round($adesao['saldo_parcela'], 2) == 0)
                    $saldoParcela = "-";
                elseif ($adesao['saldo_parcela'] < 0)
                    $saldoParcela = "R$" . number_format($adesao['saldo_parcela'] * -1, 2, ',', '.');
                else
                    $saldoParcela = "R$" . number_format($adesao['saldo_parcela'], 2, ',', '.');
                ?>
                <tr>
                    <td><?= $adesao['parcela'] . ' de ' . $adesao['total_parcelas']; ?></td>
                    <td><?= "R$" . number_format($adesao['valor'], 2, ',', '.') ?></td>
                    <td><?= date('d/m/Y', strtotime($adesao['data_vencimento'])) ?></td>
                    <td>
                        <?php
                        if ($adesao['status'] == 'paga' && !empty($adesao['data_pagamento']))
                            echo date('d/m/Y', strtotime($adesao['data_pagamento']));
                        else
                            echo '-';
                        ?>
                    </td>
                    <td><?= "R$" . number_format($adesao['valor_pago'], 2, ',', '.') ?></td>
                    <td>
                        <?= $adesao['correcao_igpm'] > 0 ? "R$" . number_format($adesao['correcao_igpm'], 2, ',', '.') : "N&atilde;o Aplicado" ?>
                        <?php if ($adesao['status_igpm'] == 'recriado') : ?>
                            <i class="icon-help" rel="tooltip" data-html="true"
                               title="Esta Parcela de IGPM foi embutida em uma parcela única. <br />Encontrada na aba IGPM"></i>
                           <?php endif; ?>
                    </td>
                    <td class="<?= $color != "" ? "$color" : "" ?>"><?= $adesao['status'] ?></td>
                    <td class="<?= $color != "" ? "$color" : "" ?>"><?= $saldoParcela ?></td>
                    <?php if($formando['turma_id'] != 3607) : ?>
                    <td>
                        <?php if (/*$gerarBoleto && */$adesao['status'] == "aberta") : ?>
                            <?php if (strtotime('now') > strtotime($adesao['data_vencimento']) &&
                                    $adesao['status'] == 'aberta') {
                                ?>
                                <div class="dropdown">
                                    <a class="dropdown-toggle button mini default" data-toggle="dropdown" href="#">Gerar Boleto
                                        <i class="icon-arrow-20"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="/<?= $this->params['prefix'] ?>/area_financeira/gerar_boleto/<?= $adesao['id'] ?>"
                                               tabindex="-1" target="_blank">
                                                Gerar Boleto
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/<?= $this->params['prefix'] ?>/area_financeira/gerar_boleto_com_multa/<?= $adesao['id'] ?>"
                                               tabindex="-1" target="_blank">
                                                Gerar Boleto Com Multa
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            <?php } else { ?>
                                <a href="<?= $this->params['prefix'] ?>/area_financeira/gerar_boleto/<?= $adesao['id'] ?>"
                                   class='button mini default' target='_blank'>
                                    Gerar Boleto
                                    <i class="icon-new"></i>
                                </a>
                            <?php } ?>
                        <?php endif; ?>
                    </td>
                    <?php endif; ?>
                </tr>
            <?php endforeach; ?>
            <?php if($igpm) { ?>
                <tr>
                    <td>IGPM</td>
                    <td>
                        <?= "R$" . number_format($igpm['valor'], 2, ',', '.') ?>
                    </td>
                    <td colspan="2"></td>
                    <td colspan="2">
                        <?= "R$" . number_format($igpm['valor_pago'], 2, ',', '.') ?>
                    </td>
                    <td colspan="2"
                        class="<?=$igpm['status'] == 'aberta' ? "fg-color-red" : "fg-color-green" ?>">
                        <?= "R$" . number_format($igpm['saldo'], 2, ',', '.') ?>
                    </td>
                    <td>
                        <a class='button mini default tab-show' href="#igpm"
                            data-toggle="tab">
                            <i class="icon-plus"></i>
                            Informa&ccedil;&otilde;s
                         </a>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
<?php } ?>
