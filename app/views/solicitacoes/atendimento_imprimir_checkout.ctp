<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/wizard.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/max/bootstrap/wizard.js"></script>
<style type="text/css">
    @media print {
        .area_print, .area_print * {
            visibility: visible !important;
        }
        .area_print {
            position: absolute !important;
            left:     0        !important;
            top:      0px     !important;
        }
        h5 {font-size: 14px!important }
        h4 {font-size: 12px!important }
    }
</style>
<script type="text/javascript">
    $('#wizard').wizard();
</script>
<div class="row-fluid area_print">
    <div class="span9">
        <div id="wizard" class="wizard">
            <ul class="steps">
                <li data-target="#step1" class="active">
                    Protocolo de Atendimento / Turma - <?=$formando['turma_id']?>
                    <span class="chevron"></span>
                </li>
            </ul>
        </div>
        <div class="step-content">
            <div class="row-fluid">
                <p>
                    <h5>
                        <strong class="fg-color-red">
                            <?=strtoupper($formando['grupo']) . ": " . strtoupper($formando['nome']) ?>
                        </strong>
                        <strong class="fg-color-black pull-right">
                            <?=date('d/m/Y &\a\g\r\a\v\e;\s H:i:s',strtotime($protocolo['Protocolo']['data_cadastro']))?>
                        </strong>
                    </h5>
                </p>
            </div>
            <br />
            <div class="row-fluid">
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <td colspan="2" width="50%"><h4 class='titulo'>Informa&ccedil;&otilde;es da ades&atilde;o</h4></td>
                            <td colspan="2" width="50%"><h4 class='titulo'>Resumo financeiro</h4></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td width="200">Código</td>
                            <td><b><?=$formando['codigo_formando']?></b></td>
                            <td>Valor de Contrato</td>
                            <td class='valor-contrato'>R$<?= number_format($valorAdesao, 2, ',', '.'); ?></td>
                        </tr>
                        <tr>
                            <td>RG</td>
                            <td><?=$formando['rg'] ?></td>
                            <td>Valor de IGPM</td>
                            <td><?= $totalIGPM > 0 ? "R$" . number_format($totalIGPM, 2, ',', '.') : "-"; ?></td>
                        </tr>
                        <tr>
                            <td>Protocolo</td>
                            <td><?=$protocolo['Protocolo']['protocolo'] ?></td>
                            <td>Valor de Contrato c/ Correção</td>
                            <td class='total-contrato'>R$<?= number_format($valorAdesao + $totalIGPM, 2, ',', '.'); ?></td>
                        </tr>
                        <tr>
                            <td width="200">Data contrato da Turma</td>
                            <td><?= date('d/m/Y', strtotime($turma['Turma']['data_assinatura_contrato'])); ?></td>
                            <td>Valor de Campanhas</td>
                            <td class='total-campanha'><?= $valorCampanhas > 0 ? "R$" . number_format($valorCampanhas, 2, ',', '.') : "-" ?></td>
                        </tr>
                        <tr>
                            <td>Data Adesão</td>
                            <td><?= date('d/m/Y', strtotime($formando['data_adesao'])); ?></td>
                            <td>Valor de Itens do Checkout</td>
                            <td><?= $totalItensCheckout > 0 ? "R$" . number_format($totalItensCheckout, 2, ',', '.') : "-" ?></td>
                        </tr>
                        <tr>
                            <td>Data IGPM</td>
                            <td>
                                <?=!empty($turma['Turma']['data_igpm']) ?
                                    date('d/m/Y', strtotime($turma['Turma']['data_igpm'])) :
                                    "IGPM não aplicado"; ?>
                            </td>
                            <td>Total Recebido Antes de Checkout</td>
                            <td><?= $totalPago > 0 ? "R$" . number_format($totalPago, 2, ',', '.') : "-" ?></td>
                        </tr>
                        <tr>
                            <td>Mesas do contrato</td>
                            <td><?= $turma['Turma']['mesas_contrato']; ?></td>
                            <td>Total Recebido no Checkout</td>
                            <td><?= $totalPagoCheckout > 0 ? "R$" . number_format($totalPagoCheckout, 2, ',', '.') : "-" ?></td>
                        </tr>
                        <tr>
                            <td>Convites do contrato</td>
                            <td><?= $turma['Turma']['convites_contrato']; ?></td>
                            <td>Saldo Final</td>
                            <td><?= $saldoTotal != 0 ? "R$" . number_format($saldoTotal, 2, ',', '.') : "-" ?></td>
                        </tr>
                        <tr>
                            <td>Número de Parcelas do Contrato</td>
                            <td><?= $formando['parcelas_adesao']; ?></td>
                            <td colspan='2'>&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
                <br />
                <?php if(!empty($pagamentosCheckout)) : ?>
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <td colspan="4"><h4>Pagamentos Recebidos Durante o Checkout</h4></td>
                        </tr>
                        <tr>
                            <td>Valor</td>
                            <td>Tipo</td>
                            <td>Referência</td>
                            <td>Vencimento</td>
                        </tr>
                    </thead>
                    <?php foreach ($pagamentosCheckout as $pagoCheckout) : ?>
                    <tr>
                        <td>R$<?=number_format($pagoCheckout['CheckoutFormandoPagamento']['valor'], 2, ',', '.') ?></td>
                        <td><?=$pagoCheckout['CheckoutFormandoPagamento']['tipo'] ?></td>
                        <td><?=implode(" - ", array_map("ucfirst", explode(";", $pagoCheckout['CheckoutFormandoPagamento']['referente']))) ?></td>
                        <td><?=date('d/m/Y', strtotime($pagoCheckout['CheckoutFormandoPagamento']['data_vencimento'])); ?></td>
                    </tr>
                    <?php endforeach; ?>
                </table>
                <?php endif; ?>
                <br />
                <div class="row-fluid">
                    <h4>
                        Eu, <em class="fg-color-red"><strong><?= $atendente['Usuario']['nome'] ?></strong></em>, confirmo que o formando, <em class="fg-color-gray"><strong><?= $formando['nome'] ?></strong></em>,
                        quitou suas pendências financeiras e têm direito a retirada dos seguintes itens para festa de formatura em nome da <strong>
                        RK Formaturas</strong>:
                    </h4>
                </div>
                <br />
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <td><h4>Itens retirados</h4></td>
                        </tr>
                    </thead>
                    <?php if (count($itensRetirados) > 0) : ?>
                    <tbody>
                    <?php foreach ($itensRetirados as $itemRetirado) : ?>
                        <tr>
                            <td><?= $itemRetirado['descricao'] ?><br />&nbsp;</td>
                        </tr>
                    <?php endforeach; ?>
                    <?php foreach ($itensCampanhaRetirados as $itemCampanhaRetirado) : ?>
                        <tr>
                            <td><?= $itemCampanhaRetirado['descricao'] ?><br />&nbsp;</td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                    <?php else : ?>
                    <tr>
                        <td>Nenhum item retirado.</td>
                    </tr>
                    <?php endif; ?>
                </table>
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <td><h4>Total</h4></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?=$totalMesas?> - Mesa(s)</td>
                        </tr>
                        <tr>
                            <td><?=$totalConvites?> - Convite(s)</td>
                        </tr>
                        <tr>
                            <td><?=$totalMeias?> - Meia(s) Entrada(s)</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>