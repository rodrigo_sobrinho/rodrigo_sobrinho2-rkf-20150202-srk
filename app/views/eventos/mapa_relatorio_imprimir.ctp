<?php if($mapa) : ?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css"><div class="row-fluid">
    <h2>
        <?=$turma['Turma']['id']?> - Relat&oacute;rio de mesas turma <?=$turma['Turma']['nome']?>
    </h2>
</div>
<br />
<div class="row-fluid">
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="15%">Cod</th>
                <th scope="col" width="40%">Nome</th>
                <th scope="col" width="10%">Qtde Mesas</th>
                <th scope="col" width="35%">N&ordm; Mesas</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($formandos as $i => $formando) : $numeros = array(); ?>
            <?php foreach($formando['EventoMapaLocal'] as $local)
                    $numeros[] = "{$local['EventoMapaColuna']['nome']}{$local['numero']}"; ?>
            <tr>
                <td><?=$formando['ViewFormandos']['codigo_formando']; ?></td>
                <td><?=$formando['ViewFormandos']['nome']; ?></td>
                <td><?=count($formando['EventoMapaLocal'])?></td>
                <td><?=implode(',', $numeros)?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="2" class="paginacao"></td>
                <td colspan="2">
                    <span class="label label-info">
                    <?=count($formandos)?> formandos
                    </span>
                </td>
            </tr>
        </tfoot>
    </table>
</div>
<?php else : ?>
<h2 class="fg-color-red">Nenhum mapa de mesa encontrado para esta turma</h2>
<?php endif; ?>