<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of files_controller
 *
 * @author brunopapa
 */
class ArquivosController extends AppController {

    var $name = 'Arquivos';
    var $nomeDoTemplateSidebar = 'turmas';
    var $uses = array('Arquivo', 'Turma');
    var $components = array('GerenciadorDeDownload');
    var $app = array('baixar','listar','get');
    // Padrão de paginação
    var $paginate = array(
        'limit' => 25,
        'order' => array(
            'Arquivo.id' => 'desc'
        )
    );
    
    function beforeFilter() {
        parent::beforeFilter();
    }
    
    function app_listar() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array();
        if(isset($this->data['turma_id'])) {
            $this->Arquivo->unbindModelAll();
            $arquivos = $this->Arquivo->find('list',array(
                'conditions' => array(
                    'Arquivo.turma_id' => $this->data['turma_id'],
                    "Arquivo.deletado is null"
                )
            ));
            $json['arquivos'] = $arquivos;
        }
        echo json_encode($json);
    }
    
    function app_get() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $json = array('arquivo' => false);
        if(isset($this->data['arquivo_id'])) {
            $this->Arquivo->unbindModel(array(
                'hasAndBelongsToMany' => array('Mensagem')
            ),false);
            $this->Arquivo->recursive = 1;
            $arquivo = $this->Arquivo->read(null,$this->data['arquivo_id']);
            if($arquivo) {
                $json['arquivo'] = array(
                    'Arquivo' => array(
                        'id' => $arquivo['Arquivo']['id'],
                        'nome' => $arquivo['Arquivo']['nome'],
                        'data_cadastro' => $arquivo['Arquivo']['criado']
                    ),
                    'Usuario' => array(
                        'id' => $arquivo['Usuario']['id'],
                        'nome' => $arquivo['Usuario']['nome'],
                        'diretorio_foto_perfil' => $arquivo['Usuario']['diretorio_foto_perfil']
                    )
                );
            }
        }
        echo json_encode($json);
    }
    
    function app_baixar($id) {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        if($id) {
            $this->Arquivo->id = $id;
            $arquivo = $this->Arquivo->read();
            if (!empty($arquivo)) {
                header("Pragma: public");
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Cache-Control: private", false);
                header("Content-Type: " . $arquivo['Arquivo']['tipo']);
                header("Content-Disposition: attachment; filename=\"" . $arquivo['Arquivo']['nome'] . "\";");
                header("Content-Transfer-Encoding: binary");
                header("Content-Length: " . $arquivo['Arquivo']['tamanho']);
                readfile($this->Arquivo->getArquivo());
            } else {
                exit();
            }
        } else {
            exit();
        }
    }
    
    function download($id) {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $arquivo = $this->Arquivo->read(null,$id);
        $json = array('error' => true);
        if($arquivo) {
            $filePath = $arquivo['Arquivo']['diretorio'].$arquivo['Arquivo']['nome_secreto'];
            if(file_exists($arquivo['Arquivo']['diretorio'].$arquivo['Arquivo']['nome_secreto'])) {
                $session = $this->GerenciadorDeDownload->iniciarDownload($filePath,$arquivo['Arquivo']['nome'],$arquivo['Arquivo']['tipo']);
                if($session) {
                    $json = array(
                        'error' => false,
                        'file' => $session
                    );
                } else {
                    $json['message'] = "Erro ao iniciar o download";
                }
            } else {
                $json['message'] = 'Arquivo não encontrado';
            }
        } else {
            $json['message'] = 'Arquivo não encontrado';
        }
        echo json_encode($json);
    }
    
    function atualizar_download($sessionId,$bytes) {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $data = $this->GerenciadorDeDownload->atualizarDownload($sessionId,$bytes);
        if($data) {
            echo json_encode(array('error' => false, 'data' => $data));
        } else {
            echo json_encode(array('error' => true));
        }
    }
    
    private function _listar() {
        $this->layout = false;
        $turma = $this->obterTurmaLogada();
        $this->set('arquivos', $this->paginate('Arquivo',
                array('Arquivo.turma_id' => $turma['Turma']['id'], 'Arquivo.categoria' => 'arquivo')));
        $this->render('_listar');
    }
    
    function jornal_mensal() {
        $this->layout = false;
        $usuario = $this->Session->read('Usuario');
        if (!empty($this->data)) {
            $this->autoRender = false;
         //   Configure::write(array('debug' => 0));
                $this->Session->write("filtros.{$usuario['Usuario']['grupo']}.jornal", $this->data['Arquivo']);
        } else {
            $options['order'] = array('Arquivo.criado' => 'asc');
            $options['conditions'] = array('Arquivo.categoria' => 'novidade');
            $filtro = $this->Session->read("filtros.{$usuario['Usuario']['grupo']}.jornal");
            if($filtro) {
                $this->data['Arquivo'] = $filtro;
                foreach ($filtro as $chave => $valor)
                    $options['conditions']['MONTH(Arquivo.criado) LIKE '] = strtolower($valor);
            }
            $options['limit'] = 20;
            $this->paginate['Arquivo'] = $options;
            $jornais = $this->paginate('Arquivo');
            $this->set('jornais', $jornais);
        }
        $this->set('meses',$meses = array(null,'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio',
            'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'));
        $this->render('jornal_mensal');
    }

    private function _manuais_procedimento() {
        $this->layout = false;
        $turma = $this->obterTurmaLogada();
        $this->set('arquivos', $this->paginate('Arquivo',
                array('Arquivo.turma_id' => $turma['Turma']['id'], 'Arquivo.categoria' => 'manual')));
        $this->render('_manuais_procedimento');
    }
    
    private function _inserir(){

        if(!empty($this->data)){
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->loadModel('Arquivo');
            $data = $_POST['data']['Arquivo'];
            $turma = $this->obterTurmaLogada();
            $usuario = $this->Session->read('Usuario');
            $data['nome_secreto'] = $this->Arquivo->generateUniqueId();
            $data['diretorio'] = $this->Arquivo->file_path();
            $data['path'] = $data['diretorio'].$data['nome_secreto'];
            $data['tipo'] = $this->data['Arquivo']['extensao'];
            if($this->Arquivo->saveBase64File($data['src'],$data['path'])) {
                $arquivo = $this->Arquivo->create();
                $arquivo['Arquivo'] = $data;
                unset($arquivo['Arquivo']['src']);
                $arquivo['Arquivo']['turma_id'] = $turma['Turma']['id'];
                $arquivo['Arquivo']['usuario_id'] = $usuario['Usuario']['id'];
                $arquivo['Arquivo']['criado'] = date('Y-m-d H:i:s');
                if($this->params['prefix'] == 'rh')
                    $arquivo['Arquivo']['categoria'] = 'manual';
                if ($this->Arquivo->save(array('Arquivo' => $arquivo['Arquivo']),
                        array('callback' => false))){
                    $this->Session->setFlash('Arquivo salvo com sucesso.',
                            'metro/flash/success');
                }else{
                    $this->Session->setFlash('Erro ao salvar o arquivo.',
                            'metro/flash/error');
                }
            }else{
                $this->Session->setFlash('Erro ao ler o arquivo.',
                            'metro/flash/error');
            }
            echo json_encode(array());
        }
    }
    
    function rh_inserir_jornal(){
        $this->layout = false;
        if(!empty($this->data)){
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->loadModel('Arquivo');
            $data = $_POST['data']['Arquivo'];
            $turma = $this->obterTurmaLogada();
            $usuario = $this->Session->read('Usuario');
            $data['nome_secreto'] = $this->Arquivo->generateUniqueId();
            $data['diretorio'] = $this->Arquivo->file_path();
            $data['path'] = $data['diretorio'].$data['nome_secreto'];
            $data['tipo'] = $this->data['Arquivo']['extensao'];
            if($this->Arquivo->saveBase64File($data['src'],$data['path'])) {
                $arquivo = $this->Arquivo->create();
                $arquivo['Arquivo'] = $data;
                unset($arquivo['Arquivo']['src']);
                $arquivo['Arquivo']['turma_id'] = $turma['Turma']['id'];
                $arquivo['Arquivo']['usuario_id'] = $usuario['Usuario']['id'];
                $arquivo['Arquivo']['criado'] = date('Y-m-d H:i:s');
                $arquivo['Arquivo']['categoria'] = 'novidade';
                if ($this->Arquivo->save(array('Arquivo' => $arquivo['Arquivo']),
                        array('callback' => false))){
                    $this->Session->setFlash('Arquivo salvo com sucesso.',
                            'metro/flash/success');
                }else{
                    $this->Session->setFlash('Erro ao salvar o arquivo.',
                            'metro/flash/error');
                }
            }else{
                $this->Session->setFlash('Erro ao ler o arquivo.',
                            'metro/flash/error');
            }
            echo json_encode(array());
        }
    }
    
    function rh_inserir_manuais_procedimento(){
        $this->_inserir();
    }
    
    function comercial_manuais_procedimento(){
        $this->_manuais_procedimento();
    }
    
    function criacao_manuais_procedimento(){
        $this->_manuais_procedimento();
    }
    
    function foto_manuais_procedimento(){
        $this->_manuais_procedimento();
    }
    
    function planejamento_manuais_procedimento(){
        $this->_manuais_procedimento();
    }
    
    function producao_manuais_procedimento(){
        $this->_manuais_procedimento();
    }
    
    function rh_manuais_procedimento(){
        $this->_manuais_procedimento();
    }
    
    function video_manuais_procedimento(){
        $this->_manuais_procedimento();
    }
    
    function comercial_inserir() {
        $this->_inserir();
    }
    
    function planejamento_inserir() {
        $this->_inserir();
    }
    
    function criacao_inserir() {
        $this->_inserir();
    }
    
    function foto_inserir() {
        $this->_inserir();
    }
    
    function producao_inserir() {
        $this->_inserir();
    }
    
    function rh_inserir() {
        $this->_inserir();
    }
    
    function video_inserir() {
        $this->_inserir();
    }
    
    function comissao_listar() {
        $this->_listar();
    }
    
    function comercial_listar() {
        $this->_listar();
    }
    
    function planejamento_listar() {
        $this->_listar();
    }
    
    function criacao_listar() {
        $this->_listar();
    }
    
    function foto_listar() {
        $this->_listar();
    }
    
    function video_listar() {
        $this->_listar();
    }
    
    function rh_listar() {
        $this->_listar();
    }
    
    private function _exibir($id) {
        $response = array('erro' => true,'mensagem' => '');
        $this->layout = false;
        $this->Arquivo->recursive = -1;
        $arquivo = $this->Arquivo->find(array('Arquivo.id' => $id));
        if($arquivo) {
            if(!$this->validar_arquivo($arquivo['Arquivo']['diretorio'] .
                $arquivo['Arquivo']['nome_secreto'])) {
                $arquivo = false;
                $response['mensagem'] = 'Arquivo Não Encontrado no Servidor';
            } elseif(!$this->validar_preview($arquivo['Arquivo']['tipo'])) {
                $response['mensagem'] = 'Visualização Não Disponível Para Este Arquivo';
            } else {
                $response['scale'] = $this->validar_imagem($arquivo['Arquivo']['tipo']);
                $response['erro'] = false;
            }
        } else {
            $response['mensagem'] = 'Arquivo Não Encontrado';
        }
        $this->set('arquivo',$arquivo);
        $this->set('response',$response);
        $this->render('_exibir');
    }
    
    function comissao_exibir($id) {
        $this->_exibir($id);
    }
    
    function comercial_exibir($id) {
        $this->_exibir($id);
    }
    
    function planejamento_exibir($id) {
        $this->_exibir($id);
    }
    
    function criacao_exibir($id) {
        $this->_exibir($id);
    }
    
    function foto_exibir($id) {
        $this->_exibir($id);
    }
    
    function rh_exibir($id){
        $this->_exibir($id);
    }
    
    function video_exibir($id) {
        $this->_exibir($id);
    }
    
    private function _preview($id) {
        $this->Arquivo->recursive = -1;
        $this->layout = false;
        $arquivo = $this->Arquivo->find(array('Arquivo.id' => $id));
        if($arquivo) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            header("Content-Type: {$arquivo['Arquivo']['tipo']}");
            readfile($arquivo['Arquivo']['diretorio'] .
                $arquivo['Arquivo']['nome_secreto']);
        } else {
            $this->render('_preview');
        }
    }
    
    function comissao_preview($id) {
        $this->_preview($id);
    }
    
    function comercial_preview($id) {
        $this->_preview($id);
    }
    
    function planejamento_preview($id) {
        $this->_preview($id);
    }
    
    function criacao_preview($id) {
        $this->_preview($id);
    }
    
    function foto_preview($id) {
        $this->_preview($id);
    }
    
    function rh_preview($id){
        $this->_preview($id);
    }
    
    function video_preview($id) {
        $this->_preview($id);
    }
    
    private function validar_arquivo($arquivo) {
        return file_exists($arquivo);
    }
    
    private function validar_imagem($tipo) {
        $retorno = false;
        if(preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $tipo))
            $retorno = true;
        return $retorno;
    }
    
    private function validar_preview($tipo) {
        $retorno = false;
        if(preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $tipo))
            $retorno = true;
        elseif(preg_match("/(pdf)$/", $tipo))
            $retorno = true;
        return $retorno;
    }

    function planejamento_index() {
        $this->comercial_index();
    }

    function planejamento_adicionar() {
        $this->comercial_adicionar();
    }

    function planejamento_visualizar($id = null) {
        $this->comercial_visualizar($id);
    }

    function planejamento_baixar($id = null) {
        $this->comercial_baixar($id);
    }
    
    function criacao_baixar($id = null) {
        $this->comercial_baixar($id);
    }
    
    function producao_baixar($id = null) {
        $this->comercial_baixar($id);
    }
    
    function atendimento_baixar($id = null) {
        $this->comercial_baixar($id);
    }
    
    function rh_baixar($id = null) {
        $this->comercial_baixar($id);
    }
    
    function foto_baixar($id = null) {
        $this->comercial_baixar($id);
    }
    
    function video_baixar($id = null) {
        $this->comercial_baixar($id);
    }

    function comercial_index() {
        $turma = $this->Session->read('turma');
        $this->set('arquivos', $this->paginate('Arquivo', array('Arquivo.turma_id' => $turma['Turma']['id'])));
    }

    function comercial_adicionar() {

        if (!empty($this->data) &&
                is_uploaded_file($this->data['Arquivo']['arquivo']['tmp_name'])) {
            $arquivo = $this->Arquivo->create();

            $session_turma = $this->Session->read('turma');
            $usuario = $this->Session->read('Usuario');

            $arquivo['Arquivo']['nome'] = $this->data['Arquivo']['arquivo']['name'];
            $arquivo['Arquivo']['tipo'] = $this->data['Arquivo']['arquivo']['type'];
            $arquivo['Arquivo']['tamanho'] = $this->data['Arquivo']['arquivo']['size'];
            $arquivo['Arquivo']['tmp_name'] = $this->data['Arquivo']['arquivo']['tmp_name'];
            $arquivo['Arquivo']['turma_id'] = $session_turma['Turma']['id'];
            $arquivo['Arquivo']['usuario_id'] = $usuario['Usuario']['id'];
            $arquivo['Arquivo']['criado'] = date('Y-m-d H:i:s');

            if ($this->Arquivo->save($arquivo['Arquivo'])) {
                $this->redirect("/{$this->params['prefix']}/Arquivos/visualizar/{$this->Arquivo->id}");
            }
        }
    }

    function comissao_adicionar() {
        $this->comercial_adicionar();
    }

    function comissao_visualizar($id = null) {
        $this->comercial_visualizar($id);
    }

    function comissao_baixar($id = null) {
        $this->comercial_baixar($id);
    }

    function formando_baixar($id = null) {
        $this->comercial_baixar($id);
    }

    // Método para verificar a qual turma o usuário pertence, caso seja comissão, e colocar a turma na session
    private function configurarTurmaComissao() {
        if (!$this->Session->check('turma')) {
            $usuario = $this->Session->read('Usuario');

            $this->Turma->bindModel(array('hasOne' => array('TurmasUsuario')), false);
            $turma = $this->Turma->find('first', array('conditions' => array('TurmasUsuario.usuario_id' => $usuario['Usuario']['id'])));

            $this->Session->write('turma', $turma);
        }
    }

    function comissao_index() {
        $this->configurarTurmaComissao();
        $this->comercial_index();
    }

    function comercial_visualizar($id = null) {
        $arquivo = $this->Arquivo->find(array('Arquivo.id' => $id));


        if (!$arquivo) {
            $this->Session->setFlash('Arquivo não existente');
        }

        $this->set('arquivo', $arquivo);
    }

    function comercial_baixar($id = null) {
        $this->super_baixar($id);
    }

    function super_index() {
        $this->set('arquivos', $this->paginate('Arquivo'));
    }

    function super_adicionar() {
        if (!empty($this->data) &&
                is_uploaded_file($this->data['Arquivo']['arquivo']['tmp_name'])) {
            $arquivo = $this->Arquivo->create();

            $arquivo['Arquivo']['nome'] = $this->data['Arquivo']['arquivo']['name'];
            $arquivo['Arquivo']['tipo'] = $this->data['Arquivo']['arquivo']['type'];
            $arquivo['Arquivo']['tamanho'] = $this->data['Arquivo']['arquivo']['size'];
            $arquivo['Arquivo']['tmp_name'] = $this->data['Arquivo']['arquivo']['tmp_name'];
            $arquivo['Arquivo']['turma_id'] = $this->Session->read('turma'); // TODO: Hard Coded is not allowed
            $arquivo['Arquivo']['formando_id'] = 1; // TODO: Hard Coded is not allowed
            if ($this->Arquivo->save($arquivo['Arquivo'])) {
                $this->redirect(array('action' => 'visualizar', 'id' => $this->Arquivo->id));
            }
        }
    }

    function super_visualizar($id = null) {
        $arquivo = $this->Arquivo->find(array('Arquivo.id' => $id));

        if (!$arquivo) {
            $this->Session->setFlash('Arquivo não existente');
        }

        $this->set('arquivo', $arquivo);
    }

    function super_baixar($id = null) {

        // Se o debugger do cake for diferente de 0 pode haver problemas
        // para alguns sistemas reconhecerem o arquivo baixado, pois pode
        // ter sido escrito texto no começo destes arquivos
        Configure::write('debug', '0');
        // Ainda assim há configurações no php.ini que podem executar
        // tarefas semelhantes, por isso tome cuidado

        $this->layout = '';

        if (!empty($id)) {
            $this->Arquivo->id = $id;
            $arquivo = $this->Arquivo->read();

            if (!empty($arquivo)) {
                header("Pragma: public"); // required 
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Cache-Control: private", false); // required for certain browsers 
                header("Content-Type: " . $arquivo['Arquivo']['tipo']);
                header("Content-Disposition: attachment; filename=\"" . $arquivo['Arquivo']['nome'] . "\";");
                header("Content-Transfer-Encoding: binary");
                header("Content-Length: " . $arquivo['Arquivo']['tamanho']);
                ob_clean();
                flush();
                readfile($this->Arquivo->getArquivo());
            } else {
                // arquivo não existe no banco
                // este erro ocorrerá se alguém modificar os arquivos do diretório sem utilizar o sistema
            }
        } else {
            $this->redirect("/{$this->params['prefix']}/Arquivos/");
        }
    }

}

?>