<?php

class SiteTurmaComponent extends Object {

    var $components = array('Session');
    var $uses = array('Turma');
    var $turma = null;
    var $CpanelApi;

    function __construct() {
        if ($this->uses !== false)
            foreach ($this->uses as $modelClass)
                $this->$modelClass = ClassRegistry::init($modelClass);
    }

    function eSiteDeTurma() {
        $hostAtual = str_replace('www.','',$_SERVER['SERVER_NAME']);
        $hostSite = Configure::read('host_site');
        return getenv('TURMA_ID') !== FALSE && $hostAtual != $hostSite;
    }

    function obterTurma() {
        $this->obterTurmaDoSite();
        if(!empty($this->turma))
            $this->conferirTurmaLogada();
        return $this->turma;
    }
    
    private function obterTurmaDaSessao() {
        $this->turma = $this->Session->read('turma');
        $id = getenv('TURMA_ID');
        if(!empty($this->turma) && $id)
            if($this->turma['Turma']['id'] != $id)
                $this->turma = null;
    }

    private function obterTurmaDoSite() {
        $id = getenv('TURMA_ID');
        if($id) {
            $this->Turma->contain(array(
                'CursoTurma.Curso.Faculdade.Universidade',
                'Usuario' => array('conditions' => array(
                    'grupo' => array('comercial', 'planejamento', 'atendimento')))
            ));
            $this->turma = $this->Turma->read(null, $id);
        } else
            $this->turma = null;
    }
    
    private function conferirTurmaLogada() {
        $this->Session->write('turma', $this->turma);
    }
    
    function criarSite($turmaId,$subdominio) {
        
        $hostLocal = Configure::read('host_site');
        $dominio = "$subdominio.$hostLocal";
        $retorno = array('erro' => true,'site' => $dominio);
        if(strpos($hostLocal,'.com.br') === false) {
            $f = fopen(APP.'../jobs/hosts','r');
            $host = fread($f,filesize(APP.'../jobs/hosts'));
            fclose ($f);
            $host = str_replace('{subdominio}',$subdominio,$host);
            $host = str_replace('{turma_id}',$turmaId,$host);
            $f = fopen('/etc/apache2/extra/httpd-vhosts.conf','a+');
            if(fwrite($f,"\n{$host}")) {
                fclose ($f);
                $dominio = "$hostLocal.$subdominio";
                $f = fopen('/etc/hosts','a+');
                $ip = "\n127.0.0.1       $dominio";
                if(fwrite($f,$ip))
                    $retorno['erro'] = false;
                else
                    $retorno['mensagem'] = "Erro ao setar IP";
            } else {
                $retorno['mensagem'] = "Erro ao criar Virtual Host";
            }
        } else {
            $dir = "/usr/local/apache/conf/userdata/std/2/thweb/$dominio";
            if(!file_exists($dir)) {
                if(!mkdir($dir))
                    $dir = false;
            }
            if($dir) {
                $f = fopen("$dir/turma_id.conf",'w+');
                $env = "SetEnv TURMA_ID $turmaId";
                if(fwrite($f,$env)) {
                    $this->CpanelApi->set_port(2083);
                    $this->CpanelApi->host = 'th1045087.underdc.net';
                    $this->CpanelApi->username = 'thweb';
                    $this->CpanelApi->password = 'a13e131294';
                    $this->CpanelApi->rawXML = TRUE;
                    $result = $this->CpanelApi->api2_query(
                        'thweb',
                        'SubDomain',
                        'addsubdomain',
                        array(
                            'dir' => '/sistema/app/webroot/',
                            'domain' => $subdominio,
                            'rootdomain' => $hostLocal
                        )
                    );
                    if($result) {
                        $retorno['erro'] = false;
                        $records = $this->CpanelApi->api2_query(
                            'thweb',
                            'ZoneEdit',
                            'fetchzone',
                            array(
                                'domain' => 'asformaturas.com.br'
                            )
                        );
                        foreach($records['Cpanelresult']['Data']['Record'] as $record) {
                            if(in_array($record['type'],array('A','CNAME','TXT')) &&
                                    strpos($record['name'],$subdominio) !== false) {
                                foreach($record as $c => $v)
                                    if($v == "10.48.0.146")
                                        $record[$c] = "177.70.19.33";
                                $record['domain'] = 'asformaturas.com.br';
                                $edit = $this->CpanelApi->api2_query(
                                    'thweb',
                                    'ZoneEdit',
                                    'edit_zone_record',
                                    $record
                                );
                            }
                        }
                    }
                    else
                        $retorno['mensagem'] = "Erro ao criar Site $subdominio $hostLocal";
                } else {
                    $retorno['mensagem'] = 'Erro ao Criar ID';
                }
            } else {
                $retorno['mensagem'] = 'Erro ao Criar Diretorio';
            }
        }
        return $retorno;
    }

}