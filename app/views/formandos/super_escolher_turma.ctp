<span id="conteudo-titulo" class="box-com-titulo-header">Turmas
	Disponíveis</span>
<div id="conteudo-container">
	<div class="tabela-adicionar-item">
		<?php echo $form->create(false, array('url' => "/{$this->params['prefix']}/turmas", 'class' => 'procurar-form-inline')); ?>
		<span>ID: <?php echo $form->input('filtro-id', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
		</span>
		<?php echo $form->input('filtro-status', array('options' => $statusExistentesParaTurmas, 'type' => 'select', 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
		<?php echo $form->end(array('label' => ' ', 'div' => false, 'class' => 'submit-busca')); ?>
		<div style="clear: both;"></div>
	</div>

	<div class="container-tabela" width="auto">

		<table>
			<?php $isOdd = false; ?>
			<?php foreach ($turmas as $turma): ?>
			<?php if($isOdd):?>

			<tr class="odd">
				<?php else:?>
			
			
			<tr>
				<?php endif;?>
				<td colspan="1" width="3%"><?php echo $turma['Turma']['id']; ?>
				</td>
				<td colspan="1" width="30%"><?php echo $turma['Turma']['nome']; ?>
				</td>
				<td colspan="1" width="3%"><?php echo $turma['Turma']['ano_formatura']; ?>
				</td>
				<td colspan="1" width="8%"><?php echo $turma['Turma']['status']; ?>
				</td>
				<td colspan="1" width="10%"><?php echo $html->link('Selecionar', array($this->params['prefix'] => true,
						'controller' => 'formandos','action' =>'confirmar_troca',
									 $formando['Usuario']['id'], $turma['Turma']['id']), array('class' => 'submit button')); ?>
				</td>
			</tr>
			<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>

		</table>
	</div>
</div>
