<?php

use Aws\S3\S3Client;

/**
 * Classe responsável por faze comunicação com o serviço S3 da Amazon
 */
class AwsS3Component extends Object {

	protected $bucket;
	protected $key;
	protected $secret;

	protected $s3Client;
	protected $resultado;

	/**
	 * O parametros são opcionais
	 *
	 * @param $key Informação de acesso fornecida pela Amazon
	 * @param $secret Informação de acesso fornecida pela Amazon
	 */
	public function __construct($key = null ,$secret = null)
	{
		if($key && $secret)
			$this->setCredenciais($key,$secret);

		$this->resultado = null;
	}

	/**
	 * Ajusta informações para acessar o serviço S3
	 *
	 * Para todas operações será necessário o uso de credenciais
	 *
	 * @param $key Informação de acesso fornecida pela Amazon
	 * @param $secret Informação de acesso fornecida pela Amazon
	 * @return AwsS3Component
	 */
	public function setCredenciais($key,$secret)
	{
		$this->key = $key;
		$this->secret = $secret;
		return $this;
	}


	/**
	 * Conecta se ao serviço S3 no Bucket fornecido
	 *
	 * Para todas operações será necessário o uso de credenciais
	 *
	 * @param $bucket Nome do bucket a qual deve conectar-se
	 * @return AwsS3Component
	 */
	public function conectarBucket($bucket)
	{
		$this->bucket = $bucket;

		try {
			// Instancia o serviço usando o perfil credencial
			$this->s3Client = S3Client::factory(array(
				'key'    => $this->key,
				'secret' => $this->secret
				)
			);
		} catch (\Aws\S3\Exception\S3Exception $e) {
			 echo $e->getMessage();
		}
		return $this;
	}

	/**
	 * Conecta se ao serviço S3 no Bucket fornecido
	 *
	 * @param $arquivoOrigem Caminho do arquivo
	 * @param $diretorioDestino Diretório em que arquivo deve ser salvo no Bucket (sem / no final) 
	 *
	 * @return String URL do arquivo enviado
	 */
	public function enviarArquivo($arquivoOrigem, $diretorioDestino = '' ,$metaData = array())
	{
		try
		{

			$file = new SplFileInfo($arquivoOrigem);

			if ($diretorioDestino)
				$diretorioDestino = $diretorioDestino.'/'.$file->getFilename();
			else
				$diretorioDestino = $file->getFilename();

			$this->resultado = $this->s3Client->putObject(array(
	    		'Bucket'     => $this->bucket,
	    		'Key'        => $diretorioDestino,
	    		'SourceFile' => $arquivoOrigem,
	    		'Metadata'   => $metaData,
	    		'ACL'    => 'public-read'
	    		)
			);
		} catch (Exception $e)
		{
			echo $e->getMessage();
		}

		// Retorna a URL do arquivo no Bucket
		return $this->resultado['ObjectURL'];
	}

	/**
	 * Retorna o ultimo resultado obtido
	 *
	 * @return AwsS3Component
	 */
	public function getResultado()
	{
		return $this->resultado;
	}

}