<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/fileupload.css">
<style type="text/css">
#tab-content-dados { overflow:visible; }
.well { padding:6px; background:white }
.well .button, .well button { margin:0 }
.nav-list li a { position:relative; cursor:pointer; padding:3px 10px; font-size:12px }
.nav-list li.active:first-child { margin-bottom: 5px }
.preview-foto { width:80px; position:relative; margin-left:10px; display:inline-block;
    border:6px solid #f7f7f7; height:80px; border-radius:4px;
    box-shadow: 0 0 5px rgba(0,0,0,0.2); background:white;
    box-sizing:border-box; -moz-box-sizing:border-box;
    -webkit-box-sizing:border-box; cursor:pointer; }
.preview-foto.home { width:120px; height:120px; margin-left:15px }
.preview-foto img { width: 100%; height:auto; display:block; float:left }
.preview-foto i.fechar { position:absolute; top:-17px; left:-15px; cursor:pointer;
    width:20px; height:20px; line-height: 20px; font-size:20px;
    color:white; background:#333333; border-radius:10px; 
    box-shadow: 0 2px 8px rgba(0, 0, 0, 0.5) }
.preview-foto i.fechar:hover { opacity:.9 }
#resp-site { float: right }
.turma-hotsite input[type=file] { width: 0;position: relative}
</style>
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/bootstrap/fileupload.js"></script>
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    
    <?php if(!empty($turma['Turma']['logo_site'])) { ?>
    $('.fileupload.logo')
        .removeClass('fileupload-new')
        .addClass('fileupload-exists')
        .prepend('<input type="hidden" value="" name="">');
    $('<img>',{
        src:'<?="{$turma['Turma']['logo_site']}"?>'
    }).appendTo($(".fileupload-preview.logo"));
    <?php } ?>
    
    var url = '/<?=$this->params['prefix']?>/turmas/editar_site';
    var context = ko.contextFor($("#content-body")[0]);
    
    $('.fileupload').bind('loaded',function(e) {
        tipo = $(this).data('tipo');
        var src = e.imagem;
        if(src) {
            if(tipo == 'logo')
                $("button.enviar.logo").fadeIn(500);
            else
                $("button.enviar.logo").fadeOut(500);
        }
        return;
    });
    
    $("button.enviar").click(function(e) {
        if($(this).hasClass('logo')) {
            src = $(".fileupload-preview.logo img").attr('src').replace(/^data:image\/(gif|png|jpe?g);base64,/, "");
            if(src) {
                var dados = {
                    data : {
                        src : src,
                        tipo : 'logo'
                    }
                };
                context.$data.showLoading(function() {
                    $.ajax({
                        url : url,
                        data : dados,
                        type : "POST",
                        dataType : "json",
                        complete : function() {
                            context.$data.reload();
                        }
                    });
                });
            } else {
                bootbox.alert('Erro ao carregar foto');
            }
        } else if($(this).hasClass('facebook')) {
            var facebook = $("#facebook-url").val();
            if(facebook != "") {
                var dados = {
                    data : {
                        facebook : facebook,
                        tipo : 'facebook'
                    }
                };
                context.$data.showLoading(function() {
                    $.ajax({
                        url : url,
                        data : dados,
                        type : "POST",
                        dataType : "json",
                        complete : function() {
                            context.$data.reload();
                        }
                    });
                });
            } else {
                bootbox.alert('Digite a url');
            }
        }
    });
    
    $("#paginas").on('click','.carregar-pagina',function() {
        pagina = $(this);
        var titulo = $("#pagina-fotos > .nav > li.active > a");
        $("#opcoes-paginas").fadeOut(500,function() {
            $("#lista-fotos").fadeOut(500,function() {
                $("#lista-fotos").html('');
                titulo.text('Carregando');
                $.getJSON('/<?=$this->params['prefix']?>/turmas/carregar_pagina/'+pagina.data('id'), function(response) {
                    $.each(response.fotos,function(i,foto) {
                        $("<div>",{
                            class: 'preview-foto',
                            'data-id' : foto.TurmaPaginaFoto.id
                        }).append(
                            $("<i>",{
                                class:'icon-cancel-4 fechar'
                            })
                        ).append(
                            $("<img>",{
                                src:foto.TurmaPaginaFoto.arquivo
                            })
                        ).appendTo($("#lista-fotos"));
                    });
                }).always(function() {
                    titulo.text("Fotos - " + pagina.text());
                    $(".carregar-pagina.selecionado").removeClass('selecionado');
                    pagina.addClass('selecionado');
                    $("#opcoes-paginas").fadeIn(500,function() {
                        $("#lista-fotos").fadeIn(500,function() {
                            if($("#lista-fotos").find('.preview-foto').length > 1) {
                                $("#lista-fotos").sortable({
                                    connectWith: "#lista-fotos",
                                    update: function(ev,el) {
                                        var dados = {
                                            tipo : 'ordem_fotos',
                                            ids : []
                                        };
                                        $("#lista-fotos").find('.preview-foto').each(function() {
                                            dados.ids.push({
                                                id : $(this).data('id'),
                                                ordem : $(this).index()
                                            });
                                        });
                                        $.ajax({
                                            url : url,
                                            data : { data : dados },
                                            type : "POST",
                                            dataType : "json"
                                        });
                                    }
                                });
                            }
                        });
                    });
                });
            });   
        });
    });
    
    $('#nova-pagina').click(function() {
        bootbox.dialog('Carregando',[{
            label: 'Fechar'
        }],{
            remote: '<?="/{$this->params['prefix']}/turmas/adicionar_pagina/"?>'
        });
    });
    
    $('#editar-pagina').click(function() {
        paginaId = $(".carregar-pagina.selecionado").data('id');
        if(paginaId != undefined) {
            bootbox.dialog('Carregando',[{
                label: 'Fechar'
            }],{
                remote: '<?="/{$this->params['prefix']}/turmas/adicionar_pagina/"?>'+paginaId
            });
        }
    });
    
    $('#adicionar-foto').click(function() {
        paginaId = $(".carregar-pagina.selecionado").data('id');
        if(paginaId != undefined) {
            bootbox.dialog('Carregando',[{
                label: 'Fechar'
            }],{
                remote: '<?="/{$this->params['prefix']}/turmas/adicionar_foto/"?>'+paginaId
            });
        }
    });
    
    $('#remover-pagina').click(function() {
        paginaId = $(".carregar-pagina.selecionado").data('id');
        if(paginaId != undefined) {
            bootbox.confirm('Deseja remover a página?',function(response) {
                if(response) {
                    var url = '<?="/{$this->params['prefix']}/turmas/remover_pagina/"?>';
                    var dados = {
                        data : { id : paginaId }
                    };
                    $.ajax({
                        url : url,
                        data : dados,
                        type : "POST",
                        dataType : "json",
                        success : function(response) {
                            if(response.erro == 1) {
                                bootbox.alert(response.mensagem,function() {
                                    bootbox.hideAll();
                                });
                            } else {
                                $(".carregar-pagina.selecionado").fadeOut(500);
                                $("#opcoes-paginas").fadeOut(500,function() {
                                    $("#lista-fotos").fadeOut(500,function() {
                                        $("#lista-fotos").html('');
                                        var titulo = $("#pagina-fotos > .nav > li.active > a");
                                        titulo.text("Fotos");
                                    });
                                });
                            }
                        },
                        error: function() {
                            bootbox.alert('Erro ao remover página',function() {
                                bootbox.hideAll();
                            });
                        }
                    });
                }
            });
        }
    });
    
    $('#adicionar-foto-cabecalho').click(function() {
        bootbox.dialog('Carregando',[{
            label: 'Fechar'
        }],{
            remote: '<?="/{$this->params['prefix']}/turmas/adicionar_foto_home/"?>'
        });
    });
    
    $('#adicionar-foto-home').click(function() {
        bootbox.dialog('Carregando',[{
            label: 'Fechar'
        }],{
            remote: '<?="/{$this->params['prefix']}/turmas/adicionar_foto_home/home"?>'
        });
    });
    
    $("#geral").on('keyup','#endereco-site',function() {
        if($(this).val().trim() != "")
            $("#criar-site").fadeIn(500);
        else
            $("#criar-site").fadeOut(500);
    });
    
    $("#geral").on('keyup','#facebook-url',function() {
        if($(this).val().trim() != "")
            $("button.enviar.facebook").fadeIn(500);
        else
            $("button.enviar.facebook").fadeIn(500);
    });
    
    $("#verificar-site").click(function() {
        $("#resp-site").text('Aguarde');
        site = $("#endereco-site").val();
        if(site == "") {
            $("#resp-site").addClass('fg-color-red').text('Digite o site');
            $("#endereco-site").focus();
        } else {
            var url = '/turmas/verificar_disponibilidade_site/'+site;
            $.ajax({
                url: url,
                dataType: 'json',
                success: function(response) {
                    if(response.turmas > 0)
                        $("#resp-site").addClass('fg-color-red')
                            .text('Não Disponível');
                    else
                        $("#resp-site").addClass('fg-color-green')
                            .text('Disponível');
                },
                error: function() {
                    $("#resp-site").addClass('fg-color-red').text('Erro ao validar site');
                }
            })
        }
    });
    
    $("#criar-site").click(function() {
        site = $("#endereco-site").val().trim().toLowerCase();
        if(site == "") {
            $("#resp-site").addClass('fg-color-red').text('Digite o site');
            $("#endereco-site").focus();
        } else if(/[^a-zA-Z0-9.]/.test(site)) {
            $("#resp-site").addClass('fg-color-red').text('Site inválido');
            $("#endereco-site").focus();
        } else {
            var mensagem = "O endereço do site será permanente<br />" +
                "Confirma este endereço http://asformaturas.com.br/turma/" + site + " ?";
            bootbox.dialog(mensagem,[{
                label: 'Criar',
                class: 'bg-color-red',
                callback: function() {
                    var context = ko.contextFor($(".metro-button.reload")[0]);
                    context.$data.showLoading(function() {
                        $.ajax({
                            url : url,
                            data : {
                                data : {
                                    site : site,
                                    tipo : 'criar'
                                }
                            },
                            type : "POST",
                            dataType : "json",
                            complete : function() {
                                context.$data.reload();
                            }
                        });
                    });
                }
            },{
                label: 'Cancelar',
                class: 'bg-color-blue'
            }]);
        }
    });
    
    $(".editar-acesso").click(function(e) {
        e.preventDefault();
        var acesso = $(this).data('acesso');
        bootbox.dialog('Confirma a alteração no acesso ao site?',[{
            label: 'Confirmar',
            class: 'bg-color-red',
            callback: function() {
                var context = ko.contextFor($(".metro-button.reload")[0]);
                context.$data.showLoading(function() {
                    $.ajax({
                        url : url,
                        data : {
                            data : {
                                acesso : acesso,
                                tipo : 'acesso'
                            }
                        },
                        type : "POST",
                        dataType : "json",
                        complete : function() {
                            context.$data.reload();
                        }
                    });
                });
            }
        },{
            label: 'Cancelar',
            class: 'bg-color-blue'
        }]);
    });
    
    $("#paginas,#fotos-home,#fotos-cabecalho").on('click','.preview-foto > i',function() {
        var foto = $(this).parent()[0];
        bootbox.confirm('Deseja remover a foto?',function(response) {
            if(response) {
                if($(foto).hasClass('home'))
                    var url = '<?="/{$this->params['prefix']}/turmas/remover_foto_home/"?>';
                else
                    var url = '<?="/{$this->params['prefix']}/turmas/remover_foto/"?>';
                var dados = {
                    data : { id : $(foto).data('id') }
                };
                $.ajax({
                    url : url,
                    data : dados,
                    type : "POST",
                    dataType : "json",
                    success : function(response) {
                        if(response.erro == 1) {
                            bootbox.alert(response.mensagem,function() {
                                bootbox.hideAll();
                            });
                        } else {
                            $(foto).fadeOut(500);
                        }
                    },
                    error: function() {
                        bootbox.alert('Erro ao remover foto',function() {
                            bootbox.hideAll();
                        });
                    }
                });
            }
        });
    });

    $(".addFileButton").click(function(){
        $(".addFileHandler").click();
    });
});
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Editar Site da Turma
        </h2>
    </div>
</div>
<?php $session->flash(); ?>
<div class="alert alert-info">
<?php if($turma['Turma']['site_liberado'] == 1) { ?>
    <h4>O acesso ao site está liberado</h4>
    <br />
    O endereço de acesso ao site é 
    <a class="fg-color-white" href="http://<?="{$host}/turma/{$turma['Turma']['site']}"?>" target="_blank">
        http://<?="{$host}/turma/{$turma['Turma']['site']}"?>
    </a>
    <br />
    Clique no botão abaixo para bloquear o acesso
    <br />
    <br />
    <a class="bg-color-red button editar-acesso" data-acesso="0">
        Bloquear Acesso
    </a>
<?php } elseif(!empty($turma['Turma']['site'])) { ?>
    <h4>O acesso ao site ainda não está liberado</h4>
    Para liberar o acesso voce deve completar os dados
    <br />
    <br />
    <a class="fg-color-white" href="http://<?php echo $host; ?>/turma/<?php echo $turma['Turma']['site'] ?>"
        target="_blank">
        Clique aqui para ver como está ficando o site
    </a>
    <?php if(count($paginas) > 0) : ?>
    <br />
    <br />
    <a class="bg-color-red button editar-acesso" data-acesso="1">
        Clique aqui para liberar o acesso
    </a>
    <?php endif; ?>
<?php } else { ?>
    Os únicos caracteres permitidos são números, pontos e letras, sem cedilha ou acento
<?php } ?>
</div>
<div class="row-fluid">
    <ul class="nav nav-tabs" id='tab-dados'>
        <li class="active">
            <a href="#geral" data-toggle="tab">Geral</a>
        </li>
        <?php if(!empty($turma['Turma']['logo_site'])) { ?>
        <li>
            <a href="#fotos-cabecalho" data-toggle="tab">Fotos Cabeçalho</a>
        </li>
        <li>
            <a href="#fotos-home" data-toggle="tab">Fotos Home</a>
        </li>
        <?php } ?>
        <?php if(count($fotos) > 0) { ?>
        <li>
            <a href="#paginas" data-toggle="tab">Páginas</a>
        </li>
        <?php } ?>
    </ul>
    <div class="tab-content turma-hotsite" id="tab-content-dados">
        <div class="tab-pane active fade in" id="geral">
            <?php if(empty($turma['Turma']['site'])) : ?>
            <div class="row-fluid">
                <div class='span6'>
                    <label>Site<span id="resp-site"></span></label>
                    <?=$form->input('site',
                        array(
                            'label' => false,
                            'div' => 'input-control text',
                            'id' => 'endereco-site',
                            'error' => false)); ?>
                </div>
                <div class='span6'>
                    <label>&nbsp;</label>
                    <a class="button default" id="verificar-site">Verificar Disponibilidade</a>
                </div>
            </div>
            <button type="button" class="bg-color-blueDark hide" id='criar-site'>
                Criar Site
            </button>
            <?php else : ?>
            <?=$form->hidden('Turma.logo_site');?>
            <div class="row-fluid">
                <div class="fileupload fileupload-new span4 logo" data-provides="fileupload" data-tipo="logo">
                    <div>
                        <span class="btn-file">
                            <a class="addFileButton button mini input-block-level default fileupload-new">
                                <i class="icon-picture"></i>
                                Selecione o Logo
                            </a>
                            <a class="addFileButton button mini bg-color-orange input-block-level fileupload-exists">
                                <i class="icon-retweet"></i>
                                Alterar
                            </a>
                            <input class="addFileHandler" type="file" />
                        </span>
                    </div>
                    <div class="fileupload-new thumbnail" style="width:100%; height: 150px; line-height: 20px;">
                        <img src="<?="{$this->webroot}metro/img/no-image.gif"?>" />
                    </div>
                    <div class="fileupload-preview fileupload-exists thumbnail logo"
                        style="width:100%; height: 150px; line-height: 20px;">
                    </div>
                </div>
                <div class="span5 offset1">
                    <label>Facebook da Turma</label>
                    <input type="text" id="facebook-url" value="<?=$turma['Turma']['facebook']?>" class="input-block-level" />
                    <button type="button" class="bg-color-red enviar facebook hide">
                        Enviar
                    </button>
                </div>
            </div>
            <br />
            <button type="button" class="bg-color-red enviar logo hide">
                Enviar Novo Logo
            </button>
            <?php endif; ?>
        </div>
        <div class="tab-pane fade" id="fotos-cabecalho">
            <?php if(empty($fotos['cabecalho'])): ?>
                <div class="alert alert-error">
                    Voce deve adicionar pelo menos uma foto de cabecalho para prosseguir
                </div>
            <?php endif; ?>
            <button type="button" class="bg-color-blue" id="adicionar-foto-cabecalho">
                Adicionar Foto
            </button>
            <br />
            <br />
            <div>
                <?php if(isset($fotos['cabecalho'])) : foreach($fotos['cabecalho'] as $id => $arquivo) : ?>
                <div class="preview-foto home" data-id="<?=$id?>">
                    <i class="icon-cancel-4 fechar"></i>
                    <img src="<?="{$arquivo}"?>">
                </div>
                <?php endforeach; endif; ?>
            </div>
        </div>
        <div class="tab-pane fade" id="fotos-home">
            <button type="button" class="bg-color-blue" id="adicionar-foto-home">
                Adicionar Foto
            </button>
            <br />
            <br />
            <div>
                <?php if(isset($fotos['home'])) : foreach($fotos['home'] as $id => $arquivo) : ?>
                <div class="preview-foto home" data-id="<?=$id?>">
                    <i class="icon-cancel-4 fechar"></i>
                    <img src="<?="{$arquivo}"?>">
                </div>
                <?php endforeach; endif; ?>
            </div>
        </div>
        <div class="tab-pane fade" id="paginas">
            <div class="row-fluid">
                <div class="span4">
                    <div class='well'>
                        <ul class="nav nav-list">
                            <li class='active'>
                                <a>P&aacute;ginas</a>
                            </li>
                            <?php foreach($paginas as $pagina) : ?>
                            <li>
                                <a class="carregar-pagina" data-id="<?=$pagina['TurmaPagina']['id']?>">
                                    <?="{$pagina['TurmaPagina']['menu']}/{$pagina['TurmaPagina']['titulo']}"?>
                                </a>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                        <br />
                        <a class="input-block-level button mini default" id="nova-pagina">
                            Adicionar Pagina
                        </a>
                    </div>
                </div>
                <div class="span8">
                    <div class="well" id="pagina-fotos">
                        <ul class="nav nav-list">
                            <li class='active'>
                                <a>Fotos</a>
                            </li>
                        </ul>
                        <div class="row-fluid hide" id="opcoes-paginas">
                            <button type="button" class="mini default" id="editar-pagina">
                                Editar Pagina
                            </button>
                            <button type="button" class="mini bg-color-red" id="remover-pagina">
                                Remover Pagina
                            </button>
                            <button type="button" class="mini bg-color-blue" id="adicionar-foto">
                                Adicionar Foto
                            </button>
                        </div>
                        <br />
                        <div class="row-fluid" id="lista-fotos">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <br />
</div>