<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/tooltip.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery.meio.mask.js"></script>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.selectpicker').selectpicker({width:'100%'});
        $.Input();
        $('input[alt]').setMask();
        $("#form-filtro").submit(function(e){
            e.preventDefault();
            var context = ko.contextFor($(".metro-button.reload")[0]);
            context.$data.showLoading(function() {
                $("#form-filtro").find('[value="false"]').attr('disabled','disabled');
                var dados = $("#form-filtro").serialize();
                var url = $("#form-filtro").attr('action');
                $.ajax({
                    url : url,
                    data : dados,
                    type : "POST",
                    dataType : "json",
                    complete : function() {
                        context.$data.reload();
                    }
                });
            });
        });
        $('.alterar').click(function() {
            bootbox.dialog('Carregando',[{
                label: 'Fechar'
            }],{
                remote: '<?="/{$this->params['prefix']}/usuarios/alterar/"?>'+
                        $(this).attr('dir')
            });
        });
        $('.senha').click(function() {
            bootbox.dialog('Carregando',[{
                label: 'Fechar'
            }],{
                remote: '<?="/{$this->params['prefix']}/usuarios/alterar_senha_usuario/"?>'+
                        $(this).attr('dir')
            });
        });
        $('.inserir').click(function() {
            bootbox.dialog('Carregando',[{
                label: 'Fechar'
            }],{
                remote: '<?="/{$this->params['prefix']}/usuarios/inserir/"?>'
            });
        });
        $('.financeiro').click(function() {
            bootbox.dialog('Carregando',[{
                label: 'Fechar'
            }],{
                remote: '<?="/{$this->params['prefix']}/area_financeira/dados/"?>'+
                        $(this).attr('dir')
            });
        });
        $('.trocar-turma').click(function() {
            bootbox.dialog('Carregando',[{
                label: 'Fechar'
            }],{
                remote: '<?="/{$this->params['prefix']}/usuarios/trocar_turma/"?>'+
                        $(this).attr('dir')
            });
        });
    });
</script>
<?php $session->flash();?>
<div class="row-fluid">
    <div class="span12">
        <div class="span6">
            <h2>
                <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
                Usuários
            </h2>
        </div>
    </div>
</div>
<?php 
$paginator->options(array(
    'url' => array($this->params['prefix'] => true)));
$sortOptions = array('data-bind' => 'click: loadThis');
$buttonAfter = '<button class="helper" onclick="return false" ' .
    'tabindex="-1" type="button"></button>';
?>
<div class="row-fluid">
    <?=$form->create('Usuario',array(
        'url' => "/{$this->params['prefix']}/usuarios/listar/",
        'id' => 'form-filtro')) ?>
    <div class="row-fluid">
        <div class="span2">
            <?=$form->input('ViewFormandos.codigo_formando',
                array('label' => 'Código Formando', 'div' => 'input-control text',
                    'error' => false,'type' => 'text', 'after' => $buttonAfter)); ?>
        </div>
        <div class="span2">
            <?=$form->input('Usuario.nome',
                array('label' => 'Nome', 'div' => 'input-control text',
                    'error' => false,'type' => 'text', 'after' => $buttonAfter)); ?>
        </div>
        <div class="span2">
            <?=$form->input('Usuario.grupo', 
                array('options' => $grupos, 'type' => 'select', 
                    'class' => 'selectpicker', 'label' => 'Grupo', 'div' => false)); ?>
        </div>
        <div class="span2">
            <?=$form->input('Usuario.nivel', 
                array('options' => $niveis, 'type' => 'select', 
                    'class' => 'selectpicker', 'label' => 'Nivel', 'div' => false)); ?>
        </div>
        <div class="span2">
            <label>&nbsp;</label>
            <button type='submit' class='mini max bg-color-red'>
                Filtrar
                <i class='icon-search-2'></i>
            </button>
        </div>
        <div class="span2">
            <label>&nbsp;</label>
            <button type="button" class="button mini pull-right bg-color-blue inserir">
                Inserir Usuário
                <i class="icon-user-5"></i>
            </button>
        </div>
    </div>
<?php if($usuarios) : ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="5%"><?=$paginator->sort('Código Formando', 'ViewFormandos.codigo_formando',$sortOptions); ?></th>
                <th scope="col" width="25%"><?=$paginator->sort('Nome', 'Usuario.nome',$sortOptions); ?></th>
                <th scope="col" width="15%"><?=$paginator->sort('Email', 'Usuario.email',$sortOptions); ?></th>
                <th scope="col" width="10%"><?=$paginator->sort('Grupo', 'Usuario.grupo',$sortOptions); ?></th>
                <th scope="col" width="10%"><?=$paginator->sort('Nível', 'Usuario.nivel',$sortOptions); ?></th>
                <th scope="col" width="35%"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($usuarios as $usuario) : ?>
            <tr>
                <td><?=($usuario['ViewFormandos']['codigo_formando']) ? $usuario['ViewFormandos']['codigo_formando'] : "-" ?></td>
                <td><?=$usuario['Usuario']['nome']; ?></td>
                <td><?=$usuario['Usuario']['email']; ?></td>
                <td><?=ucfirst($usuario['Usuario']['grupo']); ?></td>
                <td><?=ucfirst($usuario['Usuario']['nivel']); ?></td>
                <td>
                    <button type="button" class="default mini bg-color-red alterar" dir="<?=$usuario['Usuario']['id']?>">
                            Alterar
                    </button>
                    <button type="button" class="default mini bg-color-brown senha" dir="<?=$usuario['Usuario']['id']?>">
                            Alterar Senha
                    </button>
                    <?php if($usuarioLogado['Usuario']['nivel'] == 'administrador'){ ?>
                    <a href="<?="/{$this->params['prefix']}/usuarios/entrar/{$usuario['Usuario']['id']}"?>" class="button mini bg-color-purple entrar">
                        Entrar
                    </a>
                    <?php } ?>
                    <?php if($usuario['Usuario']['grupo'] == 'comissao' ||
                             $usuario['Usuario']['grupo'] == 'formando') : ?>
                    <button type="button" class="default mini bg-color-blue financeiro" dir="<?=$usuario['Usuario']['id']?>">
                            Financeiro
                    </button>
                    <button type="button" class="default mini bg-color-greenDark trocar-turma" dir="<?=$usuario['Usuario']['id']?>">
                        Trocar Turma
                    </button>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4" class="paginacao">
                    <?=$paginator->numbers(array('separator' => ' ','data-bind' => 'click: loadThis')); ?>
                </td>
                <td colspan="5">
                    <?=$paginator->counter(array('format' => 'Total : %count% ' .  'Usuários')); ?>
                </td>
            </tr>
        </tfoot>
    </table>
    <?= $form->end(array('label' => false,
        'div' => false, 'class' => 'hide')); ?>
<?php else : ?>
    <h2 class="fg-color-red">Nenhum Usuário Encontrado</h2>
<?php endif; ?>
</div>