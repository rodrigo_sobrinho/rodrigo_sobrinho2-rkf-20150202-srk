<?php if($out == "html") : ?>
<?php include('salvar_contrato.ctp')?>
<?php endif; ?>
<?php setlocale( LC_ALL, 'pt_BR', 'pt_BR.iso-8859-1', 'pt_BR.utf-8', 'portuguese'); $mes = strftime("%B");?>
<h4 style="text-align: center"><b>CONTRATO DE PRESTAÇÃO DE SERVIÇOS</b></h4>
<br />
<p style="text-align: justify">
Pelo presente instrumento particular, de um lado:
<br />
<br />
Na qualidade de <b>Contratada: SADER & AVILA</b>, pessoa jurídica de direito privado devidamente inscrita no CNPJ/MF sob o 
nº 15.151.497/0001-16, com sede na Avenida José Maria Whitaker, 882, Planalto Paulista, São Paulo/SP, CEP 04057-000 e Coligadas, 
neste ato representada por seu sócio, o Sr. Rachid Sader, brasileiro, empresário, portador da cédula de identidade RG 
n.º 26.572.035 e inscrito no CPF sob o n.º 289.174.098-06 e/ou por seu Procurador.
<br />
<br />
E de outro lado:
<br />
<br />
Na qualidade de <b>Contratantes</b>: Os formandos do(s) curso(s) de <b><?=$turma['Turma']['cursos']?></b>,
da <b><?=$turma['Turma']['nome']?></b>, com previsão de formatura para o <b><?=($turma['Turma']['semestre_formatura'] == 1) ? 'primeiro' : 'segundo';?> 
    semestre de <?=$turma['Turma']['ano_formatura']?></b>, neste ato devidamente representados pela <b>Comissão de Formatura</b>;
<br />
<br />
<b>CONSIDERANDO</b> que:
<br />
<p style="margin-left: 30px; text-align: justify">
• Os formandos do(s) curso(s) de <b><?=$turma['Turma']['cursos']?></b>, da <b><?=$turma['Turma']['nome']?></b> (doravante denominados coletivamente 
como "Formandos" ou individualmente como "Formando"), desejam contratar uma empresa para a cobertura fotográfica de eventos 
relacionados à formatura que inclui sessão solene do Culto Ecumênico, Colação de Grau, Jantar de Gala, Baile de Formatura 
além de fotos na faculdade ou em locais pré-definidos pelo <b>CONTRATANTE</b> conforme especificado nesse contrato;
<br />
<br />
• Para que todas as atividades relacionadas a essa cobertura fotográfica, foram eleitos membros integrantes dos Formandos 
e criada uma Comissão de Formatura (doravante denominada simplesmente “Comissão”) que representará a vontade dos Formandos, 
contratando e executando todas as atividades em nome destes;
<br />
<br />
• A <b>Contratada</b> é uma empresa renomada que está no mercado de eventos, formaturas e cobertura fotográfica, detendo o 
conhecimento técnico e mercadológico, parceria com fornecedores conceituados e qualificados, possuindo uma estrutura adequada
para a realização da cobertura fotográfica;
<br />
</p>
<b>RESOLVEM</b> firmar o presente Contrato de Prestação de Serviços (o “Contrato”), que será regido pelos termos e condições a seguir 
estipulados, em conformidade com os artigos 125 e seguintes do Código Civil Brasileiro:
<br />
<br />
<b style="text-decoration: underline">1. DO OBJETO</b>
<br />
<br />
<b>1.1.</b> A prestação do serviço consiste em foto-filmagem nos eventos de formatura dos <b>CONTRATANTES</b>, eventos 
esses definidos no Anexo A desse contrato. Como as datas dos eventos ainda não estão definidas é obrigação da <b>CONTRATANTE</b>, 
informar as datas por e-mail, sistema ou por escrito com pelo menos 30 dias de antecedência dos mesmos, sendo que se possível 
a <b>CONTRATADA</b> solicita que as datas sejam informadas com maior antecedência possível, tão logo sejam definidas.<br /><br />
<b>1.2.</b> Caso existam outros eventos diferentes dos descritos no Anexo A que a <b>CONTRATANTE</b> deseje cobertura 
fotográfica, está deverá solicitar por e-mail ou sistema de comunicação com pelo menos 30 dias de antecedência. 
A <b>CONTRATADA</b> analisará a solicitação e caso existam despesas com viagens, acomodação, staff ou outros custos não 
previsto em contrato, passará para o <b>CONTRATANTE</b> os valores cabendo a <b>CONTRATADA</b> a confirmação ou não do serviço, 
com pelo menos 10 dias de antecedência.<br /><br />
<b>1.3.</b> Após a realização da festa de formatura, ou o último evento previsto nesse contrato, será direito da 
<b>CONTRATADA</b> a comercialização de álbuns de formatura e produtos fotográficos diretamente aos <b>FORMANDOS</b>, 
sem nenhuma responsabilidade ou obrigação da <b>COMISSÃO DE FORMATURA</b>.<br /><br />
<b>1.4.</b> A <b>CONTRATADA</b> e <b>CONTRATANTE</b> para a fiel execução desse contrato definem que o mesmo é estabelecido em 
caráter de <b>EXCLUSIVIDADE</b>. Assim, a <b>CONTRATANTE</b> atesta que não existe nenhuma empresa contratada previamente 
para a execução desse serviço, bem como não poderá contratar nenhuma outra empresa a partir da assinatura desse contrato 
para a prestação do serviço de cobertura fotográfica e comercialização de produtos fotográficos relacionados à formatura d
os formandos especificados nesse contrato.<br /><br />
<b style="text-decoration: underline">2. DOS DOCUMENTOS INTEGRANTES AO CONTRATO (“ANEXOS”)</b>
<br />
<br />
<b>2.1.</b> São partes integrantes do Contrato os seguintes documentos:
<br />
<br />
<p style="margin-left: 30px; text-align: justify">
<b>a)</b> Anexo ‘A’ – Previsão de Eventos de Formatura para a cobertura de Foto e Filmagem<br/><br/>
<b>b)</b> Anexo ‘B’ – Termo de Finalização da Negociação;<br/><br/>
<b>c)</b> Anexo ‘C’ – Ata de Constituição da Contratante;<br/><br/>
<b>d)</b> Anexo ‘D’ – Contraprestação da Cessão da Exclusividade Fotográfica;<br/><br/>
<b>e)</b> Anexo ‘E’ – Condição Comercial para a Comercialização dos Albuns;<br/><br/>
<b>f)</b> Anexo ‘F’ – Definição de Máquinas Fotográficas Profissionais e Semi-profissionais;<br/><br/>
<b>g)</b> Anexo ‘G’ – Termo de Adesão Individual Assinado entre Formando e CONTRATADA
</p>
<b style="text-decoration: underline">3. DOS SERVIÇOS PRESTADOS</b>
<br />
<br />
<b>3.1.</b> Todos os profissionais destacados para a cobertura dos eventos possuem treinamento e qualificação necessária 
para o mesmo. No Baile de Formatura será destacado pelo menos um fotografo a cada 12 alunos e na colação um fotografo a cada
25 alunos.<br /><br />
<b>3.2.</b> Em todos os eventos extraoficiais, os profissionais estarão trajados de acordo com a situação, no entanto, sempre
destacado como profissional contratado. Caso a Comissão de Formatura tenha alguma exigência em relação a trajes, deverá 
especificar as mesmas no Anexo B – Termo de Finalização de Negociação.<br/><br/>
<b>3.3.</b> Em todos os eventos oficiais e solenes os profissionais estarão em traje social completo.<br /><br />
<b>3.4.</b> As fotografias que comporão os álbuns serão coloridas, no tamanho 30 x 40 cm, com padrão de qualidade compatível 
com o mercado fotográfico, ilustrando todos os eventos extraoficiais solicitados pela Comissão de Formatura, além da Colação 
de Grau e Baile de Gala. <br /><br />
<b>3.5.</b> A Contratada possui tecnologia, equipamento e estrutura necessária para a realização integral de todo o processo 
de impressão de fotografias, confecção dos álbuns e produtos fotográficos. <br /><br />
<b>3.6.</b> Os materiais de vídeo serão editados em uma ESTAÇÃO DIGITAL, onde todas as imagens serão captadas através de 
câmeras digitais. <br /><br />
<b>3.7.</b> O prazo de entrega do material fotográfico dar-se-á no máximo de 180 dias úteis após a realização do Baile de Gala. 
A visita para apresentação do material será em local e horário a critério do formando através de agendamento prévio. A 
forma de pagamento que estará sujeito o formando respeitará todos os critérios ora citados, podendo a empresa criar meios 
complementares e facilitadores para aquisição dos produtos mencionados. Assim, o álbum poderá ser parcelado em até 12 vezes.
<br />
<br />
<b style="text-decoration: underline">4. DOS DETALHES DA EXCLUSIVIDADE</b>
<br />
<br />
<b>4.1.</b> A <b>CONTRATANTE</b> se obriga a realizar os eventos previstos no Anexo A, sendo que a <b>CONTRATADA</b> não 
possui nenhuma responsabilidade na execução dos mesmos, salvo se algum desses eventos estiver incluso na contraprestação da 
cessão da exclusividade fotográfica nesse contrato. .<br /><br />
<p style="margin-left: 30px; text-align: justify">
<b>4.1.1.</b> De comum acordo entre as partes estes eventos poderão ser alterados para mais ou para menos, porém a 
<b>CONTRATADA</b> deverá ser a única empresa a realizar a cobertura cine-fotográfica dos eventos de formatura. A 
inobservância desta estipulação implica em infração contratual com a possível rescisão deste contrato, salvo nos 
casos de caso fortuito ou força maior, ou por justificativa plausível da <b>CONTRATANTE</b>.<br/><br/>
</p>
<b>4.2.</b> Como definido no objeto desse contrato, a prestação do serviço ocorre em caráter de exclusividade, assim a 
<b>CONTRATADA</b>, cuidará para impedir a execução total ou mesmo parcial de iguais serviços por parte de qualquer outra 
pessoa física ou jurídica, em concorrência, ressalvada, apenas máquinas fotográficas amadoras de formandos, parentes e 
amigos e que não atrapalhe o bom andamento da equipe de fotógrafos e cinegrafistas da <b>CONTRATADA</b>.<br /><br />
<b>4.3.</b> Não será permitido à entrada bem como o uso de máquinas profissionais, semiprofissionais ou filmadora por qualquer 
pessoa (formando, convidado ou profissional) que não faça parte do staff da <b>CONTRATADA</b>.  Nos pontos de fotos montados 
pela <b>CONTRATADA</b>, tanto no Jantar de Gala como no Baile de Formatura, fica proibido também o uso de máquinas fotográficas 
amadoras enquanto ele estiver sendo utilizado pela equipe de fotógrafos profissionais da <b>CONTRATADA</b>. O uso dessas máquinas
acaba atrapalhando o trabalho do fotógrafo, do estúdio montado, bem como o fluxo de pessoas no local. <br /><br />
<b>4.4.</b> A presença de quaisquer fotógrafos e cinegrafistas profissionais convocados pela <b>CONTRATANTE</b> ou através dela, 
sem anuência expressa da <b>CONTRATADA</b>, nos locais em que esta desempenhará suas funções, será considerada como 
quebra de exclusividade, podendo ser o contrato repactuado nos limites comprovados da perda sem prejuízo de quaisquer outros 
prejuízos que sofra por conta da quebra de exclusividade.<br /><br />
<b>4.5.</b> A <b>CONTRATANTE</b> obriga-se a obter junto à direção da Faculdade, todas as facilidades para que a <b>CONTRATADA</b>,
por seus funcionários prepostos ou representantes da <b>CONTRATADA</b> sempre representem seu papel devidamente trajados e com 
equipamentos adequados.
<br />
<br />
<b style="text-decoration: underline">5. DA CONTRAPRESTAÇÃO DA CESSÃO DA EXCLUSIVIDADE FOTOGRÁFICA</b>
<br />
<br />
<b>5.1.</b> Em retribuição à cessão de exclusividade que ora se contrata, a <b>CONTRATADA</b> entregará ao <b>CONTRATANTE</b> 
os bens especificados no Anexo D.<br/><br/>
<b>5.2.</b> Todos os itens e valores cedidos são planejados no número final de formandos especificado no Anexo D, definido 
exclusivamente pela <b>CONTRATANTE</b>. No entanto, serão proporcionais ao número final de formandos que efetivamente participarem 
do Baile de Formatura, sendo esse o parâmetro para a contagem de formandos participantes da formatura.
<br />
<br />
<b style="text-decoration: underline">6. PRAZO E MULTAS</b>
<br />
<br />
<b>6.1.</b> O presente contrato tem sua vigência a partir desta data, até a data do efetivo cumprimento de todas as obrigações 
contratuais por ambas as partes.<br /><br />
<b>6.2.</b> Fica estabelecida a multa no valor total da cessão da exclusividade Fotográfica, a ser paga pela parte que 
rescindir ou der causa à rescisão do presente contrato sem justa causa, sendo que se a iniciativa for da <b>CONTRATANTE</b>, 
além da multa deverá devolver todos os valores já recebidos conforme Anexo D, além de cobrir os valores de serviços e despesas 
já realizadas.<br /><br />
<b>6.1.</b> Em caso de ocorrência de falta grave, devidamente comprovada, cometida por uma das partes, poderá a parte 
inocente dar o contrato por rescindido, enviando carta, fax ou correspondência eletrônica à parte que deu causa à rescisão.
<br />
<br />
<b style="text-decoration: underline">7. DAS DISPOSIÇÕES FINAIS</b>
<br />
<br />
<b>7.1.</b> Caso qualquer disposição deste contrato seja declarada inválida, nula ou inexequível permanecerão em vigor todas 
as demais cláusulas do ajuste.<br /><br />
<b>7.2.</b> Nenhum representante comercial, vendedor ou funcionário em nome da <b>CONTRATADA</b> tem poderes para alterar as 
cláusulas e/ou condições do presente Contrato e muito menos rescindi-lo.<br/><br/>
<b>7.3.</b> As partes deste instrumento não se responsabilizam por quaisquer promessas ou acordos, senão os constantes do 
presente contrato.<br /><br />
<b>7.4.</b> Fica eleito o Foro Central da Comarca da capital do Estado de São Paulo, para dirimir eventuais questões oriundas 
da celebração, interpretação e execução do presente Contrato, com renúncia a qualquer outro, por mais privilegiado que seja, 
arcando a parte vencida com todo o ônus de sucumbência, inclusive honorários advocatícios de 20% (vinte por cento) do total da 
condenação.
<br />
<br />
<h5 style="text-align: center; color: black">São Paulo, <?=date('d')?> de <?=ucfirst($mes)?> de <?=date('Y')?></h5><br /><br /><br />
<p style="margin-left: 30px">
<p>
<table border="0" style="width: 500px" align="center">
<tr style="text-align: center">
<td><?=str_pad("",25,"_")?></td>
<td><?=str_pad("",25,"_")?></td>
</tr>
<tr style="text-align: center; border: 0">
<td>AS EVENTOS<br/>(RACHID SADER)</td>
<td>ZAPE<br/>(RACHID SADER NETO)</td>
</tr>
</table>
</p>
<p></p><p></p>
<p>
<table border="0" style="width: 500px" align="center">
<?php foreach($turma['Turma']['comissao'] as $comissao) : ?>
<tr style="text-align: center">
<td><?=str_pad("",25,"_")?><br /><b>Nome: </b><?=key($comissao)?><br /><b>RG: </b><?=$comissao[key($comissao)]?><br /><br /><br /><br /></td>
</tr>
<tr style="text-align: center; border: 0">
<td></td>
</tr>
<?php endforeach; ?>
</table>
</p>
</p>