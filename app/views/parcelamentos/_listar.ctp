<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Parcelamentos
            <?php if ($this->params['prefix'] == 'planejamento') { ?>
                <a class="button mini bg-color-orange pull-right"
                   data-bind="click: function() {
                   page('<?= "/{$this->params['prefix']}/parcelamentos/inserir" ?>') }">
                    Inserir Parcelamento
                    <i class='icon-layers-alt'></i>
                </a>
            <?php } ?>
        </h2>
    </div>
</div>
<?php $session->flash(); ?>
<div class="row-fluid">
    <?php if (sizeof($parcelamentos) == 0) { ?>
        <h2 class="fg-color-red">Nenhum Parcelamento Cadastrado Para Esta Turma</h2>
    <?php } else { ?>
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th colspan="2">Nome</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($parcelamentos as $parcelamento) : ?>
                    <tr>
                        <td style="text-align: center"><?= $parcelamento['Parcelamento']['titulo'] ?></td>
                        <td style="text-align: center">
                            <a class="button mini bg-color-blueDark"
                               data-bind="click: function() {
                               page('<?= "/{$this->params['prefix']}/parcelamentos/exibir/{$parcelamento['Parcelamento']['id']}" ?>') }">
                                Visualizar
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php } ?>
</div>