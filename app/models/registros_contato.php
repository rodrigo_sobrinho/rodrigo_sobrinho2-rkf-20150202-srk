<?php

class RegistrosContato extends AppModel {

    var $name = 'RegistrosContato';
    var $validate = array(
        'data' => array(
            'date' => array('rule' => array('datetime'), 'message' => 'Data inválida.'),
        ),
        'assunto' => array(
            'between' => array(
                'rule' => array('between', 1, 300),
                'message' => 'Nome deve possuir de 1 a 300 caracteres.'
            ),
        )
    );
    //The Associations below have been created with all possible keys, those that are not needed can be removed

    var $belongsTo = array(
        'Turma' => array(
            'className' => 'Turma',
            'foreignKey' => 'turma_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'usuario_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    
    var $hasMany = array(
        'RegistroContatoUsuario' => array(
            'className' => 'RegistroContatoUsuario',
            'foreignKey' => 'registro_contato_id',
            'unique' => false,
        )
    );

}

?>