<?php

class FotosTelaoShell extends Shell {

    var $uses = array(
        'FormandoFotoTelao',
        'Turma',
        'ViewFormandos',
    );
    
    function criar_zip() {
        if(count($this->args) == 1) {
            ini_set('memory_limit', "3056M");
            set_time_limit(0);
            $this->Turma->unbindModelAll();
            $turma = $this->Turma->read(null,$this->args[0]);
            if($turma) {
                if($this->criarDiretorioDaTurma($turma['Turma']['id'])) {
                    $this->criarDiretorioAdulto($turma['Turma']['id']);
                    $this->criarDiretorioCrianca($turma['Turma']['id']);
                    $dirBase = APP ."webroot/upload/turmas";
                    $dirZip = "$dirBase/fotos_telao_{$turma['Turma']['id']}";
                    $fileZip = "$dirZip.zip";
                    $formandos = $this->ViewFormandos->find('list', array(
                        'conditions' => array(
                            'ViewFormandos.turma_id' => $turma['Turma']['id']
                        )
                    ));
                    $fotos = $this->FormandoFotoTelao->find('all', array(
                        'conditions' => array(
                            'FormandoFotoTelao.usuario_id' => $formandos
                        ),
                        'order' => array('FormandoFotoTelao.id' => 'asc')
                    ));
                    $dir = APP ."webroot/";
                    foreach($fotos as $foto) {
                        if($foto['FormandoFotoTelao']['tipo'] == 'adulto'){
                            $nome = $foto['Usuario']['nome_telao'];
                            copy($dir.$foto['FormandoFotoTelao']['arquivo'],"$dirZip/Adulto/$nome.jpeg");
                        }else{
                            $nome = $foto['Usuario']['nome_telao'];
                            copy($dir.$foto['FormandoFotoTelao']['arquivo'],"$dirZip/Crianca/$nome.jpeg");
                        }
                        //copy("/Library/WebServer/Documents/aseventos/homologacao/app/webroot/favicon.png","$dirZip/$nome.jpeg");
                    }
                    $command = "cd $dirBase && zip -r fotos_telao_{$turma['Turma']['id']}.zip fotos_telao_{$turma['Turma']['id']}";
                    exec($command);
                    exec("rm -rf $dirZip");
                }
            }
        }
    }
    
    function criarDiretorioDaTurma($turmaId) {
        $dir = APP ."webroot/upload/turmas/fotos_telao_{$turmaId}";
        if(file_exists($dir))
            exec("rm -rf $dir");
        return mkdir($dir);
    }
    
    function criarDiretorioAdulto($turmaId) {
        $dir = APP ."webroot/upload/turmas/fotos_telao_{$turmaId}";
        $dirAdulto = $dir . "/Adulto";
        if(file_exists($dirAdulto))
            exec("rm -rf $dirAdulto");
        return mkdir($dirAdulto);
    }
    
    function criarDiretorioCrianca($turmaId) {
        $dir = APP ."webroot/upload/turmas/fotos_telao_{$turmaId}";
        $dirCrianca = $dir . "/Crianca";
        if(file_exists($dirCrianca))
            exec("rm -rf $dirCrianca");
        return mkdir($dirCrianca);
    }
}