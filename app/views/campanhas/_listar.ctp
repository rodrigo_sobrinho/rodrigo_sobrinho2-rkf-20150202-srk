<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css?v=0.1">
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Campanhas
        <?php if($this->params['prefix'] == 'planejamento') { ?>
            <a class="button mini bg-color-blueDark pull-right"
                data-bind="click: function() {
                page('<?= "/{$this->params['prefix']}/campanhas/editar"?>') }">
                Inserir Campanha
                <i class='icon-folder-3'></i>
            </a>
        <?php } ?>
        </h2>
    </div>
</div>
<br />
<?php $session->flash(); ?>
<div class="row-fluid">
    <?php if (sizeof($campanhas) == 0) { ?>
    <h2 class="fg-color-red">Nenhuma Campanha Cadastrada Para Esta Turma</h2>
    <?php } else { ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th width="20%">Nome</th>
                <th width="40%">Descri&ccedil;&atilde;o</th>
                <th width="10%">In&iacute;cio</th>
                <th width="10%">Fim</th>
                <th width="10%">Status</th>
                <th width="10%"></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $dataAtual = strtotime(date("Y-m-d 00:00:00"));
            foreach ($campanhas as $campanha):
                if ((strtotime($campanha['Campanha']['data_fim']) >= $dataAtual) &&
                        (strtotime($campanha['Campanha']['data_inicio']) <= $dataAtual)) {
                    $status = "Aberta";
                    $class = "fg-color-green";
                } else {
                    $status = "Fechada";
                    $class = "fg-color-red";
                }
                ?>
                <tr>
                    <td><?= $campanha['Campanha']['nome'] ?></td>
                    <td><?= $campanha['Campanha']['descricao'] ?></td>
                    <td><?= date('d/m/Y', strtotime($campanha['Campanha']['data_inicio'])) ?></td>
                    <td><?= date('d/m/Y', strtotime($campanha['Campanha']['data_fim'])) ?></td>
                    <td class="<?= $class?>"><?= $status ?></td>
                    <td>
                        <button class="button mini bg-color-greenDark"
                           data-bind="click: function() {
                           page('<?= "/{$this->params['prefix']}/campanhas/exibir/{$campanha['Campanha']['id']}" ?>') }">
                            Visualizar
                        </button>
                <?php if($this->params['prefix'] == 'planejamento'){ ?>
                        <button class="button mini bg-color-blueDark"
                           data-bind="click: function() {
                           page('<?= "/{$this->params['prefix']}/campanhas/editar/{$campanha['Campanha']['id']}" ?>') }">
                            Alterar
                        </button>    
                <?php } ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php } ?>
</div>
