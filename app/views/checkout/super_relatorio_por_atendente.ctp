<link href='<?= $this->webroot ?>metro/css/fullcalendar/fullcalendar.css' rel='stylesheet' />
<link href='<?= $this->webroot ?>metro/css/fullcalendar/custom.css?v=0.1' rel='stylesheet' />
<link href='<?= $this->webroot ?>metro/css/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='<?= $this->webroot ?>metro/js/fullcalendar/loader.js'></script>
<script src='<?= $this->webroot ?>metro/js/fullcalendar/checkout_atendente.js'></script>
<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: reload"></a>
        Relatorio de Checkout Por Atendente
    </h2>
</div>
<div class='calendar-container' id='calendar-container'>
    <div class='carregando-eventos'>
        <span>Carregando</span>
    </div>
    <div class='calendar-header'>
        <div class='calendar-nav'>
            <nav class='month'>
                <span id="custom-prev" class="custom-prev"></span>
                <span id="custom-next" class="custom-next"></span>
            </nav>
            <h2><b id='current-month'></b><small id='current-year'></small></h2>
        </div>
    </div>
    <div class='calendar-content' id='calendar-content'>
        <div id='calendar'></div>
    </div>
</div>