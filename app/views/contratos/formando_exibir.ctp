<script type="text/javascript">
    $(document).ready(function(){
        $('.imprimir').click(function(){
            window.print();
        });
    });
</script>
<style type="text/css">
@media print {
    body * {
        background: none    !important;
        visibility: hidden  !important;
        position:   static  !important;
    }
    .area_print, .area_print * {
        visibility: visible !important;
    }
    .area_print {
        position: absolute !important;
        left:     0        !important;
        top:      10px     !important;
        width:    auto     !important;
        height:   auto     !important;
        margin:   20px     !important;
        padding:  50px      !important;
    }
}
</style>
<div class="row-fluid">
    <div class="span10">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Contrato
            <button class="button mini bg-color-blueDark imprimir pull-right">
            Imprimir
            <i class="icon-download"></i>
        </a>
        </h2>
    </div>
</div>
<div class="row-fluid">
    <div class="border-color-gray span10 area_print"
        style='height:600px; overflow:auto; border-width: 1px; padding:12px; font-size:12px'>
        <?=$this->element('contratos/condicoes_geral'); ?>
    </div>
</div>