<span id="conteudo-titulo" class="box-com-titulo-header">IGP-M</span>
<div id="conteudo-container">
	<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
	<?php $session->flash(); ?>
	<div class="tabela-adicionar-item" >
		<?php echo $html->link('Novo IGP-M',array($this->params['prefix'] => true, 'action' => 'adicionar')); ?>
		<div style="clear:both;"></div>
	</div>

	<div class="container-tabela">
	<table>
		<thead>
			<tr>
				<th scope="col"><?php echo $paginator->sort('Mês/Ano','data'); ?></th>
				<th scope="col"><?php echo $paginator->sort('Valor','valor'); ?></th>
				<th scope="col">&nbsp;</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="2"><?php echo $paginator->counter(array('format' => 'IGP-M %start% ao %end% - página %page% de %pages%')); ?>
					<span class="paginacao">
						<?php echo $paginator->numbers(array('separator' => ' ')); ?>
					</span>
				</td>
					<td colspan="1"><?php echo $paginator->counter(array('format' => 'Total : %count% ' .  $this->name)); ?></td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach($igpm as $val): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<td colspan="1">
						<?php 
							echo $val['Igpm']['mes'] . '/' . $val['Igpm']['ano'];
						?>
					</td>
					<td colspan="1"><?php echo $val['Igpm']['valor'];?></td>
					<td colspan="1"><?php echo $html->link('Editar', array($this->params['prefix'] => true, 'controller' => 'igpm', 'action' =>'editar', $val['Igpm']['id']), array('class' => 'submit button')); ?></td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>

</div>
