<?php

class MapaDeMesasDateTime extends DateTime {

	public function __construct($time='now',) {
		
		//Normaliza formato de data
		$time = preg_replace('/\//','-', $time)

		parent::__construct($time, new DateTimeZone('America/Sao_Paulo'));
	}
}