<script type="text/javascript">
	jQuery(function($) {
 		$('span[id*="data-despesa"]').bind("click", function() {
 	 		if(!$(this).hasClass('clicado')) {
				var despesa = $(this).attr("id").substring(13);
				console.log($(this).html());
				$(this).addClass("clicado");
				$(this).fadeOut(500, function() {
					var input = "<input type='text' name='alt-data-despesa' style='padding:3px; font-size:11px; width:70px' value='" + $(this).html().replace(/^\s+|\s+$/g,"");
					input += "' />&nbsp;";
					var button = "<span class='submit button' onclick='alteraData(" + despesa + ")' id='ok-despesa-" + despesa + "'>Ok</span>";
					$(this).html(input + button);
					$(this).fadeIn(500);
				});
 	 		}
		});
 		$(".vincular-pagamento").click(function() {
 	 		var dados = $(this).attr("id").split("-");
 	 		var pagamento = dados[1];
 	 		var usuario = dados[3];
 	 		var td = $(this).parent();
 	 		if(<?=sizeof($despesas_adesao)?> > 0) {
 	 	 		var html = "<select name='despesa-" + pagamento + "-para-vincular' style='font-size:11px!important' id='select-despesa-" + pagamento + "'>";
 	 	 		html += "<option value='' selected='selected'>Parcela</option>";
 	 	 		var url = "/<?=$this->params["prefix"]?>/vincular_pagamentos/despesas";
 		 	 	$.ajax({
 		 	 		url : url,
 		 	 		data : { "usuario" : usuario },
 		 	 		type : "POST",
 		 	 		dataType : "json",
 		 	 		success : function(response) {
 	 	 	 		$.each(response,function(i,despesa) {
 	 	 	 	 		if(despesa.Despesa.data_vencimento != null)
 		 	 	 			var data = despesa.Despesa.data_vencimento.split("-").reverse().join("/");
 	 	 	 	 		else
 	 	 	 	 	 		var data = "sem dada de Pagto.";
 		 	 	 		html += "<option value='" + despesa.Despesa.id + "'>" + despesa.Despesa.tipo + " - " + data + "</option>";
 	 	 	 		});
 		 	 	 	html += "</select>&nbsp;&nbsp;";
 		 	 		html += "<span class='ok-pagamento submit' id='ok-pagamento-" + pagamento + "'>OK</span>";
 	 	 	 		td.html(html);
 	 	 	 		$("#div-pagamento-" + pagamento).show("slow");
 	 	 	 		carregaPagamento();
 		 	 		},
 		 	 		error : function() {
 	 	 	 		alert("Erro ao listar parcelas. Tente novamente mais tarde.");
 		 	 		}
 		 	 	});
 	 		} else {
 	 	 		alert("Formando sem despesa para vincular o pagamento");
 	 		}
 		});
 	});

 	function alteraData(despesa) {
 	 	var button = $("#ok-despesa-"+despesa);
 	 	var input = button.prev();
 	 	if(input.val() != "") {
	 	 	var span = $("#data-despesa-"+despesa);
	 	 	button.fadeOut(500, function() {
	 	 	 	span.append("<img src='<?=$this->webroot?>img/load.gif' width='12' /></center><div>");
	 	 	 	var url = "/<?=$this->params["prefix"]?>/area_financeira/despesa_alterar_data";
		 	 	$.ajax({
		 			url : url,
		 			dataType : "json",
		 			type : "POST",
		 			data : { "despesa" : despesa , "data" : input.val() },
		 			success : function(response) {
		 				if(response.error) {
			 				alert('erro ao alterar data da despesa');
							span.fadeOut(500, function() {
			 					span.html(input.val());
			 					span.removeClass('clicado');
			 					span.fadeIn(500);
							});
		 				} else {
		 					span.fadeOut(500, function() {
			 					span.html(input.val());
			 					span.removeClass('clicado');
			 					span.css("color","green");
			 					span.fadeIn(500);
							});
		 				}
		 			},
		 			error : function(response) {
		 				alert("Erro ao alterar data da despesa");
		 			}
		 	 	});
	 	 	});
 	 	} else {
 	 	 	alert("digite a data de vencimento");
 	 	}
 	}
 	
	function carregaPagamento() {
		$(".ok-pagamento").click( function() {
			var td = $(this).parent();
	 	 	var tr = td.parent();
	 	 	var select = $(this).prev();
	 	 	var despesa = select.val();
	 	 	if(despesa != "") {
 	 	 		var pagamento = $(this).attr("id").substring(13);
 	 	 		var load = "<img src='<?=$this->webroot?>img/load.gif' width='15' />";
 	 	 		$(this).fadeOut(500);
 	 	 		td.append(load);
 	 	 		url = "/<?=$this->params["prefix"]?>/vincular_pagamentos/vincular";
 	 	 		$.ajax({
 	 	 	 		url : url,
 	 	 	 		data : { "despesa" : despesa , "pagamento" : pagamento , 'usuario' : '<?=$formando['id']?>' },
 	 	 	 		type : "POST",
 	 	 	 		dataType : "json",
 	 	 	 		success : function(response) {
	 	 	 	 		if(response.erro) {
		 	 	 	 		alert("Erro ao vincular pagamento. Tente novamente");
	 	 	 	 		} else {
	 	 	 	 			$("#despesa-" + despesa + "-status").css('color','green');
		 	 	 	 		$("#despesa-" + despesa + "-status").html('paga');
		 	 	 	 		var html = "<td colspan='7' align='center'>Pagamento vinculado com sucesso</td>";
		 	 	 	 		tr.hide();
		 	 	 	 		tr.html(html);
		 	 	 	 		tr.fadeIn(500);
	 	 	 	 		}
 	 	 	 		},
 	 	 	 		error : function() {
	 	 	 	 		alert("Erro ao conectar servidor. Tente novamente mais tarde.");
 	 	 	 		}
 	 	 		});
 	 		} else {
 	 	 		alert("Selecione uma parcela");
 	 		}
	 	});
	}

 </script>
<style type="text/css">
.tooltip { z-index: 9999 }
</style>
<?=$this->element('despesas_formando', array('config' => array('gerar_boleto' => true, 'cancelar_contrato' => true, 'renegociar_contrato' => true, 'vincular_pagamentos' => true))); ?>