<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/metroui/table.css">
<div class="row-fluid">
	<div class="span12">
		<h2>
			<a class="metro-button reload" data-bind="click: function() { reload() }"></a>
			Campanhas
		</h2>
	</div>
</div>
<br />
<?php $session->flash(); ?>
<div class="row-fluid">
	<div class="span12">
		<?php if(sizeof($campanhas) == 0) { ?>
		<h2 class="fg-color-red">Nenhuma Campanha Cadastrada Para Esta Turma</h2>
		<?php } else { ?>
		<table class="table table-condensed table-striped">
			<thead>
				<tr>
					<th scope="col">Nome</th>
					<th scope="col">Descri&ccedil;&atilde;o</th>
					<th scope="col">In&iacute;cio</th>
					<th scope="col">Fim</th>
					<th scope="col">Status</th>
					<th scope="col">#</th>
				</tr>
			</thead>
			<tbody>
			<?php
			$dataAtual = strtotime(date("Y-m-d 00:00:00"));
			foreach ($campanhas as $campanha):
				if((strtotime($campanha['Campanha']['data_fim']) >= $dataAtual) &&
					(strtotime($campanha['Campanha']['data_inicio']) <= $dataAtual)) {
					$status = "Aberta";
					$class = "fg-color-green";
				} else {
					$status = "Fechada";
					$class = "fg-color-red";
				}
			?>
				<tr>
					<td><?=$campanha['Campanha']['nome']?></td>
					<td><?=$campanha['Campanha']['descricao']?></td>
					<td><?=date('d/m/Y',strtotime($campanha['Campanha']['data_inicio']))?></td>
					<td><?=date('d/m/Y',strtotime($campanha['Campanha']['data_fim']))?></td>
					<td class="strong <?=$class?>"><?=$status?></td>
					<td>
						<a class="button default"
							data-bind="click: function() {
								page('<?="/{$this->params['prefix']}/campanhas/exibir/{$campanha['Campanha']['id']}"?>') }">
							Visualizar
						</a>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
		<?php } ?>
	</div>
</div>