<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootstrap/select.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/datepicker.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/form_validate.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/wizard.css">
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/transition.js?v=0.1"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/modal.js?v=0.1"></script>
<script type="text/javascript" src="<?=$this->webroot ?>js/bootstrap/bootstrap-button.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/bootbox.js?v=0.2"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/utils.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/bootstrap/datepicker.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/max/bootstrap/wizard.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/max/knockout/checkout.js"></script>
<style type="text/css">
.btn.button.mini.active { background-color: black!important }
.btn { text-shadow: none!important }
td > .btn-group { margin-bottom: 0!important }
.naoexibir { position: absolute!important; left:-100%!important; z-index:-1!important; }
.input-control textarea { min-height: initial!important }
.img-center {
    display:block;
    width:60%;
    height:auto;
    margin: 10px auto;
    border-radius:4px;
}
.nome {
    font-size: 15px;
    text-align: center;
    margin-bottom: 5px;
}
.codigo-formando { font-size: 12px; }
.resumo-checkout {
    float:left;
    width:100%;
    margin:15px 0;
    padding:0;
    list-style-type: none;
}
.resumo-checkout li {
    float:left;
    width:50%;
    margin:2px 0;
    padding: 3px;
    box-sizing: border-box;
    font-size: 14px;
    line-height: 15px;
}
.resumo-checkout li:nth-child(odd) {
    padding-right: 10px;
    text-align: right;
    font-size: 13px;
    color:#999
}
.resumo-checkout li i { vertical-align: middle }
.datas{ z-index: 99999 }
.remover { margin-right: 10px }
</style>
<script type="text/javascript">
$(document).ready(function() {
    
    $('.selectpicker').selectpicker();
    $('.datepicker').datepicker();
    $('#formulario-checkout').submit(function(e) {
        e.preventDefault();
    });
    $('.nav-tabs').button();
    $('#wizard').wizard();
    //var context = ko.contextFor($("#content-body")[0]);
    $('#wizard').on('stepclick',function(e,data) {
        //if(data.step == 5) $("#passo-final").fadeIn(500);
    });
    $('#wizard').on('changed',function(e,data) {
        //context.$data.loaded(true);
    });
    $('[data-target="#step6"]').click(function() {
        $("#itens-formatura").hide();
        var tbody = $("#itens-formatura").find('tbody');
        tbody.html('');
        if($("[data-cancelada='0']").length > 0) {
            $("[data-cancelada='0']").each(function() {
                $(this).parents('thead').next('tbody').find("[data-quantidade-mesas]").each(function() {
                    var quantidades = [];
                    if($(this).data('quantidade-mesas') > 0)
                        quantidades.push(($(this).data('quantidade-mesas')*$(this).data('item-quantidade')) + ' Mesa(s)');
                    if($(this).data('quantidade-convites') > 0)
                        quantidades.push(($(this).data('quantidade-convites')*$(this).data('item-quantidade')) + ' Convite(s)');
                    tbody.append("<tr><td>"+$(this).data('item-nome')+"</td><td>"
                            +quantidades.join(' e ')+"</td></tr>");
                });
            });
            $("#itens-formatura").show();
        }
    });
    
    $('body').on('click', '.checkout', function(){
        if(!$(this).hasClass('active'))
            $(this).attr('data-referente', 'checkout');
        else
            $(this).attr('data-referente', '');
    });
    $('body').on('click', '.campanha', function(){
        if(!$(this).hasClass('active'))
            $(this).attr('data-referente', 'campanha');
        else
            $(this).attr('data-referente', '');
    });
    $('body').on('click', '.adesao', function(){
        if(!$(this).hasClass('active'))
            $(this).attr('data-referente', 'adesao');
        else
            $(this).attr('data-referente', '');
    });
});
</script>
<h2>
    <a class="metro-button reload" id="atualizar-pagina"
    href="<?=$this->here?>"></a>
    Checkout Formando
</h2>
<div class="row-fluid" id="checkout">
    <?=$form->hidden(false,array(
        'value' => $formando['Usuario']['id'],
        'data-bind' => "valueWithInit: 'usuario'"
        )); ?>
    <?=$form->create(false,array(
        'url' => "/{$this->params['prefix']}/checkout/cadastrar",
        'id' => 'formulario-checkout')) ?>
    <?=$form->hidden('Usuario.id'); ?>
    <?=$form->hidden('FormandoProfile.id'); ?>
    <div class="row-fluid" data-bind="visible:!carregando()">
        <div class="span3 wizard">
            <div class="row-fluid" style="border-bottom:1px solid #d4d4d4">
                <img src="<?= $this->webroot . $formando['Usuario']['diretorio_foto_perfil']?>"
                    onerror="this.src='/img/no-image.gif'"
                    class="img-center" />
                <p class="nome fg-color-red">
                    <?=$formando['Usuario']['nome']?>
                    <br />
                    <em class="codigo-formando fg-color-gray">
                        <?=$formando['ViewFormandos']['codigo_formando']?> / <?=ucfirst($formando['Usuario']['grupo'])?>
                    </em>
                </p>
            </div>
            <div class="row-fluid">
                <ol class="resumo-checkout">
                    <li>Contrato</li>
                    <li data-bind="html:formatarValor(valores().adesao())"></li>
                    <li>IGPM</li>
                    <li data-bind="html:formatarValor(valores().igpm())"></li>
                    <li>Contrato + IGPM</li>
                    <li data-bind="html:formatarValor(valores().adesao()+valores().igpm())"></li>
                    <li>Campanhas</li>
                    <li data-bind="html:formatarValor(valores().extras())"></li>
                    <li>Compra Checkout</li>
                    <li data-bind="html:formatarValor(valores().checkout())"></li>
                    <li>Valor Pago</li>
                    <li data-bind="html:formatarValor(valores().pago())"></li>
                    <li style="margin-top:20px">Saldo Total</li>
                    <li style="margin-top:20px" data-bind="html:textoSaldo()"></li>
                </ol>
            </div>
        </div>
        <div class="span9">
            <div id="wizard" class="wizard">
                <ul class="steps">
                    <li data-target="#step1" class="active cadastro">
                        Cadastro
                        <span class="chevron"></span>
                    </li>
                    <li data-target="#step2">
                        Ades&atilde;o
                        <span class="chevron"></span>
                    </li>
                    <li data-target="#step3">
                        Campanhas
                        <span class="chevron"></span>
                    </li>
                    <li data-target="#step4">
                        Comprar
                        <span class="chevron"></span>
                    </li>
                    <li data-target="#step5"
                        data-bind="css: { naoexibir: saldo() == 0 }">
                        Pagamento
                        <span class="chevron"></span>
                    </li>
                    <li data-target="#step6" class="final"
                        data-bind="css: { naoexibir: !exibirFinalizar() }">
                        Finalizar
                        <span class="chevron"></span>
                    </li>
                </ul>
            </div>
            <div class="step-content">
                <div class="step-pane active" id="step1">
                    <h2 class="fg-color-red">Dados Cadastrais</h2>
                    <br />
                    <?php include('realizar/cadastro.ctp'); ?>
                </div>
                <div class="step-pane" id="step2">
                    <?php include('realizar/adesao.ctp'); ?>
                </div>
                <div class="step-pane" id="step3">
                    <?php include('realizar/campanhas.ctp'); ?>
                </div>
                <div class="step-pane" id="step4">
                    <?php include('realizar/comprar.ctp'); ?>
                </div>
                <div class="step-pane" id="step5">
                    <?php include('realizar/pagamento.ctp'); ?>
                </div>
                <div class="step-pane" id="step6">
                    <?php include('realizar/finalizar.ctp'); ?>
                </div>
            </div>
        </div>
    </div>
    <?= $form->end(array('label' => false, 'div' => false, 'class' => 'hide')); ?>
    <div class="row-fluid" data-bind="visible:carregando()">
        <h3>Aguarde</h3>
    </div>
</div>