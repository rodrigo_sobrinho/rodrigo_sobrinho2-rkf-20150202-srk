<?php

App::import('Sanitize');

class Turma extends AppModel {

    var $name = 'Turma';
    var $actsAs = array('Containable');
    var $hasAndBelongsToMany = array(
        'Usuario' =>
        array(
            'className' => 'Usuario',
            'joinTable' => 'turmas_usuarios',
            'foreignKey' => 'turma_id',
            'associationForeignKey' => 'usuario_id',
            'unique' => true
        ),
        'Curso' =>
        array(
            'className' => 'Curso',
            'joinTable' => 'cursos_turmas',
            'foreignKey' => 'turma_id',
            'associationForeignKey' => 'curso_id',
            'unique' => true,
        )
    );
    var $hasMany = array(
        'CursoTurma' => array(
            'className' => 'CursoTurma',
            'joinTable' => 'cursos_turmas',
            'foreignKey' => 'turma_id',
            'unique' => true,
        )
    );
    var $validate = array(
        'nome' => array(
            'required' => array(
                'rule' => 'notEmpty',
                'message' => 'Campo obrigatório'
            )
        ),
        'ano_formatura' => array(
            'numeric' => array(
                'rule' => 'numeric',
                'message' => 'Insira um número'
            ),
            'required' => array(
                'rule' => 'notEmpty',
                'message' => 'Campo obrigatório'
            )
        ),
        'expectativa_formandos' => array(
            'numeric' => array(
                'rule' => 'numeric',
                'message' => 'Insira um número'
            ),
            'required' => array(
                'rule' => 'notEmpty',
                'message' => 'Campo obrigatório'
            )
        )
    );
    var $semestre_formatura = array(
        '1' => 'Primeiro',
        '2' => 'Segundo');
    var $expectativa_fechamento = array(
        'alta' => 'Alta',
        'media' => 'Média',
        'baixa' => 'Baixa');
    var $status = array(
        'aberta' => 'Aberta',
        'fechada' => 'Fechada',
        'descartada' => 'Descartada',
        'concluida' => 'Concluida');
    var $como_chegou = array(
        'internet' => 'Internet',
        'indicacao' => 'Indicação',
        'consultor' => 'Consultor',
        'outros' => 'Outros');
    var $pretensao = array(
        'festa' => 'Festa',
        'colacao' => 'Colação',
        'ambos' => 'Ambos');
    var $adesoes = array(
        'abertas' => 'Abertas',
        'fechadas' => 'Fechadas'
    );
    var $checkout = array(
        'aberto' => 'Aberto',
        'fechado' => 'Fechado'
    );
    var $cascata = array(
        0 => 'Não',
        1 => 'Sim'
    );

    function alocarUsuario($usuario_id, $turma_id) {
        $usuarioId = Sanitize::paranoid($usuario_id);
        $turmaId = Sanitize::paranoid($turma_id);
        $turmaUsuario = $this->query("SELECT * FROM turmas_usuarios WHERE turma_id = " . $turmaId . " AND usuario_id = " . $usuarioId);
        if (count($turmaUsuario) == 0) {
            $query = $this->query("INSERT INTO turmas_usuarios (turma_id, usuario_id) VALUES (" . $turmaId . "," . $usuarioId . ")");
            return true;
        } else {
            return false;
        }
    }

    function removerUsuarioAlocado($usuario_id, $turma_id) {
        $usuarioId = Sanitize::paranoid($usuario_id);
        $turmaId = Sanitize::paranoid($turma_id);
        $this->query("DELETE FROM turmas_usuarios WHERE turma_id = " . $turmaId . " AND usuario_id = " . $usuarioId);
        return true;
    }

    function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
        $parameters = compact('conditions');
        $this->recursive = $recursive;
        $count = $this->find('count', array_merge($parameters, $extra));
        if (isset($extra['group'])) {
            $count = $this->getAffectedRows();
        }
        return $count;
    }

}

?>