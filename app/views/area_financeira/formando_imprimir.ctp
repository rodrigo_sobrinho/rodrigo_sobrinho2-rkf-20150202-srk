<h2><b>&Aacute;rea financeira</b></h2>

<div id="conteudo-container">
	
	<h2><b>Despesas com Ades&atilde;o</b></h2>
	<br>
	<?php
	if( sizeof($despesas_adesao) == 0 ) {
		echo 'Nenhuma despesa com adesão encontrada.';
	} else {
	?>
	<div class="container-tabela-financeiro">
		<table class="tabela-financeiro tabela-boletos">
			<thead>
				<tr>
					<th scope="col">Id</th>
					<th scope="col">Valor</th>
					<th scope="col">Data de Vencimento</th>
					<th scope="col">Parcela</th>
					<th scope="col">Correção IGPM</th>
					<th scope="col">Multa</th>
					<th scope="col">Status</th>
				</tr>
			</thead>
			<tfoot>
				<tr class="linha-despesa">
					<td colspan="5"></td>
					<td colspan="2"><?=count($despesas_adesao);?> despesas com ades&atilde;o</td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach ($despesas_adesao as $despesa_adesao): ?>
				<?php if($isOdd):?>
					<tr class="odd linha-despesa"  id = "linha-despesa-<?=$despesa_adesao['Despesa']['id']; ?>">
				<?php else:?>
					<tr class="linha-despesa" id = "linha-despesa-<?=$despesa_adesao['Despesa']['id']; ?>">
				<?php endif;?>
					<td  colspan="1" width="10%"><?=$despesa_adesao['Despesa']['id']; ?></td>
					<td  colspan="1" width="15%"><?=$despesa_adesao['Despesa']['valor']; ?></td>
					<td  colspan="1" width="15%"><?=date('d/m/Y', strtotime($despesa_adesao['Despesa']['data_vencimento'])); ?></td>					
					<td  colspan="1" width="15%"><?=$despesa_adesao['Despesa']['parcela'] . ' de ' . $despesa_adesao['Despesa']['total_parcelas']; ?></td>
					<td  colspan="1" width="15%"><?=$despesa_adesao['Despesa']['correcao_igpm']; ?></td>
					<td  colspan="1" width="15%"><?=$despesa_adesao['Despesa']['multa']; ?></td>
					<td  colspan="1" width="15%"><?=$despesa_adesao['Despesa']['status']; ?></td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<?php
	// Fim do if que verifica se há despesas com adesão.
	}
	?>
	<h2><b>Despesas com Extras</b></h2>
	<br>
	<?php
	if( sizeof($despesas_extras) == 0 ) {
		echo 'Nenhuma despesa com extras encontrada.';
	} else {
	?>
	<div class="container-tabela-financeiro">
		<table class="tabela-financeiro tabela-boletos">
			<thead>
				<tr>
					<th scope="col">Id</th>
					<th scope="col">Campanha</th>
					<th scope="col">Valor</th>
					<th scope="col">Data de Vencimento</th>
					<th scope="col">Parcela</th>
					<th scope="col">Correção IGPM</th>
					<th scope="col">Multa</th>
					<th scope="col">Status</th>
				</tr>
			</thead>
			<tfoot>
				<tr class="linha-despesa">
					<td colspan="5">
					</td>
					<td colspan="3"><?=count($despesas_extras);?> despesas com extras</td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach ($despesas_extras as $despesa_extra): ?>
				<?php if($isOdd):?>
					<tr class="odd linha-despesa" id = "linha-despesa-<?=$despesa_extra['Despesa']['id']; ?>">
				<?php else:?>
					<tr class="linha-despesa" id = "linha-despesa-<?=$despesa_extra['Despesa']['id']; ?>">
				<?php endif;?>
					<td  colspan="1" width="10%"><?=$despesa_extra['Despesa']['id']; ?></td>
					<td  colspan="1" width="35%"><?=$despesa_extra['CampanhasUsuario']['Campanha']['nome']; ?></td>
					<td  colspan="1" width="10%"><?=$despesa_extra['Despesa']['valor']; ?></td>
					<td  colspan="1" width="5%"><?=date('d/m/Y', strtotime($despesa_extra['Despesa']['data_vencimento'])); ?></td>					
					<td  colspan="1" width="15%"><?=$despesa_extra['Despesa']['parcela'] . ' de ' . $despesa_extra['Despesa']['total_parcelas']; ?></td>
					<td  colspan="1" width="5%"><?=$despesa_extra['Despesa']['correcao_igpm']; ?></td>
					<td  colspan="1" width="10%"><?=$despesa_extra['Despesa']['multa']; ?></td>
					<td  colspan="1" width="10%"><?=$despesa_extra['Despesa']['status']; ?></td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<?php
	// Final do if que verifica se há despesas com extras 
	}
	?>

	<h2><b>Boletos gerados</b></h2>
	<?php
	if( count($boletos) == 0 ) {
		echo 'Nenhum boleto encontrado.';
	} else {
	?>
	<div class="container-tabela-financeiro">
		<table class="tabela-financeiro tabela-boletos" >
			<thead>
				<tr>
					<th scope="col">Tipo</th>
					<th scope="col">N&uacute;mero do documento</th>
					<th scope="col">Valor Nominal</th>
					<th scope="col">Valor Recebido</th>
					<th scope="col">Data de Vencimento</th>
					<th scope="col">Status</th>
					<th scope="col">Despesas do boleto</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="5">
					</td>
					<td colspan="2"><?=count($boletos);?> boletos encontrados.</td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach ($boletos as $boleto): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<td  colspan="1" width="10%"><?=$boleto['Pagamento']['tipo']; ?></td>
					<td  colspan="1" width="15%"><?=$boleto['Pagamento']['codigo']; ?></td>
					<td  colspan="1" width="10%"><?=$boleto['Pagamento']['valor_nominal']; ?></td>
					<td  colspan="1" width="10%"><?=$boleto['Pagamento']['valor_pago']; ?></td>
					<td  colspan="1" width="10%"><?=date('d/m/Y', strtotime($boleto['Pagamento']['dt_vencimento'])); ?></td>					
					<td  colspan="1" width="10%"><?=$boleto['Pagamento']['status']; ?></td>
					<td  colspan="1" width="35%">
						<a class="despesas-do-boleto" href="#conteudo-titulo"><?php 
							$primeiraVez = true;
							foreach ($boleto['DespesaPagamento'] as $despesaPagamento) { 
								if ($primeiraVez)
									$primeiraVez = false;
								else
									echo " - ";
								echo $despesaPagamento['despesa_id']; 
							} ?></a>
					</td>

				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>		
	</div>
	<?php
	// Final do if que verifica se há boletos
	}
	?>

	<h2><b>Resumo financeiro</b></h2>
	<br/>
	<div class="detalhes">
		<p class="grid_11 alpha omega first">
			<span class="grid_11 alpha omega"> <b>Total Recebido:</b> R$ <?=$total_pago;?> </span>
		</p>
		<p class="grid_11 alpha omega first">
			<span class="grid_11 alpha omega"> <b>Total Despesas:</b> R$ <?=$total_despesas;?> </span>
		</p>
		<p class="grid_11 alpha omega first">
			<span class="grid_11 alpha omega"> <b>Saldo: </b> R$ <?=$saldo_atual;?> </span>
		</p>
	</div>
	
</div>