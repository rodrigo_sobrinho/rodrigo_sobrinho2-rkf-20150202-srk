<?php

class EnquetePergunta extends AppModel {

    var $name = 'EnquetePergunta';
    var $belongsTo = array('Enquete');
    var $hasMany = array('EnqueteAlternativa' => array('order' => 'ordem'));

}