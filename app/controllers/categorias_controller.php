<?php

class CategoriasController extends AppController {
	
	var $name = 'Categorias';

	var $uses = array('Categoria');
	
	private $config = array(
		'marketing' => array(
			'tipo' => 'parcerias'
		)
	);
	
	function marketing_index() {
		$categorias['ativas'] = $this->Categoria->find('all',array('conditions' => array('status' => '1'),'order' => 'ordem asc'));
		$categorias['inativas'] = $this->Categoria->find('all',array('conditions' => array('status' => '0')));
		$this->set('categorias',$categorias);
	}
	
	function marketing_adicionar() {
		$this->layout = false;
		$this->autoRender = false;
		Configure::write(array('debug' => 0));
		if(isset($_POST['categoria'])) {
			$categoria['Categoria']['tipo'] = "parcerias";
			$categoria['Categoria']['nome'] = $_POST['categoria'];
			if(!$this->Categoria->save($categoria))
				$json = array('error' => true, "message" => "Erro ao criar categoria");
			else
				$json = array('error' => false, "categoria" => $this->Categoria->getLastInsertId());
		} else {
			$json = array('error' => true, "message" => "Categoria inv&aacute;lida");
		}
		echo json_encode($json);
	}
	
	function marketing_atualizar() {
		$this->layout = false;
		$this->autoRender = false;
		Configure::write(array('debug' => 0));
		if(!isset($_POST['categorias'])) {
			$json = array('error' => true, "message" => "Categorias inv&aacute;lidas");
		} else {
			$categorias = $_POST['categorias'];
			$json = array('error' => false);
			foreach($categorias as $categoria)
				if(!$this->Categoria->saveAll($categoria))
					$json = array('error' => true, "message" => "Erro ao inserir categorias");
		}
		echo json_encode($json);
	}
	
	function marketing_apagar() {
		$this->layout = false;
		$this->autoRender = false;
		Configure::write(array('debug' => 0));
		if(!isset($_POST['categoria'])) {
			$json = array('error' => true, "message" => "Categorias inv&aacute;lidas");
		} else {
			$categoria = $_POST['categoria'];
			$json = array('error' => false);
			if(!$this->Categoria->delete($categoria['id']))
				$json = array('error' => true, "message" => "Erro ao apagar categoria");
		}
		echo json_encode($json);
	}

}