<?php

class AppController extends Controller {

    var $helpers = array('Html', 'Form', 'Javascript', 'SidebarMenu', 'SidebarItens',
        'TopbarMenu', 'Ajax', 'Text', 'Session', 'Datas');
    var $components = array('Session', 'Auth', 'RequestHandler', 'ListaSidebarMenu', 'ListaTopbarMenu','SiteTurma');
    var $itensDoSidebar = array();
    var $avisos = array();
    var $sideBar = array();
    var $uses = array('Item', 'Assunto', 'Mensagem', 'Usuario','UsuarioAcesso',
        'Turma', 'TurmasUsuario', 'Cronograma', 'FormandoProfile');
    var $siteDeTurma = false;
    var $app = array();

    function beforeFilter() {
        foreach($this->app as $app)
            $this->Auth->allow("app_$app");
        
        $this->configurarLogin();
        $this->obterTurmaPorSite();
        $this->verificarTurmaFormandoOuComissao();
        $this->configurarSidebar();
        $this->configurarTopbar();
        $this->enviarTurmaParaView();
        $this->redirecionarParaSelecaoDeTurmasCasoNecessario();
        $this->verificarRedirecionamento();
        if (!defined('URL_IMG')) define('URL_IMG', 'http://sistema.asformaturas.com.br/');
        if($this->ambiente() == 'desenvolvimento') {
            header('Access-Control-Allow-Origin: *');
            header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
        }
    }
    
    function aumentar_memoria($m) {
        ini_set('memory_limit', $m);
        set_time_limit(0);
    }
    
    function obterTurmaPorSite() {
        if($this->SiteTurma->eSiteDeTurma()) {
            $turmaDoSite = $this->SiteTurma->obterTurma();
            if(empty($turmaDoSite))
                header('Location: '.Configure::read('url_site'));
            elseif($turmaDoSite['Turma']['site_liberado'] == 0)
                header('Location: '.Configure::read('url_site'));
            else {
                $this->set('turmaLogada',$turmaDoSite);
                $this->siteDeTurma = true;
            }
        }
    }
    
    function verificarRedirecionamento() {
        if($this->Session->check('redirecionar')) {
            $redirecionar = $this->Session->read('redirecionar');
            $this->Session->delete('redirecionar');
            $this->set('redirecionar', $redirecionar);
        }
    }

    function create_date_time_from_format($fmt, $strdate) {
        $data_formatada = null;

        if ($strdate != '' && strlen($strdate) >= 10) {
            if (preg_match('/d\-m\-Y.*/', $fmt)) {
                $arr_datetime = explode(' ', $strdate);
                $data = $arr_datetime[0];

                $hora = '';
                if (isset($arr_datetime[1]))
                    $hora = $arr_datetime[1];


                $arr_data = explode('-', $data);

                $data_formatada = $arr_data[2] . '-' . $arr_data[1] . '-' . $arr_data[0] . ' ' . $hora;
            } else if (preg_match('/Y\-m\-d.*/', $fmt)) {
                $arr_datetime = explode(' ', $strdate);
                $data = $arr_datetime[0];

                $hora = '';
                if (isset($arr_datetime[1]))
                    $hora = $arr_datetime[1];


                $arr_data = explode('-', $data);

                $data_formatada = $arr_data[2] . '-' . $arr_data[1] . '-' . $arr_data[0] . ' ' . $hora;
            }
        }

        if ($data_formatada)
            return new DateTime($data_formatada);
        else
            return null;
    }

    private function configurarLogin() {
        $this->configurarParametrosDeLogin();
        $this->configurarSwitchComissaoFormando();
        $this->enviarUsuarioLogadoParaView();
    }
    
    private function configurarLinkRifa() {
        $turma = $this->obterTurmaLogada();
        $usuario = $this->obterUsuarioLogado();


        // Verificar se formando tem cupons de rifa
        $cupons = $this->RifaTurma->Rifa->Cupom->find('count', array('conditions' => array('Usuario.id' => $usuario['Usuario']['id'], 'not' => array('status' => 'cancelado'))));

        // Verificar se a turma do formando tem rifa ativa
        $rifas_ativas = $this->RifaTurma->find('count', array('conditions' => array('turma_id' => $turma['Turma']['id'], 'ativo' => 1, 'Rifa.ativa' => 1, 'Rifa.data_sorteio >' => date('Y-m-d', strtotime('+1 day')))));

        $mostrar_link_rifa = false;

        if ($cupons > 0 || $rifas_ativas > 0) {
            $mostrar_link_rifa = 1;
        }


        $this->Session->write('mostrar_link_rifa', $mostrar_link_rifa);
    }

    function configurarSwitchComissaoFormando() {
        $usuario = $this->obterUsuarioLogado();

        // Verificar grupo
        if ($usuario['Usuario']['grupo'] == 'comissao' || ( $usuario['Usuario']['grupo'] == 'formando' && $this->Session->check('mudouGrupo') && $this->Session->read('mudouGrupo') )) {
            $this->FormandoProfile->recursive = -1;
            $formando = $this->FormandoProfile->find('first', array('conditions' => array('usuario_id' => $usuario['Usuario']['id'])));

            if ($formando['FormandoProfile']['aderido'] == 1) {
                $this->Session->write('exibirSwitchComissaoFormando', true);
                $this->log('Exibir switch!!', 'debug');
            } else {
                $this->log('Não Exibir switch!!', 'debug');
                $this->Session->write('exibirSwitchComissaoFormando', false);
            }
        } else {
            $this->Session->write('exibirSwitchComissaoFormando', false);
        }
    }
    
    function setarTurmaLogada($turmaId) {
        $options['conditions'] = array(
            'Turma.id' => $turmaId
        );
        $this->Session->delete('turma');
        $this->Turma->recursive = 2;
        $this->Turma->contain(array(
            'CursoTurma.Curso.Faculdade.Universidade',
            'Usuario' => array('conditions' => array('grupo' => array('comercial', 'planejamento', 'atendimento')))
        ));
        $turma = $this->Turma->find('first',$options);
        if($turma)
            $this->Session->write('turma', $turma);
        else
            $this->Session->setFlash('Turma não existente ou sem permissão de acesso',
                'metro/flash/error');
    }
    
    function ambiente() {
        $env = Configure::read('env');
        if(empty($env))
            $env = "producao";
        return $env;
    }

    function obterTurmaLogada() {
        $turma = $this->Session->read('turma');
        return $turma;
    }
    
    function obterUsuarioLogado() {
        return $this->Session->check('Usuario') ? $this->Session->read('Usuario') : $this->Auth->user();
    }

    /**
     * Retorna o usuário que verdadeiramente eestá logado no sistema
     * @return [type] [description]
     */
    function obterUsuarioReal() {
        if($this->Session->check('Usuario'))
            return $this->Session->check('UsuarioLogado') ? $this->Session->read('UsuarioLogado') : $this->Session->read('Usuario');

        $this->obterUsuarioLogado();
    }
    
    function eFuncionario() {
        return !in_array($this->Session->read('Usuario.Usuario.grupo'),array('formando','comissao'));
    }

    function verificarTurmaFormandoOuComissao() {
        if (!$this->Session->check('turma') && $this->Auth->user()) {
            $usuario = $this->obterUsuarioLogado();
            if ($usuario['Usuario']['grupo'] == 'comissao' || $usuario['Usuario']['grupo'] == 'formando') {
                $this->Turma->recursive = 2;
                $this->Turma->contain(array(
                    'CursoTurma.Curso.Faculdade.Universidade',
                    'TurmasUsuario',
                    'Usuario' => array('conditions' => array('grupo' => array('comercial', 'planejamento')))
                ));
                $this->Turma->bindModel(array('hasOne' => array('TurmasUsuario')), false);
                $turma = $this->Turma->find('first', array('conditions' => array('TurmasUsuario.usuario_id' => $usuario['Usuario']['id'])));

                $fp = $this->FormandoProfile->find('first', array('conditions' => array('usuario_id' => $usuario['Usuario']['id'])));
                $codigo_formando_na_turma = str_pad($fp['FormandoProfile']['codigo_formando'], 3, '0', STR_PAD_LEFT);
                if ($codigo_formando_na_turma != "" && $codigo_formando_na_turma > 0)
                    $codigo_formando = str_pad($turma['Turma']['id'], 5, '0', STR_PAD_LEFT) . $codigo_formando_na_turma;
                else
                    $codigo_formando = "";

                $this->Session->write('turma', $turma);
                $this->Session->write('codigo_formando', $codigo_formando);
            }
        }
    }

    function handleError($code, $description, $file = null, $line = null, $context = null) {
        if (error_reporting() == 0 || $code === 2048 || $code === 8192) {
            return;
        }

        // throw error for further handling
        throw new exception(strip_tags($description));
    }

    function enviarTurmaParaView() {
        $this->set('turmaLogada', $this->obterTurmaLogada());
        $codigo_formando = $this->Session->read('codigo_formando');
        $this->set('codigo_formando', $codigo_formando);
        $this->set('eFuncionario',$this->eFuncionario());
        $this->set('ambiente',$this->ambiente());
    }

    private function configurarParametrosDeLogin() {
        $this->Auth->userModel = 'Usuario';
        $this->Auth->fields = array('username' => 'email', 'password' => 'senha');
        $this->Auth->loginRedirect = array('controller' => 'principal', 'action' => 'index', 'plugin' => false);
        $this->Auth->loginAction = array('controller' => 'usuarios', 'action' => 'login');
        $this->Auth->userScope = array('Usuario.ativo' => 1);
        $this->Auth->authError = "Você n&atilde;o possui permissão para acessar esta p&aacute;gina.";
        $this->Auth->loginError = "Email ou senha incorretos.";
        $this->Auth->authorize = 'controller';
    }

    private function enviarUsuarioLogadoParaView() {
        $usuario = $this->obterUsuarioLogado();
        $this->Usuario->recursive = 0;
        $usuarioFormando = $this->Usuario->find('first', array('conditions' => array('Usuario.id' => $usuario['Usuario']['id'])));
        if(isset($usuarioFormando['FormandoProfile']))
            $usuario['FormandoProfile'] = $usuarioFormando['FormandoProfile'];
        if(isset($usuarioFormando['ViewFormandos']))
            $usuario['ViewFormandos'] = $usuarioFormando['ViewFormandos'];
        $this->set('usuario', $usuario);
        $this->Session->write('Usuario', $usuario);
        $exibirSwitchComissaoFormando = true;
        if ($this->Session->check('exibirSwitchComissaoFormando')) {
            $exibirSwitchComissaoFormando = $this->Session->read('exibirSwitchComissaoFormando');
        }
        $this->set('exibirSwitchComissaoFormando', $exibirSwitchComissaoFormando);

        $emails_com_debug = array('testeplanejamento@as.com', 'bpbueno@gmail.com', 'teste@edicao.com');

        if (in_array($usuario['Usuario']['email'], $emails_com_debug)) {
            Configure::write('debug', 2);
        }
    }

    //restringe o acesso baseado no prefixo

    function isAuthorized() {
        $usuario = $this->obterUsuarioLogado();
        if($usuario && !$this->Session->check('usuario_acesso')) {
            $acesso = array(
                'UsuarioAcesso' => array(
                    'usuario_id' => $this->Auth->user('id'),
                    'data_login' => date('Y-m-d H:i:s')
                )
            );
            if($this->UsuarioAcesso->save($acesso)) {
                $acesso = array('id'=>$this->UsuarioAcesso->getLastInsertId());
                $this->Session->write('usuario_acesso',$acesso);
            }
        }
        if($usuario && $this->params['action'] != 'logout') {
            if(in_array($usuario['Usuario']['grupo'],array('comissao','formando'))) {
                $turma = $this->obterTurmaLogada();
                if(!$turma) {
                    $this->Usuario->recursive = 1;
                    $usuario = $this->Usuario->read(null,$usuario['Usuario']['id']);
                    if(isset($usuario['Turma'][0]))
                        $turma['Turma'] = $usuario['Turma'][0];
                }
                if($turma) {
                    if($turma['Turma']['status'] == 'descartada') {
                        if($this->Session->check('usuario_acesso')) {
                            $acesso = $this->Session->read('usuario_acesso');
                            $acesso['observacoes'] = 'Turma descartada';
                            $this->Session->write('usuario_acesso',$acesso);
                        }
                        $this->Session->setFlash('Turma foi descartada');
                        $this->redirect("/usuarios/logout");
                    }
                } else {
                    $this->Session->setFlash('Turma não encontrada');
                    $this->redirect("/usuarios/logout");
                }
            }
        }
        if ($this->existePrefixoNaUrl())
            if ($this->usuarioPossuiPermissaoParaAcessarPrefixo())
                return true;
            else {
                $this->redirect("/{$usuario['Usuario']['grupo']}");
            }

        return true;
    }

    private function existePrefixoNaUrl() {
        return isset($this->params['prefix']);
    }

    private function usuarioPossuiPermissaoParaAcessarPrefixo() {
        if (strcasecmp($this->params['prefix'], 'ajax') == 0)
            return true;
        return $this->seraOprefixoIgualAoGrupoDoUsuario();
    }

    private function seraOprefixoIgualAoGrupoDoUsuario() {
        $usuario = $this->obterUsuarioLogado();
        return (strcasecmp($this->params['prefix'], $usuario['Usuario']['grupo']) == 0);
    }

    //configura a sidebar enviando para a view os links e o titulo
    function configurarSidebar() {
        //recebe a configuração pedida do sidebar
        //envia a configuração do sidebar pra view
        $usuario = $this->obterUsuarioLogado();
        $grupo = $usuario['Usuario']['grupo'];

        if (isset($this->nomeDoTemplateSidebar))
            $templateSidebar = ucfirst(strtolower($this->nomeDoTemplateSidebar));
        else
            $templateSidebar = strtolower($this->params['controller']);
        $this->adicionarItemInicioSideBar();
        $this->sideBar = array_merge($this->sideBar,
                ListaSidebarMenuComponent::obterSidebar(
                        $templateSidebar,
                        $grupo,
                        $this->turmaEstaLogada()));
        $this->adicionarItemFinalSideBar();
        $this->set('configuracaoSidebarMenu', $this->sideBar);
    }
    
    function turmaEstaLogada() {
        return $this->Session->check('turma');
    }
    
    function configurarTopbar() {
        $usuario = $this->obterUsuarioLogado();
        $grupo = $usuario['Usuario']['grupo'];
        ListaTopbarMenuComponent::obterTopbar($grupo,$this->turmaEstaLogada());
        if($this->Session->check('UsuarioLogado'))
            ListaTopbarMenuComponent::adicionarLinkTopbar('Sair do Usuario', 'usuarios', 'sair', '');
        $this->set('configuracaoTopbarMenu', ListaTopbarMenuComponent::$configuracaoTopbarMenu);
    }

    function redirecionarParaSelecaoDeTurmasCasoNecessario() {
        if ($this->Session->check('Usuario'))
            if (!($this->Session->check('turma'))) {
                $usuario = $this->obterUsuarioLogado();
                if ((!preg_match("/_senha/i", ($this->params['action']))) &&
                        (strtolower($this->params['controller']) != 'lembretes') &&
                        (strtolower($this->params['controller']) != 'principal') &&
                        (strtolower($this->params['controller']) != 'pendencias') &&
                        (strtolower($this->params['controller']) != 'mensagens') &&
                        (strtolower($this->params['controller']) != 'turmas') &&
                        (strtolower($this->params['controller']) != 'vincular_pagamentos') &&
                        (strtolower($this->params['controller']) != 'area_financeira') &&
                        (strtolower($this->params['controller']) != 'checkout') &&
                        (strtolower($this->params['controller']) != 'catalogo') &&
                        (strtolower($this->params['controller']) != 'solicitacoes') &&
                        (strpos(strtolower($this->params['action']), "atendimento") === false) &&
                        (strpos(strtolower($this->params['action']), "relatorio") === false) &&
                        (strpos(strtolower($this->params['action']), "gerenciar") === false) &&
                        (strtolower($this->params['action']) != 'index') &&
                        (strtolower($this->params['action']) != 'novo'))
                    if (isset($this->params['prefix']))
                        if ($usuario['Usuario']['grupo'] == 'planejamento'){
                            $this->Session->setFlash('Selecione uma turma.', 'flash_erro');
                            $this->redirect("/{$this->params['prefix']}/turmas");
                        } else if ($usuario['Usuario']['grupo'] == 'financeiro' ||
                                $usuario['Usuario']['grupo'] == 'marketing' ||
                                $usuario['Usuario']['grupo'] == 'super' ||
                                $usuario['Usuario']['grupo'] == 'producao' ||
                                $usuario['Usuario']['grupo'] == 'comercial' ||
                                $usuario['Usuario']['grupo'] == 'vendedor' ||
                                $usuario['Usuario']['grupo'] == 'rh' ||
                                $usuario['Usuario']['grupo'] == 'video' ||
                                $usuario['Usuario']['grupo'] == 'foto' ||
                                $usuario['Usuario']['grupo'] == 'criacao'||
                                $usuario['Usuario']['grupo'] == 'planejamento'||
                                $this->params['prefix'] == 'ios') {
                            //Não redireciona
                        }
                        else
                            $this->redirect("/{$this->params['prefix']}/");
            }
    }

    //configura a sidebar de itens enviando os itens para a view
    protected function enviarItensParaView() {
        $this->set('itensPendentesSidebar', $this->itensDoSidebar);
    }

    protected function configurarItensSidebar() {
        $this->Assunto->recursive = 0;
        $this->Item->recursive = 0;
        $this->Cronograma->recursive = 0;
        $turma = $this->obterTurmaLogada();

        $usuario = $this->obterUsuarioLogado();
        $grupo = $usuario['Usuario']['grupo'];

        $grupoDoItem = $grupo;

        if ($grupo == 'comissao') {
            ($turma['Turma']['status'] == 'fechada') ? $grupoDoItem = 'planejamento' : $grupoDoItem = 'comercial';
        }

        if ($turma) {

            if ($grupoDoItem == 'planejamento') {
                $itens_disponiveis = $this->Cronograma->find('list', array('fields' => array('item_id'), 'conditions' => array('turma_id' => $turma['Turma']['id'])));
                $itens = $this->Item->find('all', array(
                    'conditions' => array(
                        'grupo' => $grupoDoItem,
                        'id' => $itens_disponiveis
                    ),
                    'fields' => array('id', 'nome')
                ));
            } else {
                $itens = $this->Item->find('all', array(
                    'conditions' => array(
                        'grupo' => $grupoDoItem
                    ),
                    'fields' => array('id', 'nome')
                ));
            }


            $itens_com_pendencias = array();
            $itens_geral = array();

            foreach ($itens as $item):
                $assuntos = $this->Assunto->find('all', array(
                    'conditions' => array(
                        'item_id' => $item['Item']['id'],
                        'turma_id' => $turma['Turma']['id']
                    ),
                    'fields' => array('pendencia')
                ));

                global $pendencias;
                $pendencias = 0;
                foreach ($assuntos as $assunto):
                    if ($grupo == 'comissao') {
                        if (strcmp($assunto['Assunto']['pendencia'], 'Comissao') == 0) {
                            $pendencias++;
                        }
                    } else {
                        if (strcmp($assunto['Assunto']['pendencia'], 'As') == 0) {
                            $pendencias++;
                        }
                    }
                endforeach;

                $item_aux['nome'] = $item['Item']['nome'];
                $item_aux['quantidadeDeAssuntosPentendentes'] = $pendencias;
                $item_aux['id'] = $item['Item']['id'];

                $itens_geral[$pendencias][] = $item_aux;

            endforeach;

            //Ordernar por pendência decrescente
            sort($itens_geral);
            array_reverse($itens_geral, true);


            $itens_adicionados = 0;

            foreach ($itens_geral as $lista_itens) :
                foreach ($lista_itens as $item) :
                    $this->incluirItemNoSidebar($item['nome'], $item['quantidadeDeAssuntosPentendentes'], $item['id']);
                    $itens_adicionados++;
                endforeach;
            endforeach;
        }
    }

    //utilize no beforefilter dos controllers que tiver que enviar itens pendentes para a view
    protected function incluirItemNoSidebar($nome, $quantidedDeAssuntosPendentes, $id) {
        $item['nome'] = $nome;
        $item['quantidadeDeAssuntosPentendentes'] = $quantidedDeAssuntosPendentes;
        $item['id'] = $id;
        if (isset($this->params['prefix'])) {
            $item['link'] = array($this->params['prefix'] => true, 'controller' => 'itens', 'action' => 'visualizar', $id);
            array_push($this->itensDoSidebar, $item);
        }
        $this->enviarItensParaView();
    }

    protected function adicionarItemInicioSideBar() {
        $args = func_get_args();
        if (!empty($args)) {
            foreach ($args[0] as $link)
                ListaSidebarMenuComponent::adicionarLinkSidebar($link['titulo'], $link['controller'], $link['action'], $link['grupo'], (isset($link['class']) ? $link['class'] : ""), (isset($link['after']) ? $link['after'] : ""));
        }
        $this->sideBar = ListaSidebarMenuComponent::retornaSideBar();
    }

    protected function adicionarItemFinalSideBar() {
        $args = func_get_args();
        if (!empty($args)) {
            foreach ($args as $link)
                ListaSidebarMenuComponent::adicionarLinkSidebar($link['titulo'], $link['controller'], $link['action'], $link['grupo'], (isset($link['class']) ? $link['class'] : ""), (isset($link['after']) ? $link['after'] : ""));
        }
        $this->sideBar = ListaSidebarMenuComponent::retornaSideBar();
    }

    protected function downloadDeArquivo($arquivo) {
        App::import('component', 'ManipulacaoDeArquivos');
        if (is_array($arquivo) && !empty($arquivo['id'])) {
            ManipulacaoDeArquivos::realizarDownload($arquivo['id']);
        } else if (is_string($arquivo) && !empty($arquivo)) {
            ManipulacaoDeArquivos::realizarDownload($arquivo['id']);
        } else {
            return false;
        }
    }

    /**
     * uploads files to the server
     * @params:
     *      $folder     = the folder to upload the files e.g. 'img/files'
     *      $formdata   = the array containing the form files
     *      $itemId     = id of the item (optional) will create a new sub folder
     * @return:
     *      will return an array with the success of each file upload
     *      
     * http://www.jamesfairhurst.co.uk/posts/view/uploading_files_and_images_with_cakephp
     */
    function uploadFiles($folder, $formdata, $itemId = null, $single_file_name = null) {
        // setup dir names absolute and relative
        $folder_url = WWW_ROOT . $folder;
        $rel_url = $folder;

        // create the folder if it does not exist
        if (!is_dir($folder_url)) {
            mkdir($folder_url);
        }

        // if itemId is set create an item folder
        if ($itemId) {
            // set new absolute folder
            $folder_url = WWW_ROOT . $folder . '/' . $itemId;
            // set new relative folder
            $rel_url = $folder . '/' . $itemId;
            // create directory
            if (!is_dir($folder_url)) {
                mkdir($folder_url);
            }
        }

        // list of permitted file types, this is only images but documents can be added
        $permitted = array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/png');

        // loop through and deal with the files
        foreach ($formdata as $file) {
            // replace spaces with underscores
            $filename = str_replace(' ', '_', $file['name']);
            // assume filetype is false
            $typeOK = false;
            // check filetype is ok
            foreach ($permitted as $type) {
                if ($type == $file['type']) {
                    $typeOK = true;
                    break;
                }
            }

            // if file type ok upload the file
            if ($typeOK) {
                // switch based on error code
                switch ($file['error']) {
                    case 0:
                        if ($single_file_name != null) {
                            $filename_parts = explode('.', $filename);
                            if (strstr($single_file_name, '.') === false) {
                                $filename = $single_file_name . '.' . $filename_parts[count($filename_parts) - 1];
                            } else {
                                $single_file_name_parts = explode('.', $single_file_name);
                                $filename = $single_file_name_parts[0] . '.' . $filename_parts[count($filename_parts) - 1];
                            }
                            //debug($filename);
                            // create full filename
                            $full_url = $folder_url . '/' . $filename;
                            $url = $rel_url . '/' . $filename;
                            // upload the file
                            $success = move_uploaded_file($file['tmp_name'], $url);
                        } else {
                            // check filename already exists
                            if (!file_exists($folder_url . '/' . $filename)) {
                                // create full filename
                                $full_url = $folder_url . '/' . $filename;
                                $url = $rel_url . '/' . $filename;
                                // upload the file
                                $success = move_uploaded_file($file['tmp_name'], $url);
                            } else {
                                // create unique filename and upload file
                                ini_set('date.timezone', 'Europe/London');
                                $now = date('Y-m-d-His');
                                $full_url = $folder_url . '/' . $now . $filename;
                                $url = $rel_url . '/' . $now . $filename;
                                $success = move_uploaded_file($file['tmp_name'], $url);
                            }
                        }
                        // if upload was successful
                        if ($success) {
                            // save the url of the file
                            $result['urls'][] = $url;
                        } else {
                            $result['errors'][] = "Erro ao salvar imagem. Tente novamente.";
                        }
                        break;
                    case 3:
                        // an error occured
                        $result['errors'][] = "Erro ao salvar imagem. Tente novamente.";
                        break;
                    default:
                        // an error occured
                        $result['errors'][] = "Erro ao salvar imagem. Tente novamente.";
                        break;
                }
            } elseif ($file['error'] == 4) {
                // no file was selected for upload
                $result['nofiles'][] = "No file Selected";
            } else {
                // unacceptable file type
                $result['errors'][] = "Tipo de arquivo inválido. Escolha uma imagem do tipo jpg, gif ou png.";
            }
        }
        return $result;
    }

}

?>
