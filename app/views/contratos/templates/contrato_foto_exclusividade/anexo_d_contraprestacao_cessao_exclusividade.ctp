<?php if($out == "html") : ?>
<?php include('salvar_contrato.ctp')?>
<?php endif; ?>
<?php 
setlocale( LC_ALL, 'pt_BR', 'pt_BR.iso-8859-1', 'pt_BR.utf-8', 'portuguese');
$mes = strftime("%B");
?>
<h4>ANEXO D – CONTRAPRESTAÇÃO DA CESSÃO DE EXCLUSIVIDADE FOTOGRÁFICA</h4>
<p style="font-size: 1.05em; line-height: 1.8em">
<?=str_pad("Faculdade:", 70, "&nbsp;")?><?=$turma['Turma']['nome']?>
<br />
<?=str_pad("Curso:", 108, "&nbsp;")?><?=$turma['Turma']['cursos']?>
<br />
<?=str_pad("Conclusão:", 71, "&nbsp;")?><?=$turma['Turma']['ano_formatura']?>.<?=$turma['Turma']['semestre_formatura']?>
<?php $valorFormando = $turma['Turma']['valor_total_contrato'] / $turma['Turma']['expectativa_formandos'];?>
</p>
Na qualidade de <b>Contratantes</b>: Os formandos do(s) curso(s) de <b><?=$turma['Turma']['cursos']?></b>,
da <b><?=$turma['Turma']['nome']?></b>, com previsão de formatura para o <b><?=($turma['Turma']['semestre_formatura'] == 1) ? 'primeiro' : 'segundo';?> 
semestre de <?=$turma['Turma']['ano_formatura']?></b>, neste ato devidamente representados pela <b>Comissão de Formatura</b> definem:<br/><br/> 
<b>1.</b> Em retribuição à outorga de exclusividade que ora se contrata a <b>CONTRATADA</b> entregará ao <b>CONTRATANTE</b> os 
bens especificados nesse anexo, que está regulamentado e é imutável, de conhecimento das partes.<br /><br />
<b>2.</b> Existe uma previsão de <b><?=$turma['Turma']['expectativa_formandos']?></b> formandos. Para efeito desse contrato, o número 
final será o número de formandos que efetivamente participarem do baile de formatura.<br /><br />
<b>3.</b> Fica definido o valor de <b>R$ <?=number_format($valorFormando, 2, ',' , '.'); ?></b> por formando, totalizando um valor total de <b>R$ <?=number_format($turma['Turma']['valor_total_contrato'], 2, ',' , '.'); ?></b>.<br /><br />
<b>4.</b> O montante será pago da seguinte forma:<br /><br />
<?=nl2br($turma['Turma']['forma_pagamento_anexo_d'])?>
<br /><br />
<b>5.</b> Os ajustes (para mais ou para menos) em relação ao número de formandos esperado e o número de formandos 
realizado serão feitos na última parcela. Caso a comissão tenha recebido mais do que o devido (número final de formandos
for menor que o esperado) e o ajuste necessário não seja possível apenas na última parcela, a comissão deverá restituir
os valores recebidos a mais para a <b>CONTRATADA</b>.<br /><br />
<b>6.</b> Além do valor especificado acima, caso exista algum outro item incluso, evento ou promessa comercial devido à outorga
da exclusividade fotográfica, os mesmos deverão estar descritos abaixo:<br />
<br />
<br />
<?=nl2br($turma['Turma']['itens_adicionais_anexo_d'])?>
<br />
<br />
<br />
<br />
<h5 style="text-align: center; color: black">São Paulo, <?=date('d')?> de <?=$mes?> de <?=date('Y')?></h5>