<style type="text/css">
    .modal-body{
        overflow: hidden;
    }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        var button = $("<button>",{
            type: 'button',
            class: 'button bg-color-greenDark',
            id: 'alterar-comissao',
            dir: '<?=$usuario['FormandoProfile']['usuario_id']?>',
            text:'Alterar'
            });
        $('.modal-footer').prepend(button);
        $('.modal-footer').on('click', '#alterar-comissao',function(){
            if($('#FormandoProfileValorAdesaoComissao').val() == "" || $('#FormandoProfileMaxParcelasComissao').val() == ""){
                alert("Os campos valores de adesão e/ou parcelas estão vazios.");
            }else{
                dir = $(this).attr('dir');
                var context = ko.contextFor($(".metro-button.reload")[0]);
                var dados = $("#form").serialize();
                var url = '<?="/{$this->params['prefix']}/comissoes/editar/"?>'+dir;
                bootbox.hideAll();
                context.$data.showLoading(function() {
                    $.ajax({
                        url: url,
                        data: dados,
                        type: "POST",
                        dataType: "json",
                        complete: function() {
                            context.$data.reload();
                        }
                    });
                });
            }
        });
    
        $(".modal-body").on('click','#btn-adesao', function(){
            $('#btn-adesao').hide();
            $('#spn-adesao').hide();
            $('#FormandoProfileValorAdesaoComissao').fadeIn('fast');
        });

        $(".modal-body").on('click','#btn-parcelas', function(){
            $('#btn-parcelas').hide();
            $('#spn-parcelas').hide();
            $('#FormandoProfileMaxParcelasComissao').fadeIn('fast');
        });
    });

</script>
<div class="row-fluid">
    <?php if($usuario) { ?>    
    <h2 class="fg-color-red"><?=$usuario['Usuario']['nome']?></h2>
    <br />
    <div class="row-fluid">
        <div class="span6">
            <h5>Email</h5>
            <h4><em>
                <?=$usuario['Usuario']['email']?>
            </em></h4>
        </div>
        <div class="span6">
            <h5>RG</h5>
            <h4><em>
                <?=$usuario['FormandoProfile']['rg']?>
            </em></h4>
        </div>
    </div>
    <br />
    <div class="row-fluid">
        <div class="span6">
            <h5>Tel Res</h5>
            <h4><em>
                <?=$usuario['FormandoProfile']['tel_residencial']?>
            </em></h4>
        </div>
        <div class="span6">
            <h5>Tel Com</h5>
            <h4><em>
                <?=$usuario['FormandoProfile']['tel_comercial']?>
            </em></h4>
        </div>
    </div>
    <br />
    <div class="row-fluid">
        <div class="span6">
            <h5>Cargo</h5>
            <h4><em>
                <?=$usuario['FormandoProfile']['cargo_comissao']?>
            </em></h4>
        </div>
    </div>
    <br/>
    <?php
        $session->flash();
        echo $form->create('Comissao', array('url' => "/{$this->params['prefix']}/comissoes/alterar", "id" => "form"));
        echo $form->hidden('FormandoProfile.usuario_id',  array('hiddenField' => true, 'value' => $usuario['FormandoProfile']['usuario_id']));
    ?>
    <div class="row-fluid" id="formulario">
        <div class="row-fluid">
            <h5>Valor da Ades&atilde;o</h5><br/>
            <div class="span6" id="spn-adesao"><h4><em><?php echo $valor_adesao_comissao; ?></em></h4></div>
            <button id="btn-adesao" type="button" class="button mini default bg-color-blueDark" style="float:left;">Editar</button>
            <?php echo $form->text("FormandoProfile.valor_adesao_comissao", 
                array('value' => $usuario['FormandoProfile']['valor_adesao_comissao'],'style' => 'display:none;', 'error' => false, 'id' => 'FormandoProfileValorAdesaoComissao'));?>
        </div>
        <br/>
        <div class="row-fluid">
            <h5>N&uacute;mero m&aacute;ximo de parcelas</h5><br/>
            <div class="span6" id="spn-parcelas"><h4><em><?php echo $max_parcelas_comissao; ?></em></h4></div>
            <button id="btn-parcelas" type="button" class="button mini default bg-color-blueDark" style="float:left;">Editar</button>
            <?php echo $form->text("FormandoProfile.max_parcelas_comissao", 
                array('value' => $usuario['FormandoProfile']['max_parcelas_comissao'], 'style' => 'display:none;', 'error' => false, 'id' => 'FormandoProfileMaxParcelasComissao'));?>
        </div>
    </div>
    <?php }else{ ?>
    <h2 class="fg-color-red">Usuário Não Encontrado</h2>
    <?php } ?>
</div>