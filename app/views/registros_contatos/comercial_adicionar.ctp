

<span id="conteudo-titulo" class="box-com-titulo-header">Adicionar Registro de Contato</span>
<div id="conteudo-container">
	<?php echo $session->flash() ;?>
	<?php echo $form->create('RegistrosContato', array('url' => "/{$this->params['prefix']}/RegistrosContatos/adicionar")); ?>
		<?php include('_form.ctp'); ?>
	
	<p class="grid_6 alpha omega">
		<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'controller' => 'registros_internos', 'action' => 'index') ,array('class' => 'cancel')); ?>
		<?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit'));?>
	</p>
</div>
