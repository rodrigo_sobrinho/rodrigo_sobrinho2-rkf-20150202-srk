<span id="conteudo-titulo" class="box-com-titulo-header">Universidades</span>
<div id="conteudo-container">
	<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
	<?php $session->flash(); ?>
	<div class="tabela-adicionar-item">
		<?php echo $form->create('Universidades', array('url' => "/{$this->params['prefix']}/universidades/procurar",'class' => 'procurar-form-inline')) ?>
		<?php echo $form->input('chave', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
		<?php echo $form->end(array('label' => ' ', 'div' => false, 'class' => 'submit-busca')) ?>
		<?php echo $html->link('Adicionar',array($this->params['prefix'] => true, 'action' => 'adicionar')); ?>
		<div style="clear:both;"></div>
	</div>
	<div class="container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col"><?php echo $paginator->sort('Id', 'id'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Nome', 'nome'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Sigla', 'sigla'); ?></th>
					<th scope="col"> &nbsp;</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="3"><?php echo $paginator->counter(array('format' => 'Itens %start% ao %end% - página %page% de %pages%')); ?>
						<span class="paginacao">
							<?php echo $paginator->numbers(array('separator' => ' ')); ?>
						</span>
					</td>
					<td colspan="1"><?php echo $paginator->counter(array('format' => 'Total : %count% ' .  $this->name)); ?></td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach ($universidades as $universidade): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<td  colspan="1" width="10%"><?php echo $universidade['Universidade']['id']; ?></td>
					<td  colspan="1" width="30%"><?php echo $universidade['Universidade']['nome']; ?></td>
					<td  colspan="1" width="30%"><?php echo $universidade['Universidade']['sigla']; ?></td>
					<td  colspan="1" width="30%">
						<?php echo $html->link('Visualizar', array($this->params['prefix'] => true, 'controller' => 'Universidades', 'action' =>'visualizar', $universidade['Universidade']['id']), array('class' => 'submit button')); ?>
						<?php echo $html->link('Editar', array($this->params['prefix'] => true, 'controller' => 'Universidades', 'action' => 'editar', $universidade['Universidade']['id']), array('class' => 'submit button')) ?>
						<?php echo $html->link('Deletar', array($this->params['prefix'] => true, 'controller' => 'Universidades', 'action' => 'deletar', $universidade['Universidade']['id']), 
							array('class' => 'submit button', 'onclick' => "javacript: if(confirm('Deseja excluir o item {$universidade['Universidade']['nome']}?')) return true; else return false;")); ?>
					</td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>