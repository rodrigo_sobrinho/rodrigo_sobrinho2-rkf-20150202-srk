<?php $session->flash(); ?>
<?php if(isset($resposta)) : if($resposta) : ?>
<div class="alert alert-success">
    <h4>
        Muito obrigado pela atenção
        <br />
        O problema foi encaminhado para o setor responsavel e assim que for resolvido entraremos em contato
    </h4>
</div>
<?php else : ?>
<div class="alert alert-error">
    <h4>
        Houve um erro ao cadastrar a solicitação
        <br />
        Por favor entre em contato pelo endereço de email abaixo
        <br />
        <a href="mailto:atendimento@eventos.as" class="fg-color-white" target="_blank">atendimento@eventos.as</a>
    </h4>
</div>
<?php endif; endif; ?>
<a href="/<?= $this->params['prefix']?>/" class='button default'>
    Acessar Sistema
</a>