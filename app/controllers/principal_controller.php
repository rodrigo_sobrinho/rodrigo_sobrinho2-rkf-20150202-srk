<?php

class PrincipalController extends AppController {

    var $name = 'Principal';
    var $uses = array('Curso', 'CursoTurma', 'Universidade','Enquete');
    var $helpers = array('ListaAgenda');
    var $nomeDoTemplateSidebar = 'principal';

    function beforeFilter() {
        parent::beforeFilter();
        if ($this->Session->read('Usuario'))
            $this->Auth->allow('index');
        else
            $this->redirect("/usuarios/login");
    }

    function index() {
        $usuarioLogado = $this->Session->read('Usuario');
        $this->Session->delete('exibirSwitchComissaoFormando');
        $this->redirecionarUsuarioParaHome($usuarioLogado['Usuario']['grupo']);
    }
    
    function abrir_url($url = false) {
        $usuario = $this->obterUsuarioLogado();
        if(!$usuario) {
            $this->redirect("/");
        } else {
            if($url)
                $this->Session->write('redirecionar',base64_decode($url));
            $this->redirect("/{$usuario["Usuario"]["grupo"]}");
        }
    }

    function redirecionarUsuarioParaHome($grupoDoUsuario) {
        $this->redirect("/{$grupoDoUsuario}");
    }

    // Método para verificar a qual turma o usuário pertence, caso seja comissão, e colocar a turma na session
    private function configurarTurmaComissao() {
        if (!$this->Session->check('turma')) {
            $usuario = $this->Session->read('Usuario');

            $this->Turma->recursive = 2;
            $this->Turma->contain(array(
                'CursoTurma.Curso.Faculdade.Universidade',
                'TurmasUsuario',
                'Usuario' => array('conditions' => array('grupo' => array('comercial', 'planejamento')))
            ));
            $this->Turma->bindModel(array('hasOne' => array('TurmasUsuario')), false);
            $turma = $this->Turma->find('first', array('conditions' => array('TurmasUsuario.usuario_id' => $usuario['Usuario']['id'])));

            $this->Session->write('turma', $turma);
        }
    }

    function financeiro_index() {
        $this->layout = 'metro/default';
    }

    function video_index() {
        $this->layout = 'metro/default';
    }

    function criacao_index() {
        $this->layout = 'metro/default';
    }

    function foto_index() {
        $this->layout = 'metro/default';
    }

    function producao_index() {
        $this->layout = 'metro/default';
    }

    function rh_index() {
        $this->layout = 'metro/default';
    }


    function formando_index() {
        $this->loadModel('Protocolo');
        $this->configurarTurmaComissao();
        $this->verificarOpacaoMapaEvento();
        $turma = $this->Session->read('turma');
        $usuario = $this->obterUsuarioLogado();
        $this->Protocolo->bindModel(array('hasOne' => array('ProtocoloCancelamento')), false);
        $protocolo = $this->Protocolo->find('first', array(
            'conditions' => array(
                'usuario_id' => $usuario['Usuario']['id'],
            ),
        ));
        $this->set('protocolo', $protocolo);
        $this->set('enquete', $this->obterEnquete());
        $this->set('turma', $turma);
        $this->layout = 'metro/default';
    }

    function comissao_index() {
        $this->layout = 'metro/default';
        $this->configurarTurmaComissao();
        $this->verificarOpacaoMapaEvento();
        $usuario = $this->Session->read('Usuario');
        $turma = $this->Session->read('turma');
        $this->Usuario->bindModel(array('hasOne' => array('TurmasUsuario')));
        $this->set('pretensaos', $this->Turma->pretensao);
        $this->set('enquete', $this->obterEnquete());
        $this->set('turma', $turma);
        $this->set('usuario', $usuario);

        $this->loadModel('RegistroContatoUsuario');

        $festas = $this->RegistroContatoUsuario->find('all', array(
            'conditions' => array(
                'RegistroContatoUsuario.usuario_id' => $usuario['Usuario']['id'],
                "RegistroContatoUsuario.data_confirmacao is null",
                'RegistrosContato.tipo' => 'festa',
                "RegistrosContato.usuario_id <> '{$usuario['Usuario']['id']}'"
            ),
        ));
        $this->set('festas', $festas);
    }

    function marketing_index() {
        $this->layout = 'metro/default';
    }

    function super_index() {
        $this->layout = 'metro/default';
    }

    function comercial_index() {
        $this->layout = 'metro/default';
    }

    function vendedor_index() {
        $this->redirect("/{$this->params['prefix']}/pagmidas/transacoes");
    }

    function planejamento_index() {
        $this->layout = 'metro/default';
        if($this->turmaEstaLogada())
            $this->verificarFestaTurma();
    }

    function atendimento_index() {
        $this->layout = 'metro/default';
    }
    
    function obterEnquete() {
        $enquetes = $this->Enquete->obterEnquetePorUsuario($this->obterUsuarioLogado(),$this->obterTurmaLogada());
        if($enquetes)
            return json_encode($enquetes[0], JSON_UNESCAPED_UNICODE);
        else
            return false;
    }
    
    function verificarFestaTurma() {
        $turma = $this->obterTurmaLogada();
        $this->loadModel('Evento');
        $this->Evento->unbindModel(array(
            'belongsTo' => array('Turma'),
            'hasMany' => array('Extra','EventoMapa'),
        ),false);
        $festas = $this->Evento->find('count',array(
            'conditions' => array(
                'Evento.turma_id' => $turma['Turma']['id'],
                'TiposEvento.template' => 'festa'
            )
        ));
        $this->set('festas',$festas);
    }
    
    private function verificarOpacaoMapaEvento() {
        $temMapa = false;
        
        //verifica se o formando tem checkout
        if($this->formandoFezCheckout()) {
            
            $turma = $this->obterTurmaLogada();
            $this->loadModel('Evento');

            $this->Evento->unbindModel(array(
                'belongsTo' => array('Turma'),
                'hasMany' => array('Extra'),
            ),false);
            $eventos = $this->Evento->find('all',array(
                'conditions' => array(
                    'Evento.turma_id' => $turma['Turma']['id'],
                )
            ));
            

            if($eventos) {
                foreach($eventos as $evento)
                    if(!empty($evento['TurmaMapa'])) {
                        $temMapa = true;
                        break;
                    }
            }
            if($temMapa)
                $temMapa = $this->verificarFormandoEscolhaDeMesa();
        }
        $this->set('temMapa',$temMapa);
    }
    
    private function verificarFormandoEscolhaDeMesa() {
        $temMesa = false;
        $usuario = $this->Session->read('Usuario');
        if($usuario['ViewFormandos']['mesas_contrato'] > 0) {
            $temMesa = true;
        } else {
            $this->Protocolo->recursive = 2;
            $protocolo = $this->Protocolo->find('first',array(
                'conditions' => array(
                    'usuario_id' => $usuario['Usuario']['id'],
                    'tipo' => 'checkout'
                )
            ));
            if($protocolo) {
                foreach($protocolo['CheckoutUsuarioItem'] as $checkoutItem) {
                    if(($checkoutItem['quantidade']*$checkoutItem['CheckoutItem']['quantidade_mesas']) > 0) {
                        $temMesa = true;
                        break;
                    }
                }
            }
            if(!$temMesa) {
                $this->loadModel('CampanhasUsuarioCampanhasExtra');
                $this->loadModel('CampanhasExtra');
                $this->loadModel('CampanhasUsuario');
                $this->CampanhasUsuarioCampanhasExtra->unbindModel(array(
                    'belongsTo' => array('CampanhasUsuario')
                ),false);
                $this->CampanhasExtra->unbindModel(array(
                    'belongsTo' => array('Campanha'),
                    'hasAndBelongsToMany' => array('CampanhasUsuarioCampanhaExtra')
                ),false);
                $this->CampanhasUsuario->bindModel(array(
                    'hasMany' => array(
                        'CampanhasUsuarioCampanhasExtra' => array(
                            'className' => 'CampanhasUsuarioCampanhasExtra',
                            'joinTable' => 'campanhas_usuarios_campanhas_extras',
                            'foreignKey' => 'campanhas_usuario_id'
                        )
                    )
                ),false);
                $this->CampanhasUsuario->recursive = 3;
                $campanhas = $this->CampanhasUsuario->find('all',array(
                    'conditions' => array(
                        'usuario_id' => $usuario['Usuario']['id'],
                        'cancelada' => 0
                    )
                ));
                foreach($campanhas as $campanha) {
                    foreach($campanha['CampanhasUsuarioCampanhasExtra'] as $extra) {
                        if(($extra['CampanhasExtra']['quantidade_mesas']*$extra['quantidade']) > 0) {
                            $temMesa = true;
                            break;
                        }
                    }
                }
            }
        }
        return $temMesa;
    }
    
    private function formandoFezCheckout() {
        $this->loadModel('Protocolo');
        $usuario = $this->Session->read('Usuario');
        $protocolo = $this->Protocolo->find('first',array(
            'conditions' => array(
                'usuario_id' => $usuario['Usuario']['id'],
                'tipo' => 'checkout'
            )
        ));
        return !empty($protocolo);
    }

}

?>
