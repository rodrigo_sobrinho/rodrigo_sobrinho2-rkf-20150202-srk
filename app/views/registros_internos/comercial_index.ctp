<?php
/*
Array ( [id] => 15 [nome] => Turma do Amendoim [ano_formatura] => 2007 [semestre_formatura] => 1 [expectativa_formandos] => 2 [expectativa_fechamento] => alta [memorando] => test [fundo_caixa_usado] => 21.00 [igpm] => 12 [status] => aberta [data_assinatura_contrato] => 2011-08-08 [como_chegou] => internet [como_chegou_detalhes] => [data_abertura_turma] => 2011-08-08 [pretensao] => festa [analise_as] => aa [beneficios_comissao] => sd )
*/

?>

<span id="conteudo-titulo" class="box-com-titulo-header">Registro Interno</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
    <?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
	<div class="detalhes">
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Memorando do Consultor</label>
			<span class="grid_8 alpha first"><?php echo $turma['Turma']['memorando']; ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Expectativa de Fechamento</label>
			<span class="grid_8 alpha first"><?php echo $turma['Turma']['expectativa_fechamento']; ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Concorrentes</label>
			<span class="grid_8 alpha first"><?php echo $turma['Turma']['concorrentes']; ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Data Prevista de Fechamento</label>
			<span class="grid_8 alpha first"><?php echo $turma['Turma']['data_prevista_fechamento']; ?></span>
		</p>
		
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Número de Ligações</label>
			<span class="grid_8 alpha first"><?php echo $ligacoes; ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Número de Reuniões</label>
			<span class="grid_8 alpha first"><?php echo $reunioes; ?></span>
		</p>
		<p class="grid_6 alpha omega">
			<?php echo $html->link('Editar',array($this->params['prefix'] => true, 'controller' => 'turmas' , 'action' => 'editar_registro_interno') ,array('class' => 'submit')); ?>

		</p>
	</div>
	
	<div style="clear:both;"></div>
	<p class='titulo-secao'>Registro de contatos</p>
	<div class="tabela-adicionar-item">
		<?php echo $html->link('Adicionar',array($this->params['prefix'] => true, 'controller' => 'RegistrosContatos', 'action' => 'adicionar')); ?>
		<div style="clear:both;"></div>
	</div>
	<div class="container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col"><?php echo $paginator->sort('Id', 'id'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Usuário', 'usuario_id'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Data', 'data'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Tipo', 'tipo'); ?></th>
					<th scope="col"><?php echo 'Assunto'; ?></th>
					<th scope="col"> &nbsp;</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="5"><?php echo $paginator->counter(array('format' => 'Itens %start% ao %end% - página %page% de %pages%')); ?>
						<span class="paginacao">
							<?php echo $paginator->numbers(array('separator' => ' ')); ?>
						</span>
					</td>
					<td colspan="1"><?php echo $paginator->counter(array('format' => 'Total : %count% ' .  'Registro(s) Interno(s)')); ?></td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach ($registros as $registro): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<td  colspan="1" width="5%"><?php echo $registro['RegistrosContato']['id']; ?></td>
					<td  colspan="1" width="20%"><?php echo $registro['Usuario']['nome']; ?></td>
					<td  colspan="1" width="5%">
					<?php 
			        $dateTime =  $datas->create_date_time_from_format('Y-m-d H:i:s', $registro['RegistrosContato']['data']);
			        $date = date_format($dateTime, 'd/m/Y');
			        $time = date_format($dateTime, 'H:i');
			        
					echo $date; ?></td>
					<td  colspan="1" width="15%"><?php echo ucfirst(utf8_encode($registro['RegistrosContato']['tipo'])); ?></td>
					<td  colspan="1" width="35%"><?php echo ucfirst($registro['RegistrosContato']['assunto']); ?></td>										
					<td  colspan="1" width="20%">
						<?php echo $html->link('Ver', array($this->params['prefix'] => true, 'controller' => 'RegistrosContatos', 'action' => 'visualizar', $registro['RegistrosContato']['id']), array('class' => 'submit button')) ?>
						<?php echo $html->link('Editar', array($this->params['prefix'] => true, 'controller' => 'RegistrosContatos', 'action' => 'editar', $registro['RegistrosContato']['id']), array('class' => 'submit button')) ?>
						<?php echo $html->link('Deletar', array($this->params['prefix'] => true, 'controller' => 'RegistrosContatos', 'action' => 'deletar', $registro['RegistrosContato']['id']), 
							array('class' => 'submit button', 'onclick' => "javacript: if(confirm('Deseja excluir o registro {$registro['RegistrosContato']['id']}?')) return true; else return false;")); ?>
					</td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>

</div>
