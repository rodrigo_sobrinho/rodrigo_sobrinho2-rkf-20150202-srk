<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">

<?php if (count($turma_mapas) > 0) : ?>
    <br/>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th class="input-medium" scope="col">Evento</th>
                <th class="input-medium" scope="col">Mapa</th>
                <th class="input-small" scope="col">Qtd Mesas</th>
                <th scope="col">Setores</th>
                <th class="input-small" scope="col"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($turma_mapas as $turma_mapa) : ?>
            	<?php $setores = (!empty($turma_mapa['TurmaMapa']['mapa_setores']) ? implode(', ',json_decode($turma_mapa['TurmaMapa']['mapa_setores'])) : ''); ?>
	            <tr>
	                <td><?=$turma_mapa['Evento']['nome']; ?></td>
	                <td><?=$turma_mapa['Mapa']['titulo']; ?></td>
	                <td><?=$turma_mapa['TurmaMapa']['quantidade_mesas']; ?></td>
	                <td><?=$setores; ?></td>
	                <td>
	                	<button type="button" class="replicar-mapa default mini bg-color-red" turma-mapa-id="<?= $turma_mapa['TurmaMapa']['id'] ?>">
	                        Replicar
	                    </button>
	                </td>
	            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
	<h3 class="fg-color-red">Nenhum Mapa Encontrado.</h3>
<?php endif; ?>

<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/alert.js?v=0.1"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/tooltip.js?v=0.1"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/modal.js?v=0.1"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/popover.js?v=0.1"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/tab.js?v=0.1"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/bootbox.js?v=0.2"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".replicar-mapa").click(function(){
			var turma_mapa_id = $(this).attr('turma-mapa-id');
			if(confirm('Tem certeza que deseja replicar esse mapa?')) {
				$.ajax({
					url: '/<?=$this->params['prefix']?>/mapa_mesas/replicar_mapa/'+$("#evento_id").val(),
					data: {
						data: {
							turma_mapa_id: turma_mapa_id
						}
					},
					type: 'POST',
					complete: function(data){
						bootbox.hideAll();
					}
				});
			}
		});
	});
</script>