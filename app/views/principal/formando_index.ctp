<div class="span12 metro" style="position:relative" id="content-container">
    <div class="row-fluid content-loader hide" id="content-loading">
        <div class="cycle-loader"></div>
    </div>
    <div class="row-fluid content-loader hide" id="content-body"></div>
    <div class="row-fluid content-loader" id="content-home">
        <div class="tile icon app bg-color-greenDark"
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/usuarios/editar_dados') }">
            <div class="tile-content">
                <span class="img icon-user-4"></span>
            </div>
            <div class="brand"><span class="name">Meus Dados</span></div>
        </div>
        <div class="tile icon double app bg-color-blueDark"
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/usuarios/editar_dados/redes-sociais') }">
            <div class="tile-content">
                <span class="img icon-facebook-2"></span>
            </div>
            <div class="brand">
                <span class="name">
                    Vincular Facebook
                    <span class="badge badge-success">
                        Novo!
                    </span>
                </span>
            </div>
        </div>
        <div class="tile icon bg-color-darken"
             data-bind="click: function(data, event) {
             page('/mensagens/email') }">
            <div class="tile-content">
                <span class="img icon-phone"></span>
            </div>
            <div class="brand"><span class="name">Entre em contato conosco</span></div>
        </div>
        <div class="tile icon bg-color-orange"
             data-bind="click: function(data, event) {
             page('/mensagens/informativos') }">
            <div class="tile-content">
                <span class="img icon-broadcast"></span>
            </div>
            <div class="brand"><span class="name">Informativos</span></div>
        </div>
    
        <?php //if($temMapa) : ?>
        <a href="formando/eventos/selecionar_evento" target="_blank" >
            <div class="tile icon bg-color-purple">
                <div class="tile-content">
                    <span class="img icon-map"></span>
                </div>
                <div class="brand"><span class="name">Mapa de Mesas</span></div>
            </div>
        </a>
        <?php //endif; ?>
        <div class="tile icon bg-color-red "
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/campanhas/listar') }">
            <div class="tile-content">
                <span class="img icon-cart"></span>
            </div>
            <div class="brand"><span class="name">Loja</span></div>
        </div>
        <div class="tile icon bg-color-grayDark"
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/area_financeira/dados') }">
            <div class="tile-content">
                <span class="img icon-calculate"></span>
            </div>
            <div class="brand"><span class="name">&Aacute;rea Financeira</span></div>
        </div>
        <div class="tile icon bg-color-yellow "
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/eventos/listar') }">
            <div class="tile-content">
                <span class="img icon-music"></span>
            </div>
            <div class="brand"><span class="name">Eventos</span></div>
        </div>
        <div class="tile icon bg-color-greenDark"
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/rifas/cupons') }">
            <div class="tile-content">
                <span class="img icon-puzzle"></span>
            </div>
            <div class="brand"><span class="name">Rifa Online</span></div>
        </div>
        <div class="tile icon bg-color-blue"
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/formandos/fotos_telao') }">
            <div class="tile-content">
                <span class="img icon-picture-2"></span>
            </div>
            <div class="brand"><span class="name">Fotos Tel&atilde;o</span></div>
        </div>
        <?php if($protocolo['ProtocoloCancelamento']['valor_cancelamento'] > 0 &&
                $protocolo['ProtocoloCancelamento']['ordem_pagamento'] == 1 &&
                $protocolo['Protocolo']['status'] == 'pendente'){ ?>
        <div class="tile icon bg-color-orangeDark"
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/area_financeira/dados_bancarios/') }">
            <div class="tile-content">
                <span class="img icon-arrow-down"></span>
            </div>
            <div class="brand"><span class="name">Devolução Pagamentos</span></div>
        </div>
        <?php } ?>
        <div class="tile icon bg-color-green"
             data-bind="click: function(data, event) {
             page('/enquetes/nao_respondidas') }">
            <div class="tile-content">
                <div class="img icon-newspaper"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Enquetes</span>
            </div>
        </div>
    </div>
</div>
