function Checkout() {
	
	this.adesao = {
		saldo : 0,
		debitar : function(debito) {
			this.saldo += debito;
		},
		atualizar : function() {
			if(this.saldo == 0)
				$('.saldo-adesao').html('-');
			else if(this.saldo < 0)
				$('.saldo-adesao').html('R$' + number_format(this.saldo*-1,2,',','.'));
			else
				$('.saldo-adesao').html('R$' + number_format(this.saldo,2,',','.'));
			checkout.total.atualizar();
		},
		iniciar : function(debito) {
			if(debito == 0)
				this.ativo = false;
			else
				this.ativo = true;
			this.debitar(debito);
			this.atualizar();
		}
	};
	
	this.campanhas = {
		saldo : 0,
		debitar : function(debito) {
			if(debito == 0)
				this.ativo = false;
			else
				this.ativo = true;
			this.saldo += debito;
		},
		atualizar : function() {
			if(this.saldo == 0)
				$('.saldo-campanhas').html('-');
			else if(this.saldo < 0)
				$('.saldo-campanhas').html('<p style="color:red">- R$' + number_format(this.saldo*-1,2,',','.')+'</p>');
			else
				$('.saldo-campanhas').html('<p style="color:green">+ R$' + number_format(this.saldo,2,',','.')+'</p>');
			checkout.total.atualizar();
		},
		iniciar : function(debito) {
			if(debito == 0)
				this.ativo = false;
			else
				this.ativo = true;
			this.debitar(debito);
			this.atualizar();
		}
	};
	
	this.extras = {
		itens : [],
		inserir : function(item) {
			this.itens.push(item);
		},
		comprar : function(quantidade,id) {
			$.each(this.itens,function(i,item) {
				if(item.id == id) {
					if(quantidade == 0) {
						item.ativo = false;
						item.saldo = 0;
					} else {
						item.ativo = true;
						item.saldo = item.valor*quantidade*-1;
					}
					item.quantidade = quantidade;
					item.comprados = quantidade;
					checkout.extras.atualizar(item);
					return true;
				}
			});
		},
		iniciar : function(itens) {
			$.each(itens,function(i,item) {
				checkout.extras.inserir(item);
			});
		},
		atualizar : function(item) {
			if(item.saldo == 0)
				$('.saldo-item-'+item.id).html('-');
			else
				$('.saldo-item-'+item.id).html('R$' + number_format(item.saldo*-1,2,',','.'));
			if(item.quantidade != undefined)
				$('.qtde-item-'+item.id).html(item.quantidade);
			else
				$('.qtde-item-'+item.id).html('0');
			checkout.total.atualizar();
		},
		obterSaldoTotal : function() {
			var saldoItens = 0;
			$.each(this.itens,function(i,item) {
				if(item.saldo != undefined)
					saldoItens+= item.saldo;
			});
			return saldoItens;
		}
	};
	
	this.pagamentos = {
		parcelas : [],
		remover : function(id) {
			$.each(this.parcelas,function(i,parcela) {
				if(parcela.id == id) {
					checkout.pagamentos.parcelas.splice(i,1);
					return true;
				}
			});
		},
		adicionar : function(parcela) {
			checkout.pagamentos.parcelas.push(parcela);
			checkout.pagamentos.criar(parcela);
		},
		iniciar : function() {
			var html = "<div style='width:100%' id='box-pagamentos'>";
			html+= "<div class='message' style='display:none'></div>";
			html+= "<p>A soma dos pagamentos deve ser igual ao saldo total.<br /><br />";
			html+= "<span class='pendencias' dir='adesao'>Saldo de ades&atilde;o: " + $(".saldo-adesao").html() + "</span><br />";
			html+= "<span class='pendencias' dir='campanhas'>Saldo de campanhas: " + $(".saldo-campanhas").html() + "</span><br />";
			$.each(checkout.extras.itens, function(i,item) {
				if(item.ativo) {
					html+= "<span class='pendencias' dir='item-" + item.id + "'>Saldo de " + item.titulo + ": ";
					html+= $(".saldo-item-" + item.id).html() + "</span><br />";
				}
			});
			html+= "<br />Saldo Total: " + $(".saldo-total").html() + "<br />";
			html+= "Soma das parcelas: <span id='soma-parcelamentos'></span><br />";
			html+= "<span>Saldo Restante</span>: <span id='saldo-restante'></span><br /><br />";
			html+= "<table><thead>";
			html+= "<tr><th width='10%'>Valor</th><th width='15%'>Data de Vencimento</th><th width='10%'>Forma de pagamento</th>";
			html+= "<th width='20%'>Itens</th><th width='20%'>Observa&ccedil;&otilde;es</th><th>&nbsp;</th></tr></thead><tfoot><tr>";
			html+= "<td colspan='2' class='item'><span class='adicionar pagamentos'>Adicionar</span></td>";
			html+= "<td><span id='finalizar-pagamentos' class='submit button'>Finalizar</span></td>";
			html+= "<td colspan='2'><span onclick='$.colorbox.close()' class='submit button'>Cancelar</span></td>";
			html+= "</tr></tfoot><tbody></tbody></table>";
			html+= "</div>";
			$.colorbox({
				width : "90%",
				height: 600,
				html : html,
				onComplete : function() { checkout.pagamentos.montar(); checkout.pagamentos.atualizarSoma(); },
				onClosed : function() { checkout.total.atualizar() }
			});
		},
		montar : function() {
			$.each(checkout.pagamentos.parcelas,function(i,parcela) {
				checkout.pagamentos.criar(parcela);
			});
		},
		criar : function(parcela) {
			var html = "<tr id='parcela-" + parcela.id + "'>";
			html+= "<td class='td-valor'><input size='6' class='parcela-valor' value='" + number_format(parcela.valor,2,',','.') + "' /></td>";
			html+= "<td class='td-data'><input size='12' value='" + parcela.data_vencimento + "' /></td>";
			html+= "<td class='td-forma'><select>";
			html+= "<option value='cheque'" + (parcela.forma_pagamento == "" || parcela.forma_pagamento == "cheque" ? " selected='selected'>" : ">");
			html+= "Cheque</option>";
			html+= "<option value='comprovante'" + (parcela.forma_pagamento == "comprovante" ? " selected='selected'" : "") + ">Comprovante</option>";
			html+= "<option value='dinheiro'" + (parcela.forma_pagamento == "dinheiro" ? " selected='selected'" : "") + ">Dinheiro</option>";
			html+= "</select></td>";
			html+= "<td class='td-itens'>";
			$.each(checkout,function(i,obj) {
				if(obj.ativo != undefined) {
					var strItem = obj.tipo.replace(/(\ )+(-)/g,"_").toLowerCase();
					if(obj.ativo) {
						var classes = "selecionavel parcela";
						if(parcela.itens.length > 0)
							if($.inArray(strItem,parcela.itens) >= 0)
								classes = "selecionavel parcela selecionado";
						html+= "<span class='" + classes + "' dir='" + strItem + "'>" + ucfirst(obj.tipo) + "</span>";
					}
				}
			});
			$.each(checkout.extras.itens,function(i,item) {
				var strItem = item.titulo.replace(/(\ )|(-)/g,"_").toLowerCase();
				//var strItem = item.id;
				if(item.ativo) {
					var classes = "selecionavel parcela";
					if(parcela.itens.length > 0)
						if($.inArray(strItem,parcela.itens) >= 0)
							classes = "selecionavel parcela selecionado";
					html+= "<span class='" + classes + "' dir='" + strItem + "'>" + ucfirst(item.titulo) + "</span>";
				}
			});
			html+= "</td><td class='td-obs'><textarea>" + parcela.obs + "</textarea></td>";
			html+= "<td class='item'><span class='remover pagamentos'>Remover</span></td>";
			html+= "</tr>";
			$("#box-pagamentos table tbody").append(html);
		},
		obterSomaParcelas : function() {
			var valorTotal = 0;
			$('.parcela-valor').each(function() {
				var valor = stringToFloat(stringMoneyToNumber($(this).val()),2);
				if(!isNaN(valor))
					valorTotal += valor;
			});
			return stringToFloat(valorTotal,2);
		},
		atualizarSoma : function() {
			var valorTotal = checkout.pagamentos.obterSomaParcelas();
			var saldo = stringToFloat(stringMoneyToNumber($(".saldo-total").html()),2)
			if(valorTotal > 0)
				$("#soma-parcelamentos").html("R$" + number_format(valorTotal,2,',','.'));
			else
				$("#soma-parcelamentos").html("-");
		},
		atualizarSaldoRestante : function() {
			var valorTotal = checkout.pagamentos.obterSomaParcelas();
			var saldo = stringToFloat(stringMoneyToNumber($(".saldo-total").html()),2)
			var saldoRestante = valorTotal-saldo;
			if(saldoRestante > 0)
				$("#saldo-restante").prev().css("color","green").next().css("color","green");
			else if(saldoRestante < 0)
				$("#saldo-restante").prev().css("color","red").next().css("color","red");
			else
				$("#saldo-restante").prev().css("color","#575756").next().css("color","#575756");
			$("#saldo-restante").html("R$" + number_format(
					(saldoRestante < 0 ? saldoRestante*-1 : saldoRestante),2,',','.'));
		},
		validarSoma : function() {
			var valorTotal = checkout.pagamentos.obterSomaParcelas();
			var saldo = stringToFloat(stringMoneyToNumber($(".saldo-total").html()),2);
			var retorno = false;
			if(valorTotal < saldo || valorTotal > saldo)
				message.exibir('Soma das parcelas deve ser igual ao saldo total','error');
			else
				retorno = true;
			return retorno;
		},
		validarCampos : function() {
			var erros = "";
			var parcelas = [];
			var retorno = false;
			$("#box-pagamentos table tbody tr").each(function(i,tr) {
				var valor = $(this).children('.td-valor').children().val();
				var data = $(this).children('.td-data').children().val();
				var forma_pagamento = $(this).children('.td-forma').children().val();
				var obs = $(this).children('.td-obs').children().val();
				var itens = [];
				if(valor == "")
					erros+= "Digite o valor da " + (i+1) + "&ordf; parcela.<br />";
				if(data == "")
					erros+= "Digite a data de vencimento da " + (i+1) + "&ordf; parcela.<br />";
				$(this).children('.td-itens').children('.selecionado').each(function(i,item) {
					itens.push($(this).attr('dir'));
				});
				if(itens.length < 1)
					erros+= "Selecione um dos itens na " + (i+1) + "&ordf; parcela.<br />";
				parcelas.push({id:i,valor:stringToFloat(stringMoneyToNumber(valor),2),data_vencimento:data,forma_pagamento:forma_pagamento,itens:itens,obs:obs});
			});
			if(erros == "") {
				retorno = true;
				checkout.pagamentos.parcelas = parcelas;
			} else {
				message.exibir(erros.substring(0,erros.length - 6),'error');
			}
			return retorno;
		},
		finalizar : function() {
			if(checkout.pagamentos.validarCampos())
				if(checkout.pagamentos.validarSoma())
					$.colorbox.close();
			checkout.total.atualizar();
		}
	};
	
	this.total = {
		saldo : 0,
		atualizar : function() {
			var saldoItens = 0;
			$.each(checkout.extras.itens, function(i,item) {
				if(item.ativo)
					saldoItens+= item.saldo;
			});
			var adesao = !checkout.adesao.ativo ? 0 : checkout.adesao.saldo;
			var campanhas = !checkout.campanhas.ativo ? 0 : checkout.campanhas.saldo;
			this.saldo = adesao + campanhas + saldoItens;
			if(this.saldo == 0) {
				$('.saldo-total').html('-');
				$('.saldo-total-label').css('color','orange');
				$('.saldo-total-label').html('Saldo Quitado');
			} else if(this.saldo > 0) {
				$('.saldo-total').html('R$' + number_format(this.saldo,2,',','.'));
				$('.saldo-total-label').css('color','green');
				$('.saldo-total-label').html('Saldo Positivo');
			} else {
				$('.saldo-total').html('R$' + number_format(this.saldo*-1,2,',','.'));
				$('.saldo-total-label').css('color','red');
				$('.saldo-total-label').html('Saldo Negativo');
			}
			checkout.pendencias.finalizar();
		}
	};
	
	this.pendencias = {
		finalizar : function() {
			var html = "";
			var totalPago = 0;
			$.each(checkout.pagamentos.parcelas,function(i,parcela) {
				html+= "<tr>";
				html+= "<td>R$" + number_format(parcela.valor,2,',','.') + "</td>";
				html+= "<td>" + parcela.data_vencimento + "</td>";
				html+= "<td>" + parcela.forma_pagamento + "</td>";
				html+= "<td>Referente &agrave; ";
				$.each(parcela.itens,function(a,item) {
					//html+= ucfirst(item.replace("_"," ")) + (parcela.itens.length - parseInt(a) == 2 ? " e " : ", ");
					$.each(checkout.extras.itens,function(i,extra) {
						if(extra.id == item)
							item = extra.titulo;
					});
					html+= ucfirst(item) + (parcela.itens.length - parseInt(a) == 2 ? " e " : ", ");
				});
				html = html.substring(0,html.length-2) + "</td>";
				html+= "<td>" + parcela.obs + "</td>";
				html+= "</tr>";
				totalPago+= parcela.valor;
			});
			html+= "<tr><td colspan='5' style='padding-top:15px'>";
			html+= "<span class='validar' onclick='checkout.pagamentos.iniciar();'>Alterar Forma de Pagamento</span>";
			html+= "</td></tr>";
			html+= "<tr><td colspan='5' style='padding-top:15px'>";
			html+= "<span class='submit button' id='confirmar-pagamentos'>Confirmar Pagamentos</span>";
			html+= "</td></tr>";
			var saldo = stringToFloat(stringMoneyToNumber($(".saldo-total").html()),2);
			saldo = isNaN(saldo) ? 0 : saldo;
			if(stringToFloat(totalPago,2) != saldo) {
				$('#finalizar-checkout').hide();
			}
			$("#tabela-pagamentos tbody").html(html);
		}
	};
	
	this.init = function(dados) {
		$.each(dados,function(item,dados) {
			obj = checkout.get(item);
			obj.tipo = item;
			if(obj.iniciar != undefined)
				obj.iniciar(dados);
		});
		this.total.atualizar();
		delete this.init;
	}
	
	this.get = function(tipo) {
		return eval('this.'+tipo);
	}
	
}

function obterCheckout() {
	var checkout = new Checkout();
	return checkout;
}