<span id="conteudo-titulo" class="box-com-titulo-header">Contratos - Planejamento</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<?php echo $html->link('Adicionar',array($this->params['prefix'] => true, 'action' => 'adicionar') ,array('class' => 'tabela-adicionar-item')); ?>
	<div class="container-tabela">
		<table class="lista-contratos">
			<thead>
				<tr>
					<th scope="col">ID</th>
					<th scope="col">Nome ?></th>
					<th scope="col">Enviado Por</th>					
					<th scope="col">Tipo</th>
					<th scope="col">Tamanho</th>
					<th scope="col"> &nbsp;</th>				
				</tr>
			</thead>
			<tbody>
			<?php $isOdd = false; ?>
			<tr class='line-tipo-contratos'>
				<td> </td>
				<td colspan="4">Contrato Base</td>
				<td> </td>
			</tr>
			
			<?php foreach($tipos_contratos as $tipo):?>
				<tr class='line-tipo-contrato'>
					<td> </td>
					<td colspan="4"><?php echo $tipo;?></td>
					<td> </td>
				</tr>
			<?php endforeach;?>
			
			
			<?php foreach ($contratos as $contrato): ?>
				<?php if($isOdd):?><tr class="odd"><?php else:?><tr><?php endif;?>
					<td colspan="1" width="10%"><?php echo $contrato['Contrato']['id']; ?></td>
					<td colspan="1" width="30%"><?php echo $contrato['Contrato']['nome']; ?></td>
					<td colspan="1" width="20%"><?php echo $contrato['Usuario']['nome']; ?></td>					
					<td colspan="1" width="10%"><?php echo $contrato['Contrato']['tipo']; ?></td>
					<td colspan="1" width="15%"><?php echo round($contrato['Contrato']['tamanho']/1000); ?> Kb</td>
					<td  colspan="1" width="15%">
						<?php echo $html->link('Visualizar', array($this->params['prefix'] => true, 'controller' => 'Contratos', 'action' =>'visualizar', $contrato['Contrato']['id']), array('class' => 'submit button')); ?>
					</td>
				</tr>
				<?php if($isOdd){ $isOdd = false; } else {$isOdd = true; } ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>