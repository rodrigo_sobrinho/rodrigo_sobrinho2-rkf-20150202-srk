<script type="text/javascript">
$(document).ready(function() {
    $("#form-filtro").submit(function(e) {
        if($("#texto").val().trim() == "") {
            alert('Por favor, preencha o campo');
            $("#texto").focus();
            return false;
        }
        return true;
    });
    $("#mostrar-form").click(function() {
        if($("#div-form").is(":visible"))
            $("#div-form").fadeOut(500);
        else
            $("#div-form").fadeIn(500,function() {
                $(this).find('textarea').focus();
            });
    });
});
</script>
<div class="row-fluid">
    <h3>Prezado formando</h3>
    <br />
    <h4>
        Estamos melhorando o nosso sistema de emissão de boletos.
        <br />
        Para sua maior segurança, solicitamos que ao emitir o boleto você faça as seguintes verificações:
        <br />
        <br />
         <?php if(strpos($banco,'itau') !== false) : ?>
        O seu código de barra deve seguir o seguinte padrão:
        <br />
        <br />
        <span class="strong fg-color-red">
            34191.75XXX XXXXX.XXXXXX 50537.160009 X XXXXXXXXXXXXX
        </span>
        <?php elseif(strpos($banco,'hsbc') !== false) : ?> 
        Os primeiros digitos do código de barras devem ser iguais aos exibidos abaixo:
        <br />
        <br />
        <span class="strong fg-color-red">
            39994.45319 23
        </span>
        <?php else : ?>
        Os primeiros digitos do código de barras devem ser iguais aos exibidos abaixo:
        <br />
        <br />
        <span class="strong fg-color-red">
            03399.43466 92
        </span>
        <?php endif; ?>
        <br />
        <br />
        A primeira linha do seu boleto deve conter as seguintes informações:
        <br />
        <br />
        <div class="row-fluid">
            <div class="span2">
                Cedente:
            </div>
            <div class="span10 strong fg-color-red">
                <?= $boleto["cedente"] ?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span2">
                Agência:
            </div>
            <div class="span10 strong fg-color-red">
                <?= $boleto["agencia"] ?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span2">
                Código do Cedente:
            </div>
            <div class="span10 strong fg-color-red">
                <?= substr($boleto['codigo_cliente'], 0, -1) . '-' . substr($boleto['codigo_cliente'], -1) ?>
            </div>
        </div>
        <br />
        <br />
        Caso encontre qualquer inconsistência, <span class='strong fg-color-greenDark'>não efetue o pagamento</span> 
        e por favor entrar em contato com a RK
        <br />
        ou <span class='strong fg-color-purple pointer' id="mostrar-form">clique aqui</span> 
        para que sejamos notificados
        <br />
        <br />
        <div class="row-fluid hide" id="div-form">
            <div class="span8">
                <?=$form->create('FormandoProfile',array(
                    'url' => "/{$this->params['prefix']}/area_financeira/enviar_boleto_errado",
                    'id' => 'form-filtro')) ?>
                <?=$form->hidden('despesa',array('value' => $despesa))?>
                <?=$form->input('texto',array(
                    'type' => 'textarea',
                    'id' => 'texto',
                    'label' => 'Descreva abaixo os dados incorretos',
                    'div' => 'input-control textarea')); ?>
                <button type="submit" class="bg-color-red">
                    Enviar
                </button>
                <?=$form->end(array('label' => false,
                    'div' => false, 'class' => 'hide')); ?>
            </div>
        </div>
        Em até 04 dias úteis o seu pagamento será registrado em nosso sistema.
        <br />
        Caso não conste o pagamento após esse período,
        <br />
        por favor entrar em contato através do e-mail: 
        <a href="mailto:atendimento@eventos.as" target="_blank">atendimento@eventos.as</a>
        <br />
        <br />
        <a href="<?= $this->here?>/1"
            class='button default' target='_blank'>
            Imprimir Boleto
        </a>
    </h4>
    <br />
    <br />
</div>