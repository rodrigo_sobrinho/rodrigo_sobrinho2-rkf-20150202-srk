<script type="text/javascript">

 	jQuery(function($) {
 		$("#link-gerar-excel").click(function() {
			$("#form-filtro").attr('target', '_blank');
			$("#form-filtro").attr('action', $("#form-filtro").attr('action').replace('index', 'gerar_excel'));
			$("#form-filtro").submit();
			$("#form-filtro").attr('action', $("#form-filtro").attr('action').replace('gerar_excel', 'index'));
			$("#form-filtro").attr('target', '');
			return false;
		});
		
 	});
</script>

<span id="conteudo-titulo" class="box-com-titulo-header">Formandos</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="tabela-adicionar-item">
		<p>
		<?php echo $form->create(false, array('url' => "/{$this->params['prefix']}/formandos/index", 'class' => 'procurar-form-inline', 'id' => 'form-filtro')) ?>
		<label style="display:inline">C&oacute;digo: <?php echo $form->input('filtro-codigo', array('class' => 'alpha omega', 'label' => false, 'div' => false)); ?>
		</label>
		<label style="display:inline">Curso: <?php echo $form->input('filtro-curso', array('options' => $arraySelectDeCursos, 'type' => 'select', 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
		</label>
		<label style="display:inline">Situação: <?php echo $form->input('filtro-situacao', array('options' => array('todas' => 'Todas', 'ativo' => 'Ativo', 'cancelado' => 'Cancelado', 'atrasado' => 'Atrasado', 'aberto' => 'Aberto'), 'type' => 'select', 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?> <?php echo $form->end(array('label' => false, 'div' => false, 'class' => 'submit-busca')) ?></label>
		</p>
		<?php echo $html->link('Gerar excel',array($this->params['prefix'] => true, 'action' => 'index'), array('id' => 'link-gerar-excel')); ?>
		<div style="clear:both;"></div>
	</div>
	<div class="container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col">C&oacute;digo</th>
					<th scope="col">Nome</th>
					<th scope="col">E-mail</th>
					<th scope="col">Curso</th>
					<th scope="col">Situação</th>
					<th scope="col"> &nbsp;</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="4"></td>
					<td colspan="2">Total : <?php echo count($formandos);?> Formando(s)</td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach ($formandos as $formando): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<td  colspan="1" width="10%"><?php echo str_pad($turmaLogada['Turma']['id'], 5, 0 , STR_PAD_LEFT) . str_pad($formando['FormandoProfile']['codigo_formando'], 3, 0 , STR_PAD_LEFT); ?></td>
					<td  colspan="1" width="25%"><?php echo $formando['Usuario']['nome']; ?></td>
					<td  colspan="1" width="15%"><?php echo $formando['Usuario']['email']; ?></td>
					<td  colspan="1" width="15%">
						<?php 
							if (isset($formando['CursoTurma']['Curso']))
								echo $formando['CursoTurma']['Curso']['nome']; 
							else
								echo "-";
						?>
					</td>
					<td  colspan="1" width="10%"><?php echo $formando['FormandoProfile']['situacao']; ?></td>					
					<td  colspan="1" width="25%">
						<?php echo $html->link('Visualizar', array($this->params['prefix'] => true, 'controller' => 'formandos', 'action' =>'visualizar', $formando['FormandoProfile']['id']), array('class' => 'submit button')); ?>
						<?php 
							if ($this->params['prefix'] == 'planejamento' && $checkoutHabilitado)
								echo $html->link('Checkout', array($this->params['prefix'] => true, 'controller' => 'formandos', 'action' =>'checkout', $formando['FormandoProfile']['id']), array('class' => 'submit button')); 
						?>
					</td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>