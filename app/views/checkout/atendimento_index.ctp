<?php
echo $html->script('jquery.tools.min');
echo $html->script('checkout');
echo $html->css('tooltip');
echo $html->css('checkout');
echo $html->css('messages');
?>
<script type="text/javascript">

	$(document).ready(function() {
		
		$( "#tabs" ).tabs();
		
		$(".botao-proximo").click(function() {
			indiceTabSelecionada = ($(".ui-state-active").get(0).id).replace("tab-","");
			indiceDaProximaTab = parseInt(indiceTabSelecionada) + 1;
			nomeDaProximaTab = $("#tab-" + indiceDaProximaTab).children().get(0).rel;
			$("#tabs").tabs( "select" , nomeDaProximaTab );
			window.top.location.href = "#conteudo";
			return false;
		});
		
		$(".botao-anterior").click(function() {
			indiceTabSelecionada = ($(".ui-state-active").get(0).id).replace("tab-","");
			indiceDaProximaTab = parseInt(indiceTabSelecionada) - 1;
			nomeDaProximaTab = $("#tab-" + indiceDaProximaTab).children().get(0).rel;
			$("#tabs").tabs( "select" , nomeDaProximaTab );
			window.top.location.href = "#conteudo";
			return false;
		});
		var adesao = parseFloat(<?=number_format($saldoAdesao,2,'.','')?>);
		var campanhas = parseFloat(<?=number_format($saldoExtras,2,'.','')?>);
		var convites = parseFloat(<?=!empty($convitesExtras) && !isset($convitesExtras['error']) ? number_format($convitesExtras['CheckoutConviteTurma']['valor'],2,'.','') : 0;?>);
		var mesas = parseFloat(<?=!empty($mesasExtras) && !isset($mesasExtras['error']) ? number_format($mesasExtras['CheckoutMesaTurma']['valor'],2,'.','') : 0;?>);
		checkout = obterCheckout();
		checkout.init({adesao:adesao,campanhas:campanhas,convites:0,mesas:0});
		checkout.convites.precificar(convites);
		checkout.mesas.precificar(mesas);

		$("#finalizar-checkout").live('click', function() {
			var Usuario = {};
			var FormandoProfile = {};
			$('.input-checkout').each(function() {
				var campo = $(this).attr('name').replace(/data\[/g,"");
				campo = campo.replace(/\]\[/g,".");
				campo = campo.replace(/\]/g,"");
				campo = campo.replace(/-/g,"_");
				eval(campo + " = '" + $(this).val() + "'");
			});
			$.colorbox({
				width : 700,
				height: 600,
				html : '<p id="response" style="font-size:14px">Aguarde</p>',
				escKey : false,
	 			overlayClose : false,
	 			close : false,
				onLoad : function() { $('#cboxClose').remove(); },
				onComplete : function() {
					jQuery.ajax({
				 		url : "/<?=$this->params["prefix"]?>/checkout/finalizar",
				 		data : { pagamentos:JSON.parse(JSON.stringify(checkout, null, 2)),formando:JSON.parse(JSON.stringify(<?=json_encode($formando)?>)), Usuario : Usuario, FormandoProfile : FormandoProfile },
				 		type : "POST",
				 		dataType : "json",
				 		success : function(response) {
					 		var html = "";
				 	 		if(response.error) {
					 	 		html = response.message;
				 	 		} else {
					 	 		if(response.alertas != undefined) {
					 	 			html = "Checkout foi finalizado com pendências<br /><br />Pendências<br />";
					 	 			$.each(response.alertas, function(tipo,mensagem) {
						 	 			html+= mensagem + "<br />";
					 	 			});
					 	 		} else {
					 	 			html = "Checkout foi finalizado com sucesso<br /><br />";
					 	 		}
					 	 		html+= "Protocolo: " + response.protocolo;
					 	 		html+= '<br /><br /><a href="/<?=$this->params['prefix']?>/solicitacoes/visualizar/' + response.protocolo + '" class="submit button">Mais Informações</a>';
					 	 		$('#response').html(html);
				 	 		}
				 	 		html+= '<br /><br /><a href="/<?=$this->params['prefix']?>/" class="submit button">Home</a>';
				 	 		$('#response').html(html);
				 		},
				 		error : function() {
				 	 		alert("Ocorreu um erro com os nossos servidores. Por favor tente mais tarde.");
				 		}
				 	});
				}
			});
		});
	});
	
</script>
<style type="text/css">
p { font-size:12px }
#cboxLoadedContent { background-color:white!important; border:solid 1px black; border-radius:4px; padding:20px }
.ui-tabs .ui-tabs-panel { padding:10px 0 0 0!important }
#conteudo-container { margin:10px 0 0 0 !important }
h2.titulo { background-color:#C7C5C6; color:black; padding:5px }
.adicionar-itens { font-size:12px }
.adicionar-itens table { width:95%; margin-top:20px }
.adicionar-itens table thead tr th { background:white; border:none; border-bottom:solid 1px #333; padding-bottom:5px; font-size:15px; text-align:left }
.adicionar-itens table tr { background:white; border:none }
.adicionar-itens table tr td { padding:5px; text-align:left }
.upper { text-transform:capitalize; }
.resumo-checkout p { font-size:14px; line-height:30px; }
.resumo-checkout span.pendencia { color:red }
.resumo-checkout span.pendencia.green { color:green }
.pendencias-finais tr td { border-bottom: solid 1px black }
.adicionar-parcelamento {
	margin-top: 5px;
	padding-left: 21px;
	background: url("/img/adicionar_inline.png") no-repeat 0 0 transparent;
}

.adicionar-parcelamento:hover {
	background: url("/img/adicionar_inline.png") no-repeat 0 -14px transparent;
	color: #52b437;
	cursor:pointer;
}
.remover-parcelamento {
	background: url("/img/remover_inline.png") no-repeat 0 0 transparent;
	width: 14px;
	height: 14px;
	float: left;
	display: block;
	margin-right: 7px;
}
.remover-parcelamento:hover {
	background: url("/img/remover_inline.png") no-repeat 0 -14px transparent;
	cursor:pointer;
}
</style>
<span id="conteudo-titulo" class="box-com-titulo-header">Checkout de Formando</span>
<input type='hidden' id='convites-contrato' value="<?=$turma['Turma']['convites_contrato'];?>" />
<input type='hidden' id='mesas-contrato' value="<?=$turma['Turma']['mesas_contrato'];?>" />
<div id="conteudo-container">
	<?php include('_info.ctp')?>
	<div id="tabs" style='border:none!important' class="grid_full alpha omega">
		<ul>
			<li id="tab-0"><a href="#tabs-dados-formando" rel="tabs-dados-formando">Dados Cadastrais</a></li>
			<li class="seta"></li>
			<li id="tab-1"><a href="#tabs-dados-financeiros" rel="tabs-dados-financeiros">Dados Financeiros</a></li>
			<li class="seta"></li>
			<li id="tab-2"><a href="#tabs-adicionar-itens" rel="tabs-adicionar-itens">Adicionar Itens</a></li>
			<li class="seta"></li>
			<li id="tab-3"><a href="#tabs-forma-pagamento" rel="tabs-forma-pagamento">Forma de Pagamento</a></li>
			<li class="seta"></li>
			<li id="tab-4"><a href="#tabs-revisao-checkout" rel="tabs-revisao-checkout">Finalizar</a></li>
		</ul>
		<div id="tabs-dados-formando" class="grid_full alpha omega">
			<?php include('_form_formando.ctp') ?>
		</div>
		<div id="tabs-dados-financeiros" class="grid_full alpha omega">
			<?php include('despesas_formando.ctp')?>
		</div>
		<div id="tabs-adicionar-itens" class="grid_full alpha omega">
			<?php include("_itens.ctp")?>
		</div>
		<div id="tabs-forma-pagamento" class="grid_full alpha omega">
			<?php include("_forma_pagamento.ctp")?>
		</div>
		<div id="tabs-revisao-checkout" class="grid_full alpha omega">
			<?php include('_finalizar.ctp')?>
		</div>
	</div>
	<div style="clear:both;"></div>
</div>