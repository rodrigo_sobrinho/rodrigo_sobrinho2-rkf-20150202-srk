<span id="conteudo-titulo" class="box-com-titulo-header">Agenda - Adicionar Evento</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<?php echo $form->create(null, array('url' => ('/' . $this->params['prefix'] . '/agendas/adicionar'))); ?>
	<?php include('__form.ctp'); ?>
	
	<p class="grid_11 alpha omega">
		<?php echo $html->link('Voltar',array($this->params['prefix']=> true, 'action' => 'index') ,array('class' => 'cancel')); ?>
		<?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit'));?>
	</p>
</div>