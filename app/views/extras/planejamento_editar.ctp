<span id="conteudo-titulo" class="box-com-titulo-header">Editar Extra</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<?php echo $form->create('Extra', array('url' => "/{$this->params['prefix']}/extras/editar/" . $this->data['Extra']['id'])); ?>
	
	<?php include('_form.ctp');?>
	
	<p class="grid_11 alpha omega">
		<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index', $extra['Evento']['id']) ,array('class' => 'cancel')); ?>
		<?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit'));?>
	</p>
</div>