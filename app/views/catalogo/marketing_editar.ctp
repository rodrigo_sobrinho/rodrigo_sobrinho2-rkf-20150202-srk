<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>catalogos/js/nicedit/nicEdit.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>catalogos/js/filetobase64.js"></script>
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/jquery-ui-1.10.3.custom.min.js"></script>
<style type="text/css">
.div-foto { position:relative; display:inline-block; margin:0 5px 5px 0; }
.div-foto img { max-width:100px!important; max-height:100px; border:solid 2px #F2F2F2 }
.div-foto i { position: absolute; top:-5px; right:-5px; }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        var context = ko.contextFor($("#content-body")[0]);
        var eventosCadastrados = <?=json_encode($eventosCadastrados)?>;
        $(".selectpicker").selectpicker({width:'100%'});
        var jaFoiSelecionado = false;
        
        var precos = new nicEditor({fullPanel : true, maxHeight : 200}).panelInstance('precos');
        var informacoes = new nicEditor({fullPanel : true, maxHeight : 200}).panelInstance('informacoes');
        
        function buscarItensDeEvento(eventoId) {
            $("#itens").html("<option>Buscando</option>");
            $("#itens").prop("disabled",true);
            $("#itens").selectpicker("refresh");
            $.getJSON("<?=$this->params['prefix']?>/catalogo/listar_itens/" + eventoId,function(response) {
                if(Object.keys(response.itens).length > 0) {
                    $("#itens").html("<option>Selecione</option>");
                    $.each(response.itens,function(id,nome) {
                        $("#itens").append("<option value='" + id + "'>" + nome + "</option>");
                    });
                    $("#itens").prop("disabled",false);
                    if(!jaFoiSelecionado) {
                        $("#itens").selectpicker("val",'<?=$item["CatalogoItem"]['id']?>');
                        $("#itens").selectpicker("refresh");
                        $("#adicionar-item").trigger("click");
                    } else
                        $("#itens").selectpicker("refresh");
                } else {
                    $("#itens").html("<option>Nenhum Item Encontrado</option>");
                    $("#itens").selectpicker("refresh");
                }
            }).always(function() {
                
            });
        }
        
        $("#eventos-selecionados").change(function() {
            buscarItensDeEvento($(this).val());
        });
        
        $("#itens").change(function() {
            jaFoiSelecionado = true;
            if($(this).val() != "") {
                $("#adicionar-item").fadeIn(500);
            } else {
                $("#adicionar-item").fadeOut(500);
            }
        });
        
        $("#adicionar-item").click(function() {
            if($("button[data-item='"+$("#itens").val()+"']").length == 0) {
                var div = $("<button>",{
                    class : 'bg-color-blue div-item',
                    type : 'button',
                    'data-item' : $("#itens").val(),
                    text : $("#eventos-selecionados > option[value='"+$("#eventos-selecionados").val()+"']").text() +
                            " - " + $("#itens > option[value='"+$("#itens").val()+"']").text()
                });
                div.append($("<i>",{
                    class : "icon-remove remover-item fg-hover-color-red"
                }));
                div.append($("<input>",{
                    type : "hidden",
                    name : "data[itens][]",
                    value : $("#itens").val()
                }));
                $("#div-itens").append(div);
                context.$data.loaded(true);
            }
            $("#adicionar-item").fadeOut(500);
        });
        
        $("#div-itens").on('click','.remover-item',function() {
            var div = $(this).parent('.div-item');
            if(div.data('item-id') != undefined)
                $("#formulario").append($("<input>",{
                    type : "hidden",
                    name : "data[remover_itens][]",
                    value : div.data('item-id')
                }));
            div.fadeOut(500,function() {
                div.remove();
                context.$data.loaded(true);
            });
        });
        
        if(Object.keys(eventosCadastrados).length > 0)
            $("#eventos-selecionados").selectpicker('val',Object.keys(eventosCadastrados));
        else
            $("#eventos-selecionados").selectpicker('val','<?=$item['CatalogoItem']['id']; ?>');
        
        $("#nome-anexo").keyup(function() {
            if($(this).val() != '') {
                $("#anexos").fadeIn(500,function() {
                    context.$data.loaded(true);
                });
            } else {
                $("#anexos").fadeOut(500,function() {
                    context.$data.loaded(true);
                });
            }
        });
        
        $("#anexos").filetobase64({
            onload : function(file) {
                if(!file.valid) {
                    $("#resp-anexo").text("O arquivo tem mais de 10MB");
                } else {
                    var i = $(".div-anexo").length;
                    var div = $("<button>",{
                        class : 'div-anexo bg-color-blue',
                        type : 'button',
                        text : $("#nome-anexo").val() + " - " + file.name
                    });
                    div.append($("<i>",{ class : "icon-remove remover-anexo fg-hover-color-red" }));
                    div.append($("<input>",{
                        type : "hidden",
                        name : "data[anexos]["+i+"][titulo]",
                        value : $("#nome-anexo").val()
                    }));
                    div.append($("<input>",{
                        type : "hidden",
                        name : "data[anexos]["+i+"][nome]",
                        value : file.name
                    }));
                    div.append($("<input>",{
                        type : "hidden",
                        name : "data[anexos]["+i+"][tipo]",
                        value : file.type
                    }));
                    div.append($("<input>",{
                        type : "hidden",
                        name : "data[anexos]["+i+"][tamanho]",
                        value : file.size
                    }));
                    var datinha = file.data.split(",");
                    div.append($("<input>",{
                        type : "hidden",
                        name : "data[anexos]["+i+"][src]",
                        value : datinha[1]
                    }));
                    $("#div-anexos").append(div);
                    $("#resp-anexo").text("");
                    $("#nome-anexo").val('').trigger('keyup');
                    context.$data.loaded(true);
                }
            },
            max : 10000000
        });
        
        $("#div-anexos").on('click','.remover-anexo',function() {
            var div = $(this).parent('.div-anexo');
            if(div.data('id') != undefined)
                $("#formulario").append($("<input>",{
                    type : "hidden",
                    name : "data[remover_anexos][]",
                    value : div.data('id')
                }));
            div.fadeOut(500,function() {
                div.remove();
                context.$data.loaded(true);
            });
        });
        
        function addFoto(file) {
            if(file.valid) {
                var i = $(".div-foto").length;
                var div = $("<div>",{
                    class : 'div-foto'
                });
                div.append($("<i>",{ class : "icon-remove remover-foto fg-hover-color-red" }));
                div.append($("<input>",{
                    type : "hidden",
                    name : "data[fotos]["+i+"][ext]",
                    value : file.name.split('.').pop()
                }));
                div.append($("<input>",{
                    type : "hidden",
                    name : "data[fotos]["+i+"][ordem]",
                    value : i+1
                }));
                div.append($("<input>",{
                    type : "hidden",
                    name : "data[fotos]["+i+"][src]",
                    value : file.data.replace(/^data:image\/(gif|png|jpe?g);base64,/, "")
                }));
                div.append($("<img>",{
                    class : "img-rounded",
                    src : file.data
                }));
                $("#div-fotos").append(div);
            } else {
                alert('arquivo ' + file.name + ' inválido');
            }
        }
        
        $("#fotos").filetobase64({
            onload : function(files) {
                $.each(files,function(i,file) {
                    addFoto(file);
                });
                context.$data.loaded(true);
            },
            onerror : function() {
                alert("Erro ao carregar arquivos");
            },
            multiple : true,
            type : ['png','jpe?g','gif']
        });
        
        $("#div-fotos").on('click','.remover-foto',function() {
            var div = $(this).parent('.div-foto');
            if(div.data('id') != undefined)
                $("#formulario").append($("<input>",{
                    type : "hidden",
                    name : "data[remover_fotos][]",
                    value : div.data('id')
                }));
            div.fadeOut(500,function() {
                div.remove();
                context.$data.loaded(true);
            });
        });
        
        $("#div-fotos").sortable({
            containment : 'parent',
            items : ' > .div-foto',
            update : function() {
                $(".div-foto").each(function() {
                    $(this).find(".ordem").val($(this).index()-1);
                });
            }
        });
        
        $("#enviar").click(function() {
            if($(".div-item").length == 0) {
                alert("Selecione pelo menos um item");
                return;
            }
            $("#precos").val(nicEditors.findEditor('precos').getContent());
            $("#informacoes").val(nicEditors.findEditor('informacoes').getContent());
            context.$data.showLoading(function() {
                $.ajax({
                    type : 'POST',
                    url : $("#formulario").attr("action"),
                    dataType : 'json',
                    data : $("#formulario").serialize(),
                    error : function() {
                        alert("Erro ao enviar dados");
                    },
                    complete : function() {
                        context.$data.page($("#formulario").attr("action"));
                    }
                });
            });
        });
    });
</script>
<h2>
    <a class="metro-button back" data-bind="click: loadThis"
        href="/<?=$this->params['prefix']?>/catalogo/listar/<?=$evento["TiposEvento"]['id']?>/<?=$item['CatalogoItem']['id']; ?>"></a>
    <a class="metro-button reload" data-bind="click: reload"></a>
    <?=$catalogo ? "Editar" : "Inserir"?> Cat&aacute;logo
</h2>
<?php $session->flash(); ?>
<?=$form->create('Catalogo',array(
    'url' => $this->here,
    'id' => 'formulario')) ?>
<?=$form->hidden("Catalogo.id");?>
<div class="row-fluid">
    <div class="span4">
        <?=$form->input('eventos_selecionados', array(
            'label' => "Evento",
            'empty' => "Selecione",
            'id' => "eventos-selecionados",
            'class' => "selectpicker",
            "options" => $eventos,
            'type' => 'select',
            'error' => false,
            'div' => 'input-control text')); ?>
    </div>
    <div class="span4">
        <?=$form->input('Catalogo.catalogo_item_id', array(
            'label' => "Item",
            'empty' => "Selecione",
            'id' => "itens",
            'class' => "selectpicker",
            "options" => array(),
            'type' => 'select',
            'error' => false,
            'div' => 'input-control text')); ?>
    </div>
    <div class="span4">
        <label>&nbsp;</label>
        <button type="button" class="bg-color-red hide"
            id="adicionar-item">
            Adicionar
        </button>
    </div>
</div>
<div class="row-fluid" id="div-itens">
    <?php foreach($itens as $item) : ?>
    <button type="button" class="bg-color-blue div-item"
        data-item="<?=$item["item"]?>"
        data-item-id="<?=$item["id"]?>">
        <?=$item["nome"]?>
        <i class="icon-remove remover-item fg-hover-color-red"></i>
    </button>
    <?php endforeach; ?>
</div>
<div class="row-fluid">
    <?=$form->input('Catalogo.nome', array(
        'label' => 'Titulo',
        'error' => false,
        'div' => 'input-control text')); ?>
</div>
<div class="row-fluid">
    <?=$form->input('Catalogo.informacoes', array(
        'label' => 'Informações',
        'type' => 'textarea',
        'id' => 'informacoes',
        'style' => 'max-width:800px; width:800px; max-height:200px; height:200px',
        'error' => false,
        'div' => 'input-control textarea')); ?>
</div>
<div class="row-fluid">
    <?=$form->input('Catalogo.precos', array(
        'label' => 'Preços',
        'type' => 'textarea',
        'style' => 'max-width:800px; width:800px; max-height:200px; height:200px',
        'id' => 'precos',
        'error' => false,
        'div' => 'input-control textarea')); ?>
</div>
<div class="row-fluid" id="div-fotos">
    <button type="button" class="button default" id="fotos">
        Adicionar Fotos
        <i class="icon-images"></i>
    </button>
    <br />
    <?php if(count($catalogo["CatalogoFoto"]) > 0) : foreach($catalogo["CatalogoFoto"] as $i => $foto) : ?>
    <div class="div-foto" data-id="<?=$foto["id"]?>">
        <i class="icon-remove remover-foto fg-hover-color-red"></i>
        <img src="/fotos/fit/<?=base64_encode("upload/catalogo_fotos/{$foto["id"]}/default.{$foto["ext"]}")?>/100/100"
            class="img-rounded" />
        <input type="hidden" name="data[CatalogoFoto][<?=$i?>][id]"
            value="<?=$foto["id"]?>" />
        <input class="ordem" type="hidden" name="data[CatalogoFoto][<?=$i?>][ordem]"
            value="<?=$foto["ordem"]?>" />
    </div>
    <?php endforeach; endif; ?>
</div>
<br />
<div class="row-fluid" id="div-anexos">
    <input type="text" class="input-large" id="nome-anexo" placeholder="Adicionar Anexo" />
    <button type="button" class="button default hide" id="anexos">
        Adicionar Anexo
        <i class="icon-file-word"></i>
    </button>
    <span id="resp-anexo" class="fg-color-red"></span>
    <br />
    <?php if(count($catalogo["CatalogoAnexo"]) > 0) : foreach($catalogo["CatalogoAnexo"] as $anexo) : ?>
    <button type="button" class="div-anexo bg-color-blue" data-id="<?=$anexo["id"]?>">
        <?=$anexo["titulo"]?> - <?=$anexo["nome"]?>
        <i class="icon-remove remover-anexo fg-hover-color-red"></i>
    </button>
    <?php endforeach; endif; ?>
</div>
<button type="button" class="button bg-color-greenDark" id="enviar">
    Enviar
</button>
<?= $form->end(array('label' => false, 'div' => false, 'class' => 'hide')); ?>