<?php
    $paginator->options(array(
        'url' => array($this->params['prefix'] => true)));
    $sortOptions = array('data-bind' => 'click: loadThis');
?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/datepicker.css">
<style type="text/css">
tr.nova { border:none!important }
tr.nova td { padding:5px 0!important;border:none!important }
tr.nova td textarea { float:left;width:100%;margin:0; margin-bottom:5px;
    padding:5px; border:solid 2px #ededed }
tr.nova:nth-child(odd) td textarea { border-top:none; border-bottom:none;
    border-width:5px }
tr.nova td button { border:none; margin-right:5px!important }
tr.nova:nth-child(odd) td button { margin:0 0 0 5px!important }
</style>
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/bootstrap/datepicker.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery.meio.mask.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.acao-registro.exibir').click(function() {
        dir = $(this).parent().attr('dir');
        bootbox.dialog('Carregando',[{
            label: 'Fechar'
        }],{
            remote: '<?="/{$this->params['prefix']}/registros_contatos/exibir/"?>'+
                dir
        });
    });
    $('.datepicker').datepicker();
    $('input[alt]').setMask();
    $("#incluir-previsao").click(function(e) {
        e.preventDefault();
        var button = this;
        var error = $(button).next();
        var listaDatas = $(".lista-previsao");
        error.text('').attr('class','help-inline');
        var pattern = /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/;
        if($("#data-previsao").val() == "") {
            error.addClass('fg-color-red').text('Digite a data');
            $("#data-previsao").focus();
        } else if(!pattern.test($("#data-previsao").val())) {
            error.addClass('fg-color-red').text('Data inválida');
            $("#data-previsao").focus();
        } else if($(button).attr('disabled') != 'disabled') {
            var url = '<?="/{$this->params['prefix']}/registros_internos/incluir_previsao/"?>';
            var dados = {
                data : { data : $("#data-previsao").val() }
            };
            $(button).attr('disabled','disabled');
            error.text('Enviando...');
            $.ajax({
                url : url,
                data : dados,
                type : "POST",
                dataType : "json",
                success : function(response) {
                    if(response.erro == 1) {
                        error.addClass('fg-color-red').text(response.mensagem);
                    } else {
                        listaDatas.find('.vazio').remove();
                        error.addClass('fg-color-green').text('Data inserida com sucesso');
                        var texto = "<span class='label label-info'>" + $("#data-previsao").val() +
                            "</span> inserida por <span class='label label-warning'>" + response.usuario +
                            '</span> em ' + response.cadastro + '<br />';
                        listaDatas.append(texto);
                        var context = ko.contextFor($("#content-body")[0]);
                        context.$data.loaded(true);
                    }
                },
                error : function() {
                    error.addClass('fg-color-red').text('Erro ao enviar dados');
                },
                complete : function() {
                    $("#data-previsao").val('');
                    $(button).removeAttr('disabled');
                }
            });
        }
    })
    $('#content-body').tooltip({ selector: '[rel=tooltip]'});
    $('.acao-registro.apagar').click(function() {
        dir = $(this).parent().attr('dir');
        bootbox.dialog('Tem Certeza Que Deseja Apagar o Registro de Contato?',[{
            label: 'Apagar',
            class: 'bg-color-red',
            callback: function() {
                var context = ko.contextFor($(".metro-button.reload")[0]);
                var dados = {
                    data: {
                        RegistrosContato: { id: dir }
                    }
                };
                var url = '<?="/{$this->params['prefix']}/registros_contatos/apagar/"?>';
                context.$data.showLoading(function() {
                    bootbox.hideAll();
                    $.ajax({
                        url : url,
                        data : dados,
                        type : "POST",
                        dataType : "json",
                        complete : function() {
                            context.$data.reload();
                        }
                    });
                });
            }
        },{
            label: 'Cancelar',
            class: 'bg-color-blue'
        }]);
    });
    $('.acao-registro.editar,#novo-registro').click(function() {
        url = '<?="/{$this->params['prefix']}/registros_contatos/editar/"?>';
        if($(this).attr('id') != 'novo-registro') {
            dir = $(this).parent().attr('dir');
            url+= dir;
        }
        bootbox.dialog('Carregando',[{
            label: 'Salvar',
            class: 'bg-color-red',
            callback: function() {
                $("#formulario-registro").trigger('submit');
                return false;
            }
        },{
            label: 'Fechar',
            class: 'bg-color-blue'
        }],{
            remote: url
        });
    });
    
    $('#editar-registro').click(function() {
        bootbox.dialog('Carregando',[{
            label: 'Salvar',
            class: 'bg-color-red',
            callback: function() {
                var context = ko.contextFor($(".metro-button.reload")[0]);
                var dados = $("#formulario-registro").serialize();
                var url = $("#formulario-registro").attr('action');
                context.$data.showLoading(function() {
                    bootbox.hideAll();
                    $.ajax({
                        url : url,
                        data : dados,
                        type : "POST",
                        dataType : "json",
                        complete : function() {
                            context.$data.reload();
                        }
                    });
                });
            }
        },{
            label: 'Fechar',
            class: 'bg-color-blue'
        }],{
            remote: '<?="/{$this->params['prefix']}/registros_internos/editar"?>'
        });
    });
    
    var load = false;
    
    $("#festas").on('click','button',function() {
        if(!load) {
            load = true,button = this;
            if($(button).hasClass('observacoes')) {
                var tr = $(button).parent().parent().get(0);
                var id = $(tr).data('id');
                if($(button).hasClass('ativo')) {
                    $("tr.nova."+id).remove();
                    $(button).removeClass('ativo');
                } else {
                    $(button).addClass('ativo');
                    $("<tr>",{
                        class:'nova '+id,
                    }).append(
                        $("<td>",{
                            colspan:'5',
                        }).append(
                            $("<textarea>",{
                                rows:3,
                                html:$(button).data('texto'),
                                placeholder:'Observações'
                            })
                        ).append(
                            $("<button>",{
                                class:'enviar mini bg-color-blueDark',
                                text: 'Enviar'
                            })
                        ).append(
                            $("<button>",{
                                class:'cancelar mini bg-color-red',
                                text: 'Cancelar'
                            })
                        )
                    ).insertAfter($(tr));
                }
                load = false;
            } else if($(button).hasClass('cancelar')) {
                var tr = $(button).parent().parent().prev().get(0);
                var id = $(tr).data('id');
                $("tr.nova."+id).remove();
                $(tr).find('.observacoes').removeClass('ativo');
                load = false;
            } else if($(button).hasClass('enviar')) {
                var tr = $(button).parent().parent().prev().get(0);
                var id = $(tr).data('id');
                var texto = $("tr.nova."+id).find('textarea').get(0);
                if($(texto).val() == "") {
                    alert('Digite as observações');
                    $(texto).focus();
                } else {
                    dados = {
                        data : {
                            festa : id,
                            turma : <?=$turma['Turma']['id']?>,
                            obs : $(texto).val()
                        }
                    };
                    $.ajax({
                        url : '<?="/{$this->params['prefix']}/registros_internos/observacoes_festa"?>',
                        data : dados,
                        type : "POST",
                        dataType : "json",
                        success : function(response) {
                            if(response.erro == 0) {
                                alert('Observações salvas');
                                $(tr).find('.observacoes').text($(texto).val().substring(0,10)+"...")
                                        .removeClass('ativo');
                                $("tr.nova."+id).remove();
                            } else
                                alert('Erro ao salvar observações');
                        },
                        error : function() {
                            alert('Erro conectar servidor');
                        },
                        complete : function() {
                            load = false;
                        }
                    });
                }
            }
        }
    });
});
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Registro Interno
            <button class="default pull-right" type="button" id="editar-registro">
                Editar Registro Interno
            </button>
        </h2>
    </div>
</div>
<?php $session->flash(); ?>
<div class='row-fluid'>
    <div class='span6'>
        <div class='row-fluid'>
            <h4>Memorando do Consultor</h4>
            <h5>
                <?php if($turma['Turma']['memorando'] == '') : ?>
                Nenhum Memorando Adicionado
                <?php else : ?>
                <?=substr($turma['Turma']['memorando'],0,200)?>
                <?=strlen($turma['Turma']['memorando']) > 200 ? " <b>...</b>" : ''?>
                <?php endif; ?>
            </h5>
        </div>
        <br />
        <div class='row-fluid'>
            <h4>Reuni&otilde;es</h4>
            <h5><?=$reunioes?></h5>
        </div>
        <br />
        <div class='row-fluid'>
            <h4>Liga&ccedil;&otilde;es</h4>
            <h5><?=$ligacoes?></h5>
        </div>
        <br />
        <div class='row-fluid'>
            <h4>Festas</h4>
            <?php if(count($festas) > 0) : ?>
            <br />
            <table class="table table-condensed table-striped" id="festas">
                <thead>
                    <tr>
                        <th scope="col" width="20%">Data</th>
                        <th scope="col" width="40%">Local</th>
                        <th scope="col" width="8%">Conv</th>
                        <th scope="col" width="8%">Comp</th>
                        <th scope="col" width="24%">Obs</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach($festas as $festa) :
                            $observacoes = '';
                            if(!empty($festa['RegistrosContato']['observacoes'])) {
                                $obsArray = json_decode($festa['RegistrosContato']['observacoes']);
                                if(isset($obsArray->$turma['Turma']['id']))
                                    $observacoes = $obsArray->$turma['Turma']['id'];
                            }
                    ?>
                    <tr data-id="<?=$festa['RegistrosContato']['id']?>">
                        <td><?=date('d/m/Y',strtotime($festa['RegistrosContato']['data']));?></td>
                        <td><?=$festa['RegistrosContato']['local']?></td>
                        <td><?=$festa[0]['convidados']?></td>
                        <td><?=$festa[0]['compareceram']?></td>
                        <td style="text-align: center">
                            <button type="button" class="mini observacoes default"
                                data-texto="<?=$observacoes?>" style="min-width:90%; margin:3px 0">
                                <?=substr($observacoes,0,10)."..."?>
                            </button>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php else : ?>
            Nenhuma festa pra essa turma
            <?php endif; ?>
        </div>
    </div>
    <div class='span6'>
        <div class='row-fluid'>
            <h4>Expectativa de Fechamento</h4>
            <h5><?=ucfirst($turma['Turma']['expectativa_fechamento'])?></h5>
        </div>
        <br />
        <div class='row-fluid'>
            <h4>Data Prevista Para Fechamento</h4>
            <h5>
                <?php if(!(count($turma['TurmaPrevisaoFechamento']) > 0)) : ?>
                Sem Previsão de Fechamento
                <?php else : ?>
                <?=date('d/m/Y',strtotime($turma['TurmaPrevisaoFechamento'][count($turma['TurmaPrevisaoFechamento'])-1]['data_previsao']))?>
                <?php endif; ?>
            </h5>
        </div>
        <br />
        <div class='row-fluid'>
            <h4>Concorrentes</h4>
            <h5>
                <?php if($turma['Turma']['concorrentes'] == '') : ?>
                Nenhum Concorrente Adicionado
                <?php else : ?>
                <?=$turma['Turma']['concorrentes']?>
                <?php endif; ?>
            </h5>
        </div>
        <br />
        <div class='row-fluid'>
            <h4>Previs&atilde;o Fechamento de Contrato</h4>
            <br />
            <div class="row-fluid lista-previsao">
                <?php if(count($turma['TurmaPrevisaoFechamento']) > 0) : ?>
                <?php foreach($turma['TurmaPrevisaoFechamento'] as $previsao) : ?>
                <span class='label label-info'><?=date('d/m/Y',strtotime($previsao['data_previsao'])); ?></span> 
                inserida por <span class='label label-warning'><?=$previsao['Usuario']['nome']?></span> em 
                <?=date('d/m/Y à\s H:i:s',strtotime($previsao['data_cadastro'])); ?><br />
                <?php endforeach; ?>
                <?php else : ?>
                <h5 class="vazio">Sem Previsão de Fechamento</h5>
                <?php endif; ?>
            </div>
            <br />
            <div class="row-fluid">
                <div class="span12">
                    <div class="input-control text span4">
                        <input type="text" id="data-previsao" class="datepicker" alt="99/99/9999" />
                    </div>
                    <a class="button bg-color-greenDark" id="incluir-previsao">
                        Incluir Data de Previs&atilde;o
                    </a>
                    <span class="help-inline">

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<br />
<br />
<h2 class='fg-color-red'>
    Registro de Contatos
    <button class="default pull-right" type="button" id="novo-registro">
        Novo Registro de Contato
    </button>
    <button type="button" class="pull-right bg-color-orangeDark"
        href="/<?=$this->params['prefix']?>/registros_contatos/estatisticas" data-bind="click:loadThis">
        Estatisticas
        <i class="icon-stats-up"></i>
    </button>
    <button type="button" class="pull-right bg-color-blueDark"
        href="/<?=$this->params['prefix']?>/registros_contatos/timeline" data-bind="click:loadThis">
        Timeline
        <i class="icon-stats-up"></i>
    </button>
</h2>
<br />
<div class="row-fluid">
    <?php if (sizeof($registros) == 0) { ?>
    <h3 class='fg-color-red'>Nenhum Registro de Contato</h3>
    <?php } else { ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="15%"><?=$paginator->sort('Usuário', 'Usuario.nome',$sortOptions); ?></th>
                <th scope="col" width="10%"><?=$paginator->sort('Data', 'RegistrosContato.data',$sortOptions); ?></th>
                <th scope="col" width="10%"><?=$paginator->sort('Tipo', 'RegistrosContato.tipo',$sortOptions); ?></th>
                <th scope="col" width="20%"><?=$paginator->sort('Assunto', 'RegistrosContato.assunto',$sortOptions); ?></th>
                <th scope="col" width="20%">Comissao Part.</th>
                <th scope="col" width="25%"></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($registros as $registro): ?>
            <tr>
                <td><?=$registro['Usuario']['nome']?></td>
                <td><?=date('d/m/Y',strtotime($registro['RegistrosContato']['data']));?></td>
                <td><?=ucfirst(utf8_encode($registro['RegistrosContato']['tipo'])) ?></td>
                <td><?=ucfirst($registro['RegistrosContato']['assunto']); ?></td>
                <td>
                    <?php if(count($registro['RegistroContatoUsuario']) > 0) : $com = array(); ?>
                    <?php 
                        foreach($registro['RegistroContatoUsuario'] as $registroUsuario) 
                            $com[] = (!empty($registroUsuario['Usuario']['nome'])) ? $registroUsuario['Usuario']['nome'] : "<span class='label label-inverse'>Nenhum</span>"; 
                    ?>
                    <span class="label label-inverse">
                        <?=implode('<br />',$com)?>
                    </span>
                    <?php else : ?>
                    Nenhum
                    <?php endif; ?>
                </td>
                <td dir="<?=$registro['RegistrosContato']['id']?>"
                    style="text-align:center">
                    <button class="mini acao-registro exibir default" type="button">
                        Visualizar
                    </button>
                    <?php if($registro['RegistrosContato']['tipo'] != 'festa') : ?>
                    <button class="mini acao-registro editar bg-color-blue" type="button" dir="<?=$registro['RegistrosContato']['id']?>">
                        Editar
                    </button>
                    <button class="mini acao-registro apagar bg-color-red" type="button">
                        Apagar
                    </button>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="6">
                    <?=$paginator->numbers(array('separator' => ' ','data-bind' => 'click: loadThis')); ?>
                </td>
            </tr>
        </tfoot>
    </table>
    <?php } ?>
</div>