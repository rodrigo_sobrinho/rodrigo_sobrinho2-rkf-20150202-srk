<?php if($out == "html") : ?>
<?php include('salvar_contrato.ctp')?>
<?php endif; ?>
<?php 
setlocale( LC_ALL, 'pt_BR', 'pt_BR.iso-8859-1', 'pt_BR.utf-8', 'portuguese');
$mes = strftime("%B");
?>
<h4 style="text-align: center"><b>CONTRATO DE ARRECADAÇÃO DE VALORES</b></h4>
<br />
<p style="text-align: justify">
Pelo presente Contrato, as partes:
<br />
<br />
ZAPE GESTAO DE VALORES LTDA, sociedade brasileira por quotas de responsabilidade limitada, inscrita no CNPJ sob o 
n.º 18.405.543/0001-54, estabelecida no estado de São Paulo, onde é sediada à AV José Maria Whitaker, 882 – CEP 04057-000 – 
São Paulo, SP, Planalto Paulista, neste ato legalmente representada por seu sócio, o Sr. Rachid Sader Neto, brasileiro, casado, 
administrador de empresa, portador da cédula de identidade RG n.º 35.774.690 SSP/SP e inscrito no CPF/MF sob o n.º 112.710.516-72, 
residente e domiciliado à Rua Nhandu 308, Planalto Paulista na Cidade de São Paulo, Estado de São Paulo e/ou Procurador. 
<br />
<br />
E de outro lado:
<br />
<br />
Na qualidade de <b>Contratantes</b>: Os formandos do(s) curso(s) de <b><?=$turma['Turma']['cursos']?></b>,
da <b><?=$turma['Turma']['nome']?></b>, com previsão de formatura para o <b><?=($turma['Turma']['semestre_formatura'] == 1) ? 'primeiro' : 'segundo';?> 
    semestre de <?=$turma['Turma']['ano_formatura']?></b>, neste ato devidamente representados pela <b>Comissão de Formatura</b>;
<br />
<br />
Tem entre si justo e contratado o presente “Contrato de Arrecadação de Valores”, que será regido pelos termos e condições a 
seguir estipulados, em conformidade com os artigos 125 e seguintes do Código Civil Brasileiro:
<br />
<br />
<b>I - ANEXOS</b>
<br />
<br />
<b>1.</b> São parte integrante do Contrato os seguintes anexos:
<br />
<br />
<b>1.1.</b> Anexo A - Ata de Constituição da Contratante;
<br />
<br />
<b>1.2.</b> Anexo B - Ata de Fechamento com todas as condições contratuais estabelecidas entre as partes;
<br />
<br />
<b>1.3.</b> Anexo C - Definições dos valores arrecadados dos formandos, incluindo datas dos planos, cobrança de IGPM, multa, valores
e número de parcelas;
<br />
<br />
<b>1.4.</b> Anexo D - Modelo do Termo de Adesão;
<br />
<br />
<b>1.5.</b> Anexo E - Detalhes de como serão geridos os valores;
<br />
<br />
<b>1.6.</b> Anexo F - Modelo de ordem de pagamento a fornecedores.
<br />
<br />
<b>II – CONSIDERAÇÕES INICIAIS</b>
<br />
<br />
<b>2.1.</b> A CONTRATADA compromete-se a realizar todo o serviço em nome da CONTRATANTE visando arrecadação de valores para a 
realização de um determinado evento.
<br />
<br />
<b>2.2.</b> Fica desde já estipulado que a CONTRATADA não tem nenhum vínculo com a prestação de nenhum serviço referente à evento,
sendo o único objeto desse contrato a arrecadação de valores em nome da Comissão de Formatura.
<br />
<br />
<b>2.3.</b> Fica desde já estipulado que a CONTRATADA após a transferência dos valores para conta estipulada nesse contrato ou 
para contas definidas pela CONTRATANTE, não possui a menor responsabilidade legal ou tributária frente a fornecedores, Comissão 
de Formatura e principalmente formandos.
<br />
<br />
<b>2.4.</b> Para a realização de tal serviço a CONTRATADA cobrará uma taxa de <b><?=$turma['Turma']['fee']?>%</b> sobre o valor 
arrecadado, sendo que, quando houver transferências, seja para a conta principal, seja para contas definidas pela comissão de 
formatura, tal montante será retido tornando-se de direito exclusivo da CONTRATADA.
<br />
<br />
<b>III – DO OBJETO DO CONTRATO</b>
<br />
<br />
<b>3.1.</b> É objeto do presente instrumento a realização dos serviços pela Contratada, por conta e ordem da Contratante, 
conforme descrito a seguir:
<br />
<br />
<p style="margin-left: 30px; text-align: justify">
<b>a.</b> Emissão de Boletos Bancários aos formandos, com base nas informações fornecidas pela Contratante (nome 
completo / dados bancários / vencimentos / valores / multas por atraso / juros incidentes / etc );<br/><br/>
<b>b.</b> Recebimento dos recursos pagos pelos Formandos;<br/><br/>
<b>c.</b> Repasse dos valores e/ou pagamento dos fornecedores e prestadores de serviços contratados ou a serem contratados para 
realização de um determinado evento definido pela Contratante; <br/><br/>
<b>d.</b> Manter uma planilha gerencial atualizada, com todas as informações pertinentes a um determinado evento definido pela 
Contratante, a fim de que a esta possa acompanhar o desenvolvimento das atividades ora contratadas.<br/><br/>
</p>
<b>3.2.</b> Os valores pagos pelos formandos permanecerão em posse da Contratada de forma transitória, até o momento do pagamento 
e/ou repasse para a conta da comissão ou fornecedores e/ou aos prestadores de serviços do Evento definido pela Contratante.
<br />
<br />
<b>IV - VALORES</b>
<br />
<br />
<b>4.</b> É obrigação da CONTRATANTE estabelecer os valores que serão cobrados.
<br />
<br />
<b>4.1.</b> A CONTRATANTE pode definir os valores usando as seguintes variáveis:
<p style="margin-left: 30px; text-align: justify">
<b>a.</b> Mês de início de pagamento;<br/><br/>
<b>b.</b> Número de parcelas;<br/><br/>
<b>c.</b> Possibilidade de correção a cada 12 meses de assinatura do contrato entre a CONTRATADA e a CONTRATANTE;<br/><br/>
<b>d.</b> Caso exista outra variável desejada pela CONTRATANTE, deverá ser acordado com a CONTRATADA e inserido no Anexo C.<br/><br/>
</p>
<b>4.2.</b> Caso a CONTRANTANTE defina novas formas de pagamento após o lançamento do pacote, a mesma deverá ser formalizada 
pela CONTRATANTE por e-mail, sistema, escrito, virando anexo a esse contrato como Formas de pagamentos adicionais definidas pela 
CONTRATANTE.
<br />
<br />
<b>V – CADASTRO DE DADOS</b>
<br />
<br />
<b>5.</b> Para a efetiva arrecadação de valores é necessário o preenchimento dos dados cadastrais desse formando. Esses 
dados podem ser fornecidos das seguintes formas:
<p style="margin-left: 30px; text-align: justify">
<b>a.</b> Através do preenchimento do Anexo D pelo próprio formando;<br/><br/>
<b>b.</b> Através do preenchimento dos dados no sistema online da CONTRATADA usando o código da turma (estipulado na 
introdução desse contrato) (http://asformaturas.com.br/espaco_formando); <br/><br/>
<b>c.</b> Através de lista em formato eletrônico ou impresso fornecido pela CONTRATANTE com os dados dos formandos 
(principalmente nome, e-mail e CPF), valores e numero de parcelas. Caso a opção da comissão seja essa deverá ser incluída 
a esse contrato como Anexo C.<br/><br/>
</p>
<b>5.1.</b> Independente da maneira como os dados serão fornecidos, eles deverão estar disponíveis online para a CONTRATANTE 
acompanhar o desenvolvimento de todo processo de arrecadação dos valores. 
<br />
<br />
<b>VI – FORMA DE ARRECADAÇÃO</b>
<br />
<br />
<b>6.1.</b> As arrecadações serão efetuadas através de Boleto Bancário. A CONTRATADA disponibilizara para cada formando LOGIN e 
SENHA para acesso no sistema online, onde poderão gerar os boletos para os pagamentos e acompanhar a baixa dos seus pagamentos.
<br />
<br />
<b>6.2.</b> Caso os formandos optem em receber os boletos em casa é cobrado uma taxa de R$ 20,00, valor esse de direito da 
CONTRATADA.
<br />
<br />
<b>VII – RELATÓRIOS E ACESSO AS INFORMAÇÕES</b>
<br />
<br />
<b>7.</b> A CONTRATADA deverá fornecer as informações no seu sistema online, onde a CONTRATANTE possuirá LOGIN e SENHA para que de 
forma exclusiva e confidencial possa acompanhar o andamento das arrecadações.<br /><br />
<br />
<br />
<b>7.1.</b> As informações serão disponibilizadas no relatório para a CONTRATANTE da seguinte forma:<br /><br />
<p style="margin-left: 30px; text-align: justify">
<b>a.</b> Dados Cadastrais efetuados diretamente pelo formando no sistema da CONTRATADA;<br/><br/>
<b>b.</b> Dados Cadastrais entregues impressos ou em meio eletrônico para a CONTRATADA;<br/><br/>
<b>c.</b> Boletos Pagos: até 05 dias úteis após o pagamento pelo formando..<br/><br/>
</p>
<b>VIII – GESTÃO DE VALORES</b>
<br />
<br />
<b>8.1.</b> Os valores arrecadados serão geridos pela CONTRATADA conforme Anexo E.<br /><br />
<b>8.2.</b> Caso a CONTRATANTE opte pelo pagamento direto aos fornecedores ou transferências para contas que a mesma entender como 
necessários, deverá ser feito através do Anexo F.
<br />
<br />
<b>IX – INADIMPLÊNCIA</b>
<br />
<br />
<b>9.</b> A CONTRATADA não se compromete com índices de arrecadação e com os valores previstos e não arrecadados. Dessa forma, 
os valores não pagos pelos formandos, não são de responsabilidade da CONTRATADA. 
<br />
<br />
<b>9.1</b> A CONTRATADA disponibiliza relatórios online para a CONTRATANTE para que a mesma acompanhe como está evoluindo o processo 
de arrecadação dos formandos. 
<br />
<br />
<b>9.2</b> Para auxiliar a arrecadação, poderá ser enviado comunicado ao e-mail cadastrado do formando, bem como alertas no sistema 
online que existem parcelas em aberto. 
<br />
<br />
<b>9.3</b> Caso o CONTRATANTE decida cobrar juros e/ou multas por atraso deverá informar a CONTRATADA no Anexo C. Todo o 
valor de juros e multa é de direito da CONTRATANTE, exceto o FEE que incorre em todo o valor arrecadado.
<br />
<br />
<b>X - OBRIGAÇÕES E RESPONSABILIDADES DA CONTRATADA</b>
<br />
<br />
<b>10.1.</b> Para o fiel cumprimento das obrigações contratuais que se originam do presente instrumento, cabe à Contratada: 
<br />
<br />
<p style="margin-left: 30px; text-align: justify">
<b>a.</b> Fornecer sistema online que possibilite a CONTRATANTE à visualização das informações cadastrais, de pagamento;<br/><br/>
<b>b.</b> Fornecer sistema online que possibilite ao formando emitir o boleto de arrecadação e acompanhe a baixa do mesmo;<br/><br/>
</p>
<b>XI – DURAÇÃO DO CONTRATO</b>
<br />
<br />
<b>11.</b> Esse contrato não possui dado de término da prestação do serviço. 
<br />
<br />
<b>11.1.</b> A qualquer momento a CONTRATANTE pode cancelar a prestação do serviço sem ônus.
<br />
<br />
<b>11.2.</b> No caso de cancelamento do contrato, a CONTRATADA continua obrigada a transferir valores arrecadados e ainda não transferidos para a CONTRATANTE.
<br />
<br />
<b>XII - DA CONFIDENCIALIDADE</b>
<br />
<br />
<b>12.1.</b> As Partes se obrigam a manter em absoluto sigilo os termos e condições deste Contrato e de quaisquer “Informações Confidenciais” da outra Parte, que venha a ter acesso antes, durante ou após o prazo de vigência deste instrumento, sob a pena de responder por perdas e danos e demais cominações legais cabíveis.
<br />
<br />
<b>12.2.</b> Para efeitos desta cláusula, entender-se-á por “Informações Confidenciais” todo e qualquer documento e/ou informação 
de natureza sigilosa, de quaisquer das Partes ou de qualquer uma das pessoas físicas e/ou jurídicas a elas vinculadas, divulgada à 
outra Parte ou a qualquer uma das pessoas físicas ou jurídicas a ela vinculadas, de forma verbal, escrita ou por qualquer outro meio,
direto ou indireto, antes ou após a presente data, incluindo, mas não se limitando, àqueles relacionados a dados, materiais, 
especificações técnicas, ideias, conceitos, métodos, invenções, desenvolvimento de produtos e sistemas, estratégias de negócios, 
segredos profissionais em geral, empregados, fornecedores, parceiros comerciais, produtos, preços, dados financeiros e contábeis 
e processos judiciais.
<br />
<br />
<b>XIII – FORO</b>
<br />
<br />
<b>13.</b> Fica eleito o Foro Central da Comarca da capital do Estado de São Paulo, para dirimir eventuais questões oriundas da 
celebração, interpretação e execução do presente Contrato, com renúncia a qualquer outro, por mais privilegiado que seja, arcando 
a parte vencida com todo o ônus de sucumbência, inclusive honorários advocatícios de 20% (vinte por cento) do total da condenação.
<br />
<br />
<h5 style="text-align: center; color: black">São Paulo, <?=date('d')?> de <?=$mes?> de <?=date('Y')?></h5><br /><br />
<?php foreach($turma['Turma']['comissao'] as $comissao) : ?>
<p style="line-height: 1.3em">
<b>Nome: </b><?=key($comissao)?>
<br />
<b>RG: </b><?=$comissao[key($comissao)]?>
<br />
<br />
<?=str_pad("",60,"_")?> 
<br />
ASSINATURA
<br />
</p>
<?php endforeach; ?>
