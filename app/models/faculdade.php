<?php

class Faculdade extends AppModel {
	var $name = 'Faculdade';
	
	var $actsAs = array('Containable');
	
	var $hasMany = array('Curso');
	var $belongsTo = array('Universidade');
	/*
	 * Tabela no banco está assim:
	 * id INT(10)
	 * universidade_id INT(10)
	 * nome VARCHAR(100)
	 * sigla VARCHAR(10)
	 */
	 
	 var $validate = array(
		'nome' => array(
			'vazio' => array(
                'rule' => 'notEmpty',
                'message' => 'Preencha o nome.'
            )
		)
    );
}
?>