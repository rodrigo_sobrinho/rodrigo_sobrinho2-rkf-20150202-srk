<?php

class ComissaoVendedor extends AppModel {

    var $name = 'ComissaoVendedor';
    var $belongsTo = array('Turma', 'Usuario');
    var $useTable = 'comissao_vendedor';
    
    function alocarUsuarioComissao($usuario_id, $turma_id, $porcentagem) {
        $usuarioId = Sanitize::paranoid($usuario_id);
        $turmaId = Sanitize::paranoid($turma_id);
        $porcentagem = Sanitize::paranoid($porcentagem);
        $verifica = $this->query("SELECT * FROM comissao_vendedor WHERE turma_id = " . $turmaId . " AND usuario_id = " . $usuarioId);
        if (count($verifica) == 0) {
            $query = $this->query("INSERT INTO comissao_vendedor (turma_id, usuario_id, porcentagem) VALUES (" . $turmaId . "," . $usuarioId . "," . $porcentagem . ")");
            return true;
        } else {
            return false;
        }
    }
    
    function verificarPaganteEsteMes($uid){
        if(date('d') > 20){
            $start = "0 month";
            $end = "+1 month";
        }else{
            $start = "-1 month";
            $end = "0 month";
        }
        $date_start = date("Y/m/20", strtotime($start));
        $date_end = date("Y/m/20", strtotime($end));
        App::import('model','Despesa');
        $this->Despesa = new Despesa();
        $formandoDespesas = $this->Despesa->find('count', array(
            'conditions' => array(
                    'Despesa.usuario_id' => $uid,
                    'Despesa.data_pagamento BETWEEN ? AND ?' => array($date_start, $date_end),
                    'Despesa.status' => array('paga','renegociada','cancelada'),
                    'Despesa.tipo' => 'adesao'
            )
        ));
        return $formandoDespesas > 0;
    }
}

?>