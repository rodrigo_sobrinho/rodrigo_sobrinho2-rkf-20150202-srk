<?php echo $form->hidden('Usuario.id'); ?>

<p class="grid_11 alpha omega">
    <label class="grid_5 alpha">Nome</label>
    <?php echo $form->input('nome', array('class' => 'grid_6 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>

<p class="grid_11 alpha omega">
    <label class="grid_5 alpha">Nível</label>
    <label class="grid_6 omega">Grupo</label>
    <?php echo $form->input('Usuario.nivel', array('class' => 'grid_5 first alpha omega', 'label' => false, 'div' => false)); ?>
    <?php echo $form->input('Usuario.grupo', array('class' => 'grid_6 omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_5'))); ?>
</p>
<?php if($this->data['Usuario']['grupo'] == 'comercial') : ?>
<?=$html->css('chosen');?>
<?=$html->script('chosen.jquery');?>
<script type="text/javascript">
    $(document).ready(function(){
        $(".chosen").chosen({width:'100%'});
        var universidadesVinculadas = <?=json_encode($selecionados);?>;
        $("#formulario-usuario").submit(function(e) {
            var universidadesInserir = $('#universidades-vinculadas').chosen().val();
            var universidadesRemover = [];
            $.each(universidadesVinculadas,function(i,universidade) {
                if($.inArray(universidade,universidadesInserir) < 0)
                    universidadesRemover.push(universidade);
                else
                    universidadesInserir.splice($.inArray(universidade,universidadesInserir),1);
            });
            for(var i in universidadesInserir)
                $("<input>",{
                    type:'hidden',
                    name:'data[UniversidadeUsuario][][UniversidadesUsuario][universidade_id]',
                    value:universidadesInserir[i]
                }).appendTo($("#formulario-usuario"));
            for(var i in universidadesRemover)
                $("<input>",{
                    type:'hidden',
                    name:'data[universidadesRemovidas][]',
                    value:universidadesRemover[i]
                }).appendTo($("#formulario-usuario"));
        })
    })
</script>
<p class="grid_11 alpha omega universidades-vinculadas">
    <label class="grid_11 alpha omega">Faculdades Vinculadas</label>
    <?=$form->input('comissao', array(
        'options' => $universidades,
        'selected' => $selecionados,
        'multiple' => 'multiple',
        'id' => 'universidades-vinculadas',
        'data-placeholder' => 'Selecione',
        'type' => 'select',
        'class' => 'chosen',
        'label' => false,
        'div' => false)); ?>
</p>
<?php endif; ?>
<p class="grid_11 alpha omega">
    <label class="grid_5 alpha">E-mail</label>
    <?php echo $form->input('Usuario.email', array('class' => 'grid_6 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>	

<p class="grid_11 alpha omega">
    <label class="grid_5 alpha"><?php echo $form->label('Usuario.senha'); ?></label>
    <?php echo $form->input('Usuario.senha', array('class' => 'grid_6 first alpha omega', 'type' => 'password', 'value' => '', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>

<p class="grid_11 alpha omega">
    <label class="grid_5 alpha"><?php echo $form->label('Usuario.confirmar'); ?></label>
    <?php echo $form->input('Usuario.confirmar', array('class' => 'grid_6 first alpha omega', 'type' => 'password', 'value' => '', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
</p>
