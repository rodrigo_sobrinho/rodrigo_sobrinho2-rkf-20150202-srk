function Checkout() {
	
	this.extras = {
		itens : [],
		inserir : function(item) {
			this.itens.push(item);
		},
		comprar : function(quantidade,id,idValorEspecial) {
			$.each(this.itens,function(i,item) {
				if(item.id == id) {
					if(quantidade == 0) {
						item.ativo = false;
						item.saldo = 0;
					} else {
						item.ativo = true;
						item.saldo = item.valor*quantidade*-1;
					}
					item.quantidade = quantidade;
					item.comprados = quantidade;
					checkout.extras.atualizar(item, idValorEspecial);
					return true;
				}
			});
		},
		iniciar : function(itens) {
			$.each(itens,function(i,item) {
				checkout.extras.inserir(item);
			});
		},
		atualizar : function(item,idValorEspecial) {
			valorEspecial = $(idValorEspecial).val().replace(',','.')*item.comprados;
			if(item.saldo == 0){
				$('.saldo-item-'+item.id).html('-');
			}else{
				if(valorEspecial)
					item.saldo = (valorEspecial*-1);
				$('.saldo-item-'+item.id).html('R$' + number_format(item.saldo*-1,2,',','.'));
			}
			checkout.total.atualizar();
		},
		obterSaldoTotal : function() {
			var saldoItens = 0;
			$.each(this.itens,function(i,item) {
				if(item.saldo != undefined)
					saldoItens+= item.saldo;
			});
			return saldoItens;
		}
	};
	
	this.total = {
		atualizar : function() {
			saldoTotal = checkout.extras.obterSaldoTotal();
			if(saldoTotal == 0)
				$(".soma-itens").html('-');
			else
				$(".soma-itens").html('R$' + number_format(saldoTotal*-1,2,',','.'));
			var parcelas = $('select[name="parcelas"]').val();
			parcela = saldoTotal / parcelas;
			if(saldoTotal == 0) {
				$('input[name="valor-parcela"]').val('');
			} else {
				$('input[name="valor-parcela"]').val('R$' + number_format(parcela*-1,2,',','.'));
			}
		}
	}
	
}

function obterCheckout() {
	var checkout = new Checkout();
	return checkout;
}