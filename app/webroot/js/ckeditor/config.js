/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
    config.bodyClass = 'alaaa';
	config.language = 'pt-br';
	config.uiColor = '#fff';
    config.toolbar =
    [
        ['Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList', '-','JustifyLeft','JustifyCenter','JustifyRight']
    ];
    config.toolbarCanCollapse = false;
    config.resize_enabled = false;
};
