<?php

class ReciboComissaoUsuario extends AppModel {

    var $name = 'ReciboComissaoUsuario';
    var $belongsTo = array(
        'Formando' => array(
            'className' => 'ViewFormandos',
            'foreignKey' => 'usuario_id'
        ),
        'ReciboComissao'
    );
    var $useTable = 'recibo_comissao_usuario';
    
    function verificarComissaoPaga($vendedorId,$usuarioId){
        $recebidos = $this->find('count', array(
            'conditions' => array(
                'ReciboComissaoUsuario.usuario_id' => $usuarioId,
                'ReciboComissao.vendedor_id' => $vendedorId
            )
        ));
        if($recebidos > 0)
            return $recebidos;
        else
            return false;
    }
    
}

?>