<span id="conteudo-titulo" class="box-com-titulo-header"><?php echo "Extras de " . $evento['Evento']['nome'];?></span>
<div id="conteudo-container">
	<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
	<?php $session->flash(); ?>
	<div class="tabela-adicionar-item">
		<?php echo $html->link('Novo Extra',array($this->params['prefix'] => true, 'action' => 'adicionar', $evento['Evento']['id'])); ?>
		<div style="clear:both;"></div>
	</div>
	<div class="container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col"><?php echo $paginator->sort('Id', 'id'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Nome', 'nome'); ?></th>				
					<th scope="col"><?php echo $paginator->sort('Descricao', 'descricao'); ?></th>
					<th scope="col"> &nbsp;</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="2"><?php echo $paginator->counter(array('format' => 'Extra %start% ao %end% - página %page% de %pages%')); ?>
						<span class="paginacao">
							<?php echo $paginator->numbers(array('separator' => ' ')); ?>
						</span>
					</td>
					<td colspan="1"><?php echo $paginator->counter(array('format' => 'Total : %count% ' .  $this->name)); ?></td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach($extras as $extra): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<td colspan="1"><?php echo $extra['Extra']['id'];?></td>
					<td colspan="1"><?php echo $extra['Extra']['nome'];?></td>
					<td colspan="1"><?php echo $extra['Extra']['descricao']?></td>
					<td colspan="1">
					<?php echo $html->link('Visualizar', array($this->params['prefix'] => true, 'controller' => 'extras', 'action' =>'visualizar', $extra['Extra']['id']), array('class' => 'submit button')); ?>
					<?php echo $html->link('Editar', array($this->params['prefix'] => true, 'controller' => 'extras', 'action' =>'editar', $extra['Extra']['id']), array('class' => 'submit button')); ?>
                                        <?php echo $html->link('Excluir', array($this->params['prefix'] => true, 'controller' => 'extras', 'action' =>'excluir', $extra['Extra']['id']), array('class' => 'submit button')); ?></td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>