<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/datepicker.css">
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/bootstrap/datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    var salvar = $("<button>",{
        type: 'submit',
        class: 'button bg-color-greenDark salvar',
        text: 'Salvar'
    });
    $('.modal-footer').prepend(salvar);
    $(".selectpicker").selectpicker({width:'100%', dropdown_position: "down"});
    $(".selectpicker").change(function(){
        if($('.selectpicker option').filter(':selected').text() == 'Brinde'){
            $('.brinde').fadeIn('slow');
        }else{
            $('#ExtraBrinde').val('');
            $('.brinde').fadeOut('slow');
        }
    });
    $(".selectpicker").change(function(){
        if($('.selectpicker option').filter(':selected').text() == 'Outros'){
            $('.outros').fadeIn('slow');
        }else{
            $('#ExtraOutros').val('');
            $('.outros').fadeOut('slow');
        }
    });
    $(".selectpicker").on('change', function(){
        var itemSelecionado = $('.selectpicker option').filter(':selected').text();
        var itens = ["", "Convite Infantil", "Convite Luxo", "Convite Extra", "Combo" ,"Mesa Extra", "Brinde", "Outros"];
        if($.inArray(itemSelecionado, itens)){
            if(itemSelecionado.substr(0, 13) == "Convite Extra"){
                $('#CheckoutItemQuantidadeConvites').attr('disabled','disabled');
                $('#CheckoutItemQuantidadeMesas').attr('disabled','disabled');
                $('#CheckoutItemQuantidadeMesas').val(0);
                $('#CheckoutItemQuantidadeConvites').val(1);
            }
            if(itemSelecionado.substr(0, 16) == "Convite Infantil"){
                $('#CheckoutItemQuantidadeConvites').attr('disabled','disabled');
                $('#CheckoutItemQuantidadeMesas').attr('disabled','disabled');
                $('#CheckoutItemQuantidadeMesas').val(0);
                $('#CheckoutItemQuantidadeConvites').val(0);
            }
            if(itemSelecionado.substr(0, 12) == "Convite Luxo"){
                $('#CheckoutItemQuantidadeConvites').attr('disabled','disabled');
                $('#CheckoutItemQuantidadeMesas').attr('disabled','disabled');
                $('#CheckoutItemQuantidadeMesas').val(0);
                $('#CheckoutItemQuantidadeConvites').val(0);
            }
            if(itemSelecionado.substr(0, 6) == "Brinde"){
                $('#CheckoutItemQuantidadeConvites').attr('disabled','disabled');
                $('#CheckoutItemQuantidadeMesas').attr('disabled','disabled');
                $('#CheckoutItemQuantidadeMesas').val(0);
                $('#CheckoutItemQuantidadeConvites').val(0);
            }
            if(itemSelecionado.substr(0, 6) == "Outros"){
                $('#CheckoutItemQuantidadeConvites').attr('disabled','disabled');
                $('#CheckoutItemQuantidadeMesas').attr('disabled','disabled');
                $('#CheckoutItemQuantidadeMesas').val(0);
                $('#CheckoutItemQuantidadeConvites').val(0);
            }
            if(itemSelecionado.substr(0, 10) == "Mesa Extra"){
                $('#CheckoutItemQuantidadeConvites').attr('disabled','disabled');
                $('#CheckoutItemQuantidadeMesas').attr('disabled','disabled');
                $('#CheckoutItemQuantidadeMesas').val(1);
                $('#CheckoutItemQuantidadeConvites').val(0);
            }
            if(itemSelecionado.substr(0, 5) == "Combo"){
                $('#CheckoutItemQuantidadeConvites').attr('disabled','disabled');
                $('#CheckoutItemQuantidadeMesas').attr('disabled','disabled');
                $('#CheckoutItemQuantidadeMesas').val(1);
                $('#CheckoutItemQuantidadeConvites').val(10);
                console.log('aqui');
            }
        }else{
            $('#CheckoutItemQuantidadeConvites').removeAttr('disabled');
            $('#CheckoutItemQuantidadeMesas').removeAttr('disabled');
            $('#CheckoutItemQuantidadeMesas').val('');
            $('#CheckoutItemQuantidadeConvites').val('');
        }
    });
    $(".salvar").click(function(e) {
        e.preventDefault();
        $('#CheckoutItemQuantidadeConvites').removeAttr('disabled');
        $('#CheckoutItemQuantidadeMesas').removeAttr('disabled');
        var context = ko.contextFor($(".metro-button.reload")[0]);
        var dados = $("#form").serialize();
        var url = $("#form").attr('action');
        bootbox.hideAll();
        context.$data.showLoading(function() {
            $.ajax({
                url: url,
                data: dados,
                type: "POST",
                dataType: "json",
                complete: function() {
                    context.$data.reload();
                }
            });
        });
    });
});
</script>
<?php $session->flash(); ?>
<div class="row-fluid">
	<?= $form->create('CheckoutItem', array('url' => "/{$this->params['prefix']}/checkout/alterar/" . $this->data['CheckoutItem']['id'], 'id' => 'form')); ?>
        <?php echo $form->input('id', array('hiddenField' => true)); ?>
        <?=$form->hidden('CheckoutItem.turma_id', array('value' => $this->data['CheckoutItem']['turma_id'])); ?>
    <div class="row-fluid">
        <div class="span9">
            <?=$form->input('CheckoutItem.titulo',array(
                'label' => 'Tipo de Extra',
                'type' => 'select',
                'class' => 'selectpicker',
                'options' => $extras,
                'div' => 'input-control text',
                'error' => false,
            )); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span9 hide outros">
                <?php echo $form->input('CheckoutItem.outros', array(
                    'label' => 'Outros',
                    'div' => 'input-control',
                    'error' => false)); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span9 hide brinde">
                <?php echo $form->input('CheckoutItem.brinde', array(
                    'label' => 'Brinde',
                    'div' => 'input-control',
                    'error' => false)); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span3">
                <?php echo $form->input('itens_por_pessoa', array('label' => 'Qtde por Pessoa', 'div' => 'input-control text', 'error' => false)); ?>
        </div>
        <div class="span3">
                <?php echo $form->input('valor', array('label' => 'Valor', 'div' => 'input-control text', 'error' => false)); ?>
        </div>
        <div class="span3">
                <?php echo $form->input('quantidade', array('label' => 'Quantidade Total', 'div' => 'input-control text', 'error' => false)); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span3">
                <?php echo $form->input('quantidade_convites', array('label' => 'Qtde Convites', 'div' => 'input-control text', 'error' => false)); ?>
        </div>
        <div class="span3">
                <?php echo $form->input('quantidade_mesas', array('label' => 'Qtde Mesas', 'div' => 'input-control text', 'error' => false)); ?>
        </div>
    </div>
</div>