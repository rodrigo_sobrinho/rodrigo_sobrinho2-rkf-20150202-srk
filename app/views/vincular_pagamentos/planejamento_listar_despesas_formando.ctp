<?php
echo $form->create('GeraBoleto',array('url' => "/{$this->params['prefix']}/area_financeira/", 'target' => '_blank'));
$proximaDataDoIGPM = $turma['Turma']['data_assinatura_contrato'];
while (strtotime($proximaDataDoIGPM) <= strtotime('now')) {
	$arrayData = explode('-', $proximaDataDoIGPM);
	$arrayData[0]++;
	$proximaDataDoIGPM = implode('-', $arrayData);
}
?>
<script type="text/javascript">
	jQuery(function($) {
		var saldoTotal = 0;
		$(".saldo-parcela:not(.cancelada)").each( function() {
			saldoTotal+=parseFloat($(this).html());
		});
		saldoTotal = saldoTotal.toFixed(2);
		var campoSaldo = $("#saldo-total");
		if(saldoTotal > 0) {
			campoSaldo.prev().css("color","green");
			campoSaldo.prev().html("Saldo Positivo");
		} else if(saldoTotal < 0) {
			campoSaldo.prev().css("color","red");
			campoSaldo.prev().html("Saldo Negativo");
		} else {
			campoSaldo.prev().html("Saldo");
		}
		if(saldoTotal != 0)
			campoSaldo.html("R$" + (saldoTotal < 0 ? saldoTotal*-1 : saldoTotal));
		else
			campoSaldo.html("-");
		var valorPago = 0;
		$(".valor-pago").each(function() {
			valorPago+=parseFloat($(this).html());
		});
		valorPago = valorPago.toFixed(2);
		$("#total-recebido").html("R$"+valorPago);
 		$(".vincular-pagamento").click(function() {
 	 		var dados = $(this).attr("id").split("-");
 	 		var pagamento = dados[1];
 	 		var usuario = dados[3];
 	 		var td = $(this).parent();
 	 		if(<?=sizeof($despesas_adesao)?> > 0) {
 	 	 		var html = "<select name='despesa-" + pagamento + "-para-vincular' style='font-size:11px!important' id='select-despesa-" + pagamento + "'>";
 	 	 		html += "<option value='' selected='selected'>Parcela</option>";
 	 	 		var url = "/<?=$this->params["prefix"]?>/vincular_pagamentos/despesas";
 	 	 		$.ajax({
 	 	 	 		url : url,
 	 	 	 		data : { "usuario" : usuario },
 	 	 	 		type : "POST",
 	 	 	 		dataType : "json",
 	 	 	 		success : function(response) {
	 	 	 	 		$.each(response,function(i,despesa) {
		 	 	 	 		var data = despesa.Despesa.data_vencimento.split("-");
		 	 	 	 		var dataFormatada = data[2]+"/"+data[1]+"/"+data[0];
		 	 	 	 		html += "<option value='" + despesa.Despesa.id + "'>" + dataFormatada + "</option>";
	 	 	 	 		});
		 	 	 	 	html += "</select>&nbsp;&nbsp;";
		 	 	 		html += "<span class='ok-pagamento submit' id='ok-pagamento-" + pagamento + "'>OK</span>";
	 	 	 	 		td.html(html);
	 	 	 	 		$("#div-pagamento-" + pagamento).show("slow");
	 	 	 	 		carregaPagamento();
 	 	 	 		},
 	 	 	 		error : function() {
	 	 	 	 		alert("Erro ao listar parcelas. Tente novamente mais tarde.");
 	 	 	 		}
 	 	 		});
 	 		} else {
 	 	 		alert("Formando sem despesa para vincular o pagamento");
 	 		}
		});
		function carregaPagamento() {
			$(".ok-pagamento").click( function() {
				var td = $(this).parent();
 	 	 		var tr = td.parent();
 	 	 		var select = $(this).prev();
 	 	 		var despesa = select.val();
 	 	 		if(despesa != "") {
	 	 	 		var pagamento = $(this).attr("id").substring(13);
	 	 	 		var load = "<img src='<?=$this->webroot?>img/load.gif' width='15' />";
	 	 	 		$(this).fadeOut(500);
	 	 	 		td.append(load);
	 	 	 		url = "/<?=$this->params["prefix"]?>/vincular_pagamentos/vincular";
	 	 	 		$.ajax({
	 	 	 	 		url : url,
	 	 	 	 		data : { "despesa" : despesa , "pagamento" : pagamento , 'usuario' : '<?=$formando['id']?>' },
	 	 	 	 		type : "POST",
	 	 	 	 		dataType : "json",
	 	 	 	 		success : function(response) {
		 	 	 	 		if(response.erro) {
			 	 	 	 		alert("Erro ao vincular pagamento. Tente novamente");
		 	 	 	 		} else {
		 	 	 	 			$("#despesa-" + despesa + "-status").css('color','green');
			 	 	 	 		$("#despesa-" + despesa + "-status").html('paga');
			 	 	 	 		var html = "<td colspan='7' align='center'>Pagamento vinculado com sucesso</td>";
			 	 	 	 		tr.hide();
			 	 	 	 		tr.html(html);
			 	 	 	 		tr.fadeIn(500);
		 	 	 	 		}
	 	 	 	 		},
	 	 	 	 		error : function() {
		 	 	 	 		alert("Erro ao conectar servidor. Tente novamente mais tarde.");
	 	 	 	 		}
	 	 	 		});
 	 	 		} else {
 	 	 	 		alert("Selecione uma parcela");
 	 	 		}
 	 		});
		}
 	});
</script>
<style type="text/css">
.vincular-pagamento { position:relative }
.despesas-para-vincular { width:100px; position:absolute; top:-15px; left:10px; display:none }
.despesas-para-vincular ul { list-style-type:none; margin:0; padding:0; float:left; position:relative }
.despesas-para-vincular ul li { padding:5px; margin:0; background:#575756; color:white }
.despesas-para-vincular ul li:hover { color:#575756; background:white; }
</style>
<div style="margin:5px 0 5px 0;" >
	<h1 style="display:none; float: left;" id="despesa-do-boleto-selecionado">Despesas do boleto: <b id="despesa-do-boleto-selecionado-numero"></b></h1>
	<?=$html->link('Mostrar todas as despesas', array('url' => '#'), array('id' => 'botao-mostrar-todas-despesas', 'style' => 'float:right; font-size:14px; display:none;')); ?>
</div>
<div style="clear:both;" ></div>
<div style="overflow:hidden; width:100%" class="clearfix">
	<div class="legenda" style="width:50%; margin:10px 0 0 20px; border:1px solid #989999">
		<table>
			<tr>
				<td>
					<table>
						<tr>
							<td colspan="2"><h2>Informa&ccedil;&otilde;es da ades&atilde;o</h2></td>
						</tr>
						<tr height="5">
							<td colspan="2"></td>
						</tr>
						<tr>
							<td width="200">Nome</td>
							<td><?=$formando['nome']?></td>
						</tr>
						<tr>
							<td width="200">C&oacute;digo</td>
							<td><?=$formando['codigo_formando']?></td>
						</tr>
						<tr>
							<td width="200">Data contrato da Turma</td>
							<td><?=date('d/m/Y', strtotime($turmaLogada['Turma']['data_assinatura_contrato']));?></td>
						</tr>
						<tr>
							<td>Data IGPM</td>
							<td><?=date('d/m/Y', strtotime($proximaDataDoIGPM));?></td>
						</tr>
						<tr>
							<td>Mesas do contrato</td>
							<td><?=$turmaLogada['Turma']['mesas_contrato'];?></td>
						</tr>
						<tr>
							<td>Convites do contrato</td>
							<td><?=$turmaLogada['Turma']['convites_contrato'];?></td>
						</tr>
						<tr>
							<td>Valor de Contrato</td>
							<td id='valor-contrato'>R$ <?=number_format($formando['valor_adesao'],2,',','.');?></td>
						</tr>
						<tr>
							<td>Data Adesão</td>
							<td><?=date('d/m/Y', strtotime($formando['data_adesao']));?></td>
						</tr>
						<tr>
							<td>Número de Parcelas do Contrato</td>
							<td><?=$formando['parcelas_adesao'];?></td>
						</tr>
						<?php if($cancelado) { ?>
						<tr>
							<td><div style='background:red; font-weight:bold; color:white; text-align:center; padding:5px'>Contrato Cancelado</div></td>
							<td style='text-align:left!important; padding-left:10px'>
								<?php if(!empty($protocolo)) { ?>
								<?=$html->link('Mais Informações',array($this->params['prefix'] => true, 'controller' => 'solicitacoes', 'action' => 'visualizar',$protocolo),array('class' => 'submit button')); ?>
								<?php } ?>
							</td>
						</tr>
						<?php } ?>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<div class="legenda clearfix" style="width:40%; margin:10px 0 0 10px; border:1px solid #989999">
		<table>
			<tr>
				<td>
					<table>
						<tr>
							<td colspan="2"><h2>Resumo financeiro</h2></td>
						</tr>
						<tr height="5">
							<td colspan="2"></td>
						</tr>
						<tr>
							<td width="200">Total Recebido</td>
							<td id='total-recebido'></td>
						</tr>
						<!--
						<tr>
							<td>Total Despesas a Pagar</td>
							<td>R$ <?=$total_despesas_a_pagar;?></td>
						</tr>
						-->
						<tr>
							<td></td>
							<td id='saldo-total'></td>
						</tr>
						<input type="hidden" name="saldo-total" />
						<?php if($this->params['prefix'] == 'atendimento' && !$cancelado) : ?>
						<tr>
							<td></td>
							<td style="padding-top:10px">
								<?=$html->link('Renegociar Parcelas Em Atraso',array($this->params['prefix'] => true, 'controller' => 'area_financeira', 'action' => 'renegociacao',$formando['id']),array('class' => 'button submit')); ?>
								<br />
								<br />
								<?=$html->link('Cancelar Contrato',array($this->params['prefix'] => true, 'controller' => 'area_financeira', 'action' => 'cancelamento',$formando['id']),array('class' => 'button submit')); ?>
							</td>
						</tr>
						<?php endif; ?>
					</table>
				</td>
			</tr>
		</table>
	</div>
</div>
<div class="grid_full alpha omega first" id="conteudo-container">
	<?php $session->flash(); ?>
	<div>
		<h2><b>Despesas com Ades&atilde;o</b></h2>
	</div>
	<br>
	<?php
	if( sizeof($despesas_adesao) == 0 ) {
		echo '<p style="color:red">Nenhuma despesa com adesão encontrada.</p>';
	} else {
	?>
	<div class="grid_full container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col">Parcela</th>
					<th scope="col">Valor Base</th>
					<th scope="col">Data de Vencimento</th>
					<th scope="col">Valor Creditado</th>
					<th scope="col">Data de Pagamento</th>
					<!--<th scope="col">Correção IGPM</th>-->
					<!--<th scope="col">Status IGPM</th>-->
					<th scope="col">Valor IGPM</th>
					<th scope="col">Status <br>(Valor Base)</th>
					<th scope="col">Saldo da Parcela</th>
					<th scope="col">&nbsp;</th>
				</tr>
			</thead>
			<tfoot>
				<tr class="linha-despesa">
					<td colspan="5"></td>
					<td colspan="2"><?=count($despesas_adesao);?> despesas com ades&atilde;o</td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach ($despesas_adesao as $despesa_adesao): ?>
				<?php
					if($despesa_adesao["Despesa"]["status"] == "aberta") {
						$valorPago = 0;
					} else {
						$valorPago = isset($despesa_adesao['DespesaPagamento'][0]['Pagamento']) ? $despesa_adesao['DespesaPagamento'][0]['Pagamento']['valor_nominal'] : 0;
						$valorPago = $valorPago > $despesa_adesao['Despesa']['valor'] ? $despesa_adesao['Despesa']['valor'] : $valorPago;
					}
				?>
				<tr class="linha-despesa hover<?=$isOdd ? " odd" : ""?>"  id = "linha-despesa-<?=$despesa_adesao['Despesa']['id']; ?>">
				<td  colspan="1" width="5%"><?=$despesa_adesao['Despesa']['parcela'] . ' de ' . $despesa_adesao['Despesa']['total_parcelas']; ?></td>
					<td  colspan="1" width="10%"><span id='valor-despesa-<?=$despesa_adesao['Despesa']['id']?>'><?=$despesa_adesao['Despesa']['valor']; ?></span></td>
					<td valign="center" colspan="1" width="15%"><span id='data-despesa-<?=$despesa_adesao['Despesa']['id']?>'><?=date('d/m/Y', strtotime($despesa_adesao['Despesa']['data_vencimento'])); ?></span></td>
					<span style="display:none" class="valor-pago"><?=$valorPago?></span>
					<td><?=$valorPago > 0 ? "R$$valorPago" : "-"?></td>
					<td  colspan="1" width="10%">
						<?php
							if($despesa_adesao['Despesa']['status'] == 'paga')
								echo date('d/m/Y', strtotime($despesa_adesao['Despesa']['data_pagamento']));	
							else
								echo '-';
						?>
					</td>							
					<!--<td  colspan="1" width="10%"><?=$despesa_adesao['Despesa']['correcao_igpm']; ?></td>-->
					<!--
					<td  colspan="1" width="10%">
						<?php 
							if ($despesa_adesao['Despesa']['status_igpm'] == 'nao_virado')
								echo "-";
							else if ($despesa_adesao['Despesa']['status_igpm'] == 'nao_pago')
								echo "N&atilde;o pago";
							else
								echo "Pago"; 
						?>
					</td>
					-->
					<!-- Quando colocarem o IGPM pra funcionar altera aqui  -->
					<?php
					$valo_igmp = 0; //alterar com a entrada de IGPM
					$saldo_parcela = $valorPago - ($despesa_adesao['Despesa']['valor']+$valo_igmp);
					?>
					<td width="15%">N&atilde;o Aplic&aacute;vel</td>
					<td  colspan="1" width="10%">
						<?php
							if($despesa_adesao['Despesa']['status'] == 'paga' && $saldo_parcela < 0)
								echo "<span style='color:orange;'>Pago Parcialmente</span>";
							else if($despesa_adesao['Despesa']['status'] == 'aberta' || $despesa_adesao['Despesa']['status'] == 'vencida')
								echo "<span style='color:red;'>{$despesa_adesao['Despesa']['status']}</span>";
							else if ($despesa_adesao['Despesa']['status'] == 'paga' )
								echo "<span style='color:green;'>{$despesa_adesao['Despesa']['status']}</span>";
							else 
								echo "<span style='color:orange;'>{$despesa_adesao['Despesa']['status']}</span>";
						?>
					</td>
					<span class='saldo-parcela <?=$despesa_adesao['Despesa']['status']?>' style='display:none'><?=$saldo_parcela?></span>
					<?php
					$color = $saldo_parcela < 0 ? "red" : "green";
					$saldo_parcela = $saldo_parcela < 0 ? $saldo_parcela*-1 : $saldo_parcela;
					?>
					<td width="10%" style="color:<?=$color?>" id='saldo-parcela-<?=$despesa_adesao['Despesa']['id']?>'>
					<?=$saldo_parcela == 0 ? "-" : "R$" . number_format($saldo_parcela,2)?>
					</td>
					<td  colspan="1" width="25%">
						<?php 
							if ($despesa_adesao['Despesa']['status'] != 'paga' &&  $despesa_adesao['Despesa']['status'] != 'cancelada' && !$cancelado) {
								echo "<a href='/{$this->params['prefix']}/area_financeira/gerar_boleto/{$despesa_adesao['Despesa']['id']}' class='submit botao-gerar-boleto' target='_blank'>Gerar Boleto</a>";
								echo "<a href='/{$this->params['prefix']}/area_financeira/gerar_boleto_com_multa/{$despesa_adesao['Despesa']['id']}' class='submit botao-gerar-boleto' target='_blank'>Boleto Com Multa</a>";
							}
							
							if ($despesa_adesao['Despesa']['status_igpm'] == 'nao_pago' &&  $despesa_adesao['Despesa']['status'] == 'paga' && !$cancelado)
								echo "<a href='/{$this->params['prefix']}/area_financeira/gerar_boleto_igpm/{$despesa_adesao['Despesa']['id']}' class='submit botao-gerar-boleto' target='_blank'>Boleto IGPM</a>";
						?>
					 </td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<?php } ?>
	<div class="grid_full alpha omega first" id="div_despesas_extras">
	<h2><b>Despesas com Extras</b></h2>
	<br />
	<?php
	if( sizeof($despesas_extras) == 0 ) {
		echo '<p style="color:red">Nenhuma despesa com extras encontrada.</p>';
	} else {
	?>
	<div class="grid_full container-tabela" style="margin:5px 0 10px 0">
		<table>
			<thead>
				<tr>					
					<th scope="col">Campanha</th>
					<th scope="col">Valor</th>
					<th scope="col">Parcela</th>
					<th scope="col">Data de Vencimento</th>
					<th scope="col">Data de Pagamento</th>
					<th scope="col">Status (Valor base)</th>
				</tr>
			</thead>
			<tfoot>
				<tr class="linha-despesa">
					<td colspan="4">
					</td>
					<td colspan="2"><?=count($despesas_extras);?> despesas com extras</td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach ($despesas_extras as $despesa_extra): ?>
				<tr class="<?=$isOdd ? "odd" : ""?> linha-despesa hover">
					<td  colspan="1" width="25%"><?=$despesa_extra['CampanhasUsuario']['Campanha']['nome']; ?></td>
					<td  colspan="1" width="10%"><?=$despesa_extra['Despesa']['valor']; ?></td>
					<td  colspan="1" width="10%"><?=$despesa_extra['Despesa']['parcela'] . ' de ' . $despesa_extra['Despesa']['total_parcelas']; ?></td>
					<td  colspan="1" width="10%"><?=date('d/m/Y', strtotime($despesa_extra['Despesa']['data_vencimento'])); ?></td>					
					<td  colspan="1" width="10%">
						<?php
							if($despesa_extra['Despesa']['status'] == 'paga') {
								echo date('d/m/Y', strtotime($despesa_extra['Despesa']['data_pagamento']));	
							} else {
								echo '-';
							}
							
						?>
					</td>					
					<td  colspan="1" width="20%">
						<?php 
							if ($despesa_extra['Despesa']['status'] == 'aberta' || $despesa_extra['Despesa']['status'] == 'vencida')
								echo "<span style='color:red;'>";
							else if ($despesa_extra['Despesa']['status'] == 'paga' )
								echo "<span style='color:green;'>";
							else 
								echo "<span style='color:orange;'>";
							echo $despesa_extra['Despesa']['status']; 
							echo "</span>";
						?>
					</td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<?php
	// Final do if que verifica se há despesas com extras 
	}
	?>
	</div>
	<div class="grid_full alpha omega first" id="div_pagamentos_efetuados">
	<h2><b>Pagamentos recebidos não vinculados.</b></h2>
	<?php
	if( count($boletos) == 0 ) {
		echo '<p style="color:red">Nenhum pagamento efetuado foi encontrado.</p>';
	} else {
	?>
	<div class="grid_full container-tabela" style="margin:5px 0 10px 0">
		<table border="0">
			<thead>
				<tr>
					<th scope="col">Tipo</th>
					<th scope="col">No do documento</th>
					<th scope="col">Valor Nominal</th>
					<th scope="col">Valor Recebido</th>
					<th scope="col">Data de Vencimento</th>
					<th scope="col">Data de Liquida&ccedil;&atilde;o</th>
					<th scope="col">Status</th>
					<th scope="col"></th> 
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="4">
					</td>
					<td colspan="2"><?=count($boletos);?> boletos encontrados.</td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach ($boletos as $boleto): ?>
				<tr class="<?=$isOdd ? "odd" : ""?> hover">
					<td  colspan="1" width="15%" style="text-transform:capitalize;"><?=str_replace("_"," ",$boleto['Pagamento']['tipo']); ?></td>
					<td  colspan="1" width="20%"><?=$boleto['Pagamento']['codigo']; ?></td>
					<td  colspan="1" width="10%"><?=$boleto['Pagamento']['valor_nominal']; ?></td>
					<td  colspan="1" width="10%"><?=$boleto['Pagamento']['valor_pago']; ?></td>
					<td  colspan="1" width="15%"><?=date('d/m/Y', strtotime($boleto['Pagamento']['dt_vencimento'])); ?></td>
					<td  colspan="1" width="15%"><?=date('d/m/Y', strtotime($boleto['Pagamento']['dt_liquidacao'])); ?></td>				
					<td  colspan="1"><?=$boleto['Pagamento']['status']; ?></td>
					<td>
						<?php if(sizeof($despesas_adesao) > 0 ) { ?>
						<span class='vincular-pagamento submit' id="<?="pagamento-{$boleto["Pagamento"]["id"]}-usuario-{$formando['id']}"?>">Vincular Pagamento</span>
						<?php } ?>
					</td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>		
	</div>
	<?php } ?>
	</div>
	<!-- Exibir resumo de extras -->
	<br><br>
	<h2><b>Resumo de Extras</b></h2>
	<?php
	if( count($arrayResumoExtras) == 0 ) {
		echo '<p style="color:red">Nenhum pagamento de extra efetuado foi encontrado.</p>';
	} else {
	?>
	<div class="grid_full container-tabela" style="margin:5px 0 10px 0">
		<table>
			<thead>
				<tr>
					<th scope="col">Evento</th>
					<th scope="col">Extra</th>
					<th scope="col">Total Em Aberto</th>
					<th scope="col">Total Pago</th>
					<th scope="col">Total Cancelado</th>
					<th scope="col">Total Retirado</th>
					<th scope="col">A Retirar</th>
					<th scope="col"></th>
				</tr>
			</thead>
			<tbody>
			<?php $isOdd = false; 
			
				foreach($arrayResumoExtras as $extraResumo) :
			?>
				<?php if($isOdd):?>
					<tr class="odd linha-despesa">
				<?php else:?>
					<tr class="linha-despesa">
				<?php endif;?>
					<td  colspan="1" width="20%"><?=$extraResumo['evento_nome']; ?></td>
					<td  colspan="1" width="20%"><?=$extraResumo['extra_nome']; ?></td>
					<td  colspan="1" width="15%"><?=$extraResumo['total_em_aberto']; ?></td>
					<td  colspan="1" width="10%"><?=$extraResumo['total_pago']; ?></td>
					<td  colspan="1" width="10%"><?=$extraResumo['total_cancelado']; ?></td>
					<td  colspan="1" width="15%"><?=$extraResumo['total_retirado']; ?></td>
					<td  colspan="1" width="15%"><?=($extraResumo['total_em_aberto'] + $extraResumo['total_pago']) - $extraResumo['total_retirado']; ?></td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach;?>
			</tbody>
		</table>
	</div>
	<?php } ?>
</div>