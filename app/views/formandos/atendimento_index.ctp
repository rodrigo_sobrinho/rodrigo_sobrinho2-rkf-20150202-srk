<script type="text/javascript">
	jQuery(function($) {
 		$(".dados-financeiros").click(function(e) {
 	 		e.preventDefault();
 	 		var id = $(this).parent().parent().find('.usuario-id').val();
			$(this).colorbox({
 	 			width:jQuery('body').width()*0.8,
 	 			height:"900",
 	 			escKey : false,
 	 			href: "/<?=$this->params['prefix']?>/vincular_pagamentos/listar_despesas_formando/" + id,
 	 			overlayClose : false,
 	 			close : false,
 	 			top : 0
 	 	 	});
		});
 		$("#link-gerar-excel").click(function() {
			$("#form-filtro").attr('target', '_blank');
			$("#form-filtro").attr('action', $("#form-filtro").attr('action').replace('index', 'gerar_excel'));
			$("#form-filtro").submit();
			$("#form-filtro").attr('action', $("#form-filtro").attr('action').replace('gerar_excel', 'index'));
			$("#form-filtro").attr('target', '');
			return false;
		});
 	});
</script>
<style type="text/css">
#cboxLoadedContent { background-color:white!important; border:solid 1px black; border-radius:4px; padding:20px }
#cancelado{color:red!important}
</style>
<span id="conteudo-titulo" class="box-com-titulo-header">Turmas gerenciadas</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	
	<div class="tabela-adicionar-item">
		<?=$form->create(false, array('url' => "/{$this->params['prefix']}/formandos/index", 'class' => 'procurar-form-inline', 'id' => 'form-filtro')); ?>
		<span>Busca: <?=$form->input('filtro-id', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false , 'value' => isset($valor) ? $valor : "")); ?> </span>
		<?=$form->input('filtro-tipo', array('options' => $arrayOptions, 'type' => 'select', 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false , "selected" => $tipo)); ?>
		&nbsp;&nbsp;&nbsp;
		<span>Qtde Por Página:
		<?=$form->input('qtde-por-pagina', array('options' => array("20" => "20" , "50" => "50" , "100" => "100"), 'type' => 'select', "selected" => $limit, 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
		</span>
		&nbsp;
		<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
		<?=$form->end(array('label' => ' ', 'div' => false, 'class' => 'submit-busca')); ?>
		<?php if($turmaLogada) : ?>
		<?php echo $html->link('Gerar excel',array($this->params['prefix'] => true, 'action' => 'index'), array('id' => 'link-gerar-excel')); ?>
		<?php endif;?>
		<div style="clear:both;"></div>
	</div>
    <div class="container-tabela">
    	<table>
            <thead>
                <tr>
                	<th scope="col"><?=$paginator->sort('Turma', 'turma_id'); ?></th>
                    <th scope="col"><?=$paginator->sort('Cód', 'codigo_formando'); ?></th>
                    <th scope="col"><?=$paginator->sort('Nome', 'nome'); ?></th>
                    <th scope="col"><?=$paginator->sort('E-mail', 'email'); ?></th>
                    <th scope="col"><?=$paginator->sort('Curso', 'curso_nome'); ?></th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="5"><?=$paginator->counter(array('format' => 'Itens %start% ao %end% - página %page% de %pages%')); ?>
                        <span class="paginacao">
                            <?=$paginator->numbers(array('separator' => ' ')); ?>
                        </span>
                    </td>
                    <td><?=$paginator->counter(array('format' => 'Total : %count% ')); ?></td>
                </tr>
            </tfoot>
            <tbody>
                <?php $isOdd = false; ?>
                <?php foreach ($formandos as $formando): ?>
                	<tr class="hover<?=$isOdd ? " odd" : ""?>" id="<?=$formando['ViewFormandos']['situacao'] == "cancelado" ? "cancelado" : ""?>">
                		<input type="hidden" class='usuario-id' value="<?=$formando['ViewFormandos']['id']?>" />
                		<td  colspan="1" width="4%"><?=$formando['ViewFormandos']['turma_id']?></td>
                        <td  colspan="1" width="6%"><?=$formando['ViewFormandos']['codigo_formando']?></td>
                        <td  colspan="1" width="22%"><?=$formando['ViewFormandos']['nome']; ?></td>
                        <td  colspan="1" width="22%"><?=$formando['ViewFormandos']['email']; ?></td>
                        <td  colspan="1" width="15%"><?=$formando['ViewFormandos']['curso_nome']; ?></td>
                        <td  colspan="1" width="33%">
							<?php echo $html->link('Cadastro', array($this->params['prefix'] => true, 'controller' => 'usuarios', 'action' =>'editar', $formando['ViewFormandos']['id']), array('class' => 'submit button')); ?>
							<?php echo $html->link('Visualizar', array($this->params['prefix'] => true, 'action' =>'visualizar', $formando['ViewFormandos']['id']), array('class' => 'submit button')); ?>
							<?php echo $html->link('Dados Financeiros', array($this->params['prefix'] => true, 'controller' => 'area_financeira', 'action' =>'atendimento_index', $formando['ViewFormandos']['id']), array('class' => 'submit button dados-financeiros')); ?>
							<?php if($formando['Turma']['checkout'] == 'aberto' && empty($formando['Protocolo']['id'])) : ?>
							<?php echo $html->link('Checkout', array($this->params['prefix'] => true, 'controller' => 'checkout', 'action' =>'iniciar', $formando['ViewFormandos']['id']), array('class' => 'submit button')); ?>
							<?php elseif(!empty($formando['Protocolo']['id'])) : ?>
							<span class="submit button">Checkout Realizado</span>
							<?php endif; ?>
                                                        <?php if($formando['ViewFormandos']['situacao'] == 'cancelado') :?>
                                                        <span class="submit button" color="red">Contrato Cancelado</span>
                                                        <?php endif; ?>
							<span style="display:none"><?=$formando["ViewFormandos"]["id"]?></span>
                        </td>
                    </tr>
                    <?php $isOdd = !($isOdd); ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>