<style type="text/css">
table tbody tr td { border-bottom: solid 1px white!important }
</style>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.selectpicker').selectpicker();
    $.Input();
    $('#content-body').tooltip({ selector: '[rel=tooltip]'});
    $("#form-filtro").submit(function(e) {
        e.preventDefault();
        var context = ko.contextFor($(".metro-button.reload")[0]);
        context.$data.showLoading(function() {
            $("#form-filtro").find('[value="false"]').attr('disabled','disabled');
            var dados = $("#form-filtro").serialize();
            var url = $("#form-filtro").attr('action');
            $.ajax({
                url : url,
                data : dados,
                type : "POST",
                dataType : "json",
                complete : function() {
                    context.$data.reload();
                }
            });
        });
    })
})
</script>
<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: reload"></a>
        Turmas
    </h2>
</div>
<?php
$session->flash();
$sortOptions = array('data-bind' => 'click: loadThis');
$buttonAfter = '<button class="helper" onclick="return false" ' .
    'tabindex="-1" type="button"></button>';
?>
<div class="row-fluid">
    <?=$form->create('Turma',array(
        'url' => "/turmas/listar",
        'id' => 'form-filtro')) ?>
    <div class="row-fluid">
        <div class="span2">
            <?=$form->input('Turma.id',
                array(
                    'label' => 'ID',
                    'div' => 'input-control text',
                    'error' => false,
                    'type' => 'text',
                    'after' => $buttonAfter
                )); ?>
        </div>
        <div class="span2">
            <?=$form->input('Turma.nome',
                array(
                    'label' => 'Nome',
                    'div' => 'input-control text',
                    'error' => false,
                    'type' => 'text',
                    'after' => $buttonAfter
                )); ?>
        </div>
        <div class="span2">
            <?=$form->input('Turma.status',array(
                'options' => $statusExistentesParaTurmas,
                'type' => 'select',
                'class' => 'selectpicker',
                'data-width' => '100%',
                'label' => 'Status',
                'div' => 'input-control text')); ?>
        </div>
        <?php if($permissao) : ?>
        <div class="span3">
            <?=$form->input('Usuario.id',array(
                'options' => $consultores,
                'empty' => 'Consultores',
                'type' => 'select',
                'data-width' => '100%',
                'class' => 'selectpicker',
                'data-live-search' => 'true',
                'label' => 'Consultor',
                'div' => 'input-control text')); ?>
        </div>
        <?php endif; ?>
        <div class="span3">
            <div class="input-control text">
                <label>&nbsp;</label>
                <button type='submit' class='mini max bg-color-red'>
                    Filtrar
                    <i class='icon-search-2'></i>
                </button>
            </div>
        </div>
    </div>
    <?= $form->end(array('label' => false,
        'div' => false, 'class' => 'hide')); ?>
<?php if (sizeof($turmas) > 0) : ?>
    <div class="row-fluid">
        <div class="span2 bg-color-red">
            Descartada
        </div>
        <div class="span2 bg-color-yellow">
            Aberta
        </div>
        <div class="span2 bg-color-greenDark">
            Fechada
        </div>
        <div class="span2 bg-color-greenLight">
            Conclu&iacute;da
        </div>
        <div class="span3 offset1 hide">
            <a href="/<?=$this->params['prefix']?>/turmas/adicionar"
                class="button bg-color-blue pull-right mini"
                data-bind="click:loadThis">
                Nova Turma
                <i class="icon-plus"></i>
            </a>
        </div>
    </div>
    <br />
    <table class="table table-condensed">
        <thead>
            <tr>
                <th scope="col" width="5%">
                    <?=$paginator->sort('ID', 'Turma.id',$sortOptions); ?>
                </th>
                <th scope="col" width="20%">
                    <?=$paginator->sort('Nome', 'Turma.nome',$sortOptions); ?>
                </th>
                <th scope="col" width="15%">Consultores</th>
                <th scope="col" width="10%">
                    <?=$paginator->sort('Ano', 'ano',$sortOptions); ?>
                </th>
                <th scope="col" width="5%">
                    <?=$paginator->sort('Contatos', 'contatos',$sortOptions); ?>
                </th>
                <th scope="col" width="5%">
                    <?=$paginator->sort('Reuniões', 'reunioes',$sortOptions); ?>
                </th>
                <th scope="col" width="15%">
                    <?=$paginator->sort('Ult Contato', 'data_ultimo_contato',$sortOptions); ?>
                </th>
                <th scope="col" width="5%">
                    <?=$paginator->sort('Formandos', 'Turma.expectativa_formandos',$sortOptions); ?>
                </th>
                <th scope="col" width="10%">
                    <?=$paginator->sort('Prev Fechamento', 'data_previsao',$sortOptions); ?>
                </th>
                <th scope="col" width="10%">
                    <?=$paginator->sort('Status', 'Turma.status',$sortOptions); ?>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($turmas as $turma) : $consultores = array(); ?>
            <?php
                foreach($turma['Usuario'] as $us)
                    if($us['grupo'] == 'comercial' && $us['nivel'] == 'basico')
                        $consultores[] = $us['nome']; ?>
            <?php
                if($turma['Turma']['status'] == 'descartada')
                    $class = 'bg-color-red';
                elseif($turma['Turma']['status'] == 'concluida')
                    $class = 'bg-color-greenLight';
                elseif($turma['Turma']['status'] == 'fechada')
                    $class = 'bg-color-greenDark';
                else
                    $class = 'bg-color-yellow fg-color-black';
            ?>
            <tr class="<?=$class?>">
                <td><?=$turma['Turma']['id']; ?></td>
                <td><?=$turma['Turma']['nome']; ?></td>
                <td>
                    <span rel="tooltip"
                        title="<?=implode('<br />',$consultores)?>" data-html="true">
                        <?=implode(', ', array_slice($consultores, 0, 2))?>
                    </span>
                </td>
                <td><?="{$turma['Turma']['ano_formatura']}.{$turma['Turma']['semestre_formatura']}"; ?></td>
                <td><?=$turma[0]['contatos']; ?></td>
                <td><?=$turma[0]['reunioes']; ?></td>
                <td>
                    <?php if(!empty($turma[0]['data_ultimo_contato'])) : ?>
                    <?=date('d/m/Y',strtotime($turma[0]['data_ultimo_contato'])); ?>
                    <?php endif; ?>
                </td>
                <td><?=$turma['Turma']['expectativa_formandos']; ?></td>
                <td><?=(empty($turma[0]['data_previsao']) ? "Não cadastrado" : date('d/m/Y',strtotime($turma[0]['data_previsao']))); ?></td>
                <td style="text-align:center">
                    <a class="button mini visualizar default" type="button"
                        href="/turmas/selecionar/<?=$turma['Turma']['id']; ?>">
                        Selecionar
                    </a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="6" class="paginacao">
                    <?=$paginator->numbers(array('separator' => ' ',
                        'data-bind' => 'click: loadThis')); ?>
                </td>
                <td colspan="4">
                    <span class="label label-info">
                    <?=$paginator->counter(array(
                        'format' => 'Total : %count% ' .  $this->name)); ?>
                    </span>
                </td>
            </tr>
        </tfoot>
    </table>
<?php else : ?>
    <h2 class="fg-color-red">Nenhuma Turma Encontrada</h2>
<?php endif; ?>
</div>
