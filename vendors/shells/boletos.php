<?php

class BoletosShell extends Shell {

    var $uses = array('ViewFormandos','Usuario','Turma','Despesa','Item','Mensagem','Assunto');
    var $helpers = array('Html');
    
    function initialize() {
        parent::initialize();
        App::import('Component', 'BoletoConta');
        App::import('Component', 'Mensagens');
        $this->BoletoConta = new BoletoContaComponent();
        $this->MensagensComponent = new MensagensComponent();
        $this->diretorioBoletos = APP ."webroot/upload/boletos";
        App::import('Component', 'mail');
        $this->Mailer = new Mail();
    }
    
    private function criarDiretorio($dir) {
        if(!is_dir($dir))
            return mkdir($dir);
        else
            return true;
    }
    
    private function criarDiretorioBoletos($turmaId,$codigo_formando) {
        if(!$this->criarDiretorio($this->diretorioBoletos))
            return false;
        else if(!$this->criarDiretorio("{$this->diretorioBoletos}/{$turmaId}"))
            return false;
        else if(!$this->criarDiretorio("{$this->diretorioBoletos}/{$turmaId}/{$codigo_formando}"))
            return false;
        else
            return "{$this->diretorioBoletos}/{$turmaId}/{$codigo_formando}";
    }
    
    function despesa($usuario,$turma,$despesa) {
        $diretorio = $this->criarDiretorioBoletos($turma['Turma']['id'],$usuario['ViewFormandos']['codigo_formando']);
        if($diretorio !== false) {
            ob_start();
            $this->BoletoConta->dadosboleto["nosso_numero"] = "";
            $this->BoletoConta->iniciar($despesa['Despesa']['id'], $usuario, $turma);
            $this->BoletoConta->gerarBoleto("http://sistema.asformaturas.com.br/");
            $boleto = ob_get_contents();
            file_put_contents("{$diretorio}/parcela_{$despesa['Despesa']['parcela']}_".date('Y_m_d').".html", $boleto);
            ob_end_clean();
        }
    }
    
    function usuario($usuario = false) {
        if(!$usuario) {
            $usuarioId = isset($this->args[0]) ? $this->args[0] : false;
            if(!$usuarioId) exit();
            $this->Usuario->unbindModel(array(
                'hasAndBelongsToMany' => array('Campanhas','Turma'),
                'hasMany' => array('UsuarioConta','FormandoFotoTelao','Cupom')
            ),false);
            $usuario = $this->Usuario->read(null,$usuarioId);
        }
        $this->Turma->unbindModelAll();
        if($usuario) {
            $turma = $this->Turma->read(null,$usuario['ViewFormandos']['turma_id']);
            if(!$turma) exit();
            foreach($usuario['Despesa'] as $d) {
                $despesa = array('Despesa' => $d);
                $this->despesa($usuario, $turma, $despesa);
            }
        }
    }
    
    function turma() {
        $turmaId = isset($this->args[0]) ? $this->args[0] : false;
        if(!$turmaId) exit();
        $usuarios = $this->ViewFormandos->find('all',array(
            'conditions' => array(
                'turma_id' => $turmaId,
                'ativo' => 1,
                "right(codigo_formando,3) <> '000'"
            )
        ));
        $this->Usuario->unbindModel(array(
            'hasAndBelongsToMany' => array('Campanhas','Turma'),
            'hasMany' => array('UsuarioConta','FormandoFotoTelao','Cupom')
        ),false);
        foreach($usuarios as $u) {
            $usuario = $this->Usuario->read(null,$u['ViewFormandos']['id']);
            $this->usuario($usuario);
        }
    }
    
    function enviarMensagemDeAtraso() {
        $this->Despesa->unbindModelAll();
        $this->Despesa->bindModel(array(
            'belongsTo' => array(
                'ViewFormandos' => array(
                    'className' => 'ViewFormandos',
                    'foreignKey' => false,
                    'conditions' => array("Despesa.usuario_id = ViewFormandos.id")
                )
            )
        ),false);
        $despesas = $this->Despesa->find('all',array(
            'conditions' => array(
                'Despesa.status' => 'aberta',
                "date_format(date_add(data_vencimento,interval 25 day),'%Y%m%d') = date_format(now(),'%Y%m%d')",
                'ViewFormandos.ativo' => 1,
                "ViewFormandos.situacao" => 'ativo'
            )
        ));
        if(!$despesas)
            exit();
        $links = array(
            "pagamento" => "{$this->Mailer->urlSite}/area_financeira/relatar_pagamento/",
            "segunda_via" => "{$this->Mailer->urlSite}/boletos/gerar_com_multa/",
            "contato" => "{$this->Mailer->urlSite}/mensagens/email",
        );
        $texto = "Caso você já tenha retirado seu convite, desconsidere este email.<br /><br />" .
            "Prezado {{formando}},<br /><br />" .
            "Não consta em nosso sistema o pagamento da parcela no valor de R\${{valor}} ".
            "referente ao dia {{data}}.<br /><br />".
            "- Caso tenha efetuado esse pagamento, favor desconsiderar este e-mail ".
            "- No caso de duvidas, entre em contato conosco no telefone (11)5585 - 1294.<br /><br />" .
            "- Caso você queira emitir a segunda via, ".
            "<a href='{{link_login}}/{{link_segunda_via}}'>clique aqui</a>.<br /><br />" .
            "- Caso você queira renegociar a forma de pagamento, entre em " .
            "contato conosco <a href='{{link_login}}/{{link_contato}}'>clicando aqui</a>.<br /><br />" .
            "Desde já agradecemos a atenção e nos colocamos a disposição." .
            "<br /><br />Atenciosamente";
        $this->Mailer->IsHTML(true);
        $item = $this->Item->get(array(
            'nome' => "Fale Conosco",
            'grupo' => 'atendimento'
        ),array(
            'nome' => "Fale Conosco",
            'grupo' => 'atendimento'
        ));
        if(!$item)
            exit();
        foreach($despesas as $despesa) {
            $mensagem = array(
                'Mensagem' => array(
                    'usuario_id' => $despesa['ViewFormandos']['id'], 
                    
                )
            );
            $turma = $this->Turma->read(null,$despesa['ViewFormandos']['turma_id']);
            $atendente = $this->Usuario->obterAtendenteResponsavel($despesa['ViewFormandos']['id']);
            if(!$turma)
                continue;
            elseif($turma['Turma']['status'] != "fechada" || $turma['Turma']['checkout'] != "fechado")
                continue;
            $assunto = $this->Assunto->get(array(
                'item_id' => $item['Item']['id'],
                'turma_id' => $turma['Turma']['id'],
                'Assunto.nome' => "Pagamentos"
            ),array(
                'item_id' => $item['Item']['id'],
                'turma_id' => $turma['Turma']['id'],
                'nome' => "Pagamentos",
                'pendencia' => 'As'
            ));
            if(!$assunto)
                continue;
            $mensagem = str_replace("{{formando}}", $despesa['ViewFormandos']['nome'], $texto);
            $mensagem = str_replace("{{valor}}", number_format($despesa['Despesa']['valor'], 2, ',', '.'), $mensagem);
            $mensagem = str_replace("{{data}}", date('d/m',strtotime($despesa['Despesa']['data_vencimento'])), $mensagem);
            $mensagem = str_replace("{{link_pagamento}}",
                    base64_encode("{$links['pagamento']}{$despesa['Despesa']['id']}"),
                    $mensagem);
            $mensagem = str_replace("{{link_segunda_via}}",
                    base64_encode("{$links['segunda_via']}{$despesa['Despesa']['id']}"),
                    $mensagem);
            $mensagem = str_replace("{{link_contato}}",
                    base64_encode("{$links['contato']}") . "/1",
                    $mensagem);
            $mensagem = str_replace("{{link_login}}",
                    str_replace("http://","",$this->Usuario->urlAcessoDeslogado($despesa['ViewFormandos']['id'])), $mensagem);
            $data = array(
                "Mensagem" => array(
                    'turma_id' => $turma['Turma']['id'],
                    'assunto_id' => $assunto['Assunto']['id'],
                    'usuario_id' => $atendente['Usuario']['id'],
                    'data' => date('Y-m-d H:i:s'),
                    'texto' => $mensagem,
                    'pendente' => 0
                )
            );
            $result = $this->MensagensComponent->inserir($data);
            if(!$result['erro']) {
                $this->MensagensComponent->alocarDestinatarios(array(
                    array(
                        'Usuario' => array(
                            'id' => $despesa['ViewFormandos']['id']
                        )
                    ),
                    array(
                        'Usuario' => array(
                            'id' => $atendente['Usuario']['id']
                        )
                    ),
                ),array(
                    'Mensagem' => array(
                        'id' => $result['mensagem_id']
                    ),
                    'Usuario' => array(
                        'id' => $atendente['Usuario']['id']
                    )
                ));
                $this->Mailer->AddAddress($despesa['ViewFormandos']['email'],$despesa['ViewFormandos']['nome']);
                $this->Mailer->FromName = utf8_decode("Sistema AS Formaturas");
                $this->Mailer->Subject = utf8_decode("Boleto em atraso");
                $this->Mailer->Body = utf8_decode($mensagem);
                $this->Mailer->Send();
                $this->Mailer->ClearAddresses();
            }
        }
    }
}