<span id="conteudo-titulo" class="box-com-titulo-header">Turmas - Editar</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<?php echo $form->create('Turma', array('url' => "/{$this->params['prefix']}/turmas/editar_registro_interno")); ?>
		<?php include('_form_registro.ctp'); ?>
	
	<p class="grid_6 alpha omega">
		<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'controller' => 'registros_internos') ,array('class' => 'cancel')); ?>
		<?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit'));?>
	</p>
</div>