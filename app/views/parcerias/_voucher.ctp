<style type="text/css">
    .voucher { width:720px; min-height:286px; max-height:386px; position:relative; margin:0 auto; margin-top:100px; border:dashed 2px #A8A8A8; font-size:12px; color:#333333 }
    .voucher p { text-indent:0 }
    .voucher strong,h2,h1 { color:#D71920; padding:0; margin:0; text-indent:0 }
    .voucher h2 { font-size:19px; margin-bottom:8px }
    .voucher h1 { font-size:22px; font-weight:bold }
    .voucher strong { font-size:14px }
    .voucher .info { width:260px; min-height:231px; max-height:331px; float:left; position:relative; margin-top:5px; padding:10px; border-right:solid 2px #A8A8A8 }
    .voucher .info .logoas { width:260px; margin-bottom:10px; float:left }
    .voucher .info .aluno { width:100%; float:left; margin-top:5px }
    .voucher .info .aluno .img { border:solid 4px #D71920; float:left; margin-right:10px; width:70px; overflow:hidden }
    .voucher .info .aluno .img img { float:left; }
    .voucher .info .validade { position:absolute; bottom:10px; left:10px; font-size:11px; width:100%; text-indent:0 }
    .voucher .parceria { width:390px; min-height:231px; max-height:331px; position:relative; float:left; padding:10px; margin-top:5px }
    .voucher .parceria .logoparceiro { position:absolute; bottom:5px; right:10px }
    .voucher .parceria .logoparceiro img { width:120px }
    .voucher .institucional { width:100%; float:left; padding:0 5px 5px 5px; font-weight:bold }
</style>
<div class="voucher clearfix">
    <div class="info clearfix">
        <img src="<?= $this->webroot ?>img/logo_voucher.png" class="logoas" />
        <div class="aluno">
            <span class='img'>
                <img width='70' src="<?= $foto_formando ?>" />
            </span>
            <h2><?= $usuario['Usuario']['nome'] ?></h2>
            <p>
                <strong>ID: </strong><?= $codigoFormando ?>
                <br />
                <strong>RG: </strong><?= $formando['FormandoProfile']['rg'] ?>
            </p>
        </div>
        <?php if ($parceria['Parceria']['data_fim'] != "") : ?>
            <div class="validade">
                Este cupom &eacute; v&aacute;lido at&eacute; 30 dias ap&oacute;s a<br />
                data de t&eacute;rmino da promo&ccedil;&atilde;o
            </div>
        <?php endif; ?>
    </div>
    <div class="parceria">
        <h1><?= $parceria['Parceiro']['nome'] ?></h1>
        <h2><?= $parceria['Parceria']['titulo'] ?></h2>
        <?= $parceria['Parceria']['descricao'] ?>
        <br />
        <br />
        <?php if ($parceria['Parceria']['observacoes'] != "") { ?>
            <strong>Observa&ccedil;&otilde;es</strong> 
            <?= $parceria['Parceria']['observacoes'] ?>
            <br />
        <?php } ?>
        <?= $parceria['Parceria']['valor'] != '' ? "<strong>R\${$parceria['Parceria']['valor']}</strong>" : "" ?>
        <br />
        <?php if ($parceria['Parceria']['desconto'] != '') : ?>
            Desconto de <strong style="color:#333333"><?= $parceria['Parceria']['desconto'] ?></strong>
        <?php endif; ?>
        <br />
        <br />
        <strong>In&iacute;cio</strong> <?= date('d/m/Y', strtotime($parceria['Parceria']['data_inicio'])) ?>
        <?php if ($parceria['Parceria']['data_fim'] != "") : ?>
            <br />
            <strong>T&eacute;rmino</strong> <?= date('d/m/Y', strtotime($parceria['Parceria']['data_fim'])) ?>
        <?php endif; ?>
        <br />
        <br />
        <p style="width:250px"><strong>Local</strong> <?= $parceria['Parceria']['local'] ?></p>
        <span class="logoparceiro">
            <img src="<?= $this->webroot ?>img/parceiros/logos/<?= $parceria['Parceiro']['logo'] ?>" />
        </span>
    </div>
    <div class="institucional">
        Av. Senador Casemiro da Rocha, 1.000 <strong>|</strong> 
        Sa&uacute;de <strong>|</strong> 
        S&atilde;o Paulo - SP <strong>|</strong> 
        (11) 5585-1294 <strong>|</strong> 
        www.formaturas.as <strong>|</strong> 
        contato@eventos.as
    </div>
</div>