<?php

class ParceirosController extends AppController {

    var $name = 'Parceiros';
    var $caminho_raiz_logos = 'parceiros/logos';
    var $caminho_raiz_fotos = 'parceiros/fotos';
    var $paginate = array(
        'limit' => 25,
        'order' => array(
            'Parceiro.nome' => 'asc'
        )
    );
    
    function marketing_listar() {
        if(isset($this->data["filtro"])) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->Session->write("filtros.marketing.parceiros",
                $this->data["filtro"]);
        } else {
            if($this->Session->check("filtros.marketing.parceiros")) {
                $filtro = strtolower($this->Session->read("filtros.marketing.parceiros"));
                $this->set('filtro', $this->Session->read("filtros.marketing.parceiros"));
                $this->paginate['Parceiro'] = array(
                    "conditions" => array(
                        "lower(Parceiro.nome) like('%{$filtro}%')"
                    )
                );
            }
            $parceiros = $this->paginate('Parceiro');
            $this->set('parceiros', $parceiros);
        }
    }
    
    function marketing_editar($id = false) {
        if($id)
            $parceiro = $this->Parceiro->read(null,$id);
        else
            $parceiro = false;
        if($this->data) {
            $this->autoRender = false;
            Configure::write("debug",0);
            $data = $this->data;
            $this->loadModel("Arquivo");
            if(isset($data['Parceiro']["logo_src"])) {
                $logo = String::uuid().$this->Parceiro->obterExtensaoDoArquivo($data['Parceiro']["logo_name"]);
                $path = WWW_ROOT . "img/parceiros/logos/";
                if($this->Arquivo->saveBase64File($data['Parceiro']['logo_src'], $path.$logo))
                    $data["Parceiro"]["logo"] = $logo;
            }
            if($this->Parceiro->save($data)) {
                $parceiroId = !empty($data["Parceiro"]["id"]) ? $data["Parceiro"]["id"] : $this->Parceiro->getLastInsertId();
                if(isset($data["fotos"])) {
                    $this->loadModel("FotoParceiro");
                    $path = WWW_ROOT . "img/parceiros/fotos/{$parceiroId}/";
                    $c = true;
                    if(!is_dir($path))
                        $c = mkdir($path);
                    if($c)
                        foreach($data["fotos"] as $foto) {
                            $nome = String::uuid().$this->Parceiro->obterExtensaoDoArquivo($foto["name"]);
                            if($this->Arquivo->saveBase64File($foto['data'], $path.$nome)) {
                                $this->FotoParceiro->create();
                                $this->FotoParceiro->save(array(
                                    "FotoParceiro" => array(
                                        "parceiro_id" => $parceiroId,
                                        "nome" => $nome
                                    )
                                ));
                            }
                        }
                }
                $this->Session->setFlash("Dados inseridos com sucesso", 'metro/flash/success');
            } else {
                $this->Session->setFlash("Erro ao enviar dados", 'metro/flash/error');
            }
            echo json_encode(array());
        } else {
            $this->data = $parceiro;
            $this->set("parceiro",$parceiro);
        }
    }

    function marketing_index() {
        $parceiros = $this->paginate('Parceiro');
        $this->set('parceiros', $parceiros);
    }

    function marketing_exibir($id = null) {



        $parceiro = $this->Parceiro->find(array('Parceiro.id' => $id));

        if (!$parceiro) {

            $this->Session->setFlash('Parceiro não existente.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/parceiros");
        } else {

            $site_completo = $parceiro['Parceiro']['site'];

            if (stristr($site_completo, 'http://') === false) {
                $site_completo = 'http://' . $site_completo;
            }

            $this->set('parceiro', $parceiro);
            $this->set('caminho_raiz_logos', $this->caminho_raiz_logos);
            $this->set('caminho_raiz_fotos', $this->caminho_raiz_fotos);
            $this->set('site_completo', $site_completo);
        }
    }

    function marketing_alterar($id = null) {
        $this->Parceiro->id = $id;

        $parceiro_banco = $this->Parceiro->find('first', array('Parceiro.id' => $id));

        $this->set('parceiro_banco', $parceiro_banco);
        $this->set('caminho_raiz_logos', $this->caminho_raiz_logos);
        $this->set('caminho_raiz_fotos', $this->caminho_raiz_fotos);

        if (empty($this->data)) {

            $this->data = $this->Parceiro->read();
        } else {

            $parceiro = $this->data;

            $id_arquivos = $this->Parceiro->read('Parceiro.id_arquivos', $parceiro['Parceiro']['id']);
            $id_arquivos = $id_arquivos['Parceiro']['id_arquivos'];

            $logo_salvo = array();
            $fotos_salvas = array();

            if ($this->Parceiro->validates()) {

                if (empty($parceiro['Logo']['logo'])) {
                    unset($parceiro['Logo']);
                } else {
                    $logo_salvo = $this->uploadFiles('img/' . $this->caminho_raiz_logos, $parceiro['Logo'], null, $id_arquivos);

                    if (array_key_exists('urls', $logo_salvo)) {
                        $componentes_caminho = explode("/", $logo_salvo['urls'][0]);
                        $parceiro['Parceiro']['logo'] = $componentes_caminho[count($componentes_caminho) - 1];
                    }
                }

                if (!empty($parceiro['Fotos'])) {
                    $fotos_salvas = $this->uploadFiles('img/' . $this->caminho_raiz_fotos, $parceiro['Fotos'], $id_arquivos);

                    if (array_key_exists('urls', $fotos_salvas)) {
                        for ($i = 0; $i < count($fotos_salvas['urls']); $i++) {
                            $componentes_caminho_foto = explode('/', $fotos_salvas['urls'][$i]);
                            $pasta_arquivo = array_slice($componentes_caminho_foto, -2);
                            $caminho_final = implode('/', $pasta_arquivo);

                            $parceiro['FotoParceiro'][$i]['nome'] = $caminho_final;
                        }
                    }
                }
            }

            $msg_erro = "Ocorreu um erro ao salvar o Parceiro.";
            $erro_nos_arquivos = false;

            if (array_key_exists('errors', $logo_salvo)) {
                $erro_nos_arquivos = true;
                $msg_erro = "Ocorreu um erro ao salvar o logo do Parceiro.";
            }

            if (array_key_exists('errors', $fotos_salvas)) {
                $erro_nos_arquivos = true;
                $msg_erro = "Ocorreu um erro ao salvar as fotos do Parceiro.";
            }

            if ($this->Parceiro->saveAll($parceiro) && !$erro_nos_arquivos) {
                $this->Session->setFlash('O parceiro foi salvo corretamente', 'flash_sucesso');
                $this->redirect("/{$this->params['prefix']}/parceiros/visualizar/{$this->Parceiro->id}");
            } else {
                $this->Session->setFlash($msg_erro, 'flash_erro');
            }
        }

        if (!$this->data) {
            $this->Session->setFlash('Parceiro não existente.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/parceiros");
        }
    }

    function marketing_visualizar($id = null) {



        $parceiro = $this->Parceiro->find(array('Parceiro.id' => $id));

        if (!$parceiro) {

            $this->Session->setFlash('Parceiro não existente.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/parceiros");
        } else {

            $site_completo = $parceiro['Parceiro']['site'];

            if (stristr($site_completo, 'http://') === false) {
                $site_completo = 'http://' . $site_completo;
            }

            $this->set('parceiro', $parceiro);
            $this->set('caminho_raiz_logos', $this->caminho_raiz_logos);
            $this->set('caminho_raiz_fotos', $this->caminho_raiz_fotos);
            $this->set('site_completo', $site_completo);
        }
    }

    function marketing_adicionar() {
        $this->layout = false;
        if (!empty($this->data)) {

            $parceiro = $this->data;
            $this->Parceiro->set($parceiro);

            $id_arquivos = md5(date('YmdHisu'));
            $parceiro['Parceiro']['id_arquivos'] = $id_arquivos;

            $logo_salvo = array();
            $fotos_salvas = array();

            if ($this->Parceiro->validates()) {

                $logo_salvo = $this->uploadFiles('img/' . $this->caminho_raiz_logos, $parceiro['Logo'], null, $id_arquivos);

                if (array_key_exists('urls', $logo_salvo)) {
                    $componentes_caminho_logo = explode("/", $logo_salvo['urls'][0]);
                    $parceiro['Parceiro']['logo'] = $componentes_caminho_logo[count($componentes_caminho_logo) - 1];
                }

                $fotos_salvas = $this->uploadFiles('img/' . $this->caminho_raiz_fotos, $parceiro['Fotos'], $id_arquivos);

                if (array_key_exists('urls', $fotos_salvas)) {
                    for ($i = 0; $i < count($fotos_salvas['urls']); $i++) {
                        $componentes_caminho_foto = explode('/', $fotos_salvas['urls'][$i]);
                        $pasta_arquivo = array_slice($componentes_caminho_foto, -2);
                        $caminho_final = implode('/', $pasta_arquivo);

                        $parceiro['FotoParceiro'][$i]['nome'] = $caminho_final;
                    }
                }
            }

            $msg_erro = "Ocorreu um erro ao salvar o Parceiro.";
            $erro_nos_arquivos = false;

            if (array_key_exists('errors', $logo_salvo)) {
// 				$this->Parceiro->validationErrors['Logo']['logo'] = $logo_salvo['errors'][0];
                $erro_nos_arquivos = true;
                $msg_erro = "Ocorreu um erro ao salvar o logo do Parceiro.";
            }

            if (array_key_exists('errors', $fotos_salvas)) {
// 				$this->Parceiro->validationErrors['Fotos']['foto0'] = $fotos_salvas['errors'][0];
                $erro_nos_arquivos = true;
                $msg_erro = "Ocorreu um erro ao salvar as fotos do Parceiro.";
            }

            if ($this->Parceiro->saveAll($parceiro) && !$erro_nos_arquivos) {
                $this->Session->setFlash('Parceiro salvo corretamente.', 'flash_sucesso');
                $this->redirect("/{$this->params['prefix']}/parceiros/visualizar/{$this->Parceiro->id}");
            } else {
                $this->Session->setFlash($msg_erro, 'flash_erro');
            }
        }
    }

    function marketing_editar_($id = null) {
        $this->Parceiro->id = $id;

        $parceiro_banco = $this->Parceiro->find('first', array('Parceiro.id' => $id));

        $this->set('parceiro_banco', $parceiro_banco);
        $this->set('caminho_raiz_logos', $this->caminho_raiz_logos);
        $this->set('caminho_raiz_fotos', $this->caminho_raiz_fotos);

        if (empty($this->data)) {

            $this->data = $this->Parceiro->read();
        } else {

            $parceiro = $this->data;

            $id_arquivos = $this->Parceiro->read('Parceiro.id_arquivos', $parceiro['Parceiro']['id']);
            $id_arquivos = $id_arquivos['Parceiro']['id_arquivos'];

            $logo_salvo = array();
            $fotos_salvas = array();

            if ($this->Parceiro->validates()) {

                if (empty($parceiro['Logo']['logo'])) {
                    unset($parceiro['Logo']);
                } else {
                    $logo_salvo = $this->uploadFiles('img/' . $this->caminho_raiz_logos, $parceiro['Logo'], null, $id_arquivos);

                    if (array_key_exists('urls', $logo_salvo)) {
                        $componentes_caminho = explode("/", $logo_salvo['urls'][0]);
                        $parceiro['Parceiro']['logo'] = $componentes_caminho[count($componentes_caminho) - 1];
                    }
                }

                if (!empty($parceiro['Fotos'])) {
                    $fotos_salvas = $this->uploadFiles('img/' . $this->caminho_raiz_fotos, $parceiro['Fotos'], $id_arquivos);

                    if (array_key_exists('urls', $fotos_salvas)) {
                        for ($i = 0; $i < count($fotos_salvas['urls']); $i++) {
                            $componentes_caminho_foto = explode('/', $fotos_salvas['urls'][$i]);
                            $pasta_arquivo = array_slice($componentes_caminho_foto, -2);
                            $caminho_final = implode('/', $pasta_arquivo);

                            $parceiro['FotoParceiro'][$i]['nome'] = $caminho_final;
                        }
                    }
                }
            }

            $msg_erro = "Ocorreu um erro ao salvar o Parceiro.";
            $erro_nos_arquivos = false;

            if (array_key_exists('errors', $logo_salvo)) {
                $erro_nos_arquivos = true;
                $msg_erro = "Ocorreu um erro ao salvar o logo do Parceiro.";
            }

            if (array_key_exists('errors', $fotos_salvas)) {
                $erro_nos_arquivos = true;
                $msg_erro = "Ocorreu um erro ao salvar as fotos do Parceiro.";
            }

            if ($this->Parceiro->saveAll($parceiro) && !$erro_nos_arquivos) {
                $this->Session->setFlash('O parceiro foi salvo corretamente', 'flash_sucesso');
                $this->redirect("/{$this->params['prefix']}/parceiros/visualizar/{$this->Parceiro->id}");
            } else {
                $this->Session->setFlash($msg_erro, 'flash_erro');
            }
        }

        if (!$this->data) {
            $this->Session->setFlash('Parceiro não existente.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/parceiros");
        }
    }

    function marketing_desativar($id = null) {
        if ($id) {
            if ($this->Parceiro->updateAll(array("Parceiro.ativo" => 0), array("Parceiro.id" => $id)))
                $this->Session->setFlash('Parceiro desativado com sucesso.', 'flash_sucesso');
            else
                $this->Session->setFlash('Erro ao desativar parceiro.', 'flash_erro');
        } else {
            $this->Session->setFlash('Parceiro não encontrado', 'flash_erro');
        }
        $this->redirect("/{$this->params['prefix']}/parceiros");
    }

    function marketing_ativar($id = null) {
        if ($id) {
            if ($this->Parceiro->updateAll(array("Parceiro.ativo" => 1), array("Parceiro.id" => $id)))
                $this->Session->setFlash('Parceiro ativado com sucesso.', 'flash_sucesso');
            else
                $this->Session->setFlash('Erro ao ativar parceiro.', 'flash_erro');
        } else {
            $this->Session->setFlash('Parceiro não encontrado', 'flash_erro');
        }
        $this->redirect("/{$this->params['prefix']}/parceiros");
    }

    function marketing_apagar_foto($id = null) {
        Configure::write('debug', 0);
        $this->layout = 'ajax';

        $id = $this->data['id'];

        if ($id && $this->Parceiro->FotoParceiro->delete($id)) {
            $this->set('resposta', 'sucesso');
        } else {
            $this->set('resposta', 'erro');
        }

        $this->render('apagar_foto');
    }

    function marketing_apagar_logo($id = null) {
        Configure::write('debug', 0);
        $this->layout = 'ajax';

        $id = $this->data['id'];
        $parceiro = $this->Parceiro->find('first', array('Parceiro.id' => $id));

        $parceiro['Parceiro']['logo'] = "";

        if ($id && $this->Parceiro->save($parceiro)) {
            $this->set('resposta', 'sucesso');
        } else {
            $this->set('resposta', 'erro');
        }

        $this->render('apagar_logo');
    }

}
