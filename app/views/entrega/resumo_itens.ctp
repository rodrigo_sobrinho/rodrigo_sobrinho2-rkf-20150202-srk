<style type="text/css">
h2.titulo { background-color:#C7C5C6; color:black; padding:5px }
.container-parcerias { vertical-align: top!important }
</style>
<script type="text/javascript">
$(document).ready(function() {
	$("#confirmar-entrega").click(function() {
		if($("#codigo-formando").val() == "") {
			alert('Digite o cod. do formando');
		} else {
			var url = "/<?=$this->params["prefix"]?>/entrega/confirmar";
			$("#response").text("Aguarde...");
			setTimeout(function() {
				$.ajax({
					type : "POST",
					dataType : "json",
					url : url,
					data : { codigoFormando : $("#codigo-formando").val() },
					async : false,
					success : function(response) {
						html = "";
						if(response.formando != undefined)
							html+= "Formando " + response.formando.nome + "<br /><br />";
						if(response.error) {
							$.each(response.message,function(i,message) {
								html+= message + "<br />";
							});
						} else {
							if(response.itens.contrato == undefined) {
								html+= "Erro ao recuperar itens de contrato. tente novamente<br />";
							} else if(response.itens.contrato.message != undefined) {
								html+= response.itens.contrato.message.toString() + "<br />";
							} else {
								convites = response.itens.contrato.convites;
								mesas = response.itens.contrato.mesas;
								$(".contrato").find('.convites').find('.retirada').html(
									parseInt($(".contrato").find('.convites').find('.retirada').html()) + parseInt(convites)
								);
								$(".contrato").find('.convites').find('.a-retirar').html(
									parseInt($(".contrato").find('.convites').find('.a-retirar').html()) - parseInt(convites)
								);
								$(".contrato").find('.mesas').find('.retirada').html(
									parseInt($(".contrato").find('.mesas').find('.retirada').html()) + parseInt(mesas)
								);
								$(".contrato").find('.mesas').find('.a-retirar').html(
									parseInt($(".contrato").find('.mesas').find('.a-retirar').html()) - parseInt(mesas)
								);
								html+= "Itens de contrato<br />" + response.itens.contrato.mesas + " Mesas<br />";
								html+= response.itens.contrato.convites + " Convites<br /><br />";
							}
							if(response.itens.extras == undefined) {
								html+= "Erro ao recuperar itens de campanhas. tente novamente<br />";
							} else if(response.itens.extras.message != undefined) {
								html+= response.itens.extras.message +"<br />";
							} else {
								html+= "Itens de campanhas<br />";
								console.log(response.itens);
								$.each(response.itens.extras,function(i,extra) {
									quantidade = extra.quantidade != undefined ? parseInt(extra.quantidade) : 0;
									html+= quantidade + " " + extra.titulo + "<br />";
									$(".extras").find('.extra-'+i).find('.retirada').html(
										parseInt($(".extras").find('.extra-'+i).find('.retirada').html()) + quantidade
									);
									$(".extras").find('.extra-'+i).find('.a-retirar').html(
										parseInt($(".extras").find('.extra-'+i).find('.a-retirar').html()) - quantidade
									);
								});
								html+= "<br />";
							}
							if(response.itens.checkout == undefined) {
								html+= "Erro ao recuperar itens de checokut. tente novamente<br />";
							} else if(response.itens.checkout.message != undefined) {
								html+= response.itens.checkout.message +"<br />";
							} else {
								html+= "Itens de checkout<br />";
								$.each(response.itens.checkout,function(i,checkout) {
									quantidade = checkout.quantidade != undefined ? parseInt(checkout.quantidade) : 0;
									html+= quantidade + " " + checkout.titulo + "<br />";
									$(".checkout").find('.checkout-'+i).find('.retirada').html(
										parseInt($(".checkout").find('.checkout-'+i).find('.retirada').html()) + quantidade
									);
									$(".checkout").find('.checkout-'+i).find('.a-retirar').html(
										parseInt($(".checkout").find('.checkout-'+i).find('.a-retirar').html()) - quantidade
									);
								});
								html+= "<br />";
							}
						}
						$("#response").html(html);
					},
					//complete: function() { window.document.location.href = window.document.location.href; },
					error : function() {
						alert('nao foi');
					}
				});
			},2000);
		}
	});;
})
</script>
<div class="clearfix">
	<div class="legenda" style="width:95%; margin:0 0 30px 0; border:1px solid #989999">
		<table>
			<tr>
				<td colspan="3"><h2 class='titulo'>Resumo de vendas: Turma <?=$turma['Turma']['id']?></h2></td>
			</tr>
			<tr height="15">
				<td colspan="3"></td>
			</tr>
			<tr>
				<td class='container-parcerias'>
					<p class='fundo-vermelho'>Itens de contrato</p>
					<table class='contrato'>
						<tr height="15">
							<td colspan="3"></td>
						</tr>
						<tr>
							<td>Item</td>
							<td>&Agrave; Retirar</td>
							<td>Retirados</td>
						</tr>
						<tr class='convites'>
							<td>Convites</td>
							<td class='a-retirar'><?=$itens['contrato']['convites']['quantidade_total']-$itens['contrato']['convites']['quantidade_retirada']?></td>
							<td class='retirada'><?=$itens['contrato']['convites']['quantidade_retirada']?></td>
						</tr>
						<tr class='mesas'>
							<td>Mesas</td>
							<td class='a-retirar'><?=$itens['contrato']['mesas']['quantidade_total']-$itens['contrato']['mesas']['quantidade_retirada']?></td>
							<td class='retirada'><?=$itens['contrato']['mesas']['quantidade_retirada']?></td>
						</tr>
					</table>
				</td>
				<td class='container-parcerias'>
					<p class='fundo-vermelho'>Itens de campanhas</p>
					<table class='extras'>
						<tr height="15">
							<td colspan="3"></td>
						</tr>
						<tr>
							<td>Item</td>
							<td>&Agrave; Retirar</td>
							<td>Retirados</td>
						</tr>
						<?php foreach($itens['extras'] as $id => $extra) : ?>
						<tr class='extra-<?=$id?>'>
							<td><?=$extra['ViewRelatorioExtras']['extra']?></td>
							<td class='a-retirar'><?=$extra[0]['quantidade_total']-$extra[0]['quantidade_retirada']?></td>
							<td class='retirada'><?=$extra[0]['quantidade_retirada']?></td>
						</tr>
						<?php endforeach; ?>
					</table>
				</td>
				<td class='container-parcerias'>
					<p class='fundo-vermelho'>Itens de checkout</p>
					<table class='checkout'>
						<tr height="15">
							<td colspan="3"></td>
						</tr>
						<tr>
							<td>Item</td>
							<td>&Agrave; Retirar</td>
							<td>Retirados</td>
						</tr>
						<?php foreach($itens['checkout'] as $id => $extra) : ?>
						<tr class='checkout-<?=$id?>'>
							<td><?=$extra['titulo']?></td>
							<td class='a-retirar'><?=$extra['quantidade_total']-$extra['quantidade_retirada']?></td>
							<td class='retirada'><?=$extra['quantidade_retirada']?></td>
						</tr>
						<?php endforeach; ?>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<!--
	<div class="tabela-adicionar-item" style="text-align:left">
		<span>C&oacute;digo do formando: 
			<input name="codigo-formando" type="text" class="grid6 alpha omega" value="" id="codigo-formando">
		</span>
		<span class='submit button' id="confirmar-entrega">Confirmar Entrega</span>
		<div style="clear:both;"></div>
		<br />
		<br />
		<span id='response' style="float:left"></span>
	</div>
	-->
</div>