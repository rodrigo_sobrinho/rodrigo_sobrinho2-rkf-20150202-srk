<?php

class TurmaPrevisaoFechamento extends AppModel {

    var $name = 'TurmaPrevisaoFechamento';
    var $useTable = 'turmas_previsao_fechamento';
    var $validate = array(
        'data_previsao' => array(
            'date' => array('rule' => array('datetime'), 'message' => 'Data inválida.'),
        )
    );
    //The Associations below have been created with all possible keys, those that are not needed can be removed

    var $belongsTo = array(
        'Turma' => array(
            'className' => 'Turma',
            'foreignKey' => 'turma_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'usuario_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}

?>