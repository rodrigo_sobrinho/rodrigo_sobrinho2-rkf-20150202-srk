$(document).ready(function() {
	$("body").prepend("<div class='load-message' id='load-message'></div>");
	$("body").prepend("<div class='load-message centro' id='load-message-centro'></div>");
	var MensagensViewModel = function(tipo) {
		var self = this;
		
		self.tipo = ko.observable(tipo);
		self.mensagens = ko.observableArray();
		self.mensagensParaFiltro = ko.observableArray();
		self.iniciado = ko.observable(false);
		self.textoBusca = ko.observable("");
		self.paginaAtual = ko.observable(0);
		
		self.iniciar = ko.computed(function() {
			var tipo = self.tipo() == undefined ? 'mensagens' : self.tipo();
			self.tipo(tipo);
			self.paginaAtual(0);
			var url = "/mensagens/listar/" + tipo;
			var mensagens = [];
			self.iniciado(false);
			$("#load-message").text("Carregando...").fadeIn(200,function() {
				$.ajax({
		 	 		url : url,
		 	 		dataType : "json",
		 	 		async : false,
		 	 		type : "POST",
		 	 		data : { mensagens: self.mensagensParaFiltro() },
		 	 		success : function(dados) {
			 	 		if(dados.error) {
			 	 			console.log('erro');
			 	 		} else {
			 	 			self.mensagens([]);
			 	 			$.each(dados.mensagens,function(i,mensagem) {
			 	 				$.each(mensagem.MensagemUsuario,function(i,mu) {
			 	 					mensagem.MensagemUsuario[i] = ko.observable(mu);
			 	 				});
			 	 				mensagem.texto = ko.observable(mensagem.Mensagem.texto);
			 	 				if(mensagem.Mensagem.Usuario.id == mensagem.MensagemUsuario.usuario_id())
			 	 					mensagem.Mensagem.Usuario.nome = "Eu";
			 	 				if(mensagem.Mensagem.assunto != null)
			 	 					mensagem.Mensagem.Assunto.nome = mensagem.Mensagem.assunto;
			 	 				mensagem.usuario = ko.observable(mensagem.Mensagem.Usuario.nome);
			 	 				mensagem.assunto = ko.observable(mensagem.Mensagem.Assunto.nome);
			 	 				mensagem.item = ko.observable(mensagem.Mensagem.Assunto.Item.nome);
			 	 				mensagem.carregada = ko.observable(false);
			 	 				mensagem.selecionada = ko.observable(false);
			 	 				mensagem.destacados = ko.computed(function() {
			 	 					mensagem.usuario(self.destacar(mensagem.Mensagem.Usuario.nome));
			 	 					mensagem.assunto(self.destacar(mensagem.Mensagem.Assunto.nome));
			 	 					mensagem.item(self.destacar(mensagem.Mensagem.Assunto.Item.nome));
			 	 					mensagem.texto(self.destacar(self.decode(mensagem.Mensagem.texto)));
			 	 				},self);
				 	 			mensagens.push({id : mensagem.Mensagem.id, mensagem : mensagem });
			 	 			});
			 	 		}
		 	 		},
		 	 		error : function() {
		 	 			console.log("<p>Erro ao buscar servidor. Por favor tente mais tarde.</p>");
					},
					complete : function() {
						$("#load-message").fadeOut(200,function() {
							self.mensagens(mensagens);
							self.mensagensParaFiltro(mensagens);
							self.textoBusca("");
							self.todasSelecionadas(false);
							self.iniciado(true);
							$.uniform.update();
						});
					}
		 	 	});
			});
		},self);
		
		self.destacar = function(texto) {
			if(self.textoBusca().trim() != "" && texto.indexOf(self.textoBusca()) >= 0) {
				g = $('<div/>').html("<div>" + texto + "</div>");
				var r = eval("g.find('p,div').replaceText(/"+self.textoBusca()+"/gi, '<span class=\"destaque\">$&</span>' ).html()");
				return r;
			} else {
				return texto;
			}
		};
		
		self.filtrar = ko.computed(function() {
			var f = self.textoBusca().toLowerCase();
			if(self.textoBusca().trim() != "") {
				var mensagens = [];
				$.each(self.mensagensParaFiltro(),function(i,item) {
					if(self.decode(item.mensagem.Mensagem.texto).toLowerCase().indexOf(f) >= 0 ||
							item.mensagem.Mensagem.Assunto.nome.toLowerCase().indexOf(f) >= 0 ||
							item.mensagem.Mensagem.Assunto.Item.nome.toLowerCase().indexOf(f) >= 0 ||
							item.mensagem.Mensagem.Usuario.nome.toLowerCase().indexOf(f) >= 0)
						mensagens.push(item);
				});
				self.mensagens(mensagens);
			} else {
				self.mensagens(self.mensagensParaFiltro());
			}
		}, self);
		
		self.todasSelecionadas = ko.observable(false);
		
		self.selecionarTodas = function() {
			ko.utils.arrayForEach(self.itensPaginaAtual(), function(mensagem) {
				mensagem.mensagem.selecionada(self.todasSelecionadas() === true);
			});
			$.uniform.update();
			return true;
		};

		self.verificarSelecao = function() {
			var selecionadas = 0;
			ko.utils.arrayForEach(self.itensPaginaAtual(), function(mensagem) {
				selecionadas+= mensagem.mensagem.selecionada() == true ? 1 : 0;
			});
			if(selecionadas > 0)
				self.todasSelecionadas(selecionadas == self.itensPaginaAtual().length);
			else
				self.todasSelecionadas(false);
			$.uniform.update();
			return true;
		};
		
		self.selecionadas = function() {
			var selecionadas = 0;
			ko.utils.arrayForEach(self.itensPaginaAtual(), function(mensagem) {
				selecionadas+= mensagem.mensagem.selecionada() == true ? 1 : 0;
			});
			return selecionadas;
		}
		
		self.qtdePorStatus = function(tipo) {
			var qtde = 0;
			ko.utils.arrayForEach(self.itensPaginaAtual(), function(mensagem) {
				if(mensagem.mensagem.selecionada())
					qtde+= parseInt(mensagem.mensagem.MensagemUsuario[tipo]());
			});
			return qtde;
		}

		self.itensPorPagina = ko.observable(20);
		
		self.optionsQuantidadeItens = ko.observableArray(["5","10","20","30","50","100"]);
		
		self.selecionarQuantidade = function(qtde) {
			self.paginaAtual(0);
			self.itensPorPagina(qtde);
		}
		
		self.primeiroItemPaginaAtual = ko.computed(function () {
            return (self.itensPorPagina() * self.paginaAtual())+1;
        }, self);
		
		self.carregarUniform = function() {
			$('.uniform:not(.no-uniform):not(.all)').uniform();
			$('.uniform:not(.no-uniform):not(.all)').addClass('no-uniform');
			$.uniform.update();
		}
		
		self.itensPaginaAtual = ko.computed(function () {
            return self.mensagens.slice(self.primeiroItemPaginaAtual()-1, (self.primeiroItemPaginaAtual()-1) + self.itensPorPagina());
        }, self);
		
		self.ultimoItemPaginaAtual = ko.computed(function () {
			if(parseInt(self.primeiroItemPaginaAtual()) + parseInt(self.itensPorPagina() -1) > self.mensagens().length)
				return self.mensagens().length;
			else
				return parseInt(self.primeiroItemPaginaAtual()) + parseInt(self.itensPorPagina()) -1;
        }, self);
		
		self.maximoPaginas = ko.computed(function () {
            return Math.ceil(ko.utils.unwrapObservable(self.mensagens).length / self.itensPorPagina()) - 1;
        }, self);
		
		self.irParaPagina = function(dir) {
			if(dir == 'prev' && self.paginaAtual() > 0) {
				self.paginaAtual(self.paginaAtual()-1);
				self.verificarSelecao();
			} else if(dir == 'next' && self.paginaAtual() < self.maximoPaginas()) {
				self.paginaAtual(self.paginaAtual()+1);
				self.verificarSelecao();
			}
		}
		
		self.toggleFavorita = function(item) {
			var favorita = item.mensagem.MensagemUsuario.favorita() == 0 ? 1 : 0; item.mensagem.MensagemUsuario.favorita(favorita)
		};
		
		self.decode = function(s) {
			return $("<div/>").html(s).text();
		}
		
		self.getMessage = function(mensagem, callback) {
			if(!mensagem.carregada()) {
				$("#load-message").text("Carregando...").fadeIn(200,function() {
					var url = "/mensagens/exibir";
					$.ajax({
			 	 		url : url,
			 	 		data : { mensagem : mensagem.Mensagem.id },
			 	 		type : "POST",
			 	 		dataType : "json",
			 	 		success : function(response) {
			 	 			if(response.error) {
			 	 				loadMessage.html(response.message.join("<br />"));
			 	 			} else {
			 	 				mensagem.Mensagem.MensagemUsuario = response.mensagem.MensagemUsuario;
			 	 				mensagem.Mensagem.Arquivos = response.mensagem.Arquivo;
			 	 			}
			 	 		},
			 	 		error : function() {
			 	 			console.log('erro');
			 	 		}, complete : function() {
		 	 				$("#load-message").fadeOut(200,function() {
		 	 					mensagem.carregada(true);
		 	 					if(typeof callback == "function")
		 	 						callback();
		 	 					else
		 	 						mensagem.MensagemUsuario.lida(1);
		 	 				});
			 	 		}
			 	 	});
				})
			}
		};
		
		self.alterarStatus = function(tipo,v) {
			$("#load-message").text("Carregando...").fadeIn(200,function() {
				$.each(self.itensPaginaAtual(),function(i,item) {
					if(item.mensagem.MensagemUsuario[tipo]() != v && item.mensagem.selecionada())
						item.mensagem.MensagemUsuario[tipo](v);
				});
				$("#load-message").fadeOut(200);
			});
		}
		
		self.responderSelecionada = function() {
			ko.utils.arrayForEach(self.itensPaginaAtual(), function(item) {
				if(item.mensagem.selecionada()) {
					if(!item.mensagem.carregada())
						self.getMessage(item.mensagem,function() { self.responder(item) });
					else
						self.responder(item);
				}
			});
		}
		
		self.responder = function(el) {
			el.resposta = {
				assunto: "Re: " + el.mensagem.Mensagem.Assunto.nome,
				texto: el.mensagem.Mensagem.texto,
				destinos: ko.observableArray(),
				usuario_id: el.mensagem.MensagemUsuario.usuario_id,
				assunto_id : el.mensagem.Mensagem.Assunto.id,
				turma_id : el.mensagem.Mensagem.turma_id,
				removerDestino: function() { el.resposta.destinos.remove(this); }
			};
			var destinos = [];
			destinos.push({nome:el.mensagem.Mensagem.Usuario.nome,id:el.mensagem.Mensagem.Usuario.id});
			ko.utils.arrayForEach(el.mensagem.Mensagem.MensagemUsuario, function(mensagemUsuario) {
				if(el.mensagem.MensagemUsuario.usuario_id() != mensagemUsuario.Usuario.id &&
						el.mensagem.Mensagem.Usuario.id != mensagemUsuario.Usuario.id)
				destinos.push({nome:mensagemUsuario.Usuario.nome,id:mensagemUsuario.Usuario.id});
			});
			el.resposta.destinos(destinos);
			var div = $("<div id='resposta' class='resposta'/>").appendTo("body");
			ko.applyBindingsToNode(
				$("#resposta")[0],
				{
					template: {
						name: 'tmpMensagemResposta',
						data: { data : el, app: self },
						afterRender: function() { $("#texto-"+el.id).redactor(); }
					}
				}
			);
			bootbox.dialog($("#resposta"));
		};
		self.enviarResposta = function(data) {
			var url = "/mensagens/responder";
			data.resposta.texto = $("#texto-"+data.id).val();
			data.resposta.assunto = $("#assunto-"+data.id).val();
			$("#load-message").text("Carregando...").fadeIn(200,function() {
				$.ajax({
		 	 		url : url,
		 	 		dataType : "json",
		 	 		async : false,
		 	 		type : "POST",
		 	 		data : { resposta: ko.toJS(data.resposta), mensagem: ko.toJS(data.mensagem) },
		 	 		success : function(dados) { $("#load-message-centro").attr('type',dados.type).html(dados.message); },
		 	 		error : function() { $("#load-message-centro").attr('type','error').html("Erro ao se conectar ao servidor"); },
					complete : function(response) {
						$("#load-message").fadeOut(200,function() {
							self.cancelarResposta();
							delete data.resposta;
							$("#load-message-centro").fadeIn(200,function(){setTimeout(function() {$("#load-message-centro").fadeOut(200); },3000)});
						});
					}
		 	 	});
			});
		};
		self.cancelarResposta = function() {
			bootbox.hideAll();
			$("#resposta").remove();
		}
	}
	$('.uniform.all').uniform();
	var viewMensagens = new MensagensViewModel(tipo);
	viewMensagens.iniciar();
	ko.applyBindings(viewMensagens);
	$('a[href*="/"]').click(function(e) {
		e.preventDefault();
		$("#load-message").text("Carregando...").fadeIn(200,function() {
			var url = "/mensagens/atualizar";
			$.ajax({
	 	 		url : url,
	 	 		data : { mensagens : viewMensagens.mensagensParaFiltro() },
	 	 		type : "POST",
	 	 		dataType : "json",
	 	 		success : function(response) {
	 	 			console.log('sucesso')
	 	 		},
	 	 		error : function() {
	 	 			console.log('erro');
	 	 		}, complete : function() {
 	 				console.log('completo');
	 	 		}
	 	 	});
		});
		window.location.href = $(this).attr("href");
	});
})