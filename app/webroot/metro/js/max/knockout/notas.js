$(document).ready(function() {
    var NotasModel = function() {

        var self = this;
        self.notas = ko.observableArray();
        self.nota = ko.observable(false);
        self.editando = ko.observable(false);
        self.nova = ko.observable(false);
        self.timer = null;
        self.salvando = ko.observable(false);
        self.salvo = ko.observable(false);
        self.erro = ko.observable(false);
        self.carregada = ko.observable(false);
        self.texto = ko.observable('');
        self.liberarTexto = true;

        self.carregar = function() {
            var url = "/notas/carregar/";
            $.getJSON(url).done(function(response) {
                $.each(response.notas, function(i, nota) {
                    nota.indice = self.notas().length;
                    self.notas.push(nota);
                });
            });
            atualizaAltura($('#conteudo').height()+150);
            self.carregada(true);
        };
        
        self.liberarEdicao = ko.computed(function () {
            if(self.editando()) {
                if(self.nova())
                    self.texto('');
                else
                    self.liberarTexto = false;
                $("#texto").removeAttr('disabled').removeClass('bg-color-pastel')
                        .addClass('bg-color-white').focus();
            } else {
                self.nova(false);
                $("#texto").attr('disabled','disabled').removeClass('bg-color-white')
                        .addClass('bg-color-pastel');
            }
        },self);
        
        self.mensagemSalvando = ko.computed(function () {
            if(self.salvando())
                self.salvo(false);
        },self);
        
        self.mensagemSalvo = ko.computed(function () {
            if(self.salvo())
                self.salvando(false);
        },self);
        
        self.exibirNota = function(indice) {
            self.nota(self.notas()[indice]);
            self.texto(self.nota().UsuarioNota.texto);
        }
        
        self.novaNota = function() {
            self.nova(true);
            self.editando(true);
        }
        
        self.cancelarEdicao = function() {
            self.editando(false);
            if(self.nova())
                self.nova(false);
            if(self.nota())
                self.texto(self.nota().UsuarioNota.texto);
        }
        
        self.proximaNota = function() {
            self.editando(false);
            indice = self.buscarIndice(self.nota().indice);
            self.nota(self.notas()[(indice+1)]);
            self.texto(self.nota().UsuarioNota.texto);
        }
        
        self.notaAnterior = function() {
            self.editando(false);
            indice = self.buscarIndice(self.nota().indice);
            self.nota(self.notas()[(indice-1)]);
            self.texto(self.nota().UsuarioNota.texto);
        }
        
        self.buscarIndice = function(indice) {
            var id;
            $.each(self.notas(),function(i,nota) {
                if(indice == nota.indice)
                    id = i;
            });
            return id;
        }
        
        self.removerNota = function() {
            bootbox.confirm('Confirma a remoção da anotação?',function(response) {
                if(response) {
                    var indice = self.buscarIndice(self.nota().indice);
                    var id = self.nota().UsuarioNota.id;
                    var mensagem = "";
                    $.ajax({
                        url: "/notas/remover",
                        data: { data : { id : id } },
                        type: "POST",
                        dataType: "json",
                        success: function(response) {
                            if(response.erro == 0) {
                                mensagem = "Anotação removida";
                                self.notas.remove(self.nota());
                                self.exibirLista();
                            } else
                                mensagem = "Erro ao remover anotação";
                        },
                        error: function() {
                            mensagem = "Erro ao remover anotação";
                        },
                        complete: function() {
                            bootbox.alert(mensagem,function() {
                                bootbox.hideAll();
                            });
                        }
                    });
                }
            });
        }
        
        self.formataData = function(data) {
            return data.split(' ')[0].split('-').reverse().join('/');
        }
        
        self.trechoNota = function(texto) {
            if(texto.length > 18)
                return texto.substring(0,18) + " ...";
            else
                return texto;
        }
        
        self.exibirLista = function() {
            self.nota(false);
            self.nova(false);
            self.editando(false);
        }
        
        self.textoAlterado = ko.computed(function () {
            if(self.editando() && self.texto() != "" && self.liberarTexto) {
                if(self.timer != null)
                    clearTimeout(self.timer);
                self.timer = setTimeout(function() {
                    self.enviarEdicao();
                },1000);
            } else if(!self.liberarTexto) {
                self.liberarTexto = true;
            }
        },self);
        
        self.enviarEdicao = function() {
            if(self.nova()) {
                var dados = {
                    UsuarioNota : {
                        texto: self.texto(),
                        data_cadastro: (new Date(new Date().setUTCHours(new Date().getHours()))).toISOString().substring(0, 19).replace('T', ' ')
                    }
                }
            } else {
                var dados = self.nota();
                delete dados.UsuarioNota.data_atualizacao;
                dados.UsuarioNota.texto = self.texto();
            }
            self.salvando(true);
            $.ajax({
                url: "/notas/inserir",
                data: { data : dados },
                type: "POST",
                dataType: "json",
                success: function(response) {
                    if(response.erro == 0) {
                        self.erro(false);
                        if(response.nota != undefined) {
                            if(self.nova()) {
                                response.nota.indice = self.notas()[self.notas().length-1].indice+1;
                                self.notas.push(response.nota);
                                self.nova(false);
                            } else {
                                response.nota.indice = dados.indice;
                            }
                            self.nota(response.nota);
                        }
                    } else {
                        self.erro(true);
                    }
                },
                error: function() {
                    self.erro(true);
                },
                complete: function() {
                    self.salvo(true);
                }
            });
        }
    };
    
    var notasModel = new NotasModel();
    notasModel.carregar();
    ko.applyBindings(notasModel, document.getElementById('notas'));
});