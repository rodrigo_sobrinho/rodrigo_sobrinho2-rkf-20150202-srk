<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/tooltip.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery.meio.mask.js"></script>
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/datepicker.css">
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/bootstrap/datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<style type="text/css">
    input { max-height: 32px!important }
    #end { min-height: 32px; max-width: 140px }
    #start { min-height: 32px; max-width: 140px }
</style>
<script type="text/javascript">
$(document).ready(function() {
    $('.selectpicker').selectpicker({
        width:'100%',
        format: 'mm/dd/yyyy',
        startDate: '+30d'
    });
    $("#start").datepicker();
    $("#end").datepicker();
    $.Input();
    $('input[alt]').setMask();
    $("#form-filtro").submit(function(e){
        e.preventDefault();
        if(($('#start').val() == '' && $('#end').val() != '') || ($('#start').val() != '' && $('#end').val() == '')){
            bootbox.alert("<h3 class='fg-color-red'>Preencha ambas as datas ou deixe em branco!</h3>");
        }else{
            var context = ko.contextFor($(".metro-button.reload")[0]);
            context.$data.showLoading(function() {
                $("#form-filtro").find('[value="false"]').attr('disabled','disabled');
                var dados = $("#form-filtro").serialize();
                var url = $("#form-filtro").attr('action');
                $.ajax({
                    url : url,
                    data : dados,
                    type : "POST",
                    dataType : "json",
                    complete : function() {
                        context.$data.reload();
                    }
                });
            });
        }
    });
    $('.detalhes').click(function() {
        bootbox.dialog('Carregando',[{
            label: 'Fechar'
        }],{
            remote: '<?="/turmas/detalhes/"?>' + $(this).attr('tid') + "/" + $(this).attr('eid')
        });
    });
});
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Eventos
        </h2>
    </div>
</div>
<?php
$session->flash();
$sortOptions = array('data-bind' => 'click: loadThis');
$buttonAfter = '<button class="helper" onclick="return false" ' .
    'tabindex="-1" type="button"></button>';
?>
<?=$form->create('Turma',array(
        'url' => "/turmas/ficha_evento/",
        'id' => 'form-filtro')) ?>
<div class="row-fluid">
    <div class="row-fluid">
        <div class="span2">
            <?=$form->input('Turma.id',
                array('label' => 'ID', 'div' => 'input-control text', 
                    'error' => false,'type' => 'text', 'after' => $buttonAfter)); ?>
        </div>
        <div class="span2">
            <?=$form->input('Evento.date_start',
                array('label' => 'Data Início', 'class' => 'input-control text', 
                    'alt' => "99/99/9999", 'div' => false,'error' => false, 'id' => 'start')); ?>
        </div>
        <div class="span2">
            <?=$form->input('Evento.date_end',
                array('label' => 'Data Fim', 'class' => 'input-control text', 
                    'alt' => "99/99/9999", 'div' => false,'error' => false, 'id' => 'end')); ?>
        </div>
        <div class="span3">
            <?=$form->input('Evento.tipos_evento_id',array(
                'options' => array('' => 'Todos', $tiposEventos),
                'type' => 'select',
                'class' => 'selectpicker',
                'data-width' => '100%',
                'label' => 'Tipo do Evento',
                'div' => 'input-control text')); ?>
        </div>
        <div class="span2">
            <label>&nbsp;</label>
            <button type='submit' class='mini max bg-color-red'>
                Filtrar
                <i class='icon-search-2'></i>
            </button>
        </div>
    </div>
    <?= $form->end(array('label' => false,
        'div' => false, 'class' => 'hide')); ?>
<?php if (sizeof($turmas) > 0) : ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="5%">
                    <?=$paginator->sort('ID', 'Turma.id',$sortOptions); ?>
                </th>
                <th scope="col" width="30%">
                    <?=$paginator->sort('Nome', 'Turma.nome',$sortOptions); ?>
                </th>
                <th scope="col" width="5%">
                    <?=$paginator->sort('Aderidos', 'aderidos',$sortOptions); ?>
                </th>
                <th scope="col" width="5%">
                    <?=$paginator->sort('Checkout', 'checkouts',$sortOptions); ?>
                </th>
                <th scope="col" width="20%">
                    <?=$paginator->sort('Evento', 'Evento.tipos_evento_id',$sortOptions); ?>
                </th>
                <th scope="col" width="20%">
                    <?=$paginator->sort('Local', 'Evento.local',$sortOptions); ?>
                </th>
                <th scope="col" width="10%">
                    <?=$paginator->sort('Data', 'Evento.data',$sortOptions); ?>
                </th>
                <th scope="col" width="5%"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($turmas as $turma) : ?>
            <tr>
                <td style="text-align: center"><?=$turma['Turma']['id']; ?></td>
                <td><?=$turma['Turma']['nome']; ?></td>
                <td style="text-align: center"><?=$turma[0]['aderidos']; ?></td>
                <td style="text-align: center"><?=$turma[0]['checkouts']; ?></td>
                <td><?=$turma['TiposEvento']['nome']; ?></td>
                <td><?=ucfirst($turma['Evento']['local']); ?></td>
                <td><?=($turma['Evento']['data'] > 0) ? date('d/m/Y', strtotime($turma['Evento']['data'])) : "<em class='fg-color-red'>Não Cadastrado</em>"?></td>
                <td style="text-align:center">
                    <button type="button" class="mini bg-color-blueDark detalhes" tid="<?=$turma['Turma']['id']?>" eid="<?=$turma['Evento']['id']?>">
                        Detalhes
                    </button>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="6" class="paginacao">
                    <?=$paginator->numbers(array('separator' => ' ',
                        'data-bind' => 'click: loadThis')); ?>
                </td>
                <td colspan="2">
                    <span class="label label-info">
                    <?=$paginator->counter(array(
                        'format' => 'Total : %count% ' .  ' Eventos')); ?>
                    </span>
                </td>
            </tr>
        </tfoot>
    </table>
<?php else : ?>
    <h2 class="fg-color-red">Não há eventos para o filtro informado.</h2>
<?php endif; ?>
</div>