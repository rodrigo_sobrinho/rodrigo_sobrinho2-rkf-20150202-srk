<script type="text/javascript">
    $(document).ready(function() {
        var button = $("<button>",{
            type: 'button',
            class: 'button bg-color-blue',
            id:'salvar-contrato',
            text:'Salvar Contrato'
        });
        $('.modal-footer').prepend(button);
        $("#salvar-contrato").click(function(e) {
            e.preventDefault();
            var context = ko.contextFor($("#content-body")[0]);
            var url = "/<?=$this->params['prefix']?>/contratos/gerar/<?=$tipo?>";
            bootbox.hideAll();
            context.$data.showLoading(function() {
                $.getJSON(url,{},function() {
                    context.$data.reload();
                }).always(function() {
                    context.$data.reload();
                });
            });
        });
    });
</script>