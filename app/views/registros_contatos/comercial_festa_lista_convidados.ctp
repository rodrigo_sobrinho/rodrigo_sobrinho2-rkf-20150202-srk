<?php $festaJaAconteceu = strtotime('now') > strtotime($festa['RegistrosContato']['data']); ?>
<div class="row-fluid">
    <div class="span10 offset1">
        <h2>
            <?=count($festa['RegistroContatoUsuario'])?> Formandos convidados pra festa
            <a class="mini bg-color-greenDark button pull-right"
                href="<?="/{$this->params['prefix']}/registros_contatos/festa_convidados_excel/{$festa['RegistrosContato']['id']}"?>">
                Gerar Excel
            </a>
        </h2>
    </div>
</div>
<?php if(count($festa['RegistroContatoUsuario']) > 0) : ?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<style type="text/css">
.cancelar-confirmacao i { margin-left:0; line-height: 10px }
select { height:24px; margin:0; padding:0; font-size:12px }
</style>
<script type="text/javascript">
    $(document).ready(function(){
        $(".confirmar-presenca").click(function() {
            div = $(this).next();
            $(this).fadeOut(500,function() {
                div.fadeIn(500);
            });
        });
        $(".cancelar-confirmacao").click(function() {
            button = $(this).parent().prev();
            $(this).parent().fadeOut(500,function() {
                button.fadeIn(500);
            });
        });
        $(".enviar-confirmacao").click(function() {
            var td = $(this).parents('td');
            var div = $(this).parent();
            var id = td.data('id');
            var confirmacao = $(this).prev().val();
            $.ajax({
                url : '/registros_contatos/confirmar_presenca',
                data : {
                    data : {
                        id : id,
                        confirmacao : confirmacao
                    }
                },
                type : "POST",
                dataType : "json",
                success: function(response) {
                    div.fadeOut(500,function() {
                        if(confirmacao == 1)
                            td.html('Confirmada');
                        else
                            td.html('Ausente');
                        div.remove();
                    });
                },
                error : function() {
                    div.fadeOut(500,function() {
                        td.html('Erro ao enviar');
                        div.remove();
                    });
                }
            });
        });
        $("[data-compareceu] span").click(function() {
            var span = this;
            $(span).attr('class','pointer label').text('Aguarde...');
            var td = $(span).parent();
            var dados = td.data();
            dados.compareceu = dados.compareceu == 1 ? 0 : 1;
            var url = '/<?=$this->params['prefix']?>/registros_contatos/festa_compareceu/';
            $.ajax({
                url : url,
                data : {
                    data : dados
                },
                type : "POST",
                dataType : "json",
                success : function(response) {
                    if(response.erro == 0) {
                        $(span).addClass('label-important').text('Erro');
                    } else {
                        if(dados.compareceu == 1)
                            $(span).addClass('label-success').html('Sim');
                        else
                            $(span).addClass('label-important').html('N&atilde;o');
                        td.data('compareceu',dados.compareceu);
                    }
                },
                error : function() {
                    $(span).addClass('label-important').text('Erro');
                }
            });
        });
    });
</script>
<br />
<div class="row-fluid">
    <div class="span10 offset1">
        <table class="table table-condensed table-striped">
            <thead>
                <tr>
                    <th scope="col" width="5%">Turma</th>
                    <th scope="col" width="20%">Turma Nome</th>
                    <th scope="col" width="20%">Nome</th>
                    <th scope="col" width="10%">RG</th>
                    <th scope="col" width="10%">Data Convite</th>
                    <?php if($festaJaAconteceu) : ?>
                    <th scope="col" width="10%">Presença</th>
                    <th scope="col" width="10%">Compareceu</th>
                    <?php else : ?>
                    <th scope="col" width="20%">Presença</th>
                    <?php endif; ?>
                    <th scope="col" width="15%">Consultor</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($festa['RegistroContatoUsuario'] as $usuario) : ?>
                <tr>
                    <td><?="{$usuario['Usuario']['turma_id']}"?></td>
                    <td><?="{$usuario['Usuario']['turma_nome']}"?></td>
                    <td><?=$usuario['Usuario']['nome']?></td>
                    <td><?=$usuario['Usuario']['rg']?></td>
                    <td><?=date('d/m/Y',strtotime($usuario['data_cadastro']))?></td>
                    <td style="text-align:center"
                        data-id="<?=$usuario['id']?>">
                        <?php if(empty($usuario['data_confirmacao']) && !$festaJaAconteceu) : ?>
                        <button type="button" class="default mini confirmar-presenca">
                            Confirmar Presença
                        </button>
                        <div class="hide">
                            <select class="confirmacao">
                                <option value="1">Presente</option>
                                <option value="0">Ausente</option>
                            </select>
                            <button type="button" class="mini bg-color-greenDark enviar-confirmacao">
                                <i class="icon-thumbs-up"></i>
                            </button>
                            <button type="button" class="mini bg-color-red cancelar-confirmacao">
                                <i class="icon-cancel-3"></i>
                            </button>
                        </div>
                        <?php elseif(empty($usuario['data_confirmacao'])) : ?>
                        Nao Conf
                        <?php elseif($usuario['confirmou_presenca'] == 1) : ?>
                        Confirmada
                        <?php else : ?>
                        Ausente
                        <?php endif; ?>
                    </td>
                    <?php if($festaJaAconteceu) : ?>
                    <td style="text-align: center" data-id="<?=$usuario['id']?>"
                        data-compareceu="<?=$usuario['compareceu']?>">
                        <span class="pointer label label-<?=$usuario['compareceu'] == 1 ? "success" : "important"?>">
                            <?=$usuario['compareceu'] == 1 ? "Sim" : "Não"?>
                        </span>
                    </td>
                    <?php endif; ?>
                    <td><?=$usuario['Consultor']['nome']?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<?php endif; ?>
