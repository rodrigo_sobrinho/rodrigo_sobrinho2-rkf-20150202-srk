<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/bootstrap/tooltip.js"></script>
<script type="text/javascript">
$('body').tooltip({ selector: '[rel=tooltip]'});
</script>
<h2>
    <?=$turma['Turma']['id']?> - <?=$turma['Turma']['nome']?>
</h2>
<?php if($parcelamentos) : ?>
<br />
<table class="table table-condensed table-striped">
    <thead>
        <tr>
            <th scope="col">
                Plano
            </th>
            <th scope="col">
                Data de in&iacute;cio
            </th>
            <th scope="col">
                Parcelas
            </th>
            <th scope="col">
                Valor
            </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($parcelamentos as $parcelamento) : ?>
        <tr>
            <td><?=$parcelamento['Parcelamento']['titulo']; ?></td>
            <td><?=date('m/Y',strtotime($parcelamento['Parcelamento']['data'])); ?></td>
            <td><?=$parcelamento['Parcelamento']['parcelas']; ?> x R$<?=number_format($parcelamento['Parcelamento']['valor']/$parcelamento['Parcelamento']['parcelas'], 2, ',', '.'); ?></td>
            <td rel="tooltip" title="<?=$parcelamento['Parcelamento']['parcelas'] . 'x' . ' de ' . number_format(($parcelamento['Parcelamento']['valor'] / $parcelamento['Parcelamento']['parcelas']), 2, ',', '.')?>">
                R$<?=number_format($parcelamento['Parcelamento']['valor'], 2, ',', '.'); ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php endif; ?>
