<?php

class RegistroContatoUsuario extends AppModel {

    var $name = 'RegistroContatoUsuario';
    var $useTable = 'registros_contatos_usuarios';

    var $belongsTo = array(
        'RegistrosContato' => array(
            'className' => 'RegistrosContato',
            'foreignKey' => 'registro_contato_id'
        ),
        'Usuario' => array(
            'className' => 'ViewFormandos',
            'foreignKey' => 'usuario_id'
        ),
        'Consultor' => array(
            'className' => 'Usuario',
            'foreignKey' => 'consultor_id'
        )
    );
    
    var $recursive = 1;

}

?>