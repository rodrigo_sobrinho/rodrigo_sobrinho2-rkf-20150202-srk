<style type="text/css">
    #conteudo-container { margin:10px 0 0 0 !important; padding:0; width:100%!important }
    h2.titulo { background-color:#C7C5C6; text-align:left; color:black; padding:5px }
    #link-imprimir { display:none }
    div.break { page-break-before: always!important }
    #conteudo-wrap { float:none!important }
</style>
<span id="conteudo-titulo" class="box-com-titulo-header">
    Via Do Formando
</span>
<div id="conteudo-container" style="position: relative">
    <div style="overflow:hidden; width:100%; position:relative" class="clearfix">
        <div class="legenda" style="width:97%; margin:20px 0 30px 0; border:1px solid #989999; position:relative">
            <div style='color:#AA0000'>
                <img src="<?= empty($formando['diretorio_foto_perfil']) ? "{$this->webroot}img/uknown_user.gif" : "{$this->webroot}{$formando['diretorio_foto_perfil']}" ?>"
                     style='float:left; margin:0 5px 15px 0; width:120px' />
                <p>
                    <strong style='text-transform:uppercase'><?= $formando['nome'] ?></strong>
                    <br />
                    <em><?= $formando['grupo'] ?></em>
                    <br />
                    <em>Código <?= $formando['codigo_formando'] ?></em>
                    <br />
                    <em>RG <?= $formando['rg'] ?></em>
                    <br />
                    <em>Protocolo <?= $protocolo['Protocolo']['protocolo'] ?></em>
                </p>
            </div>
            <p>
                Eu, <em><?= $atendente['Usuario']['nome'] ?></em>, confirmo que o formando, <em><?= $formando['nome'] ?></em>,
                <br />quitou suas pendências financeiras e têm direito a retirada dos seguintes itens para festa de formatura:
            </p>
            <table>
                <tr>
                    <td colspan="2" width="50%"><h2 class='titulo'>Informa&ccedil;&otilde;es da ades&atilde;o</h2></td>
                    <td colspan="2" width="50%"><h2 class='titulo'>Resumo financeiro</h2></td>
                </tr>
                <tr height="5">
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td width="200">Data contrato da Turma</td>
                    <td><?= date('d/m/Y', strtotime($turma['Turma']['data_assinatura_contrato'])); ?></td>
                    <td>Valor de Contrato</td>
                    <td class='valor-contrato'>R$<?= number_format($formando['valor_adesao'], 2, ',', '.'); ?></td>
                </tr>
                <tr>
                    <td>Data Adesão</td>
                    <td><?= date('d/m/Y', strtotime($formando['data_adesao'])); ?></td>
                    <td>Valor de IGPM</td>
                    <td><?= $totalIGPM > 0 ? "R$" . number_format($totalIGPM, 2, ',', '.') : "-"; ?></td>
                </tr>
                <tr>
                    <td>Data IGPM</td>
                    <td>
                        <?=!empty($turma['Turma']['data_igpm']) ?
                            date('d/m/Y', strtotime($turma['Turma']['data_igpm'])) :
                            "IGPM não aplicado"; ?>
                    </td>
                    <td>Valor de Contrato c/ Correção</td>
                    <td class='total-contrato'>R$<?= number_format($formando['valor_adesao'] + $totalIGPM, 2, ',', '.'); ?></td>
                </tr>
                <tr>
                    <td>Mesas do contrato</td>
                    <td><?= $turma['Turma']['mesas_contrato']; ?></td>
                    <td>Valor de Campanhas</td>
                    <td class='total-campanha'><?= $totalDespesasExtras > 0 ? "R$" . number_format($totalDespesasExtras, 2, ',', '.') : "-" ?></td>
                </tr>
                <tr>
                    <td>Convites do contrato</td>
                    <td><?= $turma['Turma']['convites_contrato']; ?></td>
                    <td>Valor de Itens do Checkout</td>
                    <td><?= $totalItensCheckout > 0 ? "R$" . number_format($totalItensCheckout, 2, ',', '.') : "-" ?></td>
                </tr>
                <tr>
                    <td>Número de Parcelas do Contrato</td>
                    <td><?= $formando['parcelas_adesao']; ?></td>
                    <td colspan='2'>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan='2'>&nbsp;</td>
                    <td>Total Recebido Antes de Checkout</td>
                    <td><?= $totalPago > 0 ? "R$" . number_format($totalPago, 2, ',', '.') : "-" ?></td>
                </tr>
                <tr>
                    <td colspan='2'></td>
                    <td>Total Recebido no Checkout</td>
                    <td><?= $totalPagoCheckout > 0 ? "R$" . number_format($totalPagoCheckout, 2, ',', '.') : "-" ?></td>
                </tr>
                <tr>
                    <td colspan='2'>&nbsp;</td>
                    <td>Saldo Final</td>
                    <td><?= $saldoTotal != 0 ? "R$" . number_format($saldoTotal, 2, ',', '.') : "-" ?></td>
                </tr>
                <tr height="15">
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td colspan="4"><h2 class='titulo'>Itens a Retirar</h2></td>
                </tr>
                <tr height="5">
                    <td colspan="4"></td>
                </tr>
                <?php if (count($itens) > 0) : ?>
                <?php foreach ($itens as $item) : ?>
                <tr>
                    <td colspan="4"><?= $item['descricao'] ?><br />&nbsp;</td>
                </tr>
                <?php endforeach; ?>
                <?php else : ?>
                <tr>
                    <td colspan="4"><h3>Todos os itens foram retirados</h3></td>
                </tr>
                <?php endif; ?>
            </table>
        </div>
    </div>
    <div style="page-break-before: always;"></div>
    <span id="conteudo-titulo" class="box-com-titulo-header break">Via Do Formando</span>
    <div style="overflow:hidden; width:100%; position:relative" class="clearfix break">
        <div class="legenda" style="width:97%; margin:20px 0 30px 0; border:1px solid #989999">
            <div style='color:#AA0000'>
                <img src="<?= empty($formando['diretorio_foto_perfil']) ? "{$this->webroot}img/uknown_user.gif" : "{$this->webroot}{$formando['diretorio_foto_perfil']}" ?>"
                     style='float:left; margin:0 5px 15px 0; width:120px' />
                <p>
                    <strong style='text-transform:uppercase'><?= $formando['nome'] ?></strong>
                    <br />
                    <em><?= $formando['grupo'] ?></em>
                    <br />
                    <em>Código <?= $formando['codigo_formando'] ?></em>
                    <br />
                    <em>RG <?= $formando['rg'] ?></em>
                    <br />
                    <em>Protocolo <?= $protocolo['Protocolo']['protocolo'] ?></em>
                </p>
            </div>
            <p>
                Eu, <em><?= $atendente['Usuario']['nome'] ?></em>, confirmo que o formando, <em><?= $formando['nome'] ?></em>,
                <br />quitou suas pendências financeiras e têm direito a retirada dos seguintes itens para festa de formatura:
            </p>
        </div>
        <table style="width:80%">
            <?php if (count($pagamentosCheckout) > 0) : ?>
                <tr>
                    <td colspan="4"><h2 class='titulo'>Pagamentos Recebidos Durante o Checkout</h2></td>
                </tr>
                <tr>
                    <td>Valor</td>
                    <td>Tipo</td>
                    <td>Refer&ecirc;ncia</td>
                    <td>Vencimento</td>
                </tr>
                <?php foreach ($pagamentosCheckout as $pagamento) : ?>
                    <tr>
                        <td>R$<?= number_format($pagamento['CheckoutFormandoPagamento']['valor'], 2, ',', '.') ?></td>
                        <td><?= $pagamento['CheckoutFormandoPagamento']['tipo'] ?></td>
                        <td><?= implode(" - ", array_map("ucfirst", explode(";", $pagamento['CheckoutFormandoPagamento']['referente']))) ?></td>
                        <td><?= date('d/m/Y', strtotime($pagamento['CheckoutFormandoPagamento']['data_vencimento'])); ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </table>
    </div>
</div>