<?php if(in_array($turma['Turma']['tipos_contrato_id'], array(1, 2, 5, 6))) : ?>
<h3>Condi&ccedil;&otilde;es Contratuais</h3>
<br />
Adere ao Contrato de Cobrança de Valores, firmado entre a Comissão de Formatura de seu curso e a empresa <b>ZAPE GESTÃO DE 
VALORES</b>, inscrita no CNPJ/MF sob N.º 18.405.543/0001-54, com sede na Avenida José Maria Whitaker, 882 CEP: 04057-000 
São Paulo – SP, representada neste ato pelo sócio-diretor RACHID SADER NETO, portador do RG Nº 35.774.690 SSP/SP e 
inscrito no CPF Nº 112.710.516-72, contrato esse assinado em 99/99/9999 que será denominado CONTRATO COLETIVO e regido 
nos seguintes moldes: 
<br />
<br />
1. O formando acima qualificado reconhece a Comissão de Formatura de sua turma como legítima representante para tratar dos 
assuntos relacionados à cobrança de valores, outorgando poderes para representá-lo em atos referentes ao serviço contratado, 
ratificando os termos do CONTRATO COLETIVO, declarando ter plenos conhecimentos desse contrato e outorgando poderes para a 
Comissão de Formatura celebrar aditivos contratuais que se fizerem necessários.
<br />
<br />
2. As informações importantes a respeito do seu contrato estarão disponibilizadas no link: http://sistema.asformaturas.com.br. Caso precise do LOGIN e SENHA da sua turma, entre em contato com a sua comissão ou diretamente com a <b>ZAPE GESTÃO DE VALORES</b>.
<br />
<br />
3. O formando acima qualificado autoriza a <b>ZAPE GESTÃO DE VALORES</b> a representá-lo quando necessário na contratação e pagamento de fornecedores conforme descrito no CONTRATO COLETIVO. O valor pago pelo CONTRATANTE permanecerá em posse da CONTRATADA de forma transitória, até a transferência para uma conta corrente indicada pela Comissão de Formatura ou repasse aos fornecedores contratados pela CONTRATADA, ficando tão-somente com a comissão que lhe faz jus.
<br />
<br />
4. Os valores consignados neste contrato podem ser atualizados de 12 em 12 meses, a partir da assinatura do Contrato de Prestação de Serviços celebrado entre a Comissão de Formatura e a <b>ZAPE GESTÃO DE VALORES</b>, adotando - se a variação do IGPM/FGV do período, conforme autoriza a Lei 9.069/95, ou por outro índice que venha a substituí-lo. A <b>ZAPE GESTÃO DE VALORES</b> não é autorizada a realizar qualquer outro tipo de reajuste de valores contratuais.
<br />
<br />
5. A ZAPE GESTÃO DE VALORES não cobra taxa de emissão de boletos. Para imprimi-los, acesse o espaço do formando com o seu LOGIN e SENHA e tenha acesso a todos os seus boletos na Área Financeira.
<br />
<br />
6. Caso opte por pagar em cheque, lembramos que é sua responsabilidade a entrega dos cheques para o representante da <b>ZAPE GESTÃO DE VALORES</b> na faculdade ou diretamente em nossa sede. Sempre exija o seu recibo.
<br />
<br />
7. Lembramos que caso opte por pagar em cheque, mas decida posteriormente pagar em boletos, não há problemas, basta acessar sua área financeira e imprimir os boletos.
<br />
<br />
8. Além disso, a <b>ZAPE GESTÃO DE VALORES</b> disponibiliza um serviço adicional de envio dos boletos impressos. Caso queira recebê-los em casa, opte por este serviço no momento da adesão. Para esta opção, será cobrado o valor de R$20,00 (Vinte Reais). Lembramos que esta taxa não é obrigatória e que você pode entrar no site e imprimir seus boletos a qualquer momento.
<br />
<br />
9. Fica eleito o Foro da Capital do Estado de São Paulo por mais privilegiado que outro seja, arcando a parte vencida com o ônus da sucumbência, inclusive honorários advocatícios de 20% do total da condenação.
<?php elseif($turma['Turma']['id'] == 5642) : ?>
<h3>Condi&ccedil;&otilde;es Contratuais</h3>
<br />
<strong>Adere ao Contrato de Prestação de Serviços, Representação Financeira e de Fotos firmado entre a Comissão de Formatura de seu curso e a
empresa ÁS EVENTOS LTDA.</strong> Inscrita no CNPJ/MF sob N.º06.163.144/0001-45, com sede na Avenida José Maria Whitaker, 882 CEP: 04057-000, São
Paulo – SP, representada neste ato pelo sócio-diretor RACHID SADER, portador do RG Nº 26.572.035-7, contrato esse assinado em (28/02/2014), que será denominado <strong>CONTRATO COLETIVO</strong> e regido nos seguintes moldes:
<br />
<br />
1. O formando acima qualificado reconhece a Comissão de Formatura de sua turma como legítima representante para tratar dos assuntos relacionados à sua Formatura, outorgando poderes para representá-lo em atos referentes à sua formatura, ratificando os termos do CONTRATO COLETIVO, declarando ter plenos conhecimentos desse contrato e outorgando poderes para a Comissão celebrar aditivos contratuais que se fizerem necessários à realização dos eventos.
<br />
<br />
2. As informações importantes a respeito do seu contrato estarão disponibilizadas no link: <a href="http://sistema.asformaturas.com.br/usuarios/login" target="_blank">sistema.asformaturas.com.br/usuarios/login</a>. Seu login sempre será seu email e sua senha será escolhida por você, caso tenha alguma dúvida entre em contato com a sua comissão ou diretamente com <a href="mailto:atendimento@eventos.as">atendimento@eventos.as</a>
<br />
<br />
3. A comissão de formatura não poderá tratar assuntos de cunho financeiro, tais como forma de pagamento, renegociação de dívida ou cancelamento de contrato em nome do formando.
<br />
<br />
4. No caso de rescisão contratual, o formando seguirá os termos estabelecidos pela COMFOR no ESTATUTO DE FORMATURA DO CURSO DE MEDICINA T53.
<br />
<br />
5. Constitui obrigação do(a) CONTRATANTE manter seu endereço e demais dados cadastrais atualizados, bem como a responsabilidade pela grafia correta de seu nome, nos textos originais que deverão ser entregues à CONTRATADA para a execução dos serviços gráficos, eximindo a CONTRATADA pela falta do nome e/ou grafia incorreta conforme previsão no Contrato Principal.
<br />
<br />
6. É responsabilidade do formando exigir uma cópia do ESTATUTO DA FORMATURA para a COMFOR.
<?php elseif(in_array($turma['Turma']['id'], array('6028','6984', '6631'))) : ?>
<h3>Condi&ccedil;&otilde;es Contratuais</h3>
<br />
Adere ao Contrato de Cobrança de Valores, firmado entre a Comissão de Formatura de seu curso e a empresa ZAPE GESTÃO DE VALORES,
inscrita no CNPJ/MF sob N.º 18.405.543/0001-54, com sede na Avenida José Maria Whitaker, 882 CEP: 04057-000 São Paulo – SP,
representada neste ato pelo sócio-diretor RACHID SADER NETO, portador do RG Nº 35.774.690 SSP/SP e inscrito no 
CPF Nº 112.710.516-72, contrato esse assinado em <b><?=date("d/m/Y", strtotime($turma['Turma']['data_assinatura_contrato']))?></b> que será denominado 
<b>CONTRATO COLETIVO</b> e regido nos seguintes moldes:
<br />
<br />
<b>1.</b> O formando acima qualificado reconhece a Comissão de Formatura de sua turma como legítima representante para tratar
dos assuntos relacionados à cobrança de valores, outorgando poderes para representá-lo em atos referentes ao serviço contratado,
ratificando os termos do <b>CONTRATO COLETIVO</b>, declarando ter plenos conhecimentos desse contrato e outorgando poderes para a 
Comissão de Formatura celebrar aditivos contratuais que se fizerem necessários.
<br />
<br />
<b>2.</b> As informações importantes a respeito do seu contrato estarão disponibilizadas no link: 
<a href="http://sistema.asformaturas.com.br">http://sistema.asformaturas.com.br.</a> Caso precise do <b>LOGIN</b> e <b>SENHA</b> 
da sua turma, entre em contato com a sua comissão ou diretamente com a <b>ZAPE GESTÃO DE VALORES</b>. 
<br />
<br />
<b>3.</b> O formando acima qualificado autoriza a <b>ZAPE GESTÃO DE VALORES</b> a representá-lo quando necessário na contratação e 
pagamento de fornecedores conforme descrito no <b>CONTRATO COLETIVO</b>. O valor pago pelo <b>CONTRATANTE</b> permanecerá em posse da 
<b>CONTRATADA</b> de forma transitória, até a transferência para uma conta corrente indicada pela Comissão de Formatura ou repasse 
aos fornecedores contratados pela <b>CONTRATADA</b>, ficando tão-somente com a comissão que lhe faz jus. 
<br />
<br />
<b>4.</b> Os valores consignados neste contrato podem ser atualizados de 12 em 12 meses, a partir da assinatura do Contrato 
de Prestação de Serviços celebrado entre a Comissão de Formatura e a <b>ZAPE GESTÃO DE VALORES</b>, adotando - se a variação 
do IGPM/FGV do período, conforme autoriza a Lei 9.069/95, ou por outro índice que venha a substituí-lo. <b>A ZAPE GESTÃO 
DE VALORES</b> não é autorizada a realizar qualquer outro tipo de reajuste de valores contratuais.
<br />
<br />
<b>5.</b> A <b>ZAPE GESTÃO DE VALORES</b> não cobra taxa de emissão de boletos. Para imprimi-los, acesse o espaço do formando
com o seu LOGIN e SENHA e tenha acesso a todos os seus boletos na <b>Área Financeira</b>.
<br />
<br />
<b>6.</b> Caso opte por pagar em cheque, lembramos que é sua responsabilidade a entrega dos cheques para o representante 
da <b>ZAPE GESTÃO DE VALORES</b> na faculdade ou diretamente em nossa sede. Sempre exija o seu recibo.
<br />
<br />
<b>7.</b> Lembramos que caso opte por pagar em cheque, mas decida posteriormente pagar em boletos, não há problemas, 
basta acessar sua área financeira e imprimir os boletos.
<br />
<br />
<b>8.</b> Além disso, a <b>ZAPE GESTÃO DE VALORES</b> disponibiliza um serviço adicional de envio dos boletos impressos. 
Caso queira recebê-los em casa, opte por este serviço no momento da adesão. Para esta opção, será cobrado o valor de 
R$20,00 (Vinte Reais). Lembramos que esta taxa não é obrigatória e que você pode entrar no site e imprimir seus boletos 
a qualquer momento. 
<br />
<br />
<b>9.</b> Fica eleito o Foro da Capital do Estado de São Paulo por mais privilegiado que outro seja, arcando a parte 
vencida com o ônus da sucumbência, inclusive honorários advocatícios de 20% do total da condenação.
<?php else : ?>
<h3>Condi&ccedil;&otilde;es Contratuais</h3>
<br />
Adere ao Contrato de Prestação de Serviços, Representação Financeira e de Fotos firmado entre a Comissão de Formatura de seu curso e a empresa ÁS EVENTOS LTDA., 
inscrita no CNPJ/MF sob N.º06.163.144/0001-45, com sede na Avenida José Maria Whitaker, 882 CEP: 04057-000, São Paulo – SP, 
representada neste ato pelo sócio-diretor RACHID SADER, portador do RG Nº 26.572.035-7, 
contrato esse assinado em <strong><?= !empty($turma['Turma']['data_assinatura_contrato']) ? date('d/m/Y', strtotime($turma['Turma']['data_assinatura_contrato'])) : "" ?></strong> que será denominado CONTRATO COLETIVO e regido nos seguintes moldes:
<br />
<br />
1. O formando acima qualificado reconhece a Comissão de Formatura de sua turma como legítima representante para tratar dos assuntos 
relacionados à sua Formatura, outorgando poderes para representá-lo em atos referentes à sua formatura, ratificando os termos do CONTRATO COLETIVO, 
declarando ter plenos conhecimentos desse contrato e outorgando poderes para a Comissão celebrar aditivos contratuais que se fizerem 
necessários à realização dos eventos.
<br />
<br />
2. As informações importantes a respeito do seu contrato estarão disponibilizadas no link: http://sistema.asformaturas.com.br. 
Caso precise do login e senha da sua turma, entre em contato com a sua comissão ou diretamente com a ÁS Formaturas.
<br />
<br />
3. A comissão de formatura não poderá tratar assuntos de cunho financeiro, tais como forma de pagamento, 
renegociação de dívida ou cancelamento de contrato em nome do formando.
<br />
<br />
4. O formando acima qualificado autoriza a ÁS Formaturas a representá-lo na contratação e pagamento de 
fornecedores conforme descrito no CONTRATO COLETIVO. O valor pago pelo CONTRATANTE permanecerá em posse 
da CONTRATADA de forma transitória, até o repasse aos fornecedores do pacote de formatura do quais a 
CONTRATADA é intermediária, ficando tão-somente com a comissão que lhe faz jus.
<br />
<br />
5. O(A) CONTRATANTE se obriga a pagar a CONTRATADA pelos eventos de formatura descritos no CONTRATO COLETIVO.
<br />
<br />
6. No momento da retirada dos convites o formando que optar por boletos bancários deverá quitar as parcelas 
em atraso e trocar os boletos ainda em aberto por cheques pré-datados. No mesmo momento, o formando que 
tiver optado por cheques pré-datados e ainda não tiver os entregue, deverá entregar os mesmos.
<br />
<br />
7. Os valores consignados neste contrato serão atualizados de 12 em 12 meses, a partir da assinatura do 
Contrato de Prestação de Serviços celebrado entre a Comissão de Formatura e a ÁS EVENTOS LTDA., 
<?= $this->data['Turma']['id'] == 5148 ? '( 12/06/2012 )' : "" ?> adotando - se a variação do IGPM/FGV do período, conforme autoriza a Lei 9.069/95, ou por outro índice que venha a substituí-lo. 
A ÁS Formaturas não é autorizada a realizar qualquer outro tipo de reajuste de valores contratuais.
<br />
<br />
8. Os convites só serão entregues após feito o acerto financeiro, isto é, pendências devidamente quitadas, 
isto inclui os valores relativos ao IGPM, cheques trocados e entregues , conforme explicitado nos itens 6 e 7.
<br />
<br />
9. No caso de rescisão contratual, o aluno poderá rescindir até 90 dias antes do término do ano letivo de 
conclusão do curso, desde que pago a título de multa o valor dos itens ou brindes já entregues ou contratados 
para os formandos adicionado de 20% do valor total do contrato. Após este período será exigido o pagamento 
integral do Contrato, exceto devido a motivos justificados e comprovados, tais como <?= $this->data['Turma']['id'] != 5148 ? 'reprovação ou ' : "motivos de saúde, óbito do formando e " ?>transferência 
por motivos profissionais, nesse caso ficando o formando livre de multa.
<br />
<br />
10. Constitui obrigação do(a) CONTRATANTE manter seu endereço e demais dados cadastrais atualizados, bem como a responsabilidade 
pela grafia correta de seu nome, nos textos originais que deverão ser entregues à CONTRATADA para a execução dos serviços gráficos, 
eximindo a CONTRATADA pela falta do nome e/ou grafia incorreta conforme previsão no Contrato Principal.
<br />
<br />
11. Considerando que os convites de luxo serão montados com fotos da turma e/ou formando, a Comissão de Formatura avisará todos 
os formandos da data/local e horário marcado junto à CONTRATADA para serem fotografados, estando o formando ciente que sua 
ausência isentará a CONTRATADA de qualquer responsabilidade referente as fotografias no convite.
<br />
<br />
12. A empresa não se responsabiliza por objetos pessoais em todos os eventos organizados pela empresa.
<br />
<br />
13. Fica eleito o Foro da Capital do Estado de São Paulo por mais privilegiado que outro seja, arcando a parte vencida com 
o ônus da sucumbência, inclusive honorários advocatícios de 20% do total da condenação.
<br />
<br />
A &Aacute;s formaturas n&atilde;o cobra taxa de emiss&atilde;o de boletos. Para imprimi-los, 
acesse o espa&ccedil;o do formando com o seu login e senha e tenha acesso &agrave; todos os seus boletos na &Aacute;rea Financeira .
<br />
<br />
Caso opte por pagar em cheque, lembramos que &eacute; sua responsabilidade a entrega dos cheques para o representante da &Aacute;s na faculdade ou diretamente em nossa sede. 
Sempre exija o seu recibo.
<br />
<br />
Lembramos que caso opte em pagar em cheque mas decida posteriormente pagar em boletos, 
n&atilde;o h&aacute; problemas, basta acessar sua &aacute;rea financeira e imprimir os boletos.
<br />
<br />
Al&eacute;m disso, a &Aacute;S disponibiliza um servi&ccedil;o adicional de envio dos boletos impressos.<br />
Caso queira receb&ecirc;-los em casa, opte por boletos impressos.
<br />
Será cobrado o valor de R$ 20,00 referente ao envio dos mesmos. Lembramos que essa taxa n&atilde;o &eacute; obrigatória e voc&ecirc; facilmente pode acessar todos os seus boletos online.
Al&eacute;m disso, caso opte em receber os boletos na sua casa, você poder&aacute; tamb&eacute;m entrar no site e imprimir uma segunda via a qualquer momento.
<?php endif; ?>