<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
    <meta name="google-site-verification" content="Ip4-BMIP8v6t5ky279zPi7nhLEHl3vxifr2ccDarP30" />
    <?= $html->charset(); ?>
    <title><?= $title_for_layout; ?></title>
    <link rel="icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon"> 
    <link rel="shortcut icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon"> 
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootmetro/default.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootmetro/icons.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootmetro/responsive.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/fonts.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/typography.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/color.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/form.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/button.css?v=0.1">
    <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/default.css?v=0.1">
</head>
<body>
    <div id="menu-topo" class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container todo">
                <a class="brand">
                    <div class='logo-min-branco'></div>
                </a>
            </div>
        </div>
    </div>
    <div class="container todo">
            <div style="min-height:90px; display:block"></div>
            <div class="row-fluid">
                <div class="span8 offset2">
                    <?= $content_for_layout; ?>
                </div>
            </div>
        </div>
    </div>
</body>
</html>