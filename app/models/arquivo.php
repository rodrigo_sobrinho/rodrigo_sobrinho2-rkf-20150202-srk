<?php

class Arquivo extends AppModel {

    var $name = "Arquivo";
    var $hasAndBelongsToMany = array(
        'Mensagem' =>
        array(
            'className' => 'Mensagem',
            'joinTable' => 'arquivos_mensagens',
            'foreignKey' => 'arquivo_id',
            'associationForeignKey' => 'mensagem_id',
            'unique' => true
        )
    );
    var $belongsTo = array('Usuario');

    public function file_path() {
        return Configure::read("FileSystemDirectoryPath");
    }

    /**
     * A partir de um objeto "arquivo" do banco ele retorna qual seu caminho
     * absoluto
     * @param model Arquivo correspondendo a um objeto oriundo do banco
     * de dados
     * @return string com o caminho absoluto do arquivo no servidor
     */
    function getArquivo(&$Model = null) {
        return $this->data['Arquivo']['diretorio'] .
                $this->data['Arquivo']['nome_secreto'];
    }

    /**
     * Antes de salvar, o model deve setar qual a pasta em que está sendo
     * salvo o arquivo e o nome que este possuira no servidor, ainda deve
     * ser garantido que o arquivo foi transferido corretamente
     * @return boolean indicador se o arquivo foi salvo corretamente
     */
    function beforeSave($options = array()) {
        if(isset($this->data[$this->name]['tmp_name'])){
            ini_set('upload_max_filesize','30M');
            $this->data[$this->name]['diretorio'] = $this->file_path();
            $this->data[$this->name]['nome_secreto'] = $this->generateUniqueId();
            return move_uploaded_file($this->data[$this->name]['tmp_name'], $this->file_path() . $this->data[$this->name]['nome_secreto']);
        }else
        return true;
    }

    function beforeDelete($cascade) {
        if (is_file($this->file_path() . $this->data[$this->name]['nome_secreto'])) {
            unlink($this->file_path() . $this->data[$this->name]['nome_secreto']);
        }
        return true;
    }

    /**
     * Cria um identificador unico para o servidor.
     * @return string correspondendo ao identificador unico
     */
    public function generateUniqueId() {
        return md5(uniqid("arquivo_"));
    }
    
    public function saveBase64File($str,$arquivo) {
        $retorno = false;
        if($f = fopen($arquivo,'a'))
            if(fwrite($f,base64_decode($str)))
                $retorno = $arquivo;
        return $retorno;
    }

}

?>
