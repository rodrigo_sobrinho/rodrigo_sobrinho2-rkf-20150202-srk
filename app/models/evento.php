<?php

class Evento extends AppModel {

    var $name = 'Evento';
    var $belongsTo = array('Turma', 'TiposEvento');
    var $hasMany = array('Extra');
    var $hasOne = array('TurmaMapa');
    //var $actsAs = array('Containable');

    var $validate = array(
        'nome' => array(
            'vazio' => array(
                'rule' => 'notEmpty',
                'message' => 'Preencha o nome.'
            )
        )
    );
    
    function obterEventoMapa($id) {
        $this->unbindModel(array(
            'belongsTo' => array('Turma'),
            'hasMany' => array('Extra')
        ),false);
        $evento = $this->find('first',array(
            'conditions' => array(
                'Evento.id' => $id,
            ),
            'order' => array('Evento.id' => 'desc')
        ));
        return $evento;
    }

}

?>