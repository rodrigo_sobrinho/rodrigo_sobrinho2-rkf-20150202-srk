<?php

class Contrato extends Model {
    var $name = "Contrato";

    var $belongsTo = array ('Turma', 'Usuario');
        
    private function file_path(){
		return Configure::read("FileSystemDirectoryPath");
	}

    /**
     * A partir de um objeto "contrato" do banco ele retorna qual seu caminho
     * absoluto
     * @param model Contrato correspondendo a um objeto oriundo do banco
     * de dados
     * @return string com o caminho absoluto do arquivo no servidor
     */
    function getContrato(&$Model = null){
        return $this->data['Contrato']['diretorio'] .
                $this->data['Contrato']['nome_secreto'];
    }

    /**
     * Antes de salvar, o model deve setar qual a pasta em que está sendo
     * salvo o arquivo e o nome que este possuira no servidor, ainda deve
     * ser garantido que o arquivo foi transferido corretamente
     * @return boolean indicador se o arquivo foi salvo corretamente
     */
    function  beforeSave($options = array()) {
        if(isset($this->data[$this->name]['tmp_name'])){
            $this->data[$this->name]['diretorio'] = $this->file_path();
            $this->data[$this->name]['nome_secreto'] = $this->generateUniqueId();
            return move_uploaded_file($this->data[$this->name]['tmp_name'], 
                    $this->file_path() . $this->data[$this->name]['nome_secreto']);
        } else
            return true;
    }

	function beforeDelete($cascade){
		if(is_file($this->file_path().$this->data[$this->name]['nome_secreto']))
        {
            unlink($this->file_path().$this->data[$this->name]['nome_secreto']);
        }
		return true;
	}

    /**
     * Cria um identificador unico para o servidor.
     * @return string correspondendo ao identificador unico
     */
    private function generateUniqueId(){
        return md5(uniqid("contrato_"));
    }
}
?>
