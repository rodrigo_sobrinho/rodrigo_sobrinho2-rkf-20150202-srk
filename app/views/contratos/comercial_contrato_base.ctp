<?php
/* 
 * Dados recebidos do controller são:
 * $parametros['faculdade']
 * $parametros['curso']
 * $parametros['numero_aderentes']
 * $turma['data_conclusao']
 * $turma['valor_formando']
 * $turma['data_assinatura_contrato']
 * $turma['beneficios_comissao']
 * $turma['negociacoes_contratuais']
 * $turma['valor_total_contrato']
 */

App::import('Vendor','xtcpdf');  
$tcpdf = new XTCPDF();
$textfont = 'freesans'; // looks better, finer, and more condensed than 'dejavusans' 

$tcpdf->SetAuthor("RK Formaturas http://rkformaturas.com.br/"); 

$textfont = 'helvetica';
 
$tcpdf->SetAuthor("RK Formaturas");
 
$tcpdf->setPrintHeader(false);
$tcpdf->setPrintFooter(false);
$tcpdf->SetMargins(30, 40, 30);
$tcpdf->SetTextColor(0, 0, 0);
$tcpdf->SetFont($textfont,'',9);
 
$tcpdf->AddPage();

// Cria string de formandos com nome e RG
$lista_comissao = $this->data['Contrato']['nomes_rgs'];

$lista_cursos = $this->data['Contrato']['cursos'];

$faculdade = $this->data['Contrato']['faculdade'];

// create some HTML content
$htmlcontent = 

"
<p><b>Pelo presente Contrato, as partes:</b></p>

<p>RK FORMATURAS LTDA., sociedade brasileira por quotas de responsabilidade limitada, inscrita no CNPJ sob o n.º 06.163.144/0001-45, estabelecida no estado de São Paulo, onde é sediada à Av. José Maria Whitaker, 882, Planalto Paulista, CEP 04057-000, neste ato legalmente representada por seu sócio, o Sr. Rachid Sader, brasileiro, solteiro , administrador de empresa, portador da cédula de identidade RG n.º 26.572.035-7 SSP/SP e inscrito no CPF/MF sob o n.º 289.174.098-06, residente e domiciliado à Rua Nhandu 308, Planalto Paulista na Cidade de São Paulo, Estado de São Paulo</p>

<p>E de outro lado, os formandos da: <b>".$faculdade."</b><br>
Representando o(s) curso(s): <b>".$lista_cursos."</b><br>
Com previsão de formatura para: <b>". $this->data['Contrato']['previsao_conclusao']."</b></p>

<p>Representados pela comissão de formatura, neste ato denominado simplesmente “Contratante”,</p>

<p>Tem entre si justo e contratado o presente Contrato de Prestação e Representação de Serviços (doravante denominado \"Contrato de Prestação de Serviço\"), que será regido pelos termos e condições a seguir estipulados, em conformidade com os artigos 125 e seguintes do Código Civil Brasileiro:</p>

<p>I - ANEXOS</p>

<p>1. São parte integrante do Contrato os seguintes anexos:</p>

<p>1.1 Anexo A - Planilha de Preços e Serviços Aprovada referente à festa de formatura- Planilha aprovada em <b>".$this->data['Contrato']['data_contrato']."</b>, pela Contratante e Contratada, que contém os serviços e produtos definidos pelas partes, assim <b>como a explicitação da taxa administrativa praticada pela ÁS eventos e que devem ser mantidos até o</b> <b>fiel cumprimento de todos os serviços descritos no Anexo A.</b></p>

<p>1.2 Anexo B – Termo de Finalização da negociação – Ata padrão, incluindo acertos verbais estabelecidos entre a empresa e a comissão de formatura.</p>

<p>1.3. Anexo C – Ata de Constituição da Contratante;</p>

<p>1.4. Anexo D - Modelo do Termo de Adesão e Compromisso - Contrato de adesão individual a ser firmado entre cada formando nos cursos acima qualificados, (doravante denominados coletivamente como \"Formandos\" ou individualmente como \"Formando\") e a Contratada, cujos termos ficarão vinculados ao presente Contrato tão logo cada Formando venha a aderir ao evento de formatura (doravante denominado o</p>

<p>\"Evento\"), presente objeto deste Contrato e descrito no Anexo A;</p>

<p>1.5. Anexo E - Contrato de Cobertura Fotográfica e de Filmagem;</p>

<p>1.6. Anexo F - Boletim Informativo, com as formas de pagamento disponíveis para os formandos.</p>

<p>II – OBJETO E CONSIDERAÇÕES INICIAIS</p>

<p>2.1. A Contratada compromete-se a <b>representar a Contratante</b>, e providenciar a contratação de todos os serviços e produtos descritos no Anexo A ao presente instrumento (doravante denominados respectivamente \"Serviços\" e \"Produtos\"), bem como realizar todo o serviço de cobrança e captação de recursos para a contratante;</p>

<p>2.2. A Contratada declara ter estrutura adequada para atender ao objeto do presente Contrato e assume, neste ato, o compromisso de organizar e fazer realizar todas as festividades descritas no Anexo A dos alunos representados pelos Contratantes.</p>

<p>2.3 O valor pago individualmente pelos formandos permanecerá em posse da CONTRATADA de forma transitória, até o repasse aos fornecedores do pacote de formatura do quais a CONTRATADA é intermediária, ficando tão-somente com a comissão que lhe faz jus;</p>

<p>2.4. Ficam estipuladas as seguintes condições gerais do contrato:</p>

<p>2.4.1 Número de Formandos aderentes: <b>".$this->data['Contrato']['num_formandos']." </b></p>

<p>2.4.2 Valor total do contrato: <b>".$this->data['Contrato']['valor_contrato']." </b></p>

<p>2.4.3 Valor por formando: <b>".$this->data['Contrato']['valor_formando']." </b></p>

<p><b>2.5. Fica desde já estabelecido que a comissão de formatura que assina esse Contrato não tem nenhum tipo de obrigação ou responsabilidade civil ou financeira referente a este. Assim sendo, toda e qualquer obrigação ou responsabilidade civil ou financeira é de inteira responsabilidade da CONTRATADA.</b></p>

<p>III - PREÇOS E REAJUSTES</p>

<p>3. A Contratada obriga-se a realizar os Serviços do Anexo A, representando a contratante na contratação de terceiros pelo preço estabelecido no item 2.4.2 – Valor total do contrato - rateado entre o número de formandos definidos no item 2.4.1 – Número de Formandos Aderentes - cabendo individualmente ao Formando a responsabilidade (conforme o Termo de Adesão e Compromisso) pelo pagamento da cota estipulada no item 2.4.3 – Valor por formando - que poderão ser quitados conforme as opções de pagamento descritas no anexo F – Boletim Informativo.</p>

<p>3.1. Os valores das parcelas remanescentes serão reajustados após o 12º mês conforme a variação do índice IGP-M, divulgado pela Fundação Getúlio Vargas. Caso esse índice seja extinto, o índice de referência para o cálculo de reajuste será substituído por outro índice que venha a ser publicado pelo Governo para substituí-lo qualitativamente, ou seja, para refletir a inflação do período acima descrito. Na ausência de um índice oficial publicado pelo Governo, as partes acordarão, de boa fé, acerca do índice de reajuste alternativo.</p>

<p>3.1.1. A Contratada garante, desde já, que se obriga à prestação de todos os serviços, nas mesmas condições e preços de todos os seus anexos, caso o número de Formandos Aderentes atinja o número mínimo estabelecido no item 2.4.1 – Numero de Formandos aderentes. Caso esse número não seja atingido, as partes se comprometem a repactuar este Contrato ao número de formandos participantes, no entanto sem onerar ou aumentar as cotas individuais de cada formando.</p>

<p>3.1.2. É de inteira responsabilidade de a Contratada assumir os reajustes de preços dos serviços e locações constantes na Planilha de Orçamento, não cabendo quaisquer responsabilidades à Contratante, onde entendido e acordado entre as partes, a Contratada terá direito de efetuar a venda dos convites e mesas excedentes, desde que exclusivamente para os formandos que aderiram ao pacote de formatura.</p>

<p>3.2. Entende-se por Formando aderente, o Formando que aderir ao Termo de Adesão e Compromisso e cumprir com as obrigações financeiras estabelecidas e não desistir da formatura.</p>

<p>3.3. Em caso de falecimento, reprovação, transferência de universidade, transferência de Estado, desistência do curso, ou enfermidade, o Formando (ou seu representante legal) receberá a quantia já paga corrigida pelo mesmo índice previsto no Contrato.</p>

<p>3.4. A desistência por motivo diverso dos descritos acima, poderá ser feita até 90 dias antes do termino do ano letivo e implicará na perda de <b>20</b>% do valor contratual, em favor da Contratada. Caso a prestação dos serviços e/ou o fornecimento dos produtos já tenha sido parcialmente cumprido e verificando-se um dano real da Contratada, mediante comprovação de planilha de custo da mesma, o montante devido pelo tipo de desistência aqui descrita, não poderá ultrapassar <b>50</b>% (cinquenta por cento) do valor já pago. 1. Após este período será exigido o pagamento integral do Contrato.</p>

<p>3.5. Eventual atraso no pagamento das parcelas remanescentes acarretará multa de 10% (dez por cento), mais juros de mora de 1% (um por cento) ao mês sobre o valor em atraso, calculados desde a data do vencimento até a data do efetivo pagamento.</p>

<p>3.5.1. A Contratada se incumbirá de emitir e enviar boleto bancário de cobrança a cada formando no endereço por este fornecido, sempre com antecedência aos vencimentos de modo a proporcionar tempo hábil ao pagamento, sob pena de isenção da multa da cláusula.</p>

<p>IV - SERVIÇOS EXTRACONTRATUAIS</p>

<p>4. Os demais serviços e produtos não constantes do Anexo A que eventualmente a Contratante e a Contratada venham a acordar, tanto no preço quanto em sua prestação ou fornecimento, deverão ser inclusos em forma de Aditamento Contratual.</p>

<p>V - OBRIGAÇÕES E RESPONSABILIDADES DA CONTRATADA</p>

<p>5.1. Para o fiel cumprimento das obrigações contratuais que se originam do presente instrumento, cabe à Contratada:</p>

<p>a. Responsabilizar-se integralmente pela prestação dos serviços, execução de subcontratações, fornecimento de produtos e prestação de informações que se façam necessárias ou que sejam solicitadas pela Contratante;</p>

<p>b. Cumprir as posturas dos municípios, bem como as disposições legais, sejam elas estaduais ou federais, que interfiram na prestação dos serviços ou fornecimento dos produtos; Inclui taxa de ECAD, e projeto necessário para aprovação do CONTRU e outras taxas quando necessárias.</p>

<p>c. Manter durante toda a execução do Contrato em compatibilidade com as obrigações assumidas, todas as condições que culminaram em sua habilitação e qualificação para sua contratação.</p>

<p>d. Reparar, corrigir, remover, substituir, trocar, às suas expensas, a critério da Contratante no todo ou em parte, quaisquer itens constantes do Anexo A e Anexo E do presente Contrato, em que se verifiquem vícios, defeitos ou incorreções resultantes da execução ou de materiais empregados, por qualquer uma das partes, sem com isso repassar qualquer custo à Contratante, desde que por culpa exclusiva da Contratada, e ou terceiros contratados por esta, dentro de um prazo de 30 (trinta) dias úteis após a notificação por parte dos Contratantes à Contratada, salvo nos casos de produtos de uso inferior a 30 dias, que deverão ser trocados em tempo hábil para a utilização dos mesmos.</p>

<p>e. Fornecer para controle dos Contratantes listagens dos pagamentos efetuados pelos formandos, sempre que solicitados por escrito, sendo expressamente proibido aos Contratantes a sua divulgação pública.</p>

<p>f. Executar os serviços dentro dos melhores padrões de qualidade, utilizando-se do que for convencionado com os Contratantes, empregando material de primeira qualidade.</p>

<p>g. Operar com empregados, mesmo os terceirizados, devidamente treinados e preparados para a execução das atividades ora contratadas, em número suficiente à perfeita prestação dos serviços.</p>

<p>h. A Contratada se responsabilizará por todo dano causado em qualquer convidado nos eventos ora contratados que sejam oriundos da montagem, de seus funcionários ou prepostos (garçom, segurança, equipe de coordenação, recepcionistas, etc...)</p>

<p>i. A Contratada, de posse dos Termos de Adesão, mesmo que haja problemas quanto ao pagamento das parcelas, se compromete a cumprir o que ora fica convencionado neste instrumento, exceto nos casos de efetiva desistência, onde a Contratada se obriga à devolução de parte da quantia paga ao formando.</p>

<p>j. Caso o número de formandos que realizarem adesão for superior ao estabelecido no item 2.4.1 – Número de Formandos Aderentes, é obrigação da CONTRATADA reverter 50% de cada adesão adicional para um fundo a ser usado pela CONTRANTANTE. Esse fundo de caixa pode ser usado para novos investimentos nos eventos contratados, na inclusão de novos itens no evento, contratação de novos serviços ou até mesmo reembolso aos formandos.</p>

<p>VI - OBRIGAÇÕES DA CONTRATANTE</p>

<p>6.1. Para a prestação dos serviços e fornecimento dos produtos, a Contratante obriga-se a:</p>

<p>a. Fornecer o número mínimo de formandos pagantes estabelecido no item 2.4.1 – Número de Formandos Aderentes, possibilitando-se, assim, a quitação total do presente instrumento.</p>

<p>a.1. Caso o número de adesões de formandos pagantes, participantes dos eventos, previsto neste Contrato não seja alcançado até o fim do ano letivo, a Contratada, após solicitação por escrito dos Contratantes, poderá alterar os produtos/serviços ora contratados, de forma a compensar o débito eventualmente existente, uma vez que o custo individual a ser pago pelo formando, a pedido exclusivo dos Contratantes, foi calculado dividindo-se o custo total deste instrumento pelo número estabelecido no item 2.4.1 – Número de Formandos Aderentes. Esse valor pago individualmente não sofrerá em hipótese alguma nenhum tipo de reajuste.</p>

<p>a.2. A alteração de produtos / serviços será feita de comum acordo entre a Contratada e o Contratante.</p>

<p>a.3. Definida a alteração de produtos / serviços a Contratada sugerirá aos Contratantes a adequação do Contrato os quais terão um prazo de 15 (quinze) dias para sua alteração ou formulação de nova sugestão.</p>

<p>b. Divulgar o presente Contrato junto aos formandos a fim de se atingir o número mínimo de adesões.</p>

<p>c. Fornecer as informações necessárias à fiel execução do presente Contrato, em tempo hábil, determinado quando da solicitação das mesmas;</p>

<p>d. No que se refere aos convites (descritos no Anexo A), fornecer especificações relativas aos textos, fotolitos, desenhos, distintivos, brasões, medalhas, gravuras ou quaisquer outras especificações que tenha sido previamente tratadas, com a antecedência de 60 (sessenta) dias da data de entrega do serviço.</p>

<p>6.2. Uma vez confirmados as datas junto a Contratada e esta comprometendo-se com terceiros, os Contratantes não mais poderão alterá-las, salvo motivo de força maior.</p>

<p>VII - OBRIGAÇÕES RECÍPROCAS</p>

<p>7. Será de responsabilidade da Contratante e da Contratada, a formulação de listas como os nomes e dados relevantes dos Formandos, para que os mesmos façam as devidas alterações e correções em tempo hábil. A fiscalização dos convites já impressos também será de responsabilidade de ambas as partes.</p>

<p>VIII – DOS DIREITOS DOS CONTRATANTES</p>

<p>8.1. Solicitar relatórios à Contratada, a qualquer momento, que demonstrem a situação financeira do presente contrato, bem como, da situação individual do formando, sobre as quais deverá manter sigilo.</p>

<p>8.2. Realizar reuniões com o atendimento da Contratada, desde que marcada com antecedência mínima de 5 (cinco) dias úteis, encaminhando obrigatoriamente o pedido por e-mail, contendo a pauta da reunião, com clara especificação dos assuntos a serem abordados, a fim de que a Contratada possa programar-se, visando a antecipação das possíveis soluções.</p>

<p>8.3. Verificar e fiscalizar todos e quaisquer produtos que estão orçados no seu orçamento, através de degustações, apresentações, visita a fornecedores terceirizados através de solicitação prévia para o atendimento da CONTRATADA.</p>

<p>IX – FORO</p>

<p>9. Fica eleito o Foro Central da Comarca da capital do Estado de São Paulo, para dirimir eventuais questões oriundas da celebração, interpretação e execução do presente Contrato, com renúncia a qualquer outro, por mais privilegiado que seja, arcando a parte vencida com todo o ônus de sucumbência, inclusive honorários advocatícios de 20% (vinte por cento) do total da condenação.</p>

<p>X - FOTOGRAFIA E FILMAGEM</p>

<p>10. A prestação dos serviços de cobertura fotográfica e de filmagem dos Eventos, bem como a venda das respectivas fotografias e fitas editadas deverá obedecer ao disposto no Anexo E.</p>

<p>XI – DOCUMENTOS</p>

<p>11.1. O presente Contrato, seus respectivos anexos e eventuais aditamentos são os únicos instrumentos legais e reguladores dos serviços, produtos e eventos ora contratados, substituindo quaisquer documentos anteriormente trocados entre Contratante e Contratada.</p>

<p>11.2. Todos os documentos e/ou cartas entre a Contratante e a Contratada serão trocados através de expediente protocolado ou e-mail. Os documentos a serem entregues à Contratante deverão ser recebidos por qualquer membro da comissão de formatura.</p>

<p><b>São Paulo, ".$this->data['Contrato']['data_contrato'].".</b></p>

<p>PELA CONTRATADA</p>

<p>_______________________________________</p>

<p>RACHID SADER</p>

<p>R.G. 26.572.035-07</p>

".$lista_comissao;

// output the HTML content
$tcpdf->writeHTML($htmlcontent, true, 0, true, 0, 'J');

echo $tcpdf->Output(date('Y-m-d H:i:s').' Contrato Base.pdf', 'D'); 
?>