<style type="text/css">
#conteudo-container { margin:10px 0 0 0 !important }
h2.titulo { background-color:#C7C5C6; color:black; padding:5px }
</style>
<div style="overflow:hidden; width:100%" class="clearfix">
	<div class="legenda" style="width:98%; margin:20px 0 30px 0; border:1px solid #989999">
		<div style='color:#AA0000'>
			<img src="<?=empty($formando['diretorio_foto_perfil']) ? "{$this->webroot}img/uknown_user.gif" : "{$this->webroot}{$formando['diretorio_foto_perfil']}"?>" style='float:left; margin:0 5px 15px 0; width:120px' />
			<p>
				<strong style='text-transform:uppercase'><?=$formando['nome']?></strong>
				<br />
				<em><?=$formando['grupo']?></em>
				<br />
				<em>Código <?=$formando['codigo_formando']?></em>
				<br />
				<em>RG <?=$formando['rg']?></em>
			</p>
		</div>
		<table>
			<tr>
				<td>
					<table>
						<tr>
							<td colspan="2" width="50%"><h2 class='titulo'>Informa&ccedil;&otilde;es da ades&atilde;o</h2></td>
							<td colspan="2" width="50%"><h2 class='titulo'>Resumo financeiro</h2></td>
						</tr>
						<tr height="5">
							<td colspan="4"></td>
						</tr>
						<tr>
							<td width="25%">Data contrato da Turma</td>
							<td width="25%"><?=date('d/m/Y', strtotime($turma['Turma']['data_assinatura_contrato']));?></td>
							<td width="25%">Valor de Contrato</td>
							<td width="25%" class='valor-contrato'>R$<?=number_format($formando['valor_adesao'],2,',','.');?></td>
						</tr>
						<tr>
							<td>Data Adesão</td>
							<td><?=date('d/m/Y', strtotime($formando['data_adesao']));?></td>
							<td>Valor de IGPM</td>
							<td><?=$totalIGPM > 0 ? "R$" . number_format($totalIGPM,2,',','.') : "-";?></td>
						</tr>
						<tr>
							<td>Data IGPM</td>
							<td><?=!empty($turma['Turma']['data_igpm']) ? date('d/m/Y', strtotime($turma['Turma']['data_igpm'])) : "IGPM ainda não aplicado";?></td>
							<td>Valor de Contrato c/ Correção</td>
							<td class='total-contrato'>R$<?=number_format($formando['valor_adesao']+$totalIGPM,2,',','.');?></td>
						</tr>
						<tr>
							<td>Mesas do contrato</td>
							<td><?=$turma['Turma']['mesas_contrato'];?></td>
							<td>Valor de Campanhas</td>
							<td class='total-campanha'><?=$totalDespesasExtras > 0 ? "R$".number_format($totalDespesasExtras,2,',','.') : "-"?></td>
						</tr>
						<tr>
							<td>Convites do contrato</td>
							<td id='qtde-convites'><?=$turma['Turma']['convites_contrato'];?></td>
							<td>Convites Extras</td>
							<td class='saldo-convites'>-</td>
						</tr>
						<tr>
							<td>Número de Parcelas do Contrato</td>
							<td><?=$formando['parcelas_adesao'];?></td>
							<td>Total Recebido</td>
							<td class='total-pago'></td>
						</tr>
						<tr>
							<td colspan='2'>&nbsp;</td>
							<td>Pagamento Avulso</td>
							<td class='saldo-credito'></td>
						</tr>
						<tr>
							<td colspan='2'>&nbsp;</td>
							<td class='saldo-total-label'></td>
							<td class='saldo-total'></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
</div>