<style type="text/css">
    .custom-content-reveal{
        overflow-y: hidden!important;
    }
    .prop{
        font-size: 14px;
    }
    iframe{
        width: 350px;
        height: 250px;
        border:dashed 3px #000000;
        -moz-border-radius-topleft: 6px;
        -moz-border-radius-topright:5px;
        -moz-border-radius-bottomleft:5px;
        -moz-border-radius-bottomright:5px;
        -webkit-border-top-left-radius:6px;
        -webkit-border-top-right-radius:5px;
        -webkit-border-bottom-left-radius:5px;
        -webkit-border-bottom-right-radius:5px;
        border-top-left-radius:6px;
        border-top-right-radius:5px;
        border-bottom-left-radius:5px;
        border-bottom-right-radius:5px;
    }
</style>
<script type="text/javascript">
    $("#form").submit(function(e) {
        e.preventDefault();
        var dados = $("#form").serialize();
        var url = $(this).attr('action');
        $.ajax({
            url: url,
            data: dados,
            type: "POST",
            dataType: "json",
            success: function(data) {
                if(data == 0){
                    $('#resposta').append("<h4 class='fg-color-greenDark'>Fotógrafos cadastrados com sucesso.</h4>");
                }else{
                    $('#resposta').append("<h4 class='fg-color-red'>Erro ao alocar fotógrafo(s).</h4>");
                }
            }
        });
        
        setTimeout(function() {
            var filho = $('#resposta').children();
            $('#resposta').children().fadeOut(300, function(){
                filho.remove();
            });
        }, 3000);
    });
    
    $("#google").change(function() {
        if(this.checked) {
            $.ajax({
                url: '/calendario/google/<?=$evento['Evento']['id']?>',
                type: "POST",
                dataType: "json",
                success: function(response) {
                    console.log(response.data);
                    if(response.data == 1){
                        $('.not-checked').fadeOut(400);
                        $('.checks').append("<div class='checked'><span class='fg-color-greenDark'>Google <i class='icon-checkmark'></i></span></div>").fadeIn(800);
                    }else{
                        bootbox.alert('Ocorreu um erro ao atualizar google.');
                    }
                }
            });
        }
    });
    
    $('#EventoFotografos').click(function(){
        var filho = $('#resposta').children();
        $('#resposta').children().fadeOut(300, function(){ 
            filho.remove();
        });
    });
</script>
<?php if($evento) : ?>
<div class='row-fluid'>
    <div class="span6">
        <h4 class="fg-color-red pull-left">
            <?="{$evento['Evento']['nome']} - {$evento['Turma']['id']} {$evento['Turma']['nome']} - {$evento['Turma']['ano_formatura']}/{$evento['Turma']['semestre_formatura']}"?>
        </h4>
    </div>
    <div class="span6">
        <h4 class="fg-color-greenDark pull-right">
            Total: <?=$formandos?> Formandos
        </h4>
    </div>
    <h6 class="fg-color-gray pull-right">
            Data Cadastro: <?=($evento['Evento']['data_cadastro']) ? 
                date('d/m/Y &\a\g\r\a\v\e;\s H:i:s',strtotime($evento['Evento']['data_cadastro'])) : 
                "<em class='fg-color-red'>Não informado</em>"?>
        </h6>
</div>
<br />
<p class='evento-detalhes'>
    <div class="row-fluid">
        <span class="span6">
            <b><em class='prop'>Tipo:</em></b>
            <?=($evento['TiposEvento']['nome']) ? $evento['TiposEvento']['nome'] : "<em class='fg-color-red'>Não informado</em>"?>
            <br />
            <br />
            <b><em class='prop'>Data:</em></b>
                <?=($evento['Evento']['data']) ? date('d/m/Y &\a\g\r\a\v\e;\s H:i:s',strtotime($evento['Evento']['data'])) : "<em class='fg-color-red'>Não informado</em>"?>
            <br />
            <br />
            <b><em class='prop'>Duração:</em></b>
                <?=($evento['Evento']['duracao']) ? $evento['Evento']['duracao'] : "<em class='fg-color-red'>Não informado</em>"?>
            <br />
            <br />
            <b><em class='prop'>Planejamento:</em></b>
                <?php
                    foreach($planejamento as $p)
                        echo $p['Usuario']['nome'] . ", ";
                ?>
            <br />
            <br />
            <b><em class='prop'>Local:</em></b>
                <?=($evento['Evento']['local']) ? $evento['Evento']['local'] : "<em class='fg-color-red'>Não informado</em>"?>
            <br />
            <br />
            <b><em class='prop'>Endereço:</em></b>
                <?=($evento['Evento']['local_endereco']) ? $evento['Evento']['local_endereco'] : "<em class='fg-color-red'>Não informado</em>"?>
            <br />
            <br />
        </span>
        <span class="span6">
            <b><em class='prop'>Status:</em></b>
                <?=($evento['Evento']['status']) ? $evento['Evento']['status'] : "<em class='fg-color-red'>Não informado</em>"?>
            <br />
            <br />
            <b><em class='prop'>Banda:</em></b>
                <?=($evento['Evento']['banda']) ? $evento['Evento']['banda'] : "<em class='fg-color-red'>Não informado</em>"?>
            <br />
            <br />
            <b><em class='prop'>Atracoes:</em></b>
                <?=($evento['Evento']['atracoes']) ? $evento['Evento']['atracoes'] : "<em class='fg-color-red'>Não informado</em>"?>
            <br />
            <br />
            <b><em class='prop'>Buffet:</em></b>
                <?=($evento['Evento']['buffet']) ? $evento['Evento']['buffet'] : "<em class='fg-color-red'>Não informado</em>"?>
            <br />
            <br />
            <b><em class='prop'>Bar:</em></b>
                <?=($evento['Evento']['bar']) ? $evento['Evento']['bar'] : "<em class='fg-color-red'>Não informado</em>"?>
            <br />
            <br />
            <b><em class='prop'>Brindes:</em></b>
                <?=($evento['Evento']['brindes']) ? $evento['Evento']['brindes'] : "<em class='fg-color-red'>Não informado</em>"?>
            <br />
            <br />
            <?php if($usuario['Usuario']['grupo'] == 'foto') : ?>
                <div class="checks">
                    <?php if($evento['Evento']['google'] == 0) : ?>
                        <div class="not-checked">
                            <label class="input-control checkbox">
                                <input type="checkbox" id="google">
                                <span class="helper">Google?</span>
                            </label>
                        </div>
                    <?php else : ?>
                        <div class="checked">
                            <span class="fg-color-greenDark">Google <i class="icon-checkmark"></i></span>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </span>
    </div>
    <div class="row-fluid">
        <span class="span12">
            <b><em class='prop'>Outras Informações:</em></b>
                <?=($evento['Evento']['outras_informacoes']) ? $evento['Evento']['outras_informacoes'] : "<em class='fg-color-red'>Não informado</em>"?>
            <br />
            <br />
        </span>
    </div>
    <div class="row-fluid" style="min-height: 400px">
        <div class="span6">
            <label class="strong">Mapa</label>
            <?php if(!empty($evento['Evento']['local_mapa'])) : ?>
            <?php echo $evento['Evento']['local_mapa'];?>
            <?php else : ?>
            <label class="strong fg-color-red">Mapa do local n&atilde;o dispon&iacute;vel.</label>
            <?php endif; ?>
        </div>
        <?php if(in_array($usuario['Usuario']['grupo'],array('video','foto'))) : ?>
        <?php echo $form->create('Evento', array('url' => "/calendario/inserir_fotografo/", 'id' => 'form')); ?>
        <?php echo $form->input('id', array('hiddenField' => true, 'value' => $evento['Evento']['id'])); ?>
        <div class="span6">
            <label class="strong">Fotógrafos/Cinegrafistas</label>
            <label id="resposta"></label>
                <?php echo $form->input('informacoes_fotografos', array(
                    'class' => 'input-control text', 'label' => false, 'div' => false, 'error' => false,
                    'value' => preg_replace('/<br\\s*?\/??>/i', '', $evento['Evento']['informacoes_fotografos']), 'style' => 'width: 397px; height: 293px'
                )); ?>
            <br />
        <?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'button mini bg-color-greenDark enviar'));?>
        </div>
        <?php else : ?>
        <div class="span6">
            <label class="strong">Fotógrafos</label>
            <p>
                <?=$evento['Evento']['fotografos'];?>
            </p>
        </div> 
        <?php endif; ?>
    </div>
</p>
<?php else : ?>
<h2 class="fg-color-red">Evento nao encontrado.</h2>
<?php endif; ?>
