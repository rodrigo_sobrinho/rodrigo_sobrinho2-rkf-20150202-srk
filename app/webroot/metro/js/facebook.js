$(document).ready(function() {
    $.ajax({
        url: '//connect.facebook.net/en_US/all.js',
        dataType: "script",
        cache : true,
        success: function(){
            FB.init({
                appId: '724822500864729',
                status: true,
                cookie: true,
                xfbml: true
            });
            $("body").addClass('fbinit').trigger('fbinit');
            FB.Event.subscribe('auth.authResponseChange', function(response) {
                if(response.status === 'connected') {
                    $("body").trigger({
                        type : 'conta-logada',
                        tipo : 'facebook',
                        response : response
                    });
                }
            });
        }
    });
});