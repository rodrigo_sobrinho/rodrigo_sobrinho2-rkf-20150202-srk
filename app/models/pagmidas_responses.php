<?php

class PagmidasResponses extends Model {
	var $name = "PagmidasResponses";
	var $useTable = 'pagmidas_responses';
	var $belongsTo = array(
		'PagmidasStatusCodigos' => array(
			'className' => 'PagmidasStatusCodigos',
			'foreignKey' => false,
			'conditions' => array('PagmidasResponses.status_transacao = PagmidasStatusCodigos.codigo')
		),
	);
}
?>