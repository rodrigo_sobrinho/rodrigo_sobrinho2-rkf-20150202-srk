<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/chosen.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/chosen.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".chosen").chosen({width:'100%'});
    })
</script>
<?=$form->create('Turma', array(
    'url' => "/{$this->params['prefix']}/registros_internos/editar",
    'id' => 'formulario-registro'
)); ?>
<div class="row-fluid">
    <div class="span11">
        <label>Concorrentes</label>
        <?=$form->input('concorrentes',array(
            'label' => false,
            'div' => 'input-control text',
            'error' => false)); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span5">
        <label>Data Prevista Fechamento</label>
        <?=$form->input('data_prevista_fechamento',array(
            'label' => false,
            'div' => 'input-control text',
            'error' => false)); ?>
    </div>
    <div class="span5 offset1">
        <?=$form->input('expectativa_fechamento', array(
            'options' => $expectativa_fechamentos,
            'type' => 'select',
            'class' => 'chosen',
            'label' => 'Expectativa Fechamento',
            'div' => false)); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span11">
        <label>Memorando</label>
        <?=$form->input('memorando',array(
            'label' => false,
            'rows' => '5',
            'div' => 'input-control textarea',
            'error' => false)); ?>
    </div>
</div>
<?=$form->end(array('label' => false, 'div' => false, 'style' => 'display:none')); ?>