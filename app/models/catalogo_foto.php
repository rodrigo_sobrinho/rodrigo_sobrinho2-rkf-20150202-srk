<?php

class CatalogoFoto extends AppModel {

    var $name = 'CatalogoFoto';
    var $useTable = 'catalogos_fotos';
    var $belongsTo = 'Catalogo';
    var $order = "ordem";
    
    function afterSave($created) {
        parent::afterSave($created);
        if($created && isset($this->data["CatalogoFoto"]['src']) && isset($this->data["CatalogoFoto"]['ext'])) {
            $path = WWW_ROOT . "upload/catalogo_fotos/{$this->id}/";
            $salvou = false;
            if(mkdir($path,0777,true)) {
                $file = "{$path}default.{$this->data["CatalogoFoto"]['ext']}";
                if ($f = fopen($file, 'a'))
                    if (fwrite($f, base64_decode($this->data["CatalogoFoto"]['src'])))
                        $salvou = true;
            }
            if(!$salvou) $this->delete($this->id);
        }
        return true;
    }
    
    function afterDelete() {
        parent::afterDelete();
        $path = WWW_ROOT . "upload/catalogo_fotos/{$this->id}/";
        exec("rm -rf $path");
    }

}

?>