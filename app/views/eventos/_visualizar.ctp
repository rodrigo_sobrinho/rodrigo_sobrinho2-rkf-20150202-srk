<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Evento</label>
	<label class="grid_6 alpha">Tipo</label>
	<span class="grid_5 alpha first"><?php echo $evento['Evento']['nome']; ?></span>
	<span class="grid_6 alpha"><?php echo $evento['TiposEvento']['nome']; ?></span>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Local</label>
	<span class="grid_8 alpha first"><?php echo $evento['Evento']['local']; ?></span>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Endereço</label>
	<span class="grid_8 alpha first"><?php echo $evento['Evento']['local_endereco']; ?></span>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Mapa</label>
	<span class="grid_8 alpha first"><?php echo $evento['Evento']['local_mapa']; ?></span>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Convites Formandos</label>
	<label class="grid_6 alpha">Convites Extras</label>
	<span class="grid_5 alpha first"><?php echo $evento['Evento']['convites_formandos']; ?></span>
	<span class="grid_6 alpha"><?php echo $evento['Evento']['convites_extras']; ?></span>	
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Data</label>
	<span class="grid_8 alpha first">
		<?php echo date('d/m/Y H:i',strtotime($evento['Evento']['data'])); ?>
	</span>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Duração</label>
	<span class="grid_8 alpha first"><?php echo $evento['Evento']['duracao']; ?></span>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Banda</label>
	<span class="grid_8 alpha first"><?php echo $evento['Evento']['banda']; ?></span>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Atrações</label>
	<span class="grid_8 alpha first"><?php echo $evento['Evento']['atracoes']; ?></span>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Buffet</label>
	<span class="grid_8 alpha first"><?php echo $evento['Evento']['buffet']; ?></span>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Bar</label>
	<span class="grid_8 alpha first"><?php echo $evento['Evento']['bar']; ?></span>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Brindes</label>
	<span class="grid_8 alpha first"><?php echo $evento['Evento']['brindes']; ?></span>
</p>
<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Outras Informações</label>
	<span class="grid_8 alpha first"><?php echo $evento['Evento']['outras_informacoes']; ?></span>
</p>


<?php include("_visualizar_" . $tipoEvento['TiposEvento']['template'] . ".ctp"); ?>
