<span id="conteudo-titulo" class="box-com-titulo-header">Parceiros</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="container-tabela">
		<table>
			<thead>
				<tr>
                                        <th scope="col">Turma</th>
					<th scope="col">Cod. Formando</th>
					<th scope="col">Nome</th>				
					<th scope="col">Parceria</th>
                                        <th scope="col">Data Cadastro</th>
				</tr>
			</thead>
			<tfoot>

			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach($relatorio as $resultado): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
                                        <td colspan="1" width="20%"><?php echo $resultado['f']['turma_id'];?></td>
					<td colspan="1" width="20%"><?php echo $resultado['f']['codigo_formando'];?></td>
					<td colspan="1" width="20"><?php echo $resultado['f']['nome'];?></td>
                                        <td colspan="1" width="20%"><?php echo $resultado['po']['parceria'];?></td>
					<td colspan="1" width="20%"><?php echo $resultado['pu']['data_cadastro'];?></td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>