<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/chosen.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/form_validate.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/fileupload.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/datepicker.css">
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/bootstrap/datepicker.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/chosen.js"></script>
<script type="text/javascript" src="<?=$this->webroot ?>metro/js/min/jquery.meio.mask.js"></script>
<script type="text/javascript" src="<?=$this->webroot?>metro/js/min/bootstrap/fileupload.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/jquery-validate.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".chosen").chosen({width:'100%'});
        $('#data-nascimento').datepicker();
        $('input[alt]').setMask();
        
        $('#formulario').validate({
            sendForm : false,
            onBlur: true,
            eachValidField : function() {
                $(this).removeClass('error').removeClass('form-error').addClass('success');
                var label = $('label[for="'+$(this).attr('id')+'"]');
                if(label.length > 0) {
                    if(label.children('span').length > 0)
                        label.children('span').fadeOut(500,function() { $(this).remove()});
                }
            },
            eachInvalidField : function() {
                $(this).removeClass('success').addClass('error');
            },
            description: {
                notEmpty : {
                    required : function() {
                        var label = $('label[for="'+$(this).attr('id')+'"]');
                        if(label.length > 0) {
                            if(label.children('span').length > 0)
                                label.children('span').html('').attr('class','fg-color-red');
                            else
                                label.append($('<span>',{class:'fg-color-red'}));
                            mensagem = $(this).data('error') ||
                                'Complete o campo';
                            label.children('span').html(mensagem);
                        }
                        return '';
                    },
                    conditional : function() {
                        var label = $('label[for="'+$(this).attr('id')+'"]');
                        if(label.length > 0) {
                            if(label.children('span').length > 0)
                                label.children('span').html('').attr('class','fg-color-red');
                            else
                                label.append($('<span>',{class:'fg-color-red'}));
                            mensagem = $(this).data('error-conditional') ||
                                'Complete o campo';
                            label.children('span').html(mensagem);
                        }
                        return '';
                    },
                    pattern : function() {
                        var label = $('label[for="'+$(this).attr('id')+'"]');
                        if(label.length > 0) {
                            if(label.children('span').length > 0)
                                label.children('span').html('').attr('class','fg-color-red');
                            else
                                label.append($('<span>',{class:'fg-color-red'}));
                            mensagem = $(this).data('error-pattern') ||
                                'Complete o campo';
                            label.children('span').html(mensagem);
                        }
                        return '';
                    }
                }
            },
            invalid: function() {
                    if($(this).find('[data-required="true"].error').length > 0) {
                        $("a[href='#"+$(this).attr('id')+"']").parent().addClass('error');
                    } else {
                        $("a[href='#"+$(this).attr('id')+"']").parent().removeClass('error');
                    }
            },
            valid: function(){
                var context = ko.contextFor($(".metro-button.reload")[0]);
                context.$data.showLoading(function() {
                dados = $('#formulario').serialize();
                    $.ajax({
                        url: '<?="/usuarios/editar_dados_funcionario/"?>',
                        dataType: "json",
                        type: "POST",
                        data: dados,
                        complete: function() {
                            context.$data.reload();
                        }
                    });
                });
            }
        });
        
        $('#fileupload').bind('loaded',function(e) {
            var src = e.imagem.replace(/^data:image\/(gif|png|jpe?g);base64,/, "");
            var extensao = e.imagem.match(/^data:image\/(gif|png|jpe?g);base64,/);
            if(src && extensao[1]) {
                $('#UsuarioFotoSrc').val(src);
                $('#UsuarioFotoExt').val(extensao[1]);
                $('#UsuarioFotoData').val(e.imagem);
            } else {
                $('#UsuarioFotoSrc').val('');
                $('#UsuarioFotoData').val('');
            }
            return;
        });
        
        function verificarEmail(){
            var input = $('#input-email');
            var span = input.parent().prev().children('em');
            var url = "/formando/formandos/verificaEmail/";
            if($('#input-email').val() != ''){
                $.ajax({
                    url: url,
                    data: { 
                        data: { 
                           email: $('#input-email').val()
                        }
                    },
                    type: "POST",
                    dataType: "json",
                    success: function(data) {
                        if(data.mensagem == 1)
                            span.addClass('fg-color-green').text('Email disponível.');
                        else
                            span.addClass('fg-color-red').text('Email não disponível');
                    },
                    error: function(data) {
                        span.addClass('fg-color-red').text('Erro ao verificar.');
                    }
                });
            };
        };
        
        var timer;
        
        $("#input-email").keyup(function() {
            var input = $('#input-email');
            if(input.parent().prev().children('em').length < 1)
                input.parent().prev().append('<em></em>');
            var span = input.parent().prev().children('em');
            span.attr('class', 'pull-right').text('Verificando...');
            if(timer != null)
                clearTimeout(timer);
            timer = setTimeout(function(){verificarEmail()}, 700);
        });

    });
</script>
<style type="text/css">
    h3 { text-align: center; color: white }
</style>
<?php $session->flash(); ?>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Editar Cadastro
        </h2>
    </div>
</div>
<?php echo $form->create('Usuario', array('url' => "/usuarios/editar_dados_funcionario", 'id' => 'formulario')); ?>
<?php echo $form->hidden('id', array('value' => $usuario['Usuario']['id'])); ?>
<?php echo $form->hidden('FormandoProfile.usuario_id', array('value' => $usuario['Usuario']['id'])); ?>
<?=$form->hidden('foto_src')?>
<?=$form->hidden('foto_ext')?>
<?=$form->hidden('Usuario.diretorio_foto_perfil', array('value' => "/{$usuario['Usuario']['diretorio_foto_perfil']}"))?>
<div class="row-fluid">
    <div class="span9">
            <div class="row-fluid">
                <div class="row-fluid">
                    <div class="span6">
                        <?php
                        if (isset($usuario['Usuario']['diretorio_foto_perfil']))
                            if (!empty($usuario['Usuario']['diretorio_foto_perfil']))
                                if (file_exists(APP . "webroot/{$usuario['Usuario']['diretorio_foto_perfil']}"))
                                    $fotoPerfil = $usuario['Usuario']['diretorio_foto_perfil'];
                        ?>
                        <div id='fileupload' class="fileupload <?=!empty($fotoPerfil) ? "fileupload-exists" : 'fileupload-new'?> row-fluid"
                            data-provides="fileupload">
                            <div class='span12 fileupload-new pull-left' style='margin-left:0'>
                                <a class='button bg-color-blueDark input-block-level btn-file'>
                                    Foto de Perfil
                                    <i class='icon-picture'></i>
                                    <input type="file" />
                                </a>
                            </div>
                            <div class='span6 fileupload-exists' style='margin-left:0'>
                                <a class='button mini bg-color-orange btn-file input-block-level'>
                                    Alterar
                                    <i class='icon-reload'></i>
                                    <input type="file" />
                                </a>
                            </div>
                            <div class='span6 fileupload-exists'>
                                <a class='button bg-color-red mini input-block-level' data-dismiss="fileupload">
                                    Remover
                                    <i class='icon-remove'></i>
                                </a>
                            </div>
                            <div class="fileupload-new thumbnail" style="width:100%; height: 180px">
                                <?=$html->image('no-image.gif') ?>
                            </div>
                            <div class="fileupload-preview fileupload-exists thumbnail"
                                 style="width:100%; height: 186px; line-height: 20px;" id='fileupload-preview'>
                                <?php if(!empty($fotoPerfil)):?>
                                    <img src="<?="{$this->webroot}$fotoPerfil"?>" />
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="span6">
                        <label class="required" for="input-nome">Nome</label>
                        <?=
                        $form->input('nome', array(
                            'label' => false,
                            'div' => 'input-control text',
                            'error' => false,
                            'id' => 'input-nome',
                            'value' => $usuario['Usuario']['nome'],
                            'data-required' => 'true',
                            'data-description' => 'notEmpty',
                            'data-describedby' => 'input-nome',
                            'data-error' => 'Digite seu Nome'));
                        ?>
                    </div>	
                    <div class="span6">
                        <label>Grupo</label>
                        <?=$form->input('grupo',array(
                        'label' => false,
                        'type' => 'select',
                        'class' => 'chosen',
                        'selected' => $usuario['Usuario']['grupo'],
                        'options' => $tiposgrupos['funcionarios'],
                        'div' => 'input-control',
                        'error' => false,
                        'id' => 'seletor')); ?>
                    </div>
                    <div class="span3">
                        <label class="required" for="data-nascimento">Data Nascimento</label>
                        <?=$form->input('FormandoProfile.data_nascimento', array(
                            'label' => false,
                            'type' => 'text',
                            'dateFormat' => 'DMY',
                            'id' => 'data-nascimento',
                            'value' => $usuario['FormandoProfile']['data_nascimento'],
                            'data-required' => 'true',
                            'alt' => '99/99/9999',
                            'class' => 'datepicker',
                            'div' => 'input-control',
                            'error' => false)); ?>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <label class="required" for="input-email">Email</label>
                        <?=
                        $form->input('email', array(
                            'label' => false,
                            'div' => 'input-control text',
                            'error' => false,
                            'id' => 'input-email',
                            'class' => 'verificaEmail',
                            'value' => $usuario['Usuario']['email'],
                            'data-required' => 'true',
                            'data-description' => 'notEmpty',
                            'data-describedby' => 'input-email',
                            'data-error' => 'Digite seu Email',
                            'data-pattern' => '^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}',
                            'data-error-pattern' => 'Email inválido')); ?>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span6">
                        <label class="required">Tel Residencial</label>
                            <?=$form->input('FormandoProfile.tel_residencial',array(
                                'label' => false,
                                'div' => 'input-control text',
                                'value' => $usuario['FormandoProfile']['tel_residencial'],
                                'error' => false,
                                'data-required' => 'true',
                                'alt' => 'phone'));
                            ?>
                    </div>
                    <div class="span6">
                        <label class="required">Celular</label>
                            <?=$form->input('FormandoProfile.tel_celular',array(
                                'label' => false,
                                'div' => 'input-control text',
                                'value' => $usuario['FormandoProfile']['tel_celular'],
                                'alt' => 'celphone',
                                'data-required' => 'true',
                                'error' => false));
                            ?>
                    </div>
                </div>
                <div class="row-fluid">
                        <?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'button bg-color-greenDark submit'));?>
                </div>
        </div>
    </div>
</div>