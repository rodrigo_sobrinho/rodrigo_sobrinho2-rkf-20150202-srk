<?php

class BancoConta extends AppModel {

    var $name = 'BancoConta';
    var $belongsTo = array('Banco');
    var $validate = array(
        'banco_id' => array(
            'vazio' => array(
                'rule' => 'notEmpty',
                'message' => 'Selecione um banco'
            )
        ),
        'agencia' => array(
            'vazio' => array(
                'rule' => 'notEmpty',
                'message' => 'Insira a agencia'
            ),
            'tamanho_minimo' => array(
                'rule' => array('minLength', 2),
                'required' => true,
                'message' => 'Mínimo 2 digitos'
            ),
        ),
        'conta' => array(
            'vazio' => array(
                'rule' => 'notEmpty',
                'message' => 'Insira a conta'
            ),
            'numerico' => array(
                'rule' => 'numeric',
                'message' => 'Apenas numeros'
            ),
            'tamanho_minimo' => array(
                'rule' => array('minLength', 2),
                'required' => true,
                'message' => 'Mínimo 2 digitos'
            )
        ),
        'cedente' => array(
            'vazio' => array(
                'rule' => 'notEmpty',
                'message' => 'Insira o cedente'
            )
        ),
        'codigo_cliente' => array(
            'vazio' => array(
                'rule' => 'notEmpty',
                'message' => 'Insira o codigo do cliente'
            ),
            'tamanho_minimo' => array(
                'rule' => array('minLength', 2),
                'required' => true,
                'message' => 'Mínimo 2 digitos'
            )
        ),
        'cpf_cnpj' => array(
            'vazio' => array(
                'rule' => 'notEmpty',
                'message' => 'Insira o CPF/CNPJ'
            ),
            'numerico' => array(
                'rule' => 'numeric',
                'message' => 'Apenas numeros'
            ),
            'tamanho' => array(
                'rule' => array('checarCpfCnpj'),
                'message' => 'CPF/CNPJ deve conter 11 ou 14 digitos'
            ),
        ),
        'carteira' => array(
            'vazio' => array(
                'rule' => 'notEmpty',
                'message' => 'Insira'
            ),
        ),
        'endereco' => array(
            'vazio' => array(
                'rule' => 'notEmpty',
                'message' => 'Insira o endereco'
            ),
        ),
        'cidade' => array(
            'vazio' => array(
                'rule' => 'notEmpty',
                'message' => 'Insira a cidade'
            ),
        ),
        'uf' => array(
            'vazio' => array(
                'rule' => 'notEmpty',
                'message' => 'Selecione a UF'
            ),
        ),
    );
    
    function checarCpfCnpj($check) {
        $value = array_shift($check);
        return strlen($value) == 11 || strlen($value) == 14;
    }

}
