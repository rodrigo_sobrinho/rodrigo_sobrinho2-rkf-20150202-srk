<?php if($out == "html") : ?>
<?php include('salvar_contrato.ctp')?>
<?php endif; ?>
<h4>ANEXO D - CONTRATO DE ADESÃO E COMPROMISSO</h4><br />
<p>
Pelo presente instrumento particular de prestação de serviços e na melhor forma de direito, o(a) formando(a) abaixo qualificado
</p>
<br />
Nome: ____________________________________________________ Data Nascimento: ___/___/________<br />
R.G.: ___________ CPF: _ _ _ . _ _ _ . _ _ _ - _ _ Rua/Av: _________________________________________<br />
Núm.: ____ Complemento: _____________ CEP: _ _ _ _ _ - _ _ _ Cidade: ____________________________<br />
Tel Residencial: ( _ _ ) _ _ _ _ - _ _ _ _ Tel Celular: ( _ _ ) _ _ _ _ _ - _ _ _ _ Outro: (_ _) _ _ _ _ _ - _ _ _ _<br />
Email: _____________________________________________(será o seu login no sistema da Contratada)<br />
Senha Acesso: _________(defina senha para o seu 1º acesso http://www.asformaturas.com.br/espaco_formando)<br />
adere ao Contrato de Prestação de Serviços de Foto e Filmagem firmado entre a <b>COMISSÃO DE FORMATURA DE SEU CURSO</b> e 
a empresa SADER & AVILA FOTO E FILMAGEM LTDA., inscrita no CNPJ/MF sob N.15.151.497/0001-16 , com sede na Rua José Maria Whitaker,
882 – Planalto Paulista, CEP 04057-000, São Paulo/SP, neste ato representada na forma de seu contrato social. 
Os dados do básicos contrato estão especificados abaixo e o mesmo passa a ser considerado como <b>CONTRATO COLETIVO</b>.<br />
CÓDIGO DO CONTRATO - <b><?=$turma['Turma']['id']?></b> - ESTE CÓDIGO DEVE SER USADO PARA ADESÃO ONLINE - WWW.ASFORMATURAS.COM.BR<br />
DATA DE ASSINATURA DO CONTRATO ENTRE A COMISSÃO DE FORMATURA E A SADER E AVILA  <b><?=date("d/m/Y", strtotime($turma['Turma']['data_assinatura_contrato']))?></b><br />
Faculdade: _______________________________________  Curso: _________________________________ <br />
Sala/Turma: ________ Período: __________________<br />
<p style="text-align: justify">
<b>1.</b> O formando acima qualificado reconhece a Comissão de Formatura de sua turma como legítima representante para tratar 
dos assuntos relacionados a sua formatura, outorgando poderes para representá-lo referente a exclusividade Fotográfica, 
ratificando os termos do CONTRATO COLETIVO, declarando ter plenos conhecimentos desse contrato e outorgando poderes para 
a Comissão celebrar aditivos contratuais que se fizerem necessários à realização da cobertura de Foto e Filmagem.<br />
<b>2.</b> As informações importantes a respeito do seu contrato estarão disponibilizadas no link - http://www.asformaturas.com.br/espaco_formando<br />
<b>3.</b> A Comissão de Formatura não poderá tratar assuntos de cunho financeiro, tais como forma de pagamento, renegociação de 
dívida ou cancelamento de contrato em nome do formando.<br />
<b>4.</b> Constitui obrigação do (a) CONTRATANTE manter seu endereço e demais dados cadastrais atualizados, bem como a responsabilidade
pela grafia correta de seu nome nos textos originais que serão entregues à CONTRATADA para a execução dos serviços gráficos, 
eximindo a CONTRATADA pela falta do nome e/ou grafia incorreta conforme previsão no Contrato Principal.<br /><br />
<table border="0">
<tr style="text-align: center">
<td><b>E assim por estarem justos e contratados,</b></td>
<td>São Paulo, ______ de ________________ de ________.</td>
</tr>
</table>
<br />
<br />
<br />
</p>
<table border="0">
<tr style="text-align: center">
<td><?=str_pad("",25,"_")?></td>
<td><?=str_pad("",25,"_")?></td>
</tr>
<tr style="text-align: center; border: 0">
<td>CONTRATANTE</td>
<td>SADER & AVILA FOTO E FILMAGEM LTDA</td>
</tr>
</table>