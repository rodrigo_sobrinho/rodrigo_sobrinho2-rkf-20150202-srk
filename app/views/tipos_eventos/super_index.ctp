<span id="conteudo-titulo" class="box-com-titulo-header">Tipos de Eventos</span>
<div id="conteudo-container">
	<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
	<?php $session->flash(); ?>
	<div class="tabela-adicionar-item">
		<?php echo $html->link('Novo Tipo de Evento',array($this->params['prefix'] => true, 'action' => 'adicionar')); ?>
		<div style="clear:both;"></div>
	</div>
	<div class="container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col"><?php echo $paginator->sort('Id', 'id'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Nome', 'nome'); ?></th>
					<th scope="col"><?php echo $paginator->sort('Template', 'template'); ?></th>					
					<th scope="col"><?php echo $paginator->sort('Descrição', 'descricao'); ?></th>
					<th scope="col"> &nbsp;</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="3"><?php echo $paginator->counter(array('format' => 'Tipo de Evento %start% ao %end% - página %page% de %pages%')); ?>
						<span class="paginacao">
							<?php echo $paginator->numbers(array('separator' => ' ')); ?>
						</span>
					</td>
					<td colspan="1"><?php echo $paginator->counter(array('format' => 'Total : %count% ' .  $this->name)); ?></td>
				</tr>
			</tfoot>
			<tbody>
			<?php $isOdd = false; ?>
			<?php foreach($tiposeventos as $tipoevento): ?>
				<?php if($isOdd):?>
					<tr class="odd">
				<?php else:?>
					<tr>
				<?php endif;?>
					<td colspan="1"><?php echo $tipoevento['TiposEvento']['id'];?></td>
					<td colspan="1"><?php echo $tipoevento['TiposEvento']['nome'];?></td>
					<td colspan="1"><?php echo $tipoevento['TiposEvento']['template']?></td>
					<td colspan="1"><?php echo $tipoevento['TiposEvento']['descricao'];?></td>
					<td colspan="1"><?php echo $html->link('Editar', array($this->params['prefix'] => true, 'controller' => 'tipos_eventos', 'action' =>'editar', $tipoevento['TiposEvento']['id']), array('class' => 'submit button')); ?></td>
				</tr>
				<?php $isOdd = !($isOdd); ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>