<span id="conteudo-titulo" class="box-com-titulo-header">Cadastro de Formando</span>
<div id="conteudo-container" style="margin-left:0;">
	<?php 
		$session->flash();
		echo $form->create('Turma', array('url' => "adicionar", 'class' => 'form-cadastro-formando', 'id' => 'form-cadastro-formando-inicio')); ?>
		<p class="grid_full alpha">
		Insira o c&oacute;digo da sua turma para dar prosseguimento ao seu cadastro.
		</p>	
		<p class="grid_full alpha">
		<?php echo $form->input('Turma.id', array('type' => 'text','class' => 'input-grande', 'size' => 5, 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_10'))); ?>
		</p>

		<?php echo $form->hidden('comissao'); ?>
			
			
		<p class="grid_full alpha omega">
			<?php echo $form->end(array('label' => 'Iniciar Cadastro', 'div' => false, 'class' => 'submit', 'style' =>'float:right;', 'type' => "number"));?>
		</p>
</div>
