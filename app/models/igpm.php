<?php

class Igpm extends AppModel {
	var $name = 'Igpm';
	var $useTable = 'igpm';

	var $validate = array(
		'valor' => array ( 
			'required' => array (
				'rule' => 'notEmpty',
				'message' => 'Campo obrigatório.'
			),
			
			'moeda' => array (
				'rule' => '/[-]?[0-9]+[,\.][0-9]+/',
				'message' => 'Campo deve ser do formato decimal.'
			)
		),
		
	
		'ano' => array(
			'rule' => array('range', 1960, 2200),        
	       	'required' => true,
	        'message' => 'Insira um ano válido.'
	    ),
	
		'mes'=>array(
			'unique'=>array(
				"rule"=>array(
					"checkUnique", array("mes", "ano")),
				"message"=>"IGPM já cadastrado para este mês/ano."
			),
			'range'=>array(
				'rule' => array('range',0,13),
				'required' => true,
				'message' => 'Insira um mês válido.'
			)
		)
	); 

}

?>
