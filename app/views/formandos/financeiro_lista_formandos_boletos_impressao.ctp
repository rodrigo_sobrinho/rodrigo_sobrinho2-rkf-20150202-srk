<style type="text/css">
.load-boletos { width:100%; padding:0; background-color:white; height:466px; position:relative; overflow:hidden }
.load-boletos img { margin:0 auto }
</style>
<?php $session->flash(); ?>
<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
<label id="conteudo-titulo" class="box-com-titulo-header">Lista de Formandos Para Gerar Boletos</label>
<div id="conteudo-container">
	<div class="tabela-adicionar-item">
		<?php echo $form->create(false, array('url' => "/{$this->params['prefix']}/formandos/lista_formandos_boletos_impressao", 'class' => 'procurar-form-inline')); ?>
		<span><?php echo $form->input('value', array('class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?> </span>
		<?php echo $form->input('field', array('options' => $optionsFiltro, 'type' => 'select', 'class' => 'grid6 alpha omega', 'label' => false, 'div' => false)); ?>
		<?php echo $form->end(array('label' => ' ', 'div' => false, 'class' => 'submit-busca')); ?>
		<div style="clear:both;"></div>
	</div>
	<div class="container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col">Cód. do Formando</th>
					<th scope="col" style="text-align:left"><?=$paginator->sort('Nome', 'Usuario.nome'); ?></th>
					<th scope="col" style="text-align:left"><?=$paginator->sort('Endereço', 'FormandoProfile.end_rua'); ?></th>
					<th scope="col"><?=$paginator->sort('Impressão', 'FormandoProfile.impresso'); ?></th>
					<th scope="col"><?=$paginator->sort('Envio', 'FormandoProfile.enviado'); ?></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="4"><?php echo $paginator->counter(array('format' => 'Itens %start% ao %end% - página %page% de %pages%')); ?>
						<span class="paginacao">
							<?php echo $paginator->numbers(array('separator' => ' ')); ?>
						</span>
					</td>
					<td colspan="2"><?php echo $paginator->counter(array('format' => 'Total : %count% de formandos')); ?></td>
				</tr>
			</tfoot>
			<tbody id='tbody'>
			<?php $isOdd = true; ?>
			<?php if(count($formandos) > 0) :?>
			<?php foreach($formandos as $formando): ?>
				<?php $codigoFormando = str_pad($formando["TurmasUsuario"]["turma_id"],4,'0',STR_PAD_LEFT) . str_pad($formando["FormandoProfile"]["codigo_formando"],3,'0',STR_PAD_LEFT)?>
				<?=$isOdd ? "<tr class='odd'>" : "<tr>"?>
					<td width="10%"><?=$codigoFormando; ?></td>
					<td style="text-align:left"><?=$formando['Usuario']['nome']; ?></td>
					<td style='font-size:12px; text-align:left'>
						<?="{$formando['FormandoProfile']['end_rua']}, {$formando['FormandoProfile']['end_numero']}"?>
						<?=$formando['FormandoProfile']['end_complemento'] != "" ? " {$formando['FormandoProfile']['end_complemento']}" : ""?>
						<?=" - {$formando['FormandoProfile']['end_bairro']}"?>
						<br />
						<?="{$formando['FormandoProfile']['end_cep']} - {$formando['FormandoProfile']['end_cidade']}/{$formando['FormandoProfile']['end_uf']}"?>
					</td>
					<td width="20%" style="text-align:center">
						<?php
						if($formando['FormandoProfile']['impresso'] == 1) {
							echo "Boleto j&aacute; foi impresso<br /><br />";
							echo $html->link('Reimprimir', array($this->params['prefix'] => true,
									'controller' => 'area_financeira','action' =>'gerar_boletos', $formando['FormandoProfile']['usuario_id']), array('class' => 'submit button', 'target' => '_blank'));
						} else {
							echo $html->link('Imprimir Boleto', array($this->params['prefix'] => true,
									'controller' => 'area_financeira','action' =>'gerar_boletos', $formando['FormandoProfile']['usuario_id']), array('class' => 'submit button', 'target' => '_blank'));
						}
						?>
					</td>
					<td width="20%" style="text-align:center">
						<?php
						if($formando['FormandoProfile']['impresso'] == 1) {
							if($formando['FormandoProfile']['enviado'] == 1)
								echo "Boleto j&aacute; foi enviado";
							else
								echo $html->link('Confirmar Envio', array($this->params['prefix'] => true,
										'controller' => 'area_financeira','action' =>'finaliza_geracao_boletos', $formando['FormandoProfile']['usuario_id']), array('class' => 'submit button'));
						}
						?>
					</td>
				</tr>
			<?php $isOdd = !($isOdd)?>
			<?php endforeach; ?>
			<?php  else : ?>
				<tr><td colspan="5"><h2>Nenhum boleto precisa ser gerado</h2></td></tr>
			<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>