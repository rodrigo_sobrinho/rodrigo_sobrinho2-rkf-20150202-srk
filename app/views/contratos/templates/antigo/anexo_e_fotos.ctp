<?php if($out == "html") : ?>
<?php include('salvar_contrato.ctp')?>
<?php endif; ?>
<h4>ANEXO E – CONTRATO DE COBERTURA FOTOGRÁFICA E DE FILMAGEM</h4>
<p style="font-size: 1.05em; line-height: 1.6em">
<?=str_pad("Faculdade:", 70, "&nbsp;")?><?=$turma['Turma']['nome']?>
<br />
<?=str_pad("Curso:", 108, "&nbsp;")?><?=$turma['Turma']['cursos']?>
<br />
<?=str_pad("Conclusão:", 71, "&nbsp;")?><?=$turma['Turma']['ano_formatura']?>.<?=$turma['Turma']['semestre_formatura']?>
</p>
<p style="text-align: justify; text-indent: 20px; font-size:0.95em; line-height: 1.08em">
1.&nbsp;&nbsp;As fotografias que comporão os álbuns serão coloridas, no tamanho 24 x 30 cm, com padrão de qualidade 
compatível com o mercado fotográfico, ilustrando todos os eventos extraoficiais solicitados pela Comissão de 
formatura, além da Colação de Grau e Baile de Gala. Para garantir a máxima qualidade, todas as fotos serão 
capturadas digitalmente e ampliadas em Mini Labs Digitais.
<br />
<br />
2.&nbsp;&nbsp;Os materiais de vídeo serão editados em uma ESTAÇÃO DIGITAL onde todas as imagens serão captadas através 
de câmeras digitais. Quando o formando optar por DVD não há necessidade de transcodificação uma vez que a fonte 
e o destino são digitais. A captação digital garante a máxima qualidade de imagem e som.
<br />
<br />
3.&nbsp;&nbsp;Os profissionais destacados para os eventos Extra Oficiais estarão vestidos com calça social preta, sapato social 
preto, camisa social branca e para os eventos oficiais, estarão em traje social completo. Em todos os eventos os 
profissionais estarão ainda identificados de crachás ÁS EVENTOS.
<br />
<br />
4.&nbsp;&nbsp;Serão destacados para o baile de formatura no mínimo um fotógrafo para cada doze alunos e um cinegrafista para 
cada vinte e cinco alunos. 
<br />
<br />
5.&nbsp;&nbsp;Os preços das fotografias terão como base o valor de R$ 26,50, sendo esse preço referência em agosto de 
2014, sendo que será corrigido pelo IGPM no momento da venda do álbum ao formando (a correção do IGPM 
será o índice acumulado entre 01.08.2014 e a data da venda do álbum). Serão dadas diversas opções de 
parcelamento do álbum, negociadas diretamente entre formando e vendedor, sendo que os preços das fotos
serão ajustados para absorver o financiamento.
<br />
<br />
6.&nbsp;&nbsp;O prazo de entrega do material fotográfico dar-se-á no máximo de 180 dias úteis após a realização do Baile de 
Gala. A visita para apresentação do material será em local e horário a critério do formando através de agendamento 
prévio. A forma de pagamento que estará sujeito o formando respeitará todos os critérios ora citados, podendo a 
empresa criar meios complementares e facilitadores para aquisição dos produtos mencionados. Assim, o álbum
poderá ser parcelado em até 12 vezes.
<br />
<br />
7.&nbsp;&nbsp;Na festa de formatura será permitido o uso de máquinas fotográficas não profissionais após a valsa de formatura. 
Na colação de grau não será permitido o uso de máquina fotográfica nem filmadora. O uso de máquinas fotográficas 
profissionais e filmadoras ficam extremamente proibidos em qualquer evento.
<br />
<br />
8.&nbsp;&nbsp;O formando não tem nenhuma obrigação na aquisição de fotografias e filmagem. No caso da compra das fotos, é 
permitido um descarte máximo de 30% de fotos do total do álbum de formatura.
<br />
<br />
9.&nbsp;&nbsp;A ÁS eventos é detentora da exclusividade vídeo-fotográfica dos eventos relativos às festividades de formatura, 
oferecendo descontos e eventos extras conforme consta no Anexo A e eventualmente no Anexo B, sempre 
proporcional ao número de formandos identificados e que estejam aderidos ao baile de formatura.
</p>