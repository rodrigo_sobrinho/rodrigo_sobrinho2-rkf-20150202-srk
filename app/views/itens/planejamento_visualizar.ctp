<?php $paginator->options(array('url' => array_merge(array($this->params['prefix'] => true), $this->passedArgs))); ?>
<span id="conteudo-titulo" class="box-com-titulo-header">Item: <?php echo $item['Item']['nome']; ?></span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="detalhes">
		
		
		<?php if(!empty($item['Item']['descricao'])): ?>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Descrição</label>
			<span class="grid_8 alpha"><?php echo $item['Item']['descricao']; ?></span>
		</p>
		<div class="clear"></div>
		<?php endif; ?>
		
		
		<div class="tabela-adicionar-item">
			<label class="tabela-label">Selecione um assunto</label>
			<?php echo $html->link('Novo assunto',array($this->params['prefix'] => true, 'controller' => 'assuntos', 'action' => 'adicionar', $item['Item']['id']), array('class' => 'add')); ?>
			<div style="clear:both;"></div>
		</div>

		<div class="container-tabela">
			<table>
				<thead>
					<tr>
						<th scope="col"><?php echo $paginator->sort('Turma', 'turma'); ?></th>
						<th scope="col"><?php echo $paginator->sort('Resolvido?', 'resolvido'); ?></th>
						<th scope="col"><?php echo $paginator->sort('Pendência', 'pendencia'); ?></th>
						<th scope="col"><?php echo $paginator->sort('Assunto', 'nome'); ?></th>
						<th scope="col"><?php echo $paginator->sort('Total mensagens', 'mensagens'); ?></th>
						<th scope="col"> &nbsp;</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="5"><?php echo $paginator->counter(array('format' => 'Itens %start% ao %end% - página %page% de %pages%')); ?>
							<span class="paginacao">
								<?php echo $paginator->numbers(array('separator' => ' ')); ?>
							</span>
						</td>
						<td colspan="1"><?php echo $paginator->counter(array('format' => 'Total : %count% ' .  $this->name)); ?></td>
					</tr>
				</tfoot>
				<tbody>
				<?php $isOdd = false; ?>
				<?php foreach($assuntos as $assunto): ?>
					<?php if($isOdd):?>
						<tr class="odd">
					<?php else:?>
						<tr>
					<?php endif;?>
						<td colspan="1"><?php echo $assunto['Turma']['nome'];?></td>
						<td colspan="1"><?php echo ($assunto['Assunto']['resolvido'] == 1 ? "Sim" : "<b>Não</b>");?></td>
						<td colspan="1"><?php echo $assunto['Assunto']['pendencia'];?></td>
						<td colspan="1"><?php echo $assunto['Assunto']['nome'];?></td>
						<td colspan="1"><?php echo count($assunto['Mensagem']);?></td>
						<td colspan="1"><?php echo $html->link('Entrar', array($this->params['prefix'] => true, 'controller' => 'assuntos', 'action' =>'visualizar', $assunto['Assunto']['id']), array('class' => 'submit button')); ?></td>
					</tr>
					<?php $isOdd = !($isOdd); ?>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		<p class="grid_full alpha omega">
			<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index') ,array('class' => 'cancel')); ?>
		</p>
	</div>
</div>

<script  type="text/javascript">
	
		$('#conteudo-container').delegate('.add', 'click', function(event) {
			if(!confirm('Você tem certeza que deseja começar um novo assunto?\nO tema do seu assunto já não está sendo tratado?')) {
				event.preventDefault();
			}
		});
		
	
</script>