<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript">

$(document).ready(function(){
    //plugin de ativacao do plugin
    $('.selectpicker').selectpicker({width:'100%'});
});
</script>
<?php $session->flash(); ?>
<div class="row-fluid">
    <h2>
        Selecione o Evento
    </h2>
</div>
<br />
<?php echo $form->create('Evento', array(
    'url' => "/{$this->params['prefix']}/mapa_mesas",
    'id' => 'form'
)); ?>
<div class="row-fluid">
    <div class="span6">
        <?=$form->input('Evento.id', array(
            'options' => $eventos,
            'type' => 'select',
            'class' => 'selectpicker',
            'label' => 'Eventos',
            'div' => 'input-control text')); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span2">
        <?php echo $form->end(array(
            'label' => 'Enviar', 
            'div' => false, 
            'class' => 
            'button max bg-color-greenDark')
            );
        ?>
    </div>
</div>