<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<style type="text/css">
    .tab-content { overflow:visible; }
</style>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            &Aacute;rea Financeira
        </h2>
    </div>
</div>
<br />
<div class="row-fluid">
    <div class="span12">
        <ul class="nav nav-tabs" id='tab-financeiro'>
            <li>
                <a href="#adesao" data-toggle="tab">
                    Boletos Ades&atilde;o
                </a>
            </li>
            <li>
                <a href="#resumo" data-toggle="tab">
                    Resumo Financeiro
                </a>
            </li>
            <li>
                <a href="#igpm" data-toggle="tab">
                    Boletos IGPM
                </a>
            </li>
            <li>
                <a href="#campanhas" data-toggle="tab">
                    Boletos Extras
                </a>
            </li>
            <li>
                <a href="#nao_vinculados" data-toggle="tab">
                    Pagamentos N&atilde;o Vinculados
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade" id="resumo">
                
            </div>
            <div class="tab-pane fade" id="adesao">
                
            </div>
            <div class="tab-pane fade" id="igpm">
                
            </div>
            <div class="tab-pane fade" id="campanhas">
                
            </div>
            <div class="tab-pane fade" id="nao_vinculados">
                
            </div>
        </div>
    </div>
</div>