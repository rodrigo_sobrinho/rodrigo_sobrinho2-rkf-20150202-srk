<h2>Resumo</h2>

<?php if (!$agendamento['MapaMesasAgendamento']['escolha_liberada']): ?>
    <h5 class="fg-color-red">
        Seleção de mesa bloqueada.
    </h5>
    <a id="desbloquear" href="<?php echo $this->webroot . $this->params['prefix'] ?>/mapa_mesas/desbloquear_mapa/<?php echo $eventoId ?>/<?php echo $turmaMapaId ?>" >
        <div class="button max bg-color-red">
            Desbloquear seleção de mesa
        </div>
    </a>
<?php else: ?>
    <a id="bloquear" href="<?php echo $this->webroot . $this->params['prefix'] ?>/mapa_mesas/bloquear_mapa/<?php echo $eventoId ?>/<?php echo $turmaMapaId ?>" >
        <div class="button max bg-color-red">
            Bloquear seleção de mesa
        </div>
    </a>
<?php endif ?>

<table class="table table-striped table-hovered">
    <tbody >
         <tr>
            <td>Evento:</td>
            <td><?php echo $evento['nome'] ?></td>
        </tr>
        <tr>
            <td>Local:</td>
            <td><?php echo $evento['espaco'] ?></td>
        </tr>
        <tr>
            <td>Total de Alunos:</td>
            <td><?php echo $totalFormandos ?></td>
        </tr>
        <tr>
            <td>Total de Mesas:</td>
            <td><?php echo $quantidadeMesas ?></td>
        </tr>
        <!-- <tr> -->
            <!-- <td>Total de Mesas Escolhidas:</td> -->
            <!-- <td><?php echo '5' ?></td> -->
        <!-- </tr> -->
    </tbody>
</table>

<!-- <h2>Agendamento da comissão</h2>

<table class="table table-striped table-hovered">
    <thead>
        <td >Codigo Formando</td>
        <td >Nome</td>
        <td >Data adesão</td>
        <td >Data liberação</td>
        <td >Status</td>
        <td >Posição da mesa</td>
    </thead>
    <tbody >
        <?php //foreach ($comissao as $formando) : ?>
            <tr>
                <td><?php //echo $formando['Formandos']['codigo_formando'] ?></td>
                <td><?php //echo $formando['Usuario']['nome'] ?></td>
                <td><?php //echo $formando['data_adesao'] ?></td>
                <td><?php //echo $formando['data_agendada'] ?></td>
                <td><?php //echo $formando['status'] ?></td>
                <td><?php //echo $formando['referencias'] ?></td>
            </tr>
        <?php //endforeach; ?>
    </tbody>
</table> -->

<h2>Agendamento de Comissão</h2>

<table class="table table-striped table-hovered">
    <thead>
        <td class="input-mini">Código Formando</td>
        <td >Nome</td>
        <td >Grupo</td>
        <td >Data adesão</td>
        <td >Data liberação</td>
        <td >Setor</td>
        <td >Status</td>
        <td class="input-mini">Qtd Mesas</td>
        <td >Posição da mesa</td>
    </thead>
    <tbody >
        <?php foreach ($comissao as $formando) : ?>
            <tr>
                <td><?php echo $formando['Formandos']['codigo_formando'] ?></td>
                <td><?php echo $formando['Usuario']['nome'] ?></td>
                <td><?php echo $formando['Usuario']['grupo'] ?></td>
                <td><?php echo date('d/m/Y H:i:s',strtotime($formando['FormandoProfile']['data_adesao'])) ?></td>
                <td><?php echo ($formando['FormandoHorario']['data_liberacao'] ? date('d/m/Y H:i:s',strtotime($formando['FormandoHorario']['data_liberacao'])) : ' - ') ?></td>
                <td><?php echo utf8_encode($formando['FormandoHorario']['setor']) ?></td>
                <td><?php echo $formando['status'] ?></td>
                <td><?php echo $formando['FormandoMesas']['quantidade_mesas'] ?></td>
                <td><?php echo $formando['FormandoMesas']['referencias'] ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<h2>Agendamento de Formandos</h2>

<table class="table table-striped table-hovered">
    <thead>
        <td class="input-mini">Código Formando</td>
        <td >Nome</td>
        <td >Grupo</td>
        <td >Data adesão</td>
        <td >Data liberação</td>
        <td >Setor</td>
        <td >Status</td>
        <td class="input-mini">Qtd Mesas</td>
        <td >Posição da mesa</td>
    </thead>
    <tbody >
        <?php foreach ($formandos as $formando) : ?>
            <tr>
                <td><?php echo $formando['Formandos']['codigo_formando'] ?></td>
                <td style="text-overflow-mode: ellipsis;"><?php echo $formando['Usuario']['nome'] ?></td>
                <td><?php echo $formando['Usuario']['grupo'] ?></td>
                <td><?php echo date('d/m/Y H:i:s',strtotime($formando['FormandoProfile']['data_adesao'])) ?></td>
                <td><?php echo ($formando['FormandoHorario']['data_liberacao'] ? date('d/m/Y H:i:s',strtotime($formando['FormandoHorario']['data_liberacao'])) : ' - ') ?></td>
                <td><?php echo utf8_encode($formando['FormandoHorario']['setor']) ?></td>
                <td><?php echo $formando['status'] ?></td>
                <td><?php echo $formando['FormandoMesas']['quantidade_mesas'] ?></td>
                <td><?php echo $formando['FormandoMesas']['referencias'] ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<script type="text/javascript">
    $(document).ready(function(){
        $('#bloquear').click(function(){
            if(!confirm('Deseja bloquear a seleção de mesa desse evento?')){
                return false;
            }
        });
        $('#desbloquear').click(function(){
            if(!confirm('Deseja desbloquear a seleção de mesa desse evento?')){
                return false;
            }
        });
    });
</script>