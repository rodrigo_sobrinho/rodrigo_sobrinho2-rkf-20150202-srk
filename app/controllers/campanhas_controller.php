<?php

class CampanhasController extends AppController {

    var $name = 'Campanhas';
    var $uses = array('Campanha','Extra','Despesa','Evento','CampanhasExtra',
        'CampanhasUsuario','CampanhasUsuarioCampanhasExtra',
        'FormandoProfile','ViewFormandos');
    
    function atendimento_comprar_extras($usuarioId) {
        $this->layout = false;
        $atendente = $this->obterUsuarioLogado();
        $formando = $this->ViewFormandos->read(null,$usuarioId);
        $this->set('formando',$formando['ViewFormandos']);
        $turma = $this->Turma->read(null,$formando['ViewFormandos']['turma_id']);
        $this->set('turma',$turma['Turma']);
        if(empty($this->data)) {
            $this->Campanha->recursive = 2;
            $this->Campanha->unbindModelAll();
            $this->Campanha->bindModel(array('hasMany' => array('CampanhasExtra' => array('foreignKey' => 'campanha_id'))));
            $this->CampanhasExtra->bindModel(array('belongsTo' => array('Extra')));
            $this->Extra->unbindModel(array('hasAndBelongsToMany' => array('Campanha')));
            $campanha = $this->Campanha->find('first',array(
                'conditions' => array(
                    'Campanha.turma_id' => $turma['Turma']['id'],
                    'Campanha.tipo' => 'atendimento'
                )
            ));
            $eventos = $this->Evento->find('all',array(
                'conditions' => array(
                    'Evento.turma_id' => $turma['Turma']['id']
                )
            ));
            $extras = array();
            $listaEventos = array();
            if($eventos)
                foreach($eventos as $evento) {
                    $listaEventos[$evento['Evento']['id']] = $evento['Evento']['nome'];
                    foreach($evento['Extra'] as $extraEvento)
                        $extras[$evento['Evento']['nome']][$extraEvento['id']] = $extraEvento['nome'];
                }
            $this->set('eventos',$listaEventos);
            $this->set('extras',$extras);
            $formaParcelamento = array(
                'dinheiro' => 'À vista em dinheiro',
                'boleto' => 'Uma vez no boleto',
                'cheque' => 'Uma vez em cheque',
                'parcelar' => 'Parcelar Valor'
            );
            $formaPagamento = array(
                'dinheiro' => 'Dinheiro',
                'cheque' => 'Cheque',
                'boleto' => 'Boleto'
            );
            $this->set('formaParcelamento',$formaParcelamento);
            $this->set('formaPagamento',$formaPagamento);
        } else {
            Configure::write(array('debug' => 0));
            $hoje = date('Y-m-d H:i:s');
            $this->autoRender = false;
            $this->Campanha->unbindModelAll();
            $campanha = $this->Campanha->find('first',array(
                'conditions' => array(
                    'Campanha.turma_id' => $turma['Turma']['id'],
                    'Campanha.tipo' => 'atendimento'
                )
            ));
            if(!$campanha) {
                $campanha = array(
                    'Campanha' => array(
                        'turma_id' => $turma['Turma']['id'],
                        'nome' => 'Extras Comprados Pelo Atendimento',
                        'tipo' => 'atendimento',
                        'descricao' => '',
                        'max_parcelas' => 1,
                        'data_inicio' => date('Y-m-d H:i:s',strtotime("-2 day")),
                        'data_fim' => date('Y-m-d H:i:s',strtotime("-1 day")),
                        'data_cadastro' => $hoje
                    )
                );
                if(!$this->Campanha->save($campanha)) {
                    $this->Session->setFlash('Erro ao inserir a compra', 'metro/flash/error');
                    echo json_encode(array($campanha));
                    exit();
                } else {
                    $campanha = $this->Campanha->read(null,$this->Campanha->getLastInsertId());
                }
            }
            $campanhaUsuario = array(
                'CampanhasUsuario' => array(
                    'usuario_id' => $formando['ViewFormandos']['id'],
                    'campanha_id' => $campanha['Campanha']['id'],
                    'parcelas' => count($this->data['despesas']),
                    'cancelada' => 0,
                    'atendente_id' => $atendente['Usuario']['id'],
                    'data' => $hoje
                )
            );
            if(!$this->CampanhasUsuario->save($campanhaUsuario)) {
                $this->Session->setFlash('Erro ao inserir a compra', 'metro/flash/error');
                echo json_encode(array());
                exit();
            } else {
                $campanhaUsuario = $this->CampanhasUsuario->read(null,$this->CampanhasUsuario->getLastInsertId());
            }
            $compras = $this->data['compras'];
            $campanhasUsuarioCampanhaExtra = array();
            $valorTotal = 0;
            foreach($compras as $compra) {
                $extra = $this->Extra->read(null,$compra['extra_id']);
                $campanhaExtra = $this->CampanhasExtra->find('first',array(
                    'conditions' => array(
                        'campanha_id' => $campanha['Campanha']['id'],
                        'extra_id' => $extra['Extra']['id']
                    )
                ));
                if(!$campanhaExtra) {
                    $qtdeConvites = $compra['quantidade-convites'];
                    $qtdeMesas = $compra['quantidade-mesas'];
                    $campanhaExtra = array(
                        'CampanhasExtra' => array(
                            'campanha_id' => $campanha['Campanha']['id'],
                            'extra_id' => $extra['Extra']['id'],
                            'quantidade_minima' => 0,
                            'quantidade_maxima' => 0,
                            'quantidade_disponivel' => 0,
                            'quantidade_convites' => $qtdeConvites,
                            'quantidade_mesas' => $qtdeMesas,
                            'valor' => 0,
                            'ativo' => 0,
                            'tem_observacao' => ''
                        )
                    );
                    $this->CampanhasExtra->create();
                    if(!$this->CampanhasExtra->save($campanhaExtra)) {
                        $this->Session->setFlash('Erro ao inserir o extra', 'metro/flash/error');
                        echo json_encode(array());
                        exit();
                    } else {
                        $campanhaExtra = $this->CampanhasExtra->read(null,$this->CampanhasExtra->getLastInsertId());
                    }
                }
                $campanhasUsuarioCampanhaExtra[] = array(
                    'CampanhasUsuarioCampanhasExtra' => array(
                        'campanhas_usuario_id' => $campanhaUsuario['CampanhasUsuario']['id'],
                        'campanhas_extra_id' => $campanhaExtra['CampanhasExtra']['id'],
                        'quantidade' => $compra['quantidade'],
                        'retirou' => 0,
                        'valor_unitario' => $compra['valor']
                    )
                );
                $valorTotal+= $compra['valor'];
            }
            if(empty($campanhasUsuarioCampanhaExtra)) {
                $this->Session->setFlash('Houve um erro ao receber os dados', 'metro/flash/error');
                echo json_encode(array());
            } elseif(!$this->CampanhasUsuarioCampanhasExtra->saveAll($campanhasUsuarioCampanhaExtra)) {
                $this->Session->setFlash('Erro ao inserir a compra dos extras', 'metro/flash/error');
                echo json_encode(array());
            } else {
                $parcelas = $this->data['despesas'];
                $despesas = array();
                foreach($parcelas as $a => $parcela) {
                    $dataBase = empty($parcela['data_vencimento']) ? date('d/m/Y') : $parcela['data_vencimento'];
                    $dataVencimento = implode('-',array_reverse(explode('/',$dataBase)));
                    $despesas[] = array(
                        'Despesa' => array(
                            'usuario_id' => $formando['ViewFormandos']['id'],
                            'campanhas_usuario_id' => $campanhaUsuario['CampanhasUsuario']['id'],
                            'valor' => $parcela['valor'],
                            'data_vencimento' => $dataVencimento,
                            'data_cadastro' => $hoje,
                            'tipo' => 'extra',
                            'parcela' => ($a+1),
                            'total_parcelas' => count($parcelas),
                            'correcao_igpm' => 0,
                            'multa' => 0,
                            'status' => 'aberta'
                        )
                    );
                }
                if(!$this->Despesa->saveAll($despesas)) {
                    $erro = array(
                        'Erro ao inserir despesas',
                        'Entre em contato com o administrador'
                    );
                    $this->Session->setFlash($erro, 'metro/flash/error');
                    echo json_encode(array($despesas));
                } else {
                   
                    $this->Session->setFlash('Compra efetuada com sucesso', 'metro/flash/success');
                    echo json_encode(array());
                }
            }
        }
    }
    
    function planejamento_comprar_extras($usuarioId){
        $this->atendimento_comprar_extras($usuarioId);
    }

    private function _listar() {
        $this->layout = false;
        $turma = $this->obterTurmaLogada();
        $campanhas = $this->paginate('Campanha', array('Campanha.tipo' => 'comum',
            'Campanha.turma_id' => $turma['Turma']['id']));
        $this->set('campanhas', $campanhas);
        $this->set('permissao', $this->params['prefix'] == 'planejamento');
    }
    
    private function _editar($campanhaId = false) {
        $this->layout = false;
        $turma = $this->obterTurmaLogada();
        $this->Campanha->recursive = 3;
        $this->Campanha->unbindModelAll();
        $this->Campanha->bindModel(array('hasMany' => array('CampanhasExtra' => array('foreignKey' => 'campanha_id'))));
        $this->CampanhasExtra->bindModel(array('belongsTo' => array('Extra')));
        $this->Extra->unbindModel(array('hasAndBelongsToMany' => array('Campanha')));
        if (!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $dateTime = str_replace('/','-',$this->data['Campanha']['data_inicio']);
            $dateTime = $this->create_date_time_from_format('d-m-Y',$dateTime);
            $this->data['Campanha']['data_inicio'] = date_format($dateTime, 'Y-m-d');
            $dateTime = str_replace('/','-',$this->data['Campanha']['data_fim']);
            $dateTime = $this->create_date_time_from_format('d-m-Y', $dateTime);
            $this->data['Campanha']['data_fim'] = date_format($dateTime, 'Y-m-d');
            $this->data['Campanha']['turma_id'] = $turma['Turma']['id'];
            if($this->Campanha->saveAll($this->data))
                $this->Session->setFlash('Campanha foi salva corretamente.', 'metro/flash/success');
            else
                $this->Session->setFlash('Um erro ocorreu na atualização da campanha.', 'metro/flash/error');
            json_encode(array());
        } else {
            if($campanhaId) {
                $this->data = $this->Campanha->read(null,$campanhaId);
                $this->data['Campanha']['data_fim'] = date('d/m/Y',strtotime($this->data['Campanha']['data_fim']));
                $this->data['Campanha']['data_inicio'] = date('d/m/Y',strtotime($this->data['Campanha']['data_inicio']));
            } else {
                $this->data['CampanhasExtra'] = array();
            }
            $this->Evento->unbindModelAll();
            $eventos = $this->Evento->find('list',array(
                'conditions' => array(
                    'Evento.turma_id' => $turma['Turma']['id'],
                    "Evento.status <> 'cancelado'"
                ),
                'fields' => array('nome')
            ));
            $this->set('eventos',$eventos);
            $this->render('_editar');
        }
    }
    
    function planejamento_editar($campanhaId = false){
        $this->_editar($campanhaId);
    }
    
    private function _alterar($id = false){
        $this->layout = false;
        $turma = $this->Session->read('turma');
        $turma_id = $turma['Turma']['id'];

        $this->Campanha->recursive = 3;
        $this->Campanha->bindModel(array('hasMany' => array('CampanhasExtra' => array('foreignKey' => 'campanha_id'))));

        $this->CampanhasExtra->bindModel(array('belongsTo' => array('Extra')));
        $this->CampanhasExtra->recursive = 3;

        $this->Extra->bindModel(array('belongsTo' => array('Evento')));
        $this->Extra->recursive = 2;

        $this->Evento->recursive = 2;
        $this->Evento->bindModel(array('hasMany' => array('Extra' => array('foreignKey' => 'evento_id'))));

        if (!empty($this->data)) {
            $this->autoRender = false;
          //  Configure::write(array('debug' => 0));
            if (($this->data['Campanha']['data_inicio_aux'] != "") || ($this->data['Campanha']['data_fim_aux'] != "")) {
                //formatar as datas do date picker
                $dateTime = str_replace('/','-',$this->data['Campanha']['data_inicio_aux']);
                $dateTime = $this->create_date_time_from_format('d-m-Y',$dateTime);
                $this->data['Campanha']['data_inicio'] = date_format($dateTime, 'Y-m-d');
                unset($this->data['Campanha']['data_inicio_aux']);
                
                $dateTime = str_replace('/','-',$this->data['Campanha']['data_fim_aux']);
                $dateTime = $this->create_date_time_from_format('d-m-Y', $dateTime);
                $this->data['Campanha']['data_fim'] = date_format($dateTime, 'Y-m-d');
                unset($this->data['Campanha']['data_fim_aux']);
                $this->data['CampanhasExtra'] = array();                
                // Retira apenas as partes importantes e as insere em um array 
                preg_match_all('/\{extra_id:([\d]*), evento_id:([a-z]*), quantidade_minima:([\d]*), quantidade_maxima:([\d]*), quantidade_disponivel:([\d]*), valor:([\d]*[,.]?[\d]*)\}*/', $this->data['Campanha']['adicionar_extras_array'], $matches);
                $adicionarArray = array();
                for ($i = 0; $i < count($matches[1]); $i++) {
                    $adicionarArray[] = array(
                        'extra_id' => $matches[1][$i],
                        'evento_id' => $matches[2][$i],
                        'quantidade_minima' => $matches[3][$i],
                        'quantidade_maxima' => $matches[4][$i],
                        'quantidade_disponivel' => $matches[5][$i],
                        'valor' => str_replace(',', '.', $matches[6][$i]),
                        'tem_observacao' => 0
                    );
                }
                
                preg_match_all('/\{campanha_extra_id:([\d]*)\}*/', $this->data['Campanha']['remover_extras_array'], $matches);
                $removerArray = array();
                for ($i = 0; $i < count($matches[1]); $i++) {
                    $removerArray[] = $matches[1][$i];
                }

                $compras = $this->CampanhasUsuarioCampanhasExtra->find('count', array(
                    'conditions' => array(
                            'CampanhasUsuarioCampanhasExtra.campanhas_extra_id' => $removerArray
                        )
                    ));
                
                $this->data['CampanhasExtra'] = $adicionarArray;
                if (count($adicionarArray) == 0) {
                    $this->Campanha->unbindModel(array('hasMany' => array('CampanhasExtra')));
                }
                else
                    $this->Campanha->bindModel(array('hasMany' => array('CampanhasExtra' => array('foreignKey' => 'campanha_id'))));

                //debug($this->data);
                if($compras == 0){
                    $this->CampanhasExtra->deleteAll(array('CampanhasExtra.id' => $removerArray));
                
                    if ($this->Campanha->saveAll($this->data)) {
                        $this->Session->setFlash('Campanha foi salva corretamente.', 'metro/flash/success');
                    } else {
                        $this->Session->setFlash('Um erro ocorreu na atualização da campanha.', 'metro/flash/error');
                    }
                    json_encode(array());
                }else{
                    $this->Session->setFlash('Esta campanha já foi comprada por algum formando, impossibilitando sua remoção.', 'metro/flash/error');
                }
            } else {
                $this->Session->setFlash('Ocorreu um erro ao editar a Campanha. Datas de inicio ou fim invalidas.', 'metro/flash/error');
            }
        }

        $this->Campanha->id = $id;

        $campanha = $this->Campanha->read();

        if (!$campanha) {
            $this->Session->setFlash('Campanha inválida ou inexistente.', 'metro/flash/error');
        } else {
            //debug($campanha);
            //formatar as datas do date picker
            $dateTime = $this->create_date_time_from_format('Y-m-d', $campanha['Campanha']['data_inicio']);
            $campanha['Campanha']['data_inicio_aux'] = date_format($dateTime, 'd-m-Y');
            unset($campanha['Campanha']['data_inicio']);
            
            $dateTime = $this->create_date_time_from_format('Y-m-d', $campanha['Campanha']['data_fim']);
            $campanha['Campanha']['data_fim_aux'] = date_format($dateTime, 'd-m-Y');
            unset($campanha['Campanha']['data_fim']);

            $cond = array('campanha_id' => $id);

            //$extras_atuais = $this->Extra->find('all', array('conditions' => $cond));
            $this->set('extras', $campanha['CampanhasExtra']);

            $this->data = $campanha;

            //debug($this->data);
        }
        

        $eventos = array();
        $this->Evento->recursive = 1;
        foreach ($this->Evento->find('all', array('conditions' => array('turma_id' => $turma_id))) as $evento) {
            $eventos[$evento['Evento']['id']] = $evento['Evento']['nome'];
        }

        //debug($eventos);
        $this->set('eventos', $eventos);
    }
    
    function planejamento_alterar($id = false){
        $this->_alterar($id);
    }
    
    private function _inserir(){
        $this->layout = false;
        $turma = $this->Session->read('turma');
            $turma_id = $turma['Turma']['id'];

            if (!empty($this->data)) {
                $this->autoRender = false;
                Configure::write(array('debug' => 0));
                if (($this->data['Campanha']['data_inicio_aux'] != "") || ($this->data['Campanha']['data_fim_aux'] != "")) {
                    //formatar as datas do date picker
                    $dateTime = str_replace('/','-',$this->data['Campanha']['data_inicio_aux']);
                    $dateTime = $this->create_date_time_from_format('d-m-Y',$dateTime);
                    $this->data['Campanha']['data_inicio'] = date_format($dateTime, 'Y-m-d');
                    unset($this->data['Campanha']['data_inicio_aux']);

                    $dateTime = str_replace('/','-',$this->data['Campanha']['data_fim_aux']);
                    $dateTime = $this->create_date_time_from_format('d-m-Y', $dateTime);
                    $this->data['Campanha']['data_fim'] = date_format($dateTime, 'Y-m-d');
                    unset($this->data['Campanha']['data_fim_aux']);

                    $this->data['Campanha']['turma_id'] = $turma_id;

                    $this->data['CampanhasExtra'] = array();

                    // Retira apenas as partes importantes e as insere em um array 
                    preg_match_all('/\{extra_id:([\d]*), evento_id:([a-z]*), quantidade_minima:([\d]*), quantidade_maxima:([\d]*), quantidade_disponivel:([\d]*), valor:([\d]*[,.]?[\d]*)\}*/', $this->data['Campanha']['adicionar_extras_array'], $matches);
                    $adicionarArray = array();
                    for ($i = 0; $i < count($matches[1]); $i++) {
                        $adicionarArray[] = array(
                            'extra_id' => $matches[1][$i],
                            'evento_id' => $matches[2][$i],
                            'quantidade_minima' => $matches[3][$i],
                            'quantidade_maxima' => $matches[4][$i],
                            'quantidade_disponivel' => $matches[5][$i],
                            'valor' => str_replace(',', '.', $matches[6][$i]),
                            'tem_observacao' => 0
                        );
                    }

                    $this->data['CampanhasExtra'] = $adicionarArray;

                    $this->Campanha->bindModel(array('hasMany' => array('CampanhasExtra' =>
                            array('foreignKey' => 'campanha_id'))));

                    if ($this->Campanha->saveAll($this->data)) {
                        $this->Session->setFlash('Campanha criada com sucesso.', 'metro/flash/success');
                    } else {
                        $this->Session->setFlash('Ocorreu um erro ao criar a Campanha.', 'metro/flash/error');
                    }
                    json_encode(array());
                } else {
                    $this->Session->setFlash('Ocorreu um erro ao criar a Campanha. Datas de inicio ou fim invalidas.', 'metro/flash/error');
                }
            }

            $eventos = array();
            $this->Evento->recursive = 1;
            foreach ($this->Evento->find('all', array('conditions' => array('turma_id' => $turma_id))) as $evento) {
                $eventos[$evento['Evento']['id']] = $evento['Evento']['nome'];
            }

            //debug($eventos);
            $this->set('eventos', $eventos);
            $this->set('extras', '');
        }
    
    function planejamento_inserir(){
        $this->_inserir();
    }

    function formando_listar() {
        $this->_listar();
    }

    function comissao_listar() {
        $this->_listar();
    }
    
    function planejamento_listar() {
        $this->_listar();
    }
    
    function atendimento_listar() {
        $this->_listar();
    }
    
    private function _exibir($id = false){
        $this->set('data', $this->obterCampanhaDeID($id));
    }
    
    function planejamento_exibir($id = false){
        $this->_exibir($id);
    }
    
    function atendimento_exibir($id = false){
        $this->_exibir($id);
    }

    function planejamento_index($arrayDeTiposQueDevemSerExibidos = false) {

        $turma = $this->Session->read('turma');

        $cond = array(
            'Campanha.turma_id' => $turma['Turma']['id']
        );

        if ($arrayDeTiposQueDevemSerExibidos != false)
            $cond['Campanha.tipo'] = $arrayDeTiposQueDevemSerExibidos;

        $campanhasPaginadas = $this->paginate('Campanha', $cond);

        for ($indiceCampanha = 0; $indiceCampanha < count($campanhasPaginadas); $indiceCampanha++) {
            $dataInicioDaCampanha = strtotime($campanhasPaginadas[$indiceCampanha]['Campanha']['data_inicio']);
            $dataLimiteDaCampanha = strtotime($campanhasPaginadas[$indiceCampanha]['Campanha']['data_fim']);
            $dataAtual = strtotime(date("Y-m-d 00:00:00"));
            if (($dataLimiteDaCampanha >= $dataAtual) && ($dataInicioDaCampanha <= $dataAtual))
                $campanhasPaginadas[$indiceCampanha]['Campanha']['status'] = "Aberta";
            else
                $campanhasPaginadas[$indiceCampanha]['Campanha']['status'] = "Fechada";

            $campanhasPaginadas[$indiceCampanha]['Campanha']['data_inicio'] = date("d-m-Y H:i:s", $dataInicioDaCampanha);
            $campanhasPaginadas[$indiceCampanha]['Campanha']['data_fim'] = date("d-m-Y H:i:s", $dataLimiteDaCampanha);
        }



        $this->set('campanhas', $campanhasPaginadas);
    }

    function formando_index() {
        $this->planejamento_index(array('comum'));
    }

    function comissao_index() {
        $this->layout = false;
    	$this->planejamento_index(array('comum', 'final'));
    }

    function atendimento_index() {
        $this->planejamento_index(array('comum', 'final'));
    }

    function comissao_exibir($id) {
    	$this->planejamento_exibir($id);
    }
    
    function comissao_visualizar($id) {
    	$this->planejamento_exibir($id);
    }

    function atendimento_visualizar($id) {
    	$this->planejamento_exibir($id);
    }

    function planejamento_adicionar() {
        $turma = $this->Session->read('turma');
        $turma_id = $turma['Turma']['id'];

        if (!empty($this->data)) {
            if (($this->data['Campanha']['data_inicio_aux'] != "") || ($this->data['Campanha']['data_fim_aux'] != "")) {
                //formatar as datas do date picker
                $dateTime = $this->create_date_time_from_format('d-m-Y', $this->data['Campanha']['data_inicio_aux']);
                $this->data['Campanha']['data_inicio'] = date_format($dateTime, 'Y-m-d');
                unset($this->data['Campanha']['data_inicio_aux']);

                $dateTime = $this->create_date_time_from_format('d-m-Y', $this->data['Campanha']['data_fim_aux']);
                $this->data['Campanha']['data_fim'] = date_format($dateTime, 'Y-m-d');
                unset($this->data['Campanha']['data_fim_aux']);


                $this->data['Campanha']['turma_id'] = $turma_id;



                $this->data['CampanhasExtra'] = array();

                // Retira apenas as partes importantes e as insere em um array 
                preg_match_all('/\{extra_id:([\d]*), evento_id:([a-z]*), quantidade_minima:([\d]*), quantidade_maxima:([\d]*), quantidade_disponivel:([\d]*), valor:([\d]*[,.]?[\d]*)\}*/', $this->data['Campanha']['adicionar_extras_array'], $matches);
                $adicionarArray = array();
                for ($i = 0; $i < count($matches[1]); $i++) {
                    $adicionarArray[] = array(
                        'extra_id' => $matches[1][$i],
                        'evento_id' => $matches[2][$i],
                        'quantidade_minima' => $matches[3][$i],
                        'quantidade_maxima' => $matches[4][$i],
                        'quantidade_disponivel' => $matches[5][$i],
                        'valor' => str_replace(',', '.', $matches[6][$i]),
                        'tem_observacao' => 0
                    );
                }

                $this->data['CampanhasExtra'] = $adicionarArray;



                $this->Campanha->bindModel(array('hasMany' => array('CampanhasExtra' =>
                        array('foreignKey' => 'campanha_id'))));

                if ($this->Campanha->saveAll($this->data)) {
                    $this->Session->setFlash('Campanha criada com sucesso.', 'flash_sucesso');
                    $this->redirect("/{$this->params['prefix']}/campanhas/visualizar/{$this->Campanha->getLastInsertId()}");
                } else {
                    $this->Session->setFlash('Ocorreu um erro ao criar a Campanha.', 'flash_erro');
                }
            } else {
                $this->Session->setFlash('Ocorreu um erro ao criar a Campanha. Datas de inicio ou fim invalidas.', 'flash_erro');
            }
        }

        $eventos = array();
        $this->Evento->recursive = 1;
        foreach ($this->Evento->find('all', array('conditions' => array('turma_id' => $turma_id))) as $evento) {
            $eventos[$evento['Evento']['id']] = $evento['Evento']['nome'];
        }

        //debug($eventos);
        $this->set('eventos', $eventos);
        $this->set('extras', '');
    }

    function planejamento_edditar($id = null) {
        $turma = $this->Session->read('turma');
        $turma_id = $turma['Turma']['id'];

        $this->Campanha->recursive = 3;
        $this->Campanha->bindModel(array('hasMany' => array('CampanhasExtra' => array('foreignKey' => 'campanha_id'))));

        $this->CampanhasExtra->bindModel(array('belongsTo' => array('Extra')));
        $this->CampanhasExtra->recursive = 3;

        $this->Extra->bindModel(array('belongsTo' => array('Evento')));
        $this->Extra->recursive = 2;

        $this->Evento->recursive = 2;
        $this->Evento->bindModel(array('hasMany' => array('Extra' => array('foreignKey' => 'evento_id'))));
        $ocorreuErro = false;

        if (!empty($this->data)) {
            if (($this->data['Campanha']['data_inicio_aux'] != "") || ($this->data['Campanha']['data_fim_aux'] != "")) {
                //formatar as datas do date picker
                $dateTime = $this->create_date_time_from_format('d-m-Y', $this->data['Campanha']['data_inicio_aux']);
                $this->data['Campanha']['data_inicio'] = date_format($dateTime, 'Y-m-d');
                unset($this->data['Campanha']['data_inicio_aux']);

                $dateTime = $this->create_date_time_from_format('d-m-Y', $this->data['Campanha']['data_fim_aux']);
                $this->data['Campanha']['data_fim'] = date_format($dateTime, 'Y-m-d');
                unset($this->data['Campanha']['data_fim_aux']);

                $this->data['CampanhasExtra'] = array();
                // Retira apenas as partes importantes e as insere em um array 
                preg_match_all('/\{extra_id:([\d]*), evento_id:([a-z]*), quantidade_minima:([\d]*), quantidade_maxima:([\d]*), quantidade_disponivel:([\d]*), valor:([\d]*[,.]?[\d]*)\}*/', $this->data['Campanha']['adicionar_extras_array'], $matches);
                $adicionarArray = array();
                for ($i = 0; $i < count($matches[1]); $i++) {
                    $adicionarArray[] = array(
                        'extra_id' => $matches[1][$i],
                        'evento_id' => $matches[2][$i],
                        'quantidade_minima' => $matches[3][$i],
                        'quantidade_maxima' => $matches[4][$i],
                        'quantidade_disponivel' => $matches[5][$i],
                        'valor' => str_replace(',', '.', $matches[6][$i]),
                        'tem_observacao' => 0
                    );
                }

                preg_match_all('/\{campanha_extra_id:([\d]*)\}*/', $this->data['Campanha']['remover_extras_array'], $matches);
                $removerArray = array();
                for ($i = 0; $i < count($matches[1]); $i++) {
                    $removerArray[] = $matches[1][$i];
                }


                $this->CampanhasExtra->deleteAll(array('CampanhasExtra.id' => $removerArray));

                $this->data['CampanhasExtra'] = $adicionarArray;

                if (count($adicionarArray) == 0) {
                    $this->Campanha->unbindModel(array('hasMany' => array('CampanhasExtra')));
                }
                else
                    $this->Campanha->bindModel(array('hasMany' => array('CampanhasExtra' => array('foreignKey' => 'campanha_id'))));

                //debug($this->data);
                if ($this->Campanha->saveAll($this->data)) {
                    $this->Session->setFlash('Campanha foi salva corretamente.', 'flash_sucesso');
                    $this->redirect("/{$this->params['prefix']}/campanhas/visualizar/{$id}");
                } else {
                    $ocorreuErro = true;
                    $this->Session->setFlash('Um erro ocorreu na atualização da campanha.', 'flash_erro');
                }
            } else {
                $ocorreuErro = true;
                $this->Session->setFlash('Ocorreu um erro ao editar a Campanha. Datas de inicio ou fim invalidas.', 'flash_erro');
            }
        }

        $this->Campanha->id = $id;

        $campanha = $this->Campanha->read();

        if (!$campanha) {
            $this->Session->setFlash('Campanha inválida ou inexistente.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/campanhas");
        } else {
            //debug($campanha);
            //formatar as datas do date picker
            $dateTime = $this->create_date_time_from_format('Y-m-d', $campanha['Campanha']['data_inicio']);
            $campanha['Campanha']['data_inicio_aux'] = date_format($dateTime, 'd-m-Y');
            unset($campanha['Campanha']['data_inicio']);

            $dateTime = $this->create_date_time_from_format('Y-m-d', $campanha['Campanha']['data_fim']);
            $campanha['Campanha']['data_fim_aux'] = date_format($dateTime, 'd-m-Y');
            unset($campanha['Campanha']['data_fim']);

            $cond = array('campanha_id' => $id);

            //$extras_atuais = $this->Extra->find('all', array('conditions' => $cond));
            $this->set('extras', $campanha['CampanhasExtra']);

            $this->data = $campanha;

            //debug($this->data);
        }


        $eventos = array();
        $this->Evento->recursive = 1;
        foreach ($this->Evento->find('all', array('conditions' => array('turma_id' => $turma_id))) as $evento) {
            $eventos[$evento['Evento']['id']] = $evento['Evento']['nome'];
        }

        //debug($eventos);
        $this->set('eventos', $eventos);

        if ($ocorreuErro)
            $this->redirect("/{$this->params['prefix']}/campanhas/editar/{$id}");
    }

    function planejamento_checkout_visualizar($idDaCampanha = null, $idDoUsuario = null, $idDoFormandoProfile, $tiposPermitidos = array('final')) {
        $campanha = $this->obterCampanhaDeID($idDaCampanha);

        if ($this->usuarioPodeAcessarATurmaDeID($campanha['Campanha']['turma_id']) && in_array($campanha['Campanha']['tipo'], $tiposPermitidos)) {
            $this->set('data', $campanha);

            $adesoesDoUsuario = $this->obterAdesoesDoUsuarioParaACampanhaDeID($campanha['Campanha']['id'], $idDoUsuario);
            $optionsDeCompraDeExtras = $this->obterOptionsDeCompraDeExtras($campanha['CampanhasExtra'], $adesoesDoUsuario);
            $campanhasExtras = $this->obterHashDeExtrasPorIDDaCampanha($campanha['CampanhasExtra']);

            $this->set('idDaCampanha', $idDaCampanha);
            $this->set('idDoUsuario', $idDoUsuario);
            $this->set('idDoFormandoProfile', $idDoFormandoProfile);
            $this->set('adesoesDoUsuario', $adesoesDoUsuario);
            $this->set('campanhasExtras', $campanhasExtras);
            $this->set('optionsDeCompraDeExtras', $optionsDeCompraDeExtras);
        } else {
            $this->Session->setFlash('Campanha inválida ou inexistente.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/ormandos/checkout/");
        }
    }

    function formando_exibir($id = false, $tiposPermitidos = array('comum')) {
        $this->autoLayout = false;
        $campanha = $this->obterCampanhaDeID($id);
        if ($this->usuarioPodeAcessarATurmaDeID($campanha['Campanha']['turma_id']) && in_array($campanha['Campanha']['tipo'], $tiposPermitidos)) {
            $this->set('campanha', $campanha);
            $adesoesDoUsuario = $this->obterAdesoesDoUsuarioLogadoParaACampanhaDeID($campanha['Campanha']['id']);
            $optionsDeCompraDeExtras = $this->obterOptionsDeCompraDeExtras($campanha['CampanhasExtra'], $adesoesDoUsuario);
            $campanhasExtras = $this->obterHashDeExtrasPorIDDaCampanha($campanha['CampanhasExtra']);
            
            if ($this->Session->check('mensagem_despesa')) {
                $mensagemDeAviso = $this->Session->read('mensagem_despesa');
                $this->Session->delete('mensagem_despesa');
                $this->set('mensagem_despesa', $mensagemDeAviso);
            }

            $this->set('adesoesDoUsuario', $adesoesDoUsuario);
            $this->set('campanhasExtras', $campanhasExtras);
            $this->set('optionsDeCompraDeExtras', $optionsDeCompraDeExtras);
        } else {
            $this->set('campanha', false);
        }
    }

    function formando_visualizar($id = null, $tiposPermitidos = array('comum')) {
        $campanha = $this->obterCampanhaDeID($id);
        if ($this->usuarioPodeAcessarATurmaDeID($campanha['Campanha']['turma_id']) && in_array($campanha['Campanha']['tipo'], $tiposPermitidos)) {
            $this->set('data', $campanha);

            $adesoesDoUsuario = $this->obterAdesoesDoUsuarioLogadoParaACampanhaDeID($campanha['Campanha']['id']);
            $optionsDeCompraDeExtras = $this->obterOptionsDeCompraDeExtras($campanha['CampanhasExtra'], $adesoesDoUsuario);
            $campanhasExtras = $this->obterHashDeExtrasPorIDDaCampanha($campanha['CampanhasExtra']);

            if ($this->Session->check('mensagem_despesa')) {
                $mensagemDeAviso = $this->Session->read('mensagem_despesa');
                $this->Session->delete('mensagem_despesa');
                $this->set('mensagem_despesa', $mensagemDeAviso);
            }

            $this->set('adesoesDoUsuario', $adesoesDoUsuario);
            $this->set('campanhasExtras', $campanhasExtras);
            $this->set('optionsDeCompraDeExtras', $optionsDeCompraDeExtras);
        } else {
            $this->Session->setFlash('Campanha inválida ou inexistente.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/campanhas");
        }
    }

    function formando_comprar() {
        $campanha = $this->obterCampanhaDeID($this->params['data']['Campanha']['id']);
        if ($this->dadosEnviadosParaAdesaoSaoValidos($campanha)) {
            $this->salvarAdesaoDoUsuarioLogadoACampanha($campanha);
        }

        $this->redirect("/{$this->params['prefix']}/campanhas/visualizar/{$this->params['data']['Campanha']['id']}");
    }

    function formando_inserir() {
        if (!$this->RequestHandler->isAjax())
            $this->redirect("/{$this->params['prefix']}/");
       Configure::write(array('debug' => 0));
        $this->autoRender = false;
        $campanha = $this->obterCampanhaDeID($this->params['data']['Campanha']['id']);
        if ($this->dadosEnviadosParaAdesaoSaoValidos($campanha, "flash_metro_error")){
            $this->salvarAdesaoUsuarioLogadoACampanha($campanha);
        }
        $this->set('usuario', $this->obterUsuarioLogado());
    }

    function planejamento_checkout_comprar($idDoUsuario, $idDoFormandoProfile) {
        $campanha = $this->obterCampanhaDeID($this->params['data']['Campanha']['id']);
        if ($this->dadosEnviadosParaAdesaoSaoValidos($campanha)) {
            $this->salvarAdesaoDoUsuarioACampanha($idDoUsuario, $campanha);
        }

        $this->redirect("/{$this->params['prefix']}/campanhas/checkout_visualizar/{$this->params['data']['Campanha']['id']}/{$idDoUsuario}/{$idDoFormandoProfile}");
    }

    private function salvarAdesaoUsuarioLogadoACampanha($campanha) {
        $usuarioLogado = $this->obterUsuarioLogado();
        $adesoesDoUsuario = $this->obterAdesoesDoUsuarioLogadoParaACampanhaDeID($campanha['Campanha']['id']);
        $optionsDeCompraDeExtras = $this->obterOptionsDeCompraDeExtras($campanha['CampanhasExtra'], $adesoesDoUsuario);
        $campanhasExtras = $this->obterHashDeExtrasPorIDDaCampanha($campanha['CampanhasExtra']);

        $campanhasUsuarioId = $this->criarCampanhasUsuarioParaUsuarioLogado($campanha['Campanha']['id']);

        if ($campanhasUsuarioId > 0) {
            $valorTotal = 0.0;
            foreach ($campanha['CampanhasExtra'] as $relacaoCampanhaExtra) {
                $quantidadeASerComprada = $this->params['data']['campanhaExtra' . $relacaoCampanhaExtra['id']];
                if ($quantidadeASerComprada > 0) {
                    $this->criarCampanhasExtra($relacaoCampanhaExtra['id'], $campanhasUsuarioId, $quantidadeASerComprada, $relacaoCampanhaExtra['valor']);
                    $valorTotal += $quantidadeASerComprada * $relacaoCampanhaExtra['valor'];
                }
            }

            $this->criarDespesasParaUsuarioLogado($valorTotal, $campanhasUsuarioId);
            $this->Session->setFlash("Solicitação efetuada com sucesso. Clique <a class='fg-color-red' data-bind=' "
                    . "click: function (){ page (\"/{$this->params['prefix']}/area_financeira/dados/{$usuarioLogado['Usuario']['id']}/campanhas/1\")}'><strong>aqui</strong></a> para gerar os boletos. <br />"
                    . "<h4 class='fg-color-red'>As solicitações de compras de extras só serão efetivadas após o pagamento da primeira ou única parcela, "
                    . "até o seu vencimento. Caso não ocorra o pagamento, sua solicitação será cancelada.</h4>", 'flash_metro_warning');
        }
    }

    private function salvarAdesaoDoUsuarioACampanha($idDoUsuario, $campanha) {
        $adesoesDoUsuario = $this->obterAdesoesDoUsuarioParaACampanhaDeID($campanha['Campanha']['id'], $idDoUsuario);
        $optionsDeCompraDeExtras = $this->obterOptionsDeCompraDeExtras($campanha['CampanhasExtra'], $adesoesDoUsuario);
        $campanhasExtras = $this->obterHashDeExtrasPorIDDaCampanha($campanha['CampanhasExtra']);

        $campanhasUsuarioId = $this->criarCampanhasUsuario($campanha['Campanha']['id'], $idDoUsuario);
        if ($campanhasUsuarioId > 0) {
            $valorTotal = 0.0;
            foreach ($campanha['CampanhasExtra'] as $relacaoCampanhaExtra) {
                $quantidadeASerComprada = $this->params['data']['campanhaExtra' . $relacaoCampanhaExtra['id']];
                if ($quantidadeASerComprada > 0) {
                    $this->criarCampanhasExtra($relacaoCampanhaExtra['id'], $campanhasUsuarioId, $quantidadeASerComprada);
                    $valorTotal += $quantidadeASerComprada * $relacaoCampanhaExtra['valor'];
                }
            }

            $this->criarDespesas($valorTotal, $campanhasUsuarioId, $idDoUsuario);

            $this->Session->setFlash("Compra efetuada com sucesso. Clique <a href='/{$this->params['prefix']}/area_financeira#div_despesas_extras'>aqui</a> para mais informações.", 'flash_sucesso');
        }
    }

    private function salvarAdesaoDoUsuarioLogadoACampanha($campanha) {
        $adesoesDoUsuario = $this->obterAdesoesDoUsuarioLogadoParaACampanhaDeID($campanha['Campanha']['id']);
        $optionsDeCompraDeExtras = $this->obterOptionsDeCompraDeExtras($campanha['CampanhasExtra'], $adesoesDoUsuario);
        $campanhasExtras = $this->obterHashDeExtrasPorIDDaCampanha($campanha['CampanhasExtra']);

        $campanhasUsuarioId = $this->criarCampanhasUsuarioParaUsuarioLogado($campanha['Campanha']['id']);

        if ($campanhasUsuarioId > 0) {
            $valorTotal = 0.0;
            foreach ($campanha['CampanhasExtra'] as $relacaoCampanhaExtra) {
                $quantidadeASerComprada = $this->params['data']['campanhaExtra' . $relacaoCampanhaExtra['id']];
                if ($quantidadeASerComprada > 0) {
                    $this->criarCampanhasExtra($relacaoCampanhaExtra['id'], $campanhasUsuarioId, $quantidadeASerComprada, $relacaoCampanhaExtra['valor']);
                    $valorTotal += $quantidadeASerComprada * $relacaoCampanhaExtra['valor'];
                }
            }

            $this->criarDespesasParaUsuarioLogado($valorTotal, $campanhasUsuarioId);

            $this->Session->setFlash("Compra efetuada com sucesso. Clique <a href='/{$this->params['prefix']}/area_financeira#div_despesas_extras'>aqui</a> para mais informações.", 'flash_sucesso');
        }
    }

    private function criarCampanhasUsuarioParaUsuarioLogado($idDaCampanha) {
        $usuario = $this->Session->read('Usuario');
        return $this->criarCampanhasUsuario($idDaCampanha, $usuario['Usuario']['id']);
    }

    private function criarCampanhasUsuario($idDaCampanha, $idDoUsuario) {
        $this->CampanhasUsuario->create();
        $novaCampanhaUsuario['CampanhasUsuario']['parcelas'] = $this->params['data']['numero-de-parcelas'];
        $novaCampanhaUsuario['CampanhasUsuario']['usuario_id'] = $idDoUsuario;
        $novaCampanhaUsuario['CampanhasUsuario']['campanha_id'] = $idDaCampanha;
        $novaCampanhaUsuario['CampanhasUsuario']['data'] = date('Y-m-d H:i:s');
        $this->CampanhasUsuario->save($novaCampanhaUsuario);
        return $this->CampanhasUsuario->id;
    }

    private function criarCampanhasExtra($relacaoCampanhaExtraId, $campanhasUsuarioId, $quantidadeComprada, $valor_unitario) {
        $this->CampanhasUsuarioCampanhasExtra->create();
        $novaCampanhasUsuarioCampanhasExtra['CampanhasUsuarioCampanhasExtra']['campanhas_usuario_id'] = $campanhasUsuarioId;
        $novaCampanhasUsuarioCampanhasExtra['CampanhasUsuarioCampanhasExtra']['campanhas_extra_id'] = $relacaoCampanhaExtraId;
        $novaCampanhasUsuarioCampanhasExtra['CampanhasUsuarioCampanhasExtra']['quantidade'] = $quantidadeComprada;
        $novaCampanhasUsuarioCampanhasExtra['CampanhasUsuarioCampanhasExtra']['valor_unitario'] = $valor_unitario;
        $this->CampanhasUsuarioCampanhasExtra->save($novaCampanhasUsuarioCampanhasExtra);
        return;
    }

    private function criarDespesasParaUsuarioLogado($valorTotal, $campanhasUsuarioId) {
        $usuario = $this->Session->read('Usuario');
        $this->criarDespesas($valorTotal, $campanhasUsuarioId, $usuario['Usuario']['id']);
        return;
    }

    private function criarDespesas($valorTotal, $campanhasUsuarioId, $idDoUsuario) {
        $valorParcela = round($valorTotal / $this->params['data']['numero-de-parcelas'], 2);

        $this->FormandoProfile->recursive = -1;
        $formandProfileDoUsuarioLogado = $this->FormandoProfile->find('first', array('conditions' => array('FormandoProfile.usuario_id' => $idDoUsuario)));

        for ($i = 0; $i < $this->params['data']['numero-de-parcelas']; $i++) {
            $this->Despesa->create();
            if($i == 0)
                $diaDeVencimento = date('Y-m-d', strtotime('+5 days'));
            else
                $diaDeVencimento = date('Y-m-d', strtotime("+{$i} month"));
            $novaDespesa['Despesa'] = array(
                'usuario_id' => $idDoUsuario,
                'campanhas_usuario_id' => $campanhasUsuarioId,
                'valor' => $valorParcela,
                'data_cadastro' => date('Y-m-d H:i:s', strtotime('now')),
                'tipo' => 'extra',
                'parcela' => $i + 1,
                'total_parcelas' => $this->params['data']['numero-de-parcelas'],
                'data_vencimento' => $diaDeVencimento
            );
            $this->Despesa->save($novaDespesa['Despesa']);
        }
        return;
    }

    private function obterCampanhaDeID($id) {
        $this->Campanha->recursive = 3;
        $this->Campanha->bindModel(
                array(
                    'hasMany' => array(
                        'CampanhasExtra' => array('foreignKey' => 'campanha_id')
                    )
                )
        );

        $this->CampanhasExtra->bindModel(array('belongsTo' => array('Extra')));
        $this->CampanhasExtra->recursive = 3;

        $this->Extra->bindModel(array('belongsTo' => array('Evento')));
        $this->Extra->recursive = 2;

        $this->Evento->recursive = 2;
        $this->Evento->bindModel(array('hasMany' => array('Extra' => array('foreignKey' => 'evento_id'))));

        $campanha = $this->Campanha->find('first', array('conditions' => array('Campanha.id' => $id)));

        $dataInicioDaCampanha = strtotime($campanha['Campanha']['data_inicio']);
        $dataLimiteDaCampanha = strtotime($campanha['Campanha']['data_fim']);
        $dataAtual = strtotime(date("Y-m-d 00:00:00"));
        if (($dataLimiteDaCampanha >= $dataAtual) && ($dataInicioDaCampanha <= $dataAtual))
            $campanha['Campanha']['status'] = "Aberta";
        else
            $campanha['Campanha']['status'] = "Fechada";
        $campanha['Campanha']['data_inicio'] = date("d-m-Y H:i:s", $dataInicioDaCampanha);
        $campanha['Campanha']['data_fim'] = date("d-m-Y H:i:s", $dataLimiteDaCampanha);

        return $campanha;
    }

    private function usuarioPodeAcessarATurmaDeID($idDaTurmaASerVerificada) {
        $turmaLogada = $this->obterTurmaLogada();
        if ($turmaLogada['Turma']['id'] == $idDaTurmaASerVerificada)
            return true;
        return false;
    }

    private function obterAdesoesDoUsuarioLogadoParaACampanhaDeID($idDaCampanha) {
        $usuario = $this->Session->read('Usuario');

        return $this->obterAdesoesDoUsuarioParaACampanhaDeID($idDaCampanha, $usuario['Usuario']['id']);
    }

    private function obterAdesoesDoUsuarioParaACampanhaDeID($idDaCampanha, $idDoUsuario) {
        $this->CampanhasUsuario->recursive = 3;
        $this->CampanhasUsuario->bindModel(
                array(
                    'hasMany' => array(
                        'CampanhasUsuariosCampanhasExtras' => array('foreignKey' => 'campanhas_usuario_id')
                    )
                )
        );

        $usuario = $this->Session->read('Usuario');

        $adesoesDoUsuario = $this->CampanhasUsuario->find('all', array(
            'conditions' => array('campanha_id' => $idDaCampanha, 'usuario_id' => $idDoUsuario, 'cancelada' => 0))
        );

        return $adesoesDoUsuario;
    }

    private function obterHashDeExtrasPorIDDaCampanha($relacoesCampanhaExtra) {
        $campanhasExtras;

        foreach ($relacoesCampanhaExtra as $CampanhaExtra) {
            $campanhasExtras[$CampanhaExtra['id']]['Extra'] = $CampanhaExtra['Extra'];
            $campanhasExtras[$CampanhaExtra['id']]['valor'] = $CampanhaExtra['valor'];
        }

        return $campanhasExtras;
    }

    private function obterOptionsDeCompraDeExtras($relacoesCampanhaExtra, $adesoesDoUsuario) {
        $arrayDeOptions = array();
        $quantidadesMinimaDeExtras = $this->obterQuantidadeMinimaASerCompradaDeCadaRelacaoCampanhaExtra($relacoesCampanhaExtra, $adesoesDoUsuario);
        $quantidadesMaximaDeExtras = $this->obterQuantidadeMaximaASerCompradaDeCadaRelacaoCampanhaExtra($relacoesCampanhaExtra, $adesoesDoUsuario);
        $qtdeDisponivelDeExtras = $this->obterQuantidadeDisponivelDeExtras($relacoesCampanhaExtra);
        foreach ($relacoesCampanhaExtra as $CampanhaExtra) {
            $limiteInferiorDoOption = $quantidadesMinimaDeExtras[$CampanhaExtra['id']];
            $limiteSuperiorDoOption = $quantidadesMaximaDeExtras[$CampanhaExtra['id']];
            if ($quantidadesMaximaDeExtras[$CampanhaExtra['id']] > $qtdeDisponivelDeExtras[$CampanhaExtra['id']])
                $limiteSuperiorDoOption = $qtdeDisponivelDeExtras[$CampanhaExtra['id']];
            $optionArray = array();

            if ($limiteInferiorDoOption > 0 || $limiteSuperiorDoOption < 0)
                $optionArray[0] = 0;

            for ($i = $limiteInferiorDoOption; $i <= $limiteSuperiorDoOption; $i++) {
                $optionArray[$i] = $i;
            }
            $arrayDeOptions[$CampanhaExtra['id']] = $optionArray;
        }
        return $arrayDeOptions;
    }

    private function obterQuantidadeMinimaASerCompradaDeCadaRelacaoCampanhaExtra($relacoesCampanhaExtra, $adesoesDoUsuario) {
        $numeroDeComprasDeCadaCampanhaExtra = $this->obterNumeroDeComprasDeCadaRelacaoCampanhaExtra($adesoesDoUsuario);
        $quantidadesMinimas = array();

        foreach ($relacoesCampanhaExtra as $CampanhaExtra) {
            if (!isset($numeroDeComprasDeCadaCampanhaExtra[$CampanhaExtra['id']]))
                $numeroDeComprasDeCadaCampanhaExtra[$CampanhaExtra['id']] = 0;

            if ($numeroDeComprasDeCadaCampanhaExtra[$CampanhaExtra['id']] >= $CampanhaExtra['quantidade_minima'])
                $quantidadesMinimas[$CampanhaExtra['id']] = 0;
            else
                $quantidadesMinimas[$CampanhaExtra['id']] = $CampanhaExtra['quantidade_minima'] - $numeroDeComprasDeCadaCampanhaExtra[$CampanhaExtra['id']];
        }

        return $quantidadesMinimas;
    }

    private function obterQuantidadeMaximaASerCompradaDeCadaRelacaoCampanhaExtra($relacoesCampanhaExtra, $adesoesDoUsuario) {
        $numeroDeComprasDeCadaCampanhaExtra = $this->obterNumeroDeComprasDeCadaRelacaoCampanhaExtra($adesoesDoUsuario);
        $quantidadesMaximas = array();

        foreach ($relacoesCampanhaExtra as $CampanhaExtra) {
            if (!isset($numeroDeComprasDeCadaCampanhaExtra[$CampanhaExtra['id']]))
                $numeroDeComprasDeCadaCampanhaExtra[$CampanhaExtra['id']] = 0;
            $quantidadeDisponivelParaCompra = $CampanhaExtra['quantidade_maxima'] - $numeroDeComprasDeCadaCampanhaExtra[$CampanhaExtra['id']];
            $quantidadesMaximas[$CampanhaExtra['id']] = min($CampanhaExtra['quantidade_maxima'], $quantidadeDisponivelParaCompra);
        }

        return $quantidadesMaximas;
    }

    private function obterNumeroDeComprasDeCadaRelacaoCampanhaExtra($adesoesDoUsuario) {
        $numeroDeComprasDeUmaCampanhaExtra = array();
        for ($indiceAdesoesDoUsuario = 0; $indiceAdesoesDoUsuario < count($adesoesDoUsuario); $indiceAdesoesDoUsuario++) {
            foreach ($adesoesDoUsuario[$indiceAdesoesDoUsuario]['CampanhasUsuariosCampanhasExtras'] as $adesaoExtra) {
                if (isset($numeroDeComprasDeUmaCampanhaExtra[$adesaoExtra['campanhas_extra_id']]))
                    $numeroDeComprasDeUmaCampanhaExtra[$adesaoExtra['campanhas_extra_id']] += $adesaoExtra['quantidade'];
                else
                    $numeroDeComprasDeUmaCampanhaExtra[$adesaoExtra['campanhas_extra_id']] = $adesaoExtra['quantidade'];
            }
        }
        return $numeroDeComprasDeUmaCampanhaExtra;
    }
    
    private function obterQuantidadeDisponivelDeExtras($campanhasExtras) {
        $qtdeDisponivelDeExtras = array();
        foreach ($campanhasExtras as $campanhaExtra) {
            $this->CampanhasUsuarioCampanhasExtra->recursive = 1;
            $qtdeVendida = $this->CampanhasUsuarioCampanhasExtra->find('all', array(
                'conditions' => array(
                    'CampanhasUsuarioCampanhasExtra.campanhas_extra_id' => $campanhaExtra['id'],
                    'CampanhasUsuario.cancelada' => 0
                ),
                'fields' => array('sum(quantidade) as total')));
            
            $qtdeDisponivelDeExtras[$campanhaExtra['id']] = $campanhaExtra['quantidade_disponivel'];
            if (!empty($qtdeVendida[0][0]['total']))
                $qtdeDisponivelDeExtras[$campanhaExtra['id']]-= $qtdeVendida[0][0]['total'];
        }
        return $qtdeDisponivelDeExtras;
    }

    private function dadosEnviadosParaAdesaoSaoValidos($campanha, $flash = "flash_erro") {
        if ($campanha['Campanha']['status'] != "Aberta") {
            $this->Session->setFlash('Campanha inválida.', $flash);
            return false;
        }

        if ($this->usuarioPodeAcessarATurmaDeID($campanha['Campanha']['turma_id']) == false) {
            $this->Session->setFlash('Não possui acesso a campanha.', $flash);
            return false;
        }

        if ($this->numeroDeParcelasEscolhidasEValido($campanha) == false) {
            $this->Session->setFlash('Número de parcelas inválido.', $flash);
            return false;
        }

        $campanhasExtras = $this->obterHashDeExtrasPorIDDaCampanha($campanha['CampanhasExtra']);

        if ($this->quantidadeDeExtrasCompradaEValida($campanha) == false) {
            $this->Session->setFlash('Quantidade comprada inválida.', $flash);
            return false;
        }

        return true;
    }

    private function numeroDeParcelasEscolhidasEValido($campanha) {
        if ($this->params['data']['numero-de-parcelas'] > $campanha['Campanha']['max_parcelas'] || $this->params['data']['numero-de-parcelas'] == 0)
            return false;

        return true;
    }

    private function quantidadeDeExtrasCompradaEValida($campanha) {
        $adesoesDoUsuario = $this->obterAdesoesDoUsuarioLogadoParaACampanhaDeID($campanha['Campanha']['id']);
        $optionsDeCompraDeExtras = $this->obterOptionsDeCompraDeExtras($campanha['CampanhasExtra'], $adesoesDoUsuario);
        $quantidadeTotalComprada = 0;
        foreach ($campanha['CampanhasExtra'] as $relacaoCampanhaExtra) {
            if($relacaoCampanhaExtra['ativo'] == 1){
                if ($this->quantidadeCompradaPeloUsuarioExisteNoSelectOption($this->params['data']['campanhaExtra' . $relacaoCampanhaExtra['id']], $optionsDeCompraDeExtras[$relacaoCampanhaExtra['id']]) == true)
                    $quantidadeTotalComprada += $this->params['data']['campanhaExtra' . $relacaoCampanhaExtra['id']];
            }
        }

        if ($quantidadeTotalComprada == 0)
            return false;
        return true;
    }

    private function quantidadeCompradaPeloUsuarioExisteNoSelectOption($quantidadeCompradaPeloUsuario, $selectOption) {
        if (isset($selectOption[$quantidadeCompradaPeloUsuario]))
            return true;

        return false;
    }

    function planejamento_excluir($id) {

        $campanha = $this->Campanha->find('first', array('conditions' => array('Campanha.id' => $id)));
        
        if ($this->Campanha->delete($id)) {
            $this->Session->setFlash('Campanha excluída com sucesso.', 'flash_sucesso');
            $this->redirect("/{$this->params['prefix']}/campanhas/index/");
        } else {
            $this->Session->setFlash('Erro ao excluir a campanha.', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/campanhas/visualizar/$id");
        }
    }
    
    function listar_extras($eventoId) {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $this->Extra->unbindModelAll();
        $extras = $this->Extra->find('all',array(
            'conditions' => array(
                'evento_id' => $eventoId
            )
        ));
        echo json_encode(array('extras' => $extras));
    }

    function planejamento_extra($id) {
        $this->layout = 'ajax';
        $this->set('extra', $this->Extra->find('all', array('conditions' => array('Extra.evento_id' => $id))));
    }

}

?>
