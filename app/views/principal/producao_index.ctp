<div class="span12 metro" style="position:relative" id="content-container">
    <div class="row-fluid content-loader hide" id="content-loading">
        <div class="cycle-loader"></div>
    </div>
    <div class="row-fluid content-loader hide" id="content-body"></div>
    <div class="row-fluid content-loader" id="content-home">
        <?php if(!empty($turmaLogada)) : ?>
        <a href="producao/eventos/selecionar_evento" target="_blank" >
            <div class="tile icon bg-color-purple">
                <div class="tile-content">
                    <span class="img icon-map"></span>
                </div>
                <div class="brand"><span class="name">Mapa de Mesas</span></div>
            </div>
        </a>
        <a href="<?=$this->params['prefix']?>/turmas/relatorio_entrega" target="_blank">
            <div class="tile icon bg-color-pinkDark">
               <div class="tile-content">
                   <span class="img icon-tablet"></span>
               </div>
               <div class="brand"><span class="name">Relatório Mesas e Convites</span></div>
            </div>
        </a>
        <div class="tile icon bg-color-greenLight"
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/relatorio_financeiro/exibir') }">
            <div class="tile-content">
                <span class="img icon-calculate"></span>
            </div>
            <div class="brand"><span class="name">Relatório Financeiro</span></div>
        </div>
        <div class="tile icon bg-color-black"
             data-bind="click: function(data, event) {
             page('<?= $this->webroot . $this->params['prefix'] ?>/checkout/total_checkout/<?=$turmaLogada['Turma']['id']?>') }">
            <div class="tile-content">
                <span class="img icon-bell-3"></span>
            </div>
            <div class="brand"><span class="name">Relatório de Entrega</span></div>
        </div>
        <?php else : ?>
             <div class="tile icon bg-color-red"
                 data-bind="click: function(data, event) {
                 page('/producao/turmas/listar') }">
                <div class="tile-content">
                    <div class="img icon-light-bulb"></div>
                </div>
                <div class="brand">
                    <div class="metro-badge"></div>
                    <span class="name">Turmas Gerenciadas</span>
                </div>
            </div>
            <div class="tile icon bg-color-purple"
                data-bind="click: function(data, event) {
                page('<?=$this->webroot . $this->params['prefix'] ?>/mapa_mesas/plantas') }">
               <div class="tile-content">
                   <span class="img icon-map"></span>
               </div>
               <div class="brand"><span class="name">Plantas<br/>Mapa de Mesa</span></div>
            </div>
        <?php endif; ?>
        <div class="tile icon bg-color-orangeDark"
             data-bind="click: function(data, event) {
             page('/turmas/ficha_evento') }">
            <div class="tile-content">
                <div class="img icon-drawer-2"></div>
            </div>
            <div class="brand">
                <div class="metro-badge"></div>
                <span class="name">Ficha Evento</span>
            </div>
        </div>
        <div class="tile icon bg-color-pink"
             data-bind="click: function(data, event) {
             page('turmas/analise_temporada') }">
            <div class="tile-content">
                <span class="img icon-amazon-2"></span>
            </div>
            <div class="brand"><span class="name">Análise Temporada</span></div>
        </div>
        <div class="tile icon bg-color-purple"
            data-bind="click: function(data, event) {
            page('<?=$this->webroot . $this->params['prefix'] ?>/catalogo/listar') }">
           <div class="tile-content">
               <span class="img icon-book"></span>
           </div>
           <div class="brand"><span class="name">Cat&aacute;logo</span></div>
        </div>
    </div>
</div>