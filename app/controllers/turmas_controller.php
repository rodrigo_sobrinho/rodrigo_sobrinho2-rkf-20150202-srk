<?php

class TurmasController extends AppController {

    var $name = 'Turmas';
    var $uses = array('Turma', 'CursoTurma', 'Curso', 'Universidade', 'Usuario', 'Parcelamento','ViewFormandos','TiposContrato');
    var $helpers = array('ListaAgenda', 'Paginator','Excel');
    var $components = array('CpanelApi', 'WpGerenciador');
    var $nomeDoTemplateSidebar = 'turmas';
    // Padrão de paginação
    var $paginate = array(
        'limit' => 100,
        'order' => array(
            'Turma.id' => 'desc'
        )
    );
    
    function beforeFilter() {
        $this->Auth->allow('site');
        parent::beforeFilter();
    }
    
    function analise_temporada($excel = false){
        $this->layout = false;
        $usuario = $this->Session->read('Usuario');
        $this->loadModel('Evento');
        $this->loadModel('TiposEvento');
        $this->set('tiposEventos', $this->TiposEvento->find('list', array('fields' => 'TiposEvento.nome')));
        if (!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->Session->write("filtros.{$usuario['Usuario']['grupo']}.analise", $this->data);
        } else {
            $options['order'] = array('Turma.id' => 'desc');
            if($excel == 0)
                $options['limit'] = 30;
            else
                $options['limit'] = 9999;
            $options['group'] = 'Turma.id';
            $options['conditions']['Turma.status'] = array('fechada','concluida');
            $options['fields'] = array(
                'Turma.*',
                "(select count(0) from vw_formandos where Turma.id = turma_id) as formandos",
                "(select count(0) from vw_formandos f inner join protocolos p on (p.usuario_id = f.id " .
                    "and p.tipo = 'checkout') left join usuarios u on (u.id= f.id) " .
                    " where Turma.id = turma_id and f.situacao = 'ativo' and u.ativo = 1) as checkout",
                "(select count(0) from vw_formandos vw inner join usuarios u on u.id = vw.id where "
                . "vw.situacao != 'cancelado' and u.ativo = 1 and vw.turma_id = Turma.id) as aderidos",
                'concat(Turma.ano_formatura,Turma.semestre_formatura) as ano',
                "(select group_concat(tipos_eventos.nome separator '<br />') from eventos "
                . "inner join tipos_eventos on tipos_eventos.id = eventos.tipos_evento_id where "
                . "eventos.turma_id = Turma.id) as eventos",
            );
            $filtro = $this->Session->read("filtros.{$usuario['Usuario']['grupo']}.analise");
            $usuarioId = array($usuario['Usuario']['id']);
            if($this->Session->check("filtros.{$usuario['Usuario']['grupo']}.analise")) {
                $config = $this->Session->read("filtros.{$usuario['Usuario']['grupo']}.analise");
                if(isset($config['Usuario']['id']))
                    $usuarioId[] = $config['Usuario']['id'];
                foreach($config as $modelo => $filtro)
                    foreach($filtro as $chave => $valor)
                        if($valor != '')
                            if($chave == 'id')
                                $options['conditions']["$modelo.$chave"] = $valor;
                            else
                                $options['conditions'][] = "lower($modelo.$chave) like('%".strtolower($valor)."%')";
                $this->data = $config;
            }
            $this->paginate['Turma'] = $options;
            $turmas = $this->paginate('Turma');
            $this->set('turmas', $turmas);
            $this->set('semestres', array('' => 'Todos', 1 => 'Primeiro', 2 => 'Segundo'));
        }
    }
    
    function analise_temporada_excel() {
        $this->Turma->unbindModelAll();
        ini_set("memory_limit", "2048M");
        set_time_limit(0);
        $this->layout = false;
        $this->analise_temporada($excel = 1);
    }
    
    function ficha_evento($tid = false){
        $this->layout = false;
        $usuario = $this->obterUsuarioLogado();
        $this->loadModel('Evento');
        $this->loadModel('TiposEvento');
        $this->set('tiposEventos', $this->TiposEvento->find('list', array('fields' => 'TiposEvento.nome')));
        if (!empty($this->data)){
            Configure::write(array('debug' => 0));
            if(!empty($this->data['Evento']['date_start'])){
                $dateTime = str_replace('/','-',$this->data['Evento']['date_start']);
                $dateTime = $this->create_date_time_from_format('d-m-Y', $dateTime);
                $this->data['Evento']['date_start'] = date_format($dateTime, 'Y-m-d');
            }
            if(!empty($this->data['Evento']['date_end'])){
                $dateTime = str_replace('/','-',$this->data['Evento']['date_end']);
                $dateTime = $this->create_date_time_from_format('d-m-Y', $dateTime);
                $this->data['Evento']['date_end'] = date_format($dateTime, 'Y-m-d');
            }
            $this->Session->write("filtros.{$usuario['Usuario']['grupo']}.turmas", $this->data);
        }else{
            $this->Turma->unbindModelAll();
            $this->Evento->recursive = 2;
            if($tid){
                $this->Session->write("filtros.{$usuario['Usuario']['grupo']}.turmas", array('Turma' => array('id' => $tid)));   
            }
            if($this->Session->check("filtros.{$usuario['Usuario']['grupo']}.turmas")) {
                $config = $this->Session->read("filtros.{$usuario['Usuario']['grupo']}.turmas");
                if($config['Turma']['id'] != '' && empty($config['Evento']['date_start']) && empty($config['Evento']['date_end']) && empty($config['Evento']['tipos_evento_id']))
                    $options['conditions']["Turma.id"] = "{$config['Turma']['id']}";
                if($config['Turma']['id'] != '' && empty($config['Evento']['date_start']) && empty($config['Evento']['date_end']))
                    $options['conditions']["Turma.id"] = "{$config['Turma']['id']}";
                if($config['Turma']['id'] == '' && !empty($config['Evento']['date_start']) && !empty($config['Evento']['date_end']))
                    $options['conditions'] = "Evento.data between '{$config['Evento']['date_start']} 00:00:00' and '{$config['Evento']['date_end']} 23:59:59'";
                if($config['Turma']['id'] != '' && !empty($config['Evento']['date_start']) && !empty($config['Evento']['date_end']))
                    $options['conditions'] = "Turma.id = {$config['Turma']['id']} and Evento.data between '{$config['Evento']['date_start']} 00:00:00' and '{$config['Evento']['date_end']} 23:59:59'";
                if($config['Turma']['id'] != '' && !empty($config['Evento']['date_start']) && !empty($config['Evento']['date_end']) && $config['Evento']['tipos_evento_id'] != '')
                    $options['conditions'] = "Turma.id = {$config['Turma']['id']} and Evento.data between '{$config['Evento']['date_start']} 00:00:00' and '{$config['Evento']['date_end']} 23:59:59' and Evento.tipos_evento_id = {$config['Evento']['tipos_evento_id']}";
                if($config['Turma']['id'] != '' && !empty($config['Evento']['tipos_evento_id']) && empty($config['Evento']['date_start']) && empty($config['Evento']['date_end']))
                    $options['conditions'] = "Turma.id = {$config['Turma']['id']} and Evento.tipos_evento_id = {$config['Evento']['tipos_evento_id']}";
                if($config['Turma']['id'] == '' && empty($config['Evento']['date_start']) && empty($config['Evento']['date_end']) && $config['Evento']['tipos_evento_id'] != '')
                    $options['conditions']["Evento.tipos_evento_id"] = "{$config['Evento']['tipos_evento_id']}";
                if($config['Turma']['id'] == '' && !empty($config['Evento']['date_start']) && !empty($config['Evento']['date_end']) && $config['Evento']['tipos_evento_id'] != '')
                    $options['conditions'] = "Evento.data between '{$config['Evento']['date_start']} 00:00:00' and '{$config['Evento']['date_end']} 23:59:59' and Evento.tipos_evento_id = {$config['Evento']['tipos_evento_id']}";
                if(!empty($config['Evento']['date_start']))
                    $config['Evento']['date_start'] = date("d/m/Y H:i:s", strtotime($config['Evento']['date_start']));
                if(!empty($config['Evento']['date_end']))
                    $config['Evento']['date_end'] = date("d/m/Y H:i:s", strtotime($config['Evento']['date_end']));
                $this->data = $config;
            }
            $options['fields'] = array(
                'Turma.*','Evento.*',
                "(select count(0) from vw_formandos f inner join protocolos p on (p.usuario_id = f.id " .
                    "and p.tipo = 'checkout') left join usuarios u on (u.id= f.id) " .
                    " where Turma.id = turma_id and f.situacao = 'ativo' and u.ativo = 1) as checkouts",
                "(select count(0) from vw_formandos vw inner join usuarios u on u.id = vw.id where "
                . "vw.situacao != 'cancelado' and u.ativo = 1 and vw.turma_id = Turma.id) as aderidos",
                'concat(Turma.ano_formatura,Turma.semestre_formatura) as ano'
            );
            $options['limit'] = 30;
            $options['order'] = Array('Evento.data' => 'asc');
            $this->paginate['Evento'] = $options;
            $turmas = $this->paginate('Evento');
            $this->set('turmas', $turmas);
            $this->render('ficha_evento');
        }
    }
    
    function detalhes($turmaId = false, $eventoId = false){
        $this->layout = false;
        $this->loadModel('Evento');
        $this->loadModel('TurmasUsuarios');
        $this->loadModel('ViewRelatorioExtras');
        $this->loadModel('CheckoutItem');
        $this->loadModel('CheckoutUsuarioItem');
        $turma = $this->Turma->read(null, $turmaId);
        $evento = $this->Evento->read(null, $eventoId);
        App::import('Component', 'RelatorioCheckout');
        $relatorioCheckout = new RelatorioCheckoutComponent();
        $totais = $relatorioCheckout->obterRelatorioCheckout();
        $this->Usuario->unbindModelAll();
        $formandos = $this->ViewFormandos->find('count', array(
            'conditions' => array(
                'ViewFormandos.ativo' => 1,
                'ViewFormandos.situacao not' => 'cancelado',
                'ViewFormandos.turma_id' => $turmaId
            )
        ));
        $comissoes = $this->ViewFormandos->find('count', array(
            'conditions' => array(
                'ViewFormandos.grupo' => 'comissao',
                'ViewFormandos.ativo' => 1,
                'ViewFormandos.situacao not' => 'cancelado',
                'ViewFormandos.turma_id' => $turmaId
            )
        ));
        $valorContratos = $this->ViewFormandos->query("select sum(valor_adesao) as valor_total_contratos "
                . "from vw_formandos where turma_id = {$turma['Turma']['id']}");
        $this->Usuario->bindModel(array('hasOne' => array('TurmasUsuario')), false);
        $atendentes = $this->Usuario->find('all',array(
            'conditions' => array(
                'TurmasUsuario.turma_id' => $turmaId,
                'Usuario.grupo' => array('atendimento', 'planejamento', 'comercial'),
                'Usuario.ativo' => 1
            ),
            'order' => 'Usuario.grupo desc'
        ));
        $h1 = explode(":", $evento['Evento']['duracao']);
     	$h2 = explode(":", date("H:i:s", strtotime($evento['Evento']['data'])));
     	$segundo = $h1[2] + $h2[2];
     	$minuto = $h1[1] + $h2[1];
     	$horas = $h1[0] + $h2[0];
     	$dia = 0;
     	if($segundo > 59){
                $segundodif = $segundo - 60;
                $segundo = $segundodif;
                $minuto = $minuto + 1;
     	}
     	if($minuto > 59){
                $minutodif = $minuto - 60;
                $minuto = $minutodif;
                $horas = $horas + 1;
     	}
     	if($horas > 24){	
                $num = 0;
                (int)$num = $horas / 24;
                $horaAtual = (int)$num * 24;
                $horasDif = $horas - $horaAtual;
                $horas = $horasDif;				
                for($i = 1; $i <= (int)$num; $i++){
                    $dia +=  1 ;
                } 		
    	}
    	if(strlen($horas) == 1){
                $horas = "0".$horas;
    	}
    	if(strlen($minuto) == 1){
                $minuto = "0".$minuto;
    	}
    	if(strlen($segundo) == 1){
                $segundo = "0".$segundo;
     	}
        $evento['Evento']['duracao'] = $horas.":".$minuto.":".$segundo;
        $this->set('totalExtras', $totais[1]);
        $this->set('turma', $turma);
        $this->set('checkout', count($totais[2]));
        $this->set('evento', $evento);
        $this->set('valorContratos', $valorContratos);
        $this->set('atendentes', $atendentes);
        $this->set('formandos', $formandos);
        $this->set('comissoes', $comissoes);
    }
    
    function comercial_listar_comissao_consolidado(){
        $this->layout = false;
        $this->loadModel('ComissaoRelatorioConsolidado');
        $usuarioLogado = $this->obterUsuarioLogado();
        $this->paginate['ComissaoRelatorioConsolidado'] = array(
            'conditions' => array(
               'usuario_id' => $usuarioLogado['Usuario']['id']
            ),
            'limit' => 20,
            'order' => 'turma_id desc',
        );
        $comissoes = $this->paginate('ComissaoRelatorioConsolidado');
        $footer = array();
        $footer['total_contratos'] = 0;
        $footer['total_adesoes'] = 0;
        $footer['total_ativos'] = 0;
        $footer['total_este_mes'] = 0;
        $footer['total_forecast'] = 0;
        $footer['total_recebidos'] = 0;
        $footer['total_valores_recebidos'] = 0;
        $footer['total_valores_proximo_ciclo'] = 0;
        $footer['total_valores_forecast'] = 0;
        $comissoesFooter = $this->ComissaoRelatorioConsolidado->find('all', array(
           'conditions' => array(
               'ComissaoRelatorioConsolidado.usuario_id' => $usuarioLogado['Usuario']['id']
           ) 
        ));
        foreach($comissoesFooter as $comissao){
            $r = (array)json_decode($comissao['ComissaoRelatorioConsolidado']['dados']);
            $footer['total_contratos'] += $r['contratos'];
            $footer['total_adesoes'] += $r['adesoes'];
            $footer['total_ativos'] += $r['ativos'];
            $footer['total_este_mes'] += $r['este_mes'];
            $footer['total_forecast'] += $r['forecast'];
            $footer['total_recebidos'] += $r['recebidos'];
            $footer['total_valores_recebidos'] += $r['valor_recebidos'];
            $footer['total_valores_proximo_ciclo'] += $r['valor_proximo_ciclo'];
            $footer['total_valores_forecast'] += $r['valor_forecast'];
        }
        $this->set('resumo', $comissoes);
        $this->set('footer', $footer);
    }
    
    function comercial_listar_comissao_relatorio($turma = false){
        $this->layout = false;
        $usuarioLogado = $this->obterUsuarioLogado();
        $this->loadModel('ComissaoVendedor');
        $this->loadModel('ReciboComissaoUsuario');
        $formandos = $this->ViewFormandos->find('all', array(
           'conditions' => array(
               'turma_id' => $turma
           )
        ));
        $porcentagem = $this->ComissaoVendedor->find('first', array(
           'conditions' => array(
               'usuario_id' => $usuarioLogado['Usuario']['id'],
               'turma_id' => $turma
           ) 
        ));
        $aPagar = array(); 
        foreach($formandos as $formando){
            if($this->ComissaoVendedor->verificarPaganteEsteMes($formando['ViewFormandos']['id']))
                if(!$this->ReciboComissaoUsuario->verificarComissaoPaga($usuarioLogado['Usuario']['id'], $formando['ViewFormandos']['id']))
                    $aPagar[] = $formando;
        }
        $this->set('usuarioLogado',$usuarioLogado);
        $this->set('aPagar', $aPagar);
        $this->set('porcentagem', $porcentagem);
    }

    function comercial_recibos_comissao_relatorio(){
        $this->layout = false;
        $usuarioLogado = $this->obterUsuarioLogado();
        $this->loadModel('ReciboComissao');
        $this->loadModel('ReciboComissaoUsuario');
        $this->ReciboComissao->recursive = 2;
        $this->Usuario->unbindModelAll();
        $this->ReciboComissao->bindModel(array(
            'hasMany' => array(
                'ReciboComissaoUsuario' => array(
                    'className' => 'ReciboComissaoUsuario',
                    'joinTable' => 'recibo_comissao_usuario',
                    'foreignKey' => 'recibo_comissao_id'
                )
            )
        ),false);
        $recibosFormandos = $this->ReciboComissao->find('all',array(
            'conditions' => array(
                'ReciboComissao.vendedor_id' => $usuarioLogado['Usuario']['id']
            )
        ));
        $this->set('recibosFormandos',$recibosFormandos);
        $this->set('usuarioLogado',$usuarioLogado);
    }

    function comercial_recibos_comissao_relatorio_detalhado($id = false){
        $this->layout = false;
        $this->loadModel('ReciboComissaoUsuario');
        $formandos = $this->ReciboComissaoUsuario->find('all',array(
                'conditions' => array(
                    'ReciboComissaoUsuario.recibo_comissao_id' => $id
                )));
        $this->set('formandos',$formandos);
    }
    
    function financeiro_comissoes(){
        $this->layout = false;
        $this->loadModel('ReciboComissao');
        $this->loadModel('ComissaoVendedor');
        $options['joins'] = array(
                array(
                    "table" => "recibo_comissao",
                    "type" => "inner",
                    "alias" => "ReciboComissao",
                    "conditions" => array(
                        "Usuario.id = ReciboComissao.vendedor_id"
                    )
                )
            );
        $options['conditions'] = array(
                    'MONTH(ReciboComissao.data_cadastro)' => date("m"),
                    'YEAR(ReciboComissao.data_cadastro)' => date("Y"),
                    'ReciboComissao.valor >' => 0,
                    'ReciboComissao.comissao_paga ' => 0
                );
        $options['fields'] = array(
                    'ReciboComissao.*',
                    'Usuario.*'
                );
        $options['order'] = array('Usuario.nome' => 'asc');
        $options['limit'] = 50;
        $this->paginate['Usuario'] = $options;
        $comissoes = $this->paginate('Usuario');
        $usuarios = $this->Usuario->find('all', array(
           'conditions' => array(
                    'Usuario.grupo' => 'comercial',
               ) 
        ));
        $this->set('comissoes', $comissoes);
        $this->set('usuarios', $usuarios);
        $this->render('financeiro_comissoes');
    }
    
    function financeiro_pagamento_comissao($id){
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $this->loadModel('ReciboComissao');
        if($id){
            $this->ReciboComissao->id = $id;
            $this->ReciboComissao->saveField('comissao_paga', 1);
            $this->Session->setFlash('Comissao paga com sucesso.', 'metro/flash/success');
        }else{
            $this->Session->setFlash('Ocorreu um erro ao pagar a comissão.', 'metro/flash/error');
        }
    }
    
    function alterar_convites_recebidos() {
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $turma = $this->obterTurmaLogada();
        $json = array('erro' => true);
        if(isset($this->data['convites']) && $turma) {
            $this->Turma->id = $turma['Turma']['id'];
            if($this->Turma->saveField('convites_recebidos',$this->data['convites'])) {
                $json['erro'] = false;
                $this->setarTurmaLogada($turma['Turma']['id']);
            }
        }
        echo json_encode($json);
    }
    
    function planejamento_relatorio_entrega() {
        $turma = $this->obterTurmaLogada();
        if($this->RequestHandler->isAjax()) {
            $this->layout = false;
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            App::import('Component', 'RelatorioEntrega');
            $relatorioEntrega = new RelatorioEntregaComponent();
            $infos = $relatorioEntrega->obterRelatorioEntrega($turma['Turma']['id']);
            echo json_encode(array(
                'itens' => $infos['relatorio'],
                'convites_recebidos' => $turma['Turma']['convites_recebidos'],
                'campanhas' => $infos['campanhas']
            ));
        } else {
            $this->layout = 'metro/externo';
            $this->set('turma',$turma);
        }
    }
    
    function planejamento_gerar_excel_relatorio_entrega($turmaId){
        $this->_gerar_excel_relatorio_entrega($turmaId);
    }
    
    function producao_gerar_excel_relatorio_entrega($turmaId){
        $this->_gerar_excel_relatorio_entrega($turmaId);
    }

    private function _gerar_excel_relatorio_entrega($turmaId){
        ini_set('memory_limit','1024M');
        set_time_limit(0);
        $this->layout = false;
        App::import('Component', 'RelatorioEntrega');
        $relatorioEntrega = new RelatorioEntregaComponent();
        $infos = $relatorioEntrega->obterRelatorioEntrega($turmaId);
        $this->set('infos',$infos['relatorio']);
    }
    function producao_relatorio_entrega(){
        $this->planejamento_relatorio_entrega();
    }
    
    function super_relatorio_fechamento() {
        $this->Turma->unbindModelAll();
        $options = array(
            'fields' => array(
                'Turma.*',
                "(select count(0) from vw_formandos where Turma.id = turma_id) as formandos",
                "(select count(0) from vw_formandos f inner join protocolos p on (p.usuario_id = f.id " .
                    "and p.tipo = 'checkout') where Turma.id = turma_id) as checkout",
                "(select sum(if(data_adesao is not null,1,0)) from vw_formandos where Turma.id = turma_id) as aderidos",
                'concat(Turma.ano_formatura,Turma.semestre_formatura) as ano',
                "(select group_concat(nome separator '<br />') from usuarios u inner join
                    turmas_usuarios tu on usuario_id = u.id where grupo = 'comercial' and turma_id = Turma.id) as consultores"
            ),
            'limit' => 99999,
            'group' => 'Turma.id',
            'conditions' => array('Turma.status' => array('fechada','concluida'))
        );
        $this->paginate['Turma'] = $options;
        $turmas = $this->paginate('Turma');
        $this->set('turmas',$turmas);
    }
    
    function super_relatorio_fechamento_novo() {
        $this->layout = false;
        $usuario = $this->Session->read('Usuario');
        if (!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->Session->write("filtros.{$usuario['Usuario']['grupo']}.fechamentos", $this->data['Turma']);
        } else {
            $options['order'] = array('Turma.id' => 'desc');
            $options['limit'] = 30;
            $options['group'] = 'Turma.id';
            $options['conditions']['Turma.status'] = array('fechada','concluida');
            $options['fields'] = array(
                'Turma.*',
                "(select count(0) from vw_formandos where Turma.id = turma_id) as formandos",
                "(select count(0) from vw_formandos f inner join protocolos p on (p.usuario_id = f.id " .
                    "and p.tipo = 'checkout') where Turma.id = turma_id) as checkout",
                "(select sum(if(data_adesao is not null,1,0)) from vw_formandos where Turma.id = turma_id) as aderidos",
                'concat(Turma.ano_formatura,Turma.semestre_formatura) as ano',
                "(select group_concat(nome separator '<br />') from usuarios u inner join
                    turmas_usuarios tu on usuario_id = u.id where grupo = 'comercial' and turma_id = Turma.id) as consultores"
            );
            $filtro = $this->Session->read("filtros.{$usuario['Usuario']['grupo']}.fechamentos");
            if($filtro) {
                $this->data['Turma'] = $filtro;
                foreach ($filtro as $chave => $valor)
                    $options['conditions']['lower(Turma.id) LIKE '] = "%".strtolower($valor)."%";
            }
            $this->paginate['Turma'] = $options;
            $turmas = $this->paginate('Turma');
            $this->set('turmas', $turmas);
            $this->render('super_relatorio_fechamento_novo');
        }
    }
    
    function super_relatorio_fechamento_excel() {
        set_time_limit(0);
        ini_set("memory_limit", "1024M");
        $this->layout = false;
        $this->super_relatorio_fechamento();
    }
    
    function super_formandos_igpm($turmaId) {
        $this->Turma->unbindModelAll();
        $turma = $this->Turma->read(null,$turmaId);
        $this->loadModel('ViewFormandos');
        $options = array(
            'conditions' => array(
                "ViewFormandos.turma_id" => $turma['Turma']['id'],
            ),
            'limit' => 99999,
            'joins' => array(
                array(
                    'table' => 'despesas',
                    'alias' => 'Despesa',
                    'type' => 'left',
                    'foreignKey' => false,
                    'conditions' => array(
                        'Despesa.usuario_id = ViewFormandos.id',
                        "Despesa.status <> 'cancelada'"
                    )
                )
            ),
            'fields' => array(
                'ViewFormandos.*',
                'sum(if(Despesa.correcao_igpm is not null,1,0)) as despesas_igpm',
                "sum(if(Despesa.tipo = 'igpm',Despesa.valor,if(Despesa.correcao_igpm is not null and Despesa.status_igpm <> 'recriado',Despesa.correcao_igpm,0))) as soma_igpm"
            ),
            'group' => array('ViewFormandos.id')
        );
        $this->paginate['ViewFormandos'] = $options;
        $formandos = $this->paginate('ViewFormandos');
        $this->set('turma',$turma);
        $this->set('formandos',$formandos);
    }
    
    function planejamento_formandos_igpm($turmaId) {
        $this->super_formandos_igpm($turmaId);
    }
    
    function super_validar_igpm() {
        $this->Turma->unbindModelAll();
        $options = array(
            'conditions' => array(
                "Turma.data_assinatura_contrato <= DATE_SUB(NOW(),INTERVAL 11 MONTH)",
                'Turma.status' => 'fechada'
            ),
            'joins' => array(
                array(
                    'table' => 'vw_formandos',
                    'alias' => 'ViewFormandos',
                    'foreignKey' => false,
                    'conditions' => array(
                        'Turma.id = ViewFormandos.turma_id'
                    )
                ),
                array(
                    'table' => 'despesas',
                    'alias' => 'Despesa',
                    'foreignKey' => false,
                    'conditions' => array(
                        'Despesa.usuario_id = ViewFormandos.id'
                    )
                )
            ),
            'fields' => array(
                'Turma.*',
                'sum(if(Despesa.correcao_igpm is not null,1,0)) as despesas_igpm',
                '(select count(0) from vw_formandos where turma_id = Turma.id) as formandos',
                'concat(Turma.ano_formatura,Turma.semestre_formatura) as ano',
                '(select if(Turma.data_igpm is not null,max(data_aplicacao),0) from igpm_turmas where turma_id = Turma.id) as data_ultima_aplicacao'
            ),
            'group' => array('Turma.id'),
            'limit' => '9999'
        );
        $periodo = array();
        $meses = range(1,12);
        for($a = date('Y') - 3; $a <= date('Y'); $a++)
            foreach($meses as $m)
                $periodo["$a-$m-01"] = "$m/$a";
        if(!empty($this->data['periodo-ini']) && !empty($this->data['periodo-fim'])) {
            $options['conditions'][] = "Turma.data_assinatura_contrato >= '{$this->data['periodo-ini']}'";
            $options['conditions'][] = "Turma.data_assinatura_contrato <= '{$this->data['periodo-fim']}'";
        }
        if(is_numeric($this->data['mes'])) {
            $mes = str_pad($meses[$this->data['mes']], 2, '0', STR_PAD_LEFT);
            $options['conditions'][] = "date_format(Turma.data_assinatura_contrato,'%m') = '{$mes}'";
        }
        $this->set('periodo',$periodo);
        $this->set('meses',$meses);
        $this->paginate['Turma'] = $options;
        $turmas = $this->paginate('Turma');
        $this->set('turmas',$turmas);
    }
    
    function site($paginaId = 'home') {
        $this->layout = 'turma/site';
        $turma = $this->obterTurmaLogada();
        if(file_exists(APP . "webroot/{$turma['Turma']['logo_site']}_opac"))
            $this->set('logoBg',"/{$turma['Turma']['logo_site']}_opac");
        else
            $this->set('logoBg',"/{$turma['Turma']['logo_site']}");
        $this->loadModel('TurmaPagina');
        $this->TurmaPagina->unbindModel(array('belongsTo' => array('Turma')),false);
        $pagina = false;
        $this->loadModel('Parcelamento');
        $this->loadModel('TurmaSiteFoto');
        if($paginaId == 'fotos') {
            
        } elseif($paginaId == 'home') {
            $this->pageTitle = "{$turma['Turma']['nome']}";
            $fotosHome = $this->TurmaSiteFoto->find('all',array(
                'conditions' => array(
                    'TurmaSiteFoto.turma_id' => $turma['Turma']['id'],
                    'tipo' => 'home'
                ),
            ));
            $this->set('fotosHome',$fotosHome);
        } elseif($paginaId) {
            $pagina = $this->TurmaPagina->read(null,$paginaId);
            $this->pageTitle = $pagina['TurmaPagina']['titulo'];
        }
        $this->TurmaSiteFoto->recursive = 0;
        $fotosCabecalho = $this->TurmaSiteFoto->find('all',array(
            'conditions' => array(
                'TurmaSiteFoto.turma_id' => $turma['Turma']['id'],
                'tipo' => 'cabecalho'
            ),
        ));
        $this->set('pagina',$pagina);
        $options['conditions']['TurmaPagina.turma_id'] = $turma['Turma']['id'];
        $paginas = $this->TurmaPagina->find('all',$options);
        $this->set('paginas',$paginas);
        $letras = 4;
        $menu = array(
            'Home' => '/turmas/site/home'
        );
        $planos = $this->Parcelamento->find('count',array(
            'conditions' => array(
                'turma_id' => $turma['Turma']['id'],
                'data >=' => date('Y-m-\0\1')
            )
        ));
        $adesaoLiberada = false;
        if($planos > 0 && $turma['Turma']['status'] == 'fechada' && $turma['Turma']['adesoes'] == 'abertas') {
            $letras+= 15;
            $adesaoLiberada = true;
        }
        $submenu = array();
        foreach($paginas as $index => $pagina) :
            if($letras+strlen($pagina['TurmaPagina']['menu']) <= 48 && count($paginas)-1 == $index)
                $menu[$pagina['TurmaPagina']['menu']] = "/turmas/site/{$pagina['TurmaPagina']['id']}#titulo-pagina";
            elseif($letras+strlen($pagina['TurmaPagina']['menu']) > 40)
                $submenu[$pagina['TurmaPagina']['menu']] = "/turmas/site/{$pagina['TurmaPagina']['id']}#titulo-pagina";
            else {
                $menu[$pagina['TurmaPagina']['menu']] = "/turmas/site/{$pagina['TurmaPagina']['id']}#titulo-pagina";
                $letras+=strlen($pagina['TurmaPagina']['menu']);
            }
        endforeach;
        $this->set('adesaoLiberada',$adesaoLiberada);
        $this->set('menu',$menu);
        $this->set('turma',$turma);
        $this->set('submenu',$submenu);
        $this->set('fotosCabecalho',$fotosCabecalho);
    }
    
    /**
     * @param $slug String Como ficará o endereço do site
     * @param $turmaId Integer ID da Turma
     * @param $turmaNome String Nome da turma, servirá como titulo do hotsite
     *
     */
    function site_turma_criar($slug, $turmaId, $turmaNome)
    {
        $site = $slug;

        $turmas = $this->Turma->find('count',array(
            'conditions' => array(
                'Turma.site' => $site
            )
        ));
                
        if($turmas > 0) {
            $this->Session->setFlash('Este site já está sendo usado',
                    'metro/flash/error');
            return;
        }

        $this->Turma->id = $turmaId;
        
        if($this->Turma->saveField('site',$site)) {
            $this->WpGerenciador->criarSite($site,$turmaId, $turmaNome);
            $r = array('Site Criado Com Sucesso');
            $this->Session->setFlash($r,'metro/flash/success');
            $this->setarTurmaLogada($turmaId);
        } else {
            $this->Session->setFlash("Erro ao salvar endereco",'metro/flash/error'); 
        }
    }

    function site_turma_logo($turma)
    {
        $turmaSite = $turma['Turma']['site'];
        $turmaId   = $turma['Turma']['id'];

        $attachment_id = $this->WpGerenciador->atualizaLogo($turmaSite, $this->data['src']);
        $urlImagem     = $this->WpGerenciador->getImagemUrlDaMidia($turmaSite, $attachment_id);
        try {
            $this->Turma->id = $turmaId;
            if($this->Turma->saveField('logo_site', $urlImagem)) {
                $this->setarTurmaLogada($turmaId);
                $this->Session->setFlash('Logo alterado com sucesso', 'metro/flash/success');
            } else {
                $this->Session->setFlash('Erro ao gravar logo', 'metro/flash/error');
            }
        } catch(Exception $e) {
            unlink($path);
            $this->Session->setFlash('Erro no arquivo. Por favor verifique a imagem', 'metro/flash/error');
        }
    }

    function site_turma_foto_home($turma)
    {
        try{
            $descricao = '';
            $local     = 'cabecalho';
            
            if(isset($this->data['descricao'])){
                $descricao = $this->data['descricao'];
                $data['TurmaSiteFoto']['descricao'] = $this->data['descricao'];
            }
            
            if(isset($this->data['foto_tipo'])) {
                $data['TurmaSiteFoto']['tipo'] = $this->data['foto_tipo'];
                $local = $this->data['foto_tipo'];
            }
            
            $attachment_id = $this->WpGerenciador->adicionaFotoHome($turma['Turma']['site'], $this->data['src'], $local, $descricao);
            $urlImagem     = $this->WpGerenciador->getImagemUrlDaMidia($turma['Turma']['site'], $attachment_id );
            
            $data['TurmaSiteFoto']['arquivo']  = $urlImagem;
            $data['TurmaSiteFoto']['turma_id'] = $turma['Turma']['id'];
            $data['TurmaSiteFoto']['post_id'] = $attachment_id;
            
            $this->loadModel('TurmaSiteFoto');
            if($this->TurmaSiteFoto->save($data)) {
                $this->Session->setFlash('Foto salva com sucesso', 'metro/flash/success');
            } else {
                $this->Session->setFlash('Erro ao salvar foto', 'metro/flash/error');
            }
        } catch(Exception $e) {
            $this->Session->setFlash('Erro no arquivo. Por favor verifique a imagem', 'metro/flash/error');
        }
    }

   function site_turma_acesso($turma)
   {
        $this->Turma->id = $turma['Turma']['id'];
        
        if(!$this->Turma->saveField('site_liberado', $this->data['acesso'])) {
            $this->Session->setFlash('Erro ao alterar acesso ao site', 'metro/flash/error');
            return;
        }

        $this->setarTurmaLogada($turma['Turma']['id']);
        $this->Session->setFlash('O acesso ao site foi alterado', 'metro/flash/success');
   }

    function site_turma_facebook($turma) 
    {
        $this->Turma->id = $turma['Turma']['id'];

        if(!$this->Turma->saveField('facebook', $this->data['facebook'])) {
            $this->Session->setFlash('Erro ao atualizar facebook', 'metro/flash/error');
            return;
        }

        $this->WpGerenciador->atualizaFacebookFanPage($turma['Turma']['site'], $this->data['facebook']);
        $this->setarTurmaLogada($turma['Turma']['id']);
        $this->Session->setFlash('Facebook atualizado com sucesso', 'metro/flash/success');
    }

    function site_turma_ordem_fotos() 
    {
        $this->loadModel('TurmaPaginaFoto');
        $this->TurmaPaginaFoto->saveAll($this->data['ids']);
    }

    function site_turma_listar($turma)
    {
        $this->loadModel('TurmaPagina');
        $this->TurmaPagina->recursive = 0;
        $paginas = $this->TurmaPagina->find('all',array(
            'conditions' => array('TurmaPagina.turma_id' => $turma['Turma']['id'])
        ));
        $this->loadModel('TurmaSiteFoto');
        $this->TurmaSiteFoto->recursive = 0;
        $fotos = $this->TurmaSiteFoto->find('list',array(
            'conditions' => array('TurmaSiteFoto.turma_id' => $turma['Turma']['id']),
            'fields' => array('id','arquivo','tipo')
        ));

        $this->set('fotos',$fotos);
        $this->set('paginas',$paginas);
        $this->set('host',Configure::read('host_site'));
        return;
    }

    function comercial_editar_site() {
        $this->layout = false;
        $turma = $this->obterTurmaLogada();

        if(empty($this->data))
            $this->site_turma_listar($turma);

        $dir_upload       = "upload/turmas/";
        App::import('Component', 'SimpleImage');

        $turmaId   = $turma['Turma']['id'];
        $turmaNome = $turma['Turma']['nome'];

        switch ($this->data['tipo']) {
            case 'criar':
                $site = $this->data['site'];
                $this->site_turma_criar($site, $turmaId, $turmaNome);
                break;   
            case 'logo':
                 $this->site_turma_logo($turma);
                break;
            case 'foto_home':
                   $this->site_turma_foto_home($turma);
                break;
            case 'acesso':
                    if(isset($this->data['acesso']))
                         $this->site_turma_acesso($turma);
                break;
            case 'facebook':
                 $this->site_turma_facebook($turma);
                break;
             case 'ordem_fotos':
                 $this->site_turma_ordem_fotos($turma);
                break;
        }
        $this->set('turma',$turma);
    }
    
    function comercial_paginas() {
        $this->layout = false;
        $turma = $this->obterTurmaLogada();
        if(!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $dir_upload = "upload/turmas/";
            if($this->data['tipo'] == 'logo') {
                $src = base64_decode($this->data['src']);
                $logo = "{$dir_upload}l" . Security::hash(microtime(true) * 1000, 'sha1', true);
                $path = APP . "webroot/$logo";
                if (!empty($turma['Turma']['logo_site']))
                    unlink(APP . "webroot/{$turma['Turma']['logo_site']}");
                if (file_put_contents($path, $src)) {
                    $this->Turma->id = $turma['Turma']['id'];
                    if ($this->Turma->saveField('logo_site', $logo)) {
                        $this->setarTurmaLogada($turma['Turma']['id']);
                        $this->Session->setFlash('Logo alterado com sucesso', 'metro/flash/success');
                    } else {
                        $this->Session->setFlash('Erro ao gravar logo', 'metro/flash/error');
                    }
                } else {
                    $this->Session->setFlash('Erro ao salvar logo', 'metro/flash/error');
                }
            }
            echo json_encode(array());
        } else {
            $this->site_turma_listar($turma);
        }
    }
    
    function comercial_adicionar_pagina($paginaId = false) {
        $this->layout = false;
        $this->loadModel('TurmaPagina');
        if(!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $turma = $this->obterTurmaLogada();
            $turmaPagina = $this->TurmaPagina->find('count',array(
                'conditions' => array(
                    'TurmaPagina.turma_id' => $turma['Turma']['id'],
                    'TurmaPagina.menu' => $this->data['TurmaPagina']['menu'],
                    "TurmaPagina.id <> {$this->data['TurmaPagina']['id']}"
                )
            ));
            if($turmaPagina == 0) {
                $this->data['TurmaPagina']['turma_id'] = $turma['Turma']['id'];
                $this->data['TurmaPagina']['data_cadastro'] = date('Y-m-d H:i:s');
                if($this->TurmaPagina->save($this->data)) {
                    
                    if(empty($this->data['TurmaPagina']['id'])){
                        $id_categoria = $this->WpGerenciador->criarCategoria(
                            $turma['Turma']['site'],
                            $this->data["TurmaPagina"]["menu"],
                            $this->data["TurmaPagina"]["titulo"],
                            $this->TurmaPagina->id
                        );
                        if($id_categoria){
                            $this->TurmaPagina->saveField('term_id', $id_categoria);
                        }
                    }else{
                        $turma_pagina = $this->TurmaPagina->findById($this->data['TurmaPagina']['id']);
                        $retorno = $this->WpGerenciador->atualizaPagina(
                            $turma_pagina['Turma']['site'],
                            $turma_pagina['TurmaPagina']['term_id'],
                            array(
                                'name' => $this->data["TurmaPagina"]["menu"],
                                'description' => $this->data["TurmaPagina"]["titulo"]
                            )
                        );
                    }
                    
                    $this->Session->setFlash('Pagina adicionada com sucesso', 'metro/flash/success');
                } else {
                    $erros = array('Erro ao adicionar pagina');
                    if(!empty($this->TurmaPagina->validationErrors))
                        $erros = array_merge($erros,array_values($this->TurmaPagina->validationErrors));
                    $this->Session->setFlash($erros, 'metro/flash/error');
                }
            } else {
                $this->Session->setFlash('Ja existe uma pagina com esse menu na turma', 'metro/flash/error');
            }
        } elseif($paginaId) {
            $this->data = $this->TurmaPagina->read(null,$paginaId);
        }
    }

    function comercial_remover_pagina() {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $this->loadModel('TurmaPagina');
        $json = array('erro' => 1);
        if(!empty($this->data['id'])) {
            $turma_pagina = $this->TurmaPagina->findById($this->data['id']);

            $this->WpGerenciador->removePagina($turma_pagina['Turma']['site'], $turma_pagina['TurmaPagina']['term_id']);

            if($this->TurmaPagina->delete($this->data['id']))
                $json['erro'] = 0;
            else
                $json['mensagem'] = 'Erro ao apagar página';
        } else {
            $json['mensagem'] = 'Página não encontrada';
        }
        echo json_encode($json);
    }
    
    function comercial_carregar_pagina($paginaId) {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $this->loadModel('TurmaPaginaFoto');
        $this->TurmaPaginaFoto->recursive = 0;
        $fotos = $this->TurmaPaginaFoto->find('all',array(
            'conditions' => array('TurmaPaginaFoto.turma_pagina_id' => $paginaId),
            'order' => array('ordem' => 'asc')
        ));
        echo json_encode(array('fotos' => $fotos));
    }
    
    function comercial_adicionar_foto($paginaId = false) {
        $this->layout = false;
        if(!empty($this->data)) {
            $this->loadModel('TurmaPaginaFoto');
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $turma   = $this->obterTurmaLogada();
            $idMidia = $this->WpGerenciador->adicionaFotoPagina(
                    $turma['Turma']['site'], 
                    $this->data['TurmaPaginaFoto']['src'], 
                    $this->data["TurmaPaginaFoto"]["descricao"], 
                    $this->data["TurmaPaginaFoto"]["turma_pagina_id"]
            );
            $urlImagem = $this->WpGerenciador->getImagemUrlDaMidia($turma['Turma']['site'], $idMidia);
            
            unset($this->data['TurmaPaginaFoto']['src']);
            
            $this->data['TurmaPaginaFoto']['arquivo'] = $urlImagem;
            $this->data['TurmaPaginaFoto']['post_id'] = $idMidia;
            if ($this->TurmaPaginaFoto->save($this->data)) {
                $this->Session->setFlash('Foto inserida com sucesso', 'metro/flash/success');
            } else {
                $this->Session->setFlash('Erro ao gravar foto', 'metro/flash/error');
            }
            
            
        } else {
            $this->data['TurmaPaginaFoto']['turma_pagina_id'] = $paginaId;
        }
    }
    
    function comercial_adicionar_foto_home($tipo = 'cabecalho') {
        $this->layout = false;
        if(!empty($this->data)) {
            $this->loadModel('TurmaSiteFoto');
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $turma = $this->obterTurmaLogada();
            $dir_upload = "upload/turmas/";
            $src = base64_decode($this->data['TurmaSiteFoto']['src']);
            $img = "{$dir_upload}f" . Security::hash(microtime(true) * 1000, 'sha1', true);
            $path = APP . "webroot/$img";
            if(file_put_contents($path, $src)) {
                unset($this->data['TurmaSiteFoto']['src']);
                $this->data['TurmaSiteFoto']['arquivo'] = $img;
                $this->data['TurmaSiteFoto']['turma_id'] = $turma['Turma']['id'];
                $this->data['TurmaSiteFoto']['data_cadastro'] = date('Y-m-d H:i:s');
                if($this->TurmaSiteFoto->save($this->data)) {
                    $this->Session->setFlash('Foto inserida com sucesso', 'metro/flash/success');
                } else {
                    $this->Session->setFlash('Erro ao gravar foto', 'metro/flash/error');
                }
            } else {
                $this->Session->setFlash('Erro ao salvar foto', 'metro/flash/error');
            }
        } else {
            $this->render("comercial_adicionar_foto_$tipo");
        }
    }
    
    function comercial_remover_foto() {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $this->loadModel('TurmaPaginaFoto');
        $json = array('erro' => 1);
        if(!empty($this->data['id'])) {
            $turma_pagina_foto = $this->TurmaPaginaFoto->findById($this->data['id']);

            $this->WpGerenciador->removeFotoHome($turma_pagina_foto['TurmaPagina']['Turma']['site'], $turma_pagina_foto['TurmaPaginaFoto']['post_id']);

            if($this->TurmaPaginaFoto->delete($this->data['id']))
                $json['erro'] = 0;
            else
                $json['mensagem'] = 'Erro ao apagar foto';
        } else {
            $json['mensagem'] = 'Foto não encontrada';
        }
        echo json_encode($json);
    }
    
    function comercial_remover_foto_home() {
        $this->layout = false;
        $this->autoRender = false;
        Configure::write(array('debug' => 0));
        $this->loadModel('TurmaSiteFoto');
        $json = array('erro' => 1);
        if(!empty($this->data['id'])) {
            $turma_site_foto = $this->TurmaSiteFoto->findById($this->data['id']);

            $this->WpGerenciador->removeFotoHome($turma_site_foto['Turma']['site'], $turma_site_foto['TurmaSiteFoto']['post_id']);

            if($this->TurmaSiteFoto->delete($this->data['id']))
                $json['erro'] = 0;
            else
                $json['mensagem'] = 'Erro ao apagar foto';
        } else {
            $json['mensagem'] = 'Foto não encontrada';
        }
        echo json_encode($json);
    }
    
    private function _adicionar() {
        $this->layout = false;
        if(!empty($this->data)) {
            
        } else {
            $anoFormatura = array();
            for($i = date('Y') - 6 ; $i < date('Y') + 6 ; $i++)
                $anoFormatura[$i] = $i;
            $semestreFormatura = array(1=>1,2=>2);
            $this->loadModel('Concorrente');
            $concorrentes = $this->Concorrente->find('list',array('fields' => array('nome')));
            $this->set('concorrentes',$concorrentes);
            $this->set('pretensaos', $this->Turma->pretensao);
            $this->set('como_chegous', $this->Turma->como_chegou);
            $this->set('anoFormatura', $anoFormatura);
            $this->set('semestreFormatura', $semestreFormatura);
            $this->selectDeUniversidadesFaculdadesCursos();
            $this->render('_adicionar');
        }
    }
    
    function comercial_adicionar() {
        $this->_adicionar();
    }
    
    function selecionar($turmaId = false) {
        if(!$this->eFuncionario()) $this->redirect("/");
        $usuario = $this->Session->read('Usuario');
        $this->layout = false;
        $redirecionar = "/turmas/listar";
        if(!$turmaId) {
            $this->Session->setFlash('Erro ao recuperar dados da Turma',
                        'metro/flash/error');
        } elseif(!$this->eFuncionario()) {
            $this->Session->setFlash('Usuário sem permissão de acesso',
                        'metro/flash/error');
        } else {
            if(in_array($usuario['Usuario']['grupo'],array('super', 'producao', 'foto', 'video', 'marketing')))
                $permissao = true;
            elseif($usuario['Usuario']['grupo'] == 'comercial' && $usuario['Usuario']['nivel'] == 'administrador')
                $permissao = true;
            else
                $permissao = false;
            if(!$permissao) {
                $options['joins'] = array(
                    array(
                        'table' => 'turmas_usuarios',
                        'alias' => 'TurmaUsuario',
                        'foreignKey' => false,
                        'conditions' => array(
                            'Turma.id = TurmaUsuario.turma_id',
                            "TurmaUsuario.usuario_id = {$usuario['Usuario']['id']}"
                        )
                    )
                );
            }
            $options['conditions'] = array(
                'Turma.id' => $turmaId
            );
            $this->Turma->recursive = 2;
            $this->Turma->contain(array(
                'CursoTurma.Curso.Faculdade.Universidade',
                'Usuario' => array('conditions' => array('grupo' => array('comercial', 'planejamento', 'atendimento', 'video')))
            ));
            $turma = $this->Turma->find('first',$options);
            if($turma) {
                $this->Session->write('turma', $turma);
                $redirecionar = '';
            } else
                $this->Session->setFlash('Turma não existente ou sem permissão de acesso',
                        'metro/flash/error');
        }
        if(!empty($redirecionar))
            $this->Session->write('redirecionar',$redirecionar);
        $url = Router::url("/{$usuario['Usuario']['grupo']}", true);
        $this->set('url',$url);
        $this->render('selecionar');
    }
    
    function sair() {
        if(!$this->eFuncionario()) $this->redirect("/");
        $usuario = $this->Session->read('Usuario');
        $this->layout = false;
        if($this->turmaEstaLogada())
            $this->Session->delete('turma');
        else
            $this->Session->setFlash('Turma selecionada não encontrada',
                'metro/flash/error');
        $url = Router::url("/{$usuario['Usuario']['grupo']}", true);
        $this->set('url',$url);
        $this->render('selecionar');
    }
    
    private function _editar_dados() {
        $this->layout = false;
        $turma = $this->obterTurmaLogada();
        $usuario = $this->obterUsuarioLogado();
        if(!empty($this->data)) {
            $dataAssinatura = str_replace('/','-',$this->data['Turma']['data-assinatura']);
            $dateTime = $this->create_date_time_from_format('Y-m-d',$dataAssinatura);
            $this->data['Turma']['beneficios_formandos'] = json_encode(array_filter($this->data['Turma']['beneficios_formandos']));
            $this->data['Turma']['beneficios_comissao'] = json_encode(array_filter($this->data['Turma']['beneficios_comissao']));
            $this->data['Turma']['valor_arrecadado_comissao'] = str_replace(',', '', $this->data['Turma']['valor_arrecadado_comissao']);
            $this->data['Turma']['valor_arrecadar_formandos'] = str_replace(',', '', $this->data['Turma']['valor_arrecadar_formandos']);
            $this->data['Turma']['valor_total_contrato'] = str_replace(',', '', $this->data['Turma']['valor_total_contrato']);
            if ($dateTime != null)
                $this->data['Turma']['data_assinatura_contrato'] = date_format($dateTime, 'Y-m-d');
            if($this->data['Turma']['status'] == 'descartada'){
                App::import('Component', 'Mailer');
                $email = new MailerComponent();
                $this->loadModel('Concorrente');
                $concorrente = $this->Concorrente->find('first', array('conditions' => array('id' => $turma['Turma']['concorrente_id'])));
                foreach($turma['Usuario'] as $u){
                    $assunto = "{$turma['Turma']['id']} - {$turma['Turma']['nome']}";
                    $mensagem = "<h4>A Turma {$turma['Turma']['id']} - {$turma['Turma']['nome']} foi descartada pelo usuário {$usuario['Usuario']['nome']}.</h4>";
                    $mensagem.= "<h4>Concorrente : {$concorrente['Concorrente']['nome']}";
                    $email->enviarEmail($u['email'], $assunto, $mensagem);
                }
            }
            if(in_array($this->data['Turma']['tipos_contrato_id'], array(1, 5)))
                $this->data['Turma']['banco_conta_id'] = 6;
            else
                $this->data['Turma']['banco_conta_id'] = 1;
            if($this->Turma->save($this->data['Turma'])) {
                if(isset($this->data['cursos'])) {
                    $cursos = $this->data['cursos'];
                    if(isset($cursos['adicionar'])) {
                        foreach($cursos['adicionar'] as $curso) {
                            $curso['turma_id'] = $turma['Turma']['id'];
                            $cursoTurma = $this->CursoTurma->create();
                            $cursoTurma['CursoTurma'] = $curso;
                            $this->CursoTurma->save($cursoTurma['CursoTurma']);
                        }
                    }
                    if(isset($cursos['remover'])) {
                        foreach($cursos['remover'] as $curso) {
                            $cursoTurmaId = null;
                            if(isset($curso['formandos'])) {
                                $cursoFormandosData = explode(';', $curso['formandos']);
                                $conditions = array(
                                    'CursoTurma.curso_id' => $cursoFormandosData[0],
                                    'CursoTurma.turma_id' => $turma['Turma']['id']
                                );
                                if(!empty($cursoFormandosData[1]))
                                    $conditions['CursoTurma.turno'] = $cursoFormandosData[1];
                                $cursoFormandos = $this->CursoTurma->find('first',array(
                                    'conditions' => $conditions
                                ));
                                if($cursoFormandos)
                                    $cursoTurmaId = $cursoFormandos['CursoTurma']['id'];
                            }
                            if($this->FormandoProfile->updateAll(
                                array('FormandoProfile.curso_turma_id' => $cursoTurmaId),
                                array('FormandoProfile.curso_turma_id' => $curso['id'])))
                                $this->CursoTurma->deleteAll(
                                    array('CursoTurma.id' => $curso['id']));
                        }
                    }
                }
                if(isset($this->data['remover_usuarios']))
                    $this->TurmasUsuario->deleteAll(array(
                        'turma_id' => $turma['Turma']['id'],
                        'usuario_id' => $this->data['remover_usuarios']
                    ),false,false);
                
                if(isset($this->data['Turma']['adicionar_usuarios']))
                    foreach($this->data['Turma']['adicionar_usuarios'] as $usuario_id)
                        $this->Turma->alocarUsuario($usuario_id,$turma['Turma']['id']);
                
                if(isset($this->data['adicionar_comissao'])){
                    $this->loadModel('ComissaoVendedor');
                    foreach($this->data['adicionar_comissao'] as $comissao){
                        $save = $this->ComissaoVendedor->get(array(
                            'usuario_id' => $comissao['usuario_id'],
                            'turma_id' => $turma['Turma']['id']
                        ),array(
                            'usuario_id' => $comissao['usuario_id'],
                            'turma_id' => $turma['Turma']['id'],
                            'porcentagem' => $comissao['porcentagem']
                        ));
                        if($save){
                            $save['ComissaoVendedor']['porcentagem'] = $comissao['porcentagem'];
                            $this->ComissaoVendedor->save($save);
                        }
                    }
                }
                $this->Turma->id = $turma['Turma']['id'];
                $this->setarTurmaLogada($turma['Turma']['id']);
                $this->Session->setFlash('Dados atualizados com sucesso.', 'metro/flash/success');
            } else {
                $erros = array('Erro ao atualizar dados da Turma');
                if(isset($this->Turma->validationErrors['Turma']))
                    array_push ($erros,array_values($this->Turma->validationErrors['Turma']));
                $this->Session->setFlash($erros, 'metro/flash/error');
            }
            echo json_encode(array());
            exit();
        } else {
            $this->data = $this->Turma->read(null,$turma['Turma']['id']);
            if($this->data['Turma']['status'] == 'descartada') {
                $this->Turma->bindModel(array('belongsTo' => array('Concorrente')),false);
                $this->data = $this->Turma->read(null,$turma['Turma']['id']);
            }
        }
        $dateTime = $this->create_date_time_from_format('Y-m-d H:i:s', $this->data['Turma']['data_assinatura_contrato']);
        if ($dateTime != null)
            $this->data['Turma']['data-assinatura'] = date_format($dateTime, 'd/m/Y');
        else
            $this->data['Turma']['data-assinatura'] = '';
        if(!empty($this->data['Turma']['beneficios_formandos']))
            $this->data['Turma']['beneficios_formandos'] = json_decode($this->data['Turma']['beneficios_formandos']);
        else
            $this->data['Turma']['beneficios_formandos'] = array();
        if(!empty($this->data['Turma']['beneficios_comissao']))
            $this->data['Turma']['beneficios_comissao'] = json_decode($this->data['Turma']['beneficios_comissao']);
        else
            $this->data['Turma']['beneficios_comissao'] = array();
        $anoFormatura = array();
        for($i = date('Y') - 6 ; $i < date('Y') + 6 ; $i++)
            $anoFormatura[$i] = $i;
        $semestreFormatura = array(1=>1,2=>2);
        $this->Usuario->unbindModelAll();
        $this->loadModel('Concorrente');
        $concorrentes = $this->Concorrente->find('list',array('fields' => array('nome')));
        $item = $this->Item->get(array(
                'nome' => "Fale Conosco",
                'grupo' => 'atendimento'
        ),array(
            'nome' => "Fale Conosco",
            'grupo' => 'atendimento'
        ));
        $this->loadModel('TiposContrato');
        if(!empty($turma['Turma']['tipos_contrato_id']) && in_array($turma['Turma']['tipos_contrato_id'], array(2, 6))){
            $tipoContrato = $this->TiposContrato->findById($turma['Turma']['tipos_contrato_id']);
            $layout = $tipoContrato['TiposContrato']['field'] . ".ctp";
        }else{
            $layout = NULL;
        }
        $this->set('layout', $layout);
        $this->set('usuario', $usuario);
        $this->set('concorrentes',$concorrentes);
        $this->set('statuses', $this->Turma->status);
        $this->set('adesoes', $this->Turma->adesoes);
        $this->set('tiposContrato', $this->TiposContrato->tipos_contrato); 
        $this->set('como_chegous', $this->Turma->como_chegou);
        $this->set('checkout', $this->Turma->checkout);
        $this->set('cascata', $this->Turma->cascata);
        $this->set('anoFormatura', $anoFormatura);
        $this->set('semestreFormatura', $semestreFormatura);
        $this->set('turma', $turma);
        $this->selectDeUniversidadesFaculdadesCursos();
        $this->selectUsuarios();
        $usuariosComercial = $this->Usuario->find('all',array(
            'conditions' => array(
                'TurmaUsuarios.turma_id' => $turma['Turma']['id'],
                'Usuario.grupo' => 'comercial'
            ),
            'joins' => array(
                array(
                    "table" => "turmas_usuarios",
                    "type" => "inner",
                    "alias" => "TurmaUsuarios",
                    "conditions" => array(
                        "Usuario.id = TurmaUsuarios.usuario_id",
                    )
                ),
                array(
                    "table" => "comissao_vendedor",
                    "type" => "left",
                    "alias" => "ComissaoVendedor",
                    "conditions" => array(
                        "Usuario.id = ComissaoVendedor.usuario_id",
                        "TurmaUsuarios.turma_id = ComissaoVendedor.turma_id"
                    )
                )
            ),
            'fields' => array('Usuario.*','ComissaoVendedor.*')
        ));
        $this->set('usuariosComercial', $usuariosComercial);
    }
    
    function obterLayoutContrato($tipoId = false){
        $this->layout = 'ajax';
        $this->autoRender = false;
        $turma = $this->obterTurmaLogada();
        switch ($tipoId) {
            case 2:
                $this->data = $turma;  
                $this->render('campos_arrecadacao_gestao');
                break;
            case 6:
                $this->data = $turma;  
                $this->render('campos_arrecadacao_transferencia');
                break;
        }
    }
    
    function comercial_editar_dados() {
        $this->_editar_dados();
    }
    
    function planejamento_editar_dados() {
        $this->_editar_dados();
    }
    
    function super_editar_dados() {
        $this->_editar_dados();
    }
    
    private function _info() {
        $this->layout = false;
        $turma = $this->obterTurmaLogada();
        $this->Usuario->bindModel(array('hasOne' => array('TurmasUsuario')));
        $turma['Turma']['pessoas_comissao'] = $this->Usuario->find('count',array(
            'conditions' => array(
                'Usuario.grupo' => 'comissao',
                'TurmasUsuario.turma_id' => $turma['Turma']['id'])
        ));
        $this->set('turma', $turma);
        $usuario = $this->Session->read('Usuario');
        $permissao = in_array($usuario['Usuario']['grupo'],array('super','planejamento','comercial'));
        $this->set('permissao',$permissao);
    }
    
    function super_info() {
        $this->_info();
    }
    
    function planejamento_info() {
        $this->_info();
    }
    
    function formando_info() {
        $this->_info();
    }
    
    function comercial_info() {
        $this->_info();
    }
    
    function comissao_info() {
        $this->_info();
    }
    
    function video_info() {
        $this->_info();
    }
    
    function listar() {
        if(!$this->eFuncionario()) $this->redirect("/");
        $usuario = $this->Session->read('Usuario');
        $this->layout = false;
        if(!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->Session->write("filtros.{$usuario['Usuario']['grupo']}.turmas",
                    $this->data);
        } else {
            $options = array(
                'limit' => 30,
                'order' => array(
                    'Turma.id' => 'desc'
                ),
                'group' => array('Turma.id')
            );
            $options['joins'] = array(
                array(
                    'table' => 'turmas_usuarios',
                    'alias' => 'TurmaUsuario',
                    'foreignKey' => false,
                    'conditions' => array(
                        'Turma.id = TurmaUsuario.turma_id'
                    )
                ),
                array(
                    'table' => 'usuarios',
                    'alias' => 'Usuario',
                    'foreignKey' => false,
                    'conditions' => array(
                        'TurmaUsuario.usuario_id = Usuario.id',
                        'Usuario.grupo' => $usuario['Usuario']['grupo']
                    )
                )
            );
            $usuarioId = array($usuario['Usuario']['id']);
            if($this->Session->check("filtros.{$usuario['Usuario']['grupo']}.turmas")) {
                $config = $this->Session->read("filtros.{$usuario['Usuario']['grupo']}.turmas");
                if(isset($config['Usuario']['id']))
                    $usuarioId[] = $config['Usuario']['id'];
                foreach($config as $modelo => $filtro)
                    foreach($filtro as $chave => $valor)
                        if($valor != '')
                            if($chave == 'id')
                                $options['conditions']["$modelo.$chave"] = $valor;
                            else
                                $options['conditions'][] = "lower($modelo.$chave) like('%".strtolower($valor)."%')";
                $this->data = $config;
            }
            if($usuario['Usuario']['grupo'] == 'super')
                $permissao = true;
            elseif($usuario['Usuario']['grupo'] == 'comercial' && $usuario['Usuario']['nivel'] == 'administrador')
                $permissao = true;
            else
                $permissao = false;

            $options['conditions']['TurmaUsuario.usuario_id'] = $usuarioId;
            $this->set('statusExistentesParaTurmas',
                    array_merge(array('' => 'Todas'), $this->Turma->status));

            $excecoes = array('planejamento','video');
            if( in_array($usuario['Usuario']['grupo'],$excecoes) ){
                $this->Turma->bindModel(array('hasAndBelongsToMany' => array('Usuario')),false);
            }

            $this->Turma->recursive = 1;

            if($usuario['Usuario']['grupo'] == 'comercial')
                $options['fields'] = array(
                    'Turma.*',
                    '(select count(0) from registros_contatos where turma_id = Turma.id and tipo = \'telefone\') AS contatos',
                    '(select count(0) from registros_contatos where turma_id = Turma.id and tipo = \'reuniao\') AS reunioes',
                    '(select concat(data_previsao) from turmas_previsao_fechamento where turma_id = Turma.id order by id desc limit 1) AS data_previsao',
                    '(select lower(data) from registros_contatos where turma_id = Turma.id and tipo = \'telefone\' order by id desc limit 1) AS data_ultimo_contato',
                    'concat(Turma.ano_formatura,Turma.semestre_formatura) as ano'
                );
            else
                $options['fields'] = array(
                    'Turma.*',
                    '(select count(0) from vw_formandos where turma_id = Turma.id and grupo = \'comissao\' and ativo = 1) AS comissoes',
                    '(select count(0) from vw_formandos where turma_id = Turma.id and grupo = \'formando\' and ativo = 1) AS formandos',
                    'concat(Turma.ano_formatura,Turma.semestre_formatura) as ano'
                );
            $this->paginate['Turma'] = $options;
            $turmas = $this->paginate('Turma');
            $consultores = $this->Usuario->find('list',array(
                'conditions' => array('Usuario.grupo' => 'comercial'),
                'fields' => array('Usuario.nome'),
                'order' => array('Usuario.nome' => 'asc')
            ));

            if ($usuario['Usuario']['grupo'] == 'planejamento') {
                
                $this->Usuario->contain('Turma');
                $idsTurmasRelacionadasAoUsuario = $this->TurmasUsuario->find('list', array('conditions' => array('usuario_id' => $usuario['Usuario']['id']), 'fields' => array('turma_id')));

                $condicoesDaPesquisa = array('id' => $idsTurmasRelacionadasAoUsuario);

                $this->Turma->recursive = -1;
                $this->Turma->contain(array(
                    'CursoTurma.Curso.Faculdade.Universidade'));

                $turmasDoUsuario = $this->Turma->find('all', array('conditions' => array('Turma.id' => $idsTurmasRelacionadasAoUsuario, 'Turma.status' => 'fechada'), 'fields' => array('id')));
                $arrayDeIdsDeTurmasFechadasDoUsuario = array();

                foreach ($turmasDoUsuario as $turmaDoUsusario) {
                    $arrayDeIdsDeTurmasFechadasDoUsuario[] = $turmaDoUsusario['Turma']['id'];
                }

                $arrayDeTurmasSemParcelamaker = array();

                foreach ($arrayDeIdsDeTurmasFechadasDoUsuario as $idDaTurma) {
                    $numeroDeRegistrosDoMes = $this->Parcelamento->find('count', array('conditions' => array('Parcelamento.turma_id' => $idDaTurma, 'MONTH(Parcelamento.data)' => date('m', strtotime('now')), 'YEAR(Parcelamento.data)' => date('Y', strtotime('now')))));
                    if ($numeroDeRegistrosDoMes == 0)
                        $arrayDeTurmasSemParcelamaker[] = $idDaTurma;
                }

                sort($arrayDeTurmasSemParcelamaker);

                $mensagemDeErroParcelamaker = "As seguintes turmas n&atilde;o possuem parcelamaker: " . implode(", ", $arrayDeTurmasSemParcelamaker) . ".";
                
                if($usuario['Usuario']['grupo'] == 'planejamento'){
                if(count($arrayDeTurmasSemParcelamaker) > 0)
                    $this->Session->setFlash($mensagemDeErroParcelamaker, 'metro/flash/alert');
                }
            }
            $this->set('consultores',$consultores);
            $this->set('usuario',$usuario);
            $this->set('turmas', $turmas);
            $this->set('permissao',$permissao);
            $this->render("{$usuario['Usuario']['grupo']}_listar");
        }
    }
    
    private function _criar_site() {
        $this->layout = false;
        if(!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $turma = $this->Turma->find('count',array(
                'conditions' => array(
                    'Turma.site' => $this->data['Turma']['site']
                )
            ));
            if($turma > 0) {
                $this->Session->setFlash('Este site já está sendo usado',
                        'metro/flash/error');
            } else {
                $turma = $this->obterTurmaLogada();
                $turmaId = $turma['Turma']['id'];
                $site = $this->data['Turma']['site'];
                $this->SiteTurma->CpanelApi = $this->CpanelApi;
                $result = $this->SiteTurma->criarSite($turmaId,$site);
                if($result['erro'] === false) {
                    $turma['Turma']['site'] = $site;
                    $this->Turma->save($turma);
                    $r = array(
                        'Site Criado Com Sucesso',
                        "<a href='http://{$result['site']}'>Clique Aqui</a> Para Acessar"
                    );
                    $this->Session->setFlash($r,'metro/flash/success');
                } else {
                    $this->Session->setFlash($result['mensagem'],'metro/flash/error');
                }
            }
        }
    }
    
    public function comercial_criar_site() {
        $this->_criar_site();
    }
    
    function verificar_disponibilidade_site($site = false) {
        $this->autoRender = false;
        $this->layout = false;
        Configure::write(array('debug' => 0));
        $disponivel = $this->WpGerenciador->verificaDisponibilidade($site);
        echo json_encode(array('turmas' => $disponivel ? 1 :0));
    }

    function comercial_visualizar($id = null) {
        $usuario = $this->Session->read('Usuario');
        $this->Turma->recursive = 2;
        $this->Turma->contain(array(
            'CursoTurma.Curso.Faculdade.Universidade',
            'Usuario' => array('conditions' => array('grupo' => array('comercial', 'planejamento', 'atendimento')))
        ));


        if (isset($id)) {
            $this->Turma->Curso->recursive = 2;
            $turma = $this->Turma->findById($id);
            if ($usuario['Usuario']['grupo'] != 'super')
                $this->Session->write('turma', $turma);
        } else {
            $this->_redirecionaProcuraTurma();
            $turma = $this->Session->read('turma');
        }


        if ($usuario['Usuario']['grupo'] != 'super') {
            $autorizado = false;
            foreach ($turma['Usuario'] as $usuario_autorizado) {
                if ($usuario['Usuario']['id'] == $usuario_autorizado['id']) {
                    $autorizado = true;
                    break;
                }
            }

            if (!$autorizado) {
                $this->Session->setFlash('Turma não existente ou sem permissão de acesso', 'flash_erro');
                $this->Session->delete('turma');
                $this->redirect("/{$this->params['prefix']}/turmas");
            }
        }

        // Calcular o número de pessoas na comissão
        $this->Usuario->bindModel(array('hasOne' => array('TurmasUsuario')));
        $turma['Turma']['pessoas_comissao'] = $this->Usuario->find('count', array('conditions' => array('grupo' => 'comissao', 'turma_id' => $turma['Turma']['id'])));

        $this->set('pretensaos', $this->Turma->pretensao);
        $this->set('turma', $turma);
    }

    function comercial_index() {
        ini_set('memory_limit', '128M');

        $session_turma = $this->Session->read('turma');
        if (isset($session_turma)) {
            //$this->redirect("/{$this->params['prefix']}/turmas/visualizar");
            $this->super_index();
        } else {
            $this->set('hasSidebarMenuItem', FALSE);
            $this->super_index();
        }
    }

    function atendimento_index() {
        $this->comercial_index();
    }

    function comercial_editar() {
        $this->_redirecionaProcuraTurma();
        $session_turma = $this->Session->read('turma');

        if (isset($this->data)) {
            $dados = $this->data;
            $dateTime = $this->create_date_time_from_format('Y-m-d', $dados['Turma']['data-assinatura']);
            if ($dateTime != null)
                $dados['Turma']['data_assinatura_contrato'] = date_format($dateTime, 'Y-m-d');
            $this->data = $dados;
        }

        $this->super_editar($session_turma['Turma']['id']);

        $dateTime = $this->create_date_time_from_format('Y-m-d H:i:s', $this->data['Turma']['data_assinatura_contrato']);
        if ($dateTime != null)
            $this->data['Turma']['data-assinatura'] = date_format($dateTime, 'd-m-Y');
        else
            $this->data['Turma']['data-assinatura'] = '';
    }

    function comercial_agenda() {
        $this->_redirecionaProcuraTurma();
        $session_turma = $this->Session->read('turma');

        $sql = " SELECT Agenda.*";
        $sql .= " FROM agendas as Agenda";
        $sql .= " LEFT JOIN turmas ON turmas.id = Agenda.turmas_id";
        $sql .= " LEFT JOIN turmas_usuarios ON turmas.id = turmas_usuarios.turma_id";
        $sql .= " LEFT JOIN usuarios ON usuarios.id = turmas_usuarios.usuario_id";
        $sql .= " WHERE turmas.id = {$session_turma['Turma']['id']}";
        $sql .= " AND Agenda.data >= '" . date("Y-m-d H:i:00") . "'";
        $turmaShare = $this->Turma->query($sql);

        $this->loadModel('Agenda');
        $listTurmaEvents = array();
        foreach ($turmaShare as $evento) {
            $listTurmaEvents[] = $this->Agenda->findById($evento['Agenda']['id']);
        }

        $data = array();
        foreach ($listTurmaEvents as $event) {
            $dateTime = $this->create_date_time_from_format('Y-m-d H:i:s', $event['Agenda']['data']);
            $date = date_format($dateTime, 'Ymd');

            //verifica um evento duplicado
            $isDuplicated = false;
            if (isset($data[intval($date)])) {
                foreach ($data[intval($date)] as $e) {
                    if ($e['Agenda']['id'] == $event['Agenda']['id']) {
                        $isDuplicated = true;
                        break;
                    }
                }
            }
            if (!$isDuplicated) {
                $data[intval($date)][] = $event;
            }
        }
        ksort($data);
        $this->set('data', $data);
    }

    function comercial_evento($id = null) {
        $this->_redirecionaProcuraTurma();
        $this->loadModel('Agenda');
        $agenda = $this->Agenda->findById($id);

        if (!$agenda) {
            $this->Session->setFlash(__('Agenda inexistente', true), 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/{$this->params['controller']}/agenda");
        }
        $this->set('evento', $agenda);
    }

    function comercial_fechar() {
        $this->_redirecionaProcuraTurma();

        $session_turma = $this->Session->read('turma');
        $this->Turma->id = $session_turma['Turma']['id'];
        $this->data = $this->Turma->read();

        if (!$this->data) {
            $this->Session->setFlash('Turma não existente', 'flash_erro');
            $this->redirect("/{$this->params['prefix']}/turmas");
        }

        if ($this->Turma->saveField('status', 'fechada', $validate = false))
            $this->Session->setFlash('Status da turma atualizado para fechada!', 'flash_sucesso');

        unset($this->data);
        unset($session_turma);
        $this->redirect("/{$this->params['prefix']}/turmas/visualizar");
    }

    function comercial_editar_registro_interno() {
        $session_turma = $this->Session->read('turma');
        $id = $session_turma['Turma']['id'];

        $this->Turma->id = $id;
        if (!empty($this->data)) {
            if ($this->Turma->save($this->data['Turma'])) {
                $this->Session->setFlash('Turma foi Salva corretamente', 'flash_sucesso');
                $this->redirect("/{$this->params['prefix']}/registros_internos");
            }
        } else {
            $this->data = $this->Turma->read();

            if (empty($this->data)) {
                $this->redirect("/{$this->params['prefix']}/turmas/visualizar");
                $this->Session->setFlash('Registro de contato não existente.');
            }

            if (!empty($this->data['Turma']['data_assinatura_contrato'])) {
                $dateTime = $this->create_date_time_from_format('Y-m-d H:i:s', $this->data['Turma']['data_assinatura_contrato']);
                $this->data['Turma']['data-hora'] = date_format($dateTime, 'd-m-Y H:i');
            }
        }

        $this->set('semestre_formaturas', $this->Turma->semestre_formatura);

        $this->set('expectativa_fechamentos', $this->Turma->expectativa_fechamento);
        $this->set('como_chegous', $this->Turma->como_chegou);
        $this->set('statuses', $this->Turma->status);
        $this->set('pretensaos', $this->Turma->pretensao);

        if (!$this->data)
            $this->Session->setFlash('Turma não existente', 'flash_erro');
    }

    function comercial_procurar() {
        $this->super_procurar();
    }

    /*     * *** PLANEJAMENTO ******* */

    function planejamento_visualizar($id = null) {
        if (isset($id)) {

//          $this->Turma->bindModel( 
//              array('hasAndBelongsToMany' => array(
//                  'Usuario' => array( 
//                      'conditions' => array('NOT' => array('Usuario.grupo' => 'comissao'))
//          ))));

            $this->Turma->bindModel(array('hasAndBelongsToMany' => array('Curso')));

            $turma = $this->Turma->findById($id);
            if (strcmp($turma['Turma']['status'], 'aberta') == 0) {
                unset($id);
                $this->Session->setFlash('Acesso negado. Turma não está fechada. ', 'flash_erro');
                $this->redirect("/{$this->params['prefix']}/turmas");
            }
        } else if (isset($session_turma)) {
            if (strcmp($session_turma['Turma']['status'], 'aberta') == 0) {
                $this->Session->delete('turma');
            }
        }
        $this->comercial_visualizar($id);
    }

    function planejamento_index() {
        $this->comercial_index();
    }

    function planejamento_editar() {

        if (isset($this->data)) {
            $dados = $this->data;
            $dateTime = $this->create_date_time_from_format('d-m-Y', $dados['Turma']['data-igpm']);
            $dados['Turma']['data_igpm'] = date_format($dateTime, 'Y-m-d');
            $this->data = $dados;
        }

        $this->comercial_editar();


        if ($this->data['Turma']['data_igpm'] != null) {
            $dateTime = $this->create_date_time_from_format('Y-m-d', $this->data['Turma']['data_igpm']);
            $this->data['Turma']['data-igpm'] = date_format($dateTime, 'd-m-Y');
        } else {
            unset($this->data['Turma']['data-igpm']);
        }
    }

    function planejamento_agenda() {
        $this->comercial_agenda();
    }

    function planejamento_evento($id = null) {
        $this->comercial_evento($id);
    }

    function planejamento_procurar() {
        $this->super_procurar();
    }

    /* SUPER */
    function super_index() {
        $usuario = $this->Session->read('Usuario');


        if ($usuario['Usuario']['grupo'] != 'super') {
            // Selecionar id das turmas que o usuário está vinculado
            $this->Usuario->contain('Turma');
            $idsTurmasRelacionadasAoUsuario = $this->TurmasUsuario->find('list', array('conditions' => array('usuario_id' => $usuario['Usuario']['id']), 'fields' => array('turma_id')));

            $condicoesDaPesquisa = array('id' => $idsTurmasRelacionadasAoUsuario);
        } else {
            $condicoesDaPesquisa = array();
        }

        if ($usuario['Usuario']['grupo'] == 'planejamento') {
            $this->Turma->recursive = -1;
            $this->Turma->contain(array(
                'CursoTurma.Curso.Faculdade.Universidade'));

            $turmasDoUsuario = $this->Turma->find('all', array('conditions' => array('Turma.id' => $idsTurmasRelacionadasAoUsuario, 'Turma.status' => 'fechada'), 'fields' => array('id')));
            $arrayDeIdsDeTurmasFechadasDoUsuario = array();

            foreach ($turmasDoUsuario as $turmaDoUsusario) {
                $arrayDeIdsDeTurmasFechadasDoUsuario[] = $turmaDoUsusario['Turma']['id'];
            }

            $arrayDeTurmasSemParcelamaker = array();

            foreach ($arrayDeIdsDeTurmasFechadasDoUsuario as $idDaTurma) {
                $numeroDeRegistrosDoMes = $this->Parcelamento->find('count', array('conditions' => array('Parcelamento.turma_id' => $idDaTurma, 'MONTH(Parcelamento.data)' => date('m', strtotime('now')), 'YEAR(Parcelamento.data)' => date('Y', strtotime('now')))));
                if ($numeroDeRegistrosDoMes == 0)
                    $arrayDeTurmasSemParcelamaker[] = $idDaTurma;
            }

            sort($arrayDeTurmasSemParcelamaker);

            $mensagemDeErroParcelamaker = "ATEN&Ccedil;&Atilde;O: As seguintes turmas n&atilde;o possuem parcelamaker: " . implode(", ", $arrayDeTurmasSemParcelamaker) . ".";

            if (count($arrayDeTurmasSemParcelamaker) > 0)
                $this->Session->setFlash($mensagemDeErroParcelamaker, 'flash_erro');
        }

        if (isset($this->data['filtro-id'])) {

            $condicoesDaPesquisa['Turma.id'] = explode(" ", $this->data['filtro-id']);
            if ($condicoesDaPesquisa['Turma.id'][0] == "")
                unset($condicoesDaPesquisa['Turma.id']);

            $condicoesDaPesquisa['Turma.status'] = $this->data['filtro-status'];

            if ($condicoesDaPesquisa['Turma.status'] == '') {
                unset($condicoesDaPesquisa['Turma.status']);
                $this->Session->delete('condicaoBuscaSuper');
            } else {
                $this->Session->write('condicaoBuscaSuper', $condicoesDaPesquisa['Turma.status']);
                $this->set('filtroStatusSelecionado', $condicoesDaPesquisa['Turma.status']);
            }
        } else {

            if (isset($this->params['named']['page']))
                $pagina = $this->params['named']['page'];
            else
                $pagina = null;

            if (!$this->Session->check('condicaoBuscaSuper') || $pagina == null) {
                unset($condicoesDaPesquisa['Turma.status']);
                $this->Session->delete('condicaoBuscaSuper');
            } else {
                $condicoesDaPesquisa['Turma.status'] = $this->Session->read('condicaoBuscaSuper');
                $this->set('filtroStatusSelecionado', $condicoesDaPesquisa['Turma.status']);
            }
        }



        $this->set('statusExistentesParaTurmas', array_merge(array('' => 'Todas'), $this->Turma->status));

        $this->Turma->recursive = -1;
        if ($usuario['Usuario']['grupo'] != 'super') {
            $this->set('turmas', $this->paginate('Turma', $condicoesDaPesquisa));
        }
        else
            $this->set('turmas', $this->paginate('Turma', $condicoesDaPesquisa));
    }
    
    function super_listar(){
        $this->layout = false;
        $usuario = $this->obterUsuarioLogado();
        if (!empty($this->data)) {
            $this->autoRender = false;
            Configure::write(array('debug' => 0));
            $this->Session->write("filtros.{$usuario['Usuario']['grupo']}.turmas", $this->data);
        } else {
            $options['order'] = array('Turma.id' => 'desc');
            $usuarioId = array($usuario['Usuario']['id']);
            if($this->Session->check("filtros.{$usuario['Usuario']['grupo']}.turmas")) {
                $config = $this->Session->read("filtros.{$usuario['Usuario']['grupo']}.turmas");
                if(isset($config['Usuario']['id']))
                    $usuarioId[] = $config['Usuario']['id'];
                foreach($config as $modelo => $filtro)
                    foreach($filtro as $chave => $valor)
                        if($valor != '')
                            if($chave == 'id')
                                $options['conditions']["$modelo.$chave"] = $valor;
                            else
                                $options['conditions'][] = "lower($modelo.$chave) like('%".strtolower($valor)."%')";
                $this->data = $config;
            }
            $options['fields'] = array(
                'Turma.*',
                '(select count(0) from vw_formandos where turma_id = Turma.id and grupo = \'comissao\' and ativo = 1) AS comissoes',
                '(select count(0) from vw_formandos where turma_id = Turma.id and grupo = \'formando\' and ativo = 1) AS formandos',
                'concat(Turma.ano_formatura,Turma.semestre_formatura) as ano'
            );
            $options['limit'] = 30;
            $this->paginate['Turma'] = $options;
            $turmas = $this->paginate('Turma');
            $this->set('turmas', $turmas);
            $this->render("{$usuario['Usuario']['grupo']}_listar");
        }
        $this->set('usuario', $usuario);
    }
    
    function video_listar(){
        $this->super_listar();
    }
    
    function marketing_listar(){
        $this->super_listar();
    }

    function super_inserir(){
        $this->layout = false;
        if (!empty($this->data)) {
            $this->autoRender = false;
          //  Configure::write(array('debug' => 0));
            $this->loadModel('BancoConta');
            $bancoConta = $this->BancoConta->find('first',array(
                'conditions' => array(
                    'padrao' => 1
                )
            ));
            if($bancoConta) {
                $this->data['Turma']['data_abertura_turma'] = date("Y-m-d");
                $this->data['Turma']['banco_conta_id'] = $bancoConta['BancoConta']['id'];
            } else {
                $this->Session->setFlash('Erro ao selecionar conta bancaria para a turma. Entre em contato com o administrador', 'metro/flash/error');
            }
            if(!empty($this->data['Turma']['data_assinatura'])){
                $dateTime = str_replace('/','-',$this->data['Turma']['data_assinatura']);
                $dateTime = $this->create_date_time_from_format('d-m-Y', $dateTime);
                $this->data['Turma']['data_assinatura_contrato'] = date_format($dateTime, 'Y-m-d');
            }else{
                unset($this->data['Turma']['data_assinatura']);
            }
            if ($this->Turma->save($this->data['Turma'])) 
                $this->Session->setFlash('Turma salva com sucesso.', 'metro/flash/success');
            else
                $this->Session->setFlash('Ocorreu um erro ao salvar a turma.', 'metro/flash/error');
        }
        $this->set('semestre_formaturas', $this->Turma->semestre_formatura);
        $this->set('expectativa_fechamentos', $this->Turma->expectativa_fechamento);
        $this->set('como_chegous', $this->Turma->como_chegou);
        $this->set('statuses', $this->Turma->status);
        $this->set('pretensaos', $this->Turma->pretensao);
    }

    function super_procurar() {
        $chave = empty($this->data['Turmas']['chave']) ? "" : $this->data['Turmas']['chave'];
        $chaves = preg_split("/[, -.]/", $this->data['Turmas']['chave']);
        if (isset($this->data['Turmas']['status']))
            $status = $this->data['Turmas']['status'];
        else
            $status = '';

        $chaves = array_unique($chaves);
        unset($chaves[array_search('', $chaves)]);

        function construirQuery($c) {
            return array('Turma.id LIKE' => '%' . $c . '%');
        }

        $chaves = array_map('construirQuery', $chaves);
        $conditions = empty($chaves) ? array() : array('AND' => array('OR' => $chaves, 'Turma.status LIKE' => '%' . $status . '%'));
        $usuario = $this->Session->read('Usuario');
        if ($usuario['Usuario']['grupo'] != 'super') {
            $this->Turma->bindModel(array('hasOne' => array('TurmasUsuario')), false);
            $this->set('turmas', $this->paginate('Turma', array('TurmasUsuario.usuario_id' => $usuario['Usuario']['id'],
                        $conditions)));
        }
        else
            $this->set('turmas', $this->paginate('Turma', $conditions));

        $this->set('statuses', array_merge(array('' => 'Todas'), $this->Turma->status));
        $this->set('chave', $chave);

        $this->render($this->params['prefix'] . '_index');
    }

    function super_adicionar() {
        if (!empty($this->data)) {
            $this->loadModel('BancoConta');
            $bancoConta = $this->BancoConta->find('first',array(
                'conditions' => array(
                    'padrao' => 1
                )
            ));
            if($bancoConta) {
                $this->data['Turma']['data_abertura_turma'] = date("Y-m-d");
                $this->data['Turma']['banco_conta_id'] = $bancoConta['BancoConta']['id'];
                if ($this->Turma->save($this->data['Turma'])) {
                    if (!$this->adicionarRemoverCursoTurma($this->Turma->id)) {
                        if ($this->adicionarRemoverUsuario($this->Turma->id)) {
                            $this->Session->setFlash('Turma foi Salva corretamente mas alguns cursos e ' .
                                    'usuarios não forma inseridos ou deletados corretamento, por favor, resolva' .
                                    ' as pendências', 'flash_erro');
                            $this->redirect("/{$this->params['prefix']}/turmas/editar/{$this->Turma->id}");
                        } else {
                            $this->Session->setFlash('Turma foi Salva corretamente mas alguns cursos não foram ' .
                                    'inseridos ou deletados, por favor, resolva as pendências', 'flash_erro');
                            $this->redirect("/{$this->params['prefix']}/turmas/editar/{$this->Turma->id}");
                        }
                    } else {
                        if ($this->adicionarRemoverUsuario($this->Turma->id)) {
                            $this->Session->setFlash('Turma foi Salva corretamente', 'flash_sucesso');
                            $this->redirect("/{$this->params['prefix']}/turmas/visualizar/{$this->Turma->id}");
                        } else {
                            $this->Session->setFlash('Turma foi Salva corretamente mas alguns usuarios não foram ' .
                                    'inseridos ou deletados, por favor, resolva as pendências', 'flash_erro');
                            $this->redirect("/{$this->params['prefix']}/turmas/editar/{$this->Turma->id}");
                        }
                    }
                }
                else
                    $this->Session->setFlash('Ocorreu um erro na inserção de dados', 'flash_erro');
            } else {
                $this->Session->setFlash('Erro ao selecionar conta bancaria para a turma. Entre em contato com o administrador', 'flash_erro');
            }
        }

        $this->selectDeUniversidadesFaculdadesCursos();
        $this->selectUsuarios();

        $this->set('semestre_formaturas', $this->Turma->semestre_formatura);
        $this->set('expectativa_fechamentos', $this->Turma->expectativa_fechamento);
        $this->set('como_chegous', $this->Turma->como_chegou);
        $this->set('statuses', $this->Turma->status);
        $this->set('pretensaos', $this->Turma->pretensao);
    }

    function super_editar($id = null) {
        $this->Turma->id = $id;
        Configure::write('Config.language', 'pt_br');
        if (!empty($this->data)) {
            if (empty($id))
                $id = $this->data['Turma']['id'];
            $dateTime = $this->create_date_time_from_format('Y-m-d', $this->data['Turma']['data-assinatura']);
            if ($dateTime != null)
                $this->data['Turma']['data_assinatura_contrato'] = date_format($dateTime, 'Y-m-d');
            if ($this->Turma->save($this->data['Turma'])) {
                if (!$this->adicionarRemoverCursoTurma($this->Turma->id)) {
                    if ($this->adicionarRemoverUsuario($this->Turma->id)) {
                        $this->Session->setFlash('Turma foi Salva corretamente mas alguns cursos e ' .
                                'usuarios não forma inseridos ou deletados corretamento, por favor, resolva' .
                                ' as pendências', 'flash_erro');
                        $this->redirect("/{$this->params['prefix']}/turmas/editar/{$this->Turma->id}");
                    } else {
                        $this->Session->setFlash('Turma foi Salva corretamente mas alguns cursos não foram ' .
                                'inseridos ou deletados, por favor, resolva as pendências', 'flash_erro');
                        $this->redirect("/{$this->params['prefix']}/turmas/editar/{$this->Turma->id}");
                    }
                } else {
                    if ($this->adicionarRemoverUsuario($this->Turma->id)) {
                        $this->Session->setFlash('Turma foi Salva corretamente', 'flash_sucesso');
                        $this->redirect("/{$this->params['prefix']}/turmas/editar/{$this->Turma->id}");
                    } else {
                        $this->Session->setFlash('Turma foi Salva corretamente mas alguns usuarios não foram ' .
                                'inseridos ou deletados, por favor, resolva as pendências', 'flash_erro');
                        $this->redirect("/{$this->params['prefix']}/turmas/editar/{$this->Turma->id}");
                    }
                }
            }
        } else {
            $this->data = $this->Turma->read();
        }

        $this->set('semestre_formaturas', $this->Turma->semestre_formatura);
        $this->set('expectativa_fechamentos', $this->Turma->expectativa_fechamento);
        $this->set('como_chegous', $this->Turma->como_chegou);
        $this->set('statuses', $this->Turma->status);
        $this->set('pretensaos', $this->Turma->pretensao);
        $this->set('adesoes', $this->Turma->adesoes);
        $this->set('checkout', $this->Turma->checkout);
        ;

        if (!$this->data)
            $this->Session->setFlash('Turma não existente', 'flash_erro');

        $this->selectDeUniversidadesFaculdadesCursos();
        $this->selectUsuarios();
    }

    function super_visualizar($id = null) {
        $this->Turma->bindModel(
                array('hasAndBelongsToMany' => array(
                        'Usuario' => array(
                            'conditions' => array('NOT' => array('Usuario.grupo' => 'comissao'))
        ))));

        $this->comercial_visualizar($id);
    }

    function super_deletar($id = null) {

        if ($this->Turma->delete($id))
            $this->Session->setFlash('Deletado: turma número ' . $id, 'flash_sucesso');
        else
        // TODO melhorar estas frases de erro
        // Talvez colocá-las em um lugar unificado seja interessante
            $this->Session->setFlash('Erro ao deletar a turma de id ' . $id, 'flash_erro');

        $this->redirect("/{$this->params['prefix']}/turmas");
    }

    /* Atendimento */

    function atendimento_visualizar($id = null) {
        if (isset($id)) {
            $this->Turma->bindModel(array('hasAndBelongsToMany' => array('Curso')));
            $turma = $this->Turma->findById($id);
            if (strcmp($turma['Turma']['status'], 'aberta') == 0) {
                unset($id);
                $this->Session->setFlash('Acesso negado. Turma não está fechada. ', 'flash_erro');
                $this->redirect("/{$this->params['prefix']}/turmas");
            }
        } else if (isset($session_turma)) {
            if (strcmp($session_turma['Turma']['status'], 'aberta') == 0)
                $this->Session->delete('turma');
        }
        $this->comercial_visualizar($id);
    }
    
    function super_cpanel() {
        $this->autoRender = false;
        $this->layout = false;
        Configure::write(array('debug' => 1));
        $this->CpanelApi->set_port(2083);
        $this->CpanelApi->host = 'th1045087.underdc.net';
        $this->CpanelApi->username = 'thweb';
        $this->CpanelApi->password = 'a13e131294';
        $this->CpanelApi->debug = TRUE;
        $this->CpanelApi->rawXML = TRUE;
        $result = $this->CpanelApi->api2_query(
            'thweb',
            'SubDomain',
            'addsubdomain',
            array(
                'dir' => '/sistema/app/webroot/',
                'domain' => 'teste',
                'rootdomain' => 'asformaturas.com.br'
            )
        );

        if($result) {
                debug($result);
        } else {            
                debug($this->CpanelApi->error);
        }
    }

    private function selectUsuarios() {
        $gruposUsuariosPossiveis = array('Usuario.grupo' => array('planejamento', 'comercial', 'atendimento'));
        $usuariosBruto = $this->Usuario->find('all', array('conditions' => array('OR' => $gruposUsuariosPossiveis)));
        $usuarios = array();
        foreach ($usuariosBruto as $usuario) 
            $usuarios[] = $usuario['Usuario'];
        $this->set('usuarios', $usuarios);
    }

    private function selectDeUniversidadesFaculdadesCursos() {
        $cursosFaculdadesUniversidades = $this->Curso->procurarTodosUniversidadeFaculdadeCursos();

        $arvoreCursos = array();
        $cursosRelacionados = array();
        foreach ($cursosFaculdadesUniversidades as $umCurso) {
            $idUniversidade = $umCurso['Universidade']['id'];
            $idFaculdade = $umCurso['Faculdade']['id'];
            $idCurso = $umCurso['Curso']['id'];

            if (!empty($this->data['CursoTurma']))
                foreach ($this->data['CursoTurma'] as $cursoTurma)
                    if ($cursoTurma['curso_id'] == $umCurso['Curso']['id'])
                        array_push($cursosRelacionados, array_merge($umCurso, array('CursoTurma' => $cursoTurma)));

            if (empty($arvoreCursos[$idUniversidade]))
                $arvoreCursos[$idUniversidade] = $umCurso['Universidade'];

            if (empty($arvoreCursos[$idUniversidade][$idFaculdade]))
                $arvoreCursos[$idUniversidade][$idFaculdade] = $umCurso['Faculdade'];

            $arvoreCursos[$idUniversidade][$idFaculdade][$idCurso] = $umCurso['Curso'];
        }

        //debug($arvoreCursos);

        $this->set('arvoreCursos', $arvoreCursos);
        $this->set('cursosRelacionados', $cursosRelacionados);
        $periodos = $this->CursoTurma->getPeriodos();

        // O de periodos array deve ser formatado para o select
        $periodosArray = array();
        foreach ($periodos as $periodo) {
            $periodosArray[$periodo] = $periodo;
        }
        $this->set('periodo', $periodosArray);
    }

    private function adicionarRemoverUsuario($turmaId = null) {
        if (isset($this->data['Turma']['adicionar_usuarios_array']) && isset($this->data['Turma']['remover_usuarios_array'])) {
            $adicionarString = $this->data['Turma']['adicionar_usuarios_array'];
            $removerString = $this->data['Turma']['remover_usuarios_array'];

            $adicionarArray = explode(',', $adicionarString);
            $removerArray = explode(',', $removerString);

            $size = count($adicionarArray);
            for ($i = 0; $i < $size; $i++) {
                $idRepetido = array_search($adicionarArray[$i], $removerArray);
                if ($idRepetido !== false) {
                    unset($adicionarArray[$i]);
                    unset($removerArray[$idRepetido]);
                }
            }

            if (empty($turmaId))
                $turmaId = $this->data['Turma']['id'];

            $this->removeEmpty($adicionarArray);
            $this->removeEmpty($removerArray);

            $sucesso = true;

            foreach ($adicionarArray as $id) {
                if ($sucesso)
                    $sucesso = $this->Turma->alocarUsuario($id, $turmaId);
                else
                    $this->Turma->alocarUsuario($id, $turmaId);
            }

            foreach ($removerArray as $id) {
                if ($sucesso)
                    $sucesso = $this->Turma->removerUsuarioAlocado($id, $turmaId);
                else
                    $this->Turma->removerUsuarioAlocado($id, $turmaId);
            }
            return $sucesso;
        } else {
            //Nao recebeu nada para adicionar ou remover
            return true;
        }
    }

    private function adicionarRemoverCursoTurma($idTurma) {
        // Recupera os valores que sao para serem adicionados ou deletados
        $adicionarString = $this->data['Turma']['adicionar_cursos_array'];
        $removerString = $this->data['Turma']['remover_cursos_array'];

        // Retira apenas as partes importantes e as insere em um array 
        preg_match_all('/\{curso_id:([\d]*), turno:([a-z]*)\}*/', $adicionarString, $matches);
        $adicionarArray = array();
        for ($i = 0; $i < count($matches[1]); $i++) {
            $adicionarArray[] = array('curso_id' => $matches[1][$i], 'turno' => $matches[2][$i]);
        }

        preg_match_all('/\{curso_id:([\d]*), turno:([a-z]*)\}*/', $removerString, $matches);
        $removerArray = array();
        for ($i = 0; $i < count($matches[1]); $i++) {
            if (($key = array_search(array('curso_id' => $matches[1][$i], 'turno' => $matches[2][$i]), $adicionarArray)) > 0)
                unset($adicionarArray[$key]);
            else
                $removerArray[] = array('curso_id' => $matches[1][$i], 'turno' => $matches[2][$i]);
        }
        $success = true;

        foreach ($adicionarArray as $dadosCursoTurma) {
            $cursoTurma = $this->CursoTurma->create();
            $cursoTurma['CursoTurma']['curso_id'] = $dadosCursoTurma['curso_id'];
            $cursoTurma['CursoTurma']['turno'] = $dadosCursoTurma['turno'];
            $cursoTurma['CursoTurma']['turma_id'] = $idTurma;
            if (!$this->CursoTurma->save($cursoTurma['CursoTurma']))
                $success = false;
        }

        foreach ($removerArray as $dadosCursoTurma) {
            if ($this->CursoTurma->deleteAll(array('CursoTurma.curso_id' => $dadosCursoTurma['curso_id'],
                        'CursoTurma.turno' => $dadosCursoTurma['turno'],
                        'CursoTurma.turma_id' => $idTurma)))
                ;
            $success = false;
        }

        return $success;
    }

    private function sendNewUserMail() {
        /* SMTP Options */
        $this->Email->smtpOptions = array(
            'port' => '465',
            'timeout' => '30',
            'host' => 'ssl://smtp.gmail.com',
            'username' => 'guilherme.melo@touchtec.com.br',
            'password' => '');

        /* Set delivery method */
        $this->Email->delivery = 'smtp';


        $this->Email->to = "Guilherme <guimp.campos@gmail.com>"; //Formato de endereço precisa estar assim
        $this->Email->subject = 'Welcome to our really cool thing';
        $this->Email->from = "Guilherme <guilherme.melo@touchtec.com.br>"; //Formato de endereço precisa estar assim
        $this->Email->template = 'simple_message'; // note no '.ctp'
        //Send as 'html', 'text' or 'both' (default is 'text')
        $this->Email->sendAs = 'both'; // because we like to send pretty mail
        //Do not pass any args to send()
        $this->Email->send();

        /* Check for SMTP errors. */
        $this->set('smtp_errors', $this->Email->smtpError);

        // para debugar na view, colocar a linha:
        // debug($smtp_errors);
    }

    /* Função auxiliar que verifica se há uma turma na sessão.
     * Caso não exista redireciona para uma tela de procura de usuário
     * Lembrando que a turma é persistida na sessão no momento da visualização 
     * das informações da turma selecionada.
     */

    function _redirecionaProcuraTurma() {
        $turma = $this->Session->read('turma');
        if (!isset($turma)) {
            $this->super_index();
            $this->redirect("/{$this->params['prefix']}/turmas/index");
        }
    }

    private function removeEmpty(&$array) {
        $key = array_search("", $array);
        if ($key !== false) {
            unset($array[$key]);
        }
    }

    function producao_listar() {
        $this->super_listar();
    }
    
    function foto_listar() {
        $this->super_listar();
    }

    function planejamento_gerar_excel() {
        set_time_limit(0);
        ini_set("memory_limit", "4096M");
        $this->layout = false;
        $this->set('turmaLogada', $this->obterTurmaLogada());
        $this->planejamento_index();
    }
    
}
?>
