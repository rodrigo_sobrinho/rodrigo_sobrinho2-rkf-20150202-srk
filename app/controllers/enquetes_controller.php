<?php

class EnquetesController extends AppController {
    
    var $name = 'Enquetes';
    var $uses = array('Usuario','Enquete','EnquetePergunta',
        'EnqueteAlternativa','EnqueteUsuario','EnqueteTurma',
        'EnqueteUsuarioResposta','TurmasUsuario');
    
    function respostas($enqueteId) {
        $this->layout = false;
        $totais = $this->EnqueteUsuario->getResumoTotais($enqueteId);
        $listaPerguntas = $this->EnqueteUsuario->getResumoPerguntas($enqueteId);
        $resumoFormandos = $this->EnqueteUsuario->getResumoPorFormando($enqueteId);
        $this->Enquete->unbindModelAll();
        $enquete = $this->Enquete->read(null,$enqueteId);
        /*
        $this->EnquetePergunta->unbindModel(array(
            'belongsTo' => array('Enquete'),
            'hasMany' => array('EnqueteAlternativa')
        ),false);
        $this->EnqueteUsuario->unbindModel(array(
            'belongsTo' => array('Enquete')
        ),false);
        $this->EnqueteUsuario->Usuario->unbindModel(array(
            'hasOne' => array('FormandoProfile'),
            'hasMany' => array('Despesa','UsuarioConta','FormandoFotoTelao','Cupom'),
            'hasAndBelongsToMany' => array('Campanhas','Turma')
        ),false);
        $this->EnqueteUsuarioResposta->unbindModel(array(
            'belongsTo' => array('EnqueteUsuario')
        ),false);
        $this->Enquete->recursive = 4;
        $enquete = $this->Enquete->read(null,$enqueteId);
        $status = array('adiou' => 0,'finalizou' => 0,'rejeitou' => 0);
        $respostas = array();
        $perguntas = array();
        foreach($enquete['EnquetePergunta'] as $pergunta)
            $perguntas[$pergunta['id']] = $pergunta['texto'];
        foreach($enquete['EnqueteUsuario'] as $usuario) {
            foreach($status as $tipo => $valor)
                $status[$tipo]+= $usuario[$tipo];
            foreach($usuario['EnqueteUsuarioResposta'] as $resposta) {
                $alternativa = $resposta['EnqueteAlternativa'];
                if(!isset($respostas[$alternativa['enquete_pergunta_id']]))
                    $respostas[$alternativa['enquete_pergunta_id']] = array('alternativas' => array());
                if(!isset($respostas[$alternativa['enquete_pergunta_id']]['alternativas'][$alternativa['id']]))
                    $respostas[$alternativa['enquete_pergunta_id']]['alternativas'][$alternativa['id']] = array(
                        'texto' => $alternativa['texto'],
                        'qtde' => 0
                    );
                $respostas[$alternativa['enquete_pergunta_id']]['alternativas'][$alternativa['id']]['qtde']++;
            }
        }
         * 
         */
        $this->set('totais', $totais);
        $this->set('listaPerguntas', $listaPerguntas);
        $this->set('enquete',$enquete);
        $this->set('resumoFormandos',$resumoFormandos);
    }
    
    function listar() {
        $usuario = $this->obterUsuarioLogado();
        if($this->eFuncionario()) {
            $this->layout = false;
            if($usuario['Usuario']['nivel'] == "gerencial")
                $enquetes = $this->Enquete->find('all');
            else
                $enquetes = $this->Enquete->find('all',array(
                    'conditions' => array(
                        'Enquete.usuario_id' => $usuario['Usuario']['id']
                    )
                ));
            $this->set('enquetes',$enquetes);
        } else {
            $this->redirect("/{$usuario['Usuario']['grupo']}");
        }
    }
    
    function nao_respondidas() {
        $this->layout = false;
        $enquetes = $this->Enquete->obterEnquetePorUsuario($this->obterUsuarioLogado(),$this->obterTurmaLogada(),true);
        $this->set('enquetes',$enquetes);
    }
    
    function editar($enqueteId = false) {
        $usuario = $this->obterUsuarioLogado();
        if($this->eFuncionario()) {
            if($this->data) {
                $this->autoRender = false;
                $this->Enquete->unbindModel(array(
                    'belongsTo' => array('Usuario')
                ),false);
                $dateTime = str_replace('/','-',$this->data['Enquete']['data_inicio']);
                $dateTime = $this->create_date_time_from_format('d-m-Y',$dateTime);
                $this->data['Enquete']['data_inicio'] = date_format($dateTime, 'Y-m-d');
                if(!empty($this->data['Enquete']['data_fim'])) {
                    $dateTime = str_replace('/','-',$this->data['Enquete']['data_fim']);
                    $dateTime = $this->create_date_time_from_format('d-m-Y',$dateTime);
                    $this->data['Enquete']['data_fim'] = date_format($dateTime, 'Y-m-d');
                } else {
                    $this->data['Enquete']['data_fim'] = null;
                }
                $enquetePerguntas = $this->data['EnquetePergunta'];
                unset($this->data['EnquetePergunta']);
                if($this->Enquete->saveAll($this->data)) {
                    if(isset($this->data['remover_turmas']))
                        $this->EnqueteTurma->delete($this->data['remover_turmas']);
                    $perguntas = array();
                    $erro = false;
                    $this->EnquetePergunta->recursive = 2;
                    foreach($enquetePerguntas as $index => $pergunta) {
                        $pergunta['enquete_id'] = $this->Enquete->id;
                        if(!$this->EnquetePergunta->saveAll($pergunta))
                            $erro = true;
                    }
                    if(!$erro)
                        $this->Session->setFlash('Dados inseridos com sucesso', 'metro/flash/success');
                    else
                        $this->Session->setFlash('Erro ao inserir dados', 'metro/flash/error');
                } else {
                    $this->Session->setFlash('Erro ao inserir dados', 'metro/flash/error');
                }
                echo json_encode(array());
            } elseif($enqueteId) {
                $this->Enquete->recursive = 2;
                $enquete = $this->Enquete->read(null,$enqueteId);
                if($enquete) {
                    $this->set('ativa',$this->Enquete->ativa);
                    $turmasCadastradas = array();
                    foreach($enquete['EnqueteTurma'] as $enqueteTurma)
                        $turmasCadastradas[$enqueteTurma['turma_id']] = $enqueteTurma['id'];
                    $this->set('turmasCadastradas',$turmasCadastradas);
                    $enquete['Enquete']['data_inicio'] = date('d/m/Y',strtotime($enquete['Enquete']['data_inicio']));
                    if(!empty($enquete['Enquete']['data_fim']))
                        $enquete['Enquete']['data_fim'] = date('d/m/Y',strtotime($enquete['Enquete']['data_fim']));
                    $this->data = $enquete;
                } else {
                    $this->redirect("/{$usuario['Usuario']['grupo']}/");
                }
            } else {
                $this->data = array(
                    'Enquete' => array(
                        'turmas' => 'todas',
                        'periodo' => 'unico',
                        'usuario_id' => $usuario['Usuario']['id']
                    ),
                    'EnquetePergunta' => array()
                );
            }
            $this->set('enqueteId',$enqueteId);
            $this->set('periodo',$this->Enquete->periodo);
            $this->set('turmas',$this->Enquete->turmas);
            $this->set('obrigatoria',$this->Enquete->obrigatoria);
            $this->set('grupos',$this->Usuario->grupos['formandos']);
            $turmas = $this->TurmasUsuario->find('all',array(
                'conditions' => array('usuario_id' => $usuario['Usuario']['id']),
                'joins' => array(
                    array(
                        'table' => 'turmas',
                        'foreignKey' => false,
                        'alias' => 'Turma',
                        'conditions' => array(
                            'Turma.id = TurmasUsuario.turma_id',
                            'Turma.status' => 'fechada'
                        )
                    )
                ),
                'fields' => array('Turma.*')
            ));
            $listaTurmas = array();
            foreach($turmas as $turma)
                $listaTurmas[$turma['Turma']['id']] =
                    "{$turma['Turma']['id']} - {$turma['Turma']['nome']}";
            $this->set('listaTurmas',$listaTurmas);
        } else {
            $this->redirect("/{$usuario['Usuario']['grupo']}");
        }
    }
    
    function rejeitar($enqueteId,$usuarioId = false) {
        $this->autoRender = false;
        $usuarioId = $usuarioId ? $usuarioId : $this->Session->read('Usuario.Usuario.id');
        $enquete = $this->Enquete->read(null,$enqueteId);
        if($enquete) {
            $enqueteUsuario = array(
                'EnqueteUsuario' => array(
                    'usuario_id' => $usuarioId,
                    'enquete_id' => $enqueteId,
                    'rejeitou' => 1
                )
            );
            $this->EnqueteUsuario->save($enqueteUsuario);
        }
    }
    
    function adiar($enqueteId,$usuarioId = false) {
        $this->autoRender = false;
        $usuarioId = $usuarioId ? $usuarioId : $this->Session->read('Usuario.Usuario.id');
        $enquete = $this->Enquete->read(null,$enqueteId);
        if($enquete) {
            $enqueteUsuario = array(
                'EnqueteUsuario' => array(
                    'usuario_id' => $usuarioId,
                    'enquete_id' => $enqueteId,
                    'adiou' => 1
                )
            );
            $this->EnqueteUsuario->save($enqueteUsuario);
        }
        echo json_encode(array());
    }
    
    function responder($enqueteId,$usuarioId = false) {
        $this->autoRender = false;
        Configure::write('debug',0);
        $usuarioId = $usuarioId ? $usuarioId : $this->Session->read('Usuario.Usuario.id');
        $enquete = $this->Enquete->read(null,$enqueteId);
        $json = array('erro' => true);
        if($enquete && isset($this->data['respostas'])) {
            $enqueteUsuario = array(
                'EnqueteUsuario' => array(
                    'usuario_id' => $usuarioId,
                    'enquete_id' => $enqueteId,
                    'finalizou' => 1,
                    'observacoes' => $this->data['observacoes']
                )
            );
            if($this->EnqueteUsuario->saveAll($enqueteUsuario)) {
                $respostas = array();
                $enqueteUsuarioId = $this->EnqueteUsuario->id;
                foreach($this->data['respostas'] as $resposta)
                    $respostas[] = array(
                        //'EnqueteRespostaUsuario' => array(
                            'enquete_alternativa_id' => $resposta,
                            'enquete_usuario_id' => $enqueteUsuarioId
                        //)
                    );
                if($this->EnqueteUsuarioResposta->saveAll($respostas))
                    $json['erro'] = false;
            }
        }
        echo json_encode($json);
    }
    
}