<?php
class CampanhasUsuario extends AppModel {
	var $name = 'CampanhasUsuario';
	var $useTable = 'campanhas_usuarios';
	
	var $belongsTo = array(
		'Campanha', 'Usuario'
	);
	
	var $hasAndBelongsToMany = array(
        'CampanhasUsuarioCampanhasExtra' =>
            array(
                'className'              => 'CampanhasUsuarioCampanhasExtra',
                'joinTable'              => 'campanhas_usuarios_campanhas_extras',
                'foreignKey'             => 'campanhas_extra_id',
                'associationForeignKey'  => 'campanhas_usuario_id',
                'unique'                 => false
            )
    );

}