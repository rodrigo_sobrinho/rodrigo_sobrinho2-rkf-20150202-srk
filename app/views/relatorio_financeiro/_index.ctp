<label id="conteudo-titulo" class="box-com-titulo-header">Relat&oacute;rio financeiro</label>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<?php $paginator->options(array('url' => array($this->params['prefix'] => true))); ?>
	<div style="overflow:hidden; width:100%">
	<div class="legenda" style="width:30%">
		<table>
			<tr>
				<td>
					<table>
						<tr>
							<td colspan="2"><h2>Status</h2></td>
						</tr>
						<tr>
							<td width="30"><span class="color" style="background-color:#FBFCC2"></span></td>
							<td>Cancelado</td>
						</tr>
						<tr>
							<td><span class="color" style="background-color:#FCD2D2"></span></td>
							<td>Inadimplente (3 ou mais meses de atraso)</td>
						</tr>
						<tr>
							<td><span class="color" style="background-color:#E0DEDF"></span></td>
							<td>Inativo (N&atilde;o pagou parcelas)</td>
						</tr>
						<tr>
							<td><span class="color" style="background-color:#C2FCE8"></span></td>
							<td>Ativo (em dia com as parcelas)</td>
						</tr>
                                                			<tr>
							<td><span class="color" style="background-color:#BDF2BB"></span></td>
							<td>Quitado (Todas parcelas pagas)</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<?php if($resumo) : ?>
	<div class="legenda" style="width:30%">
		<table>
			<tr>
				<td>
					<table>
						<tr>
							<td><h2>Resumo</h2></td>
						</tr>
						<tr>
							<td>
								<p>
									Total de formandos: <em><?=$resumo["total"]?></em><br />
									Formandos ativos: <em><?=$resumo["ativos"]?></em><br />
									Formandos cancelados: <em><?=$resumo["cancelados"]?></em><br />
									Formandos inadimplentes: <em><?=$resumo["inadimplentes"]?></em><br />
									Formandos inativos: <em><?=$resumo["inativos"]?></em><br />
								</p>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<?php endif; ?>
	</div>
	<?php if(count($formandos) > 0) :?>
	<?php $url = strpos($this->params['url']['url'],"index") === false ? $this->params['url']['url'] . "/gerar_excel" : str_replace("index","gerar_excel",$this->params['url']['url']) ?>
	<div class="tabela-adicionar-item">
	<?=$html->link('Gerar excel',$html->url('/' . $url, true), array('id' => 'link-gerar-excel' , 'target' => '_blank')); ?>
	<div style="clear:both;"></div>
	</div>
	<?php endif; ?>
	<div class="container-tabela">
		<table>
			<thead>
				<tr>
					<th scope="col"><?=$paginator->sort('Cód. do Formando', 'codigo_formando'); ?></th>
					<th scope="col"><?=$paginator->sort('Nome', 'nome'); ?></th>
					<th scope="col"><?=$paginator->sort('Status', 'status'); ?></th>
					<th scope="col"><?=$paginator->sort('Total de Parcelas', 'total_parcelas'); ?></th>
					<th scope="col"><?=$paginator->sort('Valor Total', 'valor_total'); ?></th>
					<th scope="col"><?=$paginator->sort('Data 1a Parcela', 'data_primeira_parcela'); ?></th>
					<th scope="col"><?=$paginator->sort('Parcelas Pagas', 'parcelas_pagas'); ?></th>
					<th scope="col"><?=$paginator->sort('Total Pago', 'total_pago'); ?></th>
					<th scope="col"><?=$paginator->sort('Parcelas Atrasadas', 'parcelas_atrasadas'); ?></th>
					<th scope="col">Saldo em Aberto</th>
				</tr>
			</thead>
			<tbody>
			<?php if(count($formandos) > 0) :?>
			<?php foreach($formandos as $formando): ?>
				<?php
					$saldoAberto = $formando['ViewRelatorioFinanceiro']['valor_total'] - $formando['ViewRelatorioFinanceiro']['total_pago'];
					if($formando["ViewRelatorioFinanceiro"]["status"] == "Cancelado")
						$color = "#FBFCC2";
					elseif($formando["ViewRelatorioFinanceiro"]["status"] == "Inativo")
						$color = "#E0DEDF";
					elseif($formando["ViewRelatorioFinanceiro"]["status"] == "Inadimplente")
						$color = "#FCD2D2";
                                        elseif(($formando['ViewRelatorioFinanceiro']['total_pago']) >= ($formando['ViewRelatorioFinanceiro']['valor_total']))
                                                $color = "#BDF2BB";
					else
						$color = "#C2FCE8";
				?>
				<?="<tr style='background-color:$color'>"?>
					<td  collabel="1" width="10%"><?=$formando['ViewRelatorioFinanceiro']['codigo_formando']; ?></td>
					<td  collabel="1" width="20%"><?=$formando['ViewRelatorioFinanceiro']['nome']; ?></td>
					<td  collabel="1"><?=$formando["ViewRelatorioFinanceiro"]["status"] ?></td>
					<td  collabel="1"><?=$formando['ViewRelatorioFinanceiro']['total_parcelas']; ?></td>
					<td  collabel="1">R$<?=number_format($formando['ViewRelatorioFinanceiro']['valor_total'], 2, ',' , '.'); ?></td>
					<td  collabel="1"><?=$formando['ViewRelatorioFinanceiro']["data_primeira_parcela"]; ?></td>
					<td  collabel="1"><?=$formando['ViewRelatorioFinanceiro']['parcelas_pagas']; ?></td>
					<td  collabel="1">R$<?=number_format($formando['ViewRelatorioFinanceiro']['total_pago'], 2, ',' , '.'); ?></td>
					<td  collabel="1"><?=$formando['ViewRelatorioFinanceiro']['parcelas_atrasadas']; ?></td>
					<td  collabel="1">R$<?=number_format($saldoAberto, 2, ',' , '.'); ?></td>
				</tr>
			<?php endforeach; ?>
			<?php  else : ?>
				<td colspan="10"><h2>N&atilde;o foi encontrado registro financeiro desta turma</h2></td>
			<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>