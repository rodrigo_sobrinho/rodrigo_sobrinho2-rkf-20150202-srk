<script type="text/javascript">
	function adicionar_campo_foto() {
		$("#campos_fotos").append(gerar_elemento());
		
	}

	function gerar_elemento() {
		indice = (get_ultimo_indice() + 1).toString();
		
		return '<p id="campo_foto_'+indice+'" class="grid_full alpha omega first">' +
					'<input type="file" id="FileFoto' + 
						indice + '" value="" error="" class="grid_6 alpha omega first" name="data[Fotos][foto' + 
						indice + ']">' +
					'<img src=\"<?php echo $this->webroot ?>img/error.png\" onclick=\"remover_campo_foto(' + indice + ')\" style=\"cursor: pointer;margin-left: 20px;\" />' +
				'</p>'
	}

	function get_ultimo_indice() {
		max = 0;

		$("#campos_fotos").children("p").each(function(index) {
		    id = $(this).attr('id');
		    idx = parseInt(id.charAt(id.length-1));
		    if (idx > max)
		        max = idx;
		});

		return max;
	}

	function remover_campo_foto(indice) {
		$("#campo_foto_" + indice).remove();
	}

	function remover_logo(parceiro_id) {
		var apagar = confirm("Tem certeza que deseja apagar o logo?");

		if (apagar) {
			$.post('<?php echo $html->url(array($this->params['prefix'] => true, "controller" => "parceiros", "action" => "apagar_logo")) ?>', 
					{ data: { id: parceiro_id } },
					function(data) {
						if (data == "sucesso") {
							$("#logo_parceiro").remove();
						} else {
							alert('Erro ao remover logo.');
						}
					});
		}
	}
	
</script>

<?php echo $form->input('id',  array('hiddenField' => true)); ?>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha omega">Nome</label>
	<?php echo $form->input('nome', array('class' => 'grid_8 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8 first')));?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha omega">Logo</label>
	<p class="grid_11 alpha omega fisrt">
		<div id="logo_parceiro" class="grid_full alpha omega first">
			<?php 
				if (stristr($this->params['action'], 'editar') !== false && !empty($parceiro_banco['Parceiro']['logo'])) {
					echo $html->image($caminho_raiz_logos.'/'.$parceiro_banco['Parceiro']['logo'], array('style' => 'width: 30%;'));
					echo $html->image('error.png', array('onclick' => 'remover_logo('.$this->data['Parceiro']['id'].')', 'style' => 'cursor: pointer; vertical-align: top;'));
				}
			?>
		</div>
			<?php 
				echo $form->file('Logo.logo', array('class' => 'grid_8 alpha omega first', 'error' => array('wrap' => 'span', 'class' => 'grid_8 first')));
				echo $form->error('Logo.logo', array('wrap' => 'span', 'class' => 'grid_8 first'));
			?>
		
	</p>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Descrição</label>
	<?php echo $form->input('descricao', array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8 first'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha omega">Site</label>
	<?php echo $form->input('site', array('class' => 'grid_8 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8')));?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha omega">Status</label>
	<?php echo $form->select('Parceiro.ativo', array(1 => 'Ativo', 0 => 'Não Ativo'), null, array('empty' => false, 'class' => 'grid_4 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8 first'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha omega">Fotos</label>
	
	<?php if (stristr($this->params['action'], 'editar') !== false) { ?>
		<div id="fotos" class="grid_full alpha omega first">
			<?php include('_editar_fotos.ctp'); ?>
		</div>
	<?php } ?>
	
	<div id="campos_fotos" class="grid_full alpha omega first" style="margin-top: 20px;">
		<p id="campo_foto_0" class="grid_full alpha omega first">
			<?php 
				echo $form->file('Fotos.foto0', array('class' => 'grid_6 alpha omega first', 'error' => array('wrap' => 'span', 'class' => 'grid_8 first')));
				echo $form->error('Fotos.foto', array('wrap' => 'span', 'class' => 'grid_8 first'));
				echo $html->image('error.png', array('onclick' => 'remover_campo_foto(0)', 'style' => 'cursor: pointer; vertical-align: top;margin-left: 20px;'))
			?>
		</p>
		<div style="clear:both"></div>
	</div>
	
	<?php echo $form->button('', array('type'=>'button', 'value' => 'Adicionar', 'class' => 'grid_3 first', 'onclick' => 'adicionar_campo_foto()'));  ?>
</p>



