<style type="text/css">
.progressbar { position:relative; width:80%; float:right; margin-right:15px; }
.descricao { position:absolute; top:8px; left:5px; font-size: 11px; line-height:11px;
    color:white; font-style:italic }
</style>
<script type='text/javascript'>
    $(function() {
        var timer;
        function verificarUpload(sleep) {
            var qtdeApp = $(".app-upload:not(.finalizado)").length;
            var appsProcessados = 0;
            $(".app-upload:not(.finalizado)").each(function() {
                var app = this;
                var id = $(app).attr('id').substring(7);
                var obj = $(app).children('.status')[0];
                var actions = $(obj).children('.actions');
                var progressbar = $(obj).children('.progressbar');
                var descricao = progressbar.children('.descricao');
                var url = "/<?=$this->params["prefix"]?>/pagamentos/verificar_upload/"+id;
                $.getJSON(url, function(data) {
                    if(data.erro == 1) {
                        if(data.mensagem != undefined)
                            $(obj).css('color','red').html(data.mensagem);
                        else
                            $(obj).css('color','red').html('Erro ao Verificar Upload');
                        $(app).addClass('finalizado');
                        $('.iniciar-upload').removeClass('disabled');
                    } else {
                        var upload = data.arquivo.UploadPagamento;
                        var registrosProcessados = upload.registros_processados;
                        var total = upload.total_registros;
                        var porcentagem = (registrosProcessados*100)/total;
                        var texto = registrosProcessados + " de " + total +
                                " pagamentos importados";
                        descricao.text(texto);
                        progressbar.progressbar("option", "value",parseFloat(porcentagem.toFixed(2)));
                        if($(obj).prev().html().trim() == "" && upload.data_inicio != null)
                            $(obj).prev().html(upload.data_inicio);
                        if(data.arquivo.UploadPagamento.data_fim != null && !$(app).hasClass('finalizado')) {
                            $(app).addClass('finalizado');
                            $('.iniciar-upload').removeClass('disabled');
                            actions.children('.iniciar-upload').remove();
                            var log = '<a class="button submit" href="/<?=$this->params["prefix"]?>/pagamentos/view/'+
                                    id+'" target="_blank">Log De Importação</a>';
                            actions.prepend(log);
                            progressbar.fadeOut(500,function() {
                                actions.fadeIn(500);
                            });
                        }
                    }
                }).fail(function() {
                    $(app).addClass('finalizado');
                    $('.iniciar-upload').removeClass('disabled');
                }).always(function() {
                    appsProcessados++;
                    if(appsProcessados >= qtdeApp)
                        timer = setTimeout(function() { verificarUpload(1000) },sleep);
                });
            });
        }
        
        $(".app-upload.iniciado:not(.finalizado)").each(function() {
            iniciarUpload($(this));
        });
        
        $('.iniciar-upload').click(function(e) {
            e.preventDefault();
            if(!$(this).hasClass('disabled'))
                iniciarUpload($(this).parent().parent().parent());
        });
        
        function iniciarUpload(app) {
            $('.iniciar-upload').addClass('disabled');
            var id = app.attr('id').substring(7);
            var actions = app.children('.status').children('.actions');
            var progressbar = app.children('.status').children('.progressbar');
            var descricao = progressbar.children('.descricao');
            descricao.html('<em style="color:red">Iniciando Importa&ccedil;&atilde;o de Pagamentos...</em>');
            actions.fadeOut(500,function() {
                progressbar.progressbar({value: false});
                progressbar.fadeIn(500,function() {
                    clearTimeout(timer);
                    verificarUpload(1500);
                });
            });
        }
    });
</script>
<span id="conteudo-titulo" class="box-com-titulo-header">
    Upload Pagamentos
</span>
<div id="conteudo-container">
    <?php $session->flash(); ?>
    <div class="container-tabela">
        <table class='tabela-upload'>
            <thead>
                <tr>
                    <th scope="col">Usuario</th>
                    <th scope="col">Data Cadastro</th>					
                    <th scope="col">Data In&iacute;cio</th>
                    <th scope="col"></th>
                    <th scope="col" width="30%"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($arquivos as $arquivo): ?>
                    <?php
                        if(!empty($arquivo['UploadPagamento']['data_fim']))
                            $appClass = 'finalizado';
                        elseif(!empty($arquivo['UploadPagamento']['data_inicio']))
                            $appClass = 'iniciado';
                        else
                            $appClass = ''
                    ?>
                    <tr class="app-upload <?=$appClass?>" id='upload-<?=$arquivo['UploadPagamento']['id']?>'>
                        <td colspan="1"><?=$arquivo['Usuario']['nome']; ?></td>
                        <td colspan="1"><?=date('d/m/Y à\s H:i',
                                strtotime($arquivo['UploadPagamento']['data_cadastro'])); ?></td>
                        <td colspan="1">
                            <?php
                                if(!empty($arquivo['UploadPagamento']['data_inicio']))
                                    echo date('d/m/Y à\s H:i',
                                            strtotime($arquivo['UploadPagamento']['data_inicio']));
                            ?>
                        </td>
                        <?php if(empty($arquivo['UploadPagamento']['data_fim'])) : ?>
                        <td colspan="2" width="50%" class='status'>
                            <div class="progressbar"
                                 style='<?=empty($arquivo['UploadPagamento']['data_inicio']) ? 'display:none' :''?>'>
                                <div class="descricao"></div>
                            </div>
                            <div class='actions'>
                                <a class="button submit iniciar-upload">Iniciar Importação</a>
                                <?=$html->link('Baixar Excel',
                                    array($this->params['prefix'] => true,
                                        'controller' => 'pagamentos',
                                        'action' => 'excel',
                                        $arquivo['UploadPagamento']['id']),
                                    array('class' => 'button submit','target' => '_blank')); ?>
                            </div>
                        </td>
                        <?php else : ?>
                        <td colspan="1">
                            <?=date('d/m/Y à\s H:i',
                                strtotime($arquivo['UploadPagamento']['data_fim'])); ?>
                        </td>
                        <td colspan="1">
                            <?=$html->link('Log De Importação',
                                array($this->params['prefix'] => true,
                                    'controller' => 'pagamentos',
                                    'action' => 'view',
                                    $arquivo['UploadPagamento']['id']),
                                array('class' => 'button submit','target' => '_blank')); ?>
                            <?=$html->link('Baixar Excel',
                                array($this->params['prefix'] => true,
                                    'controller' => 'pagamentos',
                                    'action' => 'excel',
                                    $arquivo['UploadPagamento']['id']),
                                array('class' => 'button submit','target' => '_blank')); ?>
                        </td>
                        <?php endif; ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>