<?php if($mapa) : ?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<?php
$session->flash();
$paginator->options(array(
    'url' => array($this->params['prefix'] => true)));
$sortOptions = array('data-bind' => 'click: loadThis');
?>
<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: reload"></a>
        Relat&oacute;rio de mesas
        <a class="button mini default pull-right"
            href="<?=$this->here?>/imprimir" target="_blank">
            Imprimir <i class="icon-printer"></i>
        </a>
    </h2>
</div>
<div class="row-fluid">
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="15%">
                    <?=$paginator->sort('COD', 'codigo_formando',$sortOptions); ?>
                </th>
                <th scope="col" width="40%">
                    <?=$paginator->sort('Nome', 'ViewFormandos.nome',$sortOptions); ?>
                </th>
                <th scope="col" width="10%">Qtde Mesas</th>
                <th scope="col" width="35%">N&ordm; Mesas</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($formandos as $i => $formando) : $numeros = array(); ?>
            <?php foreach($formando['EventoMapaLocal'] as $local)
                    $numeros[] = "{$local['EventoMapaColuna']['nome']}{$local['numero']}"; ?>
            <tr>
                <td><?=$formando['ViewFormandos']['codigo_formando']; ?></td>
                <td><?=$formando['ViewFormandos']['nome']; ?></td>
                <td><?=count($formando['EventoMapaLocal'])?></td>
                <td><?=implode(',', $numeros)?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="2" class="paginacao">
                    <?=$paginator->numbers(array('separator' => ' ',
                        'data-bind' => 'click: loadThis')); ?>
                </td>
                <td colspan="2">
                    <span class="label label-info">
                    <?=$paginator->counter(array(
                        'format' => 'Total : %count% formandos')); ?>
                    </span>
                </td>
            </tr>
        </tfoot>
    </table>
</div>
<?php else : ?>
<h2 class="fg-color-red">Nenhum mapa de mesa encontrado para esta turma</h2>
<?php endif; ?>