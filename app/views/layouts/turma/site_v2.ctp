<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
    <head>
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <meta name="google-site-verification" content="Ip4-BMIP8v6t5ky279zPi7nhLEHl3vxifr2ccDarP30" />
        <?= $html->charset(); ?>
        <title><?= $title_for_layout; ?></title>
        <link rel="icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon"> 
        <link rel="shortcut icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootmetro/default.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootmetro/icons.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootmetro/responsive.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/fonts.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/typography.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/button.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/color.css">
        <!--<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/font_jacques.css">-->
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/site_turma.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/unslider.css">
        <!--
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/color.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/form.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/button.css">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/default.css">
        -->
        <!--[if IE 7]>
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootmetro/icons-ie7.css">
        <![endif]-->
        <script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/modernizr-2.6.2.js"></script>
        <script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/jquery-1.9.1.js"></script>
        <script src="<?= $this->webroot ?>metro/js/unslider.min.js"></script>
    </head>
    <body>
        <div class="container" id="container">
            <div class="row-fluid logo">
                <img src="<?=$this->webroot?>img/logo_turma.png" width="150" />
            </div>
            <div class="row-fluid cabecalho">
                <div class="banner">
                    <ul>
                        <?php foreach($fotosHome as $fotoHome) : ?>
                        <li style='background-image: url(<?="{$this->webroot}{$fotoHome['TurmaSiteFoto']['arquivo']}"?>)'></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="menu-navegacao">
                    <ul>
                        <li>
                            <a href="/turmas/site/">Home</a>
                        </li>
                        <?php foreach($paginas as $pagina) : ?>
                        <li>
                            <a href="/turmas/site/<?=$pagina['TurmaPagina']['id']?>"
                               class="scroll"><?=$pagina['TurmaPagina']['menu']?></a>
                        </li>
                        <?php endforeach; ?>
                        <li>
                            <a href="/turmas/site/fotos">Fotos</a>
                        </li>
                    </ul>
                </div>
            </div>
            <?=$content_for_layout; ?>
            <div class="rodape">
                
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.banner').unslider({
                    arrows: false,
                    fluid: true,
                    keys: true,
                    dots: true
                });
            });
        </script>
    </body>
</html>