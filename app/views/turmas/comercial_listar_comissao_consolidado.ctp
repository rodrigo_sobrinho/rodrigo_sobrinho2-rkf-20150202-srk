<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<style type="text/css">
    .footer{
        color: white;
        background-color: #25a0da;
        box-sizing: border-box;
        height: 25px!important;
    }
</style>
<script type="text/javascript">
$(document).ready(function() {
    $('.detalhar').click(function() {
        bootbox.dialog('Carregando',[{
            label: 'Fechar'
        }],{
            remote: '<?="/{$this->params['prefix']}/turmas/listar_comissao_relatorio/"?>' + $(this).attr('dir')
        });
    });
})
</script>
<?php 
    $session->flash();
    $paginator->options(array(
        'url' => array($this->params['prefix'] => true)));
    $sortOptions = array('data-bind' => 'click: loadThis');
?>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Comissões Turmas
            <a href="comercial/turmas/recibos_comissao_relatorio/" 
               class="button mini pull-right bg-color-orangeDark relatorio" data-bind="click:loadThis">
                Comissões Recebidas
                <i class="img icon-cloud-9"></i>
            </a>
    </div>
</div>
<?php if (sizeof($resumo) > 0) : ?>
<div class="row-fluid">
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="25%">
                    Turmas
                </th>
                <th scope="col" width="5%">
                    Contratos
                </th>
                <th scope="col" width="5%">
                    Adesões
                </th>
                <th scope="col" width="5%">
                    Ativos
                </th>
                <th scope="col" width="5%">
                    Este Mês
                </th>
                <th scope="col" width="5%">
                    Potencial a Receber
                </th>
                <th scope="col" width="5%">
                    Recebidos
                </th>
                <th scope="col" width="5%">
                    %
                </th>
                <th scope="col" width="10%">
                    Valores Recebidos
                </th>
                <th scope="col" width="10%">
                    Receber Próximo Ciclo
                </th>
                <th scope="col" width="10%">
                    Valor Máximo a Receber
                </th>
                <th scope="col" width="10%"></th>
            </tr>
        </thead>
        <tbody>
            <?php 
                foreach($resumo as $res) : 
                    $r = (array)json_decode($res['ComissaoRelatorioConsolidado']['dados']);
            ?>
            <tr>
                <td><?=$r['turma']?></td>
                <td><?=$r['contratos'];?></td>
                <td><?=$r['adesoes'];?></td>
                <td><?=$r['ativos'];?></td>
                <td><?=$r['este_mes'];?></td>
                <td><?=$r['forecast']?></td>
                <td><?=$r['recebidos']?></td>
                <td><?=number_format($r['porcentagem'], 1);?></td>
                <td>R$ <?=number_format(($r['valor_recebidos']), 2, ',', '.');?></td>
                <td>R$ <?=number_format(($r['valor_proximo_ciclo']), 2, ',', '.');?></td>
                <td>R$ <?=number_format(($r['valor_forecast']), 2, ',', '.');?></td>
                <td>
                    <button type="button" class="button mini bg-color-blueDark detalhar" dir="<?=substr($r['turma'], 0, 4)?>">
                        Detalhar
                        <i class="icon-upload"></i>
                    </button>
                </td>
            </tr>
            <?php 
                endforeach; 
            ?>
        </tbody>
        <tfoot>
            <tr class="footer">
                <td>&nbsp;</td>
                <td><b><?=$footer['total_contratos']?><b/></td>
                <td><b><?=$footer['total_adesoes']?><b/></td>
                <td><b><?=$footer['total_ativos']?><b/></td>
                <td><b><?=$footer['total_este_mes']?><b/></td>
                <td><b><?=$footer['total_forecast']?><b/></td>
                <td><b><?=$footer['total_recebidos']?><b/></td>
                <td></td>
                <td><b>R$ <?=number_format($footer['total_valores_recebidos'], 2, ',', '.');?><b/></td>
                <td><b>R$ <?=number_format($footer['total_valores_proximo_ciclo'], 2, ',', '.');?><b/></td>
                <td><b>R$ <?=number_format($footer['total_valores_forecast'], 2, ',', '.');?><b/></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="10" class="paginacao">
                    <?=$paginator->numbers(array('separator' => ' ','data-bind' => 'click: loadThis')); ?>
                </td>
                <td colspan="2">
                    <?=$paginator->counter(array('format' => 'Total : %count% ' .  'Turmas')); ?>
                </td>
            </tr>
        </tfoot>
    </table>
<?php else : ?>
    <h2 class="fg-color-red">Nenhuma comissão a receber encontrada.</h2>
<?php endif; ?>
</div>