<?php

class UploadPagamentoShell extends Shell {

    var $uses = array(
        'Pagamento',
        'Usuario',
        'FormandoProfile',
        'DespesasPagamento',
        'Despesa',
        'TurmasUsuario',
        'Igpm',
        'Turma',
        'CursoTurma',
        'ViewFormandos',
        'UploadPagamento'
    );
    var $leitorExcel = null;
    var $pagamentos;
    var $arquivo = null;
    const BOLETO_PAGO_SUCESSO = 1;
    const BOLETO_PAGO_PARCIALMENTE = 2;
    const BOLETO_DUPLICADO = 3;
    const BOLETO_NAO_CADASTRADO = 4;
    const BOLETO_PERDIDO = 5;
    const BOLETO_ANTIGO_SUCESSO = 6;
    const BOLETO_ANTIGO_PERDIDO = 7;
    const CHEQUE_PAGO_COM_DESPESA = 8;
    const CHEQUE_PAGO_SEM_DESPESA = 9;
    const CHEQUE_PAGO_PERDIDO = 10;

    var $texto_status = array(
        self :: BOLETO_PAGO_SUCESSO => 'Pago com sucesso', //pagou a despesa com sucesso
        self :: BOLETO_PAGO_PARCIALMENTE => 'Pago parcialmente', //pagou somente parte da divida
        self :: BOLETO_DUPLICADO => 'Boleto já existente. Foi duplicado', //pagamento existia e já estava pago
        self :: BOLETO_NAO_CADASTRADO => 'Boleto n&atilde;o cadastrado', // (usuario encontrato, mas boleto nao cadastrado)
        self :: BOLETO_PERDIDO => 'Boleto sem usu&aacute;rio', //(sem usuario encontrado)
        self :: BOLETO_ANTIGO_SUCESSO => 'Boleto Antigo - Com usu&aacute;rio', //(encontrou usuario)
        self :: BOLETO_ANTIGO_PERDIDO => 'Boleto Antigo - Sem usu&aacute;rio', //(sem usuario encontrado)
        self :: CHEQUE_PAGO_COM_DESPESA => 'Cheque pagou despesa',
        self :: CHEQUE_PAGO_SEM_DESPESA => 'Cheque virou crédito',
        self :: CHEQUE_PAGO_PERDIDO => 'Cheque cadastrado. FORMANDO NAO ENCONTRADO.'
    );
    
    var $pagamentosProcessados;
    
    var $extensoesPermitidas = array(
        'xlsx' => 'PHPExcel_Reader_Excel2007',
        'xls' => 'PHPExcel_Reader_Excel5',
        'ods' => 'PHPExcel_Reader_OOCalc'
    );
    
    function criarPagamentos() {
        $this->pagamentosProcessados = array(
            'boleto' => array(
                'antigo' => array(),
                'novo' => array(),
                'nao_vinculados' => array(),
                'igpm' => array()
             ),
            'cheque' => array()
        );
        $this->pagamentos = array();
    }
    
    function executarAgendamento() {
        $upload = $this->UploadPagamento->find('all',array(
            'conditions' => array(
                'UploadPagamento.agendado' => 1,
                "UploadPagamento.data_fim is null"
            )
        ));
        if($upload) {
            foreach($upload as $u) {
                $this->args[0] = $u['UploadPagamento']['id'];
                $this->criarPagamentos();
                $this->processar();
                $this->UploadPagamento->id = $u['UploadPagamento']['id'];
                $this->UploadPagamento->saveField('agendado',0);
            }
            $caminho = APP."../cake/console/cake.php igpm atualizar_formandos";
            $comando = "php {$caminho} > /dev/null 2>&1 &";
            exec($comando);
        }
    }

    function processar() {
        if (!isset($this->args[0]))
            exit();
        $this->UploadPagamento->id = $this->args[0];
        $this->arquivo = $this->UploadPagamento->read();
        ini_set('memory_limit', '1024M');
        set_time_limit(0);
        if($this->validarArquivo()) {
            $this->obterPagamentosDoArquivo();
            $this->iniciarProcesso();
            $this->finalizarProcesso();
        } elseif($this->arquivo) {
            $this->UploadPagamento->save($this->arquivo);
        }
    }
    
    private function validarArquivo() {
        $retorno = false;
        if(!$this->arquivo)
            $this->arquivo['UploadPagamento']['erro'].= "Erro ao carregar registro de upload do banco de dados;";
        elseif(!$this->validarExtensao())
            $this->arquivo['UploadPagamento']['erro'].= "Arquivo nao permitido;";
        elseif(!$this->criarArquivoTemporario())
            $this->arquivo['UploadPagamento']['erro'].= "Erro ao carregar arquivo;";
        elseif(!$this->criarLeitorExcel())
            $this->arquivo['UploadPagamento']['erro'].= "Erro ao ler arquivo;";
        else
            $retorno = true;
        return $retorno;
    }
    
    private function validarExtensao() {
        $retorno = false;
        foreach($this->extensoesPermitidas as $ext => $obj)
            if($this->arquivo['UploadPagamento']['extensao'] == $ext)
                $retorno = true;
        return $retorno;
    }
    
    private function criarArquivoTemporario() {
        $this->arquivo['UploadPagamento']['tmp'] = $this->UploadPagamento->file_path().
                date('Y_m_d_H_i_s').'.'.$this->arquivo['UploadPagamento']['extensao'];
        $f = fopen($this->arquivo['UploadPagamento']['tmp'],'a');
        if(fwrite($f,$this->arquivo['UploadPagamento']['arquivo'])) {
            fclose($f);
            return true;
        } else {
            return false;
        }
    }
    
    private function apagarArquivoTemporario() {
        unlink($this->arquivo['UploadPagamento']['tmp']);
    }
    
    private function criarLeitorExcel() {
        try {
            App :: import('Vendor', 'PHPExcel', array(
                'file' => 'PHPExcel' . DS . 'IOFactory.php'
            ));
            $obj = new $this->extensoesPermitidas[$this->arquivo['UploadPagamento']['extensao']]();
            $obj->setReadDataOnly(true);
            $this->leitorExcel = $obj->load($this->arquivo['UploadPagamento']['tmp']);
            $this->leitorExcel->setActiveSheetIndex(0);
            return true;
        } catch(Exception $e) {
            return false;
        }
    }
    
    private function iniciarProcesso() {
        if(empty($this->arquivo['UploadPagamento']['data_inicio']))
            $this->arquivo['UploadPagamento']['data_inicio'] = date('Y-m-d H:i:s');
        if($this->arquivo['UploadPagamento']['total_registros'] == 0)
            $this->arquivo['UploadPagamento']['total_registros'] = count($this->pagamentos);
        $this->UploadPagamento->save($this->arquivo);
        foreach ($this->pagamentos as $pagamento) {
            if(preg_match("/boleto/i", ($pagamento['formaDePagamento']))) {
                $this->processarPagamentoDeBoleto($pagamento);
            } else if (preg_match("/cheque/i", ($pagamento['formaDePagamento'])) ||
                    preg_match("/dinheiro/i", ($pagamento['formaDePagamento']))) {
                $this->processarPagamentoDeCheque($pagamento);
            } else {
                debug("No Match:  " . $pagamento['formaDePagamento']);
            }
            $this->arquivo['UploadPagamento']['registros_processados']++;
            $this->arquivo['UploadPagamento']['data_ultima_atualizacao'] = date('Y-m-d H:i:s');
            $this->UploadPagamento->save($this->arquivo);
        }
    }
    
    private function finalizarProcesso() {
        $this->criarHtml();
        $this->apagarArquivoTemporario();
        $this->arquivo['UploadPagamento']['data_fim'] = date('Y-m-d H:i:s');
        $this->UploadPagamento->save($this->arquivo);
    }
    
    private function criarHtml() {
        $c = $this->UploadPagamento->createController();
        $c->webroot = '/';
        $c->layout = false;
        $c->viewVars['pagamentosProcessados'] = $this->pagamentosProcessados;
        $view = $c->render('/pagamentos/financeiro_processar_pagamentos');
        if(empty($this->arquivo['UploadPagamento']['arquivo_html']))
            $this->arquivo['UploadPagamento']['arquivo_html'] = 
                $this->UploadPagamento->file_path().date('Y_m_d_H_i_s').'.ctp';
        $f = fopen($this->arquivo['UploadPagamento']['arquivo_html'],'a');
        if(fwrite($f,$view) === FALSE)
            $this->arquivo['UploadPagamento']['erro'].= "Erro ao escrever arquivo html;";
    }

    private function obterPagamentosDoArquivo() {
        $linhaIterator = $this->leitorExcel->getActiveSheet()->
                getRowIterator(($this->arquivo['UploadPagamento']['registros_processados'] + 2));
        foreach ($linhaIterator as $linhaExcel) {
            if ($linhaExcel->getRowIndex() <= 1){
                continue;
            }
            $dados = $this->formatarPagamentoDoExcel($linhaExcel);
            if(!empty($dados['codigoDoFormando']))
                $this->pagamentos[] = $dados;
            else
                return false;
        }
    }

    private function formatarPagamentoDoExcel($linhaExcel) {
        $colunaIterator = $linhaExcel->getCellIterator();
        $colunaIterator->setIterateOnlyExistingCells(true);
        $pagamentoDaLinha = array('codigoDoFormando' => '', 'formaDePagamento' => '', 'nossoNumero' => 0,
                                  'dataDeEntrada' => NULL, 'observacao' => '', 'dataPagamento' => NULL,
                                  'valorPagoBase' => 0, 'multa' => 0, 'dataDeLiquidacao' => NULL);
        foreach ($colunaIterator as $colunaDaLinha) {
            switch ($colunaDaLinha->getColumn()) {
                case 'A' :
                    $pagamentoDaLinha['codigoDoFormando'] = str_pad($colunaDaLinha->getCalculatedValue(), 8, "0", STR_PAD_LEFT);
                    break;
                case 'B' :
                    $pagamentoDaLinha['formaDePagamento'] = $colunaDaLinha->getCalculatedValue();
                    break;
                case 'C' :
                    //$pagamentoDaLinha['nossoNumero'] = str_pad(substr($colunaDaLinha->getCalculatedValue(), -13), 13, "0", STR_PAD_LEFT);
                    $pagamentoDaLinha['nossoNumero'] = strlen($colunaDaLinha->getCalculatedValue()) > 8 ? str_pad(substr($colunaDaLinha->getCalculatedValue(), -13), 13, "0", STR_PAD_LEFT) : $colunaDaLinha->getCalculatedValue();
                    break;
                case 'D' :
                    $pagamentoDaLinha['dataDeEntrada'] = PHPExcel_Style_NumberFormat::toFormattedString($colunaDaLinha->getCalculatedValue(), 'YYYY-MM-DD');
                    break;
                case 'E' :
                    $pagamentoDaLinha['observacao'] = $colunaDaLinha->getCalculatedValue();
                case 'G' :
                    $pagamentoDaLinha['dataPagamento'] = PHPExcel_Style_NumberFormat::toFormattedString($colunaDaLinha->getCalculatedValue(), 'YYYY-MM-DD');
                    break;
                case 'H' :
                    $pagamentoDaLinha['valorPagoBase'] = $colunaDaLinha->getCalculatedValue();
                    break;
                case 'I' :
                    $pagamentoDaLinha['multa'] = $colunaDaLinha->getCalculatedValue();
                    break;
                case 'J' :
                    $valorDaCelula = $colunaDaLinha->getCalculatedValue();
                    if (empty($valorDaCelula) || $valorDaCelula == null || $valorDaCelula == '' || $valorDaCelula == "")
                        $pagamentoDaLinha['dataDeLiquidacao'] = NULL;
                    else
                        $pagamentoDaLinha['dataDeLiquidacao'] = PHPExcel_Style_NumberFormat::toFormattedString($colunaDaLinha->getCalculatedValue(), 'YYYY-MM-DD');
                    break;
                default:
                    break;
            }
            if(empty($pagamentoDaLinha['codigoDoFormando']))
                break;
        }

        $pagamentoDaLinha['valorPagoTotal'] = floatval($pagamentoDaLinha['valorPagoBase']) + floatval($pagamentoDaLinha['multa']);

        return $pagamentoDaLinha;
    }

    private function processarPagamentoDeBoleto($pagamentoDoBoleto) {
        if ($this->eBoletoIgpm($pagamentoDoBoleto))
            $this->processarPagamentoDeBoletoIgpm($pagamentoDoBoleto);
        else if ($this->eBoletoAntigo($pagamentoDoBoleto))
            $this->processarPagamentoDeBoletoAntigo($pagamentoDoBoleto);
        else
            $this->processarPagamentoDeBoletoNovo($pagamentoDoBoleto);
    }

    private function eBoletoIgpm($pagamentoExcel) {
        $retorno = false;
        $formando = $this->obterFormandoPorCodigo(substr($pagamentoExcel['codigoDoFormando'], -7));
        if (!empty($formando)) {
            $pagamentoBandoDeDados = $this->Pagamento->find('first', array(
                'conditions' => array(
                    'Pagamento.codigo' => $pagamentoExcel['nossoNumero'],
                    'Pagamento.tipo' => 'boleto_igpm',
                    'Pagamento.usuario_id' => $formando['ViewFormandos']['id']
                )
            ));
            if (!empty($pagamentoBandoDeDados)) {
                $retorno = true;
            } else {
                $despesaIgpm = $this->Despesa->find('first', array('conditions' => array(
                        'tipo' => 'igpm',
                        'status' => 'aberta',
                        'Despesa.usuario_id' => $formando['ViewFormandos']['id'])));
                if ($despesaIgpm) {
                    if ($despesaIgpm['Despesa']['valor'] == $pagamentoExcel['valorPagoBase'])
                        $retorno = true;
                }
                /*
                if (!$retorno) {
                    $despesas = $this->Despesa->find('all', array('conditions' =>
                        array(
                            'Despesa.usuario_id' => $formando['ViewFormandos']['id'],
                            'status' => 'paga',
                            "status_igpm <> 'pago'",
                            'tipo' => 'adesao',
                            'correcao_igpm > 0'
                    )));
                    foreach ($despesas as $despesa) {
                        if ($despesa['Despesa']['correcao_igpm'] == $pagamentoExcel['valorPagoBase']) {
                            $retorno = true;
                            continue;
                        }
                    }
                }
                 * 
                 */
            }
        }
        return $retorno;
    }

    private function eBoletoAntigo($pagamentoDoBoleto) {
        return ($pagamentoDoBoleto['nossoNumero'][1] >= 4);
    }

    private function processarPagamentoDeBoletoIgpm($pagamentoExcel) {
        $formando = $this->obterFormandoPorCodigo(substr($pagamentoExcel['codigoDoFormando'], -7));
        App::import('Component', 'Igpm');
        $igpm = new IgpmComponent();
        $pagamentoIgpm = $this->Pagamento->find('first', array(
            'conditions' => array(
                'Pagamento.codigo' => $pagamentoExcel['nossoNumero'],
                'Pagamento.tipo' => 'boleto_igpm',
                'Pagamento.usuario_id' => $formando['ViewFormandos']['id']
            )
        ));
        if (empty($pagamentoIgpm)) {
            $igpm->gerarParcela($formando['ViewFormandos'], $pagamentoExcel['valorPagoBase']);
            $pagamentoIgpm = $this->Pagamento->find('first', array('conditions' => array('Pagamento.id' => $this->Pagamento->getLastInsertId())));
        }
        $this->Pagamento->updateAll(
                array(
            'Pagamento.status' => "'pago'",
            'Pagamento.valor_pago' => $pagamentoExcel['valorPagoTotal'],
            'Pagamento.dt_liquidacao' => "'{$pagamentoExcel['dataDeLiquidacao']} " . date("H:i:s") . "'"), array('Pagamento.id' => $pagamentoIgpm['Pagamento']['id'])
        );
        $despesaIgpm = $this->Despesa->find('first', array('conditions' => array('Despesa.usuario_id' => $formando['ViewFormandos']['id'], 'tipo' => 'igpm')));
        if (empty($despesaIgpm)) {
            if ($igpm->gerarDespesa($formando['ViewFormandos']))
                $this->Despesa->read(null, $this->Despesa->getLastInsertId());
            else
                return false;
        } else {
            $igpm->atualizarDespesa($formando['ViewFormandos']);
            $this->Despesa->read(null, $despesaIgpm['Despesa']['id']);
        }
        $pagamentoExcel['despesa']['id'] = $this->Despesa->id;
        $pagamentoExcel['texto_status'] = $this->texto_status[self :: BOLETO_PAGO_SUCESSO];
        $this->pagamentosProcessados['boleto']['igpm'][] = $pagamentoExcel;
        if ($despesaIgpm['Despesa']['valor'] >= $igpm->obterTotalPago($formando['ViewFormandos'])) {
            $this->Despesa->set('status', 'paga');
            $this->Despesa->save();
            $this->Despesa->id = null;
        }
    }

    private function processarPagamentoDeBoletoAntigo($pagamentoDoBoleto) {
        $codigoFormando = substr($pagamentoDoBoleto['codigoDoFormando'], 5, 3);
        $despesasNaoPagas = $this->obterDespesasNaoPagasDoFormandoOrdenadasPorVencimento($pagamentoDoBoleto);
        if (!$this->pagouComBoletoAntigoPrimeiraDespesaComValorDistanteDeQuatroDoPagamento($despesasNaoPagas, $pagamentoDoBoleto))
            $this->salvarPagamentoDeBoletoAntigoNoBanco($pagamentoDoBoleto);
    }

    private function pagouComBoletoAntigoPrimeiraDespesaComValorDistanteDeQuatroDoPagamento($despesasNaoPagasOrdenadasPorVencimento, $pagamentoASerProcessado) {
        $despesaDistanteDeQuatroDoPagamento = $this->obterDespesaDistanteDeQuatroDoPagamento($despesasNaoPagasOrdenadasPorVencimento, $pagamentoASerProcessado);
        if ($despesaDistanteDeQuatroDoPagamento == null)
            return false;
        $this->gravarPagamentoDeDespesaPorBoletoAntigo($despesaDistanteDeQuatroDoPagamento, $pagamentoASerProcessado);

        return true;
    }

    private function obterDespesaDistanteDeQuatroDoPagamento($despesasNaoPagasOrdenadasPorVencimento, $pagamentoASerProcessado) {
        $idDaTurma = intval(substr($pagamentoASerProcessado['codigoDoFormando'], 0, 5));
        $ordemDoFormando = intval(substr($pagamentoASerProcessado['codigoDoFormando'], 5, 3));
        $formandoDaDespesa = $this->obterFormandoPorTurmaECodigo($idDaTurma, $ordemDoFormando);

        $despesaDistanteDeQuatroDoPagamento = null;
        ////debug($despesasNaoPagasOrdenadasPorVencimento);
        for ($indiceDaDespesa = 0; $despesaDistanteDeQuatroDoPagamento == null && ($indiceDaDespesa < count($despesasNaoPagasOrdenadasPorVencimento)); $indiceDaDespesa++) {
            $despesaAtual = $despesasNaoPagasOrdenadasPorVencimento[$indiceDaDespesa];
            $pagamentoAtualDaDespesa = (count($despesasNaoPagasOrdenadasPorVencimento[$indiceDaDespesa]['DespesaPagamento']) > 0) ? $despesasNaoPagasOrdenadasPorVencimento[$indiceDaDespesa]['DespesaPagamento'][0] : null;
            $multaRealDaDespesa = $this->calcularMultaDeDespesaAPartirDaDataDePagamento($despesaAtual['Despesa']['valor'], $despesaAtual['Despesa']['data_vencimento'], $pagamentoASerProcessado['dataDeLiquidacao']);
            $IGPMRealDaDespesa = $despesasNaoPagasOrdenadasPorVencimento[$indiceDaDespesa]['Despesa']['correcao_igpm'] - $this->calcular_desconto_igpm($idDaTurma, $pagamentoASerProcessado['dataDeLiquidacao'], $despesasNaoPagasOrdenadasPorVencimento[$indiceDaDespesa]['Despesa']['valor']);
            $valorPago = floatval($pagamentoASerProcessado['valorPagoTotal']);

            $valorDespesaSemIGPM = $despesaAtual['Despesa']['valor'] + $multaRealDaDespesa;
            $valorDespesaComIGPM = $valorDespesaSemIGPM + $IGPMRealDaDespesa;
            ////debug("$valorPago # $valorDespesaSemIGPM");

            $despesaAtual['Despesa']['status'] = 'paga';
            $despesaAtual['Despesa']["data_pagamento"] = $pagamentoASerProcessado['dataDeLiquidacao'];

            if (abs($valorPago - $valorDespesaSemIGPM) < 4) {

                if (abs($valorPago - $valorDespesaComIGPM) < 4 && $despesaAtual['Despesa']['status_igpm'] != 'nao_virado')
                    $despesaAtual['Despesa']['status_igpm'] = 'pago';

                $despesaDistanteDeQuatroDoPagamento = $despesaAtual;
            } else if (abs($valorPago + $multaRealDaDespesa - $valorDespesaSemIGPM) < 4) {

                $despesaAtual['Despesa']['multa'] = 0;

                if (abs($valorPago + $multaRealDaDespesa - $valorDespesaComIGPM) < 4 && $despesaAtual['Despesa']['status_igpm'] != 'nao_virado')
                    $despesaAtual['Despesa']['status_igpm'] = 'pago';

                $despesaDistanteDeQuatroDoPagamento = $despesaAtual;
            }
        }

        return $despesaDistanteDeQuatroDoPagamento;
    }

    private function gravarPagamentoDeDespesaPorBoletoAntigo($despesaASerPaga, $pagamentoASerProcessado) {
        //debug("PAGAMENTO boleto antigo - tem usuario - tem despesa");
        //if ( $valorPgto - $valorDespesa < 3.6) $despesa.Valor = $pagamento.valor;
        $codBoleto = $pagamentoASerProcessado['nossoNumero'];


        $dadosDoPagamentoASerSalvo = array(
            'tipo' => $this->obterTipoDoBoleto($pagamentoASerProcessado['nossoNumero']),
            'usuario_id' => $despesaASerPaga['Despesa']['usuario_id'],
            'codigo' => $pagamentoASerProcessado['nossoNumero'],
            'dt_cadastro' => $pagamentoASerProcessado['dataPagamento'],
            'dt_vencimento' => null,
            'dt_liquidacao' => $pagamentoASerProcessado['dataDeLiquidacao'],
            'status' => Pagamento :: STATUS_PAGO,
            'dt_transferencia' => null,
            'linha_digitavel' => null,
            'valor_pago' => $pagamentoASerProcessado['valorPagoTotal'],
            'valor_nominal' => $pagamentoASerProcessado['valorPagoBase'],
            'observacao' => $pagamentoASerProcessado['observacao']
        );
        $pagamentoASerSalvo = array();

        $valor_despesa = $despesaASerPaga['Despesa']['valor'];
        $valor_pagamento = $pagamentoASerProcessado['valorPagoTotal'];
        $diferenca = $valor_pagamento - $valor_despesa;
        if ($diferenca >= 2.9 && $diferenca <= 3.7) {
            //debug("Colocando valor de boleto na despesa! $diferenca #" . $despesaASerPaga['Despesa']['usuario_id'] . "#" . $pagamentoASerProcessado['valorPagoTotal']); 

            $valor_despesa = $valor_pagamento;
        }

        if (count($despesaASerPaga['DespesaPagamento']) > 0) {
            //debug($despesaASerPaga);
            $dadosDoPagamentoASerSalvo['id'] = $despesaASerPaga['DespesaPagamento'][0]['Pagamento']['id'];
        } else {
            $dadosDaDespesaPagamentoASerSalva = array(
                'despesa_id' => $despesaASerPaga['Despesa']['id'],
                'valor' => $valor_despesa,
                'multa' => floatval($despesaASerPaga['Despesa']['multa']),
                'correcao_igpm' => floatval($despesaASerPaga['Despesa']['correcao_igpm'])
            );

            $pagamentoASerSalvo['DespesaPagamento'] = array($dadosDaDespesaPagamentoASerSalva);
        }

        $pagamentoASerSalvo['Pagamento'] = $dadosDoPagamentoASerSalvo;

        unset($despesaASerPaga['DespesaPagamento']);

        $this->Pagamento->bindModel(array('hasMany' => array('DespesaPagamento' =>
                array('foreignKey' => 'pagamento_id'))));

        $this->Despesa->begin();
        $this->Pagamento->saveAll($pagamentoASerSalvo);
        $this->Despesa->saveAll($despesaASerPaga);
        $this->Despesa->commit();

        $pagamentoASerProcessado['despesa'] = $despesaASerPaga['Despesa'];

        $pagamentoASerProcessado['texto_status'] = $this->texto_status[self :: BOLETO_PAGO_SUCESSO];
        $this->pagamentosProcessados['boleto']['antigo'][] = $pagamentoASerProcessado;
    }

    private function salvarPagamentoDeBoletoAntigoNoBanco($pagamentoASerProcessado) {
        $idDaTurma = intval(substr($pagamentoASerProcessado['codigoDoFormando'], 0, 5));
        $ordemDoFormando = intval(substr($pagamentoASerProcessado['codigoDoFormando'], 5, 3));
        $formandoDoPagamento = $this->obterFormandoPorTurmaECodigo($idDaTurma, $ordemDoFormando);

        // Obter a data de vencimento a partir da data do excel
        $dataExcel = substr($pagamentoASerProcessado['nossoNumero'], 1, 5);
        if ($dataExcel != '') {
            $timestamp = ($dataExcel - 25568) * 86400;
            $dataVencimento = date('Y-m-d', $timestamp);
        } else {
            $dataVencimento = null;
        }

        if ($formandoDoPagamento == null) {
            //debug("PAGAMENTO boleto antigo - nao tem usuario - nao tem despesa");

            $dadosDoPagamentoASerSalvo = array(
                'tipo' => $this->obterTipoDoBoleto($pagamentoASerProcessado['nossoNumero']),
                'usuario_id' => $formandoDoPagamento['Usuario']['id'],
                'codigo' => $pagamentoASerProcessado['nossoNumero'],
                'dt_cadastro' => $pagamentoASerProcessado['dataPagamento'],
                'dt_vencimento' => $dataVencimento,
                'dt_liquidacao' => $pagamentoASerProcessado['dataDeLiquidacao'],
                'status' => Pagamento :: STATUS_PAGO,
                'dt_transferencia' => null,
                'linha_digitavel' => null,
                'valor_pago' => $pagamentoASerProcessado['valorPagoTotal'],
                'valor_nominal' => $pagamentoASerProcessado['valorPagoBase'],
                'observacao' => $pagamentoASerProcessado['observacao'],
                'perdido' => true
            );
            $pagamentoASerProcessado['texto_status'] = $this->texto_status[self :: BOLETO_PERDIDO];
        } else {
            //debug("PAGAMENTO boleto antigo - tem usuario - nao tem despesa");
            $dadosDoPagamentoASerSalvo = array(
                'tipo' => $this->obterTipoDoBoleto($pagamentoASerProcessado['nossoNumero']),
                'usuario_id' => $formandoDoPagamento['Usuario']['id'],
                'codigo' => $pagamentoASerProcessado['nossoNumero'],
                'dt_cadastro' => $pagamentoASerProcessado['dataPagamento'],
                'dt_vencimento' => $dataVencimento,
                'dt_liquidacao' => $pagamentoASerProcessado['dataDeLiquidacao'],
                'status' => Pagamento :: STATUS_PAGO,
                'dt_transferencia' => null,
                'linha_digitavel' => null,
                'valor_pago' => $pagamentoASerProcessado['valorPagoTotal'],
                'valor_nominal' => $pagamentoASerProcessado['valorPagoBase'],
                'observacao' => $pagamentoASerProcessado['observacao']
            );

            $pagamentoASerProcessado['texto_status'] = $this->texto_status[self :: BOLETO_PAGO_SUCESSO];
        }

        //debug($pagamentoASerProcessado);
        $this->Pagamento->begin();
        $this->Pagamento->saveAll(array('Pagamento' => $dadosDoPagamentoASerSalvo));
        $this->Pagamento->commit();
        $this->pagamentosProcessados['boleto']['antigo'][] = $pagamentoASerProcessado;
    }

    private function obterFormandoPorTurmaECodigo($turmaId, $codigoFormando) {

        // Buscar curso_turma_id pertencente à turma

        $cursos_turmas_ids = $this->CursoTurma->find('list', array('conditions' => array('turma_id' => $turmaId)));


        $this->FormandoProfile->recursive = 0;
        $formandoDoBoletoAntigo = $this->Usuario->FormandoProfile->find('first', array(
            'conditions' => array(
                'curso_turma_id' => $cursos_turmas_ids,
                'codigo_formando' => $codigoFormando
            )
        ));


        return $formandoDoBoletoAntigo;
    }

    private function obterTipoDoBoleto($nossoNumero) {
        if ($nossoNumero[0] == 0)
            return Pagamento :: TIPO_BOLETO_ANTIGO_CONTRATO;
        else
        if ($nossoNumero[0] >= 1 AND $nossoNumero[0] <= 8)
            return Pagamento :: TIPO_BOLETO_ANTIGO_EXTRA_BAILE;
        else
            return Pagamento :: TIPO_BOLETO_ANTIGO_EXTRA_EVENTOS;
    }

    private function processarPagamentoDeBoletoNovo($pagamentoDoBoletoDaPlanilha) {
        $pagamentoDoBancoDeDadosCorrespondenteAoBoleto = $this->Pagamento->find('first', array(
            'conditions' => array(
                'Pagamento.codigo' => $pagamentoDoBoletoDaPlanilha['nossoNumero'],
                'Pagamento.tipo' => array(
                    Pagamento :: TIPO_BOLETO,
                    Pagamento :: TIPO_BOLETO_ANTIGO_CONTRATO,
                    Pagamento :: TIPO_BOLETO_ANTIGO_EXTRA_BAILE,
                    Pagamento :: TIPO_BOLETO_ANTIGO_EXTRA_EVENTOS,
                    Pagamento :: TIPO_BOLETO_IGPM
                )
            )
        ));

        if ($pagamentoDoBancoDeDadosCorrespondenteAoBoleto != null)
            $this->processarBoletoNovoComPagamentoExistente($pagamentoDoBancoDeDadosCorrespondenteAoBoleto, $pagamentoDoBoletoDaPlanilha);
        else
            $this->processarBoletoNovoComPagamentoNaoExistente($pagamentoDoBoletoDaPlanilha);
    }

    private function processarBoletoNovoComPagamentoExistente($pagamentoDoBancoDeDadosCorrespondenteAoBoleto, $pagamentoDoBoletoDaPlanilha) {
        if ($pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Pagamento']['status'] != 'pago')
            $this->processarBoletoNovoComPagamentoExistenteNaoLiquidado($pagamentoDoBancoDeDadosCorrespondenteAoBoleto, $pagamentoDoBoletoDaPlanilha);
        else
            $this->processarBoletoNovoComPagamentoExistenteLiquidado($pagamentoDoBancoDeDadosCorrespondenteAoBoleto, $pagamentoDoBoletoDaPlanilha);
    }

    private function processarBoletoNovoComPagamentoExistenteNaoLiquidado($pagamentoDoBancoDeDadosCorrespondenteAoBoleto, $pagamentoDoBoletoDaPlanilha) {
        //debug("pagamento boleto novo - pagamento existente - pagamento nao liquidado");

        $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Pagamento']['dt_liquidacao'] = $pagamentoDoBoletoDaPlanilha['dataDeLiquidacao'];
        $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Pagamento']['status'] = Pagamento :: STATUS_PAGO;
        $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Pagamento']['valor_pago'] = $pagamentoDoBoletoDaPlanilha['valorPagoTotal'];
        $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Pagamento']['observacao'] = $pagamentoDoBoletoDaPlanilha['observacao'];

        $despesaDoPagamento = $this->obterDespesaDoPagamento($pagamentoDoBancoDeDadosCorrespondenteAoBoleto);
        $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Despesa'] = $despesaDoPagamento['Despesa'];

        $turmaId = intval(substr($pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Pagamento']['codigo'], 1, 5));

        $multaReal = $this->calcularMultaDeDespesaAPartirDaDataDePagamento($pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Despesa']['valor'], $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Despesa']['data_vencimento'], $pagamentoDoBoletoDaPlanilha['dataDeLiquidacao']);
        $valorIgpmReal = $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Despesa']['correcao_igpm'] - $this->calcular_desconto_igpm($turmaId, $pagamentoDoBoletoDaPlanilha['dataDeLiquidacao'], $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Despesa']['valor']);

        $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Despesa']['multa'] = $multaReal;
        $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Despesa']['correcao_igmp'] = $valorIgpmReal;

        $this->salvarPagamentoNoBanco($pagamentoDoBancoDeDadosCorrespondenteAoBoleto, $pagamentoDoBoletoDaPlanilha);
    }

    private function obterDespesaDoPagamento($pagamentoDoBancoDeDadosCorrespondenteAoBoleto) {
        $this->DespesasPagamento->recursive = 1;
        $this->DespesasPagamento->bindModel(array(
            'belongsTo' => array(
                'Despesa',
                'Pagamento'
            )
                ), false);

        $despesa = $this->DespesasPagamento->find('first', array(
            'contains' => array(
                'Despesa'
            ),
            'conditions' => array(
                'pagamento_id' => $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Pagamento']['id']
            )
        ));

        return $despesa;
    }

    private function calcularMultaDeDespesaAPartirDaDataDePagamento($valorBase, $dataVencimento, $dataPagamento) {

        if (strtotime($dataVencimento) >= strtotime($dataPagamento) || true) {
            return 0;
        } else {
            $dias_vencidos = (strtotime($dataPagamento) - strtotime($dataVencimento)) / 60 / 60 / 24;

            $multaReal = (Despesa :: MULTA_PERCENTUAL / 100) * floatval($valorBase);
            $multaReal += $dias_vencidos * Despesa :: MULTA_POR_DIA;
            $multaReal = round($multaReal, 2);

            return $multaReal;
        }
    }

    private function salvarPagamentoNoBanco($pagamentoDoBancoDeDadosCorrespondenteAoBoleto, $pagamentoDoBoletoDaPlanilha) {

        $despesaTotalSemIGPM = $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Despesa']['valor'] + $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Despesa']['multa'];


        $despesaTotalComIGPM = $despesaTotalSemIGPM + $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Despesa']['correcao_igpm'];
        ////debug($pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Pagamento']['valor_pago']);
        ////debug($despesaTotalSemIGPM);
        if ($pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Pagamento']['valor_pago'] > $despesaTotalSemIGPM - 0.02) {
            $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Despesa']['status'] = 'paga';

            if ($pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Pagamento']['valor_pago'] > $despesaTotalComIGPM - 0.02/* || $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Despesa']['status_igpm'] == 'nao_pago' */) {
                $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Despesa']['status_igpm'] = 'pago';
            }

            $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Despesa']['data_pagamento'] = $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Pagamento']['dt_liquidacao'];

            $pagamentoDoBoletoDaPlanilha['status'] = self :: BOLETO_PAGO_SUCESSO;
            $pagamentoDoBoletoDaPlanilha['texto_status'] = $this->texto_status[self :: BOLETO_PAGO_SUCESSO];
        } else if ($pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Pagamento']['valor_pago'] > $despesaTotalSemIGPM - 0.02 - $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Despesa']['multa']) {
            // PAGAR A DESPESA MESMO SE ESTIVER FALTANDO A MULTA
            // Rachid disse que se a multa não foi paga para o banco, não tem como cobrar, e também
            // que podemos assumir que o banco cobra direitinho esse valor

            $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Despesa']['status'] = 'paga';

            $valor_multa = 0;
            if ($pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Pagamento']['valor_pago'] > $despesaTotalComIGPM - 0.02/* || $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Despesa']['status_igpm'] == 'nao_pago' */) {
                $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Despesa']['status_igpm'] = 'pago';
                // multa = valor_pago - valor_despesa - valor_igpm
                $valor_multa = $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Pagamento']['valor_pago'];
                $valor_multa -= $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Despesa']['valor'];
                $valor_multa -= $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Despesa']['correcao_igpm'];
            } else {
                // multa = valor_pago - valor_despesa;
                $valor_multa = $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Pagamento']['valor_pago'];
                $valor_multa -= $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Despesa']['valor'];
            }

            if ($valor_multa < 0)
                $valor_multa = 0;

            $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Despesa']['multa'] = $valor_multa;
            $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Despesa']['data_pagamento'] = $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Pagamento']['dt_liquidacao'];


            $pagamentoDoBoletoDaPlanilha['status'] = self :: BOLETO_PAGO_SUCESSO;
            $pagamentoDoBoletoDaPlanilha['texto_status'] = $this->texto_status[self :: BOLETO_PAGO_SUCESSO];
        } else {
            $pagamentoDoBoletoDaPlanilha['status'] = self :: BOLETO_PAGO_PARCIALMENTE;
            $pagamentoDoBoletoDaPlanilha['texto_status'] = $this->texto_status[self :: BOLETO_PAGO_PARCIALMENTE];
        }
        $this->Pagamento->begin();
        $this->Despesa->saveAll(array('Despesa' => $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Despesa']));
        $this->Pagamento->saveAll($pagamentoDoBancoDeDadosCorrespondenteAoBoleto);
        $this->Pagamento->commit();
        $pagamentoDoBoletoDaPlanilha['despesa'] = $pagamentoDoBancoDeDadosCorrespondenteAoBoleto['Despesa'];
        $this->pagamentosProcessados['boleto']['novo'][] = $pagamentoDoBoletoDaPlanilha;
    }

    private function processarBoletoNovoComPagamentoExistenteLiquidado($pagamentoDoBancoDeDadosCorrespondenteAoBoleto, $pagamentoDoBoletoDaPlanilha) {
        //debug("pagamento boleto novo - pagamento existente - pagamento liquidado");
        $dadosDoPagamento = $pagamentoDoBancoDeDadosCorrespondenteAoBoleto;
        $dadosDoPagamento['Pagamento']['data_cadastro'] = date('Y-m-d h:i:s', strtotime('now'));
        $dadosDoPagamento['Pagamento']['dt_liquidacao'] = $pagamentoDoBoletoDaPlanilha['dataDeLiquidacao'];
        $dadosDoPagamento['Pagamento']['tipo'] = Pagamento :: TIPO_BOLETO_DUPLICADO;
        $dadosDoPagamento['Pagamento']['status'] = Pagamento :: STATUS_PAGO;
        $dadosDoPagamento['Pagamento']['valor_pago'] = $pagamentoDoBoletoDaPlanilha['valorPagoTotal'];
        $dadosDoPagamento['Pagamento']['duplicado'] = true;
        $dadosDoPagamento['Pagamento']['observacao'] = $pagamentoDoBoletoDaPlanilha['observacao'];

        unset($dadosDoPagamento['Pagamento']['id']);
        unset($dadosDoPagamento['DespesaPagamento']);

        $this->Pagamento->create();
        $this->Pagamento->save($dadosDoPagamento);

        $pagamentoDoBoletoDaPlanilha['status'] = self :: BOLETO_DUPLICADO;
        $pagamentoDoBoletoDaPlanilha['texto_status'] = $this->texto_status[self :: BOLETO_DUPLICADO];

        $this->pagamentosProcessados['boleto']['novo'][] = $pagamentoDoBoletoDaPlanilha;
    }

    private function processarBoletoNovoComPagamentoNaoExistente($pagamentoDoBoletoDaPlanilha) {
        $pagamentoDoBoletoDaPlanilha['status'] = self :: BOLETO_NAO_CADASTRADO;
        $pagamentoDoBoletoDaPlanilha['texto_status'] = $this->texto_status[self :: BOLETO_NAO_CADASTRADO];

        $formandoDoBoletoNovo = $this->obterFormandoAPartirDoBoletoNovo($pagamentoDoBoletoDaPlanilha);

        $dadosDoPagamentoASerSalvo = array(
            'tipo' => Pagamento :: TIPO_BOLETO_NAO_CADASTRADO,
            'usuario_id' => null,
            //'codigo' => str_pad($pagamentoDoBoletoDaPlanilha['nossoNumero'], 13, "0", STR_PAD_LEFT),
            'codigo' => $pagamentoDoBoletoDaPlanilha['nossoNumero'],
            'dt_cadastro' => date('Y-m-d H:i:s', strtotime('now')),
            'dt_vencimento' => null,
            'dt_liquidacao' => $pagamentoDoBoletoDaPlanilha['dataDeLiquidacao'],
            'status' => Pagamento :: STATUS_PAGO,
            'dt_transferencia' => null,
            'linha_digitavel' => null,
            'observacao' => $pagamentoDoBoletoDaPlanilha['observacao'],
            'valor_pago' => $pagamentoDoBoletoDaPlanilha['valorPagoTotal'],
            'valor_nominal' => $pagamentoDoBoletoDaPlanilha['valorPagoBase']
        );

        if ($formandoDoBoletoNovo != null) {
            //debug("BOLETO NOVO - NAO ACHOU PAGAMENTO - ACHOU USUARIO");
            $dadosDoPagamentoASerSalvo['perdido'] = false;
            $pagamentoDoBoletoDaPlanilha['status'] = self :: BOLETO_NAO_CADASTRADO;
            $dadosDoPagamentoASerSalvo['usuario_id'] = $formandoDoBoletoNovo['Usuario']['id'];
            $atendenteResponsavel = $this->Usuario->obterAtendenteResponsavel($formandoDoBoletoNovo['Usuario']['id']);
            if($atendenteResponsavel)
                $pagamentoDoBoletoDaPlanilha['link'] = $this->Usuario->urlAcessoDeslogado($atendenteResponsavel['Usuario']['id']) .
                    "/" . base64_encode(Configure::read('url_site') .
                    "/atendimento/area_financeira/dados/{$formandoDoBoletoNovo['Usuario']['id']}") . "/1";
            $this->Pagamento->create();
            $this->Pagamento->save($dadosDoPagamentoASerSalvo);
            $this->pagamentosProcessados['boleto']['nao_vinculados'][] = $pagamentoDoBoletoDaPlanilha;
        } else {
            //debug("BOLETO NOVO - NAO ACHOU PAGAMENTO - NAO ACHOU USUARIO");
            $dadosDoPagamentoASerSalvo['perdido'] = true;
            $pagamentoDoBoletoDaPlanilha['status'] = self :: BOLETO_PERDIDO;
            $pagamentoDoBoletoDaPlanilha['texto_status'] = $this->texto_status[self :: BOLETO_PERDIDO];
            $this->Pagamento->create();
            $this->Pagamento->save($dadosDoPagamentoASerSalvo);
            $this->pagamentosProcessados['boleto']['novo'][] = $pagamentoDoBoletoDaPlanilha;
        }
    }

    private function obterFormandoAPartirDoBoletoNovo($pagamentoDoBoletoDaPlanilha) {
        $nossoNumero = $pagamentoDoBoletoDaPlanilha['nossoNumero'];
        if(strlen($nossoNumero) > 8) {
            if(substr($nossoNumero,0,6) == str_pad('9', 6, '9', STR_PAD_LEFT)) {
                $usuarioId = substr($nossoNumero,6,4);
                return $this->Usuario->read(null,$usuarioId);
            } else {
                $turmaId = intval(substr($nossoNumero, 1, 5));
                $codigo_formando = substr($nossoNumero, 6, 3);
                return $this->obterFormandoPorTurmaECodigo($turmaId, $codigo_formando);
            }
        } else {
            $usuarioId= substr($nossoNumero,0,5);
            return $this->Usuario->read(null,$usuarioId);
        }
    }

    private function calcular_desconto_igpm($turma_id, $data_liquidacao, $valor_despesa) {
        $desconto_igpm = 0;
        return $desconto_igpm;
    }

    private function processarPagamentoDeCheque($pagamentoASerProcessado) {
        $codigoFormando = substr($pagamentoASerProcessado['codigoDoFormando'], 5, 3);
        $despesasNaoPagas = $this->obterDespesasNaoPagasDoFormandoOrdenadasPorVencimento($pagamentoASerProcessado);
        if (!$this->pagouComChequePrimeiraDespesaComValorDistanteDeQuatroDoPagamento($despesasNaoPagas, $pagamentoASerProcessado))
            $this->salvarPagamentoDeChequeNoBanco($pagamentoASerProcessado);
    }

    private function obterDespesasNaoPagasDoFormandoOrdenadasPorVencimento($pagamentoASerProcessado) {
        $idDaTurma = intval(substr($pagamentoASerProcessado['codigoDoFormando'], 0, 5));
        $ordemDoFormando = intval(substr($pagamentoASerProcessado['codigoDoFormando'], 5, 3));
        $formandoDaDespesa = $this->obterFormandoPorTurmaECodigo($idDaTurma, $ordemDoFormando);

        $this->Pagamento->bindModel(array('hasMany' => array('DespesaPagamento' =>
                array('foreignKey' => 'pagamento_id'))));

        $this->Despesa->bindModel(array('hasMany' => array('DespesaPagamento' =>
                array('foreignKey' => 'despesa_id'))));

        $this->Despesa->recursive = 2;

        if (preg_match("/contrato/i", ($pagamentoASerProcessado['formaDePagamento'])))
            $despesasDoUsuarioOrdenadaPorVencimento = $this->Despesa->find('all', array('order' => 'Despesa.data_vencimento ASC', 'conditions' => array('Despesa.tipo' => 'adesao', 'Despesa.usuario_id' => $formandoDaDespesa['FormandoProfile']['usuario_id'], 'NOT' => array('Despesa.status' => array('cancelada', 'paga')))));
        else
            $despesasDoUsuarioOrdenadaPorVencimento = $this->Despesa->find('all', array('order' => 'Despesa.data_vencimento ASC', 'conditions' => array('Despesa.usuario_id' => $formandoDaDespesa['FormandoProfile']['usuario_id'], 'NOT' => array('Despesa.status' => array('cancelada', 'paga')))));

        return $despesasDoUsuarioOrdenadaPorVencimento;
    }

    private function obterDespesaSemMultaComValorDistanteDeQuatroDoPagamento($despesasNaoPagasOrdenadasPorVencimento, $pagamentoASerProcessado) {
        $idDaTurma = intval(substr($pagamentoASerProcessado['codigoDoFormando'], 0, 5));
        $ordemDoFormando = intval(substr($pagamentoASerProcessado['codigoDoFormando'], 5, 3));
        $formandoDaDespesa = $this->obterFormandoPorTurmaECodigo($idDaTurma, $ordemDoFormando);

        $despesaDistanteDeQuatroDoPagamento = null;
        for ($indiceDaDespesa = 0; $despesaDistanteDeQuatroDoPagamento == null && ($indiceDaDespesa < count($despesasNaoPagasOrdenadasPorVencimento)); $indiceDaDespesa++) {
            $despesaAtual = $despesasNaoPagasOrdenadasPorVencimento[$indiceDaDespesa];
            $pagamentoAtualDaDespesa = (count($despesasNaoPagasOrdenadasPorVencimento[$indiceDaDespesa]['DespesaPagamento']) > 0) ? $despesasNaoPagasOrdenadasPorVencimento[$indiceDaDespesa]['DespesaPagamento'][0] : null;
            $multaRealDaDespesa = $this->calcularMultaDeDespesaAPartirDaDataDePagamento($despesasNaoPagasOrdenadasPorVencimento[$indiceDaDespesa]['Despesa']['valor'], $despesasNaoPagasOrdenadasPorVencimento[$indiceDaDespesa]['Despesa']['data_vencimento'], $pagamentoASerProcessado['dataDeLiquidacao']);
            $valorIgpmReal = $despesasNaoPagasOrdenadasPorVencimento[$indiceDaDespesa]['Despesa']['correcao_igpm'] - $this->calcular_desconto_igpm($idDaTurma, $pagamentoASerProcessado['dataDeLiquidacao'], $despesasNaoPagasOrdenadasPorVencimento[$indiceDaDespesa]['Despesa']['valor']);

            $valorPago = floatval($pagamentoASerProcessado['valorPagoBase']);

            //ATENCAO: nao considerando multa ( a pedido do Rachid)
            $valorDespesaSemIGPM = $despesaAtual['Despesa']['valor'];
            $valorDespesaComIGPM = $valorDespesaSemIGPM + $valorIgpmReal;


            if (abs($valorPago - $valorDespesaSemIGPM) < 4) {
                $despesaAtual['Despesa']['status'] = 'paga';
                $despesaAtual['Despesa']['multa'] = 0;
                if (abs($valorPago - $valorDespesaComIGPM) < 4 && $despesaAtual['Despesa']['status_igpm'] != 'nao_virado')
                    $despesaAtual['Despesa']['status_igpm'] = 'pago';

                $despesaAtual['Despesa']['data_pagamento'] = $pagamentoASerProcessado['dataPagamento'];
                $despesaDistanteDeQuatroDoPagamento = $despesaAtual;
            }
        }

        return $despesaDistanteDeQuatroDoPagamento;
    }

    private function pagouComChequePrimeiraDespesaComValorDistanteDeQuatroDoPagamento($despesasNaoPagasOrdenadasPorVencimento, $pagamentoASerProcessado) {
        $despesaDistanteDeQuatroDoPagamento = $this->obterDespesaSemMultaComValorDistanteDeQuatroDoPagamento($despesasNaoPagasOrdenadasPorVencimento, $pagamentoASerProcessado);

        if ($despesaDistanteDeQuatroDoPagamento == null)
            return false;

        $this->gravarPagamentoDeDespesaPorCheque($despesaDistanteDeQuatroDoPagamento, $pagamentoASerProcessado);

        return true;
    }

    private function gravarPagamentoDeDespesaPorCheque($despesaASerPaga, $pagamentoASerProcessado) {
        //debug("PAGAMENTO CHEQUE - tem despesa");
        $dadosDoPagamentoASerSalvo = array(
            'tipo' => ($despesaASerPaga['Despesa']['tipo'] == 'adesao') ? Pagamento :: TIPO_CHEQUE_CONTRATO : Pagamento :: TIPO_CHEQUE_EXTRA,
            'usuario_id' => $despesaASerPaga['Despesa']['usuario_id'],
            'codigo' => '',
            'dt_cadastro' => $pagamentoASerProcessado['dataPagamento'],
            'dt_vencimento' => null,
            'dt_liquidacao' => $pagamentoASerProcessado['dataDeLiquidacao'],
            'status' => Pagamento :: STATUS_PAGO,
            'dt_transferencia' => null,
            'linha_digitavel' => null,
            'valor_pago' => $pagamentoASerProcessado['valorPagoTotal'],
            'valor_nominal' => $pagamentoASerProcessado['valorPagoBase'],
            'observacao' => $pagamentoASerProcessado['observacao']
        );

        $despesaASerPaga['Despesa']['multa'] = $pagamentoASerProcessado['multa'];

        $pagamentoASerSalvo = array();

        if (count($despesaASerPaga['DespesaPagamento']) > 0) {
            $dadosDoPagamentoASerSalvo['id'] = $despesaASerPaga['DespesaPagamento'][0]['Pagamento']['id'];
        } else {
            $dadosDaDespesaPagamentoASerSalva = array(
                'despesa_id' => $despesaASerPaga['Despesa']['id'],
                'valor' => $despesaASerPaga['Despesa']['valor'],
                'multa' => floatval($despesaASerPaga['Despesa']['multa']),
                'correcao_igpm' => floatval($despesaASerPaga['Despesa']['correcao_igpm'])
            );

            $pagamentoASerSalvo['DespesaPagamento'] = array($dadosDaDespesaPagamentoASerSalva);
        }

        $pagamentoASerSalvo['Pagamento'] = $dadosDoPagamentoASerSalvo;

        unset($despesaASerPaga['DespesaPagamento']);


        $this->Pagamento->bindModel(array('hasMany' => array('DespesaPagamento' =>
                array('foreignKey' => 'pagamento_id'))));

        $this->Despesa->begin();
        $this->Pagamento->saveAll($pagamentoASerSalvo);
        $this->Despesa->saveAll($despesaASerPaga);
        $this->Despesa->commit();

        $pagamentoASerProcessado['despesa'] = $despesaASerPaga['Despesa'];

        $pagamentoASerProcessado['texto_status'] = $this->texto_status[self :: CHEQUE_PAGO_COM_DESPESA];
        $this->pagamentosProcessados['cheque'][] = $pagamentoASerProcessado;
    }

    private function salvarPagamentoDeChequeNoBanco($pagamentoASerProcessado) {
        //debug("PAGAMENTO CHEQUE - nao tem despesa");
        $idDaTurma = intval(substr($pagamentoASerProcessado['codigoDoFormando'], 0, 5));
        $ordemDoFormando = intval(substr($pagamentoASerProcessado['codigoDoFormando'], 5, 3));
        $formandoDoPagamento = $this->obterFormandoPorTurmaECodigo($idDaTurma, $ordemDoFormando);
        if ($formandoDoPagamento == null) {
            $dadosDoPagamentoASerSalvo = array(
                'tipo' => Pagamento :: TIPO_CHEQUE_PERDIDO,
                'usuario_id' => null,
                'codigo' => '',
                'dt_cadastro' => $pagamentoASerProcessado['dataPagamento'],
                'dt_vencimento' => null,
                'dt_liquidacao' => $pagamentoASerProcessado['dataDeLiquidacao'],
                'status' => Pagamento :: STATUS_PAGO,
                'dt_transferencia' => null,
                'linha_digitavel' => null,
                'valor_pago' => $pagamentoASerProcessado['valorPagoTotal'],
                'valor_nominal' => $pagamentoASerProcessado['valorPagoBase'],
                'observacao' => $pagamentoASerProcessado['observacao']
            );
            $pagamentoASerProcessado['texto_status'] = $this->texto_status[self :: CHEQUE_PAGO_PERDIDO];
        } else {
            $dadosDoPagamentoASerSalvo = array(
                'tipo' => (preg_match("/contrato/i", ($pagamentoASerProcessado['formaDePagamento']))) ? Pagamento :: TIPO_CHEQUE_CONTRATO : Pagamento :: TIPO_CHEQUE_EXTRA,
                'usuario_id' => $formandoDoPagamento['Usuario']['id'],
                'codigo' => '',
                'dt_cadastro' => $pagamentoASerProcessado['dataPagamento'],
                'dt_vencimento' => null,
                'dt_liquidacao' => $pagamentoASerProcessado['dataDeLiquidacao'],
                'status' => Pagamento :: STATUS_PAGO,
                'dt_transferencia' => null,
                'linha_digitavel' => null,
                'valor_pago' => $pagamentoASerProcessado['valorPagoTotal'],
                'valor_nominal' => $pagamentoASerProcessado['valorPagoBase'],
                'observacao' => $pagamentoASerProcessado['observacao']
            );

            $pagamentoASerProcessado['texto_status'] = $this->texto_status[self :: CHEQUE_PAGO_SEM_DESPESA];
        }

        $this->Pagamento->begin();
        $this->Pagamento->saveAll(array('Pagamento' => $dadosDoPagamentoASerSalvo));
        $this->Pagamento->commit();
        $this->pagamentosProcessados['cheque'][] = $pagamentoASerProcessado;
    }

    private function obterFormandoPorCodigo($codigoFormando) {
        return $this->ViewFormandos->find('first',
                array('conditions' => array('codigo_formando' => $codigoFormando)));
    }

}

?>
