<?php

class MapaMesasController extends AppController {

	var $uses = array('TiposEvento', 'Evento','EventoMapa',
		'EventoMapaColuna','EventoMapaLocal','ViewFormandos');

	var $components = array('MapaDeMesas');

	public $mensagens_sistema = array(
			'agendamento_salvo'     => 'Agendamento Salvo com Sucesso!',
			'agendamento_problemas' => 'Problemas ao salvar agendamento'
		);

	public function __construct() {
		parent::__construct();
		$this->layout = 'metro/externo';

		$css_stylesheets = array(
			'metro/css/metro.css',
			'metro/css/min/metroui/tile.css?v=0.2',
			'metro/css/min/bootstrap/datepicker.css',
		);
		
		$this->set('css_stylesheets', $css_stylesheets);
	}

	public function atendimento_index(){
		$this->planejamento_index();
	}

	public function planejamento_index() {
		$turma = $this->obterTurmaLogada();
		$turmaId = $turma['Turma']['id'];

		$this->Evento->unbindModel(array(
			'belongsTo' => array('Turma'),
			'hasMany' => array('Extra'),
		));
		$eventos = $this->Evento->find('all', array(
			'conditions' => array(
				'Evento.turma_id' => $turmaId,
				'TurmaMapa.status' => 1,
				'Evento.tipos_evento_id' => array(1,13)
			)
		));

		if(empty($this->data) && count($eventos) > 1){
			$this->redirect("/{$this->params['prefix']}/mapa_mesas/selecionar_evento");
		}

		$evento_id = (!empty($this->data) ? $this->data['Evento']['id'] : $eventos[0]['Evento']['id']);
		if(empty($evento_id)){
			$this->render('planejamento_agendamento_inexistente');
			return false;
		}
		$evento = $this->Evento->findById($evento_id);

		$agendamentos = $this->MapaDeMesas->retornarAgendamentosTurma($turmaId, NULL, $evento_id);

		$this->set('agendamentos',$agendamentos);
		$this->set('evento',$evento);
		$this->set('evento_id',$evento_id);
	}

	function atendimento_selecionar_evento($id = false, $uid = false){
		$this->planejamento_selecionar_evento($id, $uid);
		$this->render('planejamento_selecionar_evento');
	}

	function planejamento_selecionar_evento ($id = false, $uid = false){
		$this->layout = 'metro/externo';
		if($id){
			$turma = $id;
		}else{
			$t = $this->obterTurmaLogada();
			$turma = $t['Turma']['id'];
		}
		$this->Evento->unbindModel(array(
			'belongsTo' => array('Turma'),
			'hasMany' => array('Extra'),
		));
		$eventos = $this->Evento->find('all', array(
			'conditions' => array(
				'Evento.turma_id' => $turma,
				'TurmaMapa.status' => 1,
				'Evento.tipos_evento_id' => array(1,13)
			)
		));

		$todos = array();
		foreach($eventos as $evento => $ev)
			$todos[$ev['Evento']['id']] = $ev['Evento']['nome'];

		//Carrega os mapas no segundo select
		//Carrega a Model dinamicamente
		$this->loadModel('Mapas');

		//executa a pesquisa de mapas por titulo
		$mapas =  $this->Mapas->find('list', array('fields' => 'Mapas.titulo'));

		$css_stylesheets = array('metro/css/min/metroui/tile.css?v=0.2');

		$this->set('mapas', $mapas);
		$this->set('eventos', $todos);
		$this->set('uid', $uid);
		$this->set('css_stylesheets', $css_stylesheets);
	}

	public function planejamento_agendamento($eventoId) {

		$turma = $this->obterTurmaLogada();
		$turmaId = $turma['Turma']['id'];

		$evento = $this->Evento->find('all', array(
			'conditions' => array(
				'Evento.id' => $eventoId,
				'Turma.id' => $turmaId
			)
		));

		if(empty($evento)){
			$this->render('planejamento_agendamento_inexistente');
			return false;
		}
		$agendamentos = $this->MapaDeMesas->retornarAgendamentosTurma($turmaId, NULL, $eventoId);
		if(!empty($agendamentos)){
			$this->render('planejamento_agendamento_efetuado');
			return false;
		}

		$intervalos = $this->MapaDeMesas->retornarIntervalos();

		$this->set('intervalos', $intervalos);
		$this->set('eventoId', $eventoId);
	}

	public function atendimento_relatorio_agendados($eventoId){
		$this->planejamento_relatorio_agendados($eventoId);
	}

	public function planejamento_relatorio_agendados($eventoId) {
		$this->loadModel('FormandoMapaHorario');
		$turma = $this->obterTurmaLogada();
		$turmaId = $turma['Turma']['id'];

		$this->loadModel('TurmaMapa');
		$turmaMapa = $this->TurmaMapa->find('first',
			array(
				'conditions'=>array(
					'TurmaMapa.turma_id'=>$turmaId,
					'TurmaMapa.evento_id'=>$eventoId,
					'TurmaMapa.status'=>1)
				));
		$turmaMapaId = $turmaMapa['TurmaMapa']['id'];
		
		$agendamento = $this->MapaDeMesas->retornarAgendamentosTurma($turmaId, NULL, $eventoId);
		if(!$agendamento){
			$this->redirect("/{$this->params['prefix']}/mapa_mesas");
		}

		$formandos=$this->MapaDeMesas->retornarFormandosPorTurma($turmaId, 'formando', $turmaMapaId);
		$comissao=$this->MapaDeMesas->retornarFormandosPorTurma($turmaId, 'comissao', $turmaMapaId);
		$formandosTotal = count($formandos) + count($comissao);

		$eventosTurma = $this->MapaDeMesas->retornarEventosPorTurma($turmaId,false);
		$evento = $eventosTurma[$turmaMapa['TurmaMapa']['evento_id']];

		$css_stylesheets = array(
			'css/bootstrap.css',
			'metro/css/metro.css',
			'metro/css/min/metroui/tile.css?v=0.2',
			'metro/css/min/bootstrap/datepicker.css',
		);

		$this->set('css_stylesheets', $css_stylesheets);
		$this->set('formandos',$formandos);
		$this->set('comissao',$comissao);
		$this->set('agendamento',$agendamento);
		$this->set('totalFormandos',$formandosTotal);
		$this->set('quantidadeMesas',$turmaMapa['TurmaMapa']['quantidade_mesas']);
		$this->set('evento',$evento);
		$this->set('eventoId',$eventoId);
		$this->set('turmaMapaId',$turmaMapaId);
	}

	public function planejamento_salvar_agendamento() {
		$data = $_POST['data'];

		if(!$data){
			return false;
			$mensagem = $this->mensagens_sistema['agendamento_problemas'];
			$this->Session->setFlash($mensagem);
			$this->render('planejamento_agendamento');
		}

		//Normalização de informações
		$dataInicio = $data['TurmaMapa']['data_inicio'];
		$dataInicio = preg_replace(
				'|([0-9]{2})/([0-9]{2})/([0-9]{4})|',
				'${3}-${2}-${1}',
				$dataInicio
			);

		$dataFinalizacao = $data['TurmaMapa']['data_finalizacao'];
		$dataFinalizacao = preg_replace(
				'|([0-9]{2})/([0-9]{2})/([0-9]{4})|',
				'${3}-${2}-${1}',
				$dataFinalizacao
			);

		$strHoraInicio = $data['TurmaMapa']['hora_inicio'].':00';
		$strHoraFinalizacao = $data['TurmaMapa']['hora_finalizacao'].':00';

		$data1 = new DateTime($dataInicio.' '.$strHoraInicio);
		$data2 = new DateTime($dataFinalizacao.' '.$strHoraFinalizacao);
		$diasTotal = $data2->diff($data1);
		$diasTotal = $diasTotal->d < 1 ? 1 : ceil($diasTotal->d + ($diasTotal->h / 24));
		$horaInicio = new DateTime($strHoraInicio);
		$horaFinalizacao = new DateTime($strHoraFinalizacao);
		$duracaoPorDia = $horaFinalizacao->diff($horaInicio)->format("%h");
		$horasTotal = $diasTotal * $duracaoPorDia;

		$turma = $this->obterTurmaLogada();
		$turmaId = $turma['Turma']['id'];

		$intervaloEscolhas = $data['TurmaMapa']['intervalo'];
		$eventoId = $data['Evento']['id'];

		if(!$dataFinalizacao || !$dataInicio || strtotime($dataFinalizacao) < strtotime($dataInicio)) {
			$this->Session->setFlash($this->mensagens_sistema['agendamento_problemas']);
			$this->redirect("/{$this->params['prefix']}/mapa_mesas/agendamento/".$eventoId);
		}
		
		$formandos = $this->MapaDeMesas->retornarFormandosPorTurma($turmaId, 'formando');
		$formandosTotal = count($formandos);
		if(((60/$intervaloEscolhas)*$horasTotal) == 0){
			$formandosPorIntervalo = $formandosTotal;
		}else{
			$formandosPorIntervalo = ceil($formandosTotal/((60/$intervaloEscolhas)*$horasTotal));            
		}

		$args = array(
			'fields'     => array('id'),
			'conditions' => array(
				'TurmaMapa.evento_id'=>$eventoId,
				'TurmaMapa.turma_id'=>$turmaId,
				'TurmaMapa.status' => 1
			)
		);

		$this->loadModel('TurmaMapa');
		$this->TurmaMapa->bindEvento();
		$turmaMapaId = $this->TurmaMapa->find('first',$args)['TurmaMapa']['id'];

		$args = array(
			'formandos' => $formandos,
			'turmaMapaId' => $turmaMapaId,
			'data1' => $data1,
			'data2' => $data2,
			'formandosPorIntervalo' => $formandosPorIntervalo,
			'intervaloEscolhas' => $intervaloEscolhas,
			'horaInicio' => $horaInicio,
			'horaFinalizacao' => $horaFinalizacao );

		// Desagenda os demais
		$this->loadModel('FormandoMapaHorario');
		$this->FormandoMapaHorario->deleteAll(
			array('id_turma_mapa'=>$turmaMapaId)
		);

		$turma = $this->MapaDeMesas->agendamentoFormandos($args);

		$comissao = $this->MapaDeMesas->retornarFormandosPorTurma($turmaId, 'comissao');
		$args['formandos'] = $comissao;
		$turma = $this->MapaDeMesas->agendamentoFormandos($args);

		$agendamento = array(
				'turmaMapaId' => $turmaMapaId,
				'dataInicio'  => $dataInicio,
				'dataFinalizacao' => $dataFinalizacao,
				'horaInicio' => $strHoraInicio,
				'horaFinalizacao' => $strHoraFinalizacao
			);

		$this->MapaDeMesas->salvarAgendamento($agendamento);
		$this->Session->setFlash($this->mensagens_sistema['agendamento_salvo']);
		$this->redirect("/{$this->params['prefix']}/mapa_mesas/relatorio_agendados/".$eventoId);
	}

	public function planejamento_bloquear_mapa($eventoId, $turma_mapa_id){
		$this->loadModel('FormandoMapaHorario');
		$this->loadModel('MapaMesasAgendamento');
		try{
			$this->FormandoMapaHorario->begin();

			$sucesso = $this->FormandoMapaHorario->updateAll(
				array('status' => 0),
				array('FormandoMapaHorario.id_turma_mapa' => $turma_mapa_id)
			);
			if(!$sucesso){
				throw new Exception('Erro');
			}

			$sucesso = $this->MapaMesasAgendamento->updateAll(
				array('escolha_liberada' => 0),
				array('MapaMesasAgendamento.turma_mapa_id' => $turma_mapa_id)
			);
			if(!$sucesso){
				throw new Exception('Erro2');
			}

			$this->FormandoMapaHorario->commit();
		}catch(Exception $e){
			$this->FormandoMapaHorario->rollback();
		}
		$this->redirect("/{$this->params['prefix']}/mapa_mesas/relatorio_agendados/".$eventoId);
	}

	public function planejamento_desbloquear_mapa($eventoId, $turma_mapa_id){
		$this->loadModel('FormandoMapaHorario');
		$this->loadModel('MapaMesasAgendamento');
		try{
			$this->FormandoMapaHorario->begin();

			$sucesso = $this->FormandoMapaHorario->updateAll(
				array('status' => 1),
				array('FormandoMapaHorario.id_turma_mapa' => $turma_mapa_id)
			);
			if(!$sucesso){
				throw new Exception('Erro');
			}

			$sucesso = $this->MapaMesasAgendamento->updateAll(
				array('escolha_liberada' => 1),
				array('MapaMesasAgendamento.turma_mapa_id' => $turma_mapa_id)
			);
			if(!$sucesso){
				throw new Exception('Erro');
			}

			$this->FormandoMapaHorario->commit();
		}catch(Exception $e){
			$this->FormandoMapaHorario->rollback();            
		}
		$this->redirect("/{$this->params['prefix']}/mapa_mesas/relatorio_agendados/".$eventoId);
	}

	public function producao_plantas(){
		$this->loadModel('Mapa');

		$mapas = $this->Mapa->find('all', array(
			'conditions' => array(
				'status' => 1
			),
		));

		$this->set('mapas', $mapas);
	}

	public function producao_importar_planta(){
		$this->loadModel('Mapa');
		$this->layout = false;
		if(!empty($this->data)){
			if($this->data['Mapa']['extensao'] == 'image/png' || $this->data['Mapa']['extensao'] == 'image/jpeg'){
				$imagem = base64_decode($this->data['Mapa']['binario']);
				$nome_arquivo = strtotime(date('Y-m-d H:i:s')) . "_" . $this->data['Mapa']['arquivo'];
				$path = APP . "webroot/mapas/" . $nome_arquivo;
				if(file_put_contents($path, $imagem)) {
					$this->Mapa->create();
					$this->Mapa->save(array(
						'url_imagem' => 'mapas/'.$nome_arquivo,
						'titulo' => $this->data['Mapa']['nome']
					));
				}
			}else{
				$this->set('mensagem_erro','Importe apenas arquivo do tipo JPG ou PNG');
			}
		}
	}

	public function producao_editar_planta($id){
		$this->loadModel('Mapa');

		if(!empty($this->data)){
			if($this->data['Mapa']['extensao'] == 'image/png' || $this->data['Mapa']['extensao'] == 'image/jpeg'){
				$imagem = base64_decode($this->data['Mapa']['binario']);
				$nome_arquivo = strtotime(date('Y-m-d H:i:s')) . "_" . $this->data['Mapa']['arquivo'];
				$path = APP . "webroot/mapas/" . $nome_arquivo;
				if(file_put_contents($path, $imagem)) {
					$this->Mapa->id = $this->data['Mapa']['id'];
					$this->Mapa->save(array(
						'url_imagem' => 'mapas/'.$nome_arquivo,
						'titulo' => $this->data['Mapa']['nome']
					));
				}
			}else{
				$this->set('mensagem_erro','Importe apenas arquivo do tipo JPG ou PNG');
			}
		}else{
			$this->data = $this->Mapa->findById($id);
			$this->data['Mapa']['nome'] = $this->data['Mapa']['titulo'];
		}
	}

	public function producao_apagar_planta($id){
		$this->loadModel('Mapa');
		$this->Mapa->delete($id);
	}

	public function planejamento_setorizacao($evento_id){
		$this->loadModel('TurmaMapa');
		$this->loadModel('FormandoMapaHorario');

		$turma = $this->obterTurmaLogada();
		$turmaId = $turma['Turma']['id'];

		$turmaMapa = $this->TurmaMapa->find('first',array(
			'conditions'=>array(
				'TurmaMapa.turma_id' => $turmaId,
				'TurmaMapa.evento_id' => $evento_id,
				'TurmaMapa.status' => 1
			)
		));
		$turmaMapaId = $turmaMapa['TurmaMapa']['id'];
		$setores = json_decode($turmaMapa['TurmaMapa']['mapa_setores']);
		$mapa_layout = json_decode($turmaMapa['TurmaMapa']['mapa_layout']);
		$setores_qtd_mesa = array();

		foreach($mapa_layout as $grupo){
			if(!empty($grupo->tipo) && $grupo->tipo == 'grupo_mesas'){
				if(!isset($setores_qtd_mesa[$grupo->setor])){
					$setores_qtd_mesa[$grupo->setor] = 0;
				}
				$setores_qtd_mesa[$grupo->setor] += $grupo->linhas * $grupo->colunas;
			}
		}

		foreach($setores as $key => $setor){
			$qtd_formandos = $this->FormandoMapaHorario->find('first', array(
				'fields' => array(
					'COUNT(DISTINCT(FormandoMapaHorario.codigo_formando)) AS qtd_formandos',
					'COUNT(DISTINCT(FormandoMapaMesa.codigo_formando)) AS qtd_formandos_marcados'
				),
				'conditions' => array(
					'id_turma_mapa' => $turmaMapaId,
					'setor' => utf8_decode($setor)
				),
				'joins' => array(
					array(
						'table' => 'formando_mapa_mesas',
						'alias' => 'FormandoMapaMesa',
						'type' => 'LEFT',
						'conditions' => array(
							'FormandoMapaMesa.codigo_formando = FormandoMapaHorario.codigo_formando',
							'FormandoMapaMesa.turma_mapa_id = FormandoMapaHorario.id_turma_mapa',
							'FormandoMapaMesa.status = 1',
						)
					)
				)
			));

			$setores[$key] = array(
				'nome' => $setor,
				'qtd_formandos' => $qtd_formandos[0]['qtd_formandos'],
				'qtd_mesas' => $setores_qtd_mesa[$setor],
				'qtd_formandos_marcados' => $qtd_formandos[0]['qtd_formandos_marcados'],
			);
		}

		$formandos_sem_setor = $this->FormandoMapaHorario->find('all', array(
			'conditions' => array(
				'id_turma_mapa' => $turmaMapaId,
				'setor IS NULL'
			),
			'joins' => array(
				array(
					'table' => 'vw_codigo_formandos',
					'alias' => 'VwCodigoFormando',
					'type' => 'INNER',
					'conditions' => 'FormandoMapaHorario.codigo_formando = VwCodigoFormando.codigo_formando'
				),
			),
			'fields' => array(
				'FormandoMapaHorario.*',
				'VwCodigoFormando.nome',
			),
			'order' => 'FormandoMapaHorario.codigo_formando'
		));

		$this->set('setores', $setores);
		$this->set('turmaMapa', $turmaMapa);
		$this->set('formandos_sem_setor', $formandos_sem_setor);
		$this->set('evento_id', $evento_id);
	}

	public function planejamento_visualizar_setor($evento_id, $setor){
		$this->loadModel('TurmaMapa');
		$this->loadModel('FormandoMapaHorario');

		$turma = $this->obterTurmaLogada();
		$turmaId = $turma['Turma']['id'];

		$turmaMapa = $this->TurmaMapa->find('first',array(
			'conditions'=>array(
				'TurmaMapa.turma_id' => $turmaId,
				'TurmaMapa.evento_id' => $evento_id,
				'TurmaMapa.status' => 1
			)
		));
		$turmaMapaId = $turmaMapa['TurmaMapa']['id'];

		$formandos = $this->FormandoMapaHorario->find('all', array(
			'conditions' => array(
				'id_turma_mapa' => $turmaMapaId,
				'setor' => utf8_decode($setor)
			),
			'joins' => array(
				array(
					'table' => 'vw_codigo_formandos',
					'alias' => 'VwCodigoFormando',
					'type' => 'INNER',
					'conditions' => 'FormandoMapaHorario.codigo_formando = VwCodigoFormando.codigo_formando'
				),
			),
			'fields' => array(
				'FormandoMapaHorario.*',
				'VwCodigoFormando.nome',
			),
			'order' => 'FormandoMapaHorario.codigo_formando'
		));

		$formandos_sem_setor = $this->FormandoMapaHorario->find('count', array(
			'conditions' => array(
				'id_turma_mapa' => $turmaMapaId,
				'setor IS NULL'
			)
		));

		$this->set('turmaMapa', $turmaMapa);
		$this->set('evento_id', $evento_id);
		$this->set('setor', $setor);
		$this->set('formandos', $formandos);
		$this->set('formandos_sem_setor', $formandos_sem_setor);
	}

	public function planejamento_visualizar_formandos_nao_marcados($evento_id, $setor){
		$this->loadModel('TurmaMapa');
		$this->loadModel('FormandoMapaHorario');

		$turma = $this->obterTurmaLogada();
		$turmaId = $turma['Turma']['id'];

		$turmaMapa = $this->TurmaMapa->find('first',array(
			'conditions'=>array(
				'TurmaMapa.turma_id' => $turmaId,
				'TurmaMapa.evento_id' => $evento_id,
				'TurmaMapa.status' => 1
			)
		));
		$turmaMapaId = $turmaMapa['TurmaMapa']['id'];

		$qtd_formandos = $this->FormandoMapaHorario->find('all', array(
			'fields' => array(
				'FormandoMapaHorario.*',
				'ViewFormandos.nome',
				'ViewFormandos.id',
				'ViewFormandos.data_adesao',
			),
			'conditions' => array(
				'id_turma_mapa' => $turmaMapaId,
				'setor' => utf8_decode($setor),
				'FormandoMapaMesa.id IS NULL'
			),
			'joins' => array(
				array(
					'table' => 'formando_mapa_mesas',
					'alias' => 'FormandoMapaMesa',
					'type' => 'LEFT',
					'conditions' => array(
						'FormandoMapaMesa.codigo_formando = FormandoMapaHorario.codigo_formando',
						'FormandoMapaMesa.turma_mapa_id = FormandoMapaHorario.id_turma_mapa',
					)
				),
				array(
					'table' => 'vw_formandos',
					'alias' => 'ViewFormandos',
					'type' => 'INNER',
					'conditions' => 'FormandoMapaHorario.codigo_formando = ViewFormandos.codigo_formando'
				),
			)
		));

		foreach ($qtd_formandos as $key => $formando) {
			$quantidade_mesas = $this->MapaDeMesas->retornaMesasCompradasPorUsuario($formando['ViewFormandos']['id'], $evento_id);
			$quantidade_mesas = array_sum($quantidade_mesas);
			$qtd_formandos[$key]['ViewFormandos']['quantidade_mesas'] = $quantidade_mesas;
		}

		$this->set('turmaMapa', $turmaMapa);
		$this->set('evento_id', $evento_id);
		$this->set('setor', $setor);
		$this->set('qtd_formandos', $qtd_formandos);
	}

	public function planejamento_setorizacao_adicionar_formandos($evento_id, $setor){
		$this->loadModel('TurmaMapa');
		$this->loadModel('FormandoMapaHorario');

		$turma = $this->obterTurmaLogada();
		$turmaId = $turma['Turma']['id'];

		$turmaMapa = $this->TurmaMapa->find('first',array(
			'conditions'=>array(
				'TurmaMapa.turma_id' => $turmaId,
				'TurmaMapa.evento_id' => $evento_id,
				'TurmaMapa.status' => 1
			)
		));
		$turmaMapaId = $turmaMapa['TurmaMapa']['id'];

		if(!empty($this->data)){
			foreach($this->data['FormandoMapaHorario'] as $codigo_formando => $formando){
				if($formando){
					$this->FormandoMapaHorario->updateAll(
						array('setor' => "'".utf8_decode($setor)."'"),
						array(
							'id_turma_mapa' => $turmaMapaId,
							'codigo_formando' => $codigo_formando
						)
					);
				}
			}
			$this->redirect("/{$this->params['prefix']}/mapa_mesas/setorizacao/".$evento_id);
		}

		$formandos_sem_setor = $this->FormandoMapaHorario->find('all', array(
			'conditions' => array(
				'id_turma_mapa' => $turmaMapaId,
				'setor IS NULL'
			),
			'joins' => array(
				array(
					'table' => 'vw_codigo_formandos',
					'alias' => 'VwCodigoFormando',
					'type' => 'INNER',
					'conditions' => 'FormandoMapaHorario.codigo_formando = VwCodigoFormando.codigo_formando'
				),
			),
			'fields' => array(
				'FormandoMapaHorario.*',
				'VwCodigoFormando.nome',
			),
			'order' => 'FormandoMapaHorario.codigo_formando'
		));

		$this->set('formandos_sem_setor', $formandos_sem_setor);
		$this->set('turmaMapa', $turmaMapa);
		$this->set('setor', $setor);
		$this->set('evento_id', $evento_id);
	}

	public function planejamento_setorizacao_remover_formando($evento_id, $codigo_formando){
		$this->loadModel('TurmaMapa');
		$this->loadModel('FormandoMapaHorario');

		$turma = $this->obterTurmaLogada();
		$turmaId = $turma['Turma']['id'];

		$turmaMapa = $this->TurmaMapa->find('first',array(
			'conditions'=>array(
				'TurmaMapa.turma_id' => $turmaId,
				'TurmaMapa.evento_id' => $evento_id,
				'TurmaMapa.status' => 1
			)
		));
		$turmaMapaId = $turmaMapa['TurmaMapa']['id'];

		$this->FormandoMapaHorario->updateAll(
			array('setor' => NULL),
			array(
				'id_turma_mapa' => $turmaMapaId,
				'codigo_formando' => $codigo_formando
			)
		);
	}

	public function planejamento_setorizacao_remover_todos($evento_id, $setor){
		$this->loadModel('TurmaMapa');
		$this->loadModel('FormandoMapaHorario');

		$turma = $this->obterTurmaLogada();
		$turmaId = $turma['Turma']['id'];

		$turmaMapa = $this->TurmaMapa->find('first',array(
			'conditions'=>array(
				'TurmaMapa.turma_id' => $turmaId,
				'TurmaMapa.evento_id' => $evento_id,
				'TurmaMapa.status' => 1
			)
		));
		$turmaMapaId = $turmaMapa['TurmaMapa']['id'];

		$this->FormandoMapaHorario->updateAll(
			array('setor' => NULL),
			array(
				'id_turma_mapa' => $turmaMapaId,
				'setor' => $setor
			)
		);
		$this->redirect("/{$this->params['prefix']}/mapa_mesas/setorizacao/".$evento_id);
	}

	public function producao_replicar_mapa($evento_id){
		$this->loadModel('Evento');
		$this->loadModel('TurmaMapa');

		$turma = $this->obterTurmaLogada();
		$turmaId = $turma['Turma']['id'];

		if(!empty($this->data)){
			// Desativa os mapas ativos
			$turma_mapas = $this->TurmaMapa->find('all', array('fields' => array('TurmaMapa.id'),'conditions' => array('TurmaMapa.evento_id' => $evento_id, 'TurmaMapa.turma_id' => $turmaId, 'TurmaMapa.status' => 1)));
			foreach($turma_mapas as $turma_mapa){
				$this->TurmaMapa->mudar_status($turma_mapa['TurmaMapa']['id'], 0);
			}

			$turma_mapa = $this->TurmaMapa->findById($this->data['turma_mapa_id']);

			// Deleta o TurmaMapa com o mesmo mapa que está replicando
			$turma_mapas = $this->TurmaMapa->find('all', array(
				'conditions' => array(
					'TurmaMapa.evento_id' => $evento_id,
					'TurmaMapa.turma_id' => $turmaId,
					'TurmaMapa.mapa_id' => $turma_mapa['TurmaMapa']['mapa_id']
				),
			));

			foreach($turma_mapas as $mapa){
				$this->TurmaMapa->delete($mapa['TurmaMapa']['id']);
			}

			// Insere o novo mapa
			unset($turma_mapa['TurmaMapa']['id']);
			unset($turma_mapa['TurmaMapa']['status']);
			$turma_mapa['TurmaMapa']['turma_id'] = $turmaId;
			$turma_mapa['TurmaMapa']['evento_id'] = $evento_id;
			$this->TurmaMapa->create();
			$this->TurmaMapa->save($turma_mapa);
			exit;
		}else{
			$evento = $this->Evento->findById($evento_id);

			$this->set('evento', $evento);
			$this->set('turma', $turma);
		}
	}

	public function producao_procurar_mapa(){
		$this->loadModel('TurmaMapa');

		$turma = $this->obterTurmaLogada();
		$turmaId = $turma['Turma']['id'];

		$turma_mapas = $this->TurmaMapa->find('all', array(
			'conditions' => array(
				'TurmaMapa.turma_id' => $this->data['turma_id'],
				'TurmaMapa.status' => 1
			)
		));

		$this->set('turma_mapas',$turma_mapas);
	}
}