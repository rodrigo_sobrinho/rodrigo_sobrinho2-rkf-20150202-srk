<script type="text/javascript">
 
    function editar_adesao()
    {
	$('#btn_adesao').hide();
	$('#spn_adesao').hide();
	$('#salvar_adesao').fadeIn('slow');
	$('#FormandoProfileValorAdesaoComissao').fadeIn('fast');
    }  

    function editar_parcelas()
    {
	$('#btn_parcelas').hide();
	$('#spn_parcelas').hide();
	$('#salvar_parcelas').fadeIn('slow');
	$('#FormandoProfileMaxParcelasComissao').fadeIn('fast');
    } 
</script>

<span id="conteudo-titulo" class="box-com-titulo-header">Informações do Membro da Comissão</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<?php echo $form->create(null, array('url' => "/{$this->params['prefix']}/comissoes/editar", "id" => "form")); ?>
	<?php echo $form->hidden('FormandoProfile.usuario_id',  array('hiddenField' => true, 'value' => $formando['FormandoProfile']['usuario_id'])); ?>
	<?php $session->flash(); ?>
	<div class="detalhes">
		<p class="grid_11 alpha omega">
    			<label class="grid_5 alpha">Nome</label>
    			<span class="grid_8 alpha first"><?php echo $formando['Usuario']['nome']; ?></span>
		</p>
    		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">E-mail</label>
			<span class="grid_8 alpha first"><?php echo $formando['Usuario']['email']; ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">RG</label>
			<span class="grid_8 alpha first"><?php echo $formando['FormandoProfile']['rg']; ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Telefone Residencial</label>
			<label class="grid_6 alpha">Telefone Comercial</label>
			<span class="grid_5 alpha first"><?php echo $formando['FormandoProfile']['tel_residencial']; ?></span>
			<span class="grid_6 alpha"><?php echo $formando['FormandoProfile']['tel_comercial']; ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_11 alpha">Telefone Celular</label>
			<span class="grid_11 alpha first"><?php echo $formando['FormandoProfile']['tel_celular']; ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_11 alpha">Cargo</label>
			<span class="grid_11 alpha first"><?php echo $formando['FormandoProfile']['cargo_comissao']; ?></span>			
		</p>
		<p class="grid_11 alpha omega">
		        <label class="grid_4 alpha">Valor da Ades&atilde;o</label><br /><br />
			<span class="grid_4 alpha first" id="spn_adesao"><?php echo $valor_adesao_comissao; ?></span>
			<input id="btn_adesao" type="button" value="Editar" class="submit button" onClick="editar_adesao()" style="float:left;"/>
			<?php echo $form->text("valor_adesao_comissao", 
			    array('value' => $formando['FormandoProfile']['valor_adesao_comissao'],'style' => 'display:none;'));?>
			<?php echo $form->button("", array('style' => 'display:none;', 'value' => 'Salvar', 
			    'onClick' => 'document.form["form"].submit()', 'id' => 'salvar_adesao'));?>	
		</p>
		<p class="grid_11 alpha omega">
		        <label class="grid_6 alpha">N&uacute;mero m&aacute;ximo de parcelas</label><br /><br />
			<span class="grid_4 alpha first" id="spn_parcelas"><?php echo $max_parcelas_comissao; ?></span>
			<input id="btn_parcelas" type="button" value="Editar" class="submit button" onClick="editar_parcelas()" style="float:left;"/>
			<?php echo $form->text("FormandoProfile.max_parcelas_comissao", 
			    array('value' => $formando['FormandoProfile']['max_parcelas_comissao'], 'style' => 'display:none;'));?>
			<?php echo $form->button("", array('style' => 'display:none;', 'value' => 'Salvar', 
			    'onClick' => 'document.form["form"].submit()', 'id' => 'salvar_parcelas'));?>
			
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_11 alpha">Informações Extras</label>
			<span class="grid_11 alpha"><?php echo $form->input('FormandoProfile.anotacoes_comissao', array('value' => $formando['FormandoProfile']['anotacoes_comissao'], 'class' => 'grid_11 alpha omega first', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_11'))); ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<?php echo $form->input('FormandoProfile.id', array('value' => $formando['FormandoProfile']['usuario_id'], 'type' => 'hidden','class' => 'grid_11 alpha omega first', 'label' => false, 'div' => false)); ?>	
			<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index') ,array('class' => 'cancel')); ?>
			<?php echo $form->end(array('label' => 'Salvar', 'div' => false, 'class' => 'submit'));?>
		</p>
	</div>
</div>
