<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootmetro/progressbar.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/max/download.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $.Input();
        $("#form-filtro").submit(function(e) {
            e.preventDefault();
            var context = ko.contextFor($(".metro-button.reload")[0]);
            context.$data.showLoading(function() {
                $("#form-filtro").find('[value="false"]').attr('disabled','disabled');
                var dados = $("#form-filtro").serialize();
                var url = $("#form-filtro").attr('action');
                $.ajax({
                    url : url,
                    data : dados,
                    type : "POST",
                    dataType : "json",
                    complete : function() {
                        context.$data.reload();
                    }
                });
            });
        });
        var button = $("#baixar")[0];
        var bar = $('#progress');
        var status = $('#status');
        $("#baixar").click(function(e) {
            if(!$(this).hasClass('baixar')) {
                e.preventDefault();
                $(button).fadeOut(500,function() {
                    $.getJSON("/formandos/criar_arquivo_telao",function() {
                        status.text('Compactando arquivo').fadeIn(500,function() {
                            verificarArquivo();
                        });
                    }).fail(function() {
                        status.addClass('bg-color-red').text('Erro ao criar zip').fadeIn(500);
                    });
                });
            };
        });
        
        function verificarArquivo() {
            $.getJSON("/formandos/verificar_arquivo_telao",function(response) {
                if(response.file) {
                    status.text('Compactando arquivo').fadeOut(500,function() {
                            status.after("<a class='button mini bg-color-greenDark pull-right' id='baixar' \n\
                            href='/upload/turmas/fotos_telao_<?=$turma['Turma']['id']?>.zip' target='_blank'> Download <i class='icon-cloud-2'></i></a>");
                        });
                } else {
                    setTimeout(function() {
                        verificarArquivo();
                    },2000);
                }
            });
        }
    });
</script>
<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: reload"></a>
        Fotos do Telão
        <a class='button mini bg-color-green pull-right'
            href='<?="/{$this->params['prefix']}/formandos/excel_fotos_telao"?>'>
            Excel
            <i class='icon-file-excel'></i>
        </a>
    </h2>
</div>
<?php
$paginator->options(array(
    'url' => array($this->params['prefix'] => true)));
$sortOptions = array('data-bind' => 'click: loadThis');
$buttonAfter = '<button class="helper" onclick="return false" ' .
    'tabindex="-1" type="button"></button>';
$session->flash(); 
?>
<div class="row-fluid">
    <?=$form->create('ViewFormandos',array(
        'url' => "/{$this->params['prefix']}/formandos/fotos_telao_listar",
        'id' => 'form-filtro')) ?>
    <div class="row-fluid">
        <div class="span2">
            <label>Codigo</label>
            <?=$form->input('codigo_formando',
                array('label' => false, 'div' => 'input-control mini text',
                    'error' => false,'type' => 'text', 'after' => $buttonAfter)); ?>
        </div>
        <div class="span3">
            <label>&nbsp;</label>
            <button type='submit' class='mini max bg-color-red'>
                Filtrar
                <i class='icon-search-2'></i>
            </button>
        </div>
        <div class="span3 pull-right">
            <label>&nbsp;</label>
            <a class='button mini bg-color-red pull-right' id="baixar"
                href=''
                target='_blank'>
                Baixar Fotos
                <i class='icon-cloud-2'></i>
            </a>
            <span class="label label-info hide pull-right" id="status"></span>
            <div class="progress progress-success hide" id="progress">
                <div class="bar"></div>
            </div>
        </div>
    </div>
    <?= $form->end(array('label' => false,
        'div' => false, 'class' => 'hide')); ?>
    <?php if (sizeof($formandos) > 0) : ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="10%"><?=$paginator->sort('Cod', 'ViewFormandos.codigo_formando',$sortOptions); ?></th>
                <th scope="col" width="40%"><?=$paginator->sort('Nome', 'ViewFormandos.nome',$sortOptions); ?></th>
                <th scope="col" width="25%"><?=$paginator->sort('Foto Crianca', 'foto_crianca',$sortOptions); ?></th>
                <th scope="col" width="25%"><?=$paginator->sort('Foto Adulto', 'foto_adulto',$sortOptions); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($formandos as $formando) : ?>
            <?php if(!empty($formando['ViewFormandos']['data_adesao'])){ ?>
            <tr>
                <td><?=$formando['ViewFormandos']['codigo_formando']; ?></td>
                <td><?=$formando['ViewFormandos']['nome']; ?></td>
                <td style="text-align: center">
                    <?php if(!empty($formando[0]['foto_crianca'])){ ?>
                    <a class="button mini bg-color-red" href="<?=$this->webroot.$formando[0]['foto_crianca'];?>" target="_blank">
                        Exibir Foto
                    </a>
                    <?php } else { ?>
                    <i class="icon-x"></i>
                    <?php } ?>
                </td>
                <td style="text-align: center">
                    <?php if(!empty($formando[0]['foto_adulto'])){ ?>
                    <a class="button mini bg-color-red" href="<?=$this->webroot.$formando[0]['foto_adulto'];?>" target="_blank">
                        Exibir Foto
                    </a>
                    <?php } else { ?>
                    <i class="icon-x"></i>
                    <?php } ?>
                </td>
            </tr>
            <?php } ?>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3" class="paginacao">
                    <?=$paginator->numbers(array('separator' => ' ','data-bind' => 'click: loadThis')); ?>
                </td>
                <td colspan="1" class="pull-right">
                    <?=$paginator->counter(array('format' => 'Total : %count% ' .  'Linhas')); ?>
                </td>
            </tr>
        </tfoot>
    </table>
    <?php else : ?>
    <h2 class="fg-color-red">Nenhum Formando Encontrado</h2>
    <?php endif; ?>
</div>