<?php
class TiposEvento extends AppModel {
    var $name = 'TiposEvento';

	/*
	 * Tabela no banco está assim:
	 * id INT
	 * nome VARCHAR(80)
	 * ativo BOOL
	 */
	 
	var $validate = array(
		'nome' => array(
			'vazio' => array(
                'rule' => 'notEmpty',
                'message' => 'Preencha o nome.'
            )
		)
    );
}
?>