<?php

class MapaDeMesasMapa {

    /**
     * Identificador único do evento
     * @var Integer
     */
    private $id;

    /**
     * Titulo/nome do evento
     * @var String
     */
    private $titulo;

    /**
     * Mapa vinculado ao evento
     * @var MapaDeMesasMapa
     */
    private $mapa;

    public function __construct() {
    }
}