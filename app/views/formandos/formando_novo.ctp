	<div class="span9 metro" style="position:relative" id="content-container">
			<div class="row-fluid content-loader hide" id="content-loading">
				<div class="cycle-loader"></div>
			</div>
			<div class="row-fluid content-loader hide" id="content-body"></div>
			<div class="row-fluid content-loader" id="content-home">
				<div class="tile icon double-vertical app bg-color-blueDark"
					data-bind="click: function(data, event) {
						page('<?=$this->webroot.$this->params['prefix']?>/usuarios/editar_dados') }">
					<div class="tile-content">
						<span class="img icon-user-4"></span>
					</div>
					<div class="brand"><span class="name">Meus Dados</span></div>
				</div>
				<div class="tile icon bg-color-red "
					data-bind="click: function(data, event) {
						page('<?=$this->webroot.$this->params['prefix']?>/campanhas/listar') }">
					<div class="tile-content">
						<span class="img icon-cart"></span>
					</div>
					<div class="brand"><span class="name">Loja</span></div>
				</div>
				<div class="tile icon bg-color-grayDark"
					data-bind="click: function(data, event) {
						page('<?=$this->webroot.$this->params['prefix']?>/area_financeira/dados') }">
					<div class="tile-content">
						<span class="img icon-pencil"></span>
					</div>
					<div class="brand"><span class="name">&Aacute;rea Financeira</span></div>
				</div>
				<div class="tile icon bg-color-yellow "
					data-bind="click: function(data, event) {
						page('<?=$this->webroot.$this->params['prefix']?>/eventos/listar') }">
					<div class="tile-content">
						<span class="img icon-music"></span>
					</div>
					<div class="brand"><span class="name">Eventos</span></div>
				</div>
				<div class="tile icon double bg-color-green"
					data-bind="click: function(data, event) {
						page('<?=$this->webroot.$this->params['prefix']?>/parcerias/listar') }">
					<div class="tile-content">
						<div class="img icon-users"></div>
					</div>
					<div class="brand"><span class="name">Clube &Aacute;S</span></div>
				</div>
				<div class="tile icon bg-color-purple"
					data-bind="click: function(data, event) {
						page('<?=$this->webroot.$this->params['prefix']?>/rifas/cupons') }">
					<div class="tile-content">
						<span class="img icon-puzzle"></span>
					</div>
					<div class="brand"><span class="name">Rifa Online</span></div>
				</div>
			</div>
		</div>
		<div class="span3" id="util">
			<ul class="nav nav-tabs">
				<li class="active">
					<a href="#mensagens" rel="tooltip" data-toggle="tab"
						class="icon-comments-2" title="Mensagens"></a>
				</li>
				<li>
					<a href="#calendario" rel="tooltip" data-toggle="tab"
						class="icon-calendar" title="Calendário"></a></li>
				<li>
					<a href="#alertas" rel="tooltip" data-toggle="tab"
						class="icon-warning" title="Alertas"></a>
				</li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="mensagens">
					<ul data-bind="template: { name: 'tmpMensagensPreview', foreach: itens }">
					</ul>
				</div>
				<div class="tab-pane" id="calendario">
					<div class="row-fluid" id="full-calendar"></div>
				</div>
				<div class="tab-pane" id="alertas">
					Alertar
				</div>
			</div>
		</div>