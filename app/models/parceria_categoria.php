<?php

class ParceriaCategoria extends AppModel {
    var $name = "ParceriaCategoria";
    var $belongsTo = array('Categoria','Parceria');
    var $useTable = "parcerias_categorias";
}
?>
