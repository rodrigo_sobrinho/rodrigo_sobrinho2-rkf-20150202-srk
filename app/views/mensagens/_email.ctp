<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/email.css?v=0.3">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/knockout/email.js?v=0.3"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#novo-informativo').click(function() {
            bootbox.dialog('Carregando',[{
                label: 'Enviar',
                class: 'bg-color-greenDark',
                callback: function() {
                    $("#formulario").trigger('submit');
                    return false;
                }
            },{
                label: 'Fechar',
                class: 'bg-color-red'
            }],{
                remote: '/mensagens/adicionar_informativo'
            });
        });
        $('#novo-contato').click(function() {
            bootbox.dialog('Carregando',[{
                label: 'Enviar',
                class: 'bg-color-green',
                callback: function() {
                    $("#formulario").trigger('submit');
                    return false;
                }
            },{
                label: 'Fechar',
                class: 'bg-color-red'
            }],{
                remote: '/mensagens/formulario_contato'
            });
        });
        $('#buscar-formando').click(function() {
            bootbox.dialog('Carregando',[{
                label: 'Cancelar',
                class: 'bg-color-red'
            }],{
                remote: '/atendimento/mensagens/buscar_formando'
            });
        });
    });
</script>
<script type="text/html" id="tmpItensPorPagina">
    <li data-bind="css: { active: $root.itensPorPagina() == $data }">
        <a href="#" data-bind="text:$data, click: function() { $root.itensPorPagina($data); $root.paginaAtual(1) }"></a>
    </li>
</script>
<script type="text/html" id="tmpListaEmails">
    <?php if($usuario['Usuario']['grupo'] == 'atendimento') : ?>
    <li data-bind="click:function(data,event) { $root.exibirMensagem($index(),event) },
        css:{ 'nao-lida': pendente() == 1 }">
    <?php else : ?>
    <li data-bind="click:function(data,event) { $root.exibirMensagem($index(),event) },
        css:{ 'nao-lida': lida() == 0 }">
    <?php endif; ?>
        <div class="row-fluid email">
            <div class="span3 nome" data-bind="html: $root.destacar(Mensagem.Usuario.nome)">
            </div>
            <div class="span6 assunto">
                <?php if($usuario['Usuario']['grupo'] == 'atendimento') : ?>
                <span data-bind="html: $root.destacar(Mensagem.Assunto.nome) + ': '"></span>
                <?php else : ?>
                <span data-bind="html: $root.destacar(Mensagem.Assunto.Item.nome) + ': '"></span>
                <?php endif; ?>
                <span data-bind="html: $root.destacar(decode(Mensagem.texto))" class="trecho"></span>
            </div>
            <div class="span3 data">
                <span data-bind="text: $root.formataData(Mensagem.data)">
                </span>
                <?php if($usuario['Usuario']['grupo'] != 'atendimento') : ?>
                <!-- ko if: Mensagem.anexos > 0 -->
                <i class="icon-attachment pull-right"></i>
                <!-- /ko -->
                <!-- ko if: Mensagem.anexos == 0 -->
                <i class="icon-attachment pull-right none"></i>
                <!-- /ko -->
                <!-- ko if: Mensagem.Assunto.pendencia == 'Informativo' -->
                <i class="icon-thumbs-up pull-right none"></i>
                <!-- /ko -->
                <!-- ko if: Mensagem.Assunto.pendencia != 'Informativo' && Mensagem.Assunto.resolvido == 1 -->
                <i class="icon-thumbs-up pull-right"></i>
                <!-- /ko -->
                <!-- ko if: Mensagem.Assunto.pendencia == 'As' && Mensagem.Assunto.resolvido == 0 -->
                <i class="icon-diamonds fg-color-red pull-right"></i>
                <!-- /ko -->
                <!-- ko if: Mensagem.Assunto.pendencia == 'Comissao' && Mensagem.Assunto.resolvido == 0 -->
                <i class="icon-user pull-right"></i>
                <!-- /ko -->
                <?php endif; ?>
            </div>
        </div>
    </li>
</script>
<script type="text/html" id="tmpPagination">
    <li class="line">
        <div class="row-fluid email">
            <div class="span12">
                <!-- ko if: paginaAtual() > 1 -->
                <button type="button" class="mini bg-color-blue"
                    data-bind="click:function() { paginaAtual(paginaAnterior()) }">
                    <
                </button>
                <!-- /ko -->
                <!-- ko if: paginaAtual() == 1 -->
                <button type="button" class="mini" disabled="disabled">
                    <
                </button>
                <!-- /ko -->
                <p class="help-inline">
                    P&aacute;gina <span data-bind="text: paginaAtual()"></span>&nbsp;&nbsp;
                </p>
                <!-- ko if: paginaAtual() < maximoPaginas() -->
                <button type="button" class="mini bg-color-blue"
                    data-bind="click:function() { paginaAtual(proximaPagina()) }">
                    >
                </button>
                <!-- /ko -->
                <!-- ko if: paginaAtual() == maximoPaginas() -->
                <button type="button" class="mini" disabled="disabled">
                    >
                </button>
                <!-- /ko -->
                <p class="help-inline">
                    <span data-bind="text: primeiroItemPaginaAtual()+1"></span> a 
                    <span data-bind="text: ultimoItemPaginaAtual()"></span> de 
                    <span data-bind="text: itens().length"></span> mensagens
                </p>
            </div>
        </div>
    </li>
</script>
<?=$form->hidden('tipo',array('value' => $tipo,'id' => 'tipo'))?>
<?=$form->hidden('usuario',array('value' => $usuario['Usuario']['id'],'id' => 'usuario_id'))?>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" id="recarregar" data-bind="click: function() { reload() }"></a>
            Mensagens
            <?php if($exibirCronograma) : ?>
            <button type="button" class="pull-right button default"
                href="/<?=$usuario['Usuario']['grupo']?>/mensagens/cronograma" data-bind="click:loadThis">
                Visão Cronograma
                <i class="icon-stats-up"></i>
            </button>
            <?php endif; ?>
            <?php if($eFuncionario) : ?>
            <button type="button" class="pull-right bg-color-orange"
                id="novo-informativo">
                Enviar Informativo
                <i class="icon-broadcast"></i>
            </button>
            <?php endif; ?>
            <?php if($usuario['Usuario']['grupo'] == 'formando' && $tipo != 'informativos') : ?>
            <button type="button" class="pull-right bg-color-greenDark"
                id="novo-contato">
                Nova Mensagem
                <i class="icon-mail"></i>
            </button>
            <?php endif; ?>
            <?php if($usuario['Usuario']['grupo'] == 'atendimento') : ?>
            <button type="button" class="pull-right bg-color-greenDark"
                id="buscar-formando">
                Nova Mensagem
                <i class="icon-mail"></i>
            </button>
            <?php endif; ?>
        </h2>
    </div>
</div>
<?php if($usuario['Usuario']['grupo'] == 'formando' && $tipo != 'informativos') : ?>
<div class="row-fluid">
    <h4><i class="icon-phone"></i> (11) 5585-1294</h4>
    <h4><i class="icon-mail"></i> <a href="mailto:atendimento@eventos.as">atendimento@eventos.as</a></h4>
</div>
<br />
<?php endif; ?>
<?php $session->flash(); ?>
<div class="row-fluid" data-bind="stopBinding: true">
    <div class="row-fluid well" id="email">
        <ul class="lista-email">
            <li class="opcoes">
                <div class="row-fluid email">
                    <div class="span6">
                        <span id='carregando'
                            data-bind="css: { carregado:true, hide: $root.carregado() } ">
                            Carregando
                        </span>
                        <input type="text" class="input-large" placeholder="Buscar"
                            style="font-size:13px" data-bind="value:textoBusca,
                            valueUpdate: 'afterkeydown', css: { hide: !$root.carregado() }"/>
                    </div>
                    <div class="span6">
                        <button type="button" class="mini button bg-color-blueDark pull-right"
                            data-bind='click:function() { $root.ordenar("nao-lida") }'>
                            N&atilde;o Lidas
                            <i data-bind="css: { 'icon-arrow-down':$root.ordem() == 'nao-lida' && $root.ordemDir() == 'desc',
                                'icon-arrow-up':$root.ordem() == 'nao-lida' && $root.ordemDir() == 'asc' }"></i>
                        </button>
                        <div class="btn-group dropdown pull-right">
                            <button type="button" class="mini button default dropdown-toggle" data-toggle="dropdown">
                                Qtde Por Pagina <span data-bind="text: $root.itensPorPagina()"></span>
                            </button>
                            <ul class="dropdown-menu mini itens-por-pagina" data-bind="template: {
                                name: 'tmpItensPorPagina', foreach: opcoesItensPorPagina }">
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <ul class="lista-email">
            <li class="header">
                <div class="row-fluid email">
                    <div class="span3 order">
                        <div data-bind='click:function() { $root.ordenar("nome") }'>
                            <span>De</span>
                            <i data-bind="css: { 'icon-arrow-down':$root.ordem() == 'nome' && $root.ordemDir() == 'desc',
                                'icon-arrow-up':$root.ordem() == 'nome' && $root.ordemDir() == 'asc' }"></i>
                        </div>
                    </div>
                    <div class="span6 order">
                        <div data-bind='click:function() { $root.ordenar("assunto") }'>
                            <span>Assunto</span>
                            <i data-bind="css: { 'icon-arrow-down':$root.ordem() == 'assunto' && $root.ordemDir() == 'desc',
                                'icon-arrow-up':$root.ordem() == 'assunto' && $root.ordemDir() == 'asc' }"></i>
                        </div>
                    </div>
                    <div class="span2 order">
                        <div data-bind='click:function() { $root.ordenar("data") }'>
                            <span>Data</span>
                            <i data-bind="css: { 'icon-arrow-down':$root.ordem() == 'data' && $root.ordemDir() == 'desc',
                                'icon-arrow-up':$root.ordem() == 'data' && $root.ordemDir() == 'asc' }"></i>
                        </div>
                    </div>
                    <div class="span1 order">
                        <div class="pull-right" style="line-height:30px" data-bind='click:function() { $root.ordenar("anexos") }'>
                            <span class="icon-attachment"></span>
                        </div>
                        <div class="pull-right" style="line-height:30px"
                            data-bind='click:function() { $root.ordenar("pendencias") }'>
                            <span class="icon-warning"></span>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <ul class="lista-email tmp" data-bind="template: {
            name: 'tmpListaEmails', foreach: itensPaginaAtual,
            afterAdd: showMessage, beforeRemove: hideMessage }">
        </ul>
        <ul class="lista-email" data-bind="template: { name: 'tmpPagination',
            if: $root.itens().length > 0, data: $root }">
        </ul>
    </div>
</div>