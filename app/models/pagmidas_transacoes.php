<?php

class PagmidasTransacoes extends Model {
	var $name = "PagMidasTransacoes";
	var $actsAs = array('Containable');
	var $useTable = 'pagmidas_transacoes';
	var $belongsTo = array(
		'PagmidasItens' => array(
			'className' => 'PagmidasItens',
			'foreignKey' => false,
			'conditions' => array('PagmidasTransacoes.id = PagmidasItens.transacao_id')
		),
		'PagmidasCartoes' => array(
			'className' => 'PagmidasCartoes',
			'foreignKey' => false,
			'conditions' => array('PagmidasTransacoes.id = PagmidasCartoes.transacao_id')
		),
		'ViewFormandos' => array(
			'className' => 'ViewFormandos',
			'foreignKey' => false,
			'conditions' => array('PagmidasTransacoes.usuario_id_formando = ViewFormandos.id')
		)
	);
	var $hasMany = array(
		'PagmidasResponses' => array(
			'className' => 'PagmidasResponses',
			'foreignKey' => 'transacao_id'
		),
	);
	var $validate = array (
		'valor' => array (
			'rule' => 'notEmpty',
			'required' => true,
			'message' => 'Digite o valor da compra'
		)
	);
}
?>