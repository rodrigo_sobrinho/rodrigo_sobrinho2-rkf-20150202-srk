<?php if($atividade) : ?>
<script type="text/javascript">
    $(document).ready(function() {
        var context = ko.contextFor($("#content-body")[0]);
        context.$data.removerAviso('<?=$atividade['Atividade']['id']?>','atividade');
        
        $(".modal").on('click','.editar-atividade',function() {
            var button = this;
            $(".modal-body").html('Carregando').load("/calendario/editar/"+$(button).data('id'));
        });
        $(".modal").on('click','.apagar-atividade',function() {
            var button = this;
            bootbox.confirm('Deseja apagar o evento?',function(response) {
                if(response) {
                    context.$data.showLoading(function() {
                        var dados = {
                            data : {
                                id : $(button).data('id')
                            }
                        };
                        var url = "/calendario/apagar/";
                        $.ajax({
                            url : url,
                            data : dados,
                            type : "POST",
                            dataType : "json",
                            complete : function() {
                                context.$data.page('/calendario/exibir');
                                bootbox.hideAll();
                            }
                        });
                    });
                }
            });
        });
    });
</script>
<?php if($podeEditar) : ?>
<div class="row-fluid" id="opcoes-eventos">
    <button type="button" class="default mini editar-atividade"
        data-id="<?=$atividade['Atividade']['id']?>"
        data-titulo="<?=$atividade['Atividade']['nome']?>">
        Editar
    </button>
    <button type="button" class="bg-color-red mini apagar-atividade"
        data-id="<?=$atividade['Atividade']['id']?>">
        Apagar
    </button>
</div>
<?php endif; ?>
<div class="alert alert-info">
    <em>
        Evento cadastrado em <b><?=date('d/m/Y',strtotime($atividade['Atividade']['data_cadastro']))?></b> 
        por <b><?=$atividade['Usuario']['nome']?></b>
    </em>
</div>
<br />
<div class='row-fluid row-evento'>
    <div class="span3"><em>In&iacute;cio</em></div>
    <div class="span9 strong">
        <?=date('d/m/Y &\a\g\r\a\v\e;\s H:i:s',strtotime($atividade['Atividade']['data_inicio']))?>
    </div>
</div>
<div class='row-fluid row-evento'>
    <div class="span3"><em>Fim</em></div>
    <div class="span9 strong">
        <?php if(!empty($atividade['Atividade']['data_fim'])) : ?>
        <?=date('d/m/Y &\a\g\r\a\v\e;\s H:i:s',strtotime($atividade['Atividade']['data_fim']))?>
        <?php else : ?>
        Indefinido
        <?php endif; ?>
    </div>
</div>
<div class='row-fluid row-evento'>
    <div class="span3"><em>Local</em></div>
    <div class="span9 strong">
        <?=$atividade['Atividade']['local']?> 
        <?php if(!empty($atividade['AtividadeLocal']['id'])) : ?>
        (<?=$atividade['AtividadeLocal']['nome']?>)
        <?php endif; ?>
    </div>
</div>
<div class='row-fluid row-evento'>
    <div class="span3"><em>N&ordm; Pessoas</em></div>
    <div class="span9 strong">
        <?=$atividade['Atividade']['numero_pessoas'] > 0 ? $atividade['Atividade']['numero_pessoas'] : "N&atilde;o informado"?>
    </div>
</div>
<div class='row-fluid row-evento'>
    <div class="span3"><em>Departamentos</em></div>
    <div class="span9 strong">
        <?php if(!empty($atividade['Atividade']['grupos'])) : ?>
        <?php foreach(explode(",",$atividade['Atividade']['grupos']) as $grupo) : ?>
        <span class="label">
            <?=ucfirst($grupo)?>
        </span>
        <?php endforeach; ?>
        <?php else : ?>
        Nenhum departamento foi convidado
        <?php endif; ?>
    </div>
</div>
<div class='row-fluid row-evento'>
    <div class="span3"><em>Turmas Convidadas</em></div>
    <div class="span9 strong">
        <?php if(empty($atividade['AtividadeTurma'])) : ?>
        Nenhuma turma foi convidada
        <?php else : ?>
        <?php foreach($atividade['AtividadeTurma'] as $atividadeTurma) : ?>
        <span class="label label-info">
            Turma <?=$atividadeTurma['turma_id']?>
            <?php if(!empty($atividadeTurma['grupos'])) : ?>
             - <?=$atividadeTurma['grupos']?>
            <?php endif; ?>
        </span>
        <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>
<div class='row-fluid row-evento'>
    <div class="span3"><em>Descrição</em></div>
    <div class="span9 strong">
        <?=$atividade['Atividade']['descricao']?>
    </div>
</div>
<?php else : ?>
<h2 class="fg-color-red">Evento nao encontrado</h2>
<?php endif; ?>
