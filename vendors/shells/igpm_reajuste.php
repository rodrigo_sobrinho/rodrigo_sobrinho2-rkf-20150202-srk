<?php

class IgpmReajusteShell extends Shell {

    var $uses = array(
        'Turma',
        'ViewFormandos'
    );
    
    function initialize(){
        parent::initialize();
        App::import('Component', 'mail');
        $this->Mailer = new Mail();
    }
    
    function main(){
        ini_set('memory_limit', '1024M');
        set_time_limit(0);
        App::import('Component', 'Financeiro');
        $financeiro = new FinanceiroComponent();
        $date_start = date("Y/m/d", strtotime('-6 days'));
        $date_end = date("Y/m/d");
        $this->Turma->unBindModelAll();
        $turmas = $this->Turma->find('all', array(
            'conditions' => array(
                'Turma.data_igpm BETWEEN ? AND ?' => array($date_start, $date_end),
                'Turma.status' => array('fechada')
            )
        ));
        $resumo = array();
        foreach($turmas as $i => $turma){
            $resumo[$i] = array(
                'turma_id' => $turma['Turma']['id'] . " " . $turma['Turma']['nome'],
                'dados' => array(
                    'ativos' => 0,
                    'valor_adesoes' => 0,
                    'valor_adesoes_igpm' => 0,
                    'valor_reajustado' => 0
                )
            );
            $formandos = $this->ViewFormandos->find('all', array(
                'conditions' => array(
                    'ViewFormandos.turma_id' => $turma['Turma']['id'],
                    'ViewFormandos.valor_adesao is not null',
                    'ViewFormandos.situacao' => 'ativo'
                )
            ));
            foreach($formandos as $formando){
                if($formando['ViewFormandos']['realizou_checkout'] == 0){ 
                    if($financeiro->obterValorPagoAdesao($formando['ViewFormandos']['id']) > 0)
                        $resumo[$i]['dados']['ativos']++;
                    $resumo[$i]['dados']['valor_adesoes'] += $formando['ViewFormandos']['valor_adesao'];
                    $resumo[$i]['dados']['valor_adesoes_igpm'] += $formando['ViewFormandos']['valor_adesao'] + $financeiro->obterValorTotalIgpm($formando['ViewFormandos']['id']);
                    $resumo[$i]['dados']['valor_reajustado'] = $resumo[$i]['dados']['valor_adesoes_igpm'] - $resumo[$i]['dados']['valor_adesoes'];
                }
            }   
        }
        $inicio = date("d/m/Y", strtotime($date_start));
        $fim = date("d/m/Y", strtotime($date_end));
        if(!empty($resumo)){
            $mensagem = "<b>Segue abaixo relação de turmas com IGPM ajustadas no período de {$inicio} até {$fim}:</b><br /><br />";
            $mensagem.= "<table border='1'>";
            $mensagem.= "<thead>";
            $mensagem.= "<tr>";
            $mensagem.= "<td>ID/Nome</td><td>Ativos</td><td>Val Adesões</td><td>Val Adesões IGPM</td><td>Val Reajustado</td>";
            $mensagem.= "</tr>";
            $mensagem.= "</thead>";
            $mensagem.= "<tbody>";
            foreach($resumo as $res){
                $mensagem.= "<tr>";
                $mensagem.= "<td>{$res['turma_id']}</td><td>{$res['dados']['ativos']}</td><td>R$ " . number_format($res['dados']['valor_adesoes'], 2, ',', '.') . "</td><td>R$ " . number_format($res['dados']['valor_adesoes_igpm'], 2, ',', '.') . "</td>" . "</td><td>R$ " . number_format($res['dados']['valor_reajustado'], 2, ',', '.') . "</td>";
                $mensagem.= "</tr>";
            }
            $mensagem.= "</tbody>";
            $mensagem.= "</table>";
        }else{
            $mensagem = "<b>Não foram encontradas turmas com IGPM ajustado no período de {$inicio} até {$fim}.</b><br /><br />";
        }
        $this->Mailer->IsHTML(true);
        $this->Mailer->AddAddress('nelida@eventos.as','Nélida Macias');
        $this->Mailer->AddAddress('bethisa.sonoda@eventos.as','Bethisa Sonoda');
        $this->Mailer->FromName = utf8_decode("Sistema AS Formaturas");
        $this->Mailer->Subject = utf8_decode("IGPMs ajustados no período...");
        $this->Mailer->Body = utf8_decode($mensagem);
        $this->Mailer->Send();
        $this->Mailer->ClearAddresses();
    }
    
    function contratos_para_ajustar(){
        ini_set('memory_limit', '1024M');
        set_time_limit(0);
        App::import('Component', 'Financeiro');
        $financeiro = new FinanceiroComponent();
        $date_start = date("Y-m-d", strtotime('-1 year'));
        $date_end = date("Y-m-d", strtotime('-1 year', strtotime("+6 days")));
        $this->Turma->unBindModelAll();
        $turmas = $this->Turma->find('all', array(
            'conditions' => array(
                'Turma.data_assinatura_contrato BETWEEN ? AND ?' => array($date_start, $date_end),
                'Turma.status' => 'fechada'
            )
        ));
        $resumo = array();
        foreach($turmas as $i => $turma){
            $date = $turma['Turma']['data_assinatura_contrato']; 
            $strtotime = strtotime($date); 
            $newDate = strtotime('+ 1 year', $strtotime);
            $dataFormatada = date('d/m/Y', $newDate); 
            $resumo[$i] = array(
                'turma_id' => $turma['Turma']['id'] . " " . $turma['Turma']['nome'],
                'dados' => array(
                    'ativos' => 0,
                    'valor_adesoes' => 0,
                    'data_aplicacao_igpm' => $dataFormatada,
                )
            );
            $formandos = $this->ViewFormandos->find('all', array(
                'conditions' => array(
                    'ViewFormandos.turma_id' => $turma['Turma']['id'],
                    'ViewFormandos.valor_adesao is not null'
                )
            ));
            foreach($formandos as $formando){
                if($formando['ViewFormandos']['realizou_checkout'] == 0){
                    if($financeiro->obterValorPagoAdesao($formando['ViewFormandos']['id']) > 0)
                        $resumo[$i]['dados']['ativos']++;
                    $resumo[$i]['dados']['valor_adesoes'] += $formando['ViewFormandos']['valor_adesao'];
                }
            }
        }
        $date_start = strtotime($date_start);
        $date_end = strtotime($date_end);
        $inicio = date("d/m/Y", strtotime("+1 year", $date_start));
        $fim = date("d/m/Y", strtotime("+1 year", $date_end));
        if(!empty($resumo)){
            $mensagem = "<b>Segue abaixo relação de turmas que o IGPM será ajustado no período de {$inicio} até {$fim}:</b><br /><br />";
            $mensagem.= "<table border='1'>";
            $mensagem.= "<thead>";
            $mensagem.= "<tr>";
            $mensagem.= "<td>ID/Nome</td><td>Ativos</td><td>Val Adesões</td><td>Data Aplicação</td>";
            $mensagem.= "</tr>";
            $mensagem.= "</thead>";
            $mensagem.= "<tbody>";
            foreach($resumo as $res){
                $mensagem.= "<tr>";
                $mensagem.= "<td>{$res['turma_id']}</td><td>{$res['dados']['ativos']}</td><td>R$ " . number_format($res['dados']['valor_adesoes'], 2, ',', '.') . "</td><td>" . $res['dados']['data_aplicacao_igpm'] . "</td>";
                $mensagem.= "</tr>";
            }
            $mensagem.= "</tbody>";
            $mensagem.= "</table>";
        }else{
            $mensagem = "<b>Não foram encontradas turmas com IGPM para serem ajustados no período de {$inicio} até {$fim}.</b><br /><br />";
        }
        $this->Mailer->IsHTML(true);
        $this->Mailer->AddAddress('nelida@eventos.as','Nélida');
        $this->Mailer->AddAddress('claudiocruz@eventos.as','Claudio');
        $this->Mailer->FromName = utf8_decode("Sistema AS Formaturas");
        $this->Mailer->Subject = utf8_decode("IGPMs para ajustar no período...");
        $this->Mailer->Body = utf8_decode($mensagem);
        $this->Mailer->Send();
        $this->Mailer->ClearAddresses();
    }
    

}

?>
