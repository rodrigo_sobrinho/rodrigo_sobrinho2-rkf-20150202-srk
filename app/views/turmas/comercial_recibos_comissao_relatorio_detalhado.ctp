<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<?php $session->flash(); ?>
<?php if (sizeof($formandos) > 0) : ?>
<div class="row-fluid">
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="10%">
                    Código Formando
                </th>
                <th scope="col" width="40%">
                    Nome
                </th>
                <th scope="col" width="25%">
                    Valor Pago
                </th>
                <th scope="col" width="25%">
                    Data Cadastro
                </th>
            </tr>
        </thead>
        <tbody>
            <?php  
            foreach($formandos as $formando => $f) :
            ?>
            <tr>
                <td><?=$f['Formando']['codigo_formando'];?></td>
                <td><?=$f['Formando']['nome'];?></td>
                <td>R$ <?=number_format($f['ReciboComissaoUsuario']['valor_pago'], 2, ',', '.'); ?></td>
                <td><?=date('d/m/Y &\a\g\r\a\v\e;\s H:i:s',strtotime($f['ReciboComissaoUsuario']['data_cadastro']));?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="2" class="paginacao"></td>
                <td class="fg-color-red" colspan="1">Formandos: <?=count($formandos)?></td>
                <td class="fg-color-greenDark">Recebidos: R$ <?=number_format($f['ReciboComissao']['valor'], 2, ',', '.'); ?></td>
            </tr>
        </tfoot>
    </table>
<?php else : ?>
    <h2 class="fg-color-red">Nenhum recibo de comissão encontrado.</h2>
<?php endif; ?>
</div>