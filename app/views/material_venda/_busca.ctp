<div class="linha-titulo">
    <div class="text-danger text-uppercase titulo">
        <?=count($catalogos)?> registros encontrados com "<?=$filtro?>"
        <div class="visualizacao text-muted">
            <span>Visualização</span>
            <a href="/<?=$this->params['prefix']?>/material_venda/busca/grade"
                class="<?=$view == 'grade' ? 'text-muted' : 'text-danger'?>">
                <i class="faicon faicon-lg faicon-th-large"></i>
                grade
            </a>
            <a href="/<?=$this->params['prefix']?>/material_venda/busca/lista"
                class="<?=$view == 'lista' ? 'text-muted' : 'text-danger'?>">
                <i class="faicon faicon-lg faicon-th-list"></i>
                lista
            </a>
        </div>
    </div>
</div>
<?php if(count($catalogos) > 0) : ?>
<?php include("itens_{$view}.ctp"); ?>
<?php else : ?>
<h3 class="text-danger">Nenhum registro com o termo "<?=$filtro?>" foi encontrado</h3>
<?php endif; ?>