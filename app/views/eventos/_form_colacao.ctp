<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Beca</label>
	<?php echo $form->input('beca', array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Cor da Faixa</label>
	<?php echo $form->input('cor_da_faixa', array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>

<p class="grid_11 alpha omega">
	<label class="grid_5 alpha">Capelo</label>
	<?php echo $form->input('capelo', array('class' => 'grid_8 first alpha omega', 'label' => false, 'div' => false, 'error' => array('wrap' => 'span', 'class' => 'grid_8'))); ?>
</p>