<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<script type="text/javascript">
$(document).ready(function() {
    $('.detalhar').click(function() {
        bootbox.dialog('Carregando',[{
            label: 'Fechar'
        }],{
            remote: '<?="/{$this->params['prefix']}/turmas/recibos_comissao_relatorio_detalhado/"?>' + $(this).attr('dir')
        });
    });
});
</script>
<?php $session->flash(); ?>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button back" data-bind="click: function() {
               page('<?="/{$this->params['prefix']}/turmas/listar_comissao_consolidado"?>') }"></a>
            Recibos Comissões
        </h2>
    </div>
</div>
<?php if (sizeof($recibosFormandos) > 0) : ?>
<div class="row-fluid">
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="25%">
                    Total Formandos
                </th>
                <th scope="col" width="25%">
                    Valor
                </th>
                <th scope="col" width="25%">
                    Data Recibo
                </th>
                <th scope="col" width="25%">
                </th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach($recibosFormandos as $reciboFormando => $recibos) :
            ?>
            <tr>
                <td><?=count($recibos['ReciboComissaoUsuario']); ?></td>
                <td>R$ <?=number_format($recibos['ReciboComissao']['valor'], 2, ',', '.'); ?></td>
                <td><?=date('d/m/Y &\a\g\r\a\v\e;\s H:i:s',strtotime($recibos['ReciboComissao']['data_cadastro']));?></td>
                <td>
                    <button type="button" class="button mini bg-color-greenDark detalhar" dir="<?=$recibos['ReciboComissao']['id'];?>">
                        Detalhar
                        <i class="icon-upload"></i>
                    </button>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else : ?>
    <h2 class="fg-color-red">Nenhum formando encontrado.</h2>
<?php endif; ?>
</div>