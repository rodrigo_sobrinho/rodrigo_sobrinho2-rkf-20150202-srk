<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<div class="row-fluid">
    <h2>
        <?php if(!$this->layout) : ?>
        <a class="metro-button reload" data-bind="click: reload"></a>
        Recibo de pagamentos
        <a class="button mini default pull-right" style="margin-left:10px"
            href="<?=$this->here?>/imprimir" target="_blank">
            Imprimir
            <i class="icon-printer"></i>
        </a>
        <a class="button mini default pull-right"
            href="<?=$this->here?>/excel" target="_blank">
            Gerar Excel
            <i class="icon-file-excel"></i>
        </a>
        <?php else : ?>
        Recibo de pagamentos
        <br />
        <br />
        <?php endif; ?>
    </h2>
</div>
<h3>Detalhes do Recibo</h3>
<br />
<div class="row-fluid">
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th width="20%">Data</th>
                <th width="20%">Hora</th>
                <th width="30%">Financeiro Respons&aacute;vel</th>
                <th width="30%">Atendente Respons&aacute;vel</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <?=date('d/m/Y',strtotime($recibo['CheckoutRecibo']['data_cadastro']))?>
                </td>
                <td>
                    <?=date('H:i',strtotime($recibo['CheckoutRecibo']['data_cadastro']))?>
                </td>
                <td><?=$recibo['Financeiro']['nome']?></td>
                <td><?=$recibo['Atendente']['nome']?></td>
            </tr>
        </tbody>
    </table>
</div>
<br />
<h3>Pagamentos Relacionados</h3>
<br />
<div class="row-fluid">
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th width="30%">Formando</th>
                <th width="15%">Tipo</th>
                <th width="15%">Valor</th>
                <th width="20%">Data Vencimento</th>
                <th width="20%">Data Cadastro</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($recibo['CheckoutReciboPagamento'] as $pagamento) : ?>
            <tr>
                <td>
                    <?=$pagamento['CheckoutFormandoPagamento']['Protocolo']['Formando']['codigo_formando']?> 
                    - 
                    <?=$pagamento['CheckoutFormandoPagamento']['Protocolo']['Formando']['nome']?>
                </td>
                <td>
                    <?=ucfirst($pagamento['CheckoutFormandoPagamento']['tipo']);?>
                </td>
                <td>R$<?=number_format($pagamento['CheckoutFormandoPagamento']['valor'], 2, ',', '.') ?></td>
                <td><?=date('d/m/Y',strtotime($pagamento['CheckoutFormandoPagamento']['data_vencimento']))?></td>
                <td>
                    <?=date('d/m/Y',strtotime($pagamento['CheckoutFormandoPagamento']['data_cadastro']))?> 
                    - 
                    <?=date('H:i',strtotime($pagamento['CheckoutFormandoPagamento']['data_cadastro']))?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>