<?php
    $paginator->options(array(
        'url' => array($this->params['prefix'] => true)));
    $sortOptions = array('data-bind' => 'click: loadThis');
?>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<script type="text/javascript">
$(document).ready(function() {
    $('.alterar').click(function() {
        bootbox.dialog('Carregando',[{
            label: 'Fechar'
        }],{
            remote: '<?="/{$this->params['prefix']}/comissoes/alterar/"?>'+
                    $(this).parent().attr('dir')
        });
    });
    $('.beneficio').click(function() {
        bootbox.dialog('Carregando',[{
            label: 'Fechar'
        }],{
            remote: '<?="/{$this->params['prefix']}/campanhas/comprar_extras/"?>'+
                    $(this).data('id')
        });
    });
    <?php if($permissao) : ?>
    $('.status').click(function() {
        var status = $(this).text();
        dir = $(this).parent().attr('dir');
        ativo = $(this).attr('ativo') == 0 ? 1 : 0;
        bootbox.confirm('Tem certeza que deseja ' + status + ' este formando?',function(response) {
            if(response) {
                var context = ko.contextFor($(".metro-button.reload")[0]);
                var url = '<?="/{$this->params['prefix']}/comissoes/alterar_status/"?>'+dir+'/'+ativo;;
                context.$data.showLoading(function() {
                    bootbox.hideAll();
                    $.ajax({
                        url : url,
                        dataType : "json",
                        complete : function() {
                            context.$data.reload();
                        }
                    });
                });
            }
        })
    })
    <?php endif; ?>
})
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Membros da Comiss&atilde;o
            <?php if($this->params['prefix'] != 'comissao') : ?>
            <button type="button" class="pull-right bg-color-blue"
                href="/<?=$this->params['prefix']?>/comitiva/adicionar" data-bind="click:loadThis">
                Adicionar
                <i class="icon-contact"></i>
            </button>
            <?php endif; ?>
        </h2>
    </div>
</div>
<?php $session->flash(); ?>
<div class="row-fluid">
<?php if (sizeof($usuarios) == 0) { ?>
    <h2 class="fg-color-red">Nenhum Membro da Comiss&atilde;o Cadastrado Para Esta Turma</h2>
<?php } else { ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="25%"><?=$paginator->sort('Nome', 'Usuario.nome',$sortOptions); ?></th>
                <th scope="col" width="20%"> <?=$paginator->sort('E-mail', 'Usuario.email',$sortOptions); ?></th>
                <th scope="col" width="12%"><?=$paginator->sort('Celular', 'FormandoProfile.tel_celular',$sortOptions); ?></th>
                <th scope="col" width="15%"><?=$paginator->sort('Cargo', 'FormandoProfile.cargo_comissao',$sortOptions); ?></th>
                <th scope="col" width="3%"><?=$paginator->sort('Ativo', 'Usuario.ativo',$sortOptions); ?></th>
                <th scope="col" width="25%"></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($usuarios as $usuario): ?>
            <tr>
                <td><?=$usuario['Usuario']['nome']?></td>
                <td><?=$usuario['Usuario']['email']; ?></td>
                <td><?=$usuario['FormandoProfile']['tel_celular']; ?></td>
                <td><?=$usuario['FormandoProfile']['cargo_comissao']; ?></td>
                <td><?=$usuario['Usuario']['ativo'] == 1 ? 'Sim' : 'Não'; ?></td>
                <td dir="<?=$usuario['Usuario']['id']?>" style='width: 130px'>
                    <button class="mini bg-color-greyDark beneficio" type="button" data-id="<?=$usuario['Usuario']['id']?>">
                        Benefício
                    </button>
                    <?php if($this->params['prefix'] == 'planejamento'){ ?>
                    <button class="mini alterar default" type="button">
                        Alterar
                    </button> 
                    <?php } ?>
                    <?php if ($this->params['prefix'] == 'comercial'){ ?>
                    <a class="button mini default bg-color-blueDark" style="width: 65px"
                               data-bind="click: function() {
                               page('<?= "/{$this->params['prefix']}/usuarios/editar_dados/pessoais/{$usuario['Usuario']['id']}" ?>') }">
                                Alterar
                    </a> 
                    <?php } ?>
                    <?php if($permissao) : ?>
                    <?php if($usuario['Usuario']['ativo'] == 0) { ?>
                    <button class="mini status bg-color-blue" type="button" ativo="<?=$usuario['Usuario']['ativo']?>">
                        Ativar
                    </button>
                    <?php } else { ?>
                    <button class="mini status bg-color-red" type="button" ativo="<?=$usuario['Usuario']['ativo']?>">
                        Desativar
                    </button>
                    <?php } ?>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="6" class="paginacao">
                    <?=$paginator->numbers(array('separator' => ' ','data-bind' => 'click: loadThis')); ?>
                </td>
            </tr>
        </tfoot>
    </table>
<?php } ?>
</div>
