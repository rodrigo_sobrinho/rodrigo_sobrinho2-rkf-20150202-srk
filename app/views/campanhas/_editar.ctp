<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootstrap/select.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/bootstrap/datepicker.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/datepicker.js"></script>
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<style type="text/css">
.image-button { padding-right:10px; margin-right:40px }
.remover-extra { position: absolute; top:0; right:-35px; min-width:35px;
        width: 35px; height: 32px }
.remover-extra i { margin-left: 0!important; position:relative; top:12px }
.remover-extra:hover { background: #333333!important }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        var context = ko.contextFor($("#content-body")[0]);
        $(".datepicker").datepicker();  
        $('.selectpicker').selectpicker({width:'100%'});
        $("#adicionar-extra").click(function(e) {
            e.preventDefault();
            var button = this;
            $(".button-extra").fadeOut(500);
            $(button).fadeOut(500,function() {
                $(button).next().fadeIn(500,function() {
                    liberarSelecaoExtra();
                    $("#extra").fadeIn(500,function() {
                        $("#enviar-form").attr('disabled','disabled');
                        context.$data.loaded(true);
                    });
                });
            });
        });
        $("#cancelar-extra").click(function(e) {
            e.preventDefault();
            var div = $(this).parent();
            $("#extra").fadeOut(500,function() {
                $("#extra").find('.completar').val('');
                $('#select-extra').html('');
                $("#extra").find('.selectpicker').selectpicker('refresh');
                $("#extras").find('.editando').removeClass("editando");
                div.fadeOut(500,function() {
                    $("#adicionar-extra").fadeIn(500,function() {
                        $(".button-extra").fadeIn(500,function() {
                            $("#enviar-form").removeAttr('disabled');
                            context.$data.loaded(true);
                        });
                    });
                });
            });
        });
        $('#select-evento').change(function() {
            var options = '';
            $('#select-extra').attr('disabled','disabled');
            if($(this).val() != "") {
                $('#select-extra')
                        .html('<option value="">Buscando</option>')
                        .selectpicker('refresh');
                var url = '/campanhas/listar_extras/'+$(this).val();
                $.get(url,function(response) {
                    var data = $.parseJSON(response);
                    if(data.extras.length > 0) {
                        options = '<option value="">Selecione o Extra</option>';
                        $.each(data.extras,function() {
                            options+= "<option value='" + this.Extra.id + "'>" +
                                this.Extra.nome + "</option>";
                        });
                        $('#select-extra').html(options);
                        if($(".button-extra.editando").length > 0)
                            $('#select-extra').val($(".button-extra.editando").data('extra-id'));
                        liberarSelecaoExtra();
                    } else {
                        options = '<option value="">Nenhum Extra Pra Esse Evento</option>';
                        $('#select-extra').html(options);
                    }
                    $('#select-extra').selectpicker('refresh');
                });
            } else {
                options = '<option value="">Selecione o Evento</option>';
                $('#select-extra').html(options).selectpicker('refresh');
            }
        });
        $("#select-extra").on('change', function(){
            var itemSelecionado = $('#select-extra option').filter(':selected').text();
            var itens = ["", "Convite Infantil", "Convite Luxo", "Convite Extra", "Combo" ,"Mesa Extra", "Brinde", "Outros"];
            if($.inArray(itemSelecionado, itens)){
                if(itemSelecionado.substr(0, 13) == "Convite Extra"){
                    $('#extraQuantidadeConvites').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').val(0);
                    $('#extraQuantidadeConvites').val(1);
                }
                if(itemSelecionado.substr(0, 16) == "Convite Infantil"){
                    $('#extraQuantidadeConvites').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').val(0);
                    $('#extraQuantidadeConvites').val(0);
                }
                if(itemSelecionado.substr(0, 12) == "Convite Luxo"){
                    $('#extraQuantidadeConvites').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').val(0);
                    $('#extraQuantidadeConvites').val(0);
                }
                if(itemSelecionado.substr(0, 6) == "Brinde"){
                    $('#extraQuantidadeConvites').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').val(0);
                    $('#extraQuantidadeConvites').val(0);
                }
                if(itemSelecionado.substr(0, 6) == "Outros"){
                    $('#extraQuantidadeConvites').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').val(0);
                    $('#extraQuantidadeConvites').val(0);
                }
                if(itemSelecionado.substr(0, 10) == "Mesa Extra"){
                    $('#extraQuantidadeConvites').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').val(1);
                    $('#extraQuantidadeConvites').val(0);
                }
                if(itemSelecionado.substr(0, 5) == "Combo"){
                    $('#extraQuantidadeConvites').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').val(1);
                    $('#extraQuantidadeConvites').val(10);
                }
            }else{
                $('#extraQuantidadeConvites').removeAttr('disabled');
                $('#extraQuantidadeMesas').removeAttr('disabled');
                $('#extraQuantidadeMesas').val('');
                $('#extraQuantidadeConvites').val('');
            }
        });
        
        $("#salvar-extra").click(function(e) {
            e.preventDefault();
            var completar = false;
            $("#extra").find('.completar:not(div)').each(function() {
                if($(this).val() == "")
                    completar = true;
            });
            if(completar) {
                bootbox.alert('Complete todos os campos');
            } else {
                var eventoId = $('#select-evento').val();
                var evento = $('#select-evento').find('option[value="'+eventoId+'"]').text();
                var extraId = $('#select-extra').val();
                var extra = $('#select-extra').find('option[value="'+extraId+'"]').text();
                if($(".button-extra.editando").length > 0)
                    var button = $(".button-extra.editando");
                else
                    var button = $("<button>",{class: 'image-button hide button-extra'}).appendTo($('#extras'));
                button.attr({
                    type: 'button',
                    class: 'image-button hide button-extra'
                });
                $('#extraQuantidadeConvites').removeAttr('disabled');
                $('#extraQuantidadeMesas').removeAttr('disabled');
                button.data({
                    'evento-id': eventoId,
                    'extra-id': extraId,
                    'quantidade-minima': $('#extraQuantidadeMinima').val(),
                    'quantidade-maxima': $('#extraQuantidadeMaxima').val(),
                    'quantidade-convites': $('#extraQuantidadeConvites').val(),
                    'quantidade-mesas': $('#extraQuantidadeMesas').val(),
                    'quantidade-disponivel': $('#extraQuantidadeDisponivel').val(),
                    'valor': $('#extraValor').val(),
                    'ativo': $('#extraAtivo').val()
                });
                button.text(evento + " - " + extra + ": R$" + $('#extraValor').val());
                $("<span>",{
                    class:'bg-color-blue remover-extra',
                    html: '<i class="icon-pencil"></i>'
                }).appendTo(button);
                $("#cancelar-extra").trigger('click');
            }
        });
        $('#extras').on('click','.remover-extra', function(e) {
            e.preventDefault();
            var button = $(this).parent();
            $('#select-evento').val(button.data('evento-id')).trigger('change').selectpicker('refresh');
            
            $.each(button.data(),function(c,v) {
                if(c == 'ativo')
                    $('#extraAtivo').val(v).trigger('change').selectpicker('refresh');
                else
                    $("#extra").find('[data-tipo="'+camelToUnderscore(c).replace('-','_')
                            +'"]').val(v);
            });
            button.addClass('editando');
            $("#adicionar-extra").trigger('click');
        });
        $("#form").submit(function(e) {
            e.preventDefault();
            $(".button-extra").each(function(i) {
                $.each($(this).data(),function(c,v) {
                    $("<input>",{
                        type: 'hidden',
                        name: 'data[CampanhasExtra]['+i+']['+camelToUnderscore(c).replace('-','_')+']',
                        value: v
                    }).appendTo($("#form"));
                });
                if($("#CampanhaId").val() != "")
                    $("<input>",{
                        type: 'hidden',
                        name: 'data[CampanhasExtra]['+i+'][campanha_id]',
                        value: $("#CampanhaId").val()
                    }).appendTo($("#form"));
            });
            $("#extra").remove();
            var dados = $("#form").serialize();
            var url = $("#form").attr("action");
            context.$data.showLoading(function() {
                $.ajax({
                    url: url,
                    data: dados,
                    type: "POST",
                    dataType: "json",
                    complete: function() {
                        context.$data.page('/<?=$this->params['prefix']?>/campanhas/listar');
                    }
                });
            });
        });
        
        function liberarSelecaoExtra() {
            if($(".button-extra.editando").length > 0) {
                if($(".button-extra.editando").data('id') != undefined) {
                    $('#select-evento').attr('disabled','disabled').selectpicker('refresh');
                    $('#select-extra').attr('disabled','disabled').selectpicker('refresh');
                    $('#extraQuantidadeConvites').attr('disabled','disabled');
                    $('#extraQuantidadeMesas').attr('disabled','disabled');
                } else {
                    $('#select-evento').removeAttr('disabled').selectpicker('refresh');
                    $('#select-extra').removeAttr('disabled').selectpicker('refresh');
                }
            } else {
                $('#select-evento').removeAttr('disabled').selectpicker('refresh');
                $('#select-extra').removeAttr('disabled').selectpicker('refresh');
            }
        }
    });
</script>
<div class="row-fluid">
    <h2>
        <a class="metro-button reload" data-bind="click: reload"></a>
        <a class="metro-button back" data-bind="click: function() { 
            page('<?="/{$this->params['prefix']}/campanhas/listar"?>') }">
        </a>
        Campanha
    </h2>
</div>
<div class="row-fluid">
    <?php $session->flash(); ?>
    <?=$form->create('Campanha',
        array(
            'url' => "/{$this->params['prefix']}/campanhas/editar/",
            'id' => 'form'
        )); ?>
    <?=$form->hidden('Campanha.id'); ?>
    <div class="row-fluid">
        <div class="span6">
            <label>Nome</label>
            <?=$form->input('nome', array('label' => false, 'div' => 'input-control text', 'error' => false)); ?>
        </div>
        <div class="span6">
            <label>Tipo</label>
            <?=$form->input('tipo',array(
                'label' => false,
                'type' => 'select',
                'class' => 'selectpicker',
                'options' => array('comum' => 'Comum'),
                'div' => 'input-control text',
                'error' => false)); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <label>Descrição</label>
            <?=$form->input('descricao',
                array(
                    'label' => false,
                    'div' => 'input-control text',
                    'error' => false
                )); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
            <label>Máximo de parcelas</label>
            <?=$form->input('max_parcelas',
                array(
                    'label' => false,
                    'div' => 'input-control text',
                    'error' => false
                )); ?>
        </div>
        <div class="span4">
            <label>Data Início</label>
            <?=$form->input('data_inicio',
                array(
                    'label' => false,
                    'type' => 'text',
                    'class' => 'datepicker',
                    'div' => 'input-control text',
                    'error' => false
                )); ?>
        </div>
        <div class="span4 pull-right">
            <label>Data Fim</label>
            <?=$form->input('data_fim',
                array(
                    'label' => false,
                    'type' => 'text',
                    'class' => 'datepicker',
                    'div' => 'input-control text',
                    'error' => false
                ));?>
        </div>
    </div>
    <div class="row-fluid" id="extras">
        <h2>
            Extras
            <a class="button mini bg-color-greenDark pull-right"
                id="adicionar-extra">
                Adicionar Extra
            </a>
            <div class="pull-right hide inserir">
                <a class="button mini bg-color-greenDark"
                    id="salvar-extra">
                    Salvar Extra
                </a>
                <a class="button mini bg-color-red"
                    id="cancelar-extra">
                    Cancelar
                </a>
            </div>
        </h2>
        <?php foreach ($this->data['CampanhasExtra'] as $extra) : ?>
        <button type="button" class="image-button button-extra"
            data-id="<?=$extra['id']?>"
            data-extra-id="<?=$extra['Extra']['id']?>"
            data-evento-id="<?=$extra['Extra']['Evento']['id']?>"
            data-quantidade-minima="<?=$extra['quantidade_minima']?>"
            data-quantidade-maxima="<?=$extra['quantidade_maxima']?>"
            data-quantidade-disponivel="<?=$extra['quantidade_disponivel']?>"
            data-quantidade-convites="<?=$extra['quantidade_convites']?>"
            data-quantidade-mesas="<?=$extra['quantidade_mesas']?>"
            data-ativo="<?=$extra['ativo']?>"
            data-valor="<?=$extra['valor']?>">
            <?="{$extra['Extra']['Evento']['nome']} - {$extra['Extra']['nome']}: R\${$extra['valor']}"?>
            <span class="bg-color-blue remover-extra">
                <i class="icon-pencil"></i>
            </span>
        </button>
        <br />
        <?php endforeach; ?>
    </div>
    <div class="row-fluid hide" id="extra">
        <div class="row-fluid">
            <div class="span6">
                <?=$form->input('evento',array(
                    'label' => 'Evento',
                    'type' => 'select',
                    'class' => 'selectpicker completar',
                    'id' => 'select-evento',
                    'options' => $eventos,
                    'empty' => 'Selecione o Evento',
                    'div' => 'input-control text',
                    'error' => false)); ?>
            </div>
            <div class="span6">
                <?=$form->input('extra',array(
                    'label' => 'Extra',
                    'type' => 'select',
                    'id' => 'select-extra',
                    'disabled' => 'disabled',
                    'class' => 'selectpicker completar',
                    'empty' => 'Selecione o Extra',
                    'div' => 'input-control text',
                    'error' => false)); ?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span3">
                <label>Qtde Min</label>
                <?=$form->input('extra.quantidade_minima',
                    array(
                        'label' => false,
                        'type' => 'text',
                        'data-tipo' => 'quantidade_minima',
                        'class' => 'completar',
                        'div' => 'input-control text',
                        'error' => false
                    )); ?>
            </div>
            <div class="span3">
                <label>Qtde Max</label>
                <?=$form->input('extra.quantidade_maxima',
                    array(
                        'label' => false,
                        'data-tipo' => 'quantidade_maxima',
                        'class' => 'completar',
                        'div' => 'input-control text',
                        'error' => false
                )); ?>
            </div>
            <div class="span6">
                <label>Quantidade Dispon&iacute;vel</label>
                <?=$form->input('extra.quantidade_disponivel',
                    array(
                        'label' => false,
                        'data-tipo' => 'quantidade_disponivel',
                        'class' => 'completar',
                        'div' => 'input-control text',
                        'error' => false
                    )); ?>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span3">
                <label>Qtde Convites</label>
                <?=$form->input('extra.quantidade_convites',
                    array(
                        'label' => false,
                        'data-tipo' => 'quantidade_convites',
                        'div' => 'input-control text',
                        'error' => false
                )); ?>
            </div>
            <div class="span3">
                <label>Qtde Mesas</label>
                <?=$form->input('extra.quantidade_mesas',
                    array(
                        'label' => false,
                        'data-tipo' => 'quantidade_mesas',
                        'div' => 'input-control text',
                        'error' => false
                )); ?>
            </div>
            <div class="span3">
                <label>Valor (R$)</label>
                <?=$form->input('extra.valor',
                    array(
                        'label' => false,
                        'class' => 'completar',
                        'data-tipo' => 'valor',
                        'div' => 'input-control text',
                        'error' => false
                    )); ?>
            </div>
            <div class="span3">
                <?=$form->input('extra.ativo',array(
                    'label' => 'Ativo',
                    'type' => 'select',
                    'class' => 'selectpicker',
                    'data-tipo' => 'ativo',
                    'options' => array('0' => 'Não', '1' => 'Sim'),
                    'div' => 'input-control text',
                    'error' => false)); ?>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <?=$form->end(array(
            'label' => 'Salvar',
            'div' => false,
            'id' => 'enviar-form',
            'class' => 'button bg-color-blueDark'
        ));?>
    </div>
</div>