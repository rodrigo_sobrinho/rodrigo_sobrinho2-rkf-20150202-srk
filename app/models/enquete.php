<?php

class Enquete extends AppModel {

    var $name = 'Enquete';
    var $belongsTo = array('Usuario');
    var $hasMany = array(
        'EnquetePergunta' => array('order' => 'ordem'),
        'EnqueteTurma',
        'EnqueteUsuario'
    );
    
    var $periodo = array(
        'unico' => "Uma única vez",
        'mensal' => "Uma vez por mês"
    );
    var $ativa = array('Não','Sim');
    var $turmas = array(
        'todas' => 'Todas',
        'selecionadas' => 'Selecionar'
    );
    var $obrigatoria = array(
        0 => 'Não',
        1 => 'Sim'
    );
    
    function obterEnquetePorUsuario($usuario,$turma, $rejeitadas = false) {
        $status = "eu.finalizou = 1";
        if(!$rejeitadas)
            $status.= " or eu.rejeitou = 1";
        $options = array(
            'conditions' => array(
                'Enquete.ativa' => 1,
                "now() >= data_inicio",
                "now() <= coalesce(data_fim,date_add(now(),interval 1 day))",
                "Enquete.grupos like('%{$usuario['Usuario']['grupo']}%')",
                'OR' => array(
                    'Enquete.turmas' => 'todas',
                    "(select count(0) from enquete_turmas where enquete_turmas.enquete_id = Enquete.id and " .
                        "enquete_turmas.turma_id = {$turma['Turma']['id']}) > 0"
                ),
                'AND' => array(
                    'OR' => array(
                        "Enquete.periodo = 'unico' and (select count(0) from enquete_usuarios eu where " .
                            "eu.enquete_id = Enquete.id and eu.usuario_id = {$usuario['Usuario']['id']} and " .
                            "($status)) = 0",
                        "(select count(0) from enquete_usuarios eu where eu.enquete_id = Enquete.id and " .
                            "eu.usuario_id = {$usuario['Usuario']['id']} and ($status) and " .
                            "((month(now())-month(eu.data_cadastro) = 1 and " .
                            "day(eu.data_cadastro) > day(Enquete.data_inicio) and day(now()) < " .
                            "day(Enquete.data_inicio)) or (month(now()) = month(eu.data_cadastro) " .
                            "and day(eu.data_cadastro) <= day(Enquete.data_inicio) and day(now()) " .
                            "< day(Enquete.data_inicio)) or (month(now()) = month(eu.data_cadastro) " .
                            "and day(eu.data_cadastro) >= day(Enquete.data_inicio) and day(now()) >= " .
                            "day(Enquete.data_inicio)))) = 0",
                    )
                )
            )
        );
        $this->unbindModel(array(
            'hasMany' => array('EnqueteUsuario','EnqueteTurma'),
            'belongsTo' => array('Usuario')
        ),false);
        $this->recursive = 2;
        $enquete = $this->find('all',$options);
        return $enquete;
    }

}