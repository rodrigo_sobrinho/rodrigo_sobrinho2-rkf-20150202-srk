<style type="text/css">
.chzn-container { line-height:16px }
.chzn-container-single .chzn-single div b { top:2px!important; }
.chzn-container-single .chzn-single span { position:relative; top:2px }
label { font-size: 13px; margin-bottom:5px; line-height: 16px }
.mini.max { font-size:12px; height:28px }
</style>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/metroui/table.css">
<link rel="stylesheet" type="text/css" href="<?=$this->webroot?>metro/css/min/chosen.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/chosen.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $(".chosen").chosen({width:'100%'});
    $("#form-filtro").submit(function(e) {
        e.preventDefault();
        var context = ko.contextFor($(".metro-button.reload")[0]);
        context.$data.showLoading(function() {
            $("#form-filtro").find('[value="false"]').attr('disabled','disabled');
            var dados = $("#form-filtro").serialize();
            var url = $("#form-filtro").attr('action');
            $.ajax({
                url : url,
                data : dados,
                type : "POST",
                dataType : "json",
                complete : function() {
                    context.$data.reload();
                }
            });
        });
    });
    $('.historico').click(function(e) {
        e.preventDefault();
        url = "/<?=$this->params['prefix']?>/mensagens/historico_assunto/" + $(this).data('id');
        bootbox.dialog('Carregando',[{
            label: 'Fechar'
        }],{
            remote: url
        });
    });
});
</script>
<div class="row-fluid">
    <div class="span12">
        <h2>
            <a class="metro-button reload" data-bind="click: function() { reload() }"></a>
            Pendencias
        </h2>
    </div>
</div>
<?php
$session->flash();
if(sizeof($assuntos) > 0) :
    $paginator->options(array(
        'url' => array($this->params['prefix'] => true)));
    $sortOptions = array('data-bind' => 'click: loadThis'); ?>
<div class="row-fluid">
    <?=$form->create('Assunto',array(
        'url' => "/{$this->params['prefix']}/pendencias/",
        'id' => 'form-filtro')) ?>
    <div class="row-fluid">
        <div class="span4">
            <label>Turma</label>
            <?=$form->input('Turma.id',array(
                'options' => $turmas,
                'type' => 'select',
                'empty' => 'Todas as turmas',
                'class' => 'chosen',
                'label' => false,
                'div' => false)); ?>
        </div>
        <div class="span3">
            <label>&nbsp;</label>
            <button type='submit' class='mini max bg-color-red'>
                Filtrar
                <i class='icon-search-2'></i>
            </button>
        </div>
    </div>
    <?= $form->end(array('label' => false,
        'div' => false, 'class' => 'hide')); ?>
    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th scope="col" width="5%">
                    <?=$paginator->sort('Turma', 'Assunto.turma_id',$sortOptions); ?>
                </th>
                <th scope="col" width="20%">
                    <?=$paginator->sort('Nome', 'Turma.nome',$sortOptions); ?>
                </th>
                <th scope="col" width="20%">
                    <?=$paginator->sort('Item', 'Item.nome',$sortOptions); ?>
                </th>
                <th scope="col" width="20%">
                    <?=$paginator->sort('Assunto', 'assunto.nome',$sortOptions); ?>
                </th>
                <th scope="col" width="35%"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($assuntos as $assunto) : ?>
            <tr>
                <td><?=$assunto['Assunto']['turma_id']; ?></td>
                <td><?=$assunto['Turma']['nome']; ?></td>
                <td><?=$assunto['Item']['nome']; ?></td>
                <td><?=$assunto['Assunto']['nome']; ?></td>
                <td>
                    <a class="button mini bg-color-greenDark"
                        href="/<?=$this->params['prefix']?>/turmas/selecionar/<?=$assunto['Assunto']['turma_id']; ?>">
                        Selecionar Turma
                    </a>
                    <a class="button mini historico default"
                        href="#"
                        data-id="<?=$assunto['Assunto']['id']; ?>">
                        Historico de conversa
                        <i class="icon-history"></i>
                    </a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="2" class="paginacao">
                    <?=$paginator->numbers(array('separator' => ' ',
                        'data-bind' => 'click: loadThis')); ?>
                </td>
                <td colspan="2">
                    <span class="label label-info">
                    <?=$paginator->counter(array(
                        'format' => 'Total : %count% ' .  $this->name)); ?>
                    </span>
                </td>
            </tr>
        </tfoot>
    </table>
</div>
<?php else : ?>
<h2 class="fg-color-red">Nenhuma Pendencia Encontrada</h2>
<?php endif; ?>