
<span id="conteudo-titulo" class="box-com-titulo-header">Visualizar assuntos</span>
<div id="conteudo-container">
	<?php $session->flash(); ?>
	<div class="detalhes">
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Item</label>
			<span class="grid_8 alpha"><?php echo $item['Item']['nome']; ?></span>
		</p>
		<p class="grid_11 alpha omega">
			<label class="grid_5 alpha">Descrição</label>
			<span class="grid_8 alpha"><?php echo $item['Item']['descricao']; ?></span>
		</p>
		<div class="clear"></div>
		
		<div class="tabela-adicionar-item">
			<?php echo $html->link('Novo assunto',array($this->params['prefix'] => true, 'controller' => 'assuntos', 'action' => 'adicionar', $item['Item']['id'])); ?>
			<div style="clear:both;"></div>
		</div>
		<div class="container-tabela">
			<table>
				<thead>
					<tr>
						<th scope="col"><?php echo $paginator->sort('Resolvido?', 'resolvido'); ?></th>
						<th scope="col"><?php echo $paginator->sort('Pendência', 'pendencia'); ?></th>
						<th scope="col"><?php echo $paginator->sort('Assunto', 'nome'); ?></th>
						<th scope="col"><?php echo $paginator->sort('Total mensagens', 'mensagens'); ?></th>
						<th scope="col"> &nbsp;</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="4"><?php echo $paginator->counter(array('format' => 'Itens %start% ao %end% - página %page% de %pages%')); ?>
							<span class="paginacao">
								<?php echo $paginator->numbers(array('separator' => ' ')); ?>
							</span>
						</td>
						<td colspan="1"><?php echo $paginator->counter(array('format' => 'Total : %count% ' .  $this->name)); ?></td>
					</tr>
				</tfoot>
				<tbody>
				<?php $isOdd = false; ?>
				<?php foreach($assuntos as $assunto): ?>
					<?php if($isOdd):?>
						<tr class="odd">
					<?php else:?>
						<tr>
					<?php endif;?>
						<td colspan="1"><?php echo $assunto['Assunto']['resolvido'];?></td>
						<td colspan="1"><?php echo $assunto['Assunto']['pendencia'];?></td>
						<td colspan="1"><?php echo $assunto['Assunto']['nome'];?></td>
						<td colspan="1"><?php echo count($assunto['Mensagem']);?></td>
						<td colspan="1"><?php //echo $html->link('Visualizar mensagens (implementar)', array($this->params['prefix'] => true, 'controller' => 'assuntos', 'action' =>'visualizar', $assunto['Assunto']['id']), array('class' => 'submit button')); ?></td>
					</tr>
					<?php $isOdd = !($isOdd); ?>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		<p class="grid_11 alpha omega">
			<?php echo $html->link('Voltar',array($this->params['prefix'] => true, 'action' => 'index') ,array('class' => 'cancel')); ?>
		</p>
	</div>
</div>