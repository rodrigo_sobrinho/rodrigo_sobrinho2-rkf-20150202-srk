<div class="row-fluid">
    <div class="span6">
        <table class="table table-condensed table-striped">
            <tbody>
                <tr>
                    <td class="header">Data Contrato Turma</td>
                    <td>
                        <?php if(!empty($turma['data_assinatura_contrato'])) : ?>
                        <?=date('d/m/Y',strtotime($turma['data_assinatura_contrato']))?>
                        <?php else : ?>
                        Indefinida
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td class="header">Data Ades&atilde;o</td>
                    <td>
                        <?php if(!empty($formando['data_adesao'])) : ?>
                        <?=date('d/m/Y',strtotime($formando['data_adesao']))?>
                        <?php else : ?>
                        Indefinida
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td class="header">Data Igpm</td>
                    <td>
                        <?php if(!empty($turma['data_igpm'])) : ?>
                        <?=date('d/m/Y',strtotime($turma['data_igpm']))?>
                        <?php else : ?>
                        N&atilde;o Aplicado
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td class="header">Parcelas Ades&atilde;o</td>
                    <td><?=$formando['parcelas_adesao']?></td>
                </tr>
                <tr>
                    <td class="header">Convites Contrato</td>
                    <td><?=$formando['convites_contrato']?></td>
                </tr>
                <tr>
                    <td class="header">Mesas Contrato</td>
                    <td><?=$formando['mesas_contrato']?></td>
                </tr>
                <tr>
                    <td class="header">Pacote</td>
                    <td><?=$parcelamento ? $parcelamento['Parcelamento']['titulo'] : "Não disponível"?></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="span6">
        <table class="table table-condensed table-striped">
            <tbody>
                <tr>
                    <td class="header">Valor de Contrato</td>
                    <td>R$<?= number_format($totalAdesao, 2, ',', '.'); ?></td>
                </tr>
                <tr>
                    <td class="header">Valor de IGPM</td>
                    <td>R$<?= number_format($totalIGPM, 2, ',', '.'); ?></td>
                </tr>
                <tr>
                    <td class="header">Valor de Contrato c/ correção</td>
                    <td>R$<?= number_format($totalAdesao + $totalIGPM, 2, ',', '.'); ?></td>
                </tr>
                <tr>
                    <td class="header">Valor de Extras</td>
                    <td>R$<?= number_format($totalExtras, 2, ',', '.'); ?></td>
                </tr>
                <tr>
                    <td class="header">Total Recebido</td>
                    <td>R$<?= number_format($totalPago, 2, ',', '.'); ?></td>
                </tr>
                <?php if($saldoAtual < 0) : ?>
                <tr>
                    <td class="bg-color-red">Saldo Negativo</td>
                    <td class="fg-color-red strong">
                        R$<?= number_format(abs($saldoAtual), 2, ',', '.'); ?>
                    </td>
                </tr>
                <?php else : ?>
                <tr>
                    <td class="header">Saldo</td>
                    <td>
                        R$<?= number_format($saldoAtual, 2, ',', '.'); ?>
                    </td>
                </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>