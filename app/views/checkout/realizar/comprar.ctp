<?php if(count($itensExtras) > 0) : ?>
<h2 class="fg-color-red">Itens dispon&iacute;veis para compra</h2>
<br />
<table class="table table-condensed table-striped">
    <thead>
        <tr>
            <th width="40%">Item</th>
            <th width="20%">Valor Unit&aacute;rio</th>
            <th width="20%">Quantidade</th>
            <th width="20%">Total</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($itensExtras as $itemExtra) : ?>
        <tr data-item="<?=$itemExtra['id']?>"
            data-valor="<?=$itemExtra['valor']?>"
            data-checkout-quantidade-mesas="<?=$itemExtra['quantidade_mesas']?>"
            data-checkout-quantidade-convites="<?=$itemExtra['quantidade_convites']?>"
            data-label="<?=$itemExtra['titulo']?>"
            data-saldo="0">
            <td><?=$itemExtra['titulo']?></td>
            <td>R$<?=number_format($itemExtra['valor'],2,',','.');?></td>
            <?php $q = array(); for($a=0;$a<=$itemExtra['itens_por_pessoa'];$a++) $q[$a] = $a; ?>
            <td style="text-align: center">
                <?=$form->input(false,
                    array(
                        'label' => false,
                        'div' => false,
                        'error' => false,
                        'class' => 'selectpicker',
                        'type' => 'select',
                        'options' => $q,
                        'data-width' => '80%',
                        'data-container' => 'body',
                        'data-bind' => "event:{change:function(){comprarItens({$itemExtra['id']})}}",
                        'title' => 'Selecione'
                    )); ?>
            </td>
            <td class="total">R$0,00</td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php else : ?>
<h2 class="fg-color-red">Nenhum item dispon&iacute;vel para compra</h2>
<?php endif; ?>
