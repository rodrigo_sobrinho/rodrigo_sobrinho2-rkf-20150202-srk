<?php

class Atividade extends AppModel {

    var $name = 'Atividade';
    var $belongsTo = array(
        'Usuario',
        'AtividadeLocal' => array(
            'className' => 'AtividadeLocal',
            'foreignKey' => 'local_id',
            'unique' => false,
        ),
    );
    var $hasMany = array(
        'AtividadeUsuario' => array(
            'className' => 'AtividadeUsuario',
            'foreignKey' => 'atividade_id',
            'unique' => false,
        ),
        'AtividadeTurma' => array(
            'className' => 'AtividadeTurma',
            'foreignKey' => 'atividade_id',
            'unique' => false,
        )
    );
    
    var $tipo = array(
        'Reunião' => array(
            'reuniao_primeira' => '1a Reunião',
            'reuniao_orcamento' => 'Reunião Orçamento',
            'reuniao_negociacao' => 'Reunião Negociação',
            'reuniao_assinatura' => 'Reunião Assinatura Contrato',
            'reuniao_abertura' => 'Reunião Abertura Contrato',
            'reuniao_redimensionamento' => 'Reunião Redimensionamento'
        ),
        'evento_as' => 'Evento Interno AS',
        'outros' => 'Outros'
    );
    
    var $tipos = array(
        'evento_as' => 'Evento Interno AS',
        'reuniao_primeira' => '1a Reunião',
        'reuniao_orcamento' => 'Reunião Orçamento',
        'reuniao_negociacao' => 'Reunião Negociação',
        'reuniao_assinatura' => 'Reunião Assinatura Contrato',
        'reuniao_abertura' => 'Reunião Abertura Contrato',
        'reuniao_redimensionamento' => 'Reunião Redimensionamento',
        'outros' => 'Outros'
    );
    
    var $local = array(
        'AS' => array(
            'As Sao Paulo' => 'As Sao Paulo',
            'As Campinas' => 'As Campinas',
            'As Jundiai' => 'As Jundiai',
            'As Santos' => 'As Santos',
        ),
        'outros' => 'Outros',
    );
    
    function formataDataHora($strDataHora) {
        $d = explode(" ",$strDataHora);
        return implode("-",array_reverse(explode("/",$d[0])))." ".$d[1];
    }
    
    function listarAtividadesPorUsuario($uid,$turmas,$dataInicio,$dataFim) {
        $usuario = $this->Usuario->read(null, $uid);
        $this->AtividadeUsuario->unbindModel(array('belongsTo' => array('Atividade')),false);
        $this->Usuario->unbindModelAll();
        $this->recursive = 1;
        if($usuario['Usuario']['nivel'] == 'administrador')
            $nivel = "in(planejamento, comercial)";
        else
            $nivel = "like('%{$usuario['Usuario']['grupo']}%')";
        $options = array(
            'conditions' => array(
                'or' => array(
                    'Atividade.usuario_id' => $usuario['Usuario']['id'],
                    "Atividade.grupos {$nivel}",
                    'AtividadeUsuario.usuario_id' => $usuario['Usuario']['id'],
                    'and' => array(
                        'AtividadeTurma.turma_id' => $turmas,
                        'or' => array(
                            "AtividadeTurma.grupos like('%{$usuario['Usuario']['grupo']}%')"
                        )
                    )
                ),
                "data_inicio >= '$dataInicio'",
                "data_inicio <= '$dataFim'",
            ),
            'joins' => array(
                array(
                    'table' => 'atividade_usuarios',
                    'alias' => 'AtividadeUsuario',
                    'type' => 'left',
                    'foreignKey' => false,
                    'conditions' => array(
                        'AtividadeUsuario.atividade_id = Atividade.id'
                    )
                ),
                array(
                    'table' => 'atividade_turmas',
                    'alias' => 'AtividadeTurma',
                    'type' => 'left',
                    'foreignKey' => false,
                    'conditions' => array(
                        'AtividadeTurma.atividade_id = Atividade.id'
                    )
                )
            ),
            'group' => array('Atividade.id'),
            'fields' => array('Atividade.*', 'AtividadeTurma.*')
        );
        if(!in_array($usuario['Usuario']['grupo'],array('comissao','formando'))) {
            $options['conditions']['or']['and']['or']["AtividadeTurma.grupos"] = '';
            $options['conditions']['or'][] = "Atividade.grupos like('%funcionarios%')";
        }
        return $this->find('all',$options);
    }
    
    function listarAtividadesPorPeriodo($dataInicio,$dataFim) {
        $this->AtividadeUsuario->unbindModel(array('belongsTo' => array('Atividade')),false);
        $this->Usuario->unbindModelAll();
        $this->recursive = 1;
        $options = array(
            'conditions' => array(
                "data_inicio >= '$dataInicio'",
                "data_inicio <= '$dataFim'",
            )
        );
        return $this->find('all',$options);
    }
}

?>