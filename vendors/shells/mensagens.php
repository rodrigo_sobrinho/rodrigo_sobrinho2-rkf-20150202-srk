<?php

class MensagensShell extends Shell {

    var $uses = array('Mensagem', 'Item', 'Assunto', 'Arquivo',
        'Usuario','UsuarioDispositivo','MensagemUsuario',
        'MensagemDispositivo');
    var $Mailer = false;
    var $teste = false;
    var $urlSite;
    var $urlImagemRodape;
    
    private function criarMailer() {
        App::import('Component', 'mail');
        $this->Mailer = new Mail();
        $this->urlSite = Configure::read('url_site');
        $this->urlImagemRodape = "{$this->urlSite}/mensagens/logo_mensagem_usuario/";
    }
    
    function enviarNotificacoes() {
        if($this->args)
            $this->notificar($this->args[0], array_slice($this->args, 1));
    }
    
    private function obterNotificacao($mensageUsuarioId,$usuarioDispositivoId) {
        $dispositivo = $this->UsuarioDispositivo->read(null,$usuarioDispositivoId);
        $mensagem = $this->MensagemUsuario->read(null,$mensageUsuarioId);
        if(!$dispositivo) {
            return false;
        } elseif(!$mensagem) {
            return false;
        } elseif($dispositivo['UsuarioDispositivo']['aceita_notificacoes'] == 0 ||
                $dispositivo['UsuarioDispositivo']['ativo'] == 0) {
            return false;
        } elseif($mensagem['MensagemUsuario']['notificacoes'] > 2) {
            return false;
        } else {
            $dados = array(
                'mensagem_usuario_id' => $mensageUsuarioId,
                'usuario_dispositivo_id' => $usuarioDispositivoId
            );
            $notificacao = $this->MensagemDispositivo->find('first',array(
                'conditions' => array(
                    'mensagem_usuario_id' => $mensageUsuarioId,
                    'usuario_dispositivo_id' => $usuarioDispositivoId
                )
            ));
            $mensagem['MensagemUsuario']['notificacoes']++;
            if($notificacao) {
                if($notificacao['MensagemDispositivo']['tentativas'] > 2 || $notificacao['MensagemDispositivo']['enviada'] == 1)
                    return false;
                else
                    return $notificacao;
            } elseif(!$this->MensagemUsuario->save($mensagem)) {
                return false;
            } else {
                $this->MensagemDispositivo->save(array('MensagemDispositivo' => $dados));
                return $this->obterNotificacao($mensageUsuarioId, $usuarioDispositivoId);
            }
        }
    }
    
    function notificar($mensagemId, $usuariosId = array()) {
        $this->Mensagem->bindModel(array('belongsTo' => array('Assunto')), false);
        $mensagemId = $mensagemId;
        $mensagem = $this->Mensagem->read(null,$mensagemId);
        if(!$mensagem) return;
        $this->MensagemUsuario->unbindModel(array(
            'belongsTo' => array('Mensagem')), false);
        $options['conditions'] = array(
            'mensagem_id' => $mensagem['Mensagem']['id'],
            "notificacoes < 3"
        );
        if(!empty($usuariosId))
            $options['conditions']['usuario_id'] = $usuariosId;
        $this->Usuario->unbindModelAll();
        $this->Usuario->bindModel(array(
            'hasMany' => array('UsuarioDispositivo')
        ),false);
        $this->MensagemUsuario->recursive = 2;
        App::import('Component', 'AndroidNotifications');
        App::import('Component', 'IosNotifications');
        $this->AndroidNotifications = new AndroidNotificationsComponent();
        $this->IosNotifications = new IosNotificationsComponent();
        $this->IosNotifications->connect();
        $usuarios = $this->MensagemUsuario->find('all',$options);
        $texto = substr(strip_tags(html_entity_decode(nl2br($mensagem["Mensagem"]['texto']))),0,20);
        $assunto = "{$mensagem['Usuario']['nome']} - {$mensagem['Assunto']['nome']}";
        $tentarNovamente = array();
        foreach($usuarios as $usuario) {
            if($usuario['Usuario']['id'] == $mensagem['Usuario']['id']) continue;
            foreach($usuario['Usuario']['UsuarioDispositivo'] as $dispositivo) {
                $notificacaoDispositivo = $this->obterNotificacao($usuario['MensagemUsuario']['id'], $dispositivo['id']);
                if($notificacaoDispositivo) {
                    if($dispositivo['tipo'] == 'android') {
                        $this->AndroidNotifications->setDevices($dispositivo['uid']);
                        $notificacao = array(
                            'message' => $texto,
                            'title' => $assunto,
                            'tipo' => 'mensagem',
                            'mensagem_usuario_id' => $usuario['MensagemUsuario']['id']
                        );
                        $result = $this->AndroidNotifications->send($notificacao);
                        $notificacaoDispositivo['MensagemDispositivo']['tentativas']++;
                        if($result) {
                            $notificacaoDispositivo['MensagemDispositivo']['enviada'] = 1;
                            $notificacaoDispositivo['MensagemDispositivo']['data_envio'] = date('Y-m-d H:i:s');
                        } else {
                            $tentarNovamente[] = $usuario['Usuario']['id'];
                            $notificacaoDispositivo['MensagemDispositivo']['enviada'] = 0;
                            $notificacaoDispositivo['MensagemDispositivo']['data_envio'] = null;
                        }
                        $this->MensagemDispositivo->save($notificacaoDispositivo);
                    } elseif($dispositivo['tipo'] == 'ios') {
                        $this->IosNotifications->send($dispositivo['uid'],str_replace("\r", "\n", $texto),$usuario['MensagemUsuario']['id'],array(
                            'tipo' => 'mensagem',
                            'mensagem_usuario_id' => $usuario['MensagemUsuario']['id']
                        ));
                        $notificacaoDispositivo['MensagemDispositivo']['tentativas']++;
                        $notificacaoDispositivo['MensagemDispositivo']['enviada'] = 1;
                        $notificacaoDispositivo['MensagemDispositivo']['data_envio'] = date('Y-m-d H:i:s');
                        $this->MensagemDispositivo->save($notificacaoDispositivo);
                    }
                }
            }
        }
        $this->IosNotifications->disconnect();
        if(!empty($tentarNovamente)) {
            sleep(60);
            $this->notificar($mensagemId,$tentarNovamente);
        }
    }
    
    function enviarEmails() {
        if($this->args)
            $this->enviar($this->args[0], array_slice($this->args, 1));
    }
    
    private function enviar($mensagemId, $usuariosId = array()) {
        $this->Mensagem->bindModel(array('belongsTo' => array('Assunto','Turma')), false);
        $mensagem = $this->Mensagem->read(null, $mensagemId);
        $assunto = "{$mensagem['Assunto']['Item']['nome']} - {$mensagem['Assunto']['nome']}";
        $texto = $this->montaTextoMensagemUsuario($mensagem);
        $this->MensagemUsuario->unbindModel(array(
            'belongsTo' => array('Mensagem')), false);
        $options['conditions'] = array(
            'mensagem_id' => $mensagem['Mensagem']['id'],
            'enviada' => 0,
            "tentativas < 3",
            'Usuario.autoriza_emails' => 1
        );
        if(!empty($usuariosId))
            $options['conditions']['usuario_id'] = $usuariosId;
        $usuarios = $this->MensagemUsuario->find('all',$options);
        $usuariosEnviados = array('falhas' => array(),'sucessos' => array());
        $this->criarMailer();
        if($this->Mailer) {
            $this->Mailer->FromName = utf8_decode("AS Formaturas - {$mensagem['Usuario']['nome']}");
            foreach($mensagem['Arquivo'] as $arquivo)
                $this->Mailer->AddAttachment(
                        $arquivo['diretorio'].$arquivo['nome_secreto'],
                        $arquivo['nome']);
            foreach($usuarios as $usuario) {
                $this->Mailer->AddAddress($usuario['Usuario']['email'],
                        $usuario['Usuario']['nome']);
                $textoMensagem = $texto . $this->montarRodapeMensagemUsuario(
                        $usuario['MensagemUsuario']['id'], $usuario['Usuario']['id'],
                        $mensagem['Assunto']['pendencia']);
                $this->Mailer->Subject = utf8_decode($assunto);
                $this->Mailer->Body    = utf8_decode($textoMensagem);
                $enviada = $this->Mailer->Send();
                $this->Mailer->ClearAddresses();
                $usuario['MensagemUsuario']['tentativas']++;
                if($enviada) {
                    $usuariosEnviados['sucessos'][] = $usuario['Usuario']['id'];
                    $usuario['MensagemUsuario']['enviada'] = 1;
                    $usuario['MensagemUsuario']['data_envio'] = date('Y-m-d H:i:s');
                } else {
                    $usuariosEnviados['falhas'][] = $usuario['Usuario']['id'];
                    $usuario['MensagemUsuario']['enviada'] = 0;
                    $usuario['MensagemUsuario']['data_envio'] = null;
                }
                $this->MensagemUsuario->save($usuario);
                if($enviada)
                    microtime(500);
            }
            if(!empty($usuariosEnviados['falhas'])) {
                sleep(60);
                $this->enviar($mensagemId,$usuariosEnviados['falhas']);
            }
        }
    }
    
    private function montaTextoMensagemUsuario($mensagem) {
        //if($mensagem['Assunto']['pendencia'] != 'Informativo')
            $texto = "Turma {$mensagem['Turma']['id']} - {$mensagem['Turma']['nome']}" .
                "<br />" . htmlspecialchars_decode($mensagem["Mensagem"]["texto"]) . 
                "<i>{$mensagem['Usuario']['nome']} " .
                "({$mensagem['Usuario']['email']})</i><br />";
        //else
            //$texto = htmlspecialchars_decode($mensagem["Mensagem"]["texto"]);
        return $texto;
    }
    
    private function montarRodapeMensagemUsuario($mensagemUsuarioId = "",$usuarioId, $pendencia) {
        $rodape = "<br /><i>Esta mensagem foi gerada automaticamente<br />" .
                "Por favor não responda este email.";
        if($mensagemUsuarioId != "" && $pendencia != "Informativo")
            $rodape.= "<br />Para respondê-lo utilize o botão abaixo" .
                "</i><br /><br /><br /><a style='background:#b91d1d;color:white;padding:5px;text-decoration:none' ".
                "href='{$this->urlSite}/mensagens/responder_deslogado/" .
                base64_encode($mensagemUsuarioId) . "'" .
                ">Responder Mensagem</a><br /><br /><br /><br />";
        elseif($pendencia == "Informativo")
            $rodape.= "</i><br /><br /><a style='background:#b91d1d;color:white;padding:5px;text-decoration:none' ".
                "href='{$this->urlSite}/usuarios/entrar_deslogado/" .
                base64_encode($usuarioId) . "'" .
                ">Acessar o Sistema</a><br /><br /><br /><br />";
        $rodape.= "<p style='font-size:7.5pt; font-family:\"Arial\",\"sans-serif\"; color:#666666'>" .
                "Se não deseja receber mais e-mails com assuntos exclusivamente da sua formatura, " .
                "<a href='{$this->urlSite}/usuarios/cancelar_envio_email/" .
                base64_encode($usuarioId) . "' style='color:#c41b14'" .
                " alt='unsubscribe'>cancele o recebimento</a></p><br />" .
                "<br /><img src='{$this->urlImagemRodape}{$mensagemUsuarioId}' />";
        return $rodape;
    }
    
    function validarAnoAtual() {
        set_time_limit(0);
        ini_set('memory_limit', '512M');
        $this->Mensagem->unbindModelAll();
        $mensagens = $this->Mensagem->find('list',array(
            'conditions' => array(
                "year(Mensagem.data) = '" . date('Y') . "'"
            ),
            'fields' => array('Mensagem.id'),
            'order' => array('Mensagem.id' => 'desc')
        ));
        $this->validarDestinatarios(array_values($mensagens));
    }
    
    function inserirMensagens() {
        $this->MensagemUsuario->unbindModelAll();
        $mensagensUsuarios = $this->MensagemUsuario->find('list',array(
            'group' => array("MensagemUsuario.mensagem_id having count(0) = 1"),
            'fields' => array('mensagem_id'),
            'order' => array('id' => 'desc')
        ));
        $this->validarDestinatarios(array_values($mensagensUsuarios));
    }
    
    function validarDestinatarios($mensagensIds = array()) {
        if(empty($mensagensIds) && empty($this->args)) {
            $this->out('Insira os ids das mensagens');
            exit();
        }
        $mensagensIds = empty($mensagensIds) ? $this->args : $mensagensIds;
        $this->Mensagem->unbindModelAll();
        $this->Assunto->unbindModelAll();
        $this->Usuario->unbindModelAll();
        $this->Mensagem->bindModel(array('belongsTo' => array('Usuario','Assunto')), false);
        $this->Assunto->bindModel(array('belongsTo' => array('Item')), false);
        $mensagens = $this->Mensagem->find('all',array(
            'conditions' => array(
                'Mensagem.id' => $mensagensIds
            )
        ));
        if(!$mensagens) {
            $this->out('Mensagens não encontradas');
            exit();
        }
        foreach($mensagens as $mensagem) {
            $usuarios = $this->obterDestinatarios($mensagem);
            foreach($usuarios as $usuario) {
                $inserido = $this->MensagemUsuario->find('count',array(
                    'conditions' => array(
                        'MensagemUsuario.usuario_id' => $usuario['Usuario']['id'],
                        'MensagemUsuario.mensagem_id' => $mensagem['Mensagem']['id']
                    )
                ));
                if($inserido == 0) {
                    $this->MensagemUsuario->create();
                    $mensagemUsuario = array(
                        'MensagemUsuario' => array(
                            'usuario_id' => $usuario['Usuario']['id'],
                            'mensagem_id' => $mensagem['Mensagem']['id'],
                            'data_cadastro' => date('Y-m-d H:i:s')
                        )
                    );
                    if($usuario['Usuario']['id'] == $mensagem['Usuario']['id'])
                        $mensagemUsuario['MensagemUsuario']['lida'] = 1;
                    $this->MensagemUsuario->save($mensagemUsuario);
                }
            }
        }
    }
    
    private function obterDestinatarios($mensagem) {
        if($mensagem['Assunto']['Item']['nome'] == 'Fale Conosco') {
            $this->out('fale conosco');
        } else {
            $grupos = array('comissao',$mensagem['Assunto']['Item']['grupo']);
            $usuarios = $this->obterUsuariosPorGrupoTurma($mensagem['Mensagem']['turma_id'],$grupos);
            return $usuarios;
        }
    }
    
    private function obterUsuariosPorGrupoTurma($turmaId,$grupos) {
        $this->Usuario->unbindModelAll();
        $this->Usuario->bindModel(array('hasOne' => array('TurmasUsuario')), false);
        $this->Usuario->recursive = 0;
        $options['conditions'] = array(
            'TurmasUsuario.turma_id' => $turmaId,
            'Usuario.grupo' => $grupos,
            'Usuario.ativo' => 1
        );
        $options['fields'] = array('Usuario.id');
        return $this->Usuario->find('all',$options);
    }
    
    function atualizarBase() {
        set_time_limit(0);
        ini_set('memory_limit', '512M');
        $this->Mensagem->unbindModelAll();
        $this->Usuario->unbindModelAll();
        $this->Assunto->unbindModelAll();
        $this->Mensagem->bindModel(array('belongsTo' => array('Usuario','Assunto')), false);
        $this->Assunto->bindModel(array('belongsTo' => array('Item')), false);
        $mensagens = $this->Mensagem->find('all');
        foreach($mensagens as $mensagem) {
            $usuarios = $this->obterUsuarios($mensagem['Usuario']['id'],
                    $mensagem['Mensagem']['turma_id'], array('comissao',$mensagem['Assunto']['Item']['grupo']));
            $usuarios[]['Usuario'] = $mensagem['Usuario'];
            $this->inserirMensagemUsuario($usuarios, $mensagem);
        }
        $this->out('Finalizou!');
    }
    
    private function obterUsuarios($usuarioLogado,$turmaLogada,$grupo) {
        $this->Usuario->unbindModelAll();
        $this->Usuario->bindModel(array('hasOne' => array('TurmasUsuario')), false);
        $this->Usuario->recursive = 0;
        $options['conditions'] = array(
            'TurmasUsuario.turma_id' => $turmaLogada,
            'Usuario.grupo' => $grupo,
            'Usuario.ativo' => 1,
            "Usuario.id <> " => $usuarioLogado
        );
        return $this->Usuario->find('all',$options);
    }
    
    private function inserirMensagemUsuario($usuarios,$mensagem) {
        $this->MensagemUsuario->unbindModelAll();
        foreach($usuarios as $usuario) {
            $mensagemUsuario = $this->MensagemUsuario->find('first',array(
                'conditions' => array(
                    'MensagemUsuario.usuario_id' => $usuario['Usuario']['id'],
                    'MensagemUsuario.mensagem_id' => $mensagem['Mensagem']['id']
                )
            ));
            if(!$mensagemUsuario) {
                $data = str_replace("00:00:00","01:00:01",$mensagem['Mensagem']['data']);
                $save = array(
                    'usuario_id' => $usuario['Usuario']['id'],
                    'mensagem_id' => $mensagem['Mensagem']['id'],
                    'lida' => 1,
                    'data_cadastro' => $data);
                $this->MensagemUsuario->create();
                $this->MensagemUsuario->save($save);
            }
        }
    }

}

?>