<span id="conteudo-titulo" class="box-com-titulo-header">Detalhe do Registro de Contato</span>
<div id="conteudo-container">
    <?php $session->flash(); ?>
    <div class="detalhes">
        <p class="grid_11 alpha omega">
            <label class="grid_5 alpha">Id:</label>
            <label class="grid_6 alpha">Turma: </label>
            <span class="grid_5 alpha first"><?php echo $registro['RegistrosContato']['id'] ?></span>
            <span class="grid_6 alpha"><?php echo $registro['Turma']['nome']?></span></p>
	    <p class="grid_11 alpha omega">
            <label class="grid_5 alpha">Criador: </label>
			<label class="grid_6 alpha">Tipo</label>
        	<span class="grid_5 alpha first"><?php echo $registro['Usuario']['nome']?></span>
            <span class="grid_6 alpha"> <?php echo ucfirst(utf8_encode($registro['RegistrosContato']['tipo']))?> </span>
		</p>
        <?php
        $dateTime =  $datas->create_date_time_from_format('Y-m-d H:i:s', $registro['RegistrosContato']['data']);
        $date = date_format($dateTime, 'd/m/Y');
        $time = date_format($dateTime, 'H:i');
        ?>
        <p class="grid_11 alpha omega">
            <label class="grid_5 alpha">Data:</label> 
            <span class="grid_8 alpha first"> <?php echo $date ?> às <?php echo $time ?> </span></p>
		<p class="grid_11 alpha omega">
            <label class="grid_5 alpha">Participantes Ás</label> 
            <span class="grid_8 alpha first"> <?php echo $registro['RegistrosContato']['funcionarios_as']?> </span>
		</p>
		<p class="grid_11 alpha omega">
            <label class="grid_5 alpha">Participantes Comissão</label> 
            <span class="grid_8 alpha first"> <?php echo $registro['RegistrosContato']['membros_comissao']?> </span>
		</p>
		<p class="grid_11 alpha omega">
            <label class="grid_5 alpha">Assunto</label> 
            <span class="grid_8 alpha first"> <?php echo $registro['RegistrosContato']['assunto']?> </span>
		</p>
		<p class="grid_11 alpha omega">
            <label class="grid_5 alpha">Detalhes</label> 
            <span class="grid_8 alpha first"> <?php echo $registro['RegistrosContato']['detalhes']?> </span>
		</p>

        <p class="grid_11 alpha omega">
            <?php echo $html->link('Voltar', array($this->params['prefix'] => true, 'controller' => 'RegistrosInternos','action' => 'index'), array('class' => 'cancel')); ?>
            <?php
            if ($registro['RegistrosContato']['usuario_id'] == $usuario['Usuario']['id']) {
                echo $html->link('Editar', array($this->params['prefix'] => true , 'controller' => 'registros_contatos', 'action' => 'editar', $registro['RegistrosContato']['id']), array('class' => 'submit '));
                echo $html->link('Apagar', array($this->params['prefix'] => true , 'controller' => 'registros_contatos', 'action' => 'deletar', $registro['RegistrosContato']['id']), array('class' => 'submit '));
            }
            ?>
        </p>
    </div>
</div>