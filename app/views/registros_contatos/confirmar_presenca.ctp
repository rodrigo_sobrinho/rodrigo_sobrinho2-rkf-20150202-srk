<?php if(!$registro) : ?>
<h2 class="fg-color-red">Convite nao encontrado</h2>
<?php elseif(!empty($registro['RegistroContatoUsuario']['data_confirmacao'])) : ?>
<h2 class="fg-color-red">Voce ja respondeu o convite</h2>
<?php else : ?>
<script type="text/javascript">
    $(document).ready(function(){
        var context = ko.contextFor($("#content-body")[0]);
        $(".confimar-presenca").click(function() {
            var id = '<?=$registro['RegistroContatoUsuario']['id'];?>';
            var confirmacao = $(this).data('confirmacao');
            $.ajax({
                url : '<?=$this->here?>',
                data : {
                    data : {
                        id : id,
                        confirmacao : confirmacao
                    }
                },
                type : "POST",
                dataType : "json",
                success: function(response) {
                    context.$data.removerAviso(id,'convite-festa');
                },
                complete : function() {
                    context.$data.avisoSelecionado(false);
                }
            });
        });
    });
</script>
<h3 class="fg-color-blueDark"><?=$registro['Usuario']['nome'];?></h3>
<br />
O consultor(a) <span class="label label-warning"><?=$registro['Consultor']['nome'];?></span> convidou você para uma festa!
<br />
<br />
Local <span class="label label-info"><?=$registro['RegistrosContato']['local'];?></span>
<br />
<br />
Data&nbsp; 
<span class="label label-important">
    <?=date('d/m/Y &\a\g\r\a\v\e;\s H:i',strtotime($registro['RegistrosContato']['data']))?>
</span>
<br />
<br />
<?php if($convidados > 0) : ?>
<span class="label"><?=$convidados?> pessoa(s) já confirmaram presença</span>
<?php else : ?>
Ninguem confirmou presença ainda. Seja o primeiro a confirmar!
<?php endif; ?>
<br />
<br />
<button type="button" class="default confimar-presenca"
    data-confirmacao="1">
    Confirmar Presença
</button>
<button type="button" class="bg-color-red confimar-presenca"
    data-confirmacao="0">
    N&atilde;o Estarei Presente
</button>
<?php endif; ?>
