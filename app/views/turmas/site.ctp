<?php if ($pagina) : ?>
    <h2 id="titulo-pagina"><?=$pagina['TurmaPagina']['titulo'] ?></h2>
    <br />
    <?php if(count($pagina['TurmaPaginaFoto']) > 0) : ?>
    <br />
    <?php foreach($pagina['TurmaPaginaFoto'] as $paginaFoto) : ?>
    <div class="row-fluid">
        <div class="span4">
            <div class="imagem">
                <img src="<?="{$this->webroot}{$paginaFoto['arquivo']}"?>">
            </div>
        </div>
        <div class="span8">
            <div class="descricao">
                <?=$paginaFoto['descricao']?>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
    <?php endif; ?>
<?php else : ?>
<div class="row-fluid">
    <?php if(empty($turma['Turma']['facebook'])) : ?>
    <div class="span12 box-plugin">
        <div class="fb-like-box" data-href="http://www.facebook.com/formaturas.as"
             data-width="940" data-height="500" data-colorscheme="light" data-show-faces="true"
             data-header="false" data-stream="true" data-show-border="false"></div>
    </div>
    <?php else : ?>
    <div class="span6 box-plugin">
        <div class="fb-like-box" data-href="http://www.facebook.com/formaturas.as"
            data-width="445" data-height="500" data-colorscheme="light" data-show-faces="true"
            data-header="false" data-stream="true" data-show-border="false"></div>
    </div>
    <div class="span6 box-plugin">
        <div class="fb-like-box" data-href="<?=$turma['Turma']['facebook']?>"
            data-width="445" data-height="500" data-colorscheme="light" data-show-faces="true"
            data-header="false" data-stream="true" data-show-border="false"></div>
    </div>
    <?php endif; ?>
</div>
<?php if(count($fotosHome) > 0) : $span = 12 / count($fotosHome); ?>
<div class="row-fluid">
<?php foreach($fotosHome as $fotoHome) : ?>
    <div class="span<?=$span?>">
        <div class="imagem">
            <img src="<?="{$this->webroot}{$fotoHome['TurmaSiteFoto']['arquivo']}"?>">
        </div>
        <div class="descricao"><?=$fotoHome['TurmaSiteFoto']['descricao']?></div>
    </div>
<?php endforeach; ?>
</div>
<?php endif; ?>
<?php endif; ?>
