<?php

class RifaTurma extends Model {
	var $name = "RifaTurma";
	var $actsAs = array('Containable');
	var $useTable = "rifas_turmas";
	
	var $belongsTo = array('Rifa','Turma');
	
	var $validate = array(
		'ativo' => array(
			'rule' => 'boolean',
			'required' => true
		),
		
		'cupons' => array(
			'rule' => array('inList', array('50', '100')),
            'message' => 'Número de cupons inválido.'
		),
		'rifa_id' => array(
			'unique'=>array(
				'rule'=>array(
					'checarUnico', array("turma_id", "rifa_id")),
				'message' => 'Esta rifa já está vinculada a essa turma.'
			)
		),
		'turma_id' => array(
		 	'regra1' => array(
            	'rule' => array('limitarAtivos', 1),
            	'message' => 'Esta turma já possui uma rifa ativa.'
            ),
        )
		
	);
	
 	
 	function checarUnico($data, $fields) { 
            if (!is_array($fields)) { 
                    $fields = array($fields); 
            } 
            foreach($fields as $key) { 
                    $tmp[$key] = $this->data[$this->name][$key]; 
            } 
            if (isset($this->data[$this->name][$this->primaryKey])) { 
                    $tmp[$this->primaryKey] = "<>".$this->data[$this->name][$this->primaryKey]; 

            } 
            return $this->isUnique($tmp, false); 
    }
 
 
    function limitarAtivos($check, $limit){
    	if($this->data['RifaTurma']['ativo'] == false)
    		return true;
    		
        //$check will have value: array('promomotion_code' => 'some-value')
        //$limit will have value: 25
        $check['ativo'] = 1;
        
        $vinculacoes_ativas_existentes = $this->find( 'count', array('conditions' => $check, 'recursive' => -1) );

        return $vinculacoes_ativas_existentes < $limit;
    }

}
?>