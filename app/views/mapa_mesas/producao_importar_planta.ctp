<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/fileupload.js"></script>
<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/min/bootstrap/fileupload.css">
<style type="text/css">
    .button.botao{
        margin-top: 0;
        min-height: 30px;
        height: 30px;
        line-height: 10px
    }
    .fileupload .uneditable-input{
        vertical-align: top;
    }
</style>
<div class="row-fluid">
    <h2>
        Mapa de Mesa - Importar Planta
    </h2>
</div>
<div class="alert alert-error hide"></div>
<?php if (!empty($mensagem_erro)): ?>
    <div class="row-fluid">
        <h2>
            <?php echo $mensagem_erro ?>
        </h2>
    </div>
<?php endif ?>
<?php 
    $session->flash(); 
    $buttonAfter = '<button class="helper" onclick="return false" ' .
        'tabindex="-1" type="button"></button>';
?>
<div class="row-fluid">
    <?=$form->create('Mapa',array(
        'url' => "/{$this->params['prefix']}/mapa_mesas/importar_planta",
        'id' => 'form-filtro')) ?>
    <?php echo $form->hidden('binario', array('id' => 'binario')); ?>
    <?php echo $form->hidden('tamanho', array('id' => 'tamanho')); ?>
    <?php echo $form->hidden('extensao', array('id' => 'extensao')); ?>
    <?php echo $form->hidden('arquivo', array('id' => 'arquivo')); ?>
    <?php echo $form->input('nome', array('label' => 'Nome', 'class' => 'input-xlarge')); ?>
    <label>Imagem</label>
    <div class="input fileupload fileupload-new" data-provides="fileupload"
         data-reader="true">
        <div class="input-append">
            <div class="uneditable-input span3"><i class="icon-file fileupload-exists"></i> 
                <span class="fileupload-preview"></span>
            </div>
            <span class="button btn-file botao bg-color-orange fileupload-new">
                <span>Selecione a imagem da planta</span>
                <input type="file"/>
            </span>
            <span class="button btn-file botao bg-color-orange fileupload-exists">
                <span>Alterar</span>
                <input type="file"/>
            </span>
            <a href="#" class="button fileupload-exists botao bg-color-red" data-dismiss="fileupload">Remover</a>
        </div>
        <small>Escolha um arquivo do tipo JPG ou PNG</small>
    </div>
    <br/>
    <div class="input-control text">
        <button type='submit' class='max bg-color-green'>
            Enviar
        </button>
    </div>
    <?= $form->end(array('label' => false,
        'div' => false, 'class' => 'hide')); ?>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $('.fileupload').bind('loaded', function(e) {
        var position = e.src.indexOf("base64,");
        if (position > 0) {
            $("#binario").val(e.src.substring(position + 7));
            $("#tamanho").val(e.file.size);
            if(e.file.type == "")
                $("#extensao").val(e.file.name.split('.').pop());
            else
                $("#extensao").val(e.file.type);
            $("#arquivo").val(e.file.name);
        } else {
            $("#binario").val("");
        }
        return;
    });
    $("#form-filtro").submit(function(e) {
        e.preventDefault();
        var context = ko.contextFor($(".metro-button.reload")[0]);
        $("#form-filtro").find('[value="false"]').attr('disabled','disabled');
        var erro = false;
        var extensao = $("#extensao").val();
        var tamanho = $("#tamanho").val();
        var nome = $("#MapaNome").val();
        if(extensao != 'image/jpeg' && extensao != 'image/png'){
            erro = true;
            $(".alert-error").html('Selecione uma imagem do tipo JPG ou PNG').show();
        }
        if(tamanho > 2000000){
            erro = true;
            $(".alert-error").html('Selecione uma imagem com até 2MB').show();
        }
        if(nome == ''){
            erro = true;
            $(".alert-error").html('Insira o nome do espaço').show();
        }
        if(!erro){
            var dados = $("#form-filtro").serialize();
            var url = $("#form-filtro").attr('action');
            $.ajax({
                url : url,
                data : dados,
                type : "POST",
                dataType : "json",
                complete : function(data) {
                    context.$data.reload();
                    bootbox.hideAll();
                }
            });
        }
    });
})
</script>