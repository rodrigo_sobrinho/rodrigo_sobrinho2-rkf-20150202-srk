<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
    <head>
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
        <?= $html->charset(); ?>
        <title><?= $title_for_layout; ?></title>
        <link rel="icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon"> 
        <link rel="shortcut icon" href="<?php echo $this->webroot; ?>img/favicon2.ico" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>catalogos/css/bootstrap.min.css?v=0.7">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>catalogos/css/fontes.css?v=0.7">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>catalogos/css/font-awesome.min.css?v=0.7">
        <link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>catalogos/css/default.css?v=0.7">
        <script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/jquery-1.9.1.js?v=0.1"></script>
        <script type="text/javascript" src="<?= $this->webroot ?>catalogos/js/bootstrap.min.js?v=0.1"></script>
        <script type="text/javascript" src="<?= $this->webroot ?>catalogos/js/default.js?v=0.1"></script>
    </head>
    <body>
        <nav class="navbar navbar-default" id="menu-topo" role="navigation">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="text-align: center">
                        <div class="logo-min-branco">
                            formaturas
                        </div>
                    </div>
                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 navegacao">
                        <ul class="nav navbar-nav">
                            <?php foreach($eventos as $evento) : ?>
                            <li<?=isset($eventoSelecionado) ? ($eventoSelecionado["TiposEvento"]['id'] == $evento["TiposEvento"]['id'] ? " class='active'" : "") : ""?>>
                                <a href="/<?=$this->params['prefix']?>/catalogo/evento/<?=$evento["TiposEvento"]['id']?>">
                                    <?=$evento["TiposEvento"]['nome']?>
                                </a>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="/comercial">
                                    Sair do Cat&aacute;logo
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <div class="container-fluid">
            <div class="row" id="content">
                <?php if(isset($itens)) : ?>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 menu-esquerda">
                    <div class="form-group has-feedback" id="url-busca"
                        data-url="/<?=$this->params['prefix']?>/catalogo/busca">
                        <label class="control-label sr-only" for="inputSuccess5">Hidden label</label>
                        <input type="text" class="form-control" id="input-busca" value="<?=isset($filtro) ? $filtro : ''?>">
                        <span class="faicon faicon-lg faicon-search form-control-feedback icone-busca"
                            id="icone-busca"></span>
                    </div>
                    <ul class="nav nav-pills nav-stacked">
                        <?php foreach($itens as $item) : ?>
                        <li<?=isset($itemSelecionado) ? ($itemSelecionado["CatalogoItem"]['id'] == $item["CatalogoItem"]['id'] ? " class='active'" : "") : ""?>>
                            <a href="/<?=$this->params['prefix']?>/catalogo/item/<?=$item["CatalogoItem"]['id']?>">
                                <?=$item["CatalogoItem"]['nome']?>
                            </a>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <?php endif; ?>
                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 box-conteudo">
                    <?=$content_for_layout; ?>
                </div>
            </div>
        </div>
    </body>
</html>
