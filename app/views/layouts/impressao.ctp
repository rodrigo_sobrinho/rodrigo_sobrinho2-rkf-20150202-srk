<?php
/* SVN FILE: $Id$ */

/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework (http://www.cakephp.org)
 * Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2005-2008, Cake Software Foundation, Inc. (http://www.cakefoundation.org)
 * @link          http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @version       $Revision$
 * @modifiedby    $LastChangedBy$
 * @lastmodified  $Date$
 * @license       http://www.opensource.org/licenses/mit-license.php The MIT License
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
            <?php echo $html->charset(); ?>
            <title>
                <?php echo $title_for_layout; ?>
            </title>
            <?php
            //echo $html->meta('icon');

            echo $html->css('estrutura');
            echo $html->css('forms');
            echo $html->css('grid');
            echo $html->css('reset');
            echo $html->css('tabelas');
            echo $html->css('texto');
            echo $html->css('calendario');
            echo $html->css('jquery-ui');
			echo $html->css('contratos');
			echo $html->css('colorbox');
			echo $html->css('painel_formando');

			?>
            <!--[if IE 6]><?php echo $html->css('grid_ie6'); ?><![endif]--> 
            <!--[if IE 7]><?php echo $html->css('grid_ie7'); ?><![endif]--> 
    </head>
    <body>
        <div class="container_16">
            <!-- #conteudo start -->
            <div id="conteudo-wrap">
                <div id="conteudo"  class="box-com-titulo">
<?php echo $content_for_layout; ?>
                </div>
            </div>
            <!-- #conteudo end -->

    </body>
</html>
