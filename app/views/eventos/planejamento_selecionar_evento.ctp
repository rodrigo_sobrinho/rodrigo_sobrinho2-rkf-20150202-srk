<link rel="stylesheet" type="text/css" href="<?= $this->webroot ?>metro/css/max/bootstrap/select.css">
<script type="text/javascript" src="<?= $this->webroot ?>metro/js/min/bootstrap/select.js"></script>
<script type="text/javascript">

$(document).ready(function(){
    //plugin de ativacao do plugin
    $('.selectpicker').selectpicker({width:'100%'});
    $(".enviar").click(function(){
        //pega o value (id)
        valor = $('.selectpicker option:selected').val();
            $.ajax ({
                //prefixo, controller, action e parametro
                url : '/<?=$this->params['prefix']?>/eventos/mapa/' + valor,
                data : {
                    Evento : $("select[name='data[Evento][id]']").val(),
                    Mapa : $("select[name='data[Evento][mapa]']").val()
                },
                type : "POST",
                complete: function(response) {
                    $('#conteudo').html(response.responseText);
                }
            });
    });
});
</script>
<?php $session->flash(); ?>
<div class="row-fluid">
    <h2>
        Selecione o Evento
    </h2>
</div>
<br />
<div class="row-fluid">
    <div class="span6">
        <?=$form->input('Evento.id', array(
            'options' => $eventos,
            'type' => 'select',
            'class' => 'selectpicker',
            'label' => 'Eventos',
            'div' => 'input-control text')); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span2">
        <button type='submit' class='button max bg-color-greenDark enviar'>
            Enviar
            <i class='icon-ok'></i>
        </button>
    </div>
</div>